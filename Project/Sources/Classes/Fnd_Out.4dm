Class constructor
	This:C1470.maxColumns:=5  // There is no actual maximum, this is just for the automatic display
	
	// Display details
	This:C1470.headerFontName:=Storage:C1525.font.largeName
	This:C1470.columnFontName:=Storage:C1525.font.largeName
	This:C1470.headerFontSize:=Storage:C1525.font.listHeader
	This:C1470.columnFontSize:=Storage:C1525.font.listItem
	This:C1470.columnRowHeight:=Storage:C1525.font.listRowHeight
	
	// Data related
	This:C1470.addRecordNumber:=No current record:K29:2
	This:C1470.recordIDField:=0
	This:C1470.currentTable:=0
	
	// Use Fnd_Out for this process
	This:C1470.active:=False:C215  // Never use this Fnd_Out unless told to
	
Function isActive($setActive : Boolean)->$isActive : Boolean
	
	If (Count parameters:C259>=1)
		This:C1470.active:=$setActive
	End if 
	
	$isActive:=This:C1470.active
	
	