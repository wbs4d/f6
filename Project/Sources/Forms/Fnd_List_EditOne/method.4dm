  // ----------------------------------------------------
  // Form Method: [Fnd_List].Fnd_List_EditOne

  // Created by Dave Batton on Oct 4, 2003
  // Modified by Dave Batton on Aug 2, 2004
  //   The Fnd_Gen_ButtonText calls now moves the Cancel button directly.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($move_i;$biggestMove_i;$left_i;$top_i;$right_i;$bottom_i)

Case of 
	: (Form event code:C388=On Load:K2:1)
		If (Fnd_Gen_ComponentAvailable ("Fnd_Loc"))  // We need this for Fnd_Loc_FixButtonWidths, but might as well bracket all of this.
			  // Localize the buttons.  Keep track of how much bigger the largest one gets.
			$move_i:=Fnd_Gen_ButtonText ("Fnd_List_AddButton_i";Fnd_Gen_GetString ("Fnd_List";"AddButton");Align right:K42:4)
			$biggestMove_i:=Fnd_Gen_ButtonText ("Fnd_List_DeleteButton_i";Fnd_Gen_GetString ("Fnd_List";"DeleteButton");Align right:K42:4)
			If ($move_i<$biggestMove_i)  // Move will be negative.
				$biggestMove_i:=$move_i
			End if 
			$move_i:=Fnd_Gen_ButtonText ("Fnd_List_SortButton_i";Fnd_Gen_GetString ("Fnd_List";"SortButton");Align right:K42:4)
			If ($move_i<$biggestMove_i)
				$biggestMove_i:=$move_i
			End if 
			
			  // Make all of the buttons the same size again. 
			Fnd_Loc_FixButtonWidths (OBJECT Get pointer:C1124(Object named:K67:5;"Fnd_List_AddButton_i");\
				OBJECT Get pointer:C1124(Object named:K67:5;"Fnd_List_DeleteButton_i");\
				OBJECT Get pointer:C1124(Object named:K67:5;"Fnd_List_SortButton_i"))
			
			  // Now move the list to make room for the buttons.
			If ($biggestMove_i<0)
				OBJECT GET COORDINATES:C663(<>Fnd_List_EditOneListItems_i;$left_i;$top_i;$right_i;$bottom_i)
				$right_i:=$right_i+$biggestMove_i  // The move is negative.
				OBJECT MOVE:C664(<>Fnd_List_EditOneListItems_i;$left_i;$top_i;$right_i;$bottom_i;*)
			End if 
			
			  // Localize the OK and Cancel buttons.
			Fnd_Gen_ButtonText ("Fnd_List_OKButton_i";Fnd_Gen_GetString ("Fnd_List";"OKButton");Align right:K42:4;"Fnd_List_CancelButton_i")
			Fnd_Gen_ButtonText ("Fnd_List_CancelButton_i";Fnd_Gen_GetString ("Fnd_List";"CancelButton");Align right:K42:4)
		End if 
End case 


If ((Form event code:C388=On Load:K2:1) | (Form event code:C388=On Clicked:K2:4))
	If (Count list items:C380(<>Fnd_List_EditOneListItems_i)>1)
		OBJECT SET ENABLED:C1123(*;"Fnd_List_SortButton_i";True:C214)
	Else 
		OBJECT SET ENABLED:C1123(*;"Fnd_List_SortButton_i";False:C215)
	End if 
	If (Selected list items:C379(<>Fnd_List_EditOneListItems_i)>0)
		OBJECT SET ENABLED:C1123(*;"Fnd_List_DeleteButton_i";True:C214)
	Else 
		OBJECT SET ENABLED:C1123(*;"Fnd_List_DeleteButton_i";False:C215)
	End if 
End if 