  // ----------------------------------------------------
  // Object Method: [Fnd_List].Fnd_List_EditOne.Fnd_List_AddButton_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------


C_TEXT:C284($item_t)
C_LONGINT:C283($winLeft_i;$winTop_i;$listLeft_i;$listTop_i;$ignore_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		$item_t:=Fnd_Gen_GetString ("Fnd_List";"NewItemDefault")
		
		  // Add the item to the top of the list.
		
		If (Count list items:C380(<>Fnd_List_EditOneListItems_i)>0)
			SELECT LIST ITEMS BY POSITION:C381(<>Fnd_List_EditOneListItems_i;1)
			INSERT IN LIST:C625(<>Fnd_List_EditOneListItems_i;*;$item_t;0)
		Else 
			APPEND TO LIST:C376(<>Fnd_List_EditOneListItems_i;$item_t;0)
		End if 
		  //REDRAW LIST(<>Fnd_List_EditOneListItems_i)
		
		  // These lines will select the item so the user can type the list title.
		
		GET WINDOW RECT:C443($winLeft_i;$winTop_i;$ignore_i;$ignore_i)
		OBJECT GET COORDINATES:C663(<>Fnd_List_EditOneListItems_i;$listLeft_i;$listTop_i;$ignore_i;$ignore_i)
		POST EVENT:C467(Mouse down event:K17:2;0;Tickcount:C458;($winLeft_i+$listLeft_i+2);($winTop_i+$listTop_i+2);Command key mask:K16:1;Current process:C322)
		POST KEY:C465(Character code:C91("A");Command key mask:K16:1;Current process:C322)
End case 
