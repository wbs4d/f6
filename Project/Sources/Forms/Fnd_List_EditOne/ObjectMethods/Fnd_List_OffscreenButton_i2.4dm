  // ----------------------------------------------------

  // Object Method: [Fnd_List].Fnd_List_EditOne.Fnd_List_OffscreenButton_i


  // Since we enable and disable the OK button just post an Enter

  //   to trigger the OK button rather than accepting the dialog here.


  // Created by Dave Batton on Sep 2, 2003

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		POST KEY:C465(Carriage return:K15:38)
End case 
