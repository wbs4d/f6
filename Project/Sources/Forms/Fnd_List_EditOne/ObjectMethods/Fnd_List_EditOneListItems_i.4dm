  // ----------------------------------------------------
  // Object Method: ◊Fnd_List_EditOneListItems_i

  // Created by Dave Batton on Oct 10, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($mouseX;$mouseY;$mouseButton;$itemRef;$dropPosition;$subList;$dragPosition)
C_TEXT:C284($itemText)
C_BOOLEAN:C305($expanded;$enterable)

Case of 
	: (Form event:C388=On Double Clicked:K2:5)
		EDIT ITEM:C870(Self:C308->)
		
		
	: (Form event:C388=On Drop:K2:12)
		$dropPosition:=Drop position:C608
		If ($dropPosition=-1)  // It was dropped below the last item.
			$dropPosition:=Count list items:C380(Self:C308->)+1
		End if 
		GET LIST ITEM:C378(Self:C308->;Selected list items:C379(Self:C308->);$itemRef;$itemText;$subList;$expanded)
		If ($itemRef#0)
			GET LIST ITEM PROPERTIES:C631(Self:C308->;$itemRef;$enterable)
		End if 
		$dragPosition:=Selected list items:C379(Self:C308->)
		DELETE FROM LIST:C624(Self:C308->;*)
		If ($dropPosition>$dragPosition)
			$dropPosition:=$dropPosition-1  // We need to account for the deleted item.
		End if 
		SELECT LIST ITEMS BY POSITION:C381(Self:C308->;$dropPosition)
		If ($dropPosition<=Count list items:C380(Self:C308->))
			INSERT IN LIST:C625(Self:C308->;*;$itemText;$itemRef;$subList;$expanded)
		Else 
			APPEND TO LIST:C376(Self:C308->;$itemText;$itemRef;$subList;$expanded)
		End if 
		If (($itemRef#0) & (Not:C34($enterable)))  // Restore this property.
			SET LIST ITEM PROPERTIES:C386(Self:C308->;0;$enterable;Plain:K14:1;0)
		End if 
		  //REDRAW LIST(Self->)
End case 