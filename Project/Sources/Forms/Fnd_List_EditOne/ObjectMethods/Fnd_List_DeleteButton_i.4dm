  // ----------------------------------------------------
  // Object Method: [Fnd_List].Fnd_List_EditOne.Fnd_List_DeleteButton_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by: Walt Nelson (2/11/10) to fix delete button
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($itemRef_t)
C_BOOLEAN:C305($enterable_b)
C_TEXT:C284($item_t)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		GET LIST ITEM:C378(<>Fnd_List_EditOneListItems_i;Selected list items:C379(<>Fnd_List_EditOneListItems_i);$itemRef_t;$item_t)
		  //GET LIST ITEM PROPERTIES(◊Fnd_List_EditOneListItems_i;$itemRef_t;$enterable_b)  ` original
		GET LIST ITEM PROPERTIES:C631(<>Fnd_List_EditOneListItems_i;*;$enterable_b)  // fix???? wwn (2/11/10)
		
		If ($enterable_b)
			DELETE FROM LIST:C624(<>Fnd_List_EditOneListItems_i;*;*)
			If (Selected list items:C379(<>Fnd_List_EditOneListItems_i)>Count list items:C380(<>Fnd_List_EditOneListItems_i))
				SELECT LIST ITEMS BY POSITION:C381(<>Fnd_List_EditOneListItems_i;0)
			End if 
			  //REDRAW LIST(<>Fnd_List_EditOneListItems_i)
		End if 
End case 
