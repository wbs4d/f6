  // ----------------------------------------------------
  // Form Method: Fnd_Gen_Reg_DemoDialogFormMethod

  // We call this from a protected method because a public form can't
  //   call private methods.

  // Created by Dave Batton on Aug 16, 2003
  // Modified by Dave Batton on Jan 29, 2004
  //   If run on 4D Server, the dialog now Continues on it's own after
  //   the seven second countdown delay.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------


C_LONGINT:C283($move_i;$secondsLeft_i;$left_i;$top_i;$right_i;$bottom_i;$ignore_i)
C_TEXT:C284($continueButtonLabel_t)

Case of 
	: (Form event:C388=On Load:K2:1)
		  // We're going to localize this without requiring the localization component.
		
		Case of 
			: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
				FORM GOTO PAGE:C247(2)
		End case 
		
		  // Start a timer to count down for the delay.
		
		SET TIMER:C645(5)
		<>Fnd_Gen_Reg_CountDown1_t:=""
		<>Fnd_Gen_Reg_CountDown2_t:=<>Fnd_Gen_Reg_CountDown1_t
		
		<>Fnd_Gen_Reg_FinishTime_i:=Tickcount:C458+(5*60)  // 5 second delay.
		
		OBJECT SET TITLE:C194(<>Fnd_Gen_Reg_OKButton_i;"")
		OBJECT SET ENABLED:C1123(<>Fnd_Gen_Reg_OKButton_i;False:C215)
		
		
	: (Form event:C388=On Timer:K2:25)
		$secondsLeft_i:=(<>Fnd_Gen_Reg_FinishTime_i-Tickcount:C458)/60
		Case of 
			: ($secondsLeft_i<=0)
				OBJECT SET VISIBLE:C603(<>Fnd_Gen_Reg_CountDown1_t;False:C215)
				OBJECT SET VISIBLE:C603(<>Fnd_Gen_Reg_CountDown2_t;False:C215)
				Case of 
					: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
						$continueButtonLabel_t:="Continuer"
					Else 
						$continueButtonLabel_t:="Continue"
				End case 
				OBJECT SET TITLE:C194(<>Fnd_Gen_Reg_OKButton_i;$continueButtonLabel_t)
				OBJECT SET ENABLED:C1123(<>Fnd_Gen_Reg_OKButton_i;True:C214)
				SET TIMER:C645(0)
				If (Application type:C494=4D Server:K5:6)  // DB040129 - Added this.
					
					ACCEPT:C269
				End if 
			: ($secondsLeft_i<=7)
				<>Fnd_Gen_Reg_CountDown1_t:=String:C10($secondsLeft_i)
				<>Fnd_Gen_Reg_CountDown2_t:=<>Fnd_Gen_Reg_CountDown1_t
		End case 
End case 
