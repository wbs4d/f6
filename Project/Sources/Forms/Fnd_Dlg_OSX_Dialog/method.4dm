// ----------------------------------------------------
// Form Method: [Fnd_Dlg].Fnd_Dlg_OSX_Dialog

// Created by Dave Batton on Jul 5, 2003
// Modified by Dave Batton on Jun 2, 2004
//   Fixed a bug that caused the Cancel button to move the wrong way
//   if the OK button was resized due to localization.
// Modified by Dave Batton on Aug 2, 2004
//   The Fnd_Gen_ButtonText calls now moves the Cancel button directly.
// Modified by Dave Batton on May 15, 2005
//   Removed some unused assignments to the $maxWidth_i variable.
//   Rewrote to use the icon picture library name rather than an icon number.
// Modified by Dave Batton on Jul 21, 2005
//   Added code to move the static text objects out of the way if the entry field is visible.
// [1] Added by: John Craig (7/2/09) Added this to check if the picture is in the host database's library.
// [2] Added by: John Craig (12/10/09) - to use external image library
// Mod by Wayne Stewart, (2021-04-05) - Add support for loading icon from Fnd_Bttn_AlternativePicturePath folder if present
// ----------------------------------------------------

C_LONGINT:C283($move_i; $lines_i; $pixels_i; $winLeft_i; $winTop_i; $winRight_i; $winBottom_i; $left_i; $top_i; $right_i; $bottom_i)
C_TEXT:C284($iconFormat_t; $Path_t)

Case of 
	: (Form event code:C388=On Load:K2:1)
		
		Fnd_Dlg_FixButtonTexts  // Set the button texts.
		
		Fnd_Gen_ButtonText(->Fnd_Dlg_OKButton_i; Fnd_Dlg_Button1_t; Align right:K42:4; ->Fnd_Dlg_CancelButton_i)
		
		If (Fnd_Dlg_Button2_t="")
			OBJECT SET VISIBLE:C603(Fnd_Dlg_CancelButton_i; False:C215)
		Else 
			Fnd_Gen_ButtonText(->Fnd_Dlg_CancelButton_i; Fnd_Dlg_Button2_t; Align right:K42:4)
		End if 
		
		If (Fnd_Dlg_Button3_t="")
			OBJECT SET VISIBLE:C603(Fnd_Dlg_OtherButton_i; False:C215)
		Else 
			Fnd_Gen_ButtonText(->Fnd_Dlg_OtherButton_i; Fnd_Dlg_Button3_t; Align left:K42:2)
		End if 
		
		Fnd_Bttn_Init  // [2]
		
		$Path_t:=Fnd_Bttn_AlternativePicturePath
		If (Length:C16($Path_t)=0)
			$Path_t:=<>Fnd_Bttn_PicturePath_t
		End if 
		
		Case of 
			: (Position:C15(Folder separator:K24:12; Fnd_Dlg_IconPictureName_t)>0)\
				 & (Test path name:C476(Fnd_Dlg_IconPictureName_t)=Is a document:K24:1)  // They have provided a full path
				READ PICTURE FILE:C678(Fnd_Dlg_IconPictureName_t; Fnd_Dlg_IconPicture_pic)
				
			: ($Path_t#"")  // [2]
				READ PICTURE FILE:C678($Path_t+Fnd_Dlg_IconPictureName_t+".png"; Fnd_Dlg_IconPicture_pic)  // [2]
			Else 
				GET PICTURE FROM LIBRARY:C565(Fnd_Dlg_IconPictureName_t; Fnd_Dlg_IconPicture_pic)
				
		End case 
		
		If (Picture size:C356(Fnd_Dlg_IconPicture_pic)=0)  //[1] Added by: John Craig (7/2/09) Check if the picture is in the host database's library. ->
			If (<>Fnd_Gen_RunningInHost_b)  // was If (Structure file(*)#"Foundation_@")
				EXECUTE METHOD:C1007("Fnd_Host_GetPictureFromLibrary"; *; Fnd_Dlg_IconPictureName_t; ->Fnd_Dlg_IconPicture_pic)  //[1]
			End if   //[1]
		End if   //[1] Added by: John Craig (7/2/09) Check if the picture is in the host database's library. <-
		
		$lines_i:=Fnd_Ext_TextLines(Fnd_Dlg_Text1_t; 372; <>Fnd_Dlg_FontName_t; <>Fnd_Dlg_FontSize_i; <>Fnd_Dlg_FontStyle_i)
		
		$pixels_i:=($lines_i*<>Fnd_Dlg_FontHeight_i)-<>Fnd_Dlg_FontHeight_i
		
		OBJECT MOVE:C664(Fnd_Dlg_Text2_t; 0; $pixels_i)
		
		Case of 
			: (Fnd_Dlg_RequestDefault_t="")  // Not a Request dialog.
				OBJECT SET VISIBLE:C603(Fnd_Dlg_RequestValue_t; False:C215)
			: (Fnd_Dlg_RequestDefault_t="*")  // It's a password entry.
				Fnd_Dlg_RequestValue_t:=""
				OBJECT SET FONT:C164(Fnd_Dlg_RequestValue_t; "%Password")
			: (Fnd_Dlg_RequestDefault_t="**")  // A Request dialog with no default.
				Fnd_Dlg_RequestValue_t:=""
			Else 
				Fnd_Dlg_RequestValue_t:=Fnd_Dlg_RequestDefault_t
		End case 
		
		// If we are displaying the entry field we need to move the other objects
		//   so they don't block it.  - DB050721
		If (Fnd_Dlg_RequestDefault_t#"")
			GET WINDOW RECT:C443($winLeft_i; $winTop_i; $winRight_i; $winBottom_i)
			OBJECT GET COORDINATES:C663(Fnd_Dlg_Text1_t; $left_i; $top_i; $right_i; $bottom_i)
			$bottom_i:=($winBottom_i-$winTop_i)-80
			OBJECT MOVE:C664(Fnd_Dlg_Text1_t; $left_i; $top_i; $right_i; $bottom_i; *)
			OBJECT GET COORDINATES:C663(Fnd_Dlg_Text2_t; $left_i; $top_i; $right_i; $bottom_i)
			$bottom_i:=($winBottom_i-$winTop_i)-80
			OBJECT MOVE:C664(Fnd_Dlg_Text2_t; $left_i; $top_i; $right_i; $bottom_i; *)
		End if 
End case 
