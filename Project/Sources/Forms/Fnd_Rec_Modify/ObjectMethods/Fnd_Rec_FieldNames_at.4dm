  // ----------------------------------------------------
  // Object Method: Fnd_Rec_FieldNames_at

  // Created by Dave Batton on Dec 4, 2005
  // ----------------------------------------------------

Case of 
	: (Type:C295((Fnd_Rec_Fields_aptr{Self:C308->})->)=Is boolean:K8:9)
		OBJECT SET VISIBLE:C603(Fnd_Rec_Value_t;False:C215)
		OBJECT SET VISIBLE:C603(Fnd_Rec_Values_at;True:C214)
		
	Else 
		OBJECT SET VISIBLE:C603(Fnd_Rec_Value_t;True:C214)
		OBJECT SET VISIBLE:C603(Fnd_Rec_Values_at;False:C215)
		GOTO OBJECT:C206(Fnd_Rec_Value_t)
End case 