// ----------------------------------------------------
// Form Method: Fnd_Rec_Modify

// Created by Dave Batton on Dec 4, 2005
// Mod by Wayne Stewart, 2023-07-26 - Call Fnd_Loc_FixLabelWidths directly without checking if available
// ----------------------------------------------------

C_POINTER:C301($field_ptr)

Case of 
	: (Form event code:C388=On Load:K2:1)
		Fnd_Rec_Label_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModDialogTitle"; String:C10(Records in selection:C76(Fnd_Rec_Table_ptr->)); Fnd_Rec_TableName_t)
		
		Fnd_Rec_FieldNameLabel_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModFieldNameLabel")
		Fnd_Rec_NewValueLabel_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModNewValueLabel")
		
		Fnd_Loc_FixLabelWidths(->Fnd_Rec_FieldNameLabel_t; ->Fnd_Rec_FieldNames_at; ->Fnd_Rec_NewValueLabel_t; ->Fnd_Rec_Values_at; ->Fnd_Rec_NewValueLabel_t; ->Fnd_Rec_Value_t)
		OBJECT MOVE:C664(Fnd_Rec_Value_t; 4; 0)  // A minor visual adjustment.
		
		Fnd_Rec_SizeArrayObject(->Fnd_Rec_FieldNames_at; Align left:K42:2)
		Fnd_Rec_SizeArrayObject(->Fnd_Rec_Values_at; Align left:K42:2)
		
		If ((Fnd_Rec_FieldNames_at<1) | (Fnd_Rec_FieldNames_at>Size of array:C274(Fnd_Rec_FieldNames_at)))
			Fnd_Rec_FieldNames_at:=1
		End if 
		
		Case of 
			: (Type:C295((Fnd_Rec_Fields_aptr{Fnd_Rec_FieldNames_at})->)=Is boolean:K8:9)
				OBJECT SET VISIBLE:C603(Fnd_Rec_Value_t; False:C215)
				If (Fnd_Rec_Values_at=0)
					Fnd_Rec_Values_at:=1
				End if 
				
			Else 
				OBJECT SET VISIBLE:C603(Fnd_Rec_Values_at; False:C215)
		End case 
		
		
		// Set or hide the Formula Editor button.
		If (Fnd_Rec_EditorButtonLabel_t="")
			OBJECT SET VISIBLE:C603(Fnd_Rec_EditorButton_i; False:C215)
		Else 
			Fnd_Gen_ButtonText(->Fnd_Rec_EditorButton_i; Fnd_Rec_EditorButtonLabel_t; Align left:K42:2)
		End if 
		
		// Localize the OK and Cancel buttons.
		Fnd_Gen_ButtonText(->Fnd_Rec_OKButton_i; Fnd_Gen_GetString("Fnd_Rec"; "ModOKButton"); Align right:K42:4; ->Fnd_Rec_CancelButton_i)
		Fnd_Gen_ButtonText(->Fnd_Rec_CancelButton_i; Fnd_Gen_GetString("Fnd_Rec"; "ModCancelButton"); Align right:K42:4)
End case 

If ((Form event code:C388=On Load:K2:1) | (Form event code:C388=On Clicked:K2:4) | (Form event code:C388=On Data Change:K2:15))
	$field_ptr:=Fnd_Rec_Fields_aptr{Fnd_Rec_FieldNames_at}
	Fnd_Rec_Formula_t:="["+Table name:C256($field_ptr)+"]"+Field name:C257($field_ptr)+":="
	Case of 
		: ((Type:C295($field_ptr->)=Is text:K8:3) | (Type:C295($field_ptr->)=Is alpha field:K8:1))
			Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+Char:C90(Double quote:K15:41)+Fnd_Rec_Value_t+Char:C90(Double quote:K15:41)
		: (Type:C295($field_ptr->)=Is date:K8:7)
			Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+"!"+Fnd_Rec_Value_t+"!"
		: (Type:C295($field_ptr->)=Is time:K8:8)
			Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+"?"+Fnd_Rec_Value_t+"?"
		: ((Type:C295($field_ptr->)=Is integer:K8:5) | (Type:C295($field_ptr->)=Is longint:K8:6) | (Type:C295($field_ptr->)=Is real:K8:4))
			Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+Fnd_Rec_Value_t
		: (Type:C295($field_ptr->)=Is boolean:K8:9)
			If (Fnd_Rec_Values_at=1)
				Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+Command name:C538(214)  //214=TRUE
			Else 
				Fnd_Rec_Formula_t:=Fnd_Rec_Formula_t+Command name:C538(215)  //215=FALSE
			End if 
		Else 
			Fnd_Gen_BugAlert(Current method name:C684; "Unable to handle a field of type "+String:C10(Type:C295($field_ptr->))+".")
	End case 
	
End if 