// ----------------------------------------------------
// Form Method: Fnd_Art_About

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on May 13, 2004
//   No longer puts the word "Version" before the version number.
// Modified by Ed Heckman, October 27, 2015
// Mod by Wayne Stewart, (2019-10-26) - Fnd-6 conversion
// ----------------------------------------------------


Case of 
	: (Form event code:C388=On Load:K2:1)
		Form:C1466.Art_DatabaseName_t:=Fnd_Gen_GetDatabaseInfo("DatabaseName")
		Form:C1466.Art_DatabaseVersion_t:=Fnd_Gen_GetDatabaseInfo("DatabaseVersion")
		Form:C1466.Art_DatabaseCopyright_t:=Fnd_Gen_GetDatabaseInfo("DatabaseCopyright")
		
		Fnd_Art_SetBestFontSize("Fnd_Art_DatabaseName_t@"; 13)
		
		Form:C1466.Art_DatabaseURL_t:=Fnd_Gen_GetDatabaseInfo("DatabaseURL")
		
		If (Form:C1466.Art_DatabaseURL_t="")
			OBJECT SET ENABLED:C1123(*; "Fnd_Art_URLButton_i"; False:C215)
		End if 
End case 

Fnd_Gen_FormMethod
