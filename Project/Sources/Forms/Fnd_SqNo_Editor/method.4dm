  // ----------------------------------------------------
  // Form Method: [Fnd_SqNo].Fnd_SqNo_Editor

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Dave Batton on May 30, 2004
  //   Changed the Fnd_Gen_GetString calls to Fnd_Gen_GetString and wrapped
  //      the Fnd_Loc_FixButtonWidths call in an EXECUTE so the Localization
  //      component isn't required.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($biggestMove_i;$move_i;$left_i;$top_i;$right_i;$bottom_i)
C_BOOLEAN:C305($showAllButtons_b)

Case of 
	: (Form event:C388=On Load:K2:1)
		<>Fnd_SqNo_Label1_t:=Fnd_Gen_GetString ("Fnd_SqNo";"ListTitle")
		
		$showAllButtons_b:=Fnd_SqNo_IsInDesignGroup 
		
		  // Localize the buttons.  Keep track of how much bigger the largest one gets.
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_SqNo_AddGroupButton_i;Fnd_Gen_GetString ("Fnd_SqNo";"AddButton");Align right:K42:4)
		$biggestMove_i:=Fnd_Gen_ButtonText (-><>Fnd_SqNo_EditGroupButton_i;Fnd_Gen_GetString ("Fnd_SqNo";"EditButton");Align right:K42:4)
		If (($showAllButtons_b) & ($move_i<$biggestMove_i))  // Move will be negative.
			$biggestMove_i:=$move_i
		End if 
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_SqNo_DeleteGroupButton_i;Fnd_Gen_GetString ("Fnd_SqNo";"DeleteButton");Align right:K42:4)
		If (($showAllButtons_b) & ($move_i<$biggestMove_i))
			$biggestMove_i:=$move_i
		End if 
		
		  // Make all of the buttons the same size again. 
		If (Fnd_Gen_ComponentAvailable ("Fnd_Loc"))  // DB060228
			EXECUTE METHOD:C1007("Fnd_Loc_FixButtonWidths";*;-><>Fnd_SqNo_AddGroupButton_i;-><>Fnd_SqNo_EditGroupButton_i;-><>Fnd_SqNo_DeleteGroupButton_i)
		End if 
		
		  // Now move the list to make room for the buttons.
		If ($biggestMove_i<0)
			OBJECT GET COORDINATES:C663(<>Fnd_SqNo_NamesList_i;$left_i;$top_i;$right_i;$bottom_i)
			$right_i:=$right_i+$biggestMove_i  // The move is negative.
			OBJECT MOVE:C664(<>Fnd_SqNo_NamesList_i;$left_i;$top_i;$right_i;$bottom_i;*)
		End if 
		
		  // Hide the Add and Delete buttons if the user isn't the Designer.
		If ($showAllButtons_b)
			OBJECT SET ENTERABLE:C238(<>Fnd_SqNo_NamesList_i;True:C214)
		Else 
			OBJECT SET ENTERABLE:C238(<>Fnd_SqNo_NamesList_i;False:C215)
			OBJECT GET COORDINATES:C663(<>Fnd_SqNo_AddGroupButton_i;$left_i;$top_i;$right_i;$bottom_i)
			OBJECT SET VISIBLE:C603(<>Fnd_SqNo_AddGroupButton_i;False:C215)
			OBJECT SET VISIBLE:C603(<>Fnd_SqNo_DeleteGroupButton_i;False:C215)
			OBJECT MOVE:C664(<>Fnd_SqNo_EditGroupButton_i;$left_i;$top_i;$right_i;$bottom_i;*)
		End if 
		
		  // Start with no items selected.
		SELECT LIST ITEMS BY POSITION:C381(<>Fnd_SqNo_NamesList_i;0)
		  //REDRAW LIST(<>Fnd_SqNo_NamesList_i)
End case 

If (Selected list items:C379(<>Fnd_SqNo_NamesList_i)>0)  // A group is selected.
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_EditGroupButton_i;True:C214)
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_DeleteGroupButton_i;True:C214)
Else 
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_EditGroupButton_i;False:C215)
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_DeleteGroupButton_i;False:C215)
End if 

Fnd_Gen_FormMethod 