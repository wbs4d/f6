  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_Editor.<>Fnd_SeqNo_AddGroupButton_i

  // Created by Dave Batton on Dec 8, 2003
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($groupName_t;$baseName_t)
C_LONGINT:C283($counter_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		  // Find/create a unique group name.
		$baseName_t:=Fnd_Gen_GetString ("Fnd_SqNo";"NewGroupDefault")
		$counter_i:=1
		$groupName_t:=$baseName_t
		While (Find in field:C653(<>Fnd_SqNo_GroupNameFld_ptr->;$groupName_t)#No current record:K29:2)
			$counter_i:=$counter_i+1
			$groupName_t:=$baseName_t+" "+String:C10($counter_i)
		End while 
		
		CREATE RECORD:C68(<>Fnd_SqNo_Table_ptr->)
		<>Fnd_SqNo_IDFld_ptr->:=Sequence number:C244(<>Fnd_SqNo_Table_ptr->)  // Ironic, isn't it?  :-)
		<>Fnd_SqNo_GroupNameFld_ptr->:=$groupName_t
		<>Fnd_SqNo_DesignerOnlyFld_ptr->:=True:C214
		<>Fnd_SqNo_NextNumberFld_ptr->:=1  // Better than zero.
		SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)
		
		READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)
		
		  // Add the item to the top of the list.
		APPEND TO LIST:C376(<>Fnd_SqNo_NamesList_i;<>Fnd_SqNo_GroupNameFld_ptr->;<>Fnd_SqNo_IDFld_ptr->)
		  //REDRAW LIST(<>Fnd_SqNo_NamesList_i)
		
		<>Fnd_SqNo_CurrentGroupID_i:=<>Fnd_SqNo_IDFld_ptr->
		Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
		Fnd_SqNo_EditGroup (<>Fnd_SqNo_CurrentGroupID_i)
		
		If (OK=1)
			SET LIST ITEM:C385(<>Fnd_SqNo_NamesList_i;<>Fnd_SqNo_CurrentGroupID_i;<>Fnd_SqNo_GroupNameFld_ptr->;<>Fnd_SqNo_CurrentGroupID_i)
			SELECT LIST ITEMS BY REFERENCE:C630(<>Fnd_SqNo_NamesList_i;<>Fnd_SqNo_CurrentGroupID_i)
			
		Else 
			  // Cancel was pressed, so we need to delete the record we created above.
			DELETE RECORD:C58(<>Fnd_SqNo_Table_ptr->)
			DELETE FROM LIST:C624(<>Fnd_SqNo_NamesList_i;<>Fnd_SqNo_CurrentGroupID_i)
		End if 
		
		  //REDRAW LIST(<>Fnd_SqNo_NamesList_i)
		UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure we don't leave any records locked.
End case 
