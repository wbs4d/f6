  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_Editor.<>Fnd_SqNo_DeleteGroupButton_i

  // Created by Dave Batton on Nov 29, 2003
  // Modified by Dave Batton on Jan 15, 2005
  //   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition when displaying alerts and confirms.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Sep 23, 2005
  //   Restored the confirm dialog message that seems to have disappeared after the previous update.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($selectedItem_i)
C_TEXT:C284($message_t)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		$selectedItem_i:=Selected list items:C379(<>Fnd_SqNo_NamesList_i)
		
		If ($selectedItem_i>0)
			GET LIST ITEM:C378(<>Fnd_SqNo_NamesList_i;$selectedItem_i;<>Fnd_SqNo_CurrentGroupID_i;<>Fnd_SqNo_CurrentGroupName_t)
			
			$message_t:=Fnd_Gen_GetString ("Fnd_SqNo";"DeleteSeqNoGroup";<>Fnd_SqNo_CurrentGroupName_t)
			
			Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
			Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
			Fnd_Dlg_SetButtons (Fnd_Gen_GetString ("Fnd_SqNo";"DeleteButton");"*")
			Fnd_Wnd_Title (Fnd_Gen_GetString ("Fnd_SqNo";"WindowTitle"))  // DB050528 - Changed from Fnd_Wnd_SetTitle.
			Fnd_Gen_GetString ("Fnd_SqNo";"DeleteSeqNoGroup";<>Fnd_SqNo_CurrentGroupName_t)
			Fnd_Dlg_Confirm ($message_t)
			
			If (OK=1)
				READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)
				QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_IDFld_ptr->=<>Fnd_SqNo_CurrentGroupID_i)
				
				If (Locked:C147(<>Fnd_SqNo_Table_ptr->))
					Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
					Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
					Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"DeleteRecordLocked"))
					
				Else 
					DELETE RECORD:C58(<>Fnd_SqNo_Table_ptr->)
					DELETE FROM LIST:C624(<>Fnd_SqNo_NamesList_i;*;*)
					<>Fnd_SqNo_CurrentGroupID_i:=0
					SELECT LIST ITEMS BY POSITION:C381(<>Fnd_SqNo_NamesList_i;0)
					  //REDRAW LIST(<>Fnd_SqNo_NamesList_i)
				End if 
			End if 
			
		Else 
			Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
			Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
			Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"DeleteRecordSelect"))
		End if 
End case 
