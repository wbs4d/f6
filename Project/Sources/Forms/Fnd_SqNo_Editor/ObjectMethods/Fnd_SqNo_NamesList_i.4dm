  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_Editor.<>Fnd_SqNo_NamesList_i

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($selectedItem_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		$selectedItem_i:=Selected list items:C379(<>Fnd_SqNo_NamesList_i)
		If ($selectedItem_i>0)
			GET LIST ITEM:C378(<>Fnd_SqNo_NamesList_i;$selectedItem_i;<>Fnd_SqNo_CurrentGroupID_i;<>Fnd_SqNo_CurrentGroupName_t)
		End if 
		
	: (Form event:C388=On Double Clicked:K2:5)
		$selectedItem_i:=Selected list items:C379(<>Fnd_SqNo_NamesList_i)
		If ($selectedItem_i>0)
			GET LIST ITEM:C378(<>Fnd_SqNo_NamesList_i;$selectedItem_i;<>Fnd_SqNo_CurrentGroupID_i;<>Fnd_SqNo_CurrentGroupName_t)
			  //READ WRITE(<>Fnd_SqNo_Table_ptr->)
			Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
			Fnd_SqNo_EditGroup (<>Fnd_SqNo_CurrentGroupID_i)
			If (OK=1)
				If (<>Fnd_SqNo_GroupNameFld_ptr->#"*Table #@")
					SET LIST ITEM:C385(<>Fnd_SqNo_NamesList_i;<>Fnd_SqNo_CurrentGroupID_i;<>Fnd_SqNo_GroupNameFld_ptr->;<>Fnd_SqNo_CurrentGroupID_i)
					  //REDRAW LIST(<>Fnd_SqNo_NamesList_i)
				End if 
			End if 
			UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure we don't leave any records locked.
		End if 
End case 