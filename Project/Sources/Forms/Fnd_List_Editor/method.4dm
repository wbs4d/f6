  // ----------------------------------------------------
  // Form Method: [Fnd_List].Fnd_List_Editor

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Dave Batton on Jun 2, 2004
  //   Localized the dialog.
  // Modified by Dave Batton on Aug 2, 2004
  //   The Fnd_Gen_ButtonText calls now moves the Delete and Sort buttons directly.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------


C_LONGINT:C283($itemRef_i)
C_TEXT:C284($itemText_t)

Case of 
	: (Form event code:C388=On Load:K2:1)
		<>Fnd_List_Label1_t:=Fnd_Gen_GetString ("Fnd_List";"ListsLabel")
		<>Fnd_List_Label2_t:=Fnd_Gen_GetString ("Fnd_List";"ListItemsLabel")
		Fnd_Gen_ButtonText ("Fnd_List_AddButton_i";Fnd_Gen_GetString ("Fnd_List";"AddButton");Align left:K42:2;"Fnd_List_DeleteButton_i";"Fnd_List_SortButton_i")
		Fnd_Gen_ButtonText ("Fnd_List_DeleteButton_i";Fnd_Gen_GetString ("Fnd_List";"DeleteButton");Align left:K42:2;"Fnd_List_SortButton_i")
		Fnd_Gen_ButtonText ("Fnd_List_SortButton_i";Fnd_Gen_GetString ("Fnd_List";"SortButton");Align left:K42:2)
		
		<>Fnd_List_ListItems_i:=New list:C375
		<>Fnd_List_ListItemsModified_b:=False:C215
		
		If (Count list items:C380(<>Fnd_List_ListOfLists_i)=0)
			<>Fnd_List_SelectedListName_t:=""
		Else 
			SELECT LIST ITEMS BY POSITION:C381(<>Fnd_List_ListOfLists_i;1)
			GET LIST ITEM:C378(<>Fnd_List_ListOfLists_i;1;$itemRef_i;$itemText_t)
			<>Fnd_List_SelectedListName_t:=$itemText_t
			Fnd_List_UpdateItemsList 
		End if 
		
End case 

If ((Form event code:C388=On Load:K2:1) | (Form event code:C388=On Clicked:K2:4))
	If (Selected list items:C379(<>Fnd_List_ListOfLists_i)>0)
		OBJECT SET ENABLED:C1123(*;"Fnd_List_AddButton_i";True:C214)
		If (Count list items:C380(<>Fnd_List_ListItems_i)>1)
			OBJECT SET ENABLED:C1123(*;"Fnd_List_SortButton_i";True:C214)
		Else 
			OBJECT SET ENABLED:C1123(*;"Fnd_List_SortButton_i";False:C215)
		End if 
		If (Selected list items:C379(<>Fnd_List_ListItems_i)>0)
			OBJECT SET ENABLED:C1123(*;"Fnd_List_DeleteButton_i";True:C214)
		Else 
			OBJECT SET ENABLED:C1123(*;"Fnd_List_DeleteButton_i";False:C215)
		End if 
		
	Else 
		OBJECT SET ENABLED:C1123(*;"Fnd_List_AddButton_i";False:C215)
		OBJECT SET ENABLED:C1123(*;"Fnd_List_DeleteButton_i";False:C215)
		OBJECT SET ENABLED:C1123(*;"Fnd_List_SortButton_i";False:C215)
	End if 
End if 

Fnd_Gen_FormMethod 
