  // ----------------------------------------------------
  // Object Method: [Fnd_List].Fnd_List_Editor.<>Fnd_List_ListItems_i

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($itemRef_i;$subList_i;$mouseX_i;$mouseY_i;$mouseButton_i;$dropPosition_i;$dragPosition_i)
C_TEXT:C284($itemText_t)
C_BOOLEAN:C305($expanded_b;$enterable_b)


Case of 
	: (Form event:C388=On Double Clicked:K2:5)
		EDIT ITEM:C870(Self:C308->)
		
		
	: (Form event:C388=On Drop:K2:12)
		$dropPosition_i:=Drop position:C608
		If ($dropPosition_i=-1)  // It was dropped below the last item.
			$dropPosition_i:=Count list items:C380(Self:C308->)+1
		End if 
		GET LIST ITEM:C378(Self:C308->;Selected list items:C379(Self:C308->);$itemRef_i;$itemText_t;$subList_i;$expanded_b)
		If ($itemRef_i#0)
			GET LIST ITEM PROPERTIES:C631(Self:C308->;$itemRef_i;$enterable_b)  // Only Design environment created items will have ref nos.
		Else 
			$enterable_b:=True:C214
		End if 
		$dragPosition_i:=Selected list items:C379(Self:C308->)
		DELETE FROM LIST:C624(Self:C308->;*)
		If ($dropPosition_i>$dragPosition_i)
			$dropPosition_i:=$dropPosition_i-1  // We need to account for the deleted item.
		End if 
		SELECT LIST ITEMS BY POSITION:C381(Self:C308->;$dropPosition_i)
		If ($dropPosition_i<=Count list items:C380(Self:C308->))
			INSERT IN LIST:C625(Self:C308->;*;$itemText_t;$itemRef_i;$subList_i;$expanded_b)
		Else 
			APPEND TO LIST:C376(Self:C308->;$itemText_t;$itemRef_i;$subList_i;$expanded_b)
		End if 
		If (($itemRef_i#0) & (Not:C34($enterable_b)))  // Restore this property.
			SET LIST ITEM PROPERTIES:C386(Self:C308->;0;$enterable_b;Plain:K14:1;0)
		End if 
		  //REDRAW LIST(Self->)
		<>Fnd_List_ListItemsModified_b:=True:C214
		
		
	: (Form event:C388=On Data Change:K2:15)
		<>Fnd_List_ListItemsModified_b:=True:C214
End case 