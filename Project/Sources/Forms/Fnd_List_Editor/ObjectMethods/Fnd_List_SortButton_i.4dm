  // ----------------------------------------------------
  // Object Method: [Fnd_List].Fnd_List_Editor.Fnd_List_SortButton_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		If (Macintosh option down:C545)
			SORT LIST:C391(<>Fnd_List_ListItems_i;<)
		Else 
			SORT LIST:C391(<>Fnd_List_ListItems_i;>)
		End if 
		
		  //REDRAW LIST(<>Fnd_List_ListItems_i)
		
		<>Fnd_List_ListItemsModified_b:=True:C214
End case 
