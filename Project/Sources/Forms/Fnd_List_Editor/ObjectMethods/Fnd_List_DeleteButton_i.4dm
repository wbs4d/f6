  // ----------------------------------------------------
  // Object Method: [Fnd_List].Fnd_List_Editor.Fnd_List_DeleteButton_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by: Walt Nelson (3/10/10) to fix delete button
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($itemRef_i)
C_TEXT:C284($item_t)
C_BOOLEAN:C305($enterable_b)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		GET LIST ITEM:C378(<>Fnd_List_ListItems_i;Selected list items:C379(<>Fnd_List_ListItems_i);$itemRef_i;$item_t)
		GET LIST ITEM PROPERTIES:C631(<>Fnd_List_ListItems_i;$itemRef_i;$enterable_b)
		
		  //If ($enterable_b)
		  // Modified by: Walt Nelson (3/10/10) $enterable_b always returns false and $itemRef_i returns 0 - why?
		DELETE FROM LIST:C624(<>Fnd_List_ListItems_i;*;*)
		If (Selected list items:C379(<>Fnd_List_ListItems_i)>Count list items:C380(<>Fnd_List_ListItems_i))
			SELECT LIST ITEMS BY POSITION:C381(<>Fnd_List_ListItems_i;0)
		End if 
		  //REDRAW LIST(<>Fnd_List_ListItems_i)
		<>Fnd_List_ListItemsModified_b:=True:C214
		  //End if 
End case 
