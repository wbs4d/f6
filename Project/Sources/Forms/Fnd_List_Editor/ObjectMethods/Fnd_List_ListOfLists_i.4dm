  // ----------------------------------------------------

  // Object Method: [Fnd_List].Fnd_List_Editor.<>Fnd_List_ListOfLists_i


  // Created by Dave Batton on Nov 27, 2003

  // ----------------------------------------------------


C_LONGINT:C283($itemRef_i)
C_TEXT:C284($listName_t)
C_BOOLEAN:C305($enterable_b)

Case of 
	: (Form event:C388=On Clicked:K2:4)  // A different list item was selected.

		GET LIST ITEM:C378(Self:C308->;Selected list items:C379(Self:C308->);$itemRef_i;$listName_t)
		GET LIST ITEM PROPERTIES:C631(Self:C308->;$itemRef_i;$enterable_b)
		
		If ($listName_t#<>Fnd_List_SelectedListName_t)  // A different list name was selected.

			Fnd_List_SaveListItems 
			
			  // Repopulate the Items list.

			<>Fnd_List_SelectedListName_t:=$listName_t
			Fnd_List_UpdateItemsList 
		End if 
End case 

