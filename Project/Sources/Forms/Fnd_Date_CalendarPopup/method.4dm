  // ----------------------------------------------------

  // Form Method: [Fnd_Date].Fnd_Date_CalendarPopup


  // Created by Tom Dillon

  // ----------------------------------------------------


C_POINTER:C301($label_ptr)
C_LONGINT:C283($dow_i)

Case of 
	: (Form event code:C388=On Load:K2:1)
		If (<>Fnd_Date_CalendarDateField_ptr->=!00-00-00!)
			<>Fnd_Date_CalendarDate_d:=Current date:C33
		Else 
			<>Fnd_Date_CalendarDate_d:=<>Fnd_Date_CalendarDateField_ptr->
		End if 
		Fnd_Date_CalendarSetDate ("date";"var")
		
		If (Is Windows:C1573)
			OBJECT SET RGB COLORS:C628(*;"Fnd_Date_CalendarDOW@";0x00FFFFFF;Background color:K23:2)
			OBJECT SET VISIBLE:C603(*;"@Mac";False:C215)
		Else 
			OBJECT SET VISIBLE:C603(*;"@Win";False:C215)
		End if 
		
		<>Fnd_Date_CalendarDOW1_t:=Substring:C12(Fnd_Gen_GetString ("Fnd_Date";("ShortDOW07"));1;1)
		For ($dow_i;Monday:K10:13;Saturday:K10:18)
			$label_ptr:=Get pointer:C304("<>Fnd_Date_CalendarDOW"+String:C10($dow_i)+"_t")
			$label_ptr->:=Substring:C12(Fnd_Gen_GetString ("Fnd_Date";("ShortDOW0"+String:C10($dow_i-1)));1;1)
		End for 
		
		
	: (Form event code:C388=On Timer:K2:25)
		ACCEPT:C269
		
	: (Form event code:C388=On Close Box:K2:21)
		  // If we had one, which we don't in this implementation.
		
		CANCEL:C270
		
	: (Form event code:C388=On Outside Call:K2:11)
		  // Doesn't currently apply, since we're using a floating palette window.
		
		CANCEL:C270
		
	: (Form event code:C388=On Outside Call:K2:11)
		  // Our event handler calls us if it's time to close.
		
		CANCEL:C270
End case 
