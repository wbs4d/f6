  // ----------------------------------------------------

  // Object Method: [Fnd_Date].Fnd_Date_CalendarPopup.<>Fnd_Date_CalendarPrevButton_i


  // Created by Dave Batton on Mar 27, 2005

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		If (Macintosh option down:C545)
			Fnd_Date_CalendarSetDate ("year";"-1")
		Else 
			Fnd_Date_CalendarSetDate ("month";"-1")
		End if 
End case 
