// ----------------------------------------------------
// Form Method: Fnd_Nav_Palette

// Created by Dave Batton on Sep 8, 2003
// Modified by Dave Batton on Jul 1, 2004
//   Added the On Outside Call test to check for updates.
//   Added support for colors.
// Mod by Wayne Stewart, (2021-04-05) - Added the resizing code to the On Load as well
// ----------------------------------------------------

C_LONGINT:C283($left_i; $top_i; $right_i; $bottom_i; $newWidth_i; $newHeight_i)

Case of 
	: (Form event code:C388=On Load:K2:1)
		
		If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 600)))  // Wait up to 10 seconds.
			Fnd_Nav_UpdateColors
			CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
		End if 
		
		CALL FORM:C1391(Storage:C1525.Fnd.Nav.WindowReference; "Fnd_Nav_Redraw")  // Force a redraw after opening window
		
	: (Form event code:C388=On Close Box:K2:21)
		CANCEL:C270
		
		
	: (Fnd_Gen_QuitNow)
		CANCEL:C270
		
		
	: (Form event code:C388=On Outside Call:K2:11)
		// Give a chance for any other palette commands to get called.
		DELAY PROCESS:C323(Current process:C322; 20)
		
		If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 900)))  // Wait up to 15 seconds.
			If (<>Fnd_Nav_NeedsRedraw_b)
				GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i)
				$newWidth_i:=Fnd_Nav_WindowWidth
				$newHeight_i:=Fnd_Nav_WindowHeight
				
				If ($newHeight_i=0)  // If there are no buttons, close the window.
					CANCEL:C270
					
				Else   // Otherwise, resize it.
					// If it's anchored to the right side of the screen, keep it that way.
					If ($right_i=Screen width:C187)
						$left_i:=Screen width:C187-$newWidth_i
						
					Else 
						// Make sure we don't resize it so it's partially offscreen.
						$right_i:=$left_i+$newWidth_i
						If ($right_i>Screen width:C187)
							$left_i:=Screen width:C187-$newWidth_i
							$right_i:=$left_i+$newWidth_i
						End if 
					End if 
					
					$bottom_i:=$top_i+$newHeight_i
					If ($bottom_i>Screen height:C188)
						$top_i:=Screen height:C188-$newHeight_i
						$bottom_i:=$top_i+$newHeight_i
					End if 
					
					SET WINDOW RECT:C444($left_i; $top_i; $right_i; $bottom_i)
				End if 
				
				Fnd_Nav_UpdateColors
				
				<>Fnd_Nav_NeedsRedraw_b:=False:C215
			End if 
			
			CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
		End if 
End case 