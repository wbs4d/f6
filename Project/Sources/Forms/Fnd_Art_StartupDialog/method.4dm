// ----------------------------------------------------
// Form Method: [Fnd_Art].Fnd_Art_StartupDialog

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on May 13, 2004
//   No longer puts the word "Version" before the version number.
// Modified by Dave Batton on Jan 15, 2005
//   Now truncates the copyright date at the first carriage return.
// ----------------------------------------------------

C_LONGINT:C283($position_i)
C_TEXT:C284($Copyright_t)
C_POINTER:C301($Object_ptr)

Case of 
	: (Form event code:C388=On Load:K2:1)
		If (Is Windows:C1573)
			OBJECT SET VISIBLE:C603(*; "Fnd_Art_Mac@"; False:C215)
		Else 
			OBJECT SET VISIBLE:C603(*; "Fnd_Art_Win@"; False:C215)
		End if 
		
		Form:C1466.DatabaseName:=Fnd_Gen_GetDatabaseInfo("DatabaseName")
		Form:C1466.DatabaseVersion:=Fnd_Gen_GetDatabaseInfo("DatabaseVersion")
		
		$Copyright_t:=Fnd_Gen_GetDatabaseInfo("DatabaseCopyright")
		
		$position_i:=Position:C15("\r"; $Copyright_t)
		If ($position_i>0)
			$Copyright_t:=Substring:C12($Copyright_t; 1; $position_i)
		End if 
		Form:C1466.DatabaseCopyright:=$Copyright_t
		
		
		Fnd_Art_SetBestFontSize("Fnd_Art_DatabaseName_t"; 24)
End case 

Fnd_Art_StartupDialogFormMethod