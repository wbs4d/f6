  // ----------------------------------------------------

  // Object Method: <>Fnd_Pswd_MinLengthPopup_ai


  // Created by Dave Batton on Mar 16, 2004

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		<>Fnd_Pswd_MinLength_i:=Self:C308->{Self:C308->}
		
		If (<>Fnd_Pswd_MaxLengthPopup_ai{<>Fnd_Pswd_MaxLengthPopup_ai}<<>Fnd_Pswd_MinLength_i)
			<>Fnd_Pswd_MaxLengthPopup_ai:=Abs:C99(Find in array:C230(<>Fnd_Pswd_MaxLengthPopup_ai;<>Fnd_Pswd_MinLength_i))
			<>Fnd_Pswd_MaxLength_i:=<>Fnd_Pswd_MaxLengthPopup_ai{<>Fnd_Pswd_MaxLengthPopup_ai}
		End if 
		
		<>Fnd_Pswd_GeneratedPassword_t:=Fnd_Pswd_GeneratePassword 
End case 
