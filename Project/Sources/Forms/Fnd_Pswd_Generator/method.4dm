  // ----------------------------------------------------
  // Form Method: Fnd_Pswd_Generator

  // Created by Dave Batton on Mar 14, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------


C_LONGINT:C283($element_i;$move_i;$left_i;$top_i;$right_i;$bottom_i)

Case of 
	: (Form event:C388=On Load:K2:1)
		  // Localize the labels for the Characters Allowed group.
		
		<>Fnd_Pswd_Label1_t:=Fnd_Gen_GetString ("Fnd_Pswd";"CharsAllowedGroupLabel")
		OBJECT SET TITLE:C194(<>Fnd_Pswd_UppercaseCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"UppercaseLetters"))
		OBJECT SET TITLE:C194(<>Fnd_Pswd_LowercaseCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"LowercaseLetters"))
		OBJECT SET TITLE:C194(<>Fnd_Pswd_NumbersCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"Numbers"))
		OBJECT SET TITLE:C194(<>Fnd_Pswd_SymbolsCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"Symbols"))
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_Pswd_CustomCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"Custom");Align left:K42:2;-><>Fnd_Pswd_CancelButton_i)
		OBJECT GET COORDINATES:C663(<>Fnd_Pswd_CustomCharacters_t;$left_i;$top_i;$right_i;$bottom_i)
		$left_i:=$left_i+$move_i
		OBJECT MOVE:C664(<>Fnd_Pswd_CustomCharacters_t;$left_i;$top_i;$right_i;$bottom_i;*)
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_Pswd_ExcludeCheckbox_i;Fnd_Gen_GetString ("Fnd_Pswd";"Exclude");Align left:K42:2;-><>Fnd_Pswd_CancelButton_i)
		OBJECT GET COORDINATES:C663(<>Fnd_Pswd_ExcludedCharacters_t;$left_i;$top_i;$right_i;$bottom_i)
		$left_i:=$left_i+$move_i
		OBJECT MOVE:C664(<>Fnd_Pswd_ExcludedCharacters_t;$left_i;$top_i;$right_i;$bottom_i;*)
		
		  // Localize the labels for the Password Length group
		
		<>Fnd_Pswd_Label2_t:=Fnd_Gen_GetString ("Fnd_Pswd";"PasswordLengthGroupLabel")
		<>Fnd_Pswd_Label3_t:=Fnd_Gen_GetString ("Fnd_Pswd";"MinimumChars")
		<>Fnd_Pswd_Label4_t:=Fnd_Gen_GetString ("Fnd_Pswd";"MaximumChars")
		
		  // Localize the Generate button and the field next to it.
		
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_Pswd_GenerateButton_i;Fnd_Gen_GetString ("Fnd_Pswd";"GenerateButton");Align right:K42:4)
		OBJECT GET COORDINATES:C663(<>Fnd_Pswd_GeneratedPassword_t;$left_i;$top_i;$right_i;$bottom_i)
		$right_i:=$right_i+$move_i
		OBJECT MOVE:C664(<>Fnd_Pswd_GeneratedPassword_t;$left_i;$top_i;$right_i;$bottom_i;*)
		
		  // Localize the OK and Cancel button.
		
		$move_i:=Fnd_Gen_ButtonText (-><>Fnd_Pswd_OKButton_i;Fnd_Gen_GetString ("Fnd_Pswd";"OKButton");Align right:K42:4)
		OBJECT MOVE:C664(<>Fnd_Pswd_CancelButton_i;$move_i;0)
		Fnd_Gen_ButtonText (-><>Fnd_Pswd_CancelButton_i;Fnd_Gen_GetString ("Fnd_Pswd";"CancelButton");Align right:K42:4)
		
		  // Set the checkboxes.
		
		<>Fnd_Pswd_UppercaseCheckbox_i:=Num:C11(<>Fnd_Pswd_UseUppercase_b)
		<>Fnd_Pswd_LowercaseCheckbox_i:=Num:C11(<>Fnd_Pswd_UseLowercase_b)
		<>Fnd_Pswd_NumbersCheckbox_i:=Num:C11(<>Fnd_Pswd_UseNumbers_b)
		<>Fnd_Pswd_SymbolsCheckbox_i:=Num:C11(<>Fnd_Pswd_UseSymbols_b)
		<>Fnd_Pswd_CustomCheckbox_i:=Num:C11(<>Fnd_Pswd_UseCustom_b)
		<>Fnd_Pswd_CustomCharacters_t:=<>Fnd_Pswd_CustomCharacters_t
		<>Fnd_Pswd_ExcludeCheckbox_i:=Num:C11(<>Fnd_Pswd_UseExcluded_b)
		<>Fnd_Pswd_ExcludedCharacters_t:=<>Fnd_Pswd_ExcludedCharacters_t
		
		  // Initialize the pop-up menus.
		
		ARRAY INTEGER:C220(<>Fnd_Pswd_MinLengthPopup_ai;<>Fnd_Pswd_MaxLengthLimit_i-<>Fnd_Pswd_MinLengthLimit_i+1)
		ARRAY INTEGER:C220(<>Fnd_Pswd_MaxLengthPopup_ai;<>Fnd_Pswd_MaxLengthLimit_i-<>Fnd_Pswd_MinLengthLimit_i+1)
		For ($element_i;1;Size of array:C274(<>Fnd_Pswd_MinLengthPopup_ai))
			<>Fnd_Pswd_MinLengthPopup_ai{$element_i}:=<>Fnd_Pswd_MinLengthLimit_i-1+$element_i
			<>Fnd_Pswd_MaxLengthPopup_ai{$element_i}:=<>Fnd_Pswd_MinLengthLimit_i-1+$element_i
		End for 
		<>Fnd_Pswd_MinLengthPopup_ai:=Abs:C99(Find in array:C230(<>Fnd_Pswd_MinLengthPopup_ai;<>Fnd_Pswd_MinLength_i))
		<>Fnd_Pswd_MaxLengthPopup_ai:=Abs:C99(Find in array:C230(<>Fnd_Pswd_MaxLengthPopup_ai;<>Fnd_Pswd_MaxLength_i))
		
		<>Fnd_Pswd_GeneratedPassword_t:=Fnd_Pswd_GeneratePassword 
End case 

OBJECT SET ENTERABLE:C238(<>Fnd_Pswd_CustomCharacters_t;(<>Fnd_Pswd_CustomCheckbox_i=1))
OBJECT SET ENTERABLE:C238(<>Fnd_Pswd_ExcludedCharacters_t;(<>Fnd_Pswd_ExcludeCheckbox_i=1))

  // Disable the Generate button if no characters are allowed.

If (Fnd_Pswd_GetValidCharacters ="")
	OBJECT SET ENABLED:C1123(<>Fnd_Pswd_GenerateButton_i;False:C215)
Else 
	OBJECT SET ENABLED:C1123(<>Fnd_Pswd_GenerateButton_i;True:C214)
End if 
