  // ----------------------------------------------------
  // Form Method: Fnd_Rec_ModFormula

  // Created by Dave Batton on Dec 8, 2005
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Load:K2:1)
		Fnd_Rec_Label_t:=Fnd_Gen_GetString ("Fnd_Rec";"ModFormulaTitle";String:C10(Records in selection:C76(Fnd_Rec_Table_ptr->));Fnd_Rec_TableName_t)
		
		  // Localize the OK and Cancel buttons.
		Fnd_Gen_ButtonText (->Fnd_Rec_OKButton_i;Fnd_Gen_GetString ("Fnd_Rec";"ModOKButton");Align right:K42:4;->Fnd_Rec_CancelButton_i)
		Fnd_Gen_ButtonText (->Fnd_Rec_CancelButton_i;Fnd_Gen_GetString ("Fnd_Rec";"ModCancelButton");Align right:K42:4)
End case 
