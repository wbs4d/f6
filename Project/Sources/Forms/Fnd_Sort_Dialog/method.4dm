// ----------------------------------------------------
// Form Method: [Fnd_Sort].Fnd_Sort_Dialog

// Created by Dave Batton on Dec 23, 2003
// Modified by Dave Batton on May 21, 2004
//   Now sets the default field based on the value of Fnd_Sort_SelectedField_ptr
//   and sets the sort direction based on the Fnd_Sort_SelectedDirection_i value.
//   Localized the form objects.
// Modified by Dave Batton on Aug 2, 2004
//   Now lets the Fnd_Gen_ButtonText handle moving the Cancel button.
// Modified by Dave Batton on Nov 8, 2004
//   Uses the sort field stored to the user's preferences file if another field wasn't specified.
//   The Fnd_VS component is now optional.
// Modified by Dave Batton on Nov 22, 2004
//   The Fnd_Loc component is no longer required.
// Modified by Dave Batton on Feb 18, 2006
//   Removed an extra unused parameter.
// Mod by Wayne Stewart, (2021-04-13) - Removed call to Fnd_Gen_ComponentAvailable("Fnd_Loc")
// ----------------------------------------------------

C_TEXT:C284($sortFieldName_t)

Case of 
	: (Form event code:C388=On Load:K2:1)
		Fnd_Sort_Label_t:=Fnd_Gen_GetString("Fnd_Sort"; "SortDialogLabel"; Fnd_Sort_TableName_t)
		
		Fnd_Loc_FixLabelWidths(->Fnd_Sort_Label_t; ->Fnd_Sort_FieldNames_at)
		
		Fnd_Gen_ButtonText(->Fnd_Sort_AscendingButton_i; Fnd_Gen_GetString("Fnd_Sort"; "AscendingButton"); Align center:K42:3)
		Fnd_Gen_ButtonText(->Fnd_Sort_descendingButton_i; Fnd_Gen_GetString("Fnd_Sort"; "DescendingButton"); Align center:K42:3)
		
		Fnd_Gen_ButtonText(->Fnd_Sort_OKButton_i; Fnd_Gen_GetString("Fnd_Sort"; "SortButton"); Align right:K42:4; ->Fnd_Sort_CancelButton_i)  // DB040802
		Fnd_Gen_ButtonText(->Fnd_Sort_CancelButton_i; Fnd_Gen_GetString("Fnd_Sort"; "CancelButton"); Align right:K42:4)
		
		//DB041108 -  If no sort field has been specified, try getting it from the user preferences file.
		If (Is nil pointer:C315(Fnd_Sort_SelectedField_ptr))
			$sortFieldName_t:=Fnd_Sort_PrefsGet("Fnd_Sort: "+Fnd_Sort_TableName_t+" Sort Field")
			Fnd_Sort_FieldNames_at:=Abs:C99(Find in array:C230(Fnd_Sort_FieldNames_at; $sortFieldName_t))  // DB041108
		Else 
			Fnd_Sort_FieldNames_at:=Abs:C99(Find in array:C230(Fnd_Sort_Fields_aptr; Fnd_Sort_SelectedField_ptr))  // DB040521
		End if 
		
		// DB041108 - If no direction has been specified, try getting it from the user prefernces file.
		If (Fnd_Sort_Direction_i=0)
			If (Fnd_Sort_PrefsGet("Fnd_Sort: "+Fnd_Sort_TableName_t+" Sort Order")="Descending")  // DB060218
				Fnd_Sort_Direction_i:=-1
			Else 
				Fnd_Sort_Direction_i:=1
			End if 
		End if 
		
		Fnd_Sort_AscendingButton_i:=Num:C11(Fnd_Sort_Direction_i=1)  // DB040521
		Fnd_Sort_DescendingButton_i:=Num:C11(Fnd_Sort_Direction_i=-1)  // DB040521
End case 
