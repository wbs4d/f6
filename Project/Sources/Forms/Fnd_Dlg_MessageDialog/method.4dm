  // ----------------------------------------------------
  // Form Method: [Fnd_Dlg].Fnd_Dlg_MessageDialog

  // Created by Dave Batton on Jul 25, 2003
  // Modified by Dave Batton on Mar 21, 2005
  //   Now calls the Fnd_Gen_QuitNow function to see if we're trying to quit 4D.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

Case of 
	: ((<>Fnd_Dlg_MessageClose_b) | (Fnd_Gen_QuitNow ))  // DB050321
		CANCEL:C270
		
		
	: (Form event:C388=On Load:K2:1)
		SET WINDOW TITLE:C213(<>Fnd_Dlg_WindowTitle_t)
		
		
		  // Hide the progress indicator if it's not being used.
		If ((<>Fnd_Dlg_ProgFrom_i=0) & (<>Fnd_Dlg_ProgTo_i=0))
			OBJECT SET VISIBLE:C603(<>Fnd_Dlg_ProgThermo_i;False:C215)
			OBJECT MOVE:C664(Fnd_Dlg_CancelButton_i;0;<>Fnd_Dlg_ProgBarHeightAdjust_i)  // This number is also used in the Fnd_Dlg_MessageOpen2 method.
		Else 
			  // Move the progress bar down if we have more than one line of text.  See Fnd_Dlg_MessageOpen2.
			OBJECT MOVE:C664(<>Fnd_Dlg_ProgThermo_i;0;<>Fnd_Dlg_TextHeightAdjust_i)
		End if 
		
		
		If (<>Fnd_Dlg_CancelButton_t="")
			OBJECT SET VISIBLE:C603(Fnd_Dlg_CancelButton_i;False:C215)
			OBJECT SET ENABLED:C1123(Fnd_Dlg_EscapeButton_i;False:C215)
		Else 
			Case of 
				: (<>Fnd_Dlg_CancelButton_t="*")
					<>Fnd_Dlg_CancelButton_t:=Fnd_Gen_GetString ("Fnd_Gen";"Stop")
				: (<>Fnd_Dlg_CancelButton_t="**")
					<>Fnd_Dlg_CancelButton_t:=Fnd_Gen_GetString ("Fnd_Gen";"Cancel")
			End case 
			
			  // Move the button down if we have more than one line of text.  See Fnd_Dlg_MessageOpen2.
			OBJECT MOVE:C664(Fnd_Dlg_CancelButton_i;0;<>Fnd_Dlg_TextHeightAdjust_i)
			Fnd_Gen_ButtonText (->Fnd_Dlg_CancelButton_i;<>Fnd_Dlg_CancelButton_t;Align center:K42:3)
			
			  // Disable the Cancel button to indicate the process can't be stopped.
			If (Not:C34(<>Fnd_Dlg_CanBeCancelled_b))
				OBJECT SET ENABLED:C1123(Fnd_Dlg_CancelButton_i;False:C215)
				OBJECT SET ENABLED:C1123(Fnd_Dlg_EscapeButton_i;False:C215)
			End if 
		End if 
		
		
		  // Load a menu bar if the user specified one.
		If (<>Fnd_Dlg_MenuBarName_t#"")
			SET MENU BAR:C67(<>Fnd_Dlg_MenuBarName_t)
		End if 
		
		
	: (Form event:C388=On Outside Call:K2:11)
		<>Fnd_Dlg_ProgThermo_i:=<>Fnd_Dlg_ProgPercent_r*300  // The indicator is 300 pixels wide.
End case 