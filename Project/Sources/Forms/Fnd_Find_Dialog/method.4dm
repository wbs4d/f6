  // ----------------------------------------------------
  // Form Method: [Fnd_Find].Fnd_Find_Dialog

  // Created by Dave Batton on Jul 14, 2003
  // Modified by Dave Batton on May 30, 2004
  //   Changed some Fnd_Loc calls to Fnd_Gen calls so the Localization
  //   component is now optional.
  // Modified by Dave Batton on Aug 2, 2004
  //   The Fnd_Gen_ButtonText calls now moves the Cancel button directly.
  // Modified by Dave Batton on Nov 6, 2004
  //   Now tries to use the last operator value stored in the prefs file.
  // Modified by Dave Batton on Feb 27, 2005
  //   Removed the call to Fnd_Find_ResizeWindow.
  //   Changed the Fnd_Gen_SizeArrayObject calls to Fnd_Find_SizeArrayObject calls.
  // Modified by Dave Batton on May 13, 2005
  //   Some of the MOVE OBJECT calls are no longer called when using 4D 2004.1.
  // Modified by Dave Batton on Jun 22, 2005
  //   Moved the initialization of the Fnd_Find_SearchString_t here.
  // Modified by Dave Batton on Jul 9, 2005
  //   More tweaking to get the resized objects to play well with 4D 2004.x.
  // Modified by Dave Batton on Oct 4, 2005
  //   More tweaking to get the resized objects to play well with 4D 2004.x.
  // Modified by Dave Batton on May 17, 2006
  //   Fixed the OK and Cancel buttons so they resize properly when localized.
  // Modified by Dave Batton on Feb 19, 2007
  //   Fixed a bug that caused the Find button to be sized incorrectly.
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($fieldType_i;$move_i;$move1_i;$move2_i;$left_i;$top_i;$right_i;$bottom_i;$element_t)
C_LONGINT:C283($width_i;$height_i;$winLeft_i;$winTop_i;$winRight_i;$winBottom_i;$buttonWidth_i;$okButtonLeft_i)

Case of 
	: (Form event:C388=On Load:K2:1)
		$move1_i:=Fnd_Find_SizeArrayObject (->Fnd_Find_FieldNames_at;Align left:K42:2)
		OBJECT MOVE:C664(Fnd_Find_OperatorStrs_at;$move1_i;0)
		
		$move2_i:=Fnd_Find_SizeArrayObject (->Fnd_Find_OperatorStrs_at;Align left:K42:2)
		OBJECT MOVE:C664(Fnd_Find_SearchString_t;$move1_i+$move2_i;0)
		
		$move_i:=$move1_i+$move2_i
		
		If ($move_i>0)
			OBJECT MOVE:C664(*;"@RadioButton_i";($move_i\2);0)
			  // DB050227 - Removed the call to Fnd_Find_ResizeWindow.
			  // DB050709 - Moved the code below out of this If statement.
		End if 
		
		Fnd_Find_SearchString_t:=Fnd_Find_PrefsGet ("Fnd_Find: "+Fnd_Find_TableName_t+" Search Value")  // DB050622
		
		  // ### - Still need to check for different field type and separator.
		If ((Fnd_Find_FieldNames_at=0) | (Fnd_Find_FieldNames_at>Size of array:C274(Fnd_Find_FieldNames_at)))
			Fnd_Find_FieldNames_at:=1
		End if 
		Fnd_Find_UpdateOperators 
		$element_t:=Find in array:C230(Fnd_Find_OperatorStrs_at;Fnd_Find_PrefsGet ("Fnd_Find: "+Fnd_Find_TableName_t+" Search Operator"))
		If ($element_t>0)
			Fnd_Find_OperatorStrs_at:=$element_t
		End if 
		
		Fnd_Find_AllRadioButton_i:=1  // Always default to this.
		
		  // Localize and resize the radio buttons on the left.
		$move1_i:=Fnd_Gen_ButtonText (->Fnd_Find_AllRadioButton_i;Fnd_Gen_GetString ("Fnd_Find";"SearchAll");Align left:K42:2)
		$move2_i:=Fnd_Gen_ButtonText (->Fnd_Find_AddRadioButton_i;Fnd_Gen_GetString ("Fnd_Find";"AddToSelection");Align left:K42:2)
		  // Move the buttons on the right so they make room for the ones on the left.
		If ($move1_i>$move2_i)
			$move_i:=$move1_i
		Else 
			$move_i:=$move2_i
		End if 
		OBJECT MOVE:C664(Fnd_Find_SelectionRadioButton_i;$move_i;0)
		OBJECT MOVE:C664(Fnd_Find_OmitRadioButton_i;$move_i;0)
		  // Localize and resize the radio buttons on the right.
		$move1_i:=Fnd_Gen_ButtonText (->Fnd_Find_SelectionRadioButton_i;Fnd_Gen_GetString ("Fnd_Find";"SearchSelection");Align left:K42:2)
		$move2_i:=Fnd_Gen_ButtonText (->Fnd_Find_OmitRadioButton_i;Fnd_Gen_GetString ("Fnd_Find";"OmitFromSelection");Align left:K42:2)
		  // Now move all of the buttons so they're centered again.
		If ($move2_i>$move1_i)
			$move1_i:=$move2_i
		End if 
		$move_i:=($move_i+$move1_i)/2
		OBJECT MOVE:C664(Fnd_Find_AllRadioButton_i;-$move_i;0)
		OBJECT MOVE:C664(Fnd_Find_AddRadioButton_i;-$move_i;0)
		OBJECT MOVE:C664(Fnd_Find_SelectionRadioButton_i;-$move_i;0)
		OBJECT MOVE:C664(Fnd_Find_OmitRadioButton_i;-$move_i;0)
		
		  // Localize the buttons at the bottom of the dialog.
		Fnd_Gen_ButtonText (->Fnd_Find_OKButton_i;Fnd_Gen_GetString ("Fnd_Find";"FindButton");Align right:K42:4;->Fnd_Find_CancelButton_i)  // DB040802
		Fnd_Gen_ButtonText (->Fnd_Find_CancelButton_i;Fnd_Gen_GetString ("Fnd_Find";"CancelButton");Align right:K42:4)
		
		  // Set or hide the Query Editor button.
		If (Fnd_Find_EditorButtonLabel_t="")
			OBJECT SET VISIBLE:C603(Fnd_Find_EditorButton_i;False:C215)
		Else 
			Fnd_Gen_ButtonText (->Fnd_Find_EditorButton_i;Fnd_Find_EditorButtonLabel_t;Align left:K42:2)
		End if 
		
		  // If there are no records in the current selection, only the first option
		  //   makes sense.
		If (Records in selection:C76(Fnd_Find_Table_ptr->)=0)
			OBJECT SET ENABLED:C1123(Fnd_Find_SelectionRadioButton_i;False:C215)
			OBJECT SET ENABLED:C1123(Fnd_Find_AddRadioButton_i;False:C215)
			OBJECT SET ENABLED:C1123(Fnd_Find_OmitRadioButton_i;False:C215)
		End if 
		
		GET FIELD PROPERTIES:C258(Fnd_Find_Fields_aptr{Fnd_Find_FieldNames_at};$fieldType_i)
		
		If ($fieldType_i=Is boolean:K8:9)
			Fnd_Find_SearchString_t:=""
			OBJECT SET VISIBLE:C603(Fnd_Find_SearchString_t;False:C215)  //no need to enter value for Boolean
		Else 
			OBJECT SET VISIBLE:C603(Fnd_Find_SearchString_t;True:C214)
			GOTO OBJECT:C206(Fnd_Find_SearchString_t)
		End if 
End case 

  // This part we need to do in the On Load form event in 4D 2003, but in the On Resize event in 4D 2004.
If (((Application version:C493<"0800") & (Form event:C388=On Load:K2:1)) | ((Application version:C493>="0800") & (Form event:C388=On Resize:K2:27)))
	GET WINDOW RECT:C443($winLeft_i;$winTop_i;$winRight_i;$winBottom_i)
	$width_i:=$winRight_i-$winLeft_i
	$height_i:=$winBottom_i-$winTop_i
	
	OBJECT GET COORDINATES:C663(Fnd_Find_SearchString_t;$left_i;$top_i;$right_i;$bottom_i)
	$right_i:=$width_i-22
	OBJECT MOVE:C664(Fnd_Find_SearchString_t;$left_i;$top_i;$right_i;$bottom_i;*)
	
	OBJECT GET COORDINATES:C663(*;"Fnd_Find_Line1";$left_i;$top_i;$right_i;$bottom_i)
	$right_i:=$width_i-20
	OBJECT MOVE:C664(*;"Fnd_Find_Line1";$left_i;$top_i;$right_i;$bottom_i;*)
	
	OBJECT GET COORDINATES:C663(*;"Fnd_Find_Line2";$left_i;$top_i;$right_i;$bottom_i)
	$right_i:=$width_i-20
	OBJECT MOVE:C664(*;"Fnd_Find_Line2";$left_i;$top_i;$right_i;$bottom_i;*)
	
	
	OBJECT GET COORDINATES:C663(Fnd_Find_OKButton_i;$left_i;$top_i;$right_i;$bottom_i)  // DB060517 - Added.
	$buttonWidth_i:=$right_i-$left_i  // DB060517 - Added.
	$okButtonLeft_i:=$width_i-$buttonWidth_i-20  // DB060517 - Now uses $buttonWidth_i rather than 69.
	$right_i:=$okButtonLeft_i+$buttonWidth_i  // DB060517 - Now uses $buttonWidth_i rather than 69.
	OBJECT MOVE:C664(Fnd_Find_OKButton_i;$okButtonLeft_i;$top_i;$right_i;$bottom_i;*)  // DB070219 - Fixed a bug. Changed $left_i to $okButtonLeft_i.
	
	OBJECT GET COORDINATES:C663(Fnd_Find_CancelButton_i;$left_i;$top_i;$right_i;$bottom_i)  // DB060517 - Added.
	$buttonWidth_i:=$right_i-$left_i  // DB060517 - Added.
	$left_i:=$okButtonLeft_i-$buttonWidth_i-12  // DB060517 - Now uses $buttonWidth_i rather than 69.
	$right_i:=$left_i+$buttonWidth_i
	OBJECT MOVE:C664(Fnd_Find_CancelButton_i;$left_i;$top_i;$right_i;$bottom_i;*)
End if 