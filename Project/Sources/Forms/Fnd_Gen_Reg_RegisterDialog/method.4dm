  // ----------------------------------------------------

  // Form Method: Fnd_Gen_Reg_RegDialogFormMethod


  // We call this from a protected method because a public form can't

  //   call private methods.


  // Created by Dave Batton on Aug 16, 2003

  // Modified by Dave Batton on May 23, 2004

  //   Removed some of the procedural localization code in favor of

  //   just switching pages to localize the text.  This way the General

  //   component can be localized without requiring the Localization

  //   component.

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Load:K2:1)
		  // We're going to localize this without requiring the localization component.

		Case of 
			: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
				FORM GOTO PAGE:C247(2)
				SET WINDOW TITLE:C213("Enregistrer")
			Else 
				SET WINDOW TITLE:C213("Register")
		End case 
		
		  // Set the default values for the fields.

		<>Fnd_Gen_Reg_UserName_t:=""
		<>Fnd_Gen_Reg_ActivationCode_t:=""
End case 
