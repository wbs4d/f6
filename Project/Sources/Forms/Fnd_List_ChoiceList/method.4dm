// ----------------------------------------------------
// Form Method: Fnd_List_ChoiceList

// Created by Dave Batton on Sep 2, 2003
// Modified by Dave Batton on Jun 2, 2004
//   Fixed a bug that caused the Cancel button to move the wrong way
//   if the OK button was resized due to localization.
// Modified by Dave Batton on Aug 2, 2004
//   The Fnd_Gen_ButtonText calls now moves the Cancel button directly.
// Modified by Dave Batton on Apr 5, 2005
//   Added support for the optional New button.
// Modified by Ed Heckman, October 27, 2015
// Modified by wwn November 9, 2015:  per Dave Nasralla's note RE: 
// v15 changed the way OBJECT GET COORDINATES works with ListBoxes
// Mod by Wayne Stewart, (2019-11-13) - Use Fnd.list object
// ----------------------------------------------------

C_POINTER:C301($OKButton_ptr; $CancelButton_ptr; $NewButton_ptr)

Case of 
	: (Form event code:C388=On Load:K2:1)
		$OKButton_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Fnd_List_OKButton_i")
		$CancelButton_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Fnd_List_CancelButton_i")
		$NewButton_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Fnd_List_NewButton_i")
		
		Fnd.List.SelectedElement:=0
		Form:C1466.TypeAheadString:=""  // Initialise the type ahead string
		
		Fnd_Gen_ButtonText("Fnd_List_OKButton_i"; Fnd_Gen_GetString("Fnd_Gen"; "OK"); Align right:K42:4; $CancelButton_ptr)
		//Fnd_Gen_ButtonText("Fnd_List_CancelButton_i"; Fnd_Gen_GetString("Fnd_Gen"; "Cancel"); Align right)
		Fnd_Gen_ButtonText($CancelButton_ptr; Fnd_Gen_GetString("Fnd_Gen"; "Cancel"); Align right:K42:4)
		
		If (Length:C16(Fnd.List.NewButtonLabel)>0)
			Fnd_Gen_ButtonText("Fnd_List_NewButton_i"; Fnd.List.NewButtonLabel; Align left:K42:2)
		Else 
			OBJECT SET VISIBLE:C603(*; "Fnd_List_NewButton_i"; False:C215)
		End if 
		
		// If there's no prompt, resize the list box to use that space.
		If (Length:C16(Fnd.List.Prompt)=0)
			OBJECT GET COORDINATES:C663(*; "Fnd_List_ListBox"; $left_i; $top_i; $right_i; $bottom_i)  // November 9, 2015: wwn
			$top_i:=20
			OBJECT MOVE:C664(*; "Fnd_List_ListBox"; $left_i; $top_i; $right_i; $bottom_i; *)  // November 9, 2015: wwn
		End if 
		
	: (Form event code:C388=On Timer:K2:25)
		// See Fnd_List_TypeAhead ()
		Form:C1466.TypeAheadString:=""
		SET TIMER:C645(0)
		
		
End case 


// Do this stuff for any event.
OBJECT SET ENABLED:C1123(*; "Fnd_List_OKButton_i"; (Fnd_List_ChoiceArray1_at>0))
Fnd.List.SelectedElement:=Fnd_List_ChoiceArray1_at
