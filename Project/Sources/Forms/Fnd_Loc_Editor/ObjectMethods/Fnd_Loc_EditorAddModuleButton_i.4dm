  // ----------------------------------------------------
  // Object Method: ◊Fnd_Loc_EditorAddModuleButton_i

  // Created by Dave Batton on Jun 11, 2004
  // Modified by : Vincent Tournier (06/17/2011)
  // ----------------------------------------------------


C_TEXT:C284($listName_t)
C_LONGINT:C283($element_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		Fnd_Loc_Editor_SaveList   // Save any previously made changes.
		
		$listName_t:=Request:C163("Enter the new module name:";"untitled_module")
		
		If (OK=1)
			$listName_t:=Replace string:C233($listName_t;" ";"_")
			$element_i:=Find in array:C230(<>Fnd_Loc_EditorModules_at;$listName_t)
			If ($element_i=-1)
				$element_i:=Size of array:C274(<>Fnd_Loc_EditorModules_at)+1
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorModules_at;$element_i)
				<>Fnd_Loc_EditorModules_at{$element_i}:=$listName_t
				SORT ARRAY:C229(<>Fnd_Loc_EditorModules_at;>)
				$element_i:=Abs:C99(Find in array:C230(<>Fnd_Loc_EditorModules_at;$listName_t))
				
				  // BEGIN Modified by : Vincent Tournier (06/17/2011)
				  //ARRAY TO LIST(◊Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
				Fnd_Loc_ArrayToList (-><>Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
				  // END Modified by : Vincent Tournier (06/17/2011)
				
				
				  // Don't set ◊Fnd_Loc_EditorModuleModified_b to True here.  If they
				  //   don't add anything to the list, we don't want to save it.
				
			End if 
			
			<>Fnd_Loc_EditorModules_at:=$element_i
			Fnd_Loc_Editor_ClickOnModsList 
		End if 
End case 
