  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorDeleteModButton_i


  // Created by Dave Batton on Jun 11, 2004
  // Modified by : Vincent Tournier (06/17/2011)
  // ----------------------------------------------------


C_TEXT:C284($listName_t)
C_LONGINT:C283($element_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		CONFIRM:C162("Are you sure you want to delete this localization module?  This cannot be undone.";"Delete")
		
		If (OK=1)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorModules_at;<>Fnd_Loc_EditorModules_at)
			
			  // BEGIN Modified by : Vincent Tournier (06/17/2011)
			  //ARRAY TO LIST(◊Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
			Fnd_Loc_ArrayToList (-><>Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
			  // END Modified by : Vincent Tournier (06/17/2011)
			
			ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;1)
			<>Fnd_Loc_EditorTempStringsEN_at{1}:="  ` Please delete this list."
			Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempStringsEN_at;<>Fnd_Loc_EditorModuleName_t)
			Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempStringsEN_at;<>Fnd_Loc_EditorModuleName_t+"_EN")
			Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempStringsEN_at;<>Fnd_Loc_EditorModuleName_t+"_"+<>Fnd_Loc_EditorLanguage2Code_t)
			ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)
			
			<>Fnd_Loc_EditorModules_at:=0
			Fnd_Loc_Editor_ClickOnModsList 
			ALERT:C41("The lists have been marked for deletion in 4D's List Editor. "+"Please delete these lists at your convenience.")
		End if 
End case 
