  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorAddCodeButton_i


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


C_TEXT:C284($lookupCode_t)
C_LONGINT:C283($element_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		$lookupCode_t:="untitled_code"
		
		$element_i:=Find in array:C230(<>Fnd_Loc_EditorCodes_at;$lookupCode_t)
		If ($element_i=-1)
			$element_i:=Size of array:C274(<>Fnd_Loc_EditorCodes_at)+1
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorCodes_at;$element_i)
			<>Fnd_Loc_EditorCodes_at{$element_i}:=$lookupCode_t
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroupCodes_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsEN_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsXX_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			
			<>Fnd_Loc_EditorGroupCodes_at{<>Fnd_Loc_EditorGroups_at}{$element_i}:=$lookupCode_t
			<>Fnd_Loc_EditorStringsEN_at{<>Fnd_Loc_EditorGroups_at}{$element_i}:="New Text"
			<>Fnd_Loc_EditorStringsXX_at{<>Fnd_Loc_EditorGroups_at}{$element_i}:=""
			
			<>Fnd_Loc_EditorModuleModified_b:=True:C214  // So this change is saved.

		End if 
		
		<>Fnd_Loc_EditorCodes_at:=$element_i
		Fnd_Loc_Editor_ClickOnCodesList 
		
		HIGHLIGHT TEXT:C210(<>Fnd_Loc_EditorLookupCode_t;1;MAXTEXTLENBEFOREV11:K35:3)
End case 
