  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorAddGroupButton_i


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


C_TEXT:C284($groupDesc_t)
C_LONGINT:C283($element_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		$groupDesc_t:="untitled group"
		
		$element_i:=Find in array:C230(<>Fnd_Loc_EditorGroups_at;$groupDesc_t)
		If ($element_i=-1)
			$element_i:=Size of array:C274(<>Fnd_Loc_EditorGroups_at)+1
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroups_at;$element_i)
			<>Fnd_Loc_EditorGroups_at{$element_i}:=$groupDesc_t
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroupCodes_at;$element_i)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsEN_at;$element_i)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsXX_at;$element_i)
			
			<>Fnd_Loc_EditorModuleModified_b:=True:C214  // So this change is saved.

		End if 
		
		<>Fnd_Loc_EditorGroups_at:=$element_i
		Fnd_Loc_Editor_ClickOnGroupList 
End case 
