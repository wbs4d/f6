  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorDeleteCodeButton_i


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


C_LONGINT:C283($element_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		CONFIRM:C162("Are you sure you want to delete this lookup code?  This cannot be undone.";"Delete")
		
		If (OK=1)
			$element_i:=<>Fnd_Loc_EditorCodes_at
			
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorCodes_at;$element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorGroupCodes_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorStringsEN_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorStringsXX_at{<>Fnd_Loc_EditorGroups_at};$element_i)
			
			<>Fnd_Loc_EditorModuleModified_b:=True:C214
			
			<>Fnd_Loc_EditorCodes_at:=0
			Fnd_Loc_Editor_ClickOnCodesList 
		End if 
End case 
