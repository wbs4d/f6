  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorGroupName_t


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On After Keystroke:K2:26)
		<>Fnd_Loc_EditorModules_at{<>Fnd_Loc_EditorModules_at}:=Get edited text:C655
		
	: (Form event:C388=On Data Change:K2:15)
		<>Fnd_Loc_EditorModules_at{<>Fnd_Loc_EditorModules_at}:=Self:C308->
		
		If (Size of array:C274(<>Fnd_Loc_EditorGroups_at)=0)  // If this is an empty list, see if we just gave this the name of an existing list.

			Fnd_Loc_Editor_ClickOnGroupList 
		Else 
			<>Fnd_Loc_EditorModuleModified_b:=True:C214
		End if 
End case 
