  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorImportButton_i


  // Created by Dave Batton on Jun 12, 2004

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		  // The import routine is not yet complete.

		ABORT:C156
		Fnd_Loc_Editor_Import 
		Fnd_Loc_Editor_LoadModules 
End case 
