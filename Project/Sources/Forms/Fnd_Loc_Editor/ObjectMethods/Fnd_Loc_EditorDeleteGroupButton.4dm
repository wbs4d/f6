  // ----------------------------------------------------

  // Object Method: ◊Fnd_Loc_EditorDeleteGroupButton_i


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


C_LONGINT:C283($groupNumber_i)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		CONFIRM:C162("Are you sure you want to delete this group?  This cannot be undone.";"Delete")
		
		If (OK=1)
			$groupNumber_i:=<>Fnd_Loc_EditorGroups_at
			
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorGroupCodes_at;$groupNumber_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorStringsEN_at;$groupNumber_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorStringsXX_at;$groupNumber_i)
			DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorGroups_at;$groupNumber_i)
			
			<>Fnd_Loc_EditorModuleModified_b:=True:C214
			
			<>Fnd_Loc_EditorGroups_at:=0
			Fnd_Loc_Editor_ClickOnGroupList 
		End if 
End case 
