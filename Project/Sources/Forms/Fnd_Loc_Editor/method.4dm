  // ----------------------------------------------------

  // Form Method: Fnd_Loc_Editor


  // Created by Dave Batton on Jun 11, 2004

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Load:K2:1)
		  // Someday the plan is to support more than two languages.

		ARRAY TEXT:C222(<>Fnd_Loc_EditorLanguageCodes_at;2)  // Currently these routines support only English and French.

		<>Fnd_Loc_EditorLanguageCodes_at{1}:="EN"
		<>Fnd_Loc_EditorLanguageCodes_at{2}:="FR"
		
		  // Right now, it's just English and French.

		<>Fnd_Loc_EditorLanguage2Code_t:=<>Fnd_Loc_EditorLanguageCodes_at{2}  //  The second language to display.

		
		Fnd_Loc_Editor_LoadModules 
		
		
	: (Form event:C388=On Close Box:K2:21)
		Fnd_Loc_Editor_ClickOnModsList 
		CANCEL:C270
		
		
	: (Form event:C388=On Activate:K2:9)
		Fnd_Loc_Editor_EditorClosed 
End case 
