// ----------------------------------------------------
// Form Method: [Fnd_SqNo].Fnd_SqNo_EditGroup

// Created by Dave Batton on Nov 27, 2003
// Modified by Dave Batton on Aug 2, 2004
//   The Fnd_Gen_ButtonText calls now moves the Cancel button directly.
// Modified by Dave Batton on Feb 28, 2006
//   Changed the call to Fnd_VS_TableName to Fnd_SqNo_TableName.
//   Changed the Fnd_Gen_GetString calls to Fnd_Gen_GetString and wrapped
//      the Fnd_Loc_FixButtonWidths call in an EXECUTE so the Localization
//      component isn't required.
// Modified by Dave Batton on Mar 23, 2006
//   This routine now references tables and fields using pointers.
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_TEXT:C284($groupName_t)
C_LONGINT:C283($tableNumber_i)

Case of 
	: (Form event code:C388=On Load:K2:1)
		<>Fnd_SqNo_Label2_t:=Fnd_Gen_GetString("Fnd_SqNo"; "GroupNameLabel")
		<>Fnd_SqNo_Label3_t:=Fnd_Gen_GetString("Fnd_SqNo"; "NextNumberLabel")
		<>Fnd_SqNo_Label4_t:=Fnd_Gen_GetString("Fnd_SqNo"; "ReuseLabel")
		
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_AutoFixButton_i; Fnd_Gen_GetString("Fnd_SqNo"; "AutoFixButton"); Align left:K42:2)
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_AddNumButton_i; Fnd_Gen_GetString("Fnd_SqNo"; "AddNumButton"); Align left:K42:2)
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_DeleteNumButton_i; Fnd_Gen_GetString("Fnd_SqNo"; "DeleteNumButton"); Align left:K42:2)
		
		// Make all of the buttons the same size again. 
		If (Fnd_Gen_ComponentAvailable("Fnd_Loc"))  // DB060228
			EXECUTE METHOD:C1007("Fnd_Loc_FixButtonWidths"; *; -><>Fnd_SqNo_AutoFixButton_i; -><>Fnd_SqNo_AddNumButton_i; -><>Fnd_SqNo_DeleteNumButton_i)
		End if 
		
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_DesignerOnlyCheckbox_i; Fnd_Gen_GetString("Fnd_SqNo"; "DesignerOnlyCheckbox"); Align left:K42:2; -><>Fnd_SqNo_SizingButton_i)
		
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_OKButton_i; Fnd_Gen_GetString("Fnd_SqNo"; "OKButton"); Align right:K42:4; -><>Fnd_SqNo_CancelButton_i)
		Fnd_Gen_ButtonText(-><>Fnd_SqNo_CancelButton_i; Fnd_Gen_GetString("Fnd_SqNo"; "CancelButton"); Align right:K42:4)
		
		// Set the group name variable. If the group name starts with "*Table" then we 
		//   need to replace the actual name with the name of the table.
		$groupName_t:=<>Fnd_SqNo_GroupNameFld_ptr->
		If ($groupName_t="*Table #@")
			$tableNumber_i:=Num:C11(Substring:C12($groupName_t; (Position:C15("#"; $groupName_t)+1)))
			$groupName_t:=Fnd_Gen_GetString("Fnd_SqNo"; "IDFieldGroupName"; Fnd_SqNo_TableName(Table:C252($tableNumber_i)))
		End if 
		<>Fnd_SqNo_EditGroupName_t:=$groupName_t
		
		// Disable nearly everything if it's a table group, or just some stuff if
		//   the current user isn't a member of the design team.
		// ### 3.7.2 Modification - More stuff disabled for table numbers.
		Case of 
			: (<>Fnd_SqNo_GroupNameFld_ptr->="*Table #@")
				OBJECT SET ENABLED:C1123(<>Fnd_SqNo_AddNumButton_i; False:C215)
				OBJECT SET ENTERABLE:C238(*; "Fnd_SqNo_Disable@"; False:C215)
				OBJECT SET RGB COLORS:C628(*; "Fnd_SqNo_Disable@"; Foreground color:K23:1; Background color:K23:2)
			: (Not:C34(Fnd_SqNo_IsInDesignGroup))
				OBJECT SET ENTERABLE:C238(<>Fnd_SqNo_EditGroupName_t; False:C215)
				OBJECT SET RGB COLORS:C628(<>Fnd_SqNo_EditGroupName_t; Foreground color:K23:1; Background color:K23:2)
		End case 
		
		// If it's not a table group, disable the Auto Fix button.
		If (<>Fnd_SqNo_GroupNameFld_ptr->#"*Table #@")
			OBJECT SET VISIBLE:C603(<>Fnd_SqNo_AutoFixButton_i; False:C215)
		End if 
		
		<>Fnd_SqNo_EditNextNumber_i:=<>Fnd_SqNo_NextNumberFld_ptr->
		
		// Load the list of numbers to reuse.
		ARRAY LONGINT:C221(<>Fnd_SqNo_ReuseNumbersArray_ai; 0)
		BLOB TO VARIABLE:C533(<>Fnd_SqNo_RecycleBinFld_ptr->; <>Fnd_SqNo_ReuseNumbersArray_ai)
		SORT ARRAY:C229(<>Fnd_SqNo_ReuseNumbersArray_ai; >)  // We don't bother to sort the array until we need something. That's now.
		<>Fnd_SqNo_ReuseNumbersArray_ai:=0
		
		<>Fnd_SqNo_DesignerOnlyCheckbox_i:=Num:C11(<>Fnd_SqNo_DesignerOnlyFld_ptr->)
		If (Not:C34(Fnd_SqNo_IsInDesignGroup))
			OBJECT SET VISIBLE:C603(<>Fnd_SqNo_DesignerOnlyCheckbox_i; False:C215)
		End if 
End case 


If ((Size of array:C274(<>Fnd_SqNo_ReuseNumbersArray_ai)>0) & (<>Fnd_SqNo_ReuseNumbersArray_ai>0))
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_DeleteNumButton_i; True:C214)
Else 
	OBJECT SET ENABLED:C1123(<>Fnd_SqNo_DeleteNumButton_i; False:C215)
End if 