  // ----------------------------------------------------

  // Object Method: [Fnd_SqNo].Fnd_SqNo_EditGroup.<>Fnd_SqNo_DeleteNumButton_i


  // Created by Dave Batton on Mar 25, 2004

  // ----------------------------------------------------


Case of 
	: (Form event:C388=On Clicked:K2:4)
		If (<>Fnd_SqNo_ReuseNumbersArray_ai>0)
			DELETE FROM ARRAY:C228(<>Fnd_SqNo_ReuseNumbersArray_ai;<>Fnd_SqNo_ReuseNumbersArray_ai)
			If (<>Fnd_SqNo_ReuseNumbersArray_ai>Size of array:C274(<>Fnd_SqNo_ReuseNumbersArray_ai))
				<>Fnd_SqNo_ReuseNumbersArray_ai:=Size of array:C274(<>Fnd_SqNo_ReuseNumbersArray_ai)
			End if 
		End if 
End case 