  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_EditGroup.<>Fnd_SqNo_AddNumButton_i

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Dave Batton on Jan 15, 2005
  //   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

C_LONGINT:C283($newNum_i;$element_i)
C_TEXT:C284($newNum_str)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
		$newNum_str:=Fnd_Dlg_Request (Fnd_Gen_GetString ("Fnd_SqNo";"AddNumberPrompt"))
		  //$newNum_str:=Fnd_Dlg_GetRequest 
		$newNum_i:=Num:C11($newNum_str)
		
		$element_i:=Find in array:C230(<>Fnd_SqNo_ReuseNumbersArray_ai;$newNum_i)
		
		Case of 
			: (OK=0)
				
			: ($newNum_str#(String:C10($newNum_i)))
				Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
				Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
				Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"InvalidNumber";$newNum_str))
				OK:=0
				
			: ($newNum_i<=0)
				Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
				Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
				Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"GreaterThanZero"))
				OK:=0
				
			: ($newNum_i>=<>Fnd_SqNo_NextNumberFld_ptr->)
				Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
				Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
				Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"TooHigh"))
				OK:=0
				
			: ($element_i>0)
				Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
				Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
				Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"AlreadyInList";$newNum_str))
				OK:=0
		End case 
		
		If (OK=1)
			INSERT IN ARRAY:C227(<>Fnd_SqNo_ReuseNumbersArray_ai;1)
			<>Fnd_SqNo_ReuseNumbersArray_ai{1}:=$newNum_i
			SORT ARRAY:C229(<>Fnd_SqNo_ReuseNumbersArray_ai;>)
			$element_i:=Find in array:C230(<>Fnd_SqNo_ReuseNumbersArray_ai;$newNum_i)
			If ($element_i>0)
				<>Fnd_SqNo_ReuseNumbersArray_ai:=$element_i
			End if 
		End if 
End case 
