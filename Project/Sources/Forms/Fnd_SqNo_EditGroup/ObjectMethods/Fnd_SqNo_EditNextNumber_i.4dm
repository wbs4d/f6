  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_EditGroup.<>Fnd_SqNo_EditNextNumber_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Data Change:K2:15)
		<>Fnd_SqNo_NextNumberFld_ptr->:=Self:C308->
End case 