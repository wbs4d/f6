  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_EditGroup.<>Fnd_SqNo_AutoFixButton_i

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Dave Batton on Jan 15, 2005
  //   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition when displaying alerts and confirms.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

C_LONGINT:C283($tableNumber_i)
C_POINTER:C301($table_ptr)

Case of 
	: (Form event:C388=On Clicked:K2:4)
		  // This button should be available only if we're editing a table ID group, but 
		  //   let's be safe.
		If (<>Fnd_SqNo_GroupNameFld_ptr->#"*Table #@")
			Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
			Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
			Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"CannotFix"))
			
		Else 
			Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
			Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
			Fnd_Dlg_SetText (Fnd_Gen_GetString ("Fnd_SqNo";"AutoFixConfirm1";<>Fnd_SqNo_EditGroupName_t);Fnd_Gen_GetString ("Fnd_SqNo";"AutoFixConfirm2"))
			Fnd_Dlg_SetButtons ("*";"*")
			Fnd_Dlg_Display 
			
			If (OK=1)
				$tableNumber_i:=Num:C11(Substring:C12(<>Fnd_SqNo_GroupNameFld_ptr->;(Position:C15("#";<>Fnd_SqNo_GroupNameFld_ptr->)+1)))
				$table_ptr:=Table:C252($tableNumber_i)
				Fnd_SqNo_Fix ($table_ptr)
				LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)  // Because SeqNoReset will unload it.
				<>Fnd_SqNo_EditNextNumber_i:=<>Fnd_SqNo_NextNumberFld_ptr->
				REDRAW:C174(<>Fnd_SqNo_EditNextNumber_i)
				
				  // Clear the Reuse numbers. They may be invalid.
				ARRAY LONGINT:C221(<>Fnd_SqNo_ReuseNumbersArray_ai;0)
			End if 
		End if 
End case 


