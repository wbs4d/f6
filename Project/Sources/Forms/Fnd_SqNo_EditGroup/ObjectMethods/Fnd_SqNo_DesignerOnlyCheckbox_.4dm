  // ----------------------------------------------------
  // Object Method: [Fnd_SqNo].Fnd_SqNo_EditGroup.<>Fnd_SqNo_DesignerOnlyCheckbox_i

  // Created by Dave Batton on Mar 25, 2004
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Clicked:K2:4)
		<>Fnd_SqNo_DesignerOnlyFld_ptr->:=(Self:C308->=1)
End case 