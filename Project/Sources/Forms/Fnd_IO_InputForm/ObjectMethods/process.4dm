C_TEXT:C284($Text_t)

If (Form event code:C388=On Load:K2:1)
	OBJECT SET VISIBLE:C603(*; "process"; (Not:C34(Is compiled mode:C492) | (Shift down:C543)))
	
Else 
	If (Fnd#Null:C1517)
		$Text_t:=JSON Stringify:C1217(Fnd; *)
	Else 
		$Text_t:="Fnd not present"  //JSON Stringify(New object)
	End if 
	SET TEXT TO PASTEBOARD:C523($Text_t)
	
End if 