//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_ClickOnModsList

  // Called from the localization editor when a module name is clicked, or
  //   when another object changes the selected list item

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 11, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($groupNumber_i;$element_i;$itemNumber_i)

Fnd_Loc_Editor_SaveList   // Save any previously made changes.


ARRAY TEXT:C222(<>Fnd_Loc_EditorGroupCodes_at;0;0)
ARRAY TEXT:C222(<>Fnd_Loc_EditorStringsEN_at;0;0)
ARRAY TEXT:C222(<>Fnd_Loc_EditorStringsXX_at;0;0)

ARRAY TEXT:C222(<>Fnd_Loc_EditorGroups_at;0)  // The list of groups.

If (<>Fnd_Loc_EditorModules_at=0)
	<>Fnd_Loc_EditorModuleName_t:=""
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorModuleName_t;False:C215)
	<>Fnd_Loc_EditorOriginalModName_t:=""
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteModButton_i;False:C215)
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorAddGroupButton_i;False:C215)
	
Else 
	<>Fnd_Loc_EditorModuleName_t:=<>Fnd_Loc_EditorModules_at{<>Fnd_Loc_EditorModules_at}
	  //SET ENTERABLE(◊Fnd_Loc_EditorModuleName_t;True)
	  //HIGHLIGHT TEXT(◊Fnd_Loc_EditorModuleName_t;1;MAXTEXTLEN )
	
	<>Fnd_Loc_EditorOriginalModName_t:=<>Fnd_Loc_EditorModules_at{<>Fnd_Loc_EditorModules_at}
	
	  // Load the lists.
	Fnd_Loc_ListToArray (<>Fnd_Loc_EditorOriginalModName_t;-><>Fnd_Loc_EditorTempCodes_at)
	Fnd_Loc_ListToArray (<>Fnd_Loc_EditorOriginalModName_t+"_EN";-><>Fnd_Loc_EditorTempStringsEN_at)
	Fnd_Loc_ListToArray (<>Fnd_Loc_EditorOriginalModName_t+"_"+<>Fnd_Loc_EditorLanguage2Code_t;-><>Fnd_Loc_EditorTempStringsXX_at)
	
	  // Make sure all of the arrays are the same size.
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;Size of array:C274(<>Fnd_Loc_EditorTempCodes_at))
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsXX_at;Size of array:C274(<>Fnd_Loc_EditorTempCodes_at))
	
	If (Size of array:C274(<>Fnd_Loc_EditorTempCodes_at)>0)
		If (<>Fnd_Loc_EditorTempCodes_at{1}#"  ` @")  // If it doesn't start with a group, it won't work right.  So create a group.
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempCodes_at;1)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempStringsEN_at;1)
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempStringsXX_at;1)
			<>Fnd_Loc_EditorTempCodes_at{1}:="  ` untitled group"
			<>Fnd_Loc_EditorTempStringsEN_at{1}:=<>Fnd_Loc_EditorTempCodes_at{1}
			<>Fnd_Loc_EditorTempStringsXX_at{1}:=<>Fnd_Loc_EditorTempCodes_at{1}
			<>Fnd_Loc_EditorModuleModified_b:=True:C214
		End if 
	End if 
	
	$groupNumber_i:=0
	For ($itemNumber_i;1;Size of array:C274(<>Fnd_Loc_EditorTempCodes_at))
		Case of 
			: (<>Fnd_Loc_EditorTempCodes_at{$itemNumber_i}="  ` @")
				$groupNumber_i:=$groupNumber_i+1
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroups_at;$groupNumber_i)
				<>Fnd_Loc_EditorGroups_at{$groupNumber_i}:=Substring:C12(<>Fnd_Loc_EditorTempCodes_at{$itemNumber_i};5)
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroupCodes_at;$groupNumber_i)
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsEN_at;$groupNumber_i)
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsXX_at;$groupNumber_i)
				
			: (<>Fnd_Loc_EditorTempCodes_at{$itemNumber_i}="")
				  // Ignore blank lines.
				
			Else 
				$element_i:=Size of array:C274(<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i})+1
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i};$element_i)
				<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i}{$element_i}:=<>Fnd_Loc_EditorTempCodes_at{$itemNumber_i}
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsEN_at{$groupNumber_i};$element_i)
				<>Fnd_Loc_EditorStringsEN_at{$groupNumber_i}{$element_i}:=<>Fnd_Loc_EditorTempStringsEN_at{$itemNumber_i}
				INSERT IN ARRAY:C227(<>Fnd_Loc_EditorStringsXX_at{$groupNumber_i};$element_i)
				<>Fnd_Loc_EditorStringsXX_at{$groupNumber_i}{$element_i}:=<>Fnd_Loc_EditorTempStringsXX_at{$itemNumber_i}
		End case 
	End for 
	
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteModButton_i;True:C214)
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorAddGroupButton_i;True:C214)
	
	  // Clear the temporary arrays.
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempCodes_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsXX_at;0)
End if 

<>Fnd_Loc_EditorGroups_at:=0
Fnd_Loc_Editor_ClickOnGroupList 
