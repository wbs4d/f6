//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_DecodeBase64Blob (->blob)

  // Converts the blob from Base64 back to ASCII.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a blob

  // Returns: Nothing

  // Based on code submitted to 4DToday.com by Peter Bozek.
  // Modified by Dave Batton on Apr 10, 2005
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_POINTER:C301($1;$pPassedBlob)  // Pointer to the Blob to decode.
C_LONGINT:C283($i;$iOffset;$iDest)
C_LONGINT:C283($lValue;$lValue1;$lValue2;$lValue3;$lValue4)
C_LONGINT:C283($temp;$temp2)
C_LONGINT:C283($lBlobSize;$lEOLLength)
C_BLOB:C604($oDecoding)
C_TEXT:C284($encoding)

$pPassedBlob:=$1
$lBlobSize:=BLOB size:C605($pPassedBlob->)

If ($lBlobSize>0)
	
	$encoding:="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	$encoding:=$encoding+"abcdefghijklmnopqrstuvwxyz0123456789+/"
	
	SET BLOB SIZE:C606($oDecoding;0x00FF;0x0040)
	
	For ($i;1;64)
		$oDecoding{Character code:C91($encoding[[$i]])}:=$i-1
	End for 
	
	SET BLOB SIZE:C606($pPassedBlob->;(($lBlobSize+3)\4)*4;0x0000)
	$lBlobSize:=BLOB size:C605($pPassedBlob->)
	
	  //resizing of passed blob should not be necessary!
	
	$iOffset:=0
	$iDest:=0
	While ($iOffset<$lBlobSize)
		If ($oDecoding{$pPassedBlob->{$iOffset}}=0x0040)  //not a part of stream
			$iOffset:=$iOffset+1
		Else 
			  //proces block of 4 Bytes
			If ($iOffset<($lBlobSize-1))
				$lValue:=BLOB to longint:C551($pPassedBlob->;Macintosh byte ordering:K22:2;$iOffset)
				
				$lValue1:=$lValue >> 0x0018
				$lValue2:=($lValue >> 0x0010) & 0x00FF
				$lValue3:=($lValue >> 0x0008) & 0x00FF
				$lValue4:=$lValue & 0x00FF
				
				
				$pPassedBlob->{$iDest}:=($oDecoding{$lValue1} << 2) | ($oDecoding{$lValue2} >> 4)
				$iDest:=$iDest+1
				
				$temp:=$oDecoding{$lValue3}
				If ($temp<0x0040)
					$pPassedBlob->{$iDest}:=($oDecoding{$lValue2} << 4) | ($temp >> 2)
					$iDest:=$iDest+1
					
					$temp2:=$oDecoding{$lValue4}
					If ($temp2<0x0040)
						$pPassedBlob->{$iDest}:=($temp << 6) | $temp2
						$iDest:=$iDest+1
					End if 
				End if 
			Else 
				$iOffset:=$lBlobSize
			End if 
		End if 
	End while 
	
	If ($iDest>0)
		SET BLOB SIZE:C606($pPassedBlob->;$iDest)
	End if 
	
End if 