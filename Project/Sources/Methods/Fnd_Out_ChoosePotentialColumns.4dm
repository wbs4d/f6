//%attributes = {}
// ----------------------------------------------------
// Project Method: Fnd_Out_ChoosePotentialColumns

// Description

// Access: ???

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Created by Wayne Stewart (2023-05-01)

//     waynestewart@mac.com
// ----------------------------------------------------


If (False:C215)
	Fnd_Out_ChoosePotentialColumns
End if 

Fnd_Out_Init

var $tableNumber_i; $fieldNumber_i; $type_i; $fieldCount_i : Integer
var $field_ptr : Pointer
var $virtualTableName_t; $fieldName_t; $virtualFieldName_t : Text

$tableNumber_i:=Fnd.out.currentTable

If (Fnd.out.potentialColumns=Null:C1517)
	Fnd.out.potentialColumns:=New object:C1471("columns"; New collection:C1472)  // add the object and collection
	
	ARRAY TEXT:C222($formula_at; 0)
	ARRAY TEXT:C222($table_at; 0)
	ARRAY TEXT:C222($field_at; 0)
	ARRAY BOOLEAN:C223($display_ab; 0)
	
	// First add this table's fields to the list.
	
	$fieldCount_i:=Get last field number:C255($tableNumber_i)
	
	$virtualTableName_t:=Fnd_VS_TableName(Table:C252($tableNumber_i))
	
	For ($fieldNumber_i; 1; $fieldCount_i)
		If (Is field number valid:C1000($tableNumber_i; $fieldNumber_i))
			$field_ptr:=Field:C253($tableNumber_i; $fieldNumber_i)
			$type_i:=Type:C295($field_ptr->)
			If (($type_i=Is alpha field:K8:1)\
				 | ($type_i=Is date:K8:7)\
				 | ($type_i=Is time:K8:8)\
				 | ($type_i=Is longint:K8:6)\
				 | ($type_i=Is integer:K8:5)\
				 | ($type_i=Is real:K8:4)\
				 | ($type_i=Is boolean:K8:9))
				
				$virtualFieldName_t:=Fnd_VS_FieldName($field_ptr)
				$fieldName_t:=Field name:C257($field_ptr)
				
				APPEND TO ARRAY:C911($table_at; $virtualTableName_t)
				APPEND TO ARRAY:C911($field_at; $virtualFieldName_t)
				APPEND TO ARRAY:C911($display_ab; False:C215)
				APPEND TO ARRAY:C911($formula_at; $fieldName_t)
				
			End if 
		End if 
	End for 
	
	
	
	
End if 