//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Find_AddSeparator ({position})


// Adds a separator line to the list of searchable fields.


// Access Type: Shared


// Parameters: 

//   $1 : Longint : Position of the separator (optional)


// Returns: Nothing


// Created by Dave Batton on Jul 20, 2003

// ----------------------------------------------------


C_LONGINT:C283($1;$position_i)

If (Count parameters:C259>=1)
	$position_i:=$1
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Find_AddItem($position_i;"-";"")