//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_SetMenuItem (menu number; item number; label)

  // A wrapper for the SET MENU ITEM command that parses out the shortcut command
  //   and sets it using 4D's SET MENU ITEM KEY command.
  // Pass the shortcut at the end of the label after the "/" character.
  //   Fnd_Menu_SetMenuItem (1;5;"Print.../P")

  // Access: Private

  // Parameters: 
  //   $1 : Longint : The menu number
  //   $2 : Longint : The menu item number
  //   $3 : Longint : The new label, with shortcut if desired

  // Returns: Nothing

  // Created by Dave Batton on Sep 29, 2003
  // Modified by Dave Batton on May 22, 2004
  //   Now tests to make sure the menu number > 0.
  // ----------------------------------------------------

C_LONGINT:C283($1;$2;$menuNumber_i;$itemNumber_i;$shortcut_i;$position_i)
C_TEXT:C284($3;$label_t)

$menuNumber_i:=$1
$itemNumber_i:=$2
$label_t:=$3

If ($menuNumber_i>0)  // DB040522 - New test
	$position_i:=Position:C15("/";$label_t)
	If ($position_i>0)
		$shortcut_i:=Character code:C91(Substring:C12($label_t;$position_i+1;1))
		$label_t:=Substring:C12($label_t;1;$position_i-1)
	Else 
		$shortcut_i:=0
	End if 
	
	SET MENU ITEM:C348($menuNumber_i;$itemNumber_i;$label_t)
	
	If ($itemNumber_i>0)  // Since we may have passed a menu title.
		SET MENU ITEM SHORTCUT:C423($menuNumber_i;$itemNumber_i;$shortcut_i)
	End if 
End if 