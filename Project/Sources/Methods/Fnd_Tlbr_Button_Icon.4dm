//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Tlbr_Button_Icon (button name{; icon name}) --> Text


// Call this method to specify the name of a picture library image to

//   use as the button's icon.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The name of the button to modify

//   $2 : Text : The name of the picture to use as the icon (optional)


// Returns: 

//   $0 : Text : The name of the button's icon


// Created by Mark Mitchenall on 11/2/02

// Toolbar Component - © mitchenall.com 2002

// Modified by Dave Batton on Jan 6, 2005

//   Changed the method name from Fnd_Tlbr_Button_Icon_Set.

//   Added a call to Fnd_Tlbr_Init and updated the variable names.

//   Now returns the icon name, and made the second parameter optional.

//   No longer complains if the button name isn't found.

//   Modifies the icon name to include the proper size for the current toolbar style.

// ----------------------------------------------------


C_TEXT:C284($0;$1;$2;$buttonName_t;$iconName_t)
C_LONGINT:C283($buttonNumber_i)

$buttonName_t:=$1

Fnd_Tlbr_Init

$buttonNumber_i:=Fnd_Tlbr_Object_GetNumber($buttonName_t)

If ($buttonNumber_i>0)
	
	If (Count parameters:C259>=2)
		$iconName_t:=$2
		
		//Only change the button text and invalidate the toolbar if the button text
		
		// has actually changed.
		
		If (Fnd_Tlbr_ObjectIconNames_at{$buttonNumber_i}#$iconName_t)
			Fnd_Tlbr_ObjectIconNames_at{$buttonNumber_i}:=$iconName_t
			Fnd_Tlbr_ButtonsAreValid_b:=False:C215
		End if 
	End if 
	
	$iconName_t:=Fnd_Tlbr_ObjectIconNames_at{$buttonNumber_i}
End if 

$0:=$iconName_t