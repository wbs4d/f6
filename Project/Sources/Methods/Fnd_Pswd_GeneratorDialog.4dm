//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pswd_GeneratorDialog --> Text

// Displays a password generator dialog.

// Access: Shared

// Parameters: None

// Returns: 
//   $0 : Text : The generated password

// Created by Dave Batton on Mar 16, 2004
// ----------------------------------------------------

C_TEXT:C284($0)
C_LONGINT:C283($width_i; $height_i)

Fnd_Pswd_Init

<>Fnd_Pswd_WindowTitle_t:=Fnd_Gen_GetString("Fnd_Pswd"; "WindowTitle")

FORM GET PROPERTIES:C674("Fnd_Pswd_Generator"; $width_i; $height_i)

Fnd_Wnd_Position(Fnd_Wnd_CenterOnScreen)
Fnd_Wnd_Title(<>Fnd_Pswd_WindowTitle_t)
Fnd_Wnd_Type(Movable form dialog box:K39:8)
Fnd_Wnd_OpenWindow($width_i; $height_i)

//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
//EXECUTE METHOD("Fnd_Wnd_Title"; *; <>Fnd_Pswd_WindowTitle_t)
//EXECUTE METHOD("Fnd_Wnd_Type"; *; Movable form dialog box)
//EXECUTE METHOD("Fnd_Wnd_Window"; *; $width_i; $height_i)
//Else 
//Fnd_Gen_CenterWindow($width_i; $height_i; Movable form dialog box; Fnd_Wnd_CenterOnScreen; <>Fnd_Pswd_WindowTitle_t; True)
//End if 

DIALOG:C40("Fnd_Pswd_Generator")
CLOSE WINDOW:C154

If (OK=1)
	$0:=<>Fnd_Pswd_GeneratedPassword_t
Else 
	$0:=""
End if 

<>Fnd_Pswd_GeneratedPassword_t:=""  // Just for safety.