//%attributes = {"invisible":true,"shared":true}
/* Fnd_List_UseTypeAhead
Project Method: Fnd_List_UseTypeAhead (boolean) -> boolean

Use type ahead on this occasion

Access: Shared

Parameters: 
   $1 : Boolean : Turn on or off
 
 // Returns: 
  //   $0 : Boolean : The current setting

Created by Wayne Stewart (2019-11-13)
     waynestewart@mac.com



*/

C_BOOLEAN:C305($1; $0)


Fnd_List_Init

If (Count parameters:C259=1)
	Fnd.List.useTypeAhead:=$1
End if 

$0:=Fnd.List.useTypeAhead


