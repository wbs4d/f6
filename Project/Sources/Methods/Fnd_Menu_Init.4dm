//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Init

// Initializes both the process and interprocess variables used by the Fnd_Menu routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 31, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Updated to version 4.0.2.
//   Removed the line that set Fnd_Menu_MenuName_t. It's no longer
//   needed because of the way the Fnd_Menu_MenuBarName method
//   is structured.
// Modified by Dave Batton on May 22, 2004
//   Updated the version number to 4.0.3.
//   Removed the <>Fnd_Menu_FileMenu_i initialization and replaced it
//   with a <>Fnd_Menu_MenuNumbersLoaded_b initialization.
//   Now initializes Fnd_Menu_LastMenuBarName_t.
//   Now requires the Localization component.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Menu_Info method.
// Modified by Dave Batton on Aug 13, 2005
//   Added a call to the new Fnd_Menu_LoadMenuTitles routine.
// Modified by Dave Batton on Mar 16, 2006
//   Added a semaphore timeout variable.
//   Wrapped the IP initialization code in the semaphore.
// Modified by Dave Batton on Mar 26, 2007
//   Removed the component check for the Foundation Windows component.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// Mod by Wayne Stewart, 2023-07-26
//   Moved the test for the semaphore to inside the initialisation routine
//   previously the semaphore was set and unset every time Fnd_XXX_Init is called
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_Menu_Initialized_b; Fnd_Menu_Initialized_b)

If (Not:C34(<>Fnd_Menu_Initialized_b))  // So we only do this once.
	<>Fnd_Menu_Semaphore_t:="$Fnd_Menu_Semaphore"
	<>Fnd_Menu_SemaphoreTimeout_i:=30*60  // DB060316 - Added - 30 seconds.
	
	If (Not:C34(Semaphore:C143(<>Fnd_Menu_Semaphore_t; <>Fnd_Menu_SemaphoreTimeout_i)))
		
		Compiler_Fnd_Menu
		
		Fnd_Menu_Localize  // Must be done before building the menu bar.
		
		Fnd_Menu_BuildMenuBar
		
		<>Fnd_Menu_MenuNumbersLoaded_b:=False:C215
		//Fnd_Menu_LoadMenuTitles   ` DB050731 - New method.
		
		ARRAY TEXT:C222(<>Fnd_Menu_Window_Items_at; 0)
		ARRAY INTEGER:C220(<>Fnd_Menu_Window_Processes_ai; 0)
		
		//Fnd_Menu_UpdateResources   ` Make sure the localization resources are set properly.
		CLEAR SEMAPHORE:C144(<>Fnd_Menu_Semaphore_t)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
	End if 
	
	<>Fnd_Menu_Initialized_b:=True:C214
End if 


If (Not:C34(Fnd_Menu_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Menu
	
	Fnd_Menu_LastMenuBarName_t:=""  // See Fnd_Menu_MenuBar.
	Fnd_Menu_Initialized_b:=True:C214
End if 
