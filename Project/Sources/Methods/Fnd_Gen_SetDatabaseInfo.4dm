//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_SetDatabaseInfo (info type; value)

// Sets the specified database information value.

// Use the Fnd_Gen_GetDatabaseInfo to retrieve this information.

// Access Type: Shared

// Parameters:

//   $1 : Text : The info type label

//   $2 : Text : The info value

// Returns: Nothing

// Created by Dave Batton on Aug 27, 2003

// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(Fnd_Gen_SetDatabaseInfo;$1;$2)
End if 

var $1 : Text
var $2 : Text

var $label_t;$value_t : Text

$label_t:=$1
$value_t:=$2

Fnd_Gen_Init

Use (Storage:C1525.Fnd.Gen)
	Storage:C1525.Fnd.Gen[$label_t]:=$value_t
End use 
