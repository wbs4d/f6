//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_ClickOnGroupList

  // Called from the localization editor when a group item is clicked, or
  //   when a higher-level list is clicked.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 11, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

ARRAY TEXT:C222(<>Fnd_Loc_EditorCodes_at;0)

If (<>Fnd_Loc_EditorGroups_at=0)
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorGroupName_t;False:C215)
	<>Fnd_Loc_EditorGroupName_t:=""
	
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteGroupButton;False:C215)
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorAddCodeButton_i;False:C215)
	
Else 
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorGroupName_t;True:C214)
	<>Fnd_Loc_EditorGroupName_t:=<>Fnd_Loc_EditorGroups_at{<>Fnd_Loc_EditorGroups_at}
	HIGHLIGHT TEXT:C210(<>Fnd_Loc_EditorGroupName_t;1;MAXTEXTLENBEFOREV11:K35:3)
	
	COPY ARRAY:C226(<>Fnd_Loc_EditorGroupCodes_at{<>Fnd_Loc_EditorGroups_at};<>Fnd_Loc_EditorCodes_at)
	
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteGroupButton;True:C214)
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorAddCodeButton_i;True:C214)
End if 

<>Fnd_Loc_EditorCodes_at:=0
Fnd_Loc_Editor_ClickOnCodesList 
