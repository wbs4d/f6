//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_MoveOnScreen (->left; ->top; ->right; ->bottom; {window type})

// Modifies the window coordinates so the window will be opened completely onscreen.
// Alternate syntax #1: do not pass any parameters and Foundation will use the settings in the Fnd.Wnd object
// Alternate syntax #2: pass an object with the desired parameters

// Access Type: Shared

// Parameters:
//   $1 : Pointer : Pointer to the left side of the window
//   $2 : Pointer : Pointer to the top of the window
//   $3 : Pointer : Pointer to the right side of the window
//   $4 : Pointer : Pointer to the bottom of the window
//   $5 : Longint : The window type (optional)

//   $1 : Object : Object containing parameters
//   $1.left : Longint : Left side of the window
//   $1.top : Longint : Top of the window
//   $1.right : Longint : Right side of the window
//   $1.bottom : Longint : Bottom of the window
//   $1.windowType : Longint : The window type (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 6, 2003
// Modified by Dave Batton on Dec 6, 2005
//   This code is now skipped on Mac if there are multiple monitors.
// Modified on March 2, 2006 by Jack Delay
//   Setup handling for multi-screen Mac systems (see also Fnd_Wnd_Init & Compiler_Fnd_Wnd)
// Modified on Sept 2, 2006 by Jack Delay
//   Added a minor fix.
// ----------------------------------------------------
C_VARIANT:C1683($1)
C_POINTER:C301($2;$3;$4)
C_LONGINT:C283($5;$left_i;$top_i;$right_i;$bottom_i;$width_i;$height_i;$scrnMagin_i;$sideBorder_i;$topBorder_i)
C_LONGINT:C283($windowType_i;$screen_width_i;$screen_height_i;$menu_bar_height_i;$screenNum_i;$screen_left_origin;$screen_top_origin)
C_BOOLEAN:C305($onScreen_b)

Fnd_Wnd_Init

$scrnMagin_i:=1  // How many pixels should be left between the window and the screen's edge?

Case of 
	: (Count parameters:C259=0)
		$left_i:=Fnd.Wnd.Left
		$top_i:=Fnd.Wnd.Top
		$right_i:=Fnd.Wnd.Right
		$bottom_i:=Fnd.Wnd.Bottom
		If (Fnd.Wnd.windowType#Null:C1517)
			$windowType_i:=Fnd.Wnd.windowType
		Else 
			$windowType_i:=-1
		End if 
		
	: (Count parameters:C259=1)
		$left_i:=$1.Left
		$top_i:=$1.Top
		$right_i:=$1.Right
		$bottom_i:=$1.Bottom
		If ($1.windowType#Null:C1517)
			$windowType_i:=$1.windowType
		Else 
			$windowType_i:=-1
		End if 
		
	Else 
		$left_i:=$1->  // According to the 4D Compiler manual, this is faster than working directly
		$top_i:=$2->  // with the pointers.
		$right_i:=$3->
		$bottom_i:=$4->
		If (Count parameters:C259=5)
			$windowType_i:=$5
		Else 
			$windowType_i:=-1
		End if 
End case 

$width_i:=$right_i-$left_i
$height_i:=$bottom_i-$top_i

Case of 
	: (($windowType_i=-1)\
		 & (Is macOS:C1572))  // If nothing was specified on Mac OS X.
		$topBorder_i:=21
		$sideBorder_i:=0
		
	: ($windowType_i=-1)  // Handle the worst case on Windows
		$topBorder_i:=30
		$sideBorder_i:=8
		
	: (($windowType_i=Plain no zoom box window:K34:1)\
		 | ($windowType_i=Plain fixed size window:K34:6)\
		 | ($windowType_i=Plain window:K34:13)\
		 | ($windowType_i=Round corner window:K34:8))  // These window types have titles.
		If (Is Windows:C1573)
			$topBorder_i:=30  // DB050721 - Changed from 19.
			$sideBorder_i:=1
		Else 
			$topBorder_i:=19  // Mac
			$sideBorder_i:=0
		End if 
		
	: ($windowType_i=Modal dialog box:K34:2)
		$topBorder_i:=8
		$sideBorder_i:=8
		
	: ($windowType_i=Movable dialog box:K34:7)  //& (Is macOS))  // Moveable modal dialogs.
		If (Is Windows:C1573)
			$topBorder_i:=30  // DB050721 - Changed from 23.
			$sideBorder_i:=8
		Else 
			$topBorder_i:=21
			$sideBorder_i:=0
		End if 
		
	: ($windowType_i=Palette window:K34:3)
		$topBorder_i:=11
		$sideBorder_i:=-1  // Yes, really.
		
	: (Abs:C99($windowType_i)>=700)  // Floating palettes.
		$topBorder_i:=11
		$sideBorder_i:=1
		
	Else   // Most other window types don't need any extra room.
		$topBorder_i:=1
		$sideBorder_i:=1
End case 

If (Is macOS:C1572)
	// 060302 - Jack Delay.
	// If not windows need to consider multi-screen mac adjustment even if screen count is 1 since
	//   any system could have previously have had another screen and even simply rearranging screen 
	//   associated with a projector to the other side of the main monitor (i.e. adapting your screen 
	//   arrangment to a new presentation environment) can leave Mac 4D windows "lost in space."
	//   I know I've done it! - Jack
	$screenNum_i:=1  //always start with main screen (may be only screen)
	$onScreen_b:=False:C215  //and assume not on any screen as a default
	While ((Not:C34($onScreen_b)) & ($screenNum_i<=<>Fnd_Wnd_ScreenCount_i))  // If upper left corner is on any screen use it!
		If (($left_i>=<>Fnd_Wnd_ScreenMap_ai{1}{$screenNum_i}) & ($left_i<=<>Fnd_Wnd_ScreenMap_ai{3}{$screenNum_i}) & ($top_i>=<>Fnd_Wnd_ScreenMap_ai{2}{$screenNum_i}) & ($top_i<=<>Fnd_Wnd_ScreenMap_ai{4}{$screenNum_i}))  //all "segment" points on this screen?
			$onScreen_b:=True:C214  //OK escape loop & show its on a screen now
		Else 
			$screenNum_i:=$screenNum_i+1  //next (if exists) screen or escape while 
		End if 
	End while 
	If (Not:C34($onScreen_b))  // JD060914 - If upper left corner was not on any screen retry with upper right
		$screenNum_i:=1
		While ((Not:C34($onScreen_b)) & ($screenNum_i<=<>Fnd_Wnd_ScreenCount_i))  //if upper left corner was not on any screen, try the upper-right corner.
			If (($right_i>=<>Fnd_Wnd_ScreenMap_ai{1}{$screenNum_i}) & ($right_i<=<>Fnd_Wnd_ScreenMap_ai{3}{$screenNum_i}) & ($top_i>=<>Fnd_Wnd_ScreenMap_ai{2}{$screenNum_i}) & ($top_i<=<>Fnd_Wnd_ScreenMap_ai{4}{$screenNum_i}))  //all "segment" points on this screen?
				$onScreen_b:=True:C214  //OK escape loop & show its on a screen now
			Else 
				$screenNum_i:=$screenNum_i+1  //next (if exists) screen or escape while 
			End if 
		End while 
	End if   // JD060914
	If (Not:C34($onScreen_b))  //if neither corner was not on any screen simply place near upper left of main screen
		$screenNum_i:=1  //reset to main screen for screen environmental variables
		$left_i:=<>Fnd_Wnd_ScreenMap_ai{1}{1}+21  //since screen swaps a rare occasion and difficult to guess what user "would want"
		$top_i:=<>Fnd_Wnd_ScreenMap_ai{2}{1}+21
		$right_i:=$left_i+$width_i
		$bottom_i:=$top_i+$height_i
	End if 
	// now set up environmental values based on actual screen
	$screen_left_origin:=<>Fnd_Wnd_ScreenMap_ai{1}{$screenNum_i}  //for Dave's finesse with window type tweaking need to set origin on Mac's (0 on Wi  `ndoze)
	$screen_top_origin:=<>Fnd_Wnd_ScreenMap_ai{2}{$screenNum_i}  //ditto for top!!!
	$screen_width_i:=<>Fnd_Wnd_ScreenMap_ai{3}{$screenNum_i}-<>Fnd_Wnd_ScreenMap_ai{1}{$screenNum_i}  //right-left of actual screen in use
	$screen_height_i:=<>Fnd_Wnd_ScreenMap_ai{4}{$screenNum_i}-<>Fnd_Wnd_ScreenMap_ai{2}{$screenNum_i}  //bottom-top of actual screen in use
	If ($screenNum_i=1)  //if main screen on mac
		$menu_bar_height_i:=Menu bar height:C440
	Else 
		$menu_bar_height_i:=0  //otherwise no menus bar to contend with so leave where user had if can.
	End if 
	
Else   //Windows users can use previous Foundation direct assignment values
	$screen_left_origin:=0
	$screen_top_origin:=0
	$screen_width_i:=Screen width:C187
	$screen_height_i:=Screen height:C188
	$menu_bar_height_i:=Menu bar height:C440
End if   //special Mac multi-screen additions by Jack Delay

// Wayne Stewart, (2019-11-15) - Allow for Toolbar
$menu_bar_height_i:=$menu_bar_height_i+Fnd_Wnd_ToolbarHeight

// Do right before left and top before bottom so that if the window is larger than
// the screen, the top-left corner of the window will be visible.
If ($right_i>($screen_left_origin+$screen_width_i-$scrnMagin_i-$sideBorder_i))
	$right_i:=$screen_left_origin+$screen_width_i-$scrnMagin_i-$sideBorder_i
	$left_i:=$right_i-$width_i  //
End if 

If ($left_i<($screen_left_origin+$scrnMagin_i+$sideBorder_i))
	$left_i:=$screen_left_origin+$scrnMagin_i+$sideBorder_i
	$right_i:=$left_i+$width_i
End if 

If ($bottom_i>($screen_top_origin+$screen_height_i-$scrnMagin_i-$sideBorder_i))
	$bottom_i:=$screen_top_origin+$screen_height_i-$scrnMagin_i-$sideBorder_i
	$top_i:=$bottom_i-$height_i
End if 

If ($top_i<($screen_top_origin+$scrnMagin_i+$menu_bar_height_i+$topBorder_i))  // Adjust for the menu bar & tool bar too.
	$top_i:=$screen_top_origin+$scrnMagin_i+$menu_bar_height_i+$topBorder_i
	$bottom_i:=$top_i+$height_i
End if 


Case of 
	: (Count parameters:C259=0)
		Fnd.Wnd.Left:=$left_i
		Fnd.Wnd.Top:=$top_i
		Fnd.Wnd.Right:=$right_i
		Fnd.Wnd.Bottom:=$bottom_i
		
	: (Count parameters:C259=1)
		$1.Left:=$left_i
		$1.Top:=$top_i
		$1.Right:=$right_i
		$1.Bottom:=$bottom_i
		
	Else 
		$1->:=$left_i
		$2->:=$top_i
		$3->:=$right_i
		$4->:=$bottom_i
		
End case 

