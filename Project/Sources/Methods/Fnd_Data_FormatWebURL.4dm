//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatWebURL (url) --> Text

  // Formats the URL. 
  // The URL is not formatted if the user holds down the Option/Alt
  //   key (or other key that's been specified by the developer).

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The URL to format.

  // Returns: 
  //   $0 : Text : The formatted URL.

  // Created by Dave Batton on May 12, 2004
  // Modified by Dave Batton on Oct 1, 2004
  //   Fixed some dumb bugs. Now formats FTP and other non-http URLs properly.
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$url_t;$testStr_t)
C_LONGINT:C283($slashCount_i)

Fnd_Data_Init 
Fnd_Data_FormatError_i:=0

If (Not:C34(Fnd_Data_FormatBypass ))
	$url_t:=Replace string:C233($1;" ";"")  // Remove all spaces, just in case.
	
	If ($url_t#"")  // DB041001 - Removed old Option key check here.
		If ((Substring:C12($url_t;1;1)#"/") & (Position:C15(".";$url_t)=0))
			$url_t:="www."+$url_t+".com"
		End if 
		
		If ($url_t="ftp.@")
			$url_t:="ftp://"+$url_t
		End if 
		
		If (($url_t#"@://@") & (Substring:C12($url_t;1;1)#"/"))  // DB041001 - Fixed
			$url_t:="http://"+$url_t
		End if 
		
		$testStr_t:=Replace string:C233($url_t;"/";"")
		$slashCount_i:=Length:C16($url_t)-Length:C16($testStr_t)
		
		  // If it's just a domain name without a specific page, make sure it ends
		  //   with a slash.
		If (($url_t="http://@") & ($slashCount_i<3))
			$url_t:=$url_t+"/"
		End if 
	End if 
End if 

$0:=$url_t