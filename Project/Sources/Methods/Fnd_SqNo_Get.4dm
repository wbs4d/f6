//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Get (sequence number group name) --> Number

  // Returns the next available sequence number for the specified group. If the group
  //   doesn't already exist, it is created and 1 is returned as the first number.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The group name

  // Returns: 
  //   $0 : Longint : The next number in the sequence

  // Created by Dave Batton on Nov 5, 2003
  // Modified by Dave Batton on Jan 29, 2004
  //   If this method is running on 4D Server, it now uses a Distributor
  //   process with a different name than the ones used by the clients.
  // Modified by Dave Batton on May 20, 2004
  //   Modified the name of the process created on 4D Server. It was too long.
  //   Changed it from "Fnd_SqNo_Distributor svr process" to "Fnd_SqNo_Distributor server".
  // Modified by Dave Batton on Sep 24, 2007
  //   Changed the $seqNoName_t variable name to $groupName_t.
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_LONGINT:C283($0;$processNumber_i;$giveUpTime_i;$newSeqNo_i)

$groupName_t:=$1

$newSeqNo_i:=0  // Our default value.

Fnd_SqNo_Init 

If (Trigger level:C398>0)
	  // If we're in a trigger, get the number directly, not using the Fnd_SqNo_Distributor 
	  //   process.
	$newSeqNo_i:=Fnd_SqNo_Get2 ($groupName_t)
	
Else 
	If (Not:C34(Semaphore:C143(<>Fnd_SqNo_Semaphore_t;300)))  // Wait up to 5 seconds.
		
		<>Fnd_SqNo_Name_t:=$groupName_t
		<>Fnd_SqNo_Number_i:=-1
		
		  // Start the Fnd_SqNo_Distributor process if it's not already running.
		  // DB040129 - We must use a different name if we're running this code on
		  //   4D Server, since all of the clients that are logged in will have a matching 
		  //   process running on 4D Server named "Fnd_SqNo_Distributor process".  By 
		  //   using a different name for the server, we won't get confused by these others.
		If (Application type:C494=4D Server:K5:6)
			$processNumber_i:=New process:C317("Fnd_SqNo_Distributor";Fnd_Gen_DefaultStackSize;"Fnd_SqNo_Distributor server";*)  // DB040520
		Else 
			$processNumber_i:=New process:C317("Fnd_SqNo_Distributor";Fnd_Gen_DefaultStackSize;"Fnd_SqNo_Distributor process";*)
		End if 
		
		If ($processNumber_i=0)
			Fnd_Gen_BugAlert (Current method name:C684;"Unable to start the sequence number distributor process.")
			
		Else 
			RESUME PROCESS:C320($processNumber_i)  // If it's already running, it may be paused. Wake it.
			
			  // Now wait for the Fnd_SqNo_Distributor process to modify the value of <>Fnd_SqNo_Number_i
			$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
			Repeat 
				DELAY PROCESS:C323(Current process:C322;1)  // We're in a hurry. Don't wait long.
			Until ((<>Fnd_SqNo_Number_i>=0) | (Tickcount:C458>$giveUpTime_i))
			
			  // Get the new sequence number.
			$newSeqNo_i:=<>Fnd_SqNo_Number_i
			
			  // Tell the SeqNoDistributor process we're done with it - we have our value.
			<>Fnd_SqNo_Number_i:=-2
			
			  // Wait for confirmation that the SeqNoDistributor process got our message before 
			  //   releasing the semaphore.
			$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
			While ((<>Fnd_SqNo_Number_i#-1) & (Tickcount:C458<$giveUpTime_i))
				DELAY PROCESS:C323(Current process:C322;1)
			End while 
		End if 
		
		CLEAR SEMAPHORE:C144(<>Fnd_SqNo_Semaphore_t)
		
		  // We might have a value of -1 if the SeqNoDistributor process never replied.  
		  //   Just change it to 0 to be consistent.
		If ($newSeqNo_i<0)
			$newSeqNo_i:=0
		End if 
	End if 
	
End if 

$0:=$newSeqNo_i