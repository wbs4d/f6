//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_XML_FilePath --> Text

// Returns the path to the Fnd_List.xml file.

// Access: Private

// Parameters: None

// Returns: 
//   $0 : Text : The file path

// Created by Dave Batton on Oct 5, 2007
// Modified by: Walt Nelson (2/19/10) added * to put the list in the resource folder next to the host
// Mod by Wayne Stewart, (2021-04-14) - Move to new storage location in prefs area
// ----------------------------------------------------

C_TEXT:C284($0)

$0:=Fnd_Gen_GetDatabaseInfo("PrefsFolder")+"Fnd_List.xml"  // Modified by: Walt Nelson (2/19/10) - added parameter *

