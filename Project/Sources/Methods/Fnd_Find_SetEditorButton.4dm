//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_SetEditorButton (label{; method})

// Allows the developer to hide or modify the Query Editor button.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The new label
//   $2 : Text : The method to run (optional)

// Returns: Nothing

// Created by Dave Batton on Dec 6, 2003
// ----------------------------------------------------

C_TEXT:C284($1;$2)

Fnd_Find_Init

Fnd_Find_EditorButtonLabel_t:=$1

If (Count parameters:C259>=2)
	Fnd_Find_EditorButtonMethod_t:=$2
Else 
	Fnd_Find_EditorButtonMethod_t:=""
End if 
