//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Platform ({platform name}) --> Text

// Sets and gets the toolbar platform name.
// Can be:
//   "Win"
//   "Mac"
//   "Auto"
// Only "Mac" or "Win" will be returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The platform to use (optional)

// Returns: 
//   $0 : Text : The selected platform

// Created by Dave Batton on Jan 16, 2005
// ----------------------------------------------------

C_TEXT:C284($0;$1;$requestedPlatform_t)

Fnd_Tlbr_Init

If (Count parameters:C259>=1)
	$requestedPlatform_t:=$1
	
	Case of 
		: ($requestedPlatform_t="Auto")
			If (Is Windows:C1573)
				Fnd_Tlbr_Platform_t:="Win"
			Else 
				Fnd_Tlbr_Platform_t:="Mac"
			End if 
		: ($requestedPlatform_t="Mac")
			Fnd_Tlbr_Platform_t:=$requestedPlatform_t
		: ($requestedPlatform_t="Win")
			Fnd_Tlbr_Platform_t:=$requestedPlatform_t
		Else 
			Fnd_Gen_BugAlert(Current method name:C684;"Invalid platform: "+$requestedPlatform_t)
	End case 
	
	Fnd_Tlbr_SetVariables
	
	Fnd_Tlbr_BackgroundIsValid_b:=False:C215  // Force the toolbar background to redraw.
End if 

$0:=Fnd_Tlbr_Platform_t
