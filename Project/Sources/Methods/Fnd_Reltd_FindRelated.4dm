//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_FindRelated

// Looks at the relationship arrays (<>Fnd_Reltd_Many_ap & <>Fnd_Reltd_One_ap) generated from Fnd_Reltd_Init, and
// generates and displays a list of tables that are related to the current tablel, for which the current user has access.
// It then uses Relate Many Selection and/or Relate One Selection to retieve the desired result.

// Access: Private

// Created by Doug Hall (20030317)

// ----------------------------------------------------



Fnd_Reltd_Init  // Make certain the arrays are declared

var $Arr1Size_i; $Arr2Size_i; $i; $j; $KeyFieldTableNum_i; $relatedTableNum_i; $crntTableNum_i; $element_i; $arraysize_i : Integer
var $NIL_ptr; $RelatedFld_ptr; $KeyField_ptr; $OneTable_ptr; $RelatedTable_ptr; $IntersectTable_ptr; $ForeignKey_ptr : Pointer
var $crntTableName_t : Text


ARRAY TEXT:C222($Reltd_Choice_at; 0)
ARRAY POINTER:C280($Reltd_RelMnyFld_aptr; 0)
ARRAY POINTER:C280($Reltd_RelOneFld_aptr; 0)

$crntTableNum_i:=Table:C252(Fnd_Gen_CurrentTable)

$Arr1Size_i:=Size of array:C274(<>Fnd_Reltd_Many_aptr{$crntTableNum_i})
For ($i; 1; $Arr1Size_i)  // For Related One Files (Use Join)
	$RelatedFld_ptr:=<>Fnd_Reltd_Many_aptr{$crntTableNum_i}{$i}
	$relatedTableNum_i:=Table:C252($RelatedFld_ptr)
	$RelatedTable_ptr:=Table:C252($relatedTableNum_i)
	If (Fnd_Tbl_UserCanAccess($RelatedTable_ptr) & Fnd_Gen_TableIsVisible($RelatedTable_ptr))
		APPEND TO ARRAY:C911($Reltd_Choice_at; Fnd_VS_TableName($RelatedTable_ptr))
		APPEND TO ARRAY:C911($Reltd_RelMnyFld_aptr; $RelatedFld_ptr)
		APPEND TO ARRAY:C911($Reltd_RelOneFld_aptr; $NIL_ptr)
	End if 
	// For each one of the related many tables, find their related one tables, excluding our current table.
	$Arr2Size_i:=Size of array:C274(<>Fnd_Reltd_One_aptr{$relatedTableNum_i})
	If ($Arr2Size_i>0)  // If this is a Related Many table, let's join intersect tables.
		For ($j; 1; $Arr2Size_i)
			$KeyField_ptr:=<>Fnd_Reltd_One_aptr{$relatedTableNum_i}{$j}
			$KeyFieldTableNum_i:=Table:C252($KeyField_ptr)
			If ($KeyFieldTableNum_i#$crntTableNum_i)
				$OneTable_ptr:=Table:C252($KeyFieldTableNum_i)
				If (Fnd_Tbl_UserCanAccess($OneTable_ptr) & Fnd_Gen_TableIsVisible($OneTable_ptr))
					APPEND TO ARRAY:C911($Reltd_Choice_at; Fnd_VS_TableName($OneTable_ptr)+" (via "+Fnd_VS_TableName($RelatedTable_ptr)+")")
					APPEND TO ARRAY:C911($Reltd_RelMnyFld_aptr; $RelatedFld_ptr)
					APPEND TO ARRAY:C911($Reltd_RelOneFld_aptr; $KeyField_ptr)
				End if 
			End if 
		End for 
	End if 
End for 
$Arr1Size_i:=Size of array:C274(<>Fnd_Reltd_One_aptr{$crntTableNum_i})
For ($i; 1; $Arr1Size_i)
	$KeyField_ptr:=<>Fnd_Reltd_One_aptr{$crntTableNum_i}{$i}
	$KeyFieldTableNum_i:=Table:C252($KeyField_ptr)
	$OneTable_ptr:=Table:C252($KeyFieldTableNum_i)
	If (Fnd_Tbl_UserCanAccess($OneTable_ptr) & Fnd_Gen_TableIsVisible($OneTable_ptr))
		APPEND TO ARRAY:C911($Reltd_Choice_at; Fnd_VS_TableName($OneTable_ptr))
		APPEND TO ARRAY:C911($Reltd_RelMnyFld_aptr; $NIL_ptr)
		APPEND TO ARRAY:C911($Reltd_RelOneFld_aptr; $KeyField_ptr)
	End if 
End for 
SORT ARRAY:C229($Reltd_Choice_at; $Reltd_RelMnyFld_aptr; $Reltd_RelOneFld_aptr; >)

$crntTableName_t:=Fnd_VS_TableName(Fnd_Gen_CurrentTable)

MESSAGES OFF:C175
$arraysize_i:=Size of array:C274($Reltd_Choice_at)
If ($arraysize_i>0)
	OK:=1
	Fnd_Wnd_Title("Find Related…")
	Fnd_Wnd_Position(Fnd_Wnd_CenterOnWindow)
	$element_i:=Fnd_List_ChoiceList("Find related records in…"; ->$Reltd_Choice_at)
	If (OK=1)
		Case of 
			: (Not:C34(Is nil pointer:C315($Reltd_RelOneFld_aptr{$element_i})) & Not:C34(Is nil pointer:C315($Reltd_RelMnyFld_aptr{$element_i})))  // A Many<->Many relationship
				$IntersectTable_ptr:=Fnd_Gen_TableOf($Reltd_RelMnyFld_aptr{$element_i})
				$RelatedTable_ptr:=Fnd_Gen_TableOf($Reltd_RelOneFld_aptr{$element_i})  // Make a Table pointer from the Related field.
				$ForeignKey_ptr:=$Reltd_RelMnyFld_aptr{$element_i}
				Fnd_Dlg_Message("Finding all "+Fnd_VS_TableName($RelatedTable_ptr)+" records related to this selection of "+$crntTableName_t+" records.")
				RELATE MANY SELECTION:C340($ForeignKey_ptr->)
				RELATE ONE SELECTION:C349($IntersectTable_ptr->; $RelatedTable_ptr->)
				
			: (Not:C34(Is nil pointer:C315($Reltd_RelOneFld_aptr{$element_i})))  // Do a Relate One Selection (Many->One)
				$RelatedTable_ptr:=Fnd_Gen_TableOf($Reltd_RelOneFld_aptr{$element_i})  // Make a Table pointer from the Related field.
				Fnd_Dlg_Message("Finding all "+Fnd_VS_TableName($RelatedTable_ptr)+" records related to this selection of "+$crntTableName_t+" records.")
				RELATE ONE SELECTION:C349(Fnd_Gen_CurrentTable->; $RelatedTable_ptr->)
				
			Else   // Do a Relate Many Selection (One->Many)
				$ForeignKey_ptr:=$Reltd_RelMnyFld_aptr{$element_i}
				$RelatedTable_ptr:=Fnd_Gen_TableOf($ForeignKey_ptr)  // Make a Table pointer from the Related field.
				Fnd_Dlg_Message("Finding all "+Fnd_VS_TableName($RelatedTable_ptr)+" records related to this selection of "+$crntTableName_t+" records.")
				RELATE MANY SELECTION:C340($ForeignKey_ptr->)
				
		End case 
		Fnd_Dlg_MessageClose
		// Now we have the related selection loaded (if any). Display the table or alert no records.
		Fnd_Reltd_ShowSet($RelatedTable_ptr)
	End if 
Else 
	Fnd_Wnd_Position(Fnd_Wnd_CenterOnWindow)
	Fnd_Dlg_Alert("There are no tables related to the "+$crntTableName_t+" table.")
End if 
MESSAGES ON:C181