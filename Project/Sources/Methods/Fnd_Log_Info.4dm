//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo method
//   for more information.

// Access: Shared

// Parameters: 
//   $1 : Text : Info desired ("version", "name", or "logging")

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on Dec 27, 2004
// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="version")
		$reply_t:="6.0.1"
		
	: ($request_t="name")
		$reply_t:="Foundation Logging"
		
	: ($request_t="logging")
		Fnd_Log_Init
		If (Fnd.Log.Enabled)
			$reply_t:="enabled"
		Else 
			$reply_t:="disabled"
		End if 
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
