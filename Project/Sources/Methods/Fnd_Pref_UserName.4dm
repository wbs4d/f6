//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_UserName ({name}) --> Text

// Gets and sets the current user name for the preferences system.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The new user name (optional)

// Returns: 
//   $0 : Text : The current user name

// Created by Dave Batton on Feb 6, 2006
// Modified by Gary Boudreaux on Dec 22, 2008
//   Enhanced parameter description to reflect optional aspect in header info
// ----------------------------------------------------

C_TEXT:C284($0;$1)
C_BOOLEAN:C305(<>Fnd_Pref_Initialized_b)

// Note that we set this value before calling Fnd_Pref_Init. This is not how we
//   normally do things, but it works here because we have special handling
//   in Fnd_Pref_Init to make this work.
If (Count parameters:C259>=1)
	<>Fnd_Pref_UserName_t:=$1
	
	If (<>Fnd_Pref_Initialized_b)
		Fnd_Pref_LoadPrefs  // We need to redo this.
	Else 
		Fnd_Pref_Init  // We need to do everything.
	End if 
	
Else 
	Fnd_Pref_Init
End if 

$0:=<>Fnd_Pref_UserName_t