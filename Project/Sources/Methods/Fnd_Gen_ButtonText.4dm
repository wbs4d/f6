//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ButtonText (->button; text; alignment{; ->obj1..->objN}) --> Number

// A replacement for 4D's BUTTON TEXT command.
// Sets the button text and resizes the button if necessary. Also moves any
//   adjacent form objects so they stay properly aligned with the modified button.

// Due to a bug in 4D 2003.1, this routine won't work properly if the
//   primary button is set as the default button.  If this is a problem, UPGRADE!

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the button
//   $2 : Text : New button text
//   $3 : Longint : Which end to keep stationary (optional)
//      2 or Align Left -  Keep left side anchored
//      3 or Center - Center the button on its original position
//      4 or Alight Right - Keep right side anchored
//   $4..$N : Pointer : Other form objects to move

// Returns: 
//   $0 : Longint : The increase in the button size

// Created by Dave Batton on Jul 11, 2003
// Modified by Dave Batton on Apr 13, 2004
//   We now test for the 4D version so we don't apply a workaround to the 
//   new version of 4D that doesn't need the workaround.
// Modified by Dave Batton on May 23, 2004
//   Moved this method from the General component to the Localization
//   component and changed it from Private to Protected.
//   Removed the optional 4th parameter for 2003.1 compatibility.
// Modified by Dave Batton on Aug 2, 2004
//   Parameters 4-N are now used to pass pointers to other form objects
//   that should be moved if the button is resized. Deleted the Fnd_Loc
//   version of this routine.
// Mod by Wayne Stewart, (2019-11-13) - Allow for buttons with different names
// ----------------------------------------------------

C_LONGINT:C283($0; $3; $alignment_i; $ValueType_i; $left_i; $top_i; $right_i; $bottom_i; $newWidth_i; $originalWidth_i; $diffWidth_i; $height_i; $move_i; $tableNum_i; $fieldNum_i; $parameter_i)
C_VARIANT:C1683($1; $4; $5; $6; $7)
C_POINTER:C301($button_ptr)
C_TEXT:C284($2; $text_t; $varName_t)
var $buttonName_t : Text

$ValueType_i:=Value type:C1509($1)  //$vt="Is text"
If (Caps lock down:C547)
	TRACE:C157
End if 

Case of 
	: ($ValueType_i=Is pointer:K8:14)
		$button_ptr:=$1
		
	: ($ValueType_i=Is text:K8:3)
		$buttonName_t:=$1
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "An unexpected variable type was passed to this method: "+String:C10($ValueType_i))
End case 

$text_t:=$2
$alignment_i:=$3

Case of 
	: ($ValueType_i=Is pointer:K8:14)
		OBJECT SET TITLE:C194($button_ptr->; $text_t)
		OBJECT GET COORDINATES:C663($button_ptr->; $left_i; $top_i; $right_i; $bottom_i)
		$originalWidth_i:=$right_i-$left_i
		OBJECT GET BEST SIZE:C717($button_ptr->; $newWidth_i; $height_i)
		
		// 4D doesn't size everything perfectly.  Make some corrections based on the type of object.
		RESOLVE POINTER:C394($button_ptr; $varName_t; $tableNum_i; $fieldNum_i)
		
	: ($ValueType_i=Is text:K8:3)
		OBJECT SET TITLE:C194(*; $buttonName_t; $text_t)
		OBJECT GET COORDINATES:C663(*; $buttonName_t; $left_i; $top_i; $right_i; $bottom_i)
		$originalWidth_i:=$right_i-$left_i
		OBJECT GET BEST SIZE:C717(*; $buttonName_t; $newWidth_i; $height_i)
		$varName_t:=$buttonName_t
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "An unexpected variable type was passed to this method: "+String:C10($ValueType_i))
End case 



Case of 
	: (Is Windows:C1573)
		Case of 
			: ($varName_t="@Button_i") | ($varName_t="@Button")  // Standard buttons
				$newWidth_i:=$newWidth_i+12
				If ($newWidth_i<$originalWidth_i)  // Treat the original width as the minimum width.
					$newWidth_i:=$originalWidth_i
				End if 
		End case 
		
	Else   // Mac
		Case of 
			: ($varName_t="@RadioButton_i") | ($varName_t="@RadioButton")
				$newWidth_i:=$newWidth_i+2  // added 2 pixels here wwn
				
			: ($varName_t="@Button_i") | ($varName_t="@Button")  // Standard buttons
				$newWidth_i:=$newWidth_i+4  //
				If ($newWidth_i<$originalWidth_i)  // Treat the original width as the minimum width.
					$newWidth_i:=$originalWidth_i
				End if 
		End case 
End case 

$diffWidth_i:=$newWidth_i-$originalWidth_i
If ($diffWidth_i#0)
	Case of 
		: ($alignment_i=Align left:K42:2)
			OBJECT MOVE:C664(*; $varName_t; 0; 0; $diffWidth_i; 0)
			$move_i:=$diffWidth_i
		: ($alignment_i=Align center:K42:3)
			$move_i:=0-($diffWidth_i/2)
			OBJECT MOVE:C664(*; $varName_t; $move_i; 0; $diffWidth_i; 0)
		: ($alignment_i=Align right:K42:4)
			$move_i:=0-$diffWidth_i
			OBJECT MOVE:C664(*; $varName_t; $move_i; 0; $diffWidth_i; 0)
	End case 
End if 

If ($move_i#0)
	For ($parameter_i; 4; Count parameters:C259)  // ### - Ugly work-around until 4D v11 SQL gets fixed.
		// MOVE OBJECT(${$parameter_i}->;$move_i;0)  ` Old code that should work but doesn't.
		
		$ValueType_i:=Value type:C1509(${$parameter_i})
		
		Case of 
			: ($ValueType_i=Is pointer:K8:14)
				$button_ptr:=${$parameter_i}
				OBJECT MOVE:C664($button_ptr->; $move_i; 0)
				
			: ($ValueType_i=Is text:K8:3)
				$buttonName_t:=${$parameter_i}
				OBJECT MOVE:C664(*; $buttonName_t; $move_i; 0)
				
				//$button_ptr:=OBJECT Get pointer(Object named; ${$parameter_i})
				
			Else 
				Fnd_Gen_BugAlert(Current method name:C684; "An unexpected variable type was passed to this method: "+String:C10($ValueType_i))
		End case 
		
	End for 
End if 

$0:=$move_i
