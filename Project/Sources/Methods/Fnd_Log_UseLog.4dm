//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_UseLog {(Log Label)}

// Pass the log label to use,
// if you call this without a parameter, then the default log will be used

// Access: Shared

// Parameters: 
//   $1 : Text : The Log to use or nothing to return to default


// Created by Wayne Stewart (2018-08-05)
//     waynestewart@mac.com
// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(Fnd_Log_UseLog; $1)
End if 

C_TEXT:C284($1)
C_OBJECT:C1216($Temp_o)


If (Count parameters:C259=1)
	Fnd_Log_DeclareLog($1)  // This will do no harm if the log has already been declared
	
	$Temp_o:=Storage:C1525.Fnd.Logs[$1]
	Fnd.Log.file:=$Temp_o.fileName
	Fnd.Log.folder:=$Temp_o.folderPath
	
Else 
	Fnd.Log.file:=Storage:C1525.Fnd.Log.defaultLog
	Fnd.Log.folder:=Storage:C1525.Fnd.Log.logsFolder
	
End if 