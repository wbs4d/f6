//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_UpdateButtons

// Redraws the toolbar, but only if it needs it.  If no code has been called which
//   effects the toolbar, then there is no reason to redraw it.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Mark Mitchenall on 7/2/02
// Modified by Dave Batton on May 15, 2004
//   Added some code to resize the background objects when the window is resized.
// Modified by Dave Batton on Mar 7, 2005
//   I was passing the button type as the third parameter to two routines that
//   didn't expect a third parameter. This has been fixed.
// Modified by Dave Batton on May 10, 2005
//   Removed the references to the unused references to the Fnd_Tlbr_ObjectStates_at array.
// Modified by Dave Batton on Nov 18, 2005
//   Added support for menu buttons.
// Modified by Dave Batton on Feb 18, 2006
//   Removed two unused parameters.
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_LONGINT:C283($FieldNumber_i;$leftOffset_i;$numActualButtons_i;$objectNumber_i;$TableNumber_i;$toolbarWidth_i;$windowBottom_i;$windowLeft_i;$windowRight_i;$windowTop_i)
C_LONGINT:C283($windowWidth_i)
C_PICTURE:C286($Picture_pic)
C_TEXT:C284($IconName_t;$Label_t;$oldButtonPlatform_t;$oldButtonStyle_t;$PointerVariableName_t;$Variation_t)

ARRAY LONGINT:C221($objectSizes_ai;0)
ARRAY LONGINT:C221($objectWidths_ai;0)
ARRAY LONGINT:C221($topOffsets_ai;0)
ARRAY TEXT:C222($objectVariations_at;0)


If (Not:C34(Fnd_Tlbr_ButtonsAreValid_b))
	Fnd_Tlbr_ButtonsAreValid_b:=True:C214
	
	// Hide all of the buttons.
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_PictureButton@";False:C215)
	
	// Hide the menu related stuff.
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_InvisibleButton@";False:C215)
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_MenuIndicator@";False:C215)
	
	// Hide all of the shortcut buttons and move them offscreen.
	//SET VISIBLE(*;"Fnd_Tlbr_Shortcut@";False)
	OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_Shortcut_@";False:C215)
	OBJECT MOVE:C664(*;"Fnd_Tlbr_Shortcut_@";-1;-1;-1;-1;*)
	
	ARRAY LONGINT:C221($objectNumbers_ai;Fnd_Tlbr_UsedObjects_i)
	ARRAY TEXT:C222($objectVariations_at;Fnd_Tlbr_UsedObjects_i)
	ARRAY LONGINT:C221($objectWidths_ai;Fnd_Tlbr_UsedObjects_i)
	ARRAY LONGINT:C221($objectSizes_ai;Fnd_Tlbr_UsedObjects_i)
	ARRAY LONGINT:C221($topOffsets_ai;Fnd_Tlbr_UsedObjects_i)
	
	$toolbarWidth_i:=0
	
	//determine the width of each button and the total width of the toolbar
	$numActualButtons_i:=0
	
	// Set the style for the drawing we're about to do.
	$oldButtonStyle_t:=Fnd_Bttn_Style
	Fnd_Bttn_Style(Fnd_Tlbr_Style_t)
	
	$oldButtonPlatform_t:=Fnd_Bttn_Platform
	Fnd_Bttn_Platform(Fnd_Tlbr_Platform_t)
	
	//*************************************************
	//* DETERMINE BUTTON WIDTHS AND MAXIMUM WIDTH OF TOOLBAR
	//*************************************************
	For ($objectNumber_i;1;Fnd_Tlbr_UsedObjects_i)
		
		Case of 
			: (Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}=4)  //it's a line
				Fnd_Tlbr_Pictures_aptr{$objectNumber_i}->:=Fnd_Tlbr_Divider_GetPicture(Fnd_Tlbr_DividerImageName_t)  // DB060218
				$objectWidths_ai{$objectNumber_i}:=1
				$toolbarWidth_i:=$toolbarWidth_i+Fnd_Tlbr_DividerPadding_i+$objectWidths_ai{$objectNumber_i}+Fnd_Tlbr_DividerPadding_i
				$topOffsets_ai{$objectNumber_i}:=Fnd_Tlbr_DividerTopOffset_i
				$objectVariations_at{$objectNumber_i}:=""
				
			Else   // It's a button.
				$numActualButtons_i:=$numActualButtons_i+1
				Case of   // DB051118 - The whole Case statement is new.
					: ((Fnd_Tlbr_Style_t="prefs") & (Fnd_Tlbr_ObjectNames_at{$objectNumber_i}=Fnd_Tlbr_CurrentButton_t))
						$objectVariations_at{$objectNumber_i}:="current"
					: ((Fnd_Tlbr_Style_t#"prefs") & (Size of array:C274(Fnd_Tlbr_ObjectMenuItems_at{$objectNumber_i})>0))
						$objectVariations_at{$objectNumber_i}:="menu"
					Else 
						$objectVariations_at{$objectNumber_i}:=""
				End case 
				$objectWidths_ai{$objectNumber_i}:=Fnd_Bttn_GetPictureWidth(Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i};Fnd_Tlbr_ObjectLabels_at{$objectNumber_i};$objectVariations_at{$objectNumber_i})  // DB051118
				$objectNumbers_ai{$numActualButtons_i}:=$objectNumber_i
				$objectSizes_ai{$objectNumber_i}:=0
				$toolbarWidth_i:=$toolbarWidth_i+$objectWidths_ai{$objectNumber_i}+Fnd_Tlbr_ButtonPadding_i
				
				// Fnd_Log_Enable(True)
				
				$IconName_t:=Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i}
				$Label_t:=Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}
				$Variation_t:=$objectVariations_at{$objectNumber_i}
				
				$Picture_pic:=Fnd_Bttn_GetPicture($IconName_t;$Label_t;$Variation_t)  // DB051118
				//Fnd_Log_AddEntry ("Fnd_Bttn_GetPicture";"Object: "+String($objectNumber_i);"Icon: "+$IconName_t;"Label: "+$Label_t;"Variation: "+$Variation_t;"Picture: "+String(Picture size($Picture_pic)))\
															
				
				
				
				
				
				If (Is nil pointer:C315(Fnd_Tlbr_Pictures_aptr{$objectNumber_i}))
					$PointerVariableName_t:="Nil"
				Else 
					RESOLVE POINTER:C394(Fnd_Tlbr_Pictures_aptr{$objectNumber_i};$PointerVariableName_t;$TableNumber_i;$FieldNumber_i)
				End if 
				
				//Fnd_Log_AddEntry ("Fnd_Tlbr_Pictures_aptr{$objectNumber_i}->:=$Picture_pic";"Object: "+String($objectNumber_i);"Pointer: "+$PointerVariableName_t)\
															
				
				
				Fnd_Tlbr_Pictures_aptr{$objectNumber_i}->:=$Picture_pic
				
				$topOffsets_ai{$objectNumber_i}:=Fnd_Tlbr_ButtonTopOffset_i
		End case 
	End for 
	
	ARRAY LONGINT:C221($objectNumbers_ai;$numActualButtons_i)
	
	//Get the width of the window and compensate for a scrollbar
	GET WINDOW RECT:C443($windowLeft_i;$windowTop_i;$windowRight_i;$windowBottom_i)
	$windowWidth_i:=$windowRight_i-$windowLeft_i
	$windowWidth_i:=$windowWidth_i-7  //space for the scrollbar
	
	//*************************************************
	//* ENSURE TOOLBAR FITS WINDOW SIZE
	//*************************************************
	//Now we have to find out if the toolbar is wider than the size of the window, and
	//if it is, we need to resize some of the buttons so that the toolbar will fit
	//nicely.
	If (Fnd_Tlbr_Style_t="Small@")  // The only way we put labels beside the icon.
		For ($objectNumber_i;Fnd_Tlbr_UsedObjects_i;1;-1)
			If ($toolbarWidth_i>$windowWidth_i)  // We're still too wide.
				If (Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}=0)  // It's a button.
					If (Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i}#"")  // It has an icon.
						If (Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}#"")  // It has text.
							Fnd_Tlbr_Pictures_aptr{$objectNumber_i}->:=Fnd_Bttn_GetPicture(Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i};"";$objectVariations_at{$objectNumber_i})  // DB051118
							$toolbarWidth_i:=$toolbarWidth_i-$objectWidths_ai{$objectNumber_i}
							$objectWidths_ai{$objectNumber_i}:=Fnd_Bttn_GetPictureWidth(Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i};"";$objectVariations_at{$objectNumber_i})  // DB051118
							$toolbarWidth_i:=$toolbarWidth_i+$objectWidths_ai{$objectNumber_i}
						End if 
					End if 
				End if 
			End if 
		End for 
	End if 
	
	//*************************************************
	//* DRAW THE TOOLBAR BUTTONS AT THE DETERMINED SIZES
	//*************************************************
	
	$leftOffset_i:=Fnd_Tlbr_ButtonLeftOffset_i
	
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_PrefHighlight";False:C215)
	
	For ($objectNumber_i;1;Fnd_Tlbr_UsedObjects_i)
		Case of 
			: (Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}=4)  //it's a divider line
				$leftOffset_i:=$leftOffset_i-Fnd_Tlbr_ButtonPadding_i+Fnd_Tlbr_DividerPadding_i  // Swap the last padding we added for the divider padding.
				Fnd_Tlbr_Object_Draw($objectNumber_i;$objectVariations_at{$objectNumber_i};$leftOffset_i;$topOffsets_ai{$objectNumber_i})
				$leftOffset_i:=$leftOffset_i+$objectWidths_ai{$objectNumber_i}+Fnd_Tlbr_DividerPadding_i
				
			: ($objectSizes_ai{$objectNumber_i}=1)  // Draw a small button.
				Fnd_Tlbr_Object_Draw($objectNumber_i;$objectVariations_at{$objectNumber_i};$leftOffset_i;$topOffsets_ai{$objectNumber_i})
				$leftOffset_i:=$leftOffset_i+$objectWidths_ai{$objectNumber_i}+Fnd_Tlbr_ButtonPadding_i
				
			Else 
				Fnd_Tlbr_Object_Draw($objectNumber_i;$objectVariations_at{$objectNumber_i};$leftOffset_i;$topOffsets_ai{$objectNumber_i})
				$leftOffset_i:=$leftOffset_i+$objectWidths_ai{$objectNumber_i}+Fnd_Tlbr_ButtonPadding_i
		End case 
		
	End for 
	
	Fnd_Bttn_Style($oldButtonStyle_t)
	Fnd_Bttn_Platform($oldButtonPlatform_t)
End if 