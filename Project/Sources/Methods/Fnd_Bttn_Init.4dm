//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_Init

// Initializes the toolbar roll-over variables.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 29, 2003
// Modified by Dave Batton on Jun 25, 2004
//   Updated to version 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Bttn_Info method.
// [1] Added by: John Craig (12/10/09) - to use external image library
// Mod by Wayne Stewart, (2021-04-04) - Remove picture export routines
// ----------------------------------------------------

Fnd_Init  // All Init methods must include this method

C_BOOLEAN:C305(<>Fnd_Bttn_Initialized_b; Fnd_Bttn_Initialized_b)

If (Not:C34(<>Fnd_Bttn_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Bttn
	
	//Set the size of the picture cache.  This really depends on how much 
	//available memory you've got in 4D or 4D Client.
	<>Fnd_Bttn_CacheMaximumSize_i:=2048*1024
	Fnd_Bttn_Cache_Startup
	
	//[1] Added by: John Craig (12/10/09) ->
	C_PICTURE:C286($picture_pic)
	C_TEXT:C284($path_t; $separator_t)
	C_LONGINT:C283($count_i)
	
	ARRAY LONGINT:C221($picRef_ai; 0)
	ARRAY TEXT:C222($picName_at; 0)
	
	$separator_t:=Folder separator:K24:12  // Mod by Wayne Stewart, (2021-04-05) - use constant
	$path_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Fnd_Bttn_Pictures"  //  This is the Foundation component resource folder
	
	<>Fnd_Bttn_PicturePath_t:=$path_t+$separator_t  //[1] Added by: John Craig (12/10/09) <-
	
	<>Fnd_Bttn_Initialized_b:=True:C214
End if 

If (Not:C34(Fnd_Bttn_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Bttn
	
	Fnd_Bttn_Style_t:="Large"
	
	//arrays to hold the position offsets of the different buttons in the rollover
	ARRAY INTEGER:C220(Fnd_Bttn_HorizonalShift_ai; 4)
	ARRAY INTEGER:C220(Fnd_Bttn_VerticalShift_ai; 4)
	
	Fnd_Bttn_Initialized_b:=True:C214
	
	Fnd_Bttn_Platform("Auto")  // Set the default button style name.
End if 
