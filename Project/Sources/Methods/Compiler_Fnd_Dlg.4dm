//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Dlg

// Compiler variables related to the Foundation Dialog routines.
// Called by Fnd_Dlg_Init.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Dlg_Initialized_b)
If (Not:C34(<>Fnd_Dlg_Initialized_b))  // So we only do this once.
	C_BOOLEAN:C305(<>Fnd_Dlg_ProgAborted_b; <>Fnd_Dlg_MessageClose_b; <>Fnd_Dlg_CanBeCancelled_b)
	
	C_LONGINT:C283(<>Fnd_Dlg_FontSize_i; <>Fnd_Dlg_SmallFontSize_i; <>Fnd_Dlg_MessageFontSize_i)
	C_LONGINT:C283(<>Fnd_Dlg_FontStyle_i; <>Fnd_Dlg_SmallFontStyle_i; <>Fnd_Dlg_MessageFontStyle_i)
	C_LONGINT:C283(<>Fnd_Dlg_FontHeight_i; <>Fnd_Dlg_SmallFontHeight_i; <>Fnd_Dlg_MessageFontHeight_i)
	C_LONGINT:C283(<>Fnd_Dlg_ProgFrom_i; <>Fnd_Dlg_ProgTo_i; <>Fnd_Dlg_ProgThermo_i)
	C_LONGINT:C283(<>Fnd_Dlg_TextHeightAdjust_i; <>Fnd_Dlg_ProgBarHeightAdjust_i)
	
	C_REAL:C285(<>Fnd_Dlg_ProgPercent_r)
	
	C_TEXT:C284(<>Fnd_Dlg_FontName_t; <>Fnd_Dlg_SmallFontName_t)
	C_TEXT:C284(<>Fnd_Dlg_MessageFontName_t)
	C_TEXT:C284(<>Fnd_Dlg_MessageProcessName_t; <>Fnd_Dlg_MessageText_t; <>Fnd_Dlg_WindowTitle_t)
	C_TEXT:C284(<>Fnd_Dlg_MenuBarName_t)
	C_TEXT:C284(<>Fnd_Dlg_CancelButton_t)
End if 


// Process Variables
C_BOOLEAN:C305(Fnd_Dlg_Initialized_b)
If (Not:C34(Fnd_Dlg_Initialized_b))
	C_BOOLEAN:C305(Fnd_Dlg_CanBeCancelled_b)
	
	C_LONGINT:C283(Fnd_Dlg_OKButton_i; Fnd_Dlg_CancelButton_i; Fnd_Dlg_OtherButton_i)
	C_LONGINT:C283(Fnd_Dlg_OffscreenButton_i; Fnd_Dlg_EscapeButton_i; Fnd_Dlg_IconButton_i)
	
	C_TEXT:C284(Fnd_Dlg_Text1_t; Fnd_Dlg_Text2_t; Fnd_Dlg_IconPictureName_t)
	C_TEXT:C284(Fnd_Dlg_Button1_t; Fnd_Dlg_Button2_t; Fnd_Dlg_Button3_t)
	C_TEXT:C284(Fnd_Dlg_RequestDefault_t; Fnd_Dlg_RequestValue_t)
	
	C_PICTURE:C286(Fnd_Dlg_IconPicture_pic)
End if 


// Parameters
If (False:C215)
	C_LONGINT:C283(Fnd_Dlg_Alert; $0)
	C_TEXT:C284(Fnd_Dlg_Alert; $1; $2)
	
	C_LONGINT:C283(Fnd_Dlg_Confirm; $0)
	C_TEXT:C284(Fnd_Dlg_Confirm; $1; $2; $3)
	
	C_TEXT:C284(Fnd_Dlg_CustomIcon; $0; $1)
	
	C_LONGINT:C283(Fnd_Dlg_Display; $0)
	
	C_TEXT:C284(Fnd_Dlg_GetRequest; $0)
	
	C_TEXT:C284(Fnd_Dlg_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Dlg_Message; $1)
	
	C_BOOLEAN:C305(Fnd_Dlg_MessageCanceled; $0)
	
	C_TEXT:C284(Fnd_Dlg_MessageOpen2; $1)
	
	C_BOOLEAN:C305(Fnd_Dlg_PasswordEntry; $0)
	C_POINTER:C301(Fnd_Dlg_PasswordEntry; $1; $2)
	
	C_LONGINT:C283(Fnd_Dlg_MessageUpdate; $1)
	
	C_TEXT:C284(Fnd_Dlg_Request; $0; $1; $2; $3; $4)
	
	C_TEXT:C284(Fnd_Dlg_SetButtons; $1; $2; $3)
	
	C_LONGINT:C283(Fnd_Dlg_SetProgress; $1; $2)
	
	C_TEXT:C284(Fnd_Dlg_SetText; $1; $2)
	
	C_LONGINT:C283(Fnd_Dlg_SetIcon; $1)
	
	C_BOOLEAN:C305(Fnd_Dlg_SetCancelable; $1)
	
	C_TEXT:C284(Fnd_Dlg_SetRequest; $1)
End if 
