//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_SetName (table pointer) --> text

// Returns the special Related Set Name for table ($1)

// Access: Shared

// Parameters: 
//   $1 : pointer : Pointer to a table

// Returns: 
//   $0 : text : Set Name

// Created by Doug Hall (20030317)
// ----------------------------------------------------

#DECLARE($table_ptr : Pointer)->$setName_t : Text

$setName_t:="<>sR"+Table name:C256($table_ptr)
