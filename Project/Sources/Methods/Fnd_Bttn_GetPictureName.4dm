//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Bttn_GetPictureName (icon name; label{; variation}) --> Text


// Creates a composite name for the picture which will be used to

//   reference it quickly in the cache.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The button icon name

//   $2 : Text : The button label name

//   $3 : Text : The variaton name (optional)


// Returns: 

//   $0 : Text : The cache picture name


// Created by Mark Mitchenall on 17/9/01

//   Original Method Name: rollover_GetPictureName

//   Part of the toolbar_Component, ©2001 mitchenall.com

//   Used with permission.

// Modified by Dave Batton on Feb 20, 2004

//   Updated specifically for use by the Foundation Shell.

// Modified by Dave Batton on Apr 13, 2005

//   Added the new "variation" parameter to support Pref buttons.

//   Restored the Fnd_Bttn_Init call.

// ----------------------------------------------------


C_TEXT:C284($0;$1;$2;$3;$rolloverPictureName_t;$iconName_t;$buttonLabel_t;$variation_t)

$iconName_t:=$1
$buttonLabel_t:=$2

If (Count parameters:C259>=3)
	$variation_t:=$3
Else 
	$variation_t:=""
End if 

Fnd_Bttn_Init  // DB050413


$rolloverPictureName_t:=Fnd_Bttn_Style_t+"/"+Fnd_Bttn_Platform_t+"/"+$iconName_t+"/"+$buttonLabel_t+"/"+$variation_t

$0:=$rolloverPictureName_t