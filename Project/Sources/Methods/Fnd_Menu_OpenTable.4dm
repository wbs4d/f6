//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_OpenTable

// Called when the user selects the Open Table command from the File menu.
// This is just a temporary placeholder.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 31, 2003
// Modified by Dave Batton on Mar 23, 2004
//   Now calls the Open Table hook rather than Fnd_Shell_OpenTableDialog.
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Shell component is no longer required.
// ----------------------------------------------------

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_OpenTable")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_OpenTable"; *)
Else 
	Fnd_Shell_OpenTableDialog
End if 
