//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_EditOne2 (list name)

  // Called as a new process by Fnd_List_EditOne to edit a list.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The list name

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Apr 23, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // ----------------------------------------------------

C_TEXT:C284($1;$listName_t;$formula_t)
C_LONGINT:C283(<>Fnd_List_EditOneListItems_i;$width_i;$height_i)
C_POINTER:C301($nil_ptr)

$listName_t:=$1

Fnd_List_Init 

Fnd_Gen_MenuBar 

$formula_t:="$2->:="+Command name:C538(383)+"(\""+$listName_t+"\")"  // 383 = Load list
EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t;-><>Fnd_List_EditOneListItems_i)

If (Is a list:C621(<>Fnd_List_EditOneListItems_i))
	Fnd_Wnd_Type (Movable form dialog box:K39:8)
	Fnd_Wnd_Title ($listName_t)
	FORM GET PROPERTIES:C674("Fnd_List_EditOne";$width_i;$height_i)
	Fnd_Wnd_OpenWindow ($width_i;$height_i)
	DIALOG:C40("Fnd_List_EditOne")
	CLOSE WINDOW:C154
	
	If (OK=1)
		$formula_t:=Command name:C538(384)+"("+String:C10(<>Fnd_List_EditOneListItems_i)+";\""+$listName_t+"\")"  // 384 = SAVE LIST
		EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t)
		Fnd_List_ListToRecord ($listName_t)  // Save the list to the [Fnd_List] table so it can survive a structure update.
	End if 
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"\""+$listName_t+"\" is not a valid list name.")
End if 