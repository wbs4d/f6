//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_ISOtoTime (date-time string) --> Time

  // Retrieves the time value from an ISO 8601 formatted date-time string. The input
  //   must be in the YYYY-MM-DDThh:mm:ss or hh:mm:ss format.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : An ISO format date-time or time string

  // Returns: 
  //   $0 : Date : The time

  // Created by Dave Batton on Jan 7, 2005
  // Modified by Dave Batton on Mar 30, 2005
  //   Now this routine can handle a time-only value too: hh:mm:ss
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TIME:C306($0;$time_time)
C_TEXT:C284($1;$isoDateTime_t)
C_TEXT:C284($hour_t;$minute_t;$second_t)

$isoDateTime_t:=$1

If (Length:C16($isoDateTime_t)=19)
	  // It has both a date and time. Strip off the date, keep the time.
	$isoDateTime_t:=Substring:C12($isoDateTime_t;12)
End if 

If (Length:C16($isoDateTime_t)=8)
	$hour_t:=Substring:C12($isoDateTime_t;1;2)
	$minute_t:=Substring:C12($isoDateTime_t;4;2)
	$second_t:=Substring:C12($isoDateTime_t;7;2)
	$time_time:=Time:C179($hour_t+":"+$minute_t+":"+$second_t)
	
Else 
	$time_time:=?00:00:00?
End if 

$0:=$time_time