//%attributes = {"invisible":true,"shared":true,"preemptive":"incapable"}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_AlternativePicturePath {(Path)} --> Text

// Foundation normally creates buttons based on icons in the 
//  Fnd_Bttn_Pictures folder within the component resource folder.
//  This allows the developer to specify an alternative location
//  for example in the host resource folder.
//  Foundation will first of all check this location first 
//  and then the  Fnd_Bttn_Pictures folder in component resoure folder

// Access: Shared

// Parameters: 
//   $1 : Text : The Path (optional)

// Returns: 
//   $0 : Text : The path

// Created by Wayne Stewart (2016-11-23)
//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284(<>Fnd_Bttn_AltPicturePath_t)
If (False:C215)
	C_TEXT:C284(Fnd_Bttn_AlternativePicturePath; $1; $0)
End if 

C_TEXT:C284($1; $0)

//  Fnd_Bttn_Init 
//   I considered calling Fnd_Bttn_Init here but it is not really necessary

If (Count parameters:C259=1)
	<>Fnd_Bttn_AltPicturePath_t:=$1
End if 

$0:=<>Fnd_Bttn_AltPicturePath_t

