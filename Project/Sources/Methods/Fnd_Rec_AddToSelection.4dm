//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_AddToSelection (record number)

// Adds the record to the current selection. Designed to be called from
//   an output form.  See Fnd_IO_FormMethod.

// Access Type: Shared

// Parameters: 
//   $1 : Longint : Record number

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Feb 20, 2004
//   Switched from using Current form table to Fnd_Gen_CurrentTable.
// Modified by Dave Batton on May 23, 2004
//   It's now safe to pass 4D's No Current Record constant to this method.
//   Now checks to make sure the record isn't already in the selection.
// Modified by Dave Batton on Mar 2, 2006
//   Added support for the Fnd_Out component.
// ----------------------------------------------------

C_LONGINT:C283($1;$recordNumber_i;$element_i)

$recordNumber_i:=$1

Case of 
	: ($recordNumber_i=No current record:K29:2)
		// There's nothing to do.
		
		
	: (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
		EXECUTE METHOD:C1007("Fnd_Out_AddToSelection";*;$recordNumber_i)
		
		
	Else 
		ARRAY LONGINT:C221($recordNumberArray_ai;0)
		LONGINT ARRAY FROM SELECTION:C647(Fnd_Gen_CurrentTable->;$recordNumberArray_ai)
		
		// If this record isn't already in the selection, add it.
		$element_i:=Find in array:C230($recordNumberArray_ai;$recordNumber_i)
		If ($element_i=-1)
			$element_i:=Size of array:C274($recordNumberArray_ai)+1
			INSERT IN ARRAY:C227($recordNumberArray_ai;$element_i)
			$recordNumberArray_ai{$element_i}:=$recordNumber_i
			CREATE SELECTION FROM ARRAY:C640(Fnd_Gen_CurrentTable->;$recordNumberArray_ai)
			Fnd_Gen_SelectionChanged  // Always call this after changing the selection.
		End if 
		
		// Highlight the record.
		ARRAY LONGINT:C221($recordNumberArray_ai;1)
		$recordNumberArray_ai{1}:=$recordNumber_i
		CREATE SET FROM ARRAY:C641(Fnd_Gen_CurrentTable->;$recordNumberArray_ai)
		HIGHLIGHT RECORDS:C656
		
		ARRAY LONGINT:C221($recordNumberArray_ai;0)  // Clear the array.
End case 
