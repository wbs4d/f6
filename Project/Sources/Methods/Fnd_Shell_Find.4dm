//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_Find

// Called from the Find menu bar item or toolbar item to display the Find dialog.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on 21 Jul 2008
// ----------------------------------------------------

Case of 
	: (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Find")=1)
		EXECUTE METHOD:C1007("Fnd_Hook_Shell_Find"; *)
		
	Else 
		Fnd_Find_Display
		//: (Fnd_Gen_ComponentAvailable("Fnd_Find"))
		//EXECUTE METHOD("Fnd_Find_Display"; *)
		
		//Else 
		//QUERY(Fnd_Gen_CurrentTable->)
		//Fnd_Gen_SelectionChanged
End case 
