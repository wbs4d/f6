//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_SetLocalizationStrings (component; ->lookup codes; ->strings)

// Called from the Fnd_xxx_Localize method of other components to pass their localization
//   info to this, and (if available) the Localization component.

// Access: Shared

// Parameters: 
//   $1 : Text : The component's code ("Fnd_xxx")
//   $2 : Pointer : Array of lookup codes
//   $3 : Pointer : Array of localized strings

// Returns: Nothing

// Created by Dave Batton on Oct 4, 2007
// Modified by Gerald Balzer on April 14, 2012
//  Now loads the english strings when the requested list is unavailable. This became necessary after the switch to 4D's DB Localization commands 
// ----------------------------------------------------

C_TEXT:C284($1; $component_t)
C_POINTER:C301($2; $3; $lookupCodesArray_ptr; $stringsArray_ptr)
C_LONGINT:C283($element_i)

$component_t:=$1
$lookupCodesArray_ptr:=$2
$stringsArray_ptr:=$3

Fnd_Gen_Init

//Gerald Balzer, if no localized strings are provided, load the english ones
If (Size of array:C274($stringsArray_ptr->)=0)
	LIST TO ARRAY:C288($component_t+"_EN"; $stringsArray_ptr->)
End if 

Fnd_Loc_SetLocalizationStrings($component_t; $lookupCodesArray_ptr; $stringsArray_ptr)

//If (Fnd_Gen_ComponentAvailable("Fnd_Loc"))
//EXECUTE METHOD("Fnd_Loc_SetLocalizationStrings"; *; $component_t; $lookupCodesArray_ptr; $stringsArray_ptr)

//Else 
//If (Not(Semaphore(<>Fnd_Gen_LocSemaphore_t; 300)))  // Wait up to 5 seconds.

//$element_i:=Find in array(<>Fnd_Gen_LocModules_at; $component_t)
//If ($element_i=-1)
//$element_i:=Size of array(<>Fnd_Gen_LocModules_at)+1
//INSERT IN ARRAY(<>Fnd_Gen_LocModules_at; $element_i)
//INSERT IN ARRAY(<>Fnd_Gen_LocLookupCodes_at; $element_i)
//INSERT IN ARRAY(<>Fnd_Gen_LocStrings_at; $element_i)
//End if 

//<>Fnd_Gen_LocModules_at{$element_i}:=$component_t
//COPY ARRAY($lookupCodesArray_ptr->; <>Fnd_Gen_LocLookupCodes_at{$element_i})
//COPY ARRAY($stringsArray_ptr->; <>Fnd_Gen_LocStrings_at{$element_i})

//If ((Size of array(<>Fnd_Gen_LocStrings_at{$element_i})=0) | (Size of array(<>Fnd_Gen_LocStrings_at{$element_i})#Size of array(<>Fnd_Gen_LocLookupCodes_at{$element_i})))
//Fnd_Gen_BugAlert(Current method name; "There is a problem with the \""+$component_t+"\" localization strings.")
//End if 

//CLEAR SEMAPHORE(<>Fnd_Gen_LocSemaphore_t)
//End if 
//End if 
