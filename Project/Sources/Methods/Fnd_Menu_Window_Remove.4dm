//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_Window_Remove ({process number})

  // Removes the specified process from the Window menu.  If the process
  //   number is not specified, the current process is used.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Process number to remove

  // Returns: Nothing

  // Created by Dave Batton on Sep 15, 2003
  // Modified by Dave Batton on May 23, 2004
  //   Moved the Fnd_Menu_MenuBar and now pass the optional * parameter.
  // Modified by Dave Batton on Mar 16, 2006
  //   Changed the timeout value to use the new variable rather than being hard-coded.
  // ----------------------------------------------------

C_LONGINT:C283($1;$processNumber_i;$element_i)

If (Count parameters:C259>=1)
	$processNumber_i:=$1
Else 
	$processNumber_i:=Current process:C322
End if 

Fnd_Menu_Init 

If (Not:C34(Semaphore:C143(<>Fnd_Menu_Semaphore_t;<>Fnd_Menu_SemaphoreTimeout_i)))  // DB060316
	$element_i:=Find in array:C230(<>Fnd_Menu_Window_Processes_ai;$processNumber_i)
	
	If ($element_i#-1)
		DELETE FROM ARRAY:C228(<>Fnd_Menu_Window_Processes_ai;$element_i)
		DELETE FROM ARRAY:C228(<>Fnd_Menu_Window_Items_at;$element_i)
	End if 
	
	CLEAR SEMAPHORE:C144(<>Fnd_Menu_Semaphore_t)
End if 

Fnd_Menu_MenuBar ("*")