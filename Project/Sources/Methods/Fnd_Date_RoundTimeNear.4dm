//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_RoundTimeNear (time) --> Time

  // Rounds to the quarter hour. If the time passed in is up to ten minutes
  //   after the quarter hour, the time is rounded down. Otherwise it's
  //   rounded up.  Won't round up past 23:59.

  // Access: Private

  // Parameters: 
  //   $1 : Time : The time to round

  // Returns: 
  //   $0 : Time : The rounded time

  // Created by Dave Batton on May 12, 2004
  // ----------------------------------------------------

C_TIME:C306($0;$1;$time_time)
C_TEXT:C284($time_time_t)
C_LONGINT:C283($hours_i;$minutes_i)

$time_time:=$1

$time_time_t:=String:C10($time_time;HH MM:K7:2)
$hours_i:=Num:C11(Substring:C12($time_time_t;1;2))
$minutes_i:=Num:C11(Substring:C12($time_time_t;4;2))

Case of 
	: ($minutes_i<10)
		$minutes_i:=0
	: ($minutes_i<25)
		$minutes_i:=15
	: ($minutes_i<40)
		$minutes_i:=30
	: ($minutes_i<55)
		$minutes_i:=45
	Else 
		$minutes_i:=0
		$hours_i:=$hours_i+1
		If ($hours_i>=24)
			$hours_i:=23
			$minutes_i:=59
		End if 
End case 

$time_time_t:=String:C10($hours_i;"00")+":"+String:C10($minutes_i;"00")
$time_time:=Time:C179($time_time_t)

$0:=$time_time