//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Date_Calendar (->date field or variable) -> bOK

// Displays a small calendar window and allows the user to select
//   a date. Modifies the passed parameter.
// returns the bOK variable to 1 if a date is selected.

// Access: Shared

// Parameters: 
//   $1 : Pointer : A date field or variable

// Returns: 
//   $0 : Boolean : True = (OK=1)

// Created by Tom Dillon's Date Entry component.
//   This method originally named DE_DisplayCalendar.
// Modified by Dave Batton on Mar 26, 2005
// Modified by Dave Batton on Dec 26, 2005
//   Added a work-around for a bug with the pop-up window type on Windows.
// Modified by: Walt Nelson (3/4/11) return bOK
// Mod by Wayne Stewart, (2021-08-11) - Correct documentation
// ----------------------------------------------------

C_POINTER:C301($1)
C_BOOLEAN:C305($0)
C_LONGINT:C283($objectLeft_i;$objectTop_i;$objectRight_i;$objectBottom_i)
C_LONGINT:C283($windowLeft_i;$windowTop_i;$windowRight_i;$windowBottom_i)
C_LONGINT:C283($calendarLeft_i;$calendarTop_i;$calendarWidth_i;$calendarHeight_i)
C_LONGINT:C283($windowType_i)
C_TEXT:C284($oldEventHandler_t)

Fnd_Date_Init

<>Fnd_Date_CalendarDateField_ptr:=$1  // ◊Fnd_Date_CalendarDateField_ptr cannot be a local variable since it is used to pass the date to and from the calendar form.

OBJECT GET COORDINATES:C663(<>Fnd_Date_CalendarDateField_ptr->;$objectLeft_i;$objectTop_i;$objectRight_i;$objectBottom_i)
GET WINDOW RECT:C443($windowLeft_i;$windowTop_i;$windowRight_i;$windowBottom_i)
FORM GET PROPERTIES:C674("Fnd_Date_CalendarPopup";$calendarWidth_i;$calendarHeight_i)

$calendarLeft_i:=$windowLeft_i+$objectLeft_i-1

Case of 
	: (Application version:C493>="0800")  // If we're in 4D 2004, use the new popup window type
		$calendarTop_i:=$windowTop_i+$objectBottom_i+4
		$windowType_i:=32  // Pop up form window 
		
	: (Is Windows:C1573)  // 4D 2003 on Windows
		$calendarLeft_i:=$windowLeft_i+$objectLeft_i
		$calendarTop_i:=$windowTop_i+$objectBottom_i+6
		$windowType_i:=Modal form dialog box:K39:7
		$oldEventHandler_t:=Method called on event:C705
		ON EVENT CALL:C190("Fnd_Date_CalendarEventHandler";"$Fnd_Date: Event Handler")
		
	Else   // 4D 2003 on Macintosh
		$calendarTop_i:=$windowTop_i+$objectBottom_i+16  // Leave room for the window title.
		$windowType_i:=0-Palette form window:K39:9
		$oldEventHandler_t:=Method called on event:C705
		ON EVENT CALL:C190("Fnd_Date_CalendarEventHandler";"$Fnd_Date: Event Handler")
End case 

<>Fnd_Date_CalendarWindowRef_i:=Open window:C153($calendarLeft_i;$calendarTop_i;$calendarLeft_i+$calendarWidth_i;$calendarTop_i+$calendarHeight_i;$windowType_i)  //;Fnd_Gen_GetString ("Fnd_Date";"CalendarWindowTitle"))  `;"Fnd_Date_DummyMethod")

// DB051226 - There's a bug in 4D 2004.0 - 2004.2 with the new pop-up
//   window type. This is the workaround. It was fixed in 4D 2004.3. 
If ((Application version:C493>="0800") & (Application version:C493<="0802"))
	BRING TO FRONT:C326(Current process:C322)
End if 

DIALOG:C40("Fnd_Date_CalendarPopup")
CLOSE WINDOW:C154(<>Fnd_Date_CalendarWindowRef_i)

If (OK=1)
	<>Fnd_Date_CalendarDateField_ptr->:=<>Fnd_Date_CalendarDate_d
	
End if 

Case of 
	: (Application version:C493>="0803")  // DB051226
		// No fix needed for 4D 2004.3 or later.
		
	: (Application version:C493>="0800")  // Another bug fix.
		BRING TO FRONT:C326(Current process:C322)
		
	Else 
		ON EVENT CALL:C190($oldEventHandler_t)
End case 
$0:=(OK=1)  // OK variable's scope is inside the component wwn Modified by: Walt Nelson (3/4/11)