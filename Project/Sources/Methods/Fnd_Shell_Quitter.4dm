//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_Quitter

  // This process tries to tell all of the other processes to quit.
  // Called as a new process from Fnd_Gen_QuitNow.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 8, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   Reworked this method so Fnd_Gen_QuitNow isn't called if we actually
  //   get to the part where we set <>Fnd_Shell_Running_b to False.
  // Modified by Dave Batton on Dec 11, 2004
  //   Now calls QUIT 4D if we're using 4D Runtime or 4D Runtime Classic.
  // Modified by: Walt Nelson (12/30/10) commented out SET MENU BAR (1;1)
  // This command CANNOT be called from a component in v12.1 or less
  // Modified by: Walt Nelson (5/14/11) - v12.2 returns the SET MENU BAR to v11 behavior
  // ----------------------------------------------------

C_BOOLEAN:C305($quitIsOK_b)
C_LONGINT:C283($4D_version_i)

Fnd_Shell_Init 

  // Make sure we're running.  Otherwise trying to save preferences
  //    that haven't been initialized will cause problems.
If (<>Fnd_Shell_Running_b)
	
	  //First, close all windows that have requested a Close message.
	  // Pass True to wait for all of the windows to close.
	If (Fnd_Wnd_CloseAllWindows (True:C214))
		
		  // If they all closed (nobody canceled the attempt) then tell all of the other processes to end.
		If (Fnd_Shell_QuitAllProcesses )
			
			Fnd_Pref_SavePrefs   // Save the user's preferences.
			
			$quitIsOK_b:=True:C214
			If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_Shell_Quit")=1)
				EXECUTE METHOD:C1007("Fnd_Hook_Shell_Quit";$quitIsOK_b)
			End if 
			
			If ($quitIsOK_b)  // See if the developer will allow us to quit.
				<>Fnd_Shell_Running_b:=False:C215  // Checked below and by Fnd_Shell_Restart.
			End if 
		End if 
	End if 
End if 

Fnd_Gen_QuitNow (False:C215)  // We're done trying to quit.

If (Not:C34(<>Fnd_Shell_Running_b))
	If ((Is compiled mode:C492) | (Macintosh option down:C545) | (Application type:C494=_o_4D Interpreted desktop:K5:3) | (Application type:C494=4D Desktop:K5:4))
		QUIT 4D:C291
	Else 
		Fnd_Gen_Reset   // So all of our variables will get re-initialized if we startup again.
		
		$4D_version_i:=Num:C11(Application version:C493)
		
		Case of 
			: ($4D_version_i<1200)  // any v11 version
				SET MENU BAR:C67(1;1)  // Set the Custom Menus process to the first menu, we MUST do this in v11 or we can't quit!?!
			: ($4D_version_i>=1220)  // any v12 version equal or later than v12.2
				SET MENU BAR:C67(1;1)  // Set the Custom Menus process to the first menu, we MUST do this in v11 or we can't quit!?!
			Else 
				  // Modified by: Walt Nelson (1/22/11)
				  // don't call this from v12.1 or SET MENU BAR will crash
		End case 
		
	End if 
End if 