//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Text_FormatNumber (string, format) --> Text

// Formats a numeric string (like a zip code or phone number).
//   Removes any dashes, "E"s, periods, etc. before formatting.

// Access: Shared

// Parameters: 
//   $1 : Text : The number as text
//   $2 : Text : The format to use

// Returns: 
//   $0 : Text : The formatted number

// Created by Dave Batton on May 11, 2004
// ----------------------------------------------------

C_TEXT:C284($0; $1; $2)

$1:=Replace string:C233($1; "E"; "")
$1:=Replace string:C233($1; "-"; "")
$1:=Replace string:C233($1; "."; "")
$1:=Replace string:C233($1; "/"; "")
$1:=Replace string:C233($1; "\\"; "")
$1:=Replace string:C233($1; "+"; "")
$1:=Replace string:C233($1; "*"; "")
$0:=String:C10(Num:C11($1); $2)