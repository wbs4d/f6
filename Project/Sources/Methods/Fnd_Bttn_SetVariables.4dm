//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_SetVariables

  // Updates the toolbar variables depending on the current platform, size, and style settings.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 15, 2004
  //   Based on code by Mark Mitchenall.
  // Modified by Dave Batton on Jan 9, 2005
  //   Changed the possible style names to Auto, Windows, Macintosh, and Small.
  // Modified by Dave Batton on Apr 13, 2005
  //   Added support for setting the "current" background images.
  // Modified by Dave Batton on Nov 18, 2005
  //   Added support for the menu indicator graphic.
  // [1] Modified by: John Craig (7/03/2009) was mistakenly set to 40 and caused a one pixel line issue
  // [2] Added by: John Craig (12/10/09) - to use external image library
  // ----------------------------------------------------

C_PICTURE:C286($leftImage_pic;$middleImage_pic;$rightImage_pic;$leftCrntImage_pic;$middleCrntImage_pic;$rightCrntImage_pic)
C_TEXT:C284($backgroundPictureNamePrefix_t)
C_LONGINT:C283($platform_i;$system_i)

Case of 
	: ((Fnd_Bttn_Style_t="Large") & (Fnd_Bttn_Platform_t="Mac"))
		  // Overall button settings.
		Fnd_Bttn_ButtonHeight_i:=56  //height of the button
		Fnd_Bttn_MinButtonWidth_i:=0  // The minumum button width.
		Fnd_Bttn_LabelPlacement_i:=At the bottom:K39:6  // Text label to the right of the icon.
		
		  // The icon settings.
		Fnd_Bttn_IconLeftOffset_i:=0
		Fnd_Bttn_IconTopOffset_i:=5
		Fnd_Bttn_IconRightOffset_i:=0
		
		  //The text settings.
		Fnd_Bttn_TextLeftOffset_i:=1
		Fnd_Bttn_TextRightOffset_i:=1
		Fnd_Bttn_TextTopOffset_i:=39  // [1] Modified by: John Craig (7/03/2009) was mistakenly set to 40 and caused a one pixel line issue
		Fnd_Bttn_TextRightPadding_i:=1
		Fnd_Bttn_TextBottomPadding_i:=1
		Fnd_Bttn_SetTextProperties ("Lucida Grande";11;Plain:K14:1)
		Fnd_Bttn_TextDownStyle_i:=Bold:K14:2
		
		  //Horizontal and vertical shift of the icon and text for each frame.
		Fnd_Bttn_HorizonalShift_ai{1}:=0  //Normal
		Fnd_Bttn_VerticalShift_ai{1}:=0
		Fnd_Bttn_HorizonalShift_ai{2}:=0  //Clicked
		Fnd_Bttn_VerticalShift_ai{2}:=0
		Fnd_Bttn_HorizonalShift_ai{3}:=0  //MouseOver
		Fnd_Bttn_VerticalShift_ai{3}:=0
		Fnd_Bttn_HorizonalShift_ai{4}:=0  //Disabled
		Fnd_Bttn_VerticalShift_ai{4}:=0
		
		Fnd_Bttn_MenuIndicatorHOffset_i:=2
		Fnd_Bttn_MenuIndicatorVOffset_i:=31
		Fnd_Bttn_MenuIndicatorPadding_i:=0
		
		
	: (((Fnd_Bttn_Style_t="Large") | (Fnd_Bttn_Style_t="Prefs")) & (Fnd_Bttn_Platform_t="Win"))
		  // Overall button settings.
		Fnd_Bttn_ButtonHeight_i:=52  //height of the button
		Fnd_Bttn_MinButtonWidth_i:=67  // The minumum button width.
		Fnd_Bttn_LabelPlacement_i:=At the bottom:K39:6  // Text label to the right of the icon.
		
		  // The icon settings.
		Fnd_Bttn_IconLeftOffset_i:=0
		Fnd_Bttn_IconTopOffset_i:=5
		Fnd_Bttn_IconRightOffset_i:=0
		
		  //The text settings.
		Fnd_Bttn_TextLeftOffset_i:=10
		Fnd_Bttn_TextRightOffset_i:=10
		Fnd_Bttn_TextTopOffset_i:=35
		Fnd_Bttn_TextRightPadding_i:=1
		Fnd_Bttn_TextBottomPadding_i:=1
		Fnd_Bttn_SetTextProperties ("Verdana";11;Plain:K14:1)
		Fnd_Bttn_TextDownStyle_i:=Bold:K14:2
		
		  //Horizontal and vertical shift of the icon and text for each frame.
		Fnd_Bttn_HorizonalShift_ai{1}:=0  //Normal
		Fnd_Bttn_VerticalShift_ai{1}:=0
		Fnd_Bttn_HorizonalShift_ai{2}:=1  //Clicked
		Fnd_Bttn_VerticalShift_ai{2}:=0
		Fnd_Bttn_HorizonalShift_ai{3}:=0  //MouseOver
		Fnd_Bttn_VerticalShift_ai{3}:=0
		Fnd_Bttn_HorizonalShift_ai{4}:=0  //Disabled
		Fnd_Bttn_VerticalShift_ai{4}:=0
		
		Fnd_Bttn_MenuIndicatorHOffset_i:=7
		Fnd_Bttn_MenuIndicatorVOffset_i:=16
		Fnd_Bttn_MenuIndicatorPadding_i:=0
		
		
	: (Fnd_Bttn_Style_t="Prefs")  // Pretty much the same as Large-Mac.
		  // Overall button settings.
		Fnd_Bttn_ButtonHeight_i:=56  //height of the button
		Fnd_Bttn_MinButtonWidth_i:=0  // The minumum button width.
		Fnd_Bttn_LabelPlacement_i:=At the bottom:K39:6  // Text label to the right of the icon.
		
		  // The icon settings.
		Fnd_Bttn_IconLeftOffset_i:=6
		Fnd_Bttn_IconTopOffset_i:=5
		Fnd_Bttn_IconRightOffset_i:=6
		
		  //The text settings.
		Fnd_Bttn_TextLeftOffset_i:=6
		Fnd_Bttn_TextRightOffset_i:=6
		Fnd_Bttn_TextTopOffset_i:=40
		Fnd_Bttn_TextRightPadding_i:=1
		Fnd_Bttn_TextBottomPadding_i:=1
		If (Fnd_Bttn_Platform_t="Mac")
			Fnd_Bttn_SetTextProperties ("Lucida Grande";11;Plain:K14:1)
		Else 
			Fnd_Bttn_SetTextProperties ("Verdana";11;Plain:K14:1)
		End if 
		Fnd_Bttn_TextDownStyle_i:=Bold:K14:2
		
		  //Horizontal and vertical shift of the icon and text for each frame.
		Fnd_Bttn_HorizonalShift_ai{1}:=0  //Normal
		Fnd_Bttn_VerticalShift_ai{1}:=0
		Fnd_Bttn_HorizonalShift_ai{2}:=0  //Clicked
		Fnd_Bttn_VerticalShift_ai{2}:=0
		Fnd_Bttn_HorizonalShift_ai{3}:=0  //MouseOver
		Fnd_Bttn_VerticalShift_ai{3}:=0
		Fnd_Bttn_HorizonalShift_ai{4}:=0  //Disabled
		Fnd_Bttn_VerticalShift_ai{4}:=0
		
		Fnd_Bttn_MenuIndicatorHOffset_i:=0
		Fnd_Bttn_MenuIndicatorVOffset_i:=0
		Fnd_Bttn_MenuIndicatorPadding_i:=0
		
		
	: (Fnd_Bttn_Style_t="Small@")
		  // Overall button settings.
		Fnd_Bttn_LabelPlacement_i:=On the right:K39:3  // Text label to the right of the icon.
		Fnd_Bttn_ButtonHeight_i:=22  //height of the button
		Fnd_Bttn_MinButtonWidth_i:=23  // The minumum button width.
		
		  // The icon settings.
		Fnd_Bttn_IconLeftOffset_i:=4
		Fnd_Bttn_IconTopOffset_i:=3
		Fnd_Bttn_IconRightOffset_i:=4
		
		  //Position of the text within the rollover
		Fnd_Bttn_TextLeftOffset_i:=4
		Fnd_Bttn_TextRightOffset_i:=3
		Fnd_Bttn_TextTopOffset_i:=5
		Fnd_Bttn_TextRightPadding_i:=1
		Fnd_Bttn_TextBottomPadding_i:=1
		If (Is Windows:C1573)  // Try to stick to a font that will be available on the current platform.
			Fnd_Bttn_SetTextProperties ("Verdana";11;Plain:K14:1)
		Else 
			Fnd_Bttn_SetTextProperties ("Lucida Grande";11;Plain:K14:1)
		End if 
		Fnd_Bttn_TextDownStyle_i:=Plain:K14:1
		
		  //Adjust the vertical and horizontal shift of the icon and text in each frame of 
		  //the button.
		Fnd_Bttn_HorizonalShift_ai{1}:=0  //Normal
		Fnd_Bttn_VerticalShift_ai{1}:=0
		Fnd_Bttn_HorizonalShift_ai{2}:=0  //Clicked
		Fnd_Bttn_VerticalShift_ai{2}:=0
		Fnd_Bttn_HorizonalShift_ai{3}:=0  //MouseOver
		Fnd_Bttn_VerticalShift_ai{3}:=0
		Fnd_Bttn_HorizonalShift_ai{4}:=0  //Disabled
		Fnd_Bttn_VerticalShift_ai{4}:=0
		
		Fnd_Bttn_MenuIndicatorHOffset_i:=-5
		Fnd_Bttn_MenuIndicatorVOffset_i:=10
		Fnd_Bttn_MenuIndicatorPadding_i:=6
		
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"Unexpected button style name: "+Fnd_Bttn_Style_t)
End case 


  // Button background names look like: Fnd_Bttn_Bkgrnd_Mac_Prefs_M ("L"=Left, "M"=Middle, "R"=Right")
  // Matching Pref backgrounds, if available, look like: Fnd_Bttn_Bkgrnd_Mac_Prefs_CM ("CL"=Current Left, etc.)
$backgroundPictureNamePrefix_t:="Fnd_Bttn_Bkgrnd_"+Fnd_Bttn_Platform_t+"_"+Fnd_Bttn_Style_t+"_"

  //[2] Modified by: John Craig (12/10/09) ->
If (<>Fnd_Bttn_PicturePath_t#"")
	READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"L"+".png";$leftImage_pic)
	READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"M"+".png";$middleImage_pic)
	READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"R"+".png";$rightImage_pic)
Else 
	GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"L";$leftImage_pic)  // DB050413 - Added it back to these.
	GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"M";$middleImage_pic)
	GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"R";$rightImage_pic)
End if 
  //[2] Modified by: John Craig (12/10/09) <-

If (Fnd_Bttn_Style_t#"Prefs")
	Fnd_Bttn_SetBackgroundPict ($leftImage_pic;$middleImage_pic;$rightImage_pic)
	
Else 
	  //[2] Modified by: John Craig (12/10/09) ->
	If (<>Fnd_Bttn_PicturePath_t#"")
		READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"CL"+".png";$leftCrntImage_pic)
		READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"CM"+".png";$middleCrntImage_pic)
		READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$backgroundPictureNamePrefix_t+"CR"+".png";$rightCrntImage_pic)
	Else 
		GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"CL";$leftCrntImage_pic)  // DB050413 - New stuff.
		GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"CM";$middleCrntImage_pic)
		GET PICTURE FROM LIBRARY:C565($backgroundPictureNamePrefix_t+"CR";$rightCrntImage_pic)
	End if 
	  //[2] Modified by: John Craig (12/10/09) <-
	
	Fnd_Bttn_SetBackgroundPict ($leftImage_pic;$middleImage_pic;$rightImage_pic;$leftCrntImage_pic;$middleCrntImage_pic;$rightCrntImage_pic)
End if 
