//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Divider_Add

// Adds a divider line to the toolbar object list.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Mark Mitchenall on 7/2/02
// ----------------------------------------------------

C_LONGINT:C283($objectNumber_i)

Fnd_Tlbr_Init

$objectNumber_i:=Fnd_Tlbr_UsedObjects_i+1

If ($objectNumber_i<=<>Fnd_Tlbr_MaxObjects_i)
	//input the button info into the toolbar button info stack
	Fnd_Tlbr_ObjectNames_at{$objectNumber_i}:="--Line"
	Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}:=4
	
	Fnd_Tlbr_UsedObjects_i:=$objectNumber_i
	
	Fnd_Tlbr_ButtonsAreValid_b:=False:C215
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Maximum number of toolbar objects reached.")
End if 
