//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_PictureToBlob (->picture; ->blob)

  // Converts a picture to a blob.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : A pointer to the picture
  //   $2 : Pointer : A pointer to the blob to fill

  // Returns: Nothing

  // Created by JPR around 2000
  // Modified by Dave Batton on Aug 5, 2003
  //   Changed the blob IP variable to a dereferenced pointer to the passed-in blob.
  //   Changed the blob and picture variables to pointer parameters.
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$picture_ptr;$blob_ptr)

$picture_ptr:=$1
$blob_ptr:=$2

VARIABLE TO BLOB:C532($picture_ptr->;$blob_ptr->)  //4D puts the same infos around the 'picture'
SET BLOB SIZE:C606($blob_ptr->;BLOB size:C605($blob_ptr->)-6)  //We remove the costume, beginning by the top
DELETE FROM BLOB:C560($blob_ptr->;0;9)  //then the bottom 