//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_SaveServerPreference (owner; pref name; type; value; scope)

  // Saves a single preference value in the database.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The item's owner name
  //   $2 : Text : The name of the preference item
  //   $3 : Longint : The variable type
  //   $4 : Text : The value to save as text
  //   $5 : Longint : The scope

  // Returns: Nothing

  // Created by Dave Batton on Feb 6, 2006
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$4;$itemOwner_t;$itemName_t;$itemValue_t;$processName_t)
C_LONGINT:C283($3;$5;$itemType_i;$itemScope_i;$state_i;$time_i)

$itemOwner_t:=$1
$itemName_t:=$2
$itemType_i:=$3
$itemValue_t:=$4
$itemScope_i:=$5

  // We need to be careful not to access the data file if this is a local process.
PROCESS PROPERTIES:C336(Current process:C322;$processName_t;$state_i;$time_i)

If ($processName_t#"$@")
	READ WRITE:C146(<>Fnd_Pref_Table_ptr->)
	QUERY:C277(<>Fnd_Pref_Table_ptr->;<>Fnd_Pref_OwnerFld_ptr->=$itemOwner_t;*)
	QUERY:C277(<>Fnd_Pref_Table_ptr->; & ;<>Fnd_Pref_NameFld_ptr->=$itemName_t;*)
	QUERY:C277(<>Fnd_Pref_Table_ptr->; & ;<>Fnd_Pref_TypeFld_ptr->=$itemType_i)
	
	If ($itemScope_i#Fnd_Pref_Local)
		
		If (Records in selection:C76(<>Fnd_Pref_Table_ptr->)=0)
			CREATE RECORD:C68(<>Fnd_Pref_Table_ptr->)
			<>Fnd_Pref_IDFld_ptr->:=Sequence number:C244(<>Fnd_Pref_Table_ptr->)
			<>Fnd_Pref_OwnerFld_ptr->:=$itemOwner_t
			<>Fnd_Pref_NameFld_ptr->:=$itemName_t
			<>Fnd_Pref_TypeFld_ptr->:=$itemType_i
		End if 
		
		<>Fnd_Pref_TypeFld_ptr->:=$itemType_i
		<>Fnd_Pref_ValueFld_ptr->:=$itemValue_t
		
		SAVE RECORD:C53(<>Fnd_Pref_Table_ptr->)
		UNLOAD RECORD:C212(<>Fnd_Pref_Table_ptr->)
		
	Else 
		  // It's no longer a server or shared pref.
		DELETE RECORD:C58(<>Fnd_Pref_Table_ptr->)
	End if 
End if 