//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_Show4DSplashScreen

  // Allows the developer to display the 4D splash screen so the database can
  //   be used in the User environment.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jan 31, 2004
  // ----------------------------------------------------

BRING TO FRONT:C326(1)  // Bring the User/Custom Menus process to the front.

SET WINDOW RECT:C444(5;55;5+500;55+300)  // Restore the splash screen. 

READ WRITE:C146(*)
