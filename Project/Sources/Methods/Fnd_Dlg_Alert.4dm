//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dlg_Alert (message{; OK button title}) --> Longint

// Replaces 4D's ALERT command.

// Access: Shared

// Parameters: 
//   $1 : Text : The message to display
//   $2 : Text : The OK button text (optional)

// Returns:
//   $0 : Longint : 1 (always)

// Created by Dave Batton on Jul 13, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Removed the redundant call to Fnd_Dlg_Reset.
// Modified by Dave Batton on May 15, 2005
//   Removed the setting of the icon. It will now default to the icon set in Fnd_Dlg_Init.
// Modified by wwn (07/02/09) v11 DIALOG cannot be executed during the draw of a list.
//   DIALOG is called by Fnd_Dlg_FormAlert further down the chain4
// Foundation 6 version by Wayne Stewart on 2021-03-26
// ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1; $2)

If (Form event code:C388#On Display Detail:K2:22)  // do the original Fnd_Dlg_Alert
	
	Fnd_Dlg_SetText($1)
	
	If (Count parameters:C259=2)
		Fnd_Dlg_SetButtons($2)
	End if 
	
	$0:=Fnd_Dlg_Display
Else 
	//ALERT($1)  ` Alert cannot be executed during the draw of a list.
	$0:=OK
End if 
