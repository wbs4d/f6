//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_CommandDialog

  // Displays the Admin dialog.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 19, 2003
  // Modified by Dave Batton on Apr 23, 2005
  //   Updated to use the new Fnd_Wnd_Type method.
  // ----------------------------------------------------

C_LONGINT:C283($element_i)

Fnd_List_Init 

Fnd_Wnd_Type (Movable dialog box:K34:7)
$element_i:=Fnd_List_ChoiceList ("";->Fnd_List_Commands_at)

If (OK=1)
	EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;Fnd_List_Methods_at{$element_i})
End if 
