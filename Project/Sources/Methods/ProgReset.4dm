//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: ProgReset

  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 22, 2004
  // ----------------------------------------------------

  // These two are used even if ThermoSet is not installed.
Fnd_Cmpt_SetLongintValue ("ThermoCan";0)  // Allow the user to cancel progress indicators.
Fnd_Cmpt_SetBooleanValue ("ThermoAbort";False:C215)  // In case someone checks.