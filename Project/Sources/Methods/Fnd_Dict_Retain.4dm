//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_Retain (dict ID)

  // Increments the retain count for a dictionary.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Dictionary ID

  // Returns: Nothing

  // Created by Rob Laveaux
  // ----------------------------------------------------

C_LONGINT:C283($1;$dictionary_i)

$dictionary_i:=$1

Fnd_Dict_LockInternalState (True:C214)

If (Fnd_Dict_IsValid ($dictionary_i))
	<>Fnd_Dict_RetainCounts_ai{$dictionary_i}:=<>Fnd_Dict_RetainCounts_ai{$dictionary_i}+1
End if 

Fnd_Dict_LockInternalState (False:C215)
