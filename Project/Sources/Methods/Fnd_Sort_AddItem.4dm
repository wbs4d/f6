//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Sort_AddItem (position; label; method{; ->field{; type}})

// Called by the protected "Fnd_Sort_Add" routines to add stuff to the arrays.
// If the label is blank, the field name will be used.

// Access: Private

// Parameters: 
//   $1 : Position : The position
//   $2 : Text : The label
//   $3 : Text : The method
//   $4 : Pointer : The field (optional)
//   $5 : Longint : The field's type (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 20, 2003
// ----------------------------------------------------

C_LONGINT:C283($1; $5; $position_i; $fieldType_i; $fieldLength_i)
C_TEXT:C284($2; $3; $fieldName_t; $method_t)
C_POINTER:C301($4; Fnd_Sort_Field_ptr; $null_ptr)
C_BOOLEAN:C305($fieldIndexed_b)

$position_i:=$1
$fieldName_t:=$2
$method_t:=$3
$fieldType_i:=0

If (Count parameters:C259>=4)
	Fnd_Sort_Field_ptr:=$4  // Otherwise Fnd_Sort_Field_ptr will be null, which is okay.
	If ($fieldName_t="")
		$fieldName_t:=Fnd_VS_FieldName(Fnd_Sort_Field_ptr)
		//If (Fnd_Gen_ComponentAvailable("Fnd_VS"))
		//EXECUTE METHOD("Fnd_VS_FieldName"; $fieldName_t; Fnd_Sort_Field_ptr)
		//Else 
		//$fieldName_t:=Field name(Fnd_Sort_Field_ptr)
		//End if 
	End if 
	
	If (Count parameters:C259>=5)
		$fieldType_i:=$5
	Else 
		GET FIELD PROPERTIES:C258(Fnd_Sort_Field_ptr; $fieldType_i; $fieldLength_i; $fieldIndexed_b)
	End if 
End if 

Fnd_Sort_Init

Case of 
	: ($position_i<1)
		$position_i:=1
	: ($position_i>(Size of array:C274(Fnd_Sort_Fields_aptr)+1))
		$position_i:=Size of array:C274(Fnd_Sort_Fields_aptr)+1
End case 

INSERT IN ARRAY:C227(Fnd_Sort_FieldNames_at; $position_i)
Fnd_Sort_FieldNames_at{$position_i}:=$fieldName_t
INSERT IN ARRAY:C227(Fnd_Sort_Fields_aptr; $position_i)
Fnd_Sort_Fields_aptr{$position_i}:=Fnd_Sort_Field_ptr
INSERT IN ARRAY:C227(Fnd_Sort_FieldTypes_ai; $position_i)
Fnd_Sort_FieldTypes_ai{$position_i}:=$fieldType_i
INSERT IN ARRAY:C227(Fnd_Sort_MethodNames_at; $position_i)
Fnd_Sort_MethodNames_at{$position_i}:=$method_t

