//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_BuyNowURL ({feature code{; URL}}) --> Text

  // Allows the developer to get and set the URL to go to if the Buy Now button is clicked.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : A feaure code (optional)
  //   $2 : Text : The URL (optional)

  // Returns: 
  //   $0 : Text : The URL

  // Created by Dave Batton on Apr 27, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$featureCode_t;$url_t)
C_LONGINT:C283($index_i)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

$url_t:=""

If (Not:C34(Semaphore:C143(<>Fnd_Reg_Semaphore_t;300)))  // Wait up to 5 seconds.
	$index_i:=Fnd_Reg_FeatureCodeIndex ($featureCode_t)
	
	If (Count parameters:C259>=2)
		<>Fnd_Reg_BuyNowURLs_at{$index_i}:=$2
	End if 
	
	$url_t:=<>Fnd_Reg_BuyNowURLs_at{$index_i}
	
	CLEAR SEMAPHORE:C144(<>Fnd_Reg_Semaphore_t)
End if 

$0:=$url_t
