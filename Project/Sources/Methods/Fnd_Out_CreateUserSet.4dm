//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_CreateUserSet

// Makes the highlighted rows the "UserSet".

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton, May 2005
// Mod by Wayne Stewart, (2022-04-21)
//  May not be needed as the user set is determined from 
//  Form.selectedItems
// ----------------------------------------------------


//C_LONGINT($element_i;$row_i)

//ARRAY LONGINT($recordIDs_ai;Count in array(Fnd_Out_Listbox_ab;True))
//$element_i:=0

//For ($row_i;1;Size of array(Fnd_Out_Listbox_ab))
//If (Fnd_Out_Listbox_ab{$row_i})
//$element_i:=$element_i+1
//$recordIDs_ai{$element_i}:=Fnd_Out_RecIDs_ai{$row_i}
//End if 
//End for 

//CREATE SET FROM ARRAY(Fnd_Out_CurrentTable_ptr->;$recordIDs_ai)
