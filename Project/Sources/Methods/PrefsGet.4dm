//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: PrefsGet (pref name) --> Text

  // Allows an older Foundation based database to use the new Fnd_Pref routines.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The preference name

  // Returns: 
  //   $0 : Text : The preference value

  // Created by Dave Batton on Feb 22, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Cmpt_Init 

$0:=Fnd_Pref_GetText ($1)
