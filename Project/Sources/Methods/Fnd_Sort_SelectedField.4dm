//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_SelectedField ({->field}) --> Pointer

  // Allows the developer to set or get the currently selected sort field.
  // If a field pointer is specified, this routine will make it the default selection
  //   if it's available in the list. If not, nothing changes.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : The field to default to (optional)

  // Returns: 
  //   $0 : Pointer : The selected sort field

  // Created by Dave Batton on May 21, 2004
  // ----------------------------------------------------

C_POINTER:C301($0;$1;$desiredField_ptr)
C_LONGINT:C283($element_i)

Fnd_Sort_Init 

If (Count parameters:C259>=1)
	$desiredField_ptr:=$1
	If (Not:C34(Is nil pointer:C315($desiredField_ptr)))
		$element_i:=Find in array:C230(Fnd_Sort_Fields_aptr;$desiredField_ptr)
		If ($element_i>0)
			Fnd_Sort_SelectedField_ptr:=Fnd_Sort_Fields_aptr{$element_i}
		End if 
	End if 
End if 

  // Make sure the Fnd_Sort_SelectedField_ptr is available in the
  //   currently initialized arrays (only if they've been set up).
If (Size of array:C274(Fnd_Sort_Fields_aptr)>0)
	$element_i:=Abs:C99(Find in array:C230(Fnd_Sort_Fields_aptr;Fnd_Sort_SelectedField_ptr))
	Fnd_Sort_SelectedField_ptr:=Fnd_Sort_Fields_aptr{$element_i}
End if 

$0:=Fnd_Sort_SelectedField_ptr
