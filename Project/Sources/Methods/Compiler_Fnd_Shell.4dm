//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Shell

// Compiler variables related to the Foundation General routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 15, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Shell_Initialized_b)
If (Not:C34(<>Fnd_Shell_Initialized_b))
	C_BOOLEAN:C305(<>Fnd_Shell_Running_b)
End if 

// The <>Fnd_Shell_NoQuitProcessNames_at array needs special handling.
C_BOOLEAN:C305(<>Fnd_Shell_NoQuitInitialized_b)
If (Not:C34(<>Fnd_Shell_NoQuitInitialized_b))
	ARRAY TEXT:C222(<>Fnd_Shell_NoQuitProcessNames_at; 0)
End if 

// Process variables
C_BOOLEAN:C305(Fnd_Shell_Initialized_b)
If (Not:C34(Fnd_Shell_Initialized_b))
	ARRAY POINTER:C280(Fnd_Shell_TablePtrsArray_aptr; 0)
	ARRAY TEXT:C222(Fnd_Shell_TableNamesArray_at; 0)
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Fnd_Shell_ExcludeFromQuit; $1)
	
	C_TEXT:C284(Fnd_Shell_Info; $0; $1)
	
	C_BOOLEAN:C305(Fnd_Shell_IsRunning; $0)
	
	C_LONGINT:C283(Fnd_Shell_MenuBar; $1)
	
	C_POINTER:C301(Fnd_Shell_OpenTableAdd; ${1})
	
	C_BOOLEAN:C305(Fnd_Shell_QuitAllProcesses; $0)
	
	C_POINTER:C301(Fnd_Shell_SelectTable; $0)
	
	C_TEXT:C284(Fnd_Shell_WriteComments; $1)
	C_LONGINT:C283(Fnd_Shell_WriteComments; $2)
	
	C_LONGINT:C283(Fnd_Shell_DoesMethodExist; $0)
	C_TEXT:C284(Fnd_Shell_DoesMethodExist; $1)
	
	C_OBJECT:C1216(Fnd_Shell_GetFndObject; $0)
	
End if 