//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SaveLocalPrefs

// Writes the preferences file to disk.
// Called from Fnd_Pref_SavePrefs, which handles the semaphore.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Nov 8, 2004
//   Fixed a bug that could cause a preference item with no name to be saved to the file.
// Modified by Dave Batton on Dec 13, 2005
//   Now calls the new Fnd_Pref_Iso2Utf8 routine to create valid UTF-8 files.
// Renamed this method from Fnd_Pref_SavePrefs to Fnd_Pref_SaveLocalPrefs.
// Removed the ALERT at the end. 
// Modified by Dave Batton on Feb 7, 2006
//   Added support for real number variables.
//   Changed the error handler from Fnd_Gen_DummyMethod to Fnd_Pref_DummyMethod.
// Modified by Ed Heckman, October 27, 2015
// Mod by Wayne Stewart, (2019-10-30) - Removed Fnd_Gen_Platform
// ----------------------------------------------------

C_TEXT:C284($path_t;$xml_t;$value_t;$left_t;$top_t;$right_t;$bottom_t;$oldErrorHandler_t;$coordinates_t)
C_TIME:C306($docRef_time)
C_LONGINT:C283($element_i;$separator_i)

Error:=0

$path_t:=Fnd_Pref_Path

If (Test path name:C476($path_t)=Is a document:K24:1)
	// Install a do-nothing error handler so 4D doesn't display its
	//   error message if there's a problem deleting the document.
	$oldErrorHandler_t:=Method called on error:C704
	ON ERR CALL:C155("Fnd_Pref_DummyMethod")  // DB060226
	DELETE DOCUMENT:C159($path_t)
	ON ERR CALL:C155($oldErrorHandler_t)
End if 

$docRef_time:=Create document:C266($path_t;"")

If (OK=1)
	
	
	
	$xml_t:="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
	$xml_t:=$xml_t+"<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" "+"\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\r"
	$xml_t:=$xml_t+"<plist version=\"1.0\">\r"
	$xml_t:=$xml_t+"\t<dict>\r"
	SEND PACKET:C103($docRef_time;$xml_t)
	
	For ($element_i;1;Size of array:C274(<>Fnd_Pref_Names_at))
		If (<>Fnd_Pref_Scopes_ai{$element_i}=0)  // We're only saving the local preferences.
			If (<>Fnd_Pref_Names_at{$element_i}#"")  // DB041108 - Don't save a pref without a name.
				$value_t:=Fnd_Pref_XmlEncode(<>Fnd_Pref_Names_at{$element_i})  // DB051214 - Added
				$xml_t:="\t\t<key>"+$value_t+"</key>\r"
				Case of 
					: (<>Fnd_Pref_Types_ai{$element_i}=Is text:K8:3)
						$value_t:=Fnd_Pref_XmlEncode(<>Fnd_Pref_Values_at{$element_i})  // DB051214 - Added
						$xml_t:=$xml_t+"\t\t<string>"+$value_t+"</string>\r"  // DB051214 - Modified
						
					: (<>Fnd_Pref_Types_ai{$element_i}=Is object:K8:27)
						$value_t:=Fnd_Pref_XmlEncode(<>Fnd_Pref_Values_at{$element_i})  // DB051214 - Added
						$xml_t:=$xml_t+"\t\t<json>"+$value_t+"</json>\r"  // DB051214 - Modified
						
					: (<>Fnd_Pref_Types_ai{$element_i}=Is longint:K8:6)
						$xml_t:=$xml_t+"\t\t<integer>"+<>Fnd_Pref_Values_at{$element_i}+"</integer>\r"
					: (<>Fnd_Pref_Types_ai{$element_i}=Is real:K8:4)  // DB060207 -  New data type.
						$xml_t:=$xml_t+"\t\t<real>"+<>Fnd_Pref_Values_at{$element_i}+"</real>\r"
					: (<>Fnd_Pref_Types_ai{$element_i}=Is boolean:K8:9)
						If (<>Fnd_Pref_Values_at{$element_i}="true")
							$xml_t:=$xml_t+"\t\t<true/>\r"
						Else 
							$xml_t:=$xml_t+"\t\t<false/>\r"
						End if 
					: (<>Fnd_Pref_Types_ai{$element_i}=-1) & (<>Fnd_Pref_Values_at{$element_i}#"")
						$coordinates_t:=<>Fnd_Pref_Values_at{$element_i}  // Make sure we copy this so we don't mess it up.
						$separator_i:=(Position:C15(", ";$coordinates_t))-1
						$left_t:=Substring:C12($coordinates_t;1;$separator_i)
						$coordinates_t:=Substring:C12($coordinates_t;$separator_i+3)
						$separator_i:=(Position:C15(", ";$coordinates_t))-1
						$top_t:=Substring:C12($coordinates_t;1;$separator_i)
						$coordinates_t:=Substring:C12($coordinates_t;$separator_i+3)
						$separator_i:=(Position:C15(", ";$coordinates_t))-1
						$right_t:=Substring:C12($coordinates_t;1;$separator_i)
						$coordinates_t:=Substring:C12($coordinates_t;$separator_i+3)
						$bottom_t:=Substring:C12($coordinates_t;1)
						$xml_t:=$xml_t+"\t\t<dict>\r"
						$xml_t:=$xml_t+"\t\t\t<key>left</key>\r\t\t\t<integer>"+$left_t+"</integer>\r"
						$xml_t:=$xml_t+"\t\t\t<key>top</key>\r\t\t\t<integer>"+$top_t+"</integer>\r"
						$xml_t:=$xml_t+"\t\t\t<key>right</key>\r\t\t\t<integer>"+$right_t+"</integer>\r"
						$xml_t:=$xml_t+"\t\t\t<key>bottom</key>\r\t\t\t<integer>"+$bottom_t+"</integer>\r"
						$xml_t:=$xml_t+"\t\t</dict>\r"
					Else 
						Fnd_Gen_BugAlert(Current method name:C684;"An unexpected data type was set for the \""+<>Fnd_Pref_Names_at{$element_i}+"\" preference.")
				End case 
				//$xml_t:=Mac to ISO($xml_t)  // DB051213 - Added
				$xml_t:=Fnd_Pref_Iso2Utf8($xml_t)  // DB051213 - Added
				SEND PACKET:C103($docRef_time;$xml_t)
			End if 
		End if 
	End for 
	
	$xml_t:="\t</dict>\r"
	$xml_t:=$xml_t+"</plist>\r"
	SEND PACKET:C103($docRef_time;$xml_t)
	
	CLOSE DOCUMENT:C267($docRef_time)
	
Else 
	Error:=1
End if 
