//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_Position ({position setting}) --> Number
// Project Method: Fnd_Wnd_Position (left; top; right; bottom) --> Number

// Allows the developer to get and set the window position for the upcoming window.
//   1 = Fnd_Wnd_CenterOnScreen = Centered on screen
//   2 = Fnd_Wnd_CenterOnWindow = Centered over frontmost window
//   3 = Fnd_Wnd_MacOSXSheet = Sheet if possible, or centered over frontmost window
//   4 = Fnd_Wnd_Stack = Stacked with other stacked windows
//   5 = Fnd_Wnd_StackOnWindow = Stacked over the frontmost window

// Optionally exact window coordinates can be passed.

// This method replaces the original Fnd_Wnd_SetPosition method, which is now obsolete.

// Access Type: Shared

// Parameters: 
//   $1 : Longint : Window position (optional)
//   $2 : Longint : Relative to Window (optional)

// Parameters: 
//   $1 : Longint : Left
//   $2 : Longint : Top 
//   $3 : Longint : Right
//   $4 : Longint : Bottom

// Returns: 
//   $0 : Longint : Current window position setting

// Created by Dave Batton on Apr 23, 2005
// Modified by Dave Batton on Mar 1, 2006
//   Added a new option that allows you to pass all four coordinates to this routine.
// Mod by Wayne Stewart, 2022-04-25 - Allow Relative to parameter
// ----------------------------------------------------

C_LONGINT:C283($0;$1;$2;$3;$4)
C_LONGINT:C283($0)
Fnd_Wnd_Init

Case of 
	: (Count parameters:C259=1)
		Fnd_Wnd_WindowPosition_i:=$1
		
	: (Count parameters:C259=2)
		Fnd_Wnd_WindowPosition_i:=$1
		Fnd.Wnd.relativeTo:=$2
		
	: (Count parameters:C259=4)
		Fnd_Wnd_WindowPosition_i:=6  // New Fnd_Wnd_Exact constant.
		Fnd.Wnd.Left:=$1
		Fnd.Wnd.Top:=$2
		Fnd.Wnd.Right:=$3
		Fnd.Wnd.Bottom:=$4
		
End case 

Case of   // Translate the 4D positioning system to the Foundation positioning system
	: (Fnd_Wnd_WindowPosition_i=(On the left:K39:2+At the top:K39:5))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_OnTheLeft+Fnd_Wnd_AtTheTop
		
	: (Fnd_Wnd_WindowPosition_i=(At the top:K39:5))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_AtTheTop
		
	: (Fnd_Wnd_WindowPosition_i=(On the right:K39:3+At the top:K39:5))  // This is ambiguous with 'On the left+At the bottom' 
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_OnTheRight+Fnd_Wnd_AtTheTop  // Assume top right and let developer know they should be using the 4D constants
		Fnd_Gen_BugAlert("Fnd_Wnd_Position";"The 4D constants 'On the right+At the top' or 'On the left+At the bottom' were passed to the Fnd_Wnd_Position method, please use Fnd_Wnd constants instead.")
		
	: (Fnd_Wnd_WindowPosition_i=(On the right:K39:3))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_OnTheRight
		
	: (Fnd_Wnd_WindowPosition_i=(On the left:K39:2))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_OnTheLeft
		
	: (Fnd_Wnd_WindowPosition_i=(At the bottom:K39:6))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_AtTheBottom
		
	: (Fnd_Wnd_WindowPosition_i=(At the bottom:K39:6+On the right:K39:3))
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_AtTheBottom+Fnd_Wnd_OnTheRight
		
	Else 
		
		//: (Fnd_Wnd_WindowPosition_i=())
		//Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_OnTheLeft+Fnd_Wnd_AtTheTop
		
		
		
		
		
		
		
		
		
End case 



$0:=Fnd_Wnd_WindowPosition_i