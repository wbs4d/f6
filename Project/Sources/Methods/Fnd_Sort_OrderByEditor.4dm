//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_OrderByEditor

  // Displays the simple Sort dialog.  If the Macintosh Option key or Windows Alt
  //   key is down, 4D's Order By editor is displayed instead.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 28, 2003
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Removed description of extraneous parameter in header
  // Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
  // ----------------------------------------------------

C_BOOLEAN:C305($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

Fnd_Sort_Init 

GET AUTOMATIC RELATIONS:C899($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

SET AUTOMATIC RELATIONS:C310(True:C214;True:C214)

If (Macintosh option down:C545)
	ORDER BY:C49(Fnd_Sort_Table_ptr->)
Else 
	ORDER BY:C49(Fnd_Sort_Table_ptr->)
End if 

SET AUTOMATIC RELATIONS:C310($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)
