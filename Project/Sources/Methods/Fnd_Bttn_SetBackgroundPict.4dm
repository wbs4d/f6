//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Bttn_SetBackgroundPict (left, middle, right{; pref left; pref middle; pref right})


// Call this to specify the images to use for the button background.


// Access Type: Shared


// Parameters: 

//   $1 : Picture : The left edge of the button

//   $2 : Picture : The middle of the button (generally 1 pixel wide)

//   $3 : Picture : The right edge of the button

//   $4 : Picture : The left edge of the currently selected button (optional)

//   $5 : Picture : The middle of the currently selected button (optional)

//   $6 : Picture : The right edge of the currently selected button (optional)


// Returns: Nothing


// Created by Mark Mitchenall on 7/8/02

//   Original Method Name: rollover_SetBackgroundPict

//   Part of the toolbar_Component, ©2001 mitchenall.com

//   Used with permission.

// Modified by Dave Batton on Feb 20, 2004

//   Updated specifically for use by the Foundation Shell.

// Modified by Dave Batton on Apr 13, 2005

//   Added support for setting the "Pref" background images.

// ----------------------------------------------------


C_PICTURE:C286($1;$2;$3;$4;$5;$6)

Fnd_Bttn_BG_Left_pic:=$1
Fnd_Bttn_BG_Mid_pic:=$2
Fnd_Bttn_BG_Right_pic:=$3

// Create the backgrounds at the various widths used to build

//   images quicker.

Fnd_Bttn_CreateBackgrounds

// If passed, set the Pref background images.

If (Count parameters:C259>=6)
	Fnd_Bttn_BG_LeftCrnt_pic:=$4
	Fnd_Bttn_BG_MidCrnt_pic:=$5
	Fnd_Bttn_BG_RightCrnt_pic:=$6
End if 