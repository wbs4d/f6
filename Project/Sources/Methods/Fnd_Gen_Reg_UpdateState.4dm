//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Gen_Reg_UpdateState


  // Updates the <>Fnd_Gen_Reg_State_i variable.  Don't check this variable directly,

  //   call Fnd_Gen_Reg_RegistrationState instead.


  // Method Type: Private


  // Parameters: None


  // Returns: 

  //   $0 : Longint : Description


  // Created by Dave Batton on Mar 14, 2003

  // ----------------------------------------------------


If (Fnd_Gen_Reg_Validate (Fnd_Gen_Reg_GetUserName ;Fnd_Gen_Reg_GetActivationCode ))
	<>Fnd_Gen_Reg_State_i:=1
Else 
	<>Fnd_Gen_Reg_State_i:=-1  // Not Registered

End if 
