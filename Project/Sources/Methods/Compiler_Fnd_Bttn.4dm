//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Bttn

//Complete compiler declarations for the rollover component.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Mark Mitchenall on 11/5/01
//   Original Method Name: COMPILER_rollover
//   Part of the toolbar_Component, ©2001 mitchenall.com
// Modified by Dave Batton on Feb 20, 2004
//   Updated specifically for use by the Foundation Shell.
// [1] Added by: John Craig (12/10/09)
// Modified by Wayne Stewart, 2018-04-16 - Removed Disk Cache
// ----------------------------------------------------



C_BOOLEAN:C305(<>Fnd_Bttn_Initialized_b)
If (Not:C34(<>Fnd_Bttn_Initialized_b))  // So we only do this once.
	//Picture cache variables
	ARRAY TEXT:C222(<>Fnd_Bttn_CacheNames_at; 0)  // composite name of the image
	ARRAY PICTURE:C279(<>Fnd_Bttn_CachePictures_apic; 0)  // actual images
	ARRAY INTEGER:C220(<>Fnd_Bttn_PictureWidths_ai; 0)  // width of the images
	ARRAY LONGINT:C221(<>Fnd_Bttn_CachePictSizes_ai; 0)  // size of the images
	ARRAY LONGINT:C221(<>Fnd_Bttn_Tickcounts_ai; 0)  // tickcounts of when image last used
	
	C_LONGINT:C283(<>Fnd_Bttn_CacheTotalSize_i)
	C_LONGINT:C283(<>Fnd_Bttn_CacheMaximumSize_i)
	C_LONGINT:C283(<>Fnd_Bttn_CacheTotalHits_i)
	C_LONGINT:C283(<>Fnd_Bttn_CacheTotalMisses_i)
	C_TEXT:C284(<>Fnd_Bttn_PicturePath_t)  // [1]
	C_TEXT:C284(<>Fnd_Bttn_AltPicturePath_t)
	
End if 


C_BOOLEAN:C305(Fnd_Bttn_Initialized_b)
If (Not:C34(Fnd_Bttn_Initialized_b))  // So we only do this once per process.
	C_TEXT:C284(Fnd_Bttn_ErrorText)
	C_TEXT:C284(Fnd_Bttn_FontName_t)
	C_TEXT:C284(Fnd_Bttn_Platform_t)
	C_TEXT:C284(Fnd_Bttn_Style_t)
	
	C_LONGINT:C283(Fnd_Bttn_TextLeftOffset_i; Fnd_Bttn_TextTopOffset_i)
	C_LONGINT:C283(Fnd_Bttn_RowBytes_i)
	C_LONGINT:C283(Fnd_Bttn_Top_i)
	C_LONGINT:C283(Fnd_Bttn_Left_i)
	C_LONGINT:C283(Fnd_Bttn_FontSize_i)
	C_LONGINT:C283(Fnd_Bttn_FontStyle_i)
	C_LONGINT:C283(Fnd_Bttn_LabelPlacement_i)
	C_LONGINT:C283(Fnd_Bttn_TextDownStyle_i)
	C_LONGINT:C283(Fnd_Bttn_ButtonHeight_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_IconLeftOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_IconRightOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_IconTopOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_TextLeftOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_TextRightOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_TextTopOffset_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_TextRightPadding_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_TextBottomPadding_i)  //Initialized in rollover_Preferences
	C_LONGINT:C283(Fnd_Bttn_LeftEdgeSize_i)  //         Initialized in rollover_Startup
	C_LONGINT:C283(Fnd_Bttn_RightEdgeSize_i)  //       Initialized in rollover_Startup
	C_LONGINT:C283(Fnd_Bttn_MinButtonWidth_i)
	C_LONGINT:C283(Fnd_Bttn_MenuIndicatorHOffset_i)
	C_LONGINT:C283(Fnd_Bttn_MenuIndicatorVOffset_i)
	C_LONGINT:C283(Fnd_Bttn_MenuIndicatorPadding_i)
	
	C_PICTURE:C286(Fnd_Bttn_BG_Left_pic; Fnd_Bttn_BG_Right_pic; Fnd_Bttn_BG_Mid_pic)
	C_PICTURE:C286(Fnd_Bttn_BG_LeftCrnt_pic; Fnd_Bttn_BG_RightCrnt_pic; Fnd_Bttn_BG_MidCrnt_pic)
	C_PICTURE:C286(Fnd_Bttn_IconImage_pic)  // [1]
	
	ARRAY INTEGER:C220(Fnd_Bttn_HorizonalShift_ai; 0)
	ARRAY INTEGER:C220(Fnd_Bttn_VerticalShift_ai; 0)
	
	ARRAY PICTURE:C279(Fnd_Bttn_BG_Middles_apic; 0)
End if 

//wrapped in "If(False)...End If" so that the compiler method can be called on
//startup without getting syntax errors.
If (False:C215)
	C_TEXT:C284(Fnd_Bttn_Cache_Image_Add; $1)
	C_PICTURE:C286(Fnd_Bttn_Cache_Image_Add; $2)
	
	C_TEXT:C284(Fnd_Bttn_Cache_Image_Delete; $1)
	
	C_PICTURE:C286(Fnd_Bttn_Cache_Image_Get; $0)
	C_TEXT:C284(Fnd_Bttn_Cache_Image_Get; $1)
	
	C_TEXT:C284(Fnd_Bttn_ColorToString; $0)
	C_LONGINT:C283(Fnd_Bttn_ColorToString; $1)
	
	C_POINTER:C301(Fnd_Bttn_CreateButton; $1; $2)
	C_TEXT:C284(Fnd_Bttn_CreateButton; $3; $4)
	
	C_PICTURE:C286(Fnd_Bttn_GetIcon; $0)
	C_TEXT:C284(Fnd_Bttn_GetIcon; $1; $2)
	
	C_PICTURE:C286(Fnd_Bttn_GetPicture; $0)
	C_TEXT:C284(Fnd_Bttn_GetPicture; $1; $2; $3)
	
	C_TEXT:C284(Fnd_Bttn_GetPictureName; $0; $1; $2; $3)
	
	C_LONGINT:C283(Fnd_Bttn_GetPictureWidth; $0)
	C_TEXT:C284(Fnd_Bttn_GetPictureWidth; $1; $2; $3)
	
	C_TEXT:C284(Fnd_Bttn_Info; $0; $1)
	
	C_PICTURE:C286(Fnd_Bttn_MakeBackground; $0)
	C_LONGINT:C283(Fnd_Bttn_MakeBackground; $1)
	
	C_TEXT:C284(Fnd_Bttn_MakeButton; $1; $2; $3)
	C_PICTURE:C286(Fnd_Bttn_MakeButton; $0)
	
	C_PICTURE:C286(Fnd_Bttn_MakeCrntBackground; $0)
	C_LONGINT:C283(Fnd_Bttn_MakeCrntBackground; $1)
	
	C_PICTURE:C286(Fnd_Bttn_MakeIconPicture; $0)
	C_TEXT:C284(Fnd_Bttn_MakeIconPicture; $1)
	
	C_PICTURE:C286(Fnd_Bttn_MakeTextPicture; $0)
	C_TEXT:C284(Fnd_Bttn_MakeTextPicture; $1)
	
	C_PICTURE:C286(Fnd_Bttn_MakeMenuIndicatorPic; $0)
	
	C_TEXT:C284(Fnd_Bttn_Platform; $0; $1)
	
	C_TEXT:C284(Fnd_Bttn_GetSizedIconName; $0; $1)
	
	C_TEXT:C284(Fnd_Bttn_Style; $0; $1)
	
	C_PICTURE:C286(Fnd_Bttn_TextToPict; $0)
	C_TEXT:C284(Fnd_Bttn_TextToPict; $1; $2)
	C_LONGINT:C283(Fnd_Bttn_TextToPict; $3; $4; $5; $6; $7)
	
	C_TEXT:C284(Fnd_Bttn_SetTextProperties; $1)
	C_LONGINT:C283(Fnd_Bttn_SetTextProperties; $2; $3)
	
	C_PICTURE:C286(Fnd_Bttn_SetBackgroundPict; $1; $2; $3; $4; $5; $6)
	
	C_LONGINT:C283(Fnd_Bttn_CacheMaximumSize; $1; $0)
	
	C_TEXT:C284(Fnd_Bttn_AlternativePicturePath; $1; $0)
	
End if 