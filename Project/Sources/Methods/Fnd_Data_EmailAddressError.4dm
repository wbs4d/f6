//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_EmailAddressError (email address) --> Longint

  // Checks Email address and returns an error number if the email address doesn't have a valid format.
  // The original code returned an error message with specific information about the problem.
  //   I'm not currently using that information, but I've left it in here in case you want to take
  //   advantage of it, or in case I decide to later.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The email address to check

  // Returns: 
  //   $0 : Longint : Error number (0=valid format, 1=invalid format)

  // Based on code posted to the 4D NUG by Owen Watson<owen.watson@paradise.net.nz>.
  // Created by Dave Batton on Apr 28, 2004
  // Modified by Dave Batton on Oct 1, 2004
  //   Changed the variable names to follow the Foundation naming conventions.
  //   No longer returns an error if the email address is blank.
  //   Now checks to make sure there's a period in the domain name.
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Explained return value description in header
  // ----------------------------------------------------

C_LONGINT:C283($0;$atPosition_i;$position_i;$ascii_i;$startPosition_i;$endPosition_i)
C_TEXT:C284($1;$errorDescription_t;$emailAddress_t;$mailbox_t;$domain_t;$domainErrorDescription_t;$mailboxErrorDescription_t)
C_BOOLEAN:C305($mailboxOK_b;$domainOK_b;$addressOK_b)

Fnd_Data_Init 

$emailAddress_t:=$1

$errorDescription_t:=""
$mailboxOK_b:=False:C215
$domainOK_b:=False:C215
$addressOK_b:=True:C214

$startPosition_i:=Position:C15("<";$emailAddress_t)
$endPosition_i:=Position:C15(">";$emailAddress_t)
If (($startPosition_i>0) & ($endPosition_i>$startPosition_i))
	$emailAddress_t:=Substring:C12($emailAddress_t;$startPosition_i+1;($endPosition_i-$startPosition_i-1))
End if 

If ($emailAddress_t#"")
	$atPosition_i:=Position:C15("@";$emailAddress_t)
	If ($atPosition_i>0)
		$mailbox_t:=Substring:C12($emailAddress_t;1;$atPosition_i-1)
		$domain_t:=Substring:C12($emailAddress_t;$atPosition_i+1)
		If (($mailbox_t#"") & ($domain_t#""))
			For ($position_i;1;Length:C16($domain_t))
				$ascii_i:=Character code:C91($domain_t[[$position_i]])
				  //Check for < > ( ) [ ] \ , ; : " @ % control chars and hi-ascii chars.
				If ((($ascii_i>=65) & ($ascii_i<=90)) | (($ascii_i>=97) & ($ascii_i<=122)) | (($ascii_i>=48) & ($ascii_i<=57)) | (($ascii_i>=45) & ($ascii_i<=46)))
					$domainOK_b:=True:C214
				Else 
					$domainErrorDescription_t:="The '"+Char:C90($ascii_i)+"' character isn't allowed."
					Case of 
						: ($ascii_i=32)  //space
							$domainErrorDescription_t:="There is a space in the address."
						: ($ascii_i<32)  //cntrl char
							$domainErrorDescription_t:="There is an invisible character (ASCII "+String:C10($ascii_i)+") in the address."
					End case 
					$position_i:=Length:C16($domain_t)
					$domainOK_b:=False:C215
				End if 
			End for 
			For ($position_i;1;Length:C16($mailbox_t))
				$ascii_i:=Character code:C91($mailbox_t[[$position_i]])
				  //now checks for < > ( ) [ ] \ , ; : " @ % control chars and hi-ascii chars
				If ((($ascii_i<33) | ($ascii_i>126)) | ($ascii_i=60) | ($ascii_i=62) | ($ascii_i=40) | ($ascii_i=41) | ($ascii_i=91) | ($ascii_i=93) | ($ascii_i=92) | ($ascii_i=44) | ($ascii_i=59) | ($ascii_i=58) | ($ascii_i=34) | ($ascii_i=64) | ($ascii_i=37))
					$mailboxOK_b:=False:C215
					$mailboxErrorDescription_t:="The '"+Char:C90($ascii_i)+"' character isn't allowed."
					Case of 
						: ($ascii_i=32)  //space
							$mailboxErrorDescription_t:="There is a space in the address."
						: ($ascii_i<32)  //cntrl char
							$mailboxErrorDescription_t:="There is an invisible character (ASCII "+String:C10($ascii_i)+") in the address."
					End case 
					$position_i:=Length:C16($mailbox_t)
				Else 
					$mailboxOK_b:=True:C214
				End if 
			End for 
			
			$addressOK_b:=False:C215
			Case of 
				: (Not:C34($mailboxOK_b))  //mailbox name malformed
					$errorDescription_t:=$mailboxErrorDescription_t
				: (Not:C34($domainOK_b))  //domain name malformed
					$errorDescription_t:=$domainErrorDescription_t
				: (Position:C15(".";$domain_t)=0)  //two sequential periods in address
					$errorDescription_t:="A period is required in the domain."
				: (Position:C15("..";$emailAddress_t)>0)  //two sequential periods in address
					$errorDescription_t:="Two periods together are not allowed."
				: (($mailbox_t[[1]]=".") | ($mailbox_t[[Length:C16($mailbox_t)]]=".") | ($domain_t[[1]]=".") | ($domain_t[[Length:C16($domain_t)]]="."))  //mailbox or domain starts or ends with a period
					$errorDescription_t:="Periods are not allowed at beginning or end of mailbox or domain."
				Else 
					$addressOK_b:=True:C214
			End case 
		Else 
			$addressOK_b:=False:C215
		End if 
	Else 
		$addressOK_b:=False:C215
	End if 
End if 

If ($addressOK_b)
	$0:=0
Else 
	$0:=1
End if 
