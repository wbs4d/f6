//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Put (sequence number group name; number to return)

  // Call this method to return a number to the specified sequence number group.
  //   If the number is higher than the current next number, it will be ignored. It will
  //   also be ignored if it's already in the list of numbers to reuse, or if the group
  //   name doesn't already exist.

  // This method always starts a new process to return the sequence number so the
  //   calling process doesn't have to wait for it to finish.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The group name
  //   $1 : Longint : The number to reuse

  // Returns: Nothing

  // Created by Dave Batton on Nov 19, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$seqNoName)
C_LONGINT:C283($2;$seqNo;$processNumber)

$seqNoName:=$1
$seqNo:=$2

Fnd_SqNo_Init 

  // Start the SeqNoPut process. Always start a new process. We can have lots of 
  //   these running if necessary.  They're small and quite submissive.
$processNumber:=New process:C317("Fnd_SqNo_Put2";Fnd_Gen_DefaultStackSize;"Fnd_SqNo_Put process";$seqNoName;$seqNo)