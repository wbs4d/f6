//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_ParseName (name; ->first name{; ->last name{; ->middle initial}})

  // Attempts to split a person's full name into separate parts.
  // Drops titles such as Mr. Mrs. Jr., Sr., M.D., etc.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name to parse
  //   $2 : Pointer : Gets the first name
  //   $3 : Pointer : Gets the last name (optional)
  //   $4 : Pointer : Gets the middle initial (optional)

  // Returns: Nothing

  // Created by Dave Batton on May 12, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Removed extraneous return parameter description in header
  //   Corrected method name in first line of header
  // ----------------------------------------------------

C_TEXT:C284($1;$fullName_t;$middleInitial_t)
C_POINTER:C301($2;$3;$4;$firstName_ptr;$lastName_ptr;$middleInitial_ptr)
C_LONGINT:C283($element_i;$spacePosition_i)

Fnd_Data_Init 

$fullName_t:=$1
$firstName_ptr:=$2
If (Count parameters:C259>=3)
	$lastName_ptr:=$3
	If (Count parameters:C259>=4)
		$middleInitial_ptr:=$4
	End if 
End if 

  // Remove titles like Mr., Dr., etc.
$fullName_t:=" "+$fullName_t+" "
For ($element_i;1;Size of array:C274(<>Fnd_Data_NameTitles_at))
	$fullName_t:=Replace string:C233($fullName_t;<>Fnd_Data_NameTitles_at{$element_i};" ")
	$fullName_t:=Replace string:C233($fullName_t;Replace string:C233(<>Fnd_Data_NameTitles_at{$element_i};".";"");" ")  // Again, without periods.
End for 

  // Now remove any periods that were orphned.
$fullName_t:=Replace string:C233($fullName_t;" . ";"")

  // Get rid of all of the extraneous spaces we've created.
$fullName_t:=Fnd_Text_StripSpaces ($fullName_t;1+2+4)  // Remove leading, trailing, and double spaces.

  // Find the last space character. This is the start of the last name (we hope).
$spacePosition_i:=Fnd_Gen_PositionR (" ";$fullName_t)

  // If there's a period at the end of the first name, remove it.
$firstName_ptr->:=Substring:C12($fullName_t;1;$spacePosition_i-1)
If ($firstName_ptr->="@.")
	$firstName_ptr->:=Substring:C12($firstName_ptr->;1;Length:C16($firstName_ptr->)-1)
End if 

  // If there's a letter by itself at the end of the first name, get it, then remove it.
If (Substring:C12($firstName_ptr->;Length:C16($firstName_ptr->)-1;1)=" ")
	$middleInitial_t:=$firstName_ptr->[[Length:C16($firstName_ptr->)]]
	$firstName_ptr->:=Substring:C12($firstName_ptr->;1;Length:C16($firstName_ptr->)-2)
Else 
	$middleInitial_t:=""
End if 

  // Get the last name if requested.
If (Count parameters:C259>=3)
	$lastName_ptr->:=Substring:C12($fullName_t;$spacePosition_i+1)
	If (Count parameters:C259>=4)
		$middleInitial_ptr->:=$middleInitial_t
	End if 
End if 