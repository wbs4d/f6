//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_LoadStringList (module name)

  // Loads arrays with localized strings.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The Foundation module name (eg: "Fnd_Dlg")

  // Returns: Nothing

  // Created by Dave Batton on Jul 15, 2003
  // Modified by Dave Batton on May 20, 2004
  //   Now load the English list if current language list isn't available.
  // ----------------------------------------------------

C_TEXT:C284($1;$moduleName_t;$stringsListName_t)
C_LONGINT:C283($element_i)

$moduleName_t:=$1

$stringsListName_t:=$moduleName_t+"_"+<>Fnd_Loc_CurrentLanguage_t

$element_i:=Find in array:C230(<>Fnd_Loc_Modules_at;$moduleName_t)
If ($element_i=-1)
	$element_i:=Size of array:C274(<>Fnd_Loc_Modules_at)+1
	INSERT IN ARRAY:C227(<>Fnd_Loc_Modules_at;$element_i)
	INSERT IN ARRAY:C227(<>Fnd_Loc_LookupCodes_at;$element_i)
	INSERT IN ARRAY:C227(<>Fnd_Loc_Strings_at;$element_i)
End if 

<>Fnd_Loc_Modules_at{$element_i}:=$moduleName_t
Fnd_Loc_ListToArray ($moduleName_t;-><>Fnd_Loc_LookupCodes_at{$element_i})
Fnd_Loc_ListToArray ($stringsListName_t;-><>Fnd_Loc_Strings_at{$element_i})

  // If we don't have a localization list for this language, try English.
If ((Size of array:C274(<>Fnd_Loc_Strings_at{$element_i})#Size of array:C274(<>Fnd_Loc_LookupCodes_at{$element_i})) & (<>Fnd_Loc_CurrentLanguage_t#"EN"))
	$stringsListName_t:=$moduleName_t+"_EN"
	Fnd_Loc_ListToArray ($stringsListName_t;-><>Fnd_Loc_Strings_at{$element_i})
End if 

If (Size of array:C274(<>Fnd_Loc_Strings_at{$element_i})#Size of array:C274(<>Fnd_Loc_LookupCodes_at{$element_i}))
	Fnd_Gen_BugAlert (Current method name:C684;"There is a problem with the \""+$moduleName_t+"\" localization list.")
End if 
