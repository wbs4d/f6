//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Remove (buttonName)

// Call this method to remove the button with the specified name from the toolbar.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the button to remove

// Returns: Nothing

// Created by Dave Batton on Jan 6, 2005
// Toolbar Component - © mitchenall.com 2002
// Modified by Dave Batton on May 10, 2005
//   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
// ----------------------------------------------------

C_TEXT:C284($1;$buttonName_t)
C_LONGINT:C283($buttonNumber_i)

$buttonName_t:=$1

Fnd_Tlbr_Init

$buttonNumber_i:=Fnd_Tlbr_Object_GetNumber($buttonName_t)

If ($buttonNumber_i>0)
	// We normally keep the arrays set to the max number of elements,
	//   so we must shift the existing elements, rather than delete them.
	For ($buttonNumber_i;$buttonNumber_i;(Fnd_Tlbr_UsedObjects_i-1))
		Fnd_Tlbr_ObjectNames_at{$buttonNumber_i}:=Fnd_Tlbr_ObjectNames_at{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectLabels_at{$buttonNumber_i}:=Fnd_Tlbr_ObjectLabels_at{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectIconNames_at{$buttonNumber_i}:=Fnd_Tlbr_ObjectIconNames_at{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectTypes_ai{$buttonNumber_i}:=Fnd_Tlbr_ObjectTypes_ai{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectEnabled_ab{$buttonNumber_i}:=Fnd_Tlbr_ObjectEnabled_ab{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectMethods_at{$buttonNumber_i}:=Fnd_Tlbr_ObjectMethods_at{$buttonNumber_i+1}
		Fnd_Tlbr_ObjectShortcuts_at{$buttonNumber_i}:=Fnd_Tlbr_ObjectShortcuts_at{$buttonNumber_i+1}
	End for 
	
	Fnd_Tlbr_UsedObjects_i:=Fnd_Tlbr_UsedObjects_i-1
	Fnd_Tlbr_ButtonsAreValid_b:=False:C215
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Button name not found: "+$buttonName_t)
End if 