//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_UpdateItemsList

  // Assumes the proper [Lists] record is loaded.
  // Assumes any previous changes to the list have been saved.
  // Called from the [Constants].ListEditor form.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Dec 21, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($formula_t)

If (<>Fnd_List_ListItemsModified_b)
	Fnd_Gen_BugAlert (Current method name:C684;"Attempted to update the items list before saving "+"changes to the currently displayed list.")
End if 

  // Get rid of anything currently in the Items list.
CLEAR LIST:C377(<>Fnd_List_ListItems_i;*)

$formula_t:="$2->:="+Command name:C538(383)+"(\""+<>Fnd_List_SelectedListName_t+"\")"  // 383 = Load list
EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t;-><>Fnd_List_ListItems_i)

  // If this list didn't exist, create a new one.
If (Not:C34(Is a list:C621(<>Fnd_List_ListItems_i)))
	<>Fnd_List_ListItems_i:=New list:C375
End if 

OBJECT SET ENTERABLE:C238(<>Fnd_List_ListItems_i;True:C214)
SELECT LIST ITEMS BY POSITION:C381(<>Fnd_List_ListItems_i;Num:C11(Count list items:C380(<>Fnd_List_ListItems_i)>0))  // 4D will highlight it anyway. This just causes our buttons to enable.
  //REDRAW LIST(<>Fnd_List_ListItems_i)