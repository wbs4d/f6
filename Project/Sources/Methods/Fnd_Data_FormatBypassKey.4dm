//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatBypassKey (key) --> Longint

  // Pass a key mask to set a new bypass key.
  // Pass 0 to disable the bypass option.
  // Pass 1 to always bypass the format routines (turn off formatting).
  // Returns the currently set bypass key.
  // Bypass key can be any of these 4D constants:
  //   Option key mask (default)
  //   Command key mask
  //   Control key mask
  //   Shift key mask
  //   Caps lock key mask

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The new bypass key mask (optional)

  // Returns: 
  //   $0 : Longint : The current bypass key mask

  // Created by Dave Batton on May 17, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$1)

Fnd_Data_Init 

If (Count parameters:C259>=1)
	<>Fnd_Data_UserBypassKey_i:=$1
End if 

$0:=<>Fnd_Data_UserBypassKey_i
