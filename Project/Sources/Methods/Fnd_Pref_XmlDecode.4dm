//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_XmlDecode (text) --> Text

  // Decodes reserved XML characters. For example, &gt; is converted to >.
  // Unlike HTML encoding, this routine only handles reserved XML characters,
  //   since accented characters are handled by the UTF-8 encoding.
  // Call Fnd_Pref_XmlEncode to reverse the process.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : An encoded text string

  // Returns: 
  //   $0 : Text : The unencoded text string

  // Created by Dave Batton on Dec 13, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$text_t)

$text_t:=$1

$text_t:=Replace string:C233($text_t;"&lt;";"<")
$text_t:=Replace string:C233($text_t;"&gt;";">")
$text_t:=Replace string:C233($text_t;"&quot;";Char:C90(Double quote:K15:41))
$text_t:=Replace string:C233($text_t;"&apos;";Char:C90(Quote:K15:44))
$text_t:=Replace string:C233($text_t;"&amp;";"&")  // Best if done last.

$0:=$text_t