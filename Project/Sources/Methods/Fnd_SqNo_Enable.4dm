//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Enable (group name{; start number})

  // Creates the sequence number group if it doesn't already exist.  Optionally starts
  //   the numbering at the specified number.  Otherwise it starts at 1.
  // The new group will be editable by the database administrator.
  // Doesn't change the [Fnd_SqNo] table's read/write state.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The group name
  //   $2 : Longint : The starting number (optional)

  // Returns: Nothing

  // Created by Dave Batton on Dec 15, 2003
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_LONGINT:C283($2;$startNumber_i)
C_BOOLEAN:C305($wasReadOnly_b)

$groupName_t:=$1

If (Count parameters:C259>=2)
	$startNumber_i:=$2
Else 
	$startNumber_i:=1
End if 

Fnd_SqNo_Init 

$wasReadOnly_b:=Read only state:C362(<>Fnd_SqNo_Table_ptr->)

READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)
QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_GroupNameFld_ptr->=$groupName_t)

If (Records in selection:C76(<>Fnd_SqNo_Table_ptr->)=0)
	CREATE RECORD:C68(<>Fnd_SqNo_Table_ptr->)
	<>Fnd_SqNo_IDFld_ptr->:=Sequence number:C244(<>Fnd_SqNo_Table_ptr->)
	<>Fnd_SqNo_GroupNameFld_ptr->:=$groupName_t
End if 

If (<>Fnd_SqNo_NextNumberFld_ptr-><$startNumber_i)
	<>Fnd_SqNo_NextNumberFld_ptr->:=$startNumber_i
End if 

If (<>Fnd_SqNo_DesignerOnlyFld_ptr->)
	<>Fnd_SqNo_DesignerOnlyFld_ptr->:=False:C215  // Make sure the Administrator can access the list.
End if 

SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)
UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure other users can load the record.

If ($wasReadOnly_b)
	READ ONLY:C145(<>Fnd_SqNo_Table_ptr->)
End if 