//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Wnd_SendCloseRequests ({process number})


// Adds the current process to the list of processes with windows that should be

//   closed if the user option-closes a window or selects Close All Windows

//   from a menu.

// Both the process name and number are stored, to make sure we're trying

//   to close the same process that was submitted here.


// Access Type: Shared


// Parameters: 

//   $1 : Longint : Process number (optional)


// Returns: Nothing


// Created by Dave Batton on Sep 12, 2003

// ----------------------------------------------------


C_LONGINT:C283($1;$processNumber_i;$processState_i;$processTime_i;$element_i)
C_TEXT:C284($processName_t)

If (Count parameters:C259>=1)
	$processNumber_i:=$1
Else 
	$processNumber_i:=Current process:C322
End if 

Fnd_Wnd_Init

PROCESS PROPERTIES:C336($processNumber_i;$processName_t;$processState_i;$processTime_i)

If ($processState_i>=0)  // Don't bother with processes that aren't running.
	
	
	// Add the process number to the name to make sure it's unique.
	
	$processName_t:=$processName_t+":"+String:C10($processNumber_i)
	
	$element_i:=Find in array:C230(<>Fnd_Wnd_CloseProcessNames_at;$processName_t)
	If ($element_i=-1)
		$element_i:=Size of array:C274(<>Fnd_Wnd_CloseProcessNames_at)+1
		INSERT IN ARRAY:C227(<>Fnd_Wnd_CloseProcessNames_at;$element_i)
	End if 
	
	<>Fnd_Wnd_CloseProcessNames_at{$element_i}:=$processName_t
End if 