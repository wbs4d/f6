//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_GetActivationCode --> Text

  // Returns the activation code that's been stored in the list.
  //   Returns an empty string if the program hasn't been registered yet.

  // Method Type: Private

  // Parameters: None

  // Returns: 
  //   $0 : Text : The activation code

  // Created by Dave Batton on August 16, 2003
  // ----------------------------------------------------

C_TEXT:C284($0)

$0:=Fnd_Gen_Reg_GetValueForElement ("activation_code")
