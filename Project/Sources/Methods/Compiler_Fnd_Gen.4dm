//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Gen

// Compiler variables related to the Foundation General routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 21, 2003
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Gen_Initialized_b)
If (Not:C34(<>Fnd_Gen_Initialized_b))
	
	C_LONGINT:C283(<>Fnd_Gen_OKButton_i)
	C_LONGINT:C283(<>Fnd_Gen_Temp_i)  // Fnd_Gen_Init
	C_TEXT:C284(<>Fnd_Gen_DatabaseName_t)
	C_TEXT:C284(<>Fnd_Gen_DatabaseVersion_t)
	C_TEXT:C284(<>Fnd_Gen_MenuName_t)
	C_TEXT:C284(<>Fnd_Gen_Title_t; <>Fnd_Gen_Message_t; <>Fnd_Gen_ButtonText_t)
	C_TEXT:C284(<>Fnd_Gen_LocSemaphore_t)
	C_BOOLEAN:C305(<>Fnd_Gen_Quitting_b)
	C_POINTER:C301(<>Fnd_Gen_Field_ptr)  // Fnd_Gen_VerifyStructureItem
	C_BOOLEAN:C305(<>Fnd_Gen_RunningInHost_b)  // Modified by: Wayne Stewart(2nd March 2009)
	
	ARRAY TEXT:C222(<>Fnd_Gen_DatabaseInfoLabels_at; 0)
	ARRAY TEXT:C222(<>Fnd_Gen_DatabaseInfoValues_at; 0)
	
	ARRAY TEXT:C222(<>Fnd_Gen_InstalledComponents_at; 0)
	ARRAY TEXT:C222(<>Fnd_Gen_UnavailableComponents_a; 0)
	
	ARRAY TEXT:C222(<>Fnd_Gen_LocModules_at; 0)
	ARRAY TEXT:C222(<>Fnd_Gen_LocLookupCodes_at; 0; 0)
	ARRAY TEXT:C222(<>Fnd_Gen_LocStrings_at; 0; 0)
	C_BOOLEAN:C305(<>Fnd_Gen_WaitToContinue_b)  // Modified by: Walt Nelson(2/19/10) per Wayne Stewart
	
End if 


// Process variables
C_BOOLEAN:C305(Fnd_Gen_Initialized_b)
If (Not:C34(Fnd_Gen_Initialized_b))
	C_LONGINT:C283(Fnd_Gen_Left_i; Fnd_Gen_Top_i; Fnd_Gen_Right_i; Fnd_Gen_Bottom_i)
	C_LONGINT:C283(Fnd_Gen_Result_i)
	C_LONGINT:C283(Fnd_Gen_CurrentFormType_i)
	
	C_TEXT:C284(Fnd_Gen_Result_t)
	
	C_BOOLEAN:C305(Fnd_Gen_ProcessLaunched_b)
	C_BOOLEAN:C305(Fnd_Gen_CloseNow_b)
	
	C_POINTER:C301(Fnd_Gen_CurrentTable_ptr)
	C_LONGINT:C283(HEADER1)  // v13 compatibility for Listboxes adds a header variable 120401: wwn
	C_LONGINT:C283(HEADER2)
	C_LONGINT:C283(HEADER3)
	C_LONGINT:C283(HEADER4)
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	// BEGIN Modification by : Vincent TOURNIER(9/12/11)
	C_BOOLEAN:C305(Fnd_Gen_IsRunning; $0)
	
	C_BOOLEAN:C305(Fnd_Gen_Quitting; $1)
	C_BOOLEAN:C305(Fnd_Gen_Quitting; $0)
	// END Modification by : Vincent TOURNIER(9/12/11)
	
	C_TEXT:C284(Fnd_Gen_Alert; $1; $2)
	
	C_TEXT:C284(Fnd_Gen_BugAlert; $1; $2)
	
	C_LONGINT:C283(Fnd_Gen_ButtonText; $0; $3)
	C_VARIANT:C1683(Fnd_Gen_ButtonText; $1; ${4})
	C_TEXT:C284(Fnd_Gen_ButtonText; $2)
	
	C_LONGINT:C283(Fnd_Gen_CenterWindow; $0; $1; $2; $3; $4)
	C_TEXT:C284(Fnd_Gen_CenterWindow; $5)
	C_BOOLEAN:C305(Fnd_Gen_CenterWindow; $6)
	
	C_BOOLEAN:C305(Fnd_Gen_ComponentAvailable; $0)
	C_TEXT:C284(Fnd_Gen_ComponentAvailable; $1)
	
	C_TEXT:C284(Fnd_Gen_ComponentCheck; $1; $2)
	
	C_POINTER:C301(Fnd_Gen_ComponentData; $1; $2; $3)
	
	C_TEXT:C284(Fnd_Gen_ComponentInfo; $0; $1; $2)
	
	C_LONGINT:C283(Fnd_Gen_CurrentFormType; $0; $1)
	
	C_POINTER:C301(Fnd_Gen_CurrentTable; $0; $1)
	
	C_TEXT:C284(Fnd_Gen_FileName; $0; $1)
	
	C_LONGINT:C283(Fnd_Gen_GetArrayWidth; $0; $3; $4)
	C_POINTER:C301(Fnd_Gen_GetArrayWidth; $1)
	C_TEXT:C284(Fnd_Gen_GetArrayWidth; $2)
	
	C_TEXT:C284(Fnd_Gen_GetDatabaseInfo; $0; $1)
	
	C_TEXT:C284(Fnd_Gen_GetString; $0; $1; $2; $3; $4; $5; $6)
	
	C_TEXT:C284(Fnd_Gen_Info; $0; $1)
	
	C_BOOLEAN:C305(Fnd_Gen_LaunchAsNewProcess; $0)
	C_TEXT:C284(Fnd_Gen_LaunchAsNewProcess; $1; $2)
	
	C_TEXT:C284(Fnd_Gen_MenuBar; $1)
	
	C_LONGINT:C283(Fnd_Gen_Platform; $0)
	
	C_BOOLEAN:C305(Fnd_Gen_PlugInAvailable; $0)
	C_TEXT:C284(Fnd_Gen_PlugInAvailable; $1)
	
	C_LONGINT:C283(Fnd_Gen_PositionR; $0)
	C_TEXT:C284(Fnd_Gen_PositionR; $1; $2)
	
	C_BOOLEAN:C305(Fnd_Gen_QuitNow; $0; $1)
	
	C_TEXT:C284(Fnd_Gen_RequiredHostMethod; $1)
	C_TEXT:C284(Fnd_Gen_RequiredHookMethod; $1)  // Added  by: Walt Nelson(Apr 25 2009)
	
	C_TEXT:C284(Fnd_Gen_SetDatabaseInfo; $1; $2)
	
	C_TEXT:C284(Fnd_Gen_SetLocalizationStrings; $1)
	C_POINTER:C301(Fnd_Gen_SetLocalizationStrings; $2; $3)
	
	C_BOOLEAN:C305(Fnd_Gen_ToolbarDisplayed; $0)
	
	C_POINTER:C301(Fnd_Gen_VerifyStructureItem; $0)
	C_TEXT:C284(Fnd_Gen_VerifyStructureItem; $1; $2)
	C_LONGINT:C283(Fnd_Gen_VerifyStructureItem; $3)
	C_BOOLEAN:C305(Fnd_Gen_VerifyStructureItem; $4)
	
	C_BOOLEAN:C305(Fnd_Gen_RunningInHost; $1; $0)  // Modified by: Wayne Stewart(2nd March 2009)
	C_BOOLEAN:C305(Fnd_Gen_WaitToContinue; $1; $0)  // Modified by: Walt Nelson(2/19/10)per Wayne Stewart
	
	C_BOOLEAN:C305(Fnd_Tbl_UserCanAccess; $0)  // Added by: Doug Hall 7 February, 2023
	C_POINTER:C301(Fnd_Tbl_UserCanAccess; $1)  // Added by: Doug Hall 7 February, 2023
	
	C_POINTER:C301(Fnd_Gen_TableOf; $1; $0)
	
	
	C_POINTER:C301(Fnd_Gen_TableIsVisible; $1)
	C_BOOLEAN:C305(Fnd_Gen_TableIsVisible; $0)
	
	C_VARIANT:C1683(Fnd_Gen_FieldIsVisible; $1)
	C_LONGINT:C283(Fnd_Gen_FieldIsVisible; $2)
	C_BOOLEAN:C305(Fnd_Gen_FieldIsVisible; $0)
	
End if 