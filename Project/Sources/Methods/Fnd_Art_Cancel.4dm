//%attributes = {"invisible":true,"shared":true}
/* Fnd_Art_Cancel
Project Method: Fnd_Art_Cancel

Signals the Startup dialog to close

Created by Wayne Stewart (2019-10-29)
     waynestewart@mac.com
*/

If (Form:C1466#Null:C1517)
	Form:C1466.CloseRequested:=True:C214
	
	If ((Tickcount:C458>Form:C1466.Art_SoonestCloseTime) & (Form:C1466.CloseRequested))
		CANCEL:C270
	End if 
	
End if 
