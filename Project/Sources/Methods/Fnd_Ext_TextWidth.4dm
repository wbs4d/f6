//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Ext_TextWidth
  // $elementWidth_i:=Fnd_Ext_TextWidth ($text_t;$fontName_t;$fontSize_i;$fontStyle_i)
  // $width_i:=Fnd_Ext_TextWidth ($buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i)

  // Replaces functionality of plugin Extras

  // Access Type: Public

  // Parameters: $buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i

  // Returns:
  //   $0 : Long Integer : Length of Text in pixels

  // Created by Walt Nelson on 10/26/10
  // ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1)
C_TEXT:C284($2)
C_LONGINT:C283($3)
C_LONGINT:C283($4)

C_LONGINT:C283($Fnd_Bttn_FontSize_i;$Fnd_Bttn_FontStyle_i;$lengthOfText_i)
C_TEXT:C284($buttonText_t;$Fnd_Bttn_FontName_t)

If (False:C215)
	C_LONGINT:C283(Fnd_Ext_TextWidth ;$0)
	C_TEXT:C284(Fnd_Ext_TextWidth ;$1)
	C_TEXT:C284(Fnd_Ext_TextWidth ;$2)
	C_LONGINT:C283(Fnd_Ext_TextWidth ;$3)
	C_LONGINT:C283(Fnd_Ext_TextWidth ;$4)
End if 

$buttonText_t:=$1
$Fnd_Bttn_FontName_t:=$2
$Fnd_Bttn_FontSize_i:=$3
$Fnd_Bttn_FontStyle_i:=$4

$lengthOfText_i:=Fnd_SVG_GetStringWidth ($buttonText_t;$Fnd_Bttn_FontName_t;$Fnd_Bttn_FontSize_i;$Fnd_Bttn_FontStyle_i)

$0:=$lengthOfText_i