//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Art_SetBestFontSize (object name; starting font size)

  // Modifies the object so the text fits within the current object size.
  // Goes down from the starting font size.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The form object name
  //   $2 : Longint : The largest possible size

  // Returns: Nothing

  // Created by Dave Batton on Sep 3, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$objectName_t)
C_LONGINT:C283($2;$fontSize_i;$bestWidth_i;$bestHeight_i;$objectWidth_i;$left_i;$top_i;$right_i;$bottom_i)

$objectName_t:=$1
$fontSize_i:=$2+1

OBJECT GET COORDINATES:C663(*;$objectName_t;$left_i;$top_i;$right_i;$bottom_i)
$objectWidth_i:=$right_i-$left_i-1  // -1 is our safety factor.

Repeat 
	$fontSize_i:=$fontSize_i-1
	OBJECT SET FONT SIZE:C165(*;$objectName_t;$fontSize_i)
	OBJECT GET BEST SIZE:C717(*;$objectName_t;$bestWidth_i;$bestHeight_i)
Until ($bestWidth_i<=$objectWidth_i)

OBJECT SET FONT SIZE:C165(*;$objectName_t;$fontSize_i)
