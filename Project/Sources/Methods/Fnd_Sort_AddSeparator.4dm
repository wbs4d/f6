//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_AddSeparator ({position})

  // Adds a separator line to the list of sortable fields.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Position of the separator (optional)

  // Returns: Nothing

  // Created by Dave Batton on May 21, 2004
  // ----------------------------------------------------

C_LONGINT:C283($1;$position_i)

If (Count parameters:C259>=1)
	$position_i:=$1
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Sort_AddItem ($position_i;"-";"")