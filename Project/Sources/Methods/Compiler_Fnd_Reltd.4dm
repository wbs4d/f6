//%attributes = {"invisible":true}
// ----------------------------------------------------
// User name (OS): Doug Hall
// Date and time: 05/27/22, 11:42:29
// ----------------------------------------------------
// Method: Compiler_Fnd_Reltd
// Description: Compiler variables and method parameters declared.
//
//
// Parameters
// ----------------------------------------------------

If (Not:C34(<>Fnd_Reltd_Initialized_b))
	C_BOOLEAN:C305(<>Fnd_Reltd_Initialized_b)
	ARRAY POINTER:C280(<>Fnd_Reltd_Many_aptr; 0; 0)
	ARRAY POINTER:C280(<>Fnd_Reltd_One_aptr; 0; 0)
	
End if 

If (False:C215)
	C_TEXT:C284(Fnd_Reltd_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Reltd_SetName; $0)
	C_POINTER:C301(Fnd_Reltd_SetName; $1)
	
	C_POINTER:C301(Fnd_Reltd_ShowSet; $1)
	C_TEXT:C284(Fnd_Reltd_ShowSet; $2)
	
	C_POINTER:C301(Fnd_Reltd_GetOneKeys; $1)
	C_POINTER:C301(Fnd_Reltd_GetOneKeys; $2)
	
	C_POINTER:C301(Fnd_Reltd_GetManyKeys; $1)
	C_POINTER:C301(Fnd_Reltd_GetManyKeys; $2)
	
	C_LONGINT:C283(Fnd_Reltd_GetOneKeys; $0)
	
	C_LONGINT:C283(Fnd_Reltd_GetManyKeys; $0)
	
End if 