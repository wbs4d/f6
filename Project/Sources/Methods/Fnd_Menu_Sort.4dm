//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Sort

// Sorts the records for the current table.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Shell component is no longer required.
// ----------------------------------------------------

var $usingFndOut_b : Boolean



If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Sort")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_Sort";*)
Else 
	
	If (Macintosh option down:C545)  // Also works for the Windows' Alt key.
		Fnd_Menu_SortOrderEditor  // Display 4D's Order By Editor.
	Else 
		
		$usingFndOut_b:=Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active"
		If ($usingFndOut_b)
			Fnd_Out_SynchroniseSelection
			Fnd_Sort_Display  // Display Foundation's Sort dialog.
			Form:C1466.listData:=Create entity selection:C1512(Fnd_Gen_CurrentTable->).copy()
			Fnd_Out_Update
		Else 
			Fnd_Sort_Display  // Display Foundation's Sort dialog.
		End if 
	End if 
End if 

