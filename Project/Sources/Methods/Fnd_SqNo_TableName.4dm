//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_SqNo_TableName (->table) --> Text

// Returns the virtual table name if the Fnd_VS component is available.
//   Otherwise the 4D table name is returned.

// Access: Private

// Parameters: 
//   $1 : Pointer : A table

// Returns: 
//   $0 : Text : The table name to display

// Created by Dave Batton on Feb 28, 2006
// ----------------------------------------------------

C_TEXT:C284($0; $tableName_t)
C_POINTER:C301($1)

<>Fnd_SqNo_TempTable_ptr:=$1

//If (Fnd_Gen_ComponentAvailable("Fnd_VS"))
//EXECUTE METHOD("Fnd_VS_TableName"; $tableName_t; <>Fnd_SqNo_TempTable_ptr)
//Else 
//$tableName_t:=Table name(<>Fnd_SqNo_TempTable_ptr)
//End if 

$tableName_t:=Fnd_VS_TableName(<>Fnd_SqNo_TempTable_ptr)

$0:=$tableName_t
