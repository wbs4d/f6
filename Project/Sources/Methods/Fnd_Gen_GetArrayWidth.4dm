//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_GetArrayWidth (->text array; font; size; style) --> Number

// Returns the width required to display the array text given the specified font settings.
// Only measures the text - does not take into account the object used to display the text.
// Basically this just measures the width of the longest element.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : The text array to measure
//   $2 : Text : The font name
//   $3 : Longint : The font size
//   $4 : Longint : The font style

// Returns: 
//   $0 : Longint : The width of the longest element

// Created by Dave Batton on Feb 27, 2005
// Modified by Dave Batton on Dec 4, 2005
//   Moved this from the Find component to the General component and changed it from Private to Protected.
// ----------------------------------------------------

C_LONGINT:C283($0;$3;$4;$fontSize_i;$fontStyle_i;$maxWidth_i;$element_i;$elementWidth_i)
C_POINTER:C301($1;$array_ptr)
C_TEXT:C284($2;$fontName_t;$text_t)

$array_ptr:=$1
$fontName_t:=$2
$fontSize_i:=$3
$fontStyle_i:=$4

$maxWidth_i:=0

For ($element_i;1;Size of array:C274($array_ptr->))
	$text_t:=$array_ptr->{$element_i}
	$elementWidth_i:=Fnd_Ext_TextWidth($text_t;$fontName_t;$fontSize_i;$fontStyle_i)
	// Modified by: Walt Nelson (11/20/10)
	If ($elementWidth_i>$maxWidth_i)
		$maxWidth_i:=$elementWidth_i
	End if 
End for 

$0:=$maxWidth_i
