//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ComponentData (->prefixes array{; ->installed versions array{; ->current versions array}})

// Modifies the arrays to contain the installed Foundation
//   component prefixes, installed versions, and current versions.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A text array to receive the component prefixes
//   $2 : Pointer : A text array to receive the installed component version numbers (optional)
//   $3 : Pointer : A text array to receive the current component version numbers (optional)

// Returns: Nothing

// Created by Dave Batton on Jan 13, 2004
// Modified by Dave Batton on Feb 5, 2004
//   This method now returns Foundation components that aren't installed if version
//   information is requested.
// Modified by Dave Batton on Dec 12, 2004
//   Now tries to use each component's _Info method if it exists.
// Modified by Dave Batton on Jan 30, 2005
//   Now also returns an array of the latest know version numbers.
// ----------------------------------------------------

C_POINTER:C301($1;$2;$3;$namesArray_ptr;$versionsArray_ptr;$crntVersionsArray_ptr)
C_TEXT:C284($componentName_t)
C_BOOLEAN:C305($getInstalledVersions_b;$geCrntVersions_b;$available_b)
C_LONGINT:C283($element_i)

$namesArray_ptr:=$1
If (Count parameters:C259>=2)
	$versionsArray_ptr:=$2
	$getInstalledVersions_b:=True:C214
	If (Count parameters:C259>=3)
		$crntVersionsArray_ptr:=$3
		$geCrntVersions_b:=True:C214
	Else 
		$geCrntVersions_b:=False:C215
	End if 
Else 
	$getInstalledVersions_b:=False:C215
End if 

LIST TO ARRAY:C288("Fnd_Gen_Components";$namesArray_ptr->)

For ($element_i;Size of array:C274($namesArray_ptr->);1;-1)
	$componentName_t:=$namesArray_ptr->{$element_i}
	$available_b:=Fnd_Gen_ComponentAvailable($componentName_t)
	Case of 
		: ($getInstalledVersions_b)  // We want to do this even if it's not available.
			INSERT IN ARRAY:C227($versionsArray_ptr->;1)
			$versionsArray_ptr->{1}:=Fnd_Gen_ComponentInfo($componentName_t;"Version")
			Case of 
				: ($versionsArray_ptr->{1}="Component Not Available")  // It failed the _Init test.
					$versionsArray_ptr->{1}:=""
				: ($versionsArray_ptr->{1}="Component Did Not Respond")  // It may not have an _Info method (yet).
					$versionsArray_ptr->{1}:=Fnd_Gen_GetDatabaseInfo($componentName_t+"Version")
			End case 
		: (Not:C34($available_b))
			DELETE FROM ARRAY:C228($namesArray_ptr->;$element_i)
	End case 
End for 

If ($geCrntVersions_b)
	LIST TO ARRAY:C288("Fnd_Gen_ComponentVersions";$crntVersionsArray_ptr->)
End if 