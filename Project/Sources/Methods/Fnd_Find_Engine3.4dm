//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Find_Engine3 ("Query" or "Query Selection"; ->field; operator)

  // Adds a query to the built search.
  // Exactly the same as Fnd_Find_Engine2, but with "|" operators.

  // Parameters: 
  //   $1 : Text : Either "Query" or "Query Selection"
  //   $2 : Pointer : A pointer to the field to search
  //   $3 : Text : The operator code to use

  // Returns: Nothing

  // Created by Dave Batton on Jul 21, 2003
  // Modified by Dave Batton on Jun 1, 2005
  //   Fixed a bad case test value. It was unable to search the selection for a Boolean set to False.
  //Line 70 - Modified by wwn on 07/26/09
  //Fnd_Gen_BugAlert ("Fnd_Find_Engine2";"An unexpected operator ("+$operator_t+") was encountered.")
  //Engine2 should be Engine3
  // Modified by Ed Heckman, October 27, 2015

  // ----------------------------------------------------

C_TEXT:C284($1;$3;$searchScope_t;$operator_t)
C_POINTER:C301($2;$field_ptr)
C_TEXT:C284($wildCard_t)
C_LONGINT:C283($fieldType_t)

$searchScope_t:=$1
$field_ptr:=$2
$operator_t:=$3

$fieldType_t:=Type:C295($field_ptr->)
$wildCard_t:=Num:C11(Fnd_Find_SearchString_t#"")*"@"

If ($searchScope_t="Query")
	Case of 
		: ($operator_t="SW")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=Fnd_Find_SearchString_t+$wildCard_t;*)
		: ($operator_t="EW")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=$wildCard_t+Fnd_Find_SearchString_t;*)
		: ($operator_t="C")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=$wildCard_t+Fnd_Find_SearchString_t+$wildCard_t;*)
		: ($operator_t="DC")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->#$wildCard_t+Fnd_Find_SearchString_t+$wildCard_t;*)
		: ($operator_t="=")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=Fnd_Find_SearchString_t;*)
		: ($operator_t=">")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->>Fnd_Find_SearchString_t;*)
		: ($operator_t="<")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr-><Fnd_Find_SearchString_t;*)
		: ($operator_t="#")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->#Fnd_Find_SearchString_t;*)
		: ($operator_t="N>")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->>(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N<")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr-><(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N>=")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->>=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N<=")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr-><=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N=")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N#")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->#(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="T")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=True:C214;*)
		: ($operator_t="F")
			QUERY:C277(Fnd_Find_Table_ptr->; | ;$field_ptr->=False:C215;*)
		Else 
			Fnd_Gen_BugAlert ("Fnd_Find_Engine3";"An unexpected operator ("+$operator_t+") was encountered.")  // wwn on 07/26/09
	End case 
	
Else 
	Case of 
		: ($operator_t="SW")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=(Fnd_Find_SearchString_t+$wildCard_t);*)
		: ($operator_t="EW")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=($wildCard_t+Fnd_Find_SearchString_t);*)
		: ($operator_t="C")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=($wildCard_t+Fnd_Find_SearchString_t+$wildCard_t);*)
		: ($operator_t="DC")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->#($wildCard_t+Fnd_Find_SearchString_t+$wildCard_t);*)
		: ($operator_t="=")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=Fnd_Find_SearchString_t;*)
		: ($operator_t=">")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->>Fnd_Find_SearchString_t;*)
		: ($operator_t="<")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr-><Fnd_Find_SearchString_t;*)
		: ($operator_t="#")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->#Fnd_Find_SearchString_t;*)
		: ($operator_t="N>")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->>(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N<")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr-><(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N>=")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->>=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N<=")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr-><=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N=")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="N#")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->#(Num:C11(Fnd_Find_SearchString_t));*)
		: ($operator_t="T")
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=True:C214;*)
		: ($operator_t="F")  // DB050631 - Changed the "T" to and "F".
			QUERY SELECTION:C341(Fnd_Find_Table_ptr->; | ;$field_ptr->=False:C215;*)
		Else 
			Fnd_Gen_BugAlert ("Fnd_Find_Engine3";"An unexpected operator ("+$operator_t+") was encountered.")
	End case 
End if 