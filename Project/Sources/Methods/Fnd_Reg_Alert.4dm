//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reg_Alert (message)

// Displays an alert message. Uses the Foundation Dialog module alert if available.

// Access: Private

// Parameters: 
//   $1 : Text : The alert message text

// Returns: Nothing

// Created by Dave Batton on Jul 31, 2003
// ----------------------------------------------------

C_TEXT:C284($1; $message_t)

$message_t:=$1  // An interprocess variable, since we only do registration from one process.

Fnd_Reg_Init

//If (Fnd_Gen_ComponentAvailable("Fnd_Dlg"))
//EXECUTE METHOD("Fnd_Wnd_Position"; *; Fnd_Wnd_MacOSXSheet)
//EXECUTE METHOD("Fnd_Dlg_Alert"; *; $message_t)
//Else 
//ALERT($message_t)
//End if 

Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
Fnd_Dlg_Alert($message_t)