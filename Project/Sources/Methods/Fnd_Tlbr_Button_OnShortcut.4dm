//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Button_OnShortcut (shortcut key)

  // Called from the On Clicked form event of all of the shortcut key buttons.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The shortcut key code

  // Returns: Nothing

  // Created by Dave Batton on Feb 17, 2004
  // Based on a routine from Mark Mitchenall's Toolbar Component.
  // Modified by Dave Batton on May 10, 2005
  //   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
  // Modified by: Walt Nelson (3/10/10) - Guy Algot fixed shortcuts after Dave Nasralla's fix for Windows user's
  //   intermittent failure to resolve the "Self" pointer
  // ----------------------------------------------------

C_TEXT:C284($1;$shortcutKey_t;$ButtonName_t)
C_LONGINT:C283($buttonNumber_i;$ignore_i)
C_POINTER:C301($ButtonPointer_ptr)  // Modified by: Walt Nelson (3/10/10)

$shortcutKey_t:=$1

$buttonNumber_i:=Find in array:C230(Fnd_Tlbr_ObjectShortcuts_at;$shortcutKey_t)

If ($buttonNumber_i>0)
	If (Fnd_Tlbr_ObjectEnabled_ab{$buttonNumber_i})
		
		$ButtonPointer_ptr:=Fnd_Tlbr_PictureButtons_aptr{$buttonNumber_i}  // Modified by: Walt Nelson (3/10/10)
		RESOLVE POINTER:C394($ButtonPointer_ptr;$ButtonName_t;$ignore_i;$ignore_i)
		Fnd_Tlbr_Button_OnClicked ($ButtonPointer_ptr;$ButtonName_t)  // End of Modified by: Walt Nelson (3/10/10)
		
		  //Fnd_Tlbr_Button_OnClicked (Fnd_Tlbr_PictureButtons_aptr{$buttonNumber_i})
	End if 
End if 