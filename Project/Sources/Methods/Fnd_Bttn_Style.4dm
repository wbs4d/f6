//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_Style ({Style name}) --> Text

// Sets and gets the toolbar style name.
// Can be:
//   "Large"
//   "Prefs"
//   "Small1"
//   "Small2"

// Access Type: Shared

// Parameters: 
//   $1 : Text : The style to use (optional)

// Returns: 
//   $0 : Text : The selected style

// Created by Dave Batton on Jan 16, 2005
// Modified by Dave Batton on Apr 13, 2005
//   Added support for the new "Prefs" button style.
// ----------------------------------------------------

C_TEXT:C284($0;$1;$requestedStyle_t)

Fnd_Bttn_Init

If (Count parameters:C259>=1)
	$requestedStyle_t:=$1
	
	Case of 
		: ($requestedStyle_t="Large")
			Fnd_Bttn_Style_t:=$requestedStyle_t
		: ($requestedStyle_t="Prefs")
			Fnd_Bttn_Style_t:=$requestedStyle_t
		: ($requestedStyle_t="Small1")
			Fnd_Bttn_Style_t:=$requestedStyle_t
		: ($requestedStyle_t="Small2")
			Fnd_Bttn_Style_t:=$requestedStyle_t
		Else 
			Fnd_Gen_BugAlert(Current method name:C684;"Invalid style: "+$requestedStyle_t)
	End case 
	
	Fnd_Bttn_SetVariables
End if 

$0:=Fnd_Bttn_Style_t
