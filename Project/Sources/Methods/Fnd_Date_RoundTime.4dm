//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_RoundTime (time{; option}) --> Time

  // Rounds the time.
  // The option parameter determines the rounding style:
  //   1 : Round up or down to the nearest quarter hour
  //   2 : Round down to the nearest quarter hour

  // Access: Shared

  // Parameters: 
  //   $1 : Time : The time to round
  //   $2 : Longint : The rounding style (optional)

  // Returns: 
  //   $0 : Time : The rounded time

  // Created by Dave Batton on May 12, 2004
  // ----------------------------------------------------

C_TIME:C306($0;$1;$2;$theTime_time)

$theTime_time:=$1

Case of 
	: (Count parameters:C259<2)
		$theTime_time:=Fnd_Date_RoundTimeNear ($theTime_time)
		
	: ($2=2)
		$theTime_time:=Fnd_Date_RoundTimeDown ($theTime_time)
		
	Else 
		$theTime_time:=Fnd_Date_RoundTimeNear ($theTime_time)
End case 

$0:=$theTime_time