//%attributes = {"invisible":true,"shared":true}
/*  ----------------------------------------------------
   Project Method: Fnd_Art_Init

   Initializes both the process and interprocess variables used by the Fnd_Art routines.

   Access: Shared

   Parameters: None

   Returns: Nothing

   Created by Dave Batton on Aug 28, 2003
   Modified by Dave Batton on Feb 5, 2004
     Removed the Fnd_Art_Initialized_b variable, since it's not used.
   Modified by Dave Batton on May 20, 2004
     Updated the version number to 4.0.3.
   Modified by Dave Batton on Dec 27, 2004
     Moved the version number to the new Fnd_Art_Info method.
   Modified by Wayne Stewart, 2018-04-16 - Remove call to Fnd_Gen_ComponentCheck
   Mod by Wayne Stewart, (2019-10-26) - Move to new object system
----------------------------------------------------*/

Fnd_Init  // All INIT methods need to start with this

If (Storage:C1525.Fnd.Art=Null:C1517)
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Art:=New shared object:C1526(\
			"startupProcessName"; "$Fnd_Art_StartupDialog"; \
			"aboutFormName"; "Fnd_Art_About"; \
			"startupFormName"; "Fnd_Art_StartupDialog"; \
			"WindowReference"; -1\
			)
	End use 
	
	//Fnd_Loc_LoadTranslation("Fnd_Art")
	
End if 

