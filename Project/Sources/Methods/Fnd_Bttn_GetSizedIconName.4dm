//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_GetSizedIconName (icon name) -> Text

  // Returns the name of the icon with the size embeded.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The icon name without the size

  // Returns: 
  //   $0 : Text : The icon name with the size

  // Created by Dave Batton on Jan 24, 2005
  // Modified by Dave Batton on Apr 13, 2005
  //   Added support for the Pref style toolbar button.
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$iconName_t)

$iconName_t:=$1

Case of 
	: ($iconName_t#"Fnd_Bttn_@")
		  // It's not one of our icons - don't mess with it.
	: (($iconName_t="Fnd_Bttn_16_@") | ($iconName_t="Fnd_Bttn_24_@") | ($iconName_t="Fnd_Bttn_32_@"))
		  // It already has the size info - don't mess with it.
	: (Fnd_Bttn_Style_t="Small@")
		$iconName_t:=Replace string:C233($iconName_t;"Fnd_Bttn_";"Fnd_Bttn_16_")
	: (Fnd_Bttn_Platform_t="Win")  // For Large or Prefs
		$iconName_t:=Replace string:C233($iconName_t;"Fnd_Bttn_";"Fnd_Bttn_24_")
	: (Fnd_Bttn_Platform_t="Mac")  // For Large or Prefs
		$iconName_t:=Replace string:C233($iconName_t;"Fnd_Bttn_";"Fnd_Bttn_32_")
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"Unable to determine the button name.")
End case 

$0:=$iconName_t