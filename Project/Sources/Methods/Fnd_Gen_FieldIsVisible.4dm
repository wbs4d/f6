//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_FieldIsVisible ( fieldPtr | tableNum {; fieldNum}  --> ReturnType

// Determines if a field is visible

// Access: Shared

// Parameters: 
// Variant One :  :
//   $1 : Pointer : Field pointer
// Variant Two :  : 
//   $1 : Longint : Table number
//   $2 : Longint : Field number

// Returns: 
//   $0 : Boolean : True - the field is visible

// Created by Wayne Stewart (2023-05-01)
//     waynestewart@mac.com
// ----------------------------------------------------

If (False:C215)
	C_VARIANT:C1683(Fnd_Gen_FieldIsVisible; $1)
	C_LONGINT:C283(Fnd_Gen_FieldIsVisible; $2)
	C_BOOLEAN:C305(Fnd_Gen_FieldIsVisible; $0)
End if 

C_VARIANT:C1683($1)
C_LONGINT:C283($2; $tableNumber_i; $fieldNumber_i; $fieldType_i; $fieldLength_i; $ValueType_i)
C_BOOLEAN:C305($0; $fieldIndexed_b; $fieldUnique_b; $fieldInvisible_b)
C_POINTER:C301($field_ptr)

$ValueType_i:=Value type:C1509($1)

Case of 
	: ($ValueType_i=Is pointer:K8:14)
		$field_ptr:=$1
		
	: ($ValueType_i=Is longint:K8:6)\
		 | ($ValueType_i=Is integer:K8:5)\
		 | ($ValueType_i=Is real:K8:4)
		$tableNumber_i:=$1
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "An unexpected variable type was passed to this method: "+String:C10($ValueType_i))
End case 

If (Count parameters:C259=2)
	$field_ptr:=Field:C253($tableNumber_i; $2)
End if 

GET FIELD PROPERTIES:C258($field_ptr; $fieldType_i; $fieldLength_i; $fieldIndexed_b; $fieldUnique_b; $fieldInvisible_b)

$0:=Not:C34($fieldInvisible_b)