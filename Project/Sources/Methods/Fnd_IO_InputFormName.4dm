//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_InputFormName ({form name}) --> Text

  // Allows the developer to get and set the name of the input form to use.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The new input form name (optional)

  // Returns: 
  //   $0 : Text : The input form name

  // Created by Dave Batton on Aug 20, 2003
  // Modified by Dave Batton on May 30, 2004
  //   Now this method also returns the input form name.
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_IO_Init 

If (Count parameters:C259>=1)
	Fnd_IO_InputFormName_t:=$1
End if 

$0:=Fnd_IO_InputFormName_t
