//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_LoadPrefs

  // Reads the preferences file from the disk.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 16, 2003
  // Modified by Dave Batton on Jan 30, 2005
  //  This routine no longer changes the value of 4D's Error variable.
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Pref_Initialized_b)

  // Don't call Fnd_Pref_Init here.  This method gets called by it. 
  // But make sure it has been called.
  // Don't use a semaphore here. The calling method should handle this.

If (<>Fnd_Pref_Initialized_b)
	Fnd_Pref_LoadServerPrefs   // This must be done first so the local prefs can override the server prefs.
	Fnd_Pref_LoadLocalPrefs 
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"This method should not be called directly.")
End if 