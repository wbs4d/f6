//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_GetDemoDaysRemaining ({feature code}) --> Number

  // Returns the number of days left in the demo. Calculated by adding
  //   the number of demo days to the demo start date.
  // If the demo hasn't been started, the number of demo days is returned.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: 
  //   $0 : Longint : Days remaining

  // Created by Dave Batton on Aug 1, 2003
  // ----------------------------------------------------

C_LONGINT:C283($0;$days_i)
C_TEXT:C284($1;$featureCode_t)
C_DATE:C307($expirationDate_d)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

$expirationDate_d:=Fnd_Reg_GetExpirationDate ($featureCode_t)

If ($expirationDate_d=!00-00-00!)
	$days_i:=Fnd_Reg_DemoDays ($featureCode_t)
	
Else 
	$days_i:=$expirationDate_d-Current date:C33
End if 

$0:=$days_i
