//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_LanguageCode ({code}) --> Text

// If no parameters are passed, this routine returns the current language
//   code that Foundation is using for localization.  If a language code is
//   passed to this routine, it's set as the new localization code.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The language code to use (optional)

// Returns: 
//   $0 : Text : The current langauge code

// Created by Dave Batton on Jan 24, 2004
// Modified by : Vincent Tournier (6/16/2011)
// ----------------------------------------------------

C_TEXT:C284($0;$1;<>Fnd_Loc_CurrentLanguage_t)

// Fnd_Loc_Init intentionally _not_ called here.

If (Count parameters:C259>=1)
	<>Fnd_Loc_CurrentLanguage_t:=$1
	Fnd_Menu_UpdateResources
	
Else 
	If (<>Fnd_Loc_CurrentLanguage_t="")
		
		// BEGIN Modified by : Vincent Tournier (6/16/2011)
		//<>Fnd_Loc_CurrentLanguage_t:="EN"
		Case of 
			: (Get database localization:C1009="@fr@")  // use Get current database localization(2) for v12
				<>Fnd_Loc_CurrentLanguage_t:="FR"
				
			: (Get database localization:C1009="@de@")  // use Get current database localization(2) for v12
				<>Fnd_Loc_CurrentLanguage_t:="GR"
				
			Else 
				<>Fnd_Loc_CurrentLanguage_t:="EN"
		End case 
		// END Modified by : Vincent Tournier (6/16/2011)
	End if 
End if 

$0:=<>Fnd_Loc_CurrentLanguage_t
