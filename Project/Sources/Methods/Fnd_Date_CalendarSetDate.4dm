//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_CalendarSetDate (function; operator)

  // Sets the date as per the string parameters function and operator.

  // Functions:
  //   "date"
  //     "var" - Sets the date to variable ◊Fnd_Date_CalendarDate_d
  //     "today" - Sets the date to today
  //     Date String - Sets the date to the date passed as a string
  //   "calendar"
  //     Pass the day number as a string to set the date
  //   "year", "month" & "day"
  //     Pass "+n" or "-n" to add or subtract n number of years, months or days

  // Access: Private

  // Parameters: 
  //   $1 : Text : The function to perform
  //   $2 : Text : Additional information

  // Returns: Nothing

  // Created by Tom Dillon
  // Modifed by Dave Batton on Mar 27, 2005
  // Modified by Dave Batton on Apr 13, 2005
  //   The date modified by the keyboard commands is now based on the displayed date, not the current date.
  //   Now supports 'n' for the middle of the moNth.
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$function_t;$operator_t;$highlightObjectName_t)
C_DATE:C307($setDateTo_d;$firstOfTheMonth_d;$endOfTheMonth_d)
C_LONGINT:C283($day_i;$month_i;$year_i;$daysInMonth_i;$firstWeekDay_i;$cell_i)
C_POINTER:C301($day_ptr)

$function_t:=$1
$operator_t:=$2

$setDateTo_d:=!00-00-00!

Case of 
	: ($function_t="date")  // Set to a specific date
		Case of 
			: ($operator_t="var")
				$setDateTo_d:=<>Fnd_Date_CalendarDate_d
			: ($operator_t="today")
				$setDateTo_d:=Current date:C33
			: ($operator_t="m")
				$setDateTo_d:=Fnd_Date_YearMonthDayToDate (Year of:C25(<>Fnd_Date_CalendarDate_d);Month of:C24(<>Fnd_Date_CalendarDate_d);1)
			: ($operator_t="n")
				$setDateTo_d:=Fnd_Date_YearMonthDayToDate (Year of:C25(<>Fnd_Date_CalendarDate_d);Month of:C24(<>Fnd_Date_CalendarDate_d);15)
			: ($operator_t="h")
				$setDateTo_d:=Fnd_Date_EndOfMonth (<>Fnd_Date_CalendarDate_d)
			: ($operator_t="y")
				$setDateTo_d:=Fnd_Date_YearMonthDayToDate (Year of:C25(<>Fnd_Date_CalendarDate_d);January:K10:1;1)
			: ($operator_t="r")
				$setDateTo_d:=Fnd_Date_YearMonthDayToDate (Year of:C25(<>Fnd_Date_CalendarDate_d);December:K10:12;31)
			: (Date:C102($operator_t)#!00-00-00!)
				$setDateTo_d:=Date:C102($operator_t)
		End case 
		
	: ($function_t="day")  // Add or subtract days
		$setDateTo_d:=Add to date:C393(<>Fnd_Date_CalendarDate_d;0;0;Num:C11($operator_t))
		
	: ($function_t="month")  // Add or subtract months
		$setDateTo_d:=Add to date:C393(<>Fnd_Date_CalendarDate_d;0;Num:C11($operator_t);0)
		
	: ($function_t="year")  // Add or subtract years
		$setDateTo_d:=Add to date:C393(<>Fnd_Date_CalendarDate_d;Num:C11($operator_t);0;0)
		
	: ($function_t="calendar")
		If (Num:C11($operator_t)#0)
			$day_i:=Num:C11($operator_t)
			$month_i:=Month of:C24(<>Fnd_Date_CalendarDate_d)
			$year_i:=Year of:C25(<>Fnd_Date_CalendarDate_d)
			$setDateTo_d:=Add to date:C393(!00-00-00!;$year_i;$month_i;$day_i)
		End if 
End case 

If ($setDateTo_d#!00-00-00!)
	<>Fnd_Date_CalendarDate_d:=$setDateTo_d
	<>Fnd_Date_MonthYear_t:=Fnd_Date_MonthName (Month of:C24(<>Fnd_Date_CalendarDate_d))+" "+String:C10(Year of:C25(<>Fnd_Date_CalendarDate_d))
	
	
	$firstOfTheMonth_d:=Add to date:C393(<>Fnd_Date_CalendarDate_d;0;0;-Day of:C23(<>Fnd_Date_CalendarDate_d)+1)
	$endOfTheMonth_d:=Fnd_Date_EndOfMonth (<>Fnd_Date_CalendarDate_d)
	$firstWeekDay_i:=Day number:C114($firstOfTheMonth_d)  // 1=Sunday
	$daysInMonth_i:=$endOfTheMonth_d-$firstOfTheMonth_d+1
	
	For ($cell_i;1;42)  // 42 is the number of display variables on the calendar form (7 days, 6 rows)
		OBJECT SET RGB COLORS:C628(*;"Fnd_Date_CalendarDay@";Foreground color:K23:1;Background color:K23:2)
		$day_ptr:=Get pointer:C304("<>Fnd_Date_CalendarDay"+String:C10($cell_i)+"_i")
		If (($cell_i>=$firstWeekDay_i) & ($cell_i<($firstWeekDay_i+$daysInMonth_i)))
			$day_ptr->:=$cell_i-$firstWeekDay_i+1
		Else 
			$day_ptr->:=0
		End if 
	End for 
	
	  // Highlight the selected date's cell.
	$cell_i:=$firstWeekDay_i+Day of:C23(<>Fnd_Date_CalendarDate_d)-1
	$day_ptr:=Get pointer:C304("<>Fnd_Date_CalendarDay"+String:C10($cell_i)+"_i")
	OBJECT GET COORDINATES:C663($day_ptr->;$left_i;$top_i;$right_i;$bottom_i)
	
	$highlightObjectName_t:="Fnd_Date_CalendarHighlight"
	If (Is Windows:C1573)
		$highlightObjectName_t:=$highlightObjectName_t+"Win"
		OBJECT SET RGB COLORS:C628($day_ptr->;0x00FFFFFF;Background color:K23:2)
	Else 
		$highlightObjectName_t:=$highlightObjectName_t+"Mac"
	End if 
	OBJECT MOVE:C664(*;$highlightObjectName_t;$left_i-2;$top_i-1;$right_i-1;$bottom_i+1;*)
End if 
