//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_GetString (component; internal string{; replace1..replace5})

// Calls the Foundation localization routines if they're available.
//   If not, the default English string is returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The component's code ("Fnd_xxx")
//   $2 : Text : The lookup code
//   $3..$5 : Text : Replacement strings (optional)

// Returns: 
//   $0 : Text : The localized string

// Created by Dave Batton on Dec 30, 2003
// Modified by Dave Batton on May 30, 2004
//   Now handles the same optional parameters handled by Fnd_Loc_GetString.
// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header to reflect actual maximum number of optional parameters
//   NOTE: Even though 6 parameters are defined, only 5 are handled by the code
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable call 
// ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$3;$4;$5;$6;$0;$localizedString_t;$moduleName_t;$internalLabel_t;$replacementString_t)
C_LONGINT:C283($moduleElement_i;$labelElement_i)

// Currently the parameters part isn't implemented.
$moduleName_t:=$1
$internalLabel_t:=$2

Fnd_FCS_Trace


$localizedString_t:=Fnd_Loc_GetString($moduleName_t;$internalLabel_t)

/* This is a large chu
If (Fnd_Gen_ComponentAvailable("Fnd_Loc"))  // Calls Fnd_Gen_Init.
EXECUTE METHOD("Fnd_Loc_GetString"; $localizedString_t; $moduleName_t; $internalLabel_t)

Else 
If (Not(Semaphore(<>Fnd_Gen_LocSemaphore_t; 300)))  // Wait up to 5 seconds.
$moduleElement_i:=Find in array(<>Fnd_Gen_LocModules_at; $moduleName_t)
If ($moduleElement_i=-1)
Fnd_Gen_BugAlert(Current method name; "Attempted to get a localized string from a component that hasn't been localized.")
End if 

If ($moduleElement_i>0)
$labelElement_i:=Find in array(<>Fnd_Gen_LocLookupCodes_at{$moduleElement_i}; $internalLabel_t)
If ($labelElement_i>0)
$localizedString_t:=<>Fnd_Gen_LocStrings_at{$moduleElement_i}{$labelElement_i}
Else 
Fnd_Gen_BugAlert(Current method name; "Unable to find the \""+$moduleName_t+"_EN:"+$internalLabel_t+"\" string for localization.")
End if 

Else 
Fnd_Gen_BugAlert(Current method name; "Unable to load the \""+$moduleName_t+"_EN\" strings for localization.")
End if 

CLEAR SEMAPHORE(<>Fnd_Gen_LocSemaphore_t)
End if 
End if 

*/
// Replace any <<n>> style placeholders with the extra parameters.
For ($labelElement_i;1;Count parameters:C259-2)
	Case of 
		: ($labelElement_i=1)  // An ugly work-around for a bug in 4D v11 SQL.
			$replacementString_t:=$3
		: ($labelElement_i=2)
			$replacementString_t:=$4
		: ($labelElement_i=3)
			$replacementString_t:=$5
		: ($labelElement_i=4)
			$replacementString_t:=$0
	End case 
	$localizedString_t:=Replace string:C233($localizedString_t;"<<"+String:C10($labelElement_i)+">>";$replacementString_t)
End for 

$0:=$localizedString_t