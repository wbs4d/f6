//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetReal (name{; default{; scope}}) --> Real

// Retrieves a real number value in from user's preferences file.  If an item 
//   with the specified name isn't found, the default value is returned (if specified).
//   Otherwise, 0 is returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Real : The default value (optional)
//   $3 : Longint : The scope if the default is used (optional)

// Returns: 
//   $0 : Longint : The saved value

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Feb 6, 2006
//   Added the scope parameter.
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
//   Now calls the corresponding Fnd_Pref_SetReal routine to handle default value
// ----------------------------------------------------

C_REAL:C285($0; $2; $itemValue_r)
C_TEXT:C284($1; $itemName_t)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_r:=0
$itemScope_i:=Fnd_Pref_Local

If (Count parameters:C259>=2)
	$itemValue_r:=$2
	If (Count parameters:C259>=3)
		$itemScope_i:=$3
	End if 
End if 

Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $itemName_t)

Case of 
	: ($element_i>0)
		If (<>Fnd_Pref_Types_ai{$element_i}=Is real:K8:4)
			$itemValue_r:=Num:C11(<>Fnd_Pref_Values_at{$element_i})
		End if 
		
	: (Fnd_Pref_GetSharedValue($itemName_t; ->$itemValue_r))  // We found it in the shared prefs.
		
	: (Count parameters:C259>=2)  // Only if they specified a default value.
		Fnd_Pref_SetReal($itemName_t; $itemValue_r; $itemScope_i)
End case 

$0:=$itemValue_r
