//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_SetupToolbar

  // Handles the toolbar setup for the input and output forms.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 17, 2004
  // Modified by Dave Batton on Sep 20, 2004
  // Modified by Dave Batton on Jan 8, 2005
  //   Now sets the toolbar icon group.
  // Modified by Dave Batton on Jan 29, 2007
  //   We now set the status message to a non-blank value.
  // ----------------------------------------------------

  // Create the output form toolbar buttons and divider lines.
  // We just pass blank button names because these will be handled by the Fnd_IO_ToolbarIconGroup call.
Fnd_Tlbr_Button_Add ("New";Fnd_Loc_GetString ("Fnd_IO";"NewButton");"";"Fnd_Rec_NewRecord";"N")
Fnd_Tlbr_Button_Add ("Delete";Fnd_Loc_GetString ("Fnd_IO";"DeleteButton");"";"Fnd_Rec_DeleteUserSet";"Backspace")
Fnd_Tlbr_Divider_Add 
Fnd_Tlbr_Button_Add ("ShowAll";Fnd_Loc_GetString ("Fnd_IO";"ShowAllButton");"";"Fnd_Rec_ShowAll";"G")
Fnd_Tlbr_Button_Add ("Find";Fnd_Loc_GetString ("Fnd_IO";"FindButton");"";"Fnd_Shell_Find";"F")
Fnd_Tlbr_Button_Add ("Sort";Fnd_Loc_GetString ("Fnd_IO";"SortButton");"";"Fnd_Shell_Sort";"T")
Fnd_Tlbr_Divider_Add 
Fnd_Tlbr_Button_Add ("Print";Fnd_Loc_GetString ("Fnd_IO";"PrintButton");"";"Fnd_Shell_Print";"P")

Fnd_IO_ToolbarIconGroup ("Bold")  // DB050208

  // We only display the status message if it's not already blank. This gives the developer a chance to hide the
  //   status message in the Fnd_Hook_IO_DisplayTable hook by setting the status message to an empty string.
Fnd_Tlbr_StatusMessage ("*")  // DB070129 - Added

  // If we're using the retro toolbar, add the input form buttons and divider lines.
  //If (Fnd_Tlbr_StyleName="retro")
  //Fnd_Tlbr_Divider_Add 
  //Fnd_Tlbr_Button_Add ("Save";Fnd_Loc_GetString ("Fnd_IO";"SaveButton");$prefix_t+"Save";Command name(269);"Enter")
  //Fnd_Tlbr_Button_Add ("Cancel";Fnd_Loc_GetString ("Fnd_IO";"CancelButton");$prefix_t+"Cancel";Command name(270);"Period")
  //Fnd_Tlbr_Divider_Add 
  //Fnd_Tlbr_Button_Add ("First";Fnd_Loc_GetString ("Fnd_IO";"FirstRecordButton");$prefix_t+"First";"Fnd_Rec_Navigate(\"first\")";"Up")
  //Fnd_Tlbr_Button_Add ("Previous";Fnd_Loc_GetString ("Fnd_IO";"PreviousRecordButton");$prefix_t+"Previous";"Fnd_Rec_Navigate(\"previous\")";"Left")
  //Fnd_Tlbr_Button_Add ("Next";Fnd_Loc_GetString ("Fnd_IO";"NextRecordButton");$prefix_t+"Next";"Fnd_Rec_Navigate(\"next\")";"Right")
  //Fnd_Tlbr_Button_Add ("Last";Fnd_Loc_GetString ("Fnd_IO";"LastRecordButton");$prefix_t+"Last";"Fnd_Rec_Navigate(\"last\")";"Down")
  //End if 
