//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_FontDiscovery

// Discovers the native fonts on this computer 

// Created by Wayne Stewart (2019-06-22)
//     waynestewart@mac.com
// ----------------------------------------------------



C_LONGINT:C283($WindowID_i)
C_TEXT:C284($Form_t)

$Form_t:="Fnd_FontDiscovery"

$WindowID_i:=Open window:C153(Screen width:C187(*)+10; Screen height:C188(*)+10; -1; -1; Plain fixed size window:K34:6)
DIALOG:C40($Form_t)
CLOSE WINDOW:C154