//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Date_Init

// Initializes both the process and interprocess variables used by the Date routines.
// Designed to be called at the beginning of any Protected routines to make sure
//   the necessary variables are initialized.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 28, 2004
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Date_Info method.
// Modified by Dave Batton on Jan 11, 2007
//   Now calls the compiler method for each new process.
// Mod by Wayne Stewart, (2021-04-13) - Swap to New Fnd object system
//   I'll leave IP vars for the moment, my suspicion is that we can get rid of them all

// ----------------------------------------------------

Fnd_Init  // All init methods need to call the Fnd_Init method

C_BOOLEAN:C305(<>Fnd_Date_Initialized_b)

If (Not:C34(<>Fnd_Date_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Date
	Fnd_Loc_LoadTranslation("Fnd_Date")
	
	<>Fnd_Date_Initialized_b:=True:C214
End if 

If (Fnd.Date=Null:C1517)  // So we only do this once per process.
	Compiler_Fnd_Date
	Fnd.Date:=New object:C1471("EntryFilterValue"; "")
End if 
