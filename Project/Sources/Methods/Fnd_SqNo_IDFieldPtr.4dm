//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_IDFieldPtr (->table) --> Pointer

  // Returns a pointer to the specified table's ID field.  Displays a bug alert if
  //   no ID field is found or specified by the hook.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : The table in question.

  // Returns: 
  //   $0 : Pointer : A pointer to the table's key field

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Feb 15, 2005
  //   We no longer bother calling the Virtual Structure component to display the BugAlert.
  // ----------------------------------------------------

C_POINTER:C301($0;$1;$table_ptr;$keyField_ptr;$firstField_ptr;$hookField_ptr)
C_LONGINT:C283($fieldType_i;$fieldLength_i)
C_BOOLEAN:C305($indexed_b)

$table_ptr:=$1

$firstField_ptr:=Field:C253(Table:C252($table_ptr);1)  // Get information about the first field of the table.
GET FIELD PROPERTIES:C258($firstField_ptr;$fieldType_i;$fieldLength_i;$indexed_b)
If (($fieldType_i=Is longint:K8:6) & ($indexed_b))
	$keyField_ptr:=$firstField_ptr
End if 

  // Give the developer a chance to override this default field.
If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_SqNo_SetIDField")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_SqNo_SetIDField";$hookField_ptr;$table_ptr)
	If (Not:C34(Is nil pointer:C315($hookField_ptr)))
		$keyField_ptr:=$hookField_ptr
	End if 
End if 

If (Is nil pointer:C315($keyField_ptr))
	Fnd_Gen_BugAlert (Current method name:C684;"No key field has been specified in the Fnd_Hook_SqNo_SetIDField method for the ["+Table name:C256($table_ptr)+"] table.")  // DB050215 - Modified
End if 

$0:=$keyField_ptr