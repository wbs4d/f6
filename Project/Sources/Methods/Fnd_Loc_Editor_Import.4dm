//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Loc_Editor_Import


// Imports Foundation localization lists.

// Designed to be called from the Localization Editor dialog.  Don't call directly.


// Access Type: Private


// Parameters: None


// Returns: Nothing


// Created by Dave Batton on Jul 16, 2003

// ----------------------------------------------------


C_TEXT:C284($import_t; $folderPath_t; $docPath_t; $directorySeparator_t; $languageCode_t; $listName_t; $rootFolderName_t)
C_TIME:C306($docRef_time)
C_LONGINT:C283($documentNumber_i; $listElement_i; $folderNumber_i; $languageNumber_i)
C_BOOLEAN:C305($validFolder_b)

$rootFolderName_t:="Foundation Localization"  // Also used in Fnd_Loc_Editor_Export.


$directorySeparator_t:=Folder separator:K24:12
$folderPath_t:=Select folder:C670("Select the \""+$rootFolderName_t+"\" folder...")

If (OK=1)
	FOLDER LIST:C473($folderPath_t; $languageFolders_at)
	
	// Make sure we' have a valid localizations folder.  Each of the enclosed
	
	//   folders should have two character names, except for the one named Lookup Codes.
	
	$validFolder_b:=Size of array:C274($languageFolders_at)>0
	For ($folderNumber_i; 1; Size of array:C274($languageFolders_at))
		Case of 
			: ($languageFolders_at{$folderNumber_i}="Lookup Codes")
			: (Length:C16($languageFolders_at{$folderNumber_i})=2)
			Else 
				$validFolder_b:=False:C215
		End case 
	End for 
	
	If ($validFolder_b)
		For ($languageNumber_i; 1; Size of array:C274($languageFolders_at))
			$languageCode_t:=$languageFolders_at{$folderNumber_i}
			
			If (OK=1)
				DOCUMENT LIST:C474($folderPath_t+$languageFolders_at{$folderNumber_i}+$directorySeparator_t; $fileNames_at)
				
				For ($documentNumber_i; 1; Size of array:C274($fileNames_at))
					ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at; 0)
					$docPath_t:=$folderPath_t+$languageFolders_at{$folderNumber_i}+$fileNames_at{$documentNumber_i}
					
					If (Test path name:C476($docPath_t)=Is a document:K24:1)
						$docRef_time:=Open document:C264($docPath_t; "TEXT"; Read mode:K24:5)
						
						If (OK=1)
							$listElement_i:=0
							Repeat 
								RECEIVE PACKET:C104($docRef_time; $import_t; Char:C90(Carriage return:K15:38))
								If ((OK=1) & ($import_t#""))
									$import_t:=Replace string:C233($import_t; Char:C90(Line feed:K15:40); "")
									$listElement_i:=$listElement_i+1
									INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempStringsEN_at; $listElement_i)
									<>Fnd_Loc_EditorTempStringsEN_at{$listElement_i}:=$import_t
								End if 
							Until ((OK#1) | ($import_t=""))
							CLOSE DOCUMENT:C267($docRef_time)
						End if 
						
						$listName_t:=$fileNames_at{$documentNumber_i}
						$listName_t:=Replace string:C233($listName_t; ".TXT"; "")
						
						Fnd_Loc_Editor_ArrayToList(-><>Fnd_Loc_EditorTempStringsEN_at; $listName_t)
					End if 
				End for 
				
				ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at; 0)  // Clear our temporary array.
				
				
				ALERT:C41("The localization files have been imported.")
			End if 
		End for 
		
	Else 
		ALERT:C41("This is not a valid Foundation Localizations folder.")
	End if 
End if 
