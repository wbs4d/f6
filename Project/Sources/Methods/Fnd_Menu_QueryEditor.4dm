//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_QueryEditor

// Displays 4D's Query Editor.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 30, 2003
// Modified by Dave Batton on Mar 30, 2004
//   Now uses Fnd_Gen_CurrentTable rather than Current form table.
//   No longer does anything (including cause errors) if the pointer is nil.
// Modified by: Walt Nelson (2/18/10) - added Fnd_Hook_Shell_QueryEditor check per Michael Check
//  moved Mike E's check for QueryPack to Fnd_Hook_Shell_QueryEditor
// Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
// Mod by Wayne Stewart, (2022-04-21) - Added Fnd_Out support
// ----------------------------------------------------

C_POINTER:C301($table_ptr)
C_BOOLEAN:C305($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

GET AUTOMATIC RELATIONS:C899($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

$table_ptr:=Fnd_Gen_CurrentTable

If (Not:C34(Is nil pointer:C315($table_ptr)))  // DB040330
	SET AUTOMATIC RELATIONS:C310(True:C214;True:C214)
	
	If (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")  // If we are using Fnd_Out make certain the current selection matches the entity selection
		Fnd_Out_SynchroniseSelection
	End if 
	
	If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_QueryEditor")=1)
		EXECUTE METHOD:C1007("Fnd_Hook_Shell_QueryEditor";*)
	Else 
		QUERY:C277($table_ptr->)
	End if 
	
	SET AUTOMATIC RELATIONS:C310($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)
	If (OK=1)
		Fnd_Gen_SelectionChanged  // Always call this after changing the selection.
	End if 
End if 
