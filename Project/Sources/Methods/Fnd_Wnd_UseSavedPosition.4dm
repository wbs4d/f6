//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Wnd_UseSavedPosition ({pref name})


// Tells the window routines we want to try to use a saved position.

// If no preference name is specified, the current process name is used.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The name of the saved position (optional)


// Returns: Nothing


// Created by Dave Batton on Sep 17, 2003

// Modified by Dave Batton on Mar 17, 2004

//   Now the preference name is optional.

// ----------------------------------------------------


C_TEXT:C284($1;$processName_t)
C_LONGINT:C283($processState_i;$processTime_i)

Fnd_Wnd_Init

If (Count parameters:C259>=1)
	Fnd_Wnd_PositionPrefName_t:=$1
	
Else 
	PROCESS PROPERTIES:C336(Current process:C322;$processName_t;$processState_i;$processTime_i)
	Fnd_Wnd_PositionPrefName_t:=$processName_t
End if 
