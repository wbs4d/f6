//%attributes = {"invisible":true}
var $Developer_t : Text
var $ModDate_d : Date



/* Developer Key

DN  : Dave Nasralla
GA  : Guy Algot
JC  : John Craig
JDH : Doug Hall
WBS : Wayne Stewart
WN  : Walt Nelson

*/


Case of 
	: (True:C214)  // So no code ever runs
	: (Year of:C25($ModDate_d)=2023)
		Case of 
			: ($ModDate_d=!2023-08-23!) & ($Developer_t="WBS")
				Fnd_Rec_Modify()  // Remove some leftover test code
				
			: ($ModDate_d=!2023-07-26!) & ($Developer_t="WBS")
				Fnd_Wnd_Init  //   Placed the semaphore checking code
				Fnd_Pref_Init  //  so that it will only run
				Fnd_List_Init  //  on the module initialisation,
				Fnd_Menu_Init  //  rather than on EVERY call
				Fnd_Gen_ButtonText  // Modified so buttons can be referred to by name as well as by pointer
				Fnd_Out_ChoosePotentialColumns  // Inital implementation
				Fnd_Wnd_OpenWindow  // Calls Fnd_Pref_GetWindow directly rather than via EXECUTE
				Fnd_Wnd_SavePosition  // Calls Fnd_Pref_SetWindow directly rather than via EXECUTE
				
				
			: ($ModDate_d=!2023-02-28!) & ($Developer_t="WBS")
				// Conform the Comments in Fnd_Reltd methods so documentation will be written correctly
				Fnd_Tbl_Info
				Fnd_Tbl_Init
				Compiler_Fnd_Tbl
				Fnd_Reltd_Info
				Fnd_Reltd_FindRelated  // Code that does the actual finding of related records has been moved here
				
				// Modified
				Fnd_Menu_FindRelated  // Removed code, this now calls Fnd_Reltd_FindRelated which does the work
				Fnd_IO_DisplayTable2  // Add support for alternative name to iLB 'listbox magic'
				
			: ($ModDate_d=!2023-02-11!) & ($Developer_t="JDH")
				
				// New methods
				Fnd_Reltd_GetManyKeys
				Fnd_Reltd_GetOneKeys
				Fnd_Reltd_Init
				Fnd_Reltd_SetName
				Fnd_Reltd_ShowSet
				Compiler_Fnd_Reltd
				
				Fnd_Gen_TableIsVisible
				Fnd_Gen_TableOf
				
				Fnd_Tbl_CreateAccessArray
				Fnd_Tbl_UserCanAccess
				
				Fnd_Menu_FindRelated
				
				
				
				// Modified
				Fnd_IO_DisplayTable2  // Added support for Fnd_Reltd routines
				Compiler_Fnd_Gen
				Fnd_Find_AddField
				Fnd_Find_AddTable
				Fnd_Menu_LoadMenuTitles
				Fnd_Shell_OnStartup
				
		End case 
		
		
	: (Year of:C25($ModDate_d)=2021)
		
		Case of 
			: ($ModDate_d=!2021-08-11!) & ($Developer_t="WBS")
				Fnd_Log_MaximumSize  // Correct error by forgetting to use "Use" with a shared object
				Fnd_Loc_ArrayToList  // Tweaked method comments
				Fnd_File_Compress  // Allowed for calling with extra parameters
				Fnd_FCS_WriteDocumentation  // Can now choose to not document non shared methods
				Fnd_FCS_BuildComponent  //  New method to build new component after changing version number
				Fnd_Dlg_PasswordEntry  // Tweaked method comments
				Fnd_Date_Calendar  // Tweaked method comments
				
			: ($ModDate_d=!2021-08-10!) & ($Developer_t="WBS")
				
				Fnd_FCS_WriteDocumentation  // some minor polishing of docs
				Fnd_File_Compress  // New method to zip a file
				Fnd_Gen_PositionR  // Tweaked the comments
				Fnd_Log_MaximumSize  // Set a maximum size, after this the log is rolled over
				Fnd_Log_StopLogWriter  // Closes logs in this method now and the terminates worker
				Fnd_Log_This  // Support for rolling over log files
				TimetoDie  // Removed the log file closing from this method so it can be used to kill any worker
				
				// List updated: Fnd_Gen_ComponentVersions
				
				
				If (True:C214)  // All the info methods have been updated to to return 6.0 unless noted otherwise
					Fnd_Bttn_Info
					Fnd_Data_Info
					Fnd_Date_Info
					Fnd_Dict_Info
					Fnd_Dlg_Info
					Fnd_Find_Info
					Fnd_Gen_Info
					Fnd_IO_Info
					Fnd_Loc_Info
					Fnd_Log_Info  // 6.0.1
					Fnd_Log_Init
					Fnd_Menu_Info
					Fnd_Msg_Info
					Fnd_Nav_Info
					Fnd_Pref_Info
					Fnd_Prnt_Info
					Fnd_Pswd_Info
					Fnd_Rec_Info
					Fnd_Reg_Info
					Fnd_RegG_Info
					Fnd_Shell_Info
					Fnd_Sort_Info
					Fnd_SqNo_Info
					Fnd_Text_Info
					Fnd_Tlbr_Info
					Fnd_VS_Info
					Fnd_Wnd_Info
				End if 
				
				
				
			: ($ModDate_d=!2021-05-23!) & ($Developer_t="WBS")
				// v19 beta release 
				Fnd_Gen_ComponentAvailable
				
				// Form: "Fnd_FontDiscovery"
				__BuildComponent
				
				aaaMods
				Compiler_DeleteThese
				Compiler_Fnd_Shell
				Fnd_FCS_WriteDocumentation
				Fnd_Gen_Info
				Fnd_Gen_MenuBar
				Fnd_Gen_QuitNow
				Fnd_Gen_Reg_Alert
				Fnd_Gen_SelectionChanged
				Fnd_Gen_SetLocalizationStrings
				Fnd_List_Editor
				Fnd_Loc_Editor
				Fnd_Loc_LanguageCode
				Fnd_Log_SilentError
				Fnd_Log_StopLogWriter
				Fnd_Log_UseLog
				Fnd_Menu_About
				Fnd_Menu_Administration
				Fnd_Menu_Delete
				Fnd_Menu_Find
				Fnd_Menu_ModifyRecords
				Fnd_Menu_NavPalette
				Fnd_Menu_NewRecord
				Fnd_Menu_OmitSubset
				Fnd_Menu_OpenTable
				Fnd_Menu_Preferences
				Fnd_Menu_Print
				Fnd_Menu_QueryEditor
				Fnd_Menu_ShowAll
				Fnd_Menu_ShowSubset
				Fnd_Menu_Sort
				Fnd_Menu_SpecialFunctions
				Fnd_Menu_WindowItem
				Fnd_Menu_Window_Add
				Fnd_Menu_Window_Update
				Fnd_Nav_AddButtonTable
				Fnd_Nav_AutoConfigure
				Fnd_Nav_ButtonMethod
				Fnd_Nav_Display
				Fnd_Pref_Alert
				Fnd_Pref_Display
				Fnd_Prnt_Alert
				Fnd_Pswd_GeneratorDialog
				Fnd_Rec_AddItem
				Fnd_Rec_Modify
				Fnd_Rec_NewRecord
				Fnd_Reg_Alert
				Fnd_Reg_DemoDialogFormMethod
				Fnd_Reg_RegDialogFormMethod
				Fnd_Shell_Administration
				Fnd_Shell_Find
				Fnd_Shell_NavigationPalette
				Fnd_Shell_Print
				Fnd_Shell_ShowAll
				Fnd_Shell_Sort
				Fnd_Sort_AddItem
				Fnd_Sort_Display
				Fnd_Sort_PrefsGet
				Fnd_Sort_PrefsPut
				Fnd_SqNo_Editor
				Fnd_SqNo_TableName
				TimetoDie
				
				
				
			: ($ModDate_d=!2021-05-23!) & ($Developer_t="WBS")
				// Continue to remove internal calls to 
				Fnd_Gen_ComponentAvailable
				
				Fnd_Gen_Info
				Fnd_Gen_MenuBar
				Fnd_Gen_QuitNow
				Fnd_Gen_Reg_Alert
				Fnd_Gen_SelectionChanged
				Fnd_Gen_SetLocalizationStrings
				
				Fnd_List_Editor
				
				Fnd_Loc_Editor
				Fnd_Loc_LanguageCode
				
				Fnd_Menu_About
				Fnd_Menu_Administration
				Fnd_Menu_Delete
				Fnd_Menu_Find
				Fnd_Menu_ModifyRecords
				Fnd_Menu_NavPalette
				Fnd_Menu_NewRecord
				Fnd_Menu_OmitSubset
				Fnd_Menu_OpenTable
				Fnd_Menu_Preferences
				Fnd_Menu_Print
				Fnd_Menu_QueryEditor
				Fnd_Menu_ShowAll
				Fnd_Menu_ShowSubset
				Fnd_Menu_Sort
				Fnd_Menu_SpecialFunctions
				Fnd_Menu_Window_Add
				Fnd_Menu_Window_Update
				Fnd_Menu_WindowItem
				
				Fnd_Nav_AddButtonTable
				Fnd_Nav_AutoConfigure
				Fnd_Nav_ButtonMethod
				Fnd_Nav_Display
				
				Fnd_Pref_Alert
				Fnd_Pref_Display
				
				Fnd_Prnt_Alert
				
				Fnd_Pswd_GeneratorDialog
				
				Fnd_Rec_AddItem
				Fnd_Rec_Modify
				Fnd_Rec_NewRecord
				
				Fnd_Reg_DemoDialogFormMethod
				Fnd_Reg_RegDialogFormMethod
				
				Fnd_Shell_Administration
				Fnd_Shell_Find
				Fnd_Shell_NavigationPalette
				Fnd_Shell_Print
				Fnd_Shell_ShowAll
				
				Fnd_Sort_AddItem
				Fnd_Sort_Display
				Fnd_Sort_PrefsGet
				Fnd_Sort_PrefsPut
				
				Fnd_SqNo_Editor
				Fnd_SqNo_TableName
				
				
				
				
				
				
				
				
			: ($ModDate_d=!2021-05-23!) & ($Developer_t="WBS")
				Fnd_Ext_TextLines  // Remove SVG calculation and swap to TEXT TO ARRAY
				
			: ($ModDate_d>=!2021-05-05!) & ($ModDate_d<!2021-05-17!) & ($Developer_t="WBS")
				
				// Continue to remove internal calls to 
				Fnd_Gen_ComponentAvailable
				
				Fnd_Find_PrefsGet
				Fnd_Find_AddItem
				Fnd_Find_Display
				Fnd_Gen_Alert
				Fnd_Gen_CenterWindow
				Fnd_Gen_FormMethod
				
			: ($ModDate_d=!2021-04-26!) & ($Developer_t="WBS")
				Fnd_Gen_ComponentAvailable  // Revised method reverted to previous db method and introduced a "not available array"
				
			: ($ModDate_d=!2021-04-16!) & ($Developer_t="WBS")
				Compiler_Fnd_Bttn
				
				// Made certain all Init method also call new method Fnd_init
				Fnd_Art_Init
				Fnd_Bttn_Init
				Fnd_Data_Init
				Fnd_Dict_Init
				Fnd_Dlg_Init
				Fnd_Find_Init
				Fnd_Init
				Fnd_IO_Init
				Fnd_List_Init
				Fnd_Loc_Init
				Fnd_Menu_Init
				Fnd_Msg_Init
				Fnd_Pref_Init
				Fnd_Prnt_Init
				Fnd_Pswd_Init
				Fnd_Rec_Init
				Fnd_Reg_Init
				Fnd_Shell_Init
				Fnd_Sort_Init
				Fnd_SqNo_Init
				Fnd_Text_Init
				Fnd_Tlbr_Init
				Fnd_VS_Init
				Fnd_Wnd_Init
				
				Fnd_List_DoesListExist  // Fixed a bug in this method
				
				
				Fnd_FontDiscovery  // Now called earlier in startup so had to make certain there was not a loop with Fnd_Gen_Init
				
				//  New Logging routines
				Compiler_Fnd_Log
				Fnd_Log_AddEntry
				Fnd_Log_CloseLog
				Fnd_Log_CloseLog2
				Fnd_Log_DeclareLog
				Fnd_Log_Enable
				Fnd_Log_FileName
				Fnd_Log_FormEventAsText
				Fnd_Log_Info
				Fnd_Log_Init
				Fnd_Log_Path
				Fnd_Log_SilentError
				Fnd_Log_StopLogWriter
				Fnd_Log_This
				Fnd_Log_UseLog
				TimetoDie  // Breaks naming convention but it's a cool name  😀
				
				// formMethod [projectForm]/Fnd_FontDiscovery/{formMethod}
				
			: ($ModDate_d=!2021-04-14!) & ($Developer_t="WBS")
				Compiler_Fnd_List  // Method declarations adde/modified
				Fnd_FCS_ListToTextFile  // Updated comments (no code change)
				Fnd_FCS_TrnsListToJSON  // Updated comments (no code change)
				Fnd_Gen_Init  //  Define and store the database name and preferences folder in Storage.Fnd.Gen
				Fnd_List_DoesListExist  // New command to determine if a list is defined, useful for checking translations for example
				Fnd_List_XML_FilePath  // Change to store Fnd_List.xml in the preferences folder rather than the Resources folder
				Fnd_Log_Path  // Change to store logs in new Preferences folder
				Fnd_Pref_Path  // Change to store preferences within new Preferences folder
				// [projectForm]/Fnd_IO_InputForm/process
				
				
			: ($ModDate_d=!2021-04-13!) & ($Developer_t="WBS")
				Compiler_Fnd_Date  // Swap to use Fnd.Date.EntryFilterValue
				Fnd_Date_EntryFilter  // Swap to use Fnd.Date.EntryFilterValue
				Fnd_Date_Init  // Swap to use Fnd.Date.EntryFilterValue
				Fnd_Date_MonthName  // Swap to use Fnd.Date.EntryFilterValue
				Fnd_Find_AddItem  // Removed Fnd_Gen_ComponentAvailable check
				Fnd_Find_PrefsPut  // Removed Fnd_Gen_ComponentAvailable check
				Fnd_List_DoesListExist  // New command to determine if a list is defined, useful for checking translations for example
				Fnd_Nav_Redraw  // Copied this code from the Form Method so it can be called via CALL FORM 
				Fnd_Shell_Sort
/*
[projectForm]/___TestForm
[projectForm]/Fnd_IO_InputForm/Form
[projectForm]/Fnd_IO_InputForm/process
[projectForm]/Fnd_IO_InputForm/Storage
[projectForm]/Fnd_IO_InputForm
[projectForm]/Fnd_Sort_Dialog/{formMethod}
*/
				
				
			: ($ModDate_d=!2021-04-05!) & ($Developer_t="WBS")
/* Started making certain  nothing is written inside the application */
				
				
				aaaMods
				Compiler_Fnd_Log
				Fnd_Bttn_Init  // Tidied up previous day's work
				Fnd_Bttn_LibraryToResource  // This probably needs deleting
				Fnd_FCS_ListToTextFile  // Added a woring in comments to only use in development
				Fnd_Gen_FileName  // Use Folder separator constant
				Fnd_Gen_GetDatabaseInfo  // Removed IP arrays and swapped to Storage.Fnd.Gen.propertyName
				Fnd_Gen_Init  // Added initialisation of Storage.Fnd.Gen
				Fnd_Gen_SetDatabaseInfo  // Removed IP arrays and swapped to Storage.Fnd.Gen.propertyName
				Fnd_Init  // Added initialisation of Storage.Fnd
				Fnd_Log_FileName  // Use Folder separator constant
				Fnd_Log_Init  // Removed platform considerations
				Fnd_Log_Path  // Use Folder separator constant, remove MacOS 9 support
				Fnd_Shell_Init  // Added initialisation of Storage.Fnd.Shell
				Fnd_FCS_WriteDocumentation  // Automatic Markdown creation
				
				// Later that Day:
				Compiler_Fnd_Nav  // Removed some IP variables
				Fnd_Art_Init  // Some debugging required
				Fnd_Art_StartupDialog  // Some debugging required
				Fnd_Art_StartupDialogClose  // Some debugging required
				Fnd_Dlg_Init  // Added Font info from the Storage.font object
				
				Fnd_Loc_Editor_Export  // Use Folder separator constant
				Fnd_Loc_Editor_Import  // Use Folder separator constant
				Fnd_Nav_Add  // Removed IP variable
				Fnd_Nav_AddButtonMethod  // Removed IP variable
				Fnd_Nav_AddButtonTable  // Removed IP variable
				Fnd_Nav_Delete  // Removed IP variable
				Fnd_Nav_Display  // Removed IP variables, stored Window Reference
				Fnd_Nav_Init  // Added Storage.Fnd.Nav shared object
				Fnd_Nav_Redraw  // A new method to resize the windo
				Fnd_Nav_WindowWidth  // Now uses Storage.font data
				
/*
[projectForm]/Fnd_Dlg_OSX_Dialog  // Refined form resizing and the picture Icon
[projectForm]/Fnd_Nav_Palette  // Button type and form resizing
*/
				
			: ($ModDate_d=!2021-04-04!) & ($Developer_t="WBS")
/* Started making certain  nothing is written inside the application */
				Fnd_Bttn_Init  // Removed code to convert images
				
		End case 
		
	: (Year of:C25($ModDate_d)=2020)
		Case of 
			: ($ModDate_d=!2020-09-22!) & ($Developer_t="WBS")
				// Created a new branch for Fnd_Loc using new translation json files instead of lists
		End case 
		
	: (Year of:C25($ModDate_d)=2019)
		Case of 
			: ($ModDate_d=!2019-11-11!) & ($Developer_t="WBS")  //Fnd_List
				// Eliminated most button variables in Fnd_List
				// Implemented type-ahead in Choice List
				
			: ($ModDate_d=!2019-10-30!) & ($Developer_t="WBS")
				// Removed Fnd_Gen_ComponentReport
				// Fnd_Art refactored to get rid of IP vars
				
				Fnd_Gen_Platform  // this method has been retained for use in host db but has been rewritten
				// Replaced all calls Fnd_Gen_Platform =Windows with a call to Is Windows
				// Replaced all calls Fnd_Gen_Platform #Windows with a call to Is macOS
				// Replaced all calls Fnd_Gen_Platform =10 with a call to Is macOS
				// Too many to note!!
				
				// Methods:
				Fnd_FCS_ConfirmListExists
				Fnd_FCS_ListToTextFile
				Fnd_Obj_SaveToFile
				Fnd_FCS_TrnsListToJSON
				Compiler_Fnd_FCS
				Fnd_Gen_SilentError
				
			: ($ModDate_d=!2019-10-26!) & ($Developer_t="WBS")
				Fnd:=New object:C1471  // Process Object system introduced
				Storage:C1525.Fnd:=New shared object:C1526  // Storage Object system introduced
				Fnd_Init
				
				Fnd_Wnd_Cancel
				
				Fnd_Art_About
				Fnd_Art_SetAboutForm
				Fnd_Art_SetStartupDialogForm
				
		End case 
		
	: (Year of:C25($ModDate_d)=2018)
		Case of 
			: ($ModDate_d=!2018-05-18!) & ($Developer_t="WBS")
				Fnd_Gen_RequiredHookMethod  // WBS - added Walt's fix for menus being greyed out
				
			: ($ModDate_d=!2018-04-18!) & ($Developer_t="DN")
				Fnd_Menu_BuildMenuBar  // Dave Nasralla
				
			: ($ModDate_d=!2018-04-16!) & ($Developer_t="WBS")
				Fnd_Gen_ComponentCheck  // Wayne Stewart - Removed all calls
				Fnd_Sort_Display  // Incorporated fix for text fields in v15
				
			: ($ModDate_d=!2018-04-10!) & ($Developer_t="DN")
				Fnd_IO_DisplayTable2  // Dave Nasralla
				
		End case 
		
	: (Year of:C25($ModDate_d)=2017)
		Case of 
			: ($ModDate_d=!2017-05-30!) & ($Developer_t="WN")  // Walt - Added Dave Nasralla's fix for memory allocation errors
				// when sorting on a text field
				Fnd_Sort_Display
			: ($ModDate_d=!2017-05-02!) & ($Developer_t="WBS")  // Wayne Stewart
				// Tidied up some of my code from development of this version
		End case 
		
	: (Year of:C25($ModDate_d)=2016)
		Case of 
			: ($ModDate_d=!2016-11-17!) & ($Developer_t="WBS")  // Mod by Wayne Stewart, 2016-11-17
				//  Toolbar mods
				//    Eliminated all icons from picture library
				
				Fnd_Bttn_CacheMaximumSize
				// New method to override the default 2 MB limit
				
				Fnd_Bttn_Init
				//  Declared path to Disk Cache
				
				Fnd_Bttn_Cache_Image_Add
				//  Added code to store created buttons within the resource folder
				//  This was commented out but has been left there for potential future usage
				
				Fnd_Bttn_MakeMenuIndicatorPic
				//  This now retreives the indicators from within the resource folder
				
				// Renamed Fnd_DoesMethodExist
				Fnd_Shell_DoesMethodExist
				//  To put it into a category
				
				// Deleted
				//  Fnd_CT_Get_FontHeight
				//  Fnd_CT_Get_TextHeight
				
				//  Added
				Fnd_File_DeleteEmptyFolder
				Fnd_File_DeleteFolder
				
				// Constants
				//  Fnd_Gen_DefaultStackSize
				//  This has been set to 0 now per 4D's recommendation
				//  http://doc.4d.com/4Dv16/4D/16/New-process.301-3036322.en.html
				
				Compiler_Fnd_Tlbr2
				
				Fnd_Bttn_AlternativePicturePath
				// A new method to allow the developer to specify an alternative location
				//  for button images
				
		End case 
		
	: (Year of:C25($ModDate_d)=2015)
		Case of 
			: ($ModDate_d=!2015-12-28!)
				
				// Modifications to Form: Fnd_Art_StartupDialog for Windows font issues
				//Fnd_Find_UpdateOperators // added check for 64-bit integer type
				
			: ($ModDate_d=!2015-10-27!)  // Removed 4D Pack plugin
				
		End case 
		
	: (Year of:C25($ModDate_d)=2014)
		Case of 
			: ($ModDate_d=!2014-07-17!)  // Create new method called Fnd_DoesMethodExist
				// 4D Pack no longer has AP Does method exist
				// AP Create method was deprecated, too
				// Fnd_Gen_RequiredHostMethod & Fnd_Gen_RequiredHookMethod
				
			: ($ModDate_d=!2014-04-07!)
				// April 7, 2014:
				//C_BOOLEAN(Fnd_Rec_NewRecord ;$0) in Compiler_Fnd_Rec.
				
			: ($ModDate_d=!2014-02-07!) & ($Developer_t="JC")  //  [1] John Craig, 02/07/2014, Changed | picture operator to COMBINE PICTURES with Superimposition method -->
				//Fnd_Bttn_MakeTextPicture
				//Fnd_Bttn_MakeMenuIndicatorPic
				//Fnd_Bttn_MakeIconPicture
				//Fnd_Bttn_MakeCrntBackground
				//Fnd_Bttn_MakeButton
				//Fnd_Bttn_MakeBackground
				//Fnd_Bttn_LibraryToResource
				// Modified: [1] John Craig, 02/07/2014, Changed | picture operator to COMBINE PICTURES with Superimposition method <--
				
				// Modified: [2] John Craig, 02/07/2014, 14:04:43 Layout "Fnd_List_ChoiceList" - Made it resizable
				// Modified: [3] Fnd_IO_InputDrawButtons John Craig, 02/07/2014, 14:09:00 Changed the button height to match standard user interface guidelines
				
		End case 
		
End case 
