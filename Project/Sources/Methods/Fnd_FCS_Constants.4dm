//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_FCS_Constants

// Creates constant file for Foundation
// NB. Rebuilt automatically on each launch of Foundation
// in interpreted mode unless this is turned off

// Access: Private

// Created by Wayne Stewart (2022-04-18)
// Based on Cannon Smith's Constant routines
//     waynestewart@mac.com
// ----------------------------------------------------


If (False:C215)
	Fnd_FCS_Constants
End if 

Constants_NewFile("Foundation Constants.xlf")

Constants_NewGroup("Foundation Dialog")
Constants_AddLong("Fnd_Dlg_CenterOnScreen"; 1)
Constants_AddLong("Fnd_Dlg_CentreOnScreen"; 1)
Constants_AddLong("Fnd_Dlg_CenterOnWindow"; 2)
Constants_AddLong("Fnd_Dlg_CentreOnWindow"; 2)
Constants_AddLong("Fnd_Dlg_DefaultIcon"; 0)
Constants_AddLong("Fnd_Dlg_MacOSXSheet"; 3)
Constants_AddLong("Fnd_Dlg_NoteIcon"; 1)
Constants_AddLong("Fnd_Dlg_StopIcon"; 3)
Constants_AddLong("Fnd_Dlg_WarnIcon"; 2)

Constants_NewGroup("Foundation Find")
Constants_AddString("Fnd_Find_DummyAttribute"; "Fnd_Find_DummyAttribute")
Constants_AddString("Fnd_Find_Semaphore"; "Fnd_Find_Semaphore")

Constants_NewGroup("Foundation Form Types")
Constants_AddLong("Fnd_Gen_AboutForm"; 6)
Constants_AddLong("Fnd_Gen_InputForm"; 3)
Constants_AddLong("Fnd_Gen_ListEditorForm"; 5)
Constants_AddLong("Fnd_Gen_NoForm"; 0)
Constants_AddLong("Fnd_Gen_OutputForm"; 2)
Constants_AddLong("Fnd_Gen_PreferencesForm"; 4)
Constants_AddLong("Fnd_Gen_SqNoEditorForm"; 7)
Constants_AddLong("Fnd_Gen_UnknownForm"; 1)

Constants_NewGroup("Foundation Log")
Constants_AddString("Fnd_Log_Writer"; "$Fnd_Log_Writer")

Constants_NewGroup("Foundation Miscellaneous")
Constants_AddLong("Fnd_Gen_DefaultStackSize"; 0)
Constants_AddLong("Fnd_Gen_ExtrasVersion"; 600)

Constants_NewGroup("Foundation Preferences")
Constants_AddLong("Fnd_Pref_Local"; 0)
Constants_AddLong("Fnd_Pref_Server"; 1)
Constants_AddLong("Fnd_Pref_Shared"; 2)
Constants_AddString("Fnd_Pref_Semaphore"; "$Fnd_Pref_Semaphore")
Constants_AddLong("Fnd_Pref_SemaphoreTimeout"; 1800)
Constants_AddString("Fnd_Pref_SharedName"; "Fnd_Pref_Shared")

Constants_NewGroup("Foundation Registration")
Constants_AddLong("Fnd_Reg_Demo"; 23002)
Constants_AddLong("Fnd_Reg_Expired"; 23003)
Constants_AddLong("Fnd_Reg_PreDemo"; 23001)
Constants_AddLong("Fnd_Reg_Registered"; 23004)

Constants_NewGroup("Foundation Window Position")
Constants_AddLong("Fnd_Wnd_CenterOnScreen"; 1)
Constants_AddLong("Fnd_Wnd_CentreOnScreen"; 1)
Constants_AddLong("Fnd_Wnd_CenterOnWindow"; 2)
Constants_AddLong("Fnd_Wnd_CentreOnWindow"; 2)
Constants_AddLong("Fnd_Wnd_MacOSXSheet"; 3)
Constants_AddLong("Fnd_Wnd_Stacked"; 4)
Constants_AddLong("Fnd_Wnd_StackedOnWindow"; 5)
Constants_AddLong("Fnd_Wnd_OnTheRight"; 10)
Constants_AddLong("Fnd_Wnd_OnTheLeft"; 20)
Constants_AddLong("Fnd_Wnd_AtTheTop"; 40)
Constants_AddLong("Fnd_Wnd_AtTheBottom"; 80)

Constants_AddLong("Fnd_Wnd_RelativeOnTheRight"; 1000)
Constants_AddLong("Fnd_Wnd_RelativeOnTheLeft"; 2000)
Constants_AddLong("Fnd_Wnd_RelativeAtTheTop"; 4000)
Constants_AddLong("Fnd_Wnd_RelativeAtTheBottom"; 8000)

// These constants are just used to aid with building listbox definition
Constants_NewGroup("Foundation Out")
Constants_AddString("fo_column")
Constants_AddString("fo_field")
Constants_AddString("fo_type")
Constants_AddString("fo_title")
Constants_AddString("fo_width")
Constants_AddString("fo_minWidth")
Constants_AddString("fo_maxWidth")
Constants_AddString("fo_hAlign")
Constants_AddString("fo_visible")
Constants_AddString("fo_style")
Constants_AddString("fo_format")



Constants_SaveFile