//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Rec_BestPopupSize (->popup array) --> Number

  // Returns the best object width to use for a pop-up menu form object.

  // Access Type: Private

  // Parameters: 
  //   $1 : Pointer : The array associated with the popup

  // Returns: 
  //   $0 : Longint : The best object size for the pop-up

  // Created by Dave Batton on Dec 5, 2005
  // ----------------------------------------------------

C_LONGINT:C283($0;$fontSize_i;$fieldType_i;$textWidth_i;$originalValue_i;$endCapSize_i)
C_POINTER:C301($1;$array_ptr)
C_TEXT:C284($fontName_t)

$array_ptr:=$1

$originalValue_i:=$array_ptr->  // So we preserve the selected element.

If (Is Windows:C1573)
	$fontName_t:="Tahoma"
	$fontSize_i:=12
	$endCapSize_i:=40
Else 
	$fontName_t:="Lucida Grande"
	$fontSize_i:=13
	$endCapSize_i:=40
End if 

$textWidth_i:=Fnd_Gen_GetArrayWidth ($array_ptr;$fontName_t;$fontSize_i;Plain:K14:1)

$array_ptr->:=$originalValue_i  // Restore the original value.

$0:=$textWidth_i+$endCapSize_i