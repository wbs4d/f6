//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_MaxLength ({max chars to use{; max limitation}}) --> Number

  // Allows the developer to set the maximum password length, and
  //   optionally the maximum password length defined in the Generator
  //   dialog. Returns the current value of the maximum password length.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The maximum password length setting (optional)
  //   $2 : Longint : The maximum value displayed in the pop-up menu (optional)

  // Returns: 
  //   $0 : Longint : The currently set maximum length

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$1;$2)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_MaxLength_i:=$1
	
	If (Count parameters:C259>=2)
		<>Fnd_Pswd_MaxLengthLimit_i:=$2
	End if 
	
	If (<>Fnd_Pswd_MaxLength_i><>Fnd_Pswd_MaxLengthLimit_i)
		<>Fnd_Pswd_MaxLength_i:=<>Fnd_Pswd_MaxLengthLimit_i
	End if 
End if 

$0:=<>Fnd_Pswd_MaxLength_i
