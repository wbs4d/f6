//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_MenuBarName ({menu name{; Window menu number}}) --> Text

  // Allows the developer to specify the menu bar to display the next time
  //   the menu bar is displayed by a Foundation component.
  // If a blank name is specified, the default Foundation menu bar name is restored.
  // You can also specify an optional number for the Window menu to use if you want
  //   to add a Window menu to a non-Foundation database.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The menu bar name (optional)
  //   $2 : Longint : The number of the Window menu (optional)

  // Returns: 
  //   $0 : Text : The menu bar name to use next

  // Created by Dave Batton on Aug 20, 2003
  // Modified by Dave Batton on Feb 26, 2004
  //   The method name was changed from Fnd_Menu_SetMenuBarName to
  //   Fnd_Menu_MenuBarName.  Now this routine also returns the name of
  //   the current menu bar.  If an empty string is passed, the default menu
  //   bar is restored the next time Fnd_Menu_MenuBar is called. 
  // Modified by Dave Batton on Mar 26, 2007
  //   This method now accepts an optional second parameter that lets you specify
  //   the number of the Window menu.
  // ----------------------------------------------------

C_TEXT:C284($0;$1)
C_LONGINT:C283($2)

Fnd_Menu_Init 

If (Count parameters:C259>=1)
	Fnd_Menu_MenuName_t:=$1
	If (Count parameters:C259>=2)
		Fnd_Menu_WindowMenu_i:=$2
	End if 
End if 

  // If no menu bar name is specified, set it to the default value.
If (Fnd_Menu_MenuName_t="")
	Fnd_Menu_MenuName_t:="Fnd_Menu"
End if 

$0:=Fnd_Menu_MenuName_t
