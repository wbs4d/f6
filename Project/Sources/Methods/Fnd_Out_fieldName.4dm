//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_fieldName (Formula) --> Text

// Pass a formula to determine
// the header to include in the display listbox

// Fnd_Out_fieldTitle("invoice.client.FirstName") --> FirstName

// Access: Shared

// Parameters: 
//   $1 : Text : A formula

// Returns: 
//   $0 : Text : The name of the field

// Created by Wayne Stewart (2022-04-19)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE($theFormula_t : Text)->$fieldName_t : Text

var $relationChain_c : Collection
var $numberOfItems_i : Integer

If (False:C215)
	C_TEXT:C284(Fnd_Out_fieldName; $1; $0)
End if 

// Break down the formula
$relationChain_c:=Split string:C1554($theFormula_t; "."; sk ignore empty strings:K86:1)

$numberOfItems_i:=$relationChain_c.length

Case of 
	: ($numberOfItems_i=0)  // They must have passed an empty string
		Fnd_Gen_BugAlert("Fnd_Out_fieldName"; "A listbox formula was not passed.")
		
	: ($numberOfItems_i=1)  // They passed a field from this table
		$fieldName_t:=$theFormula_t
		
	: ($numberOfItems_i>1)  // There is a chain of at least one relation specified
		$fieldName_t:=$relationChain_c[$numberOfItems_i-1]
		
End case 

