//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_XmlParse (element)

// Parses the XML reference into an array of element IDs and a 2D array of values.

// Access Type: Private

// Parameters: 
//   $1 : Text : A 4D XML reference

// Returns: Nothing

// Created by Dave Batton on May 19, 2003
// Modified by Dave Batton on Dec 14, 2005
//   Now decodes reserved XML characters.
// Modified by Dave Batton on Feb 7, 2006
//   Added support for real number variables.
// ----------------------------------------------------

Fnd_FCS_Trace

C_TEXT:C284($1;$element_t;$elementName_t;$elementValue_t)

$element_t:=$1
Fnd_Pref_XMLElement_t:=$element_t  // Set this for the Fnd_Pref_ErrorHandler method.

DOM GET XML ELEMENT NAME:C730($element_t;$elementName_t)
DOM GET XML ELEMENT VALUE:C731($element_t;$elementValue_t)

$elementValue_t:=Fnd_Pref_XmlDecode($elementValue_t)  // DB051214 - Added

Case of 
	: ($elementName_t="dict")
		<>Fnd_Pref_Left_i:=-999
		<>Fnd_Pref_Top_i:=-999
		<>Fnd_Pref_Right_i:=-999
		<>Fnd_Pref_Bottom_i:=-999
		<>Fnd_Pref_WindowPrefName_t:=<>Fnd_Pref_LastKey_t
		
	: ($elementName_t="key")
		<>Fnd_Pref_LastKey_t:=$elementValue_t
		
	: ($elementName_t="string")
		Fnd_Pref_SetValue(<>Fnd_Pref_LastKey_t;Is text:K8:3;$elementValue_t;Fnd_Pref_Local)
		$elementValue_t:=Fnd_Pref_XmlDecode($elementValue_t)  // DB051214 - Added
		
	: ($elementName_t="json")
		Fnd_Pref_SetValue(<>Fnd_Pref_LastKey_t;Is object:K8:27;$elementValue_t;Fnd_Pref_Local)
		$elementValue_t:=Fnd_Pref_XmlDecode($elementValue_t)  // DB051214 - Added
		
	: (($elementName_t="true") | ($elementName_t="false"))
		Fnd_Pref_SetValue(<>Fnd_Pref_LastKey_t;Is boolean:K8:9;$elementName_t;Fnd_Pref_Local)
		
	: ($elementName_t="integer")
		Case of 
			: (<>Fnd_Pref_LastKey_t="left")
				<>Fnd_Pref_Left_i:=Num:C11($elementValue_t)
			: (<>Fnd_Pref_LastKey_t="top")
				<>Fnd_Pref_Top_i:=Num:C11($elementValue_t)
			: (<>Fnd_Pref_LastKey_t="right")
				<>Fnd_Pref_Right_i:=Num:C11($elementValue_t)
			: (<>Fnd_Pref_LastKey_t="bottom")
				<>Fnd_Pref_Bottom_i:=Num:C11($elementValue_t)
			Else   // Just a regular integer.
				Fnd_Pref_SetValue(<>Fnd_Pref_LastKey_t;Is longint:K8:6;$elementValue_t;Fnd_Pref_Local)
		End case 
		
	: ($elementName_t="real")  // DB060207 - New type.
		Fnd_Pref_SetValue(<>Fnd_Pref_LastKey_t;Is real:K8:4;$elementValue_t;Fnd_Pref_Local)
End case 

$element_t:=DOM Get first child XML element:C723($element_t)
While (OK=1)
	Fnd_Pref_XmlParse($element_t)  // Recursion!
	$element_t:=DOM Get next sibling XML element:C724($element_t)
End while 

If ((<>Fnd_Pref_WindowPrefName_t#"") & (<>Fnd_Pref_Left_i#-999) & (<>Fnd_Pref_Top_i#-999) & (<>Fnd_Pref_Right_i#-999) & (<>Fnd_Pref_Bottom_i#-999))
	$elementValue_t:=String:C10(<>Fnd_Pref_Left_i)+", "+String:C10(<>Fnd_Pref_Top_i)+", "+String:C10(<>Fnd_Pref_Right_i)+", "+String:C10(<>Fnd_Pref_Bottom_i)
	Fnd_Pref_SetValue(<>Fnd_Pref_WindowPrefName_t;-1;$elementValue_t;Fnd_Pref_Local)
	<>Fnd_Pref_WindowPrefName_t:=""
	<>Fnd_Pref_Left_i:=-999
	<>Fnd_Pref_Top_i:=-999
	<>Fnd_Pref_Right_i:=-999
	<>Fnd_Pref_Bottom_i:=-999
End if 
