//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_FileName (path) --> Text

// Returns the file name from the full pathname.

// Access Type: Shared

// Parameters: 
//   $1 : Text : A full pathname

// Returns: 
//   $0 : Text : The file name at the end of the pathname

// Created by Dave Batton on Sep 16, 2003
// Mod by Wayne Stewart, (2021-04-05) - Use Folder separator constant
// ----------------------------------------------------

C_TEXT:C284($0;$1;$path_t)  //; $directorySeparator_t)

$path_t:=$1

//$directorySeparator_t:=folder separator

$0:=Substring:C12($path_t;(Fnd_Gen_PositionR(Folder separator:K24:12;$path_t)+1))