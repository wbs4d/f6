//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_Init

// Initializes both the process and interprocess variables used
//   by the Fnd_Out routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 25, 2005
// Wayne Stewart, (2023-04-30) - Added class features
// ----------------------------------------------------

If (Caps lock down:C547) & (Is compiled mode:C492)
	TRACE:C157
End if 

Fnd_Init

If (Fnd.out=Null:C1517)
	Compiler_Fnd_Out
	
	Fnd.out:=cs:C1710.Fnd_Out.new()
	
End if 

