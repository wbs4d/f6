//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Find_GetExtraWidth --> Number

  // Returns the extra width needed to display the Find dialog so none of the text in the 
  //   pop-up menus gets truncated.
  // Lots of stuff is hard-coded here because we can't get it procedurally.  :-(

  // Access Type: Private

  // Parameters: None

  // Returns: 
  //   $0 : Longint : The extra width required

  // Created by Dave Batton on Feb 27, 2005
  // Modified by Dave Batton on May 13, 2005
  //   Now calls the new Fnd_Find_BestPopupSize method to figure out the best object sizes.
  // ----------------------------------------------------

C_LONGINT:C283($0;$fieldNamesPopupObjectWidth_i;$operatorsPopupObjectWidth_i;$extraWidth_i;$bestWidth_i)

  // Remember, this is called before the window is displayed, so we can't
  //   use BEST OBJECT SIZE or GET OBJECT RECT.
$fieldNamesPopupObjectWidth_i:=145
$operatorsPopupObjectWidth_i:=138

$extraWidth_i:=0

$bestWidth_i:=Fnd_Find_BestPopupSize (->Fnd_Find_FieldNames_at)
If ($bestWidth_i>$fieldNamesPopupObjectWidth_i)
	$extraWidth_i:=$extraWidth_i+$bestWidth_i-$fieldNamesPopupObjectWidth_i
End if 

$bestWidth_i:=Fnd_Find_BestPopupSize (->Fnd_Find_OperatorStrs_at)
If ($bestWidth_i>$operatorsPopupObjectWidth_i)
	$extraWidth_i:=$extraWidth_i+$bestWidth_i-$operatorsPopupObjectWidth_i
End if 

$0:=$extraWidth_i