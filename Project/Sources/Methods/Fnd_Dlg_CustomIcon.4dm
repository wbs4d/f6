//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_CustomIcon (picture library name) --> Text

  // Allows the developer to set and get the custom icon name. 
  // See also Fnd_Dlg_SetIcon.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The picture library name

  // Returns: 
  //   $1 : Text : The current picture name

  // Created by Dave Batton on May 15, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Dlg_Init 

If (Count parameters:C259>=1)
	Fnd_Dlg_IconPictureName_t:=$1
End if 

$0:=Fnd_Dlg_IconPictureName_t