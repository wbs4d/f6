//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Dict

  // Compiler directives for the dictionary module.

  // Access: Private

  // Created by Rob Laveaux
  // Updated by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Dict_Initialized_b)
If (Not:C34(<>Fnd_Dict_Initialized_b))
	C_LONGINT:C283(<>Fnd_Dict_LockCount_ai)
	
	ARRAY TEXT:C222(<>Fnd_Dict_Names_at;0)
	ARRAY LONGINT:C221(<>Fnd_Dict_RetainCounts_ai;0)
	
	ARRAY TEXT:C222(<>Fnd_Dict_Keys_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Dict_Values_at;0;0)
	ARRAY LONGINT:C221(<>Fnd_Dict_DataTypes_ai;0;0)
End if 

C_BOOLEAN:C305(Fnd_Dict_Initialized_b)
If (Not:C34(Fnd_Dict_Initialized_b))
	C_POINTER:C301(Fnd_Dict_Destination_ptr)
	C_TEXT:C284(Fnd_Dict_RootNode_t;Fnd_Dict_ItemNode_t;Fnd_Dict_KeyNode_t;Fnd_Dict_TypeNode_t;Fnd_Dict_ValueNode_t)
End if 

If (False:C215)
	C_LONGINT:C283(Fnd_Dict_New ;$0)
	C_TEXT:C284(Fnd_Dict_New ;$1)
	
	C_LONGINT:C283(Fnd_Dict_Release ;$1)
	
	C_LONGINT:C283(Fnd_Dict_SetText ;$1)
	C_TEXT:C284(Fnd_Dict_SetText ;$2;$3)
	
	C_TEXT:C284(Fnd_Dict_GetText ;$0;$2)
	C_LONGINT:C283(Fnd_Dict_GetText ;$1)
	
	C_LONGINT:C283(Fnd_Dict_ID ;$0)
	C_TEXT:C284(Fnd_Dict_ID ;$1)
	
	C_BOOLEAN:C305(Fnd_Dict_IsValid ;$0)
	C_LONGINT:C283(Fnd_Dict_IsValid ;$1)
	
	C_BOOLEAN:C305(Fnd_Dict_HasKey ;$0)
	C_LONGINT:C283(Fnd_Dict_HasKey ;$1)
	C_TEXT:C284(Fnd_Dict_HasKey ;$2)
	
	C_LONGINT:C283(Fnd_Dict_SetLongint ;$1;$3)
	C_TEXT:C284(Fnd_Dict_SetLongint ;$2)
	
	C_LONGINT:C283(Fnd_Dict_SetReal ;$1)
	C_TEXT:C284(Fnd_Dict_SetReal ;$2)
	C_REAL:C285(Fnd_Dict_SetReal ;$3)
	
	C_LONGINT:C283(Fnd_Dict_SetDate ;$1)
	C_TEXT:C284(Fnd_Dict_SetDate ;$2)
	C_DATE:C307(Fnd_Dict_SetDate ;$3)
	
	C_LONGINT:C283(Fnd_Dict_SetTime ;$1)
	C_TEXT:C284(Fnd_Dict_SetTime ;$2)
	C_TIME:C306(Fnd_Dict_SetTime ;$3)
	
	C_LONGINT:C283(Fnd_Dict_SetBoolean ;$1)
	C_TEXT:C284(Fnd_Dict_SetBoolean ;$2)
	C_BOOLEAN:C305(Fnd_Dict_SetBoolean ;$3)
	
	C_LONGINT:C283(Fnd_Dict_SetPointer ;$1)
	C_TEXT:C284(Fnd_Dict_SetPointer ;$2)
	C_POINTER:C301(Fnd_Dict_SetPointer ;$3)
	
	C_LONGINT:C283(Fnd_Dict_GetLongint ;$0;$1)
	C_TEXT:C284(Fnd_Dict_GetLongint ;$2)
	
	C_REAL:C285(Fnd_Dict_GetReal ;$0)
	C_LONGINT:C283(Fnd_Dict_GetReal ;$1)
	C_TEXT:C284(Fnd_Dict_GetReal ;$2)
	
	C_DATE:C307(Fnd_Dict_GetDate ;$0)
	C_LONGINT:C283(Fnd_Dict_GetDate ;$1)
	C_TEXT:C284(Fnd_Dict_GetDate ;$2)
	
	C_TIME:C306(Fnd_Dict_GetTime ;$0)
	C_LONGINT:C283(Fnd_Dict_GetTime ;$1)
	C_TEXT:C284(Fnd_Dict_GetTime ;$2)
	
	C_BOOLEAN:C305(Fnd_Dict_GetBoolean ;$0)
	C_LONGINT:C283(Fnd_Dict_GetBoolean ;$1)
	C_TEXT:C284(Fnd_Dict_GetBoolean ;$2)
	
	C_POINTER:C301(Fnd_Dict_GetPointer ;$0)
	C_LONGINT:C283(Fnd_Dict_GetPointer ;$1)
	C_TEXT:C284(Fnd_Dict_GetPointer ;$2)
	
	C_LONGINT:C283(Fnd_Dict_Keys ;$1)
	C_POINTER:C301(Fnd_Dict_Keys ;$2)
	
	C_LONGINT:C283(Fnd_Dict_Values ;$1)
	C_POINTER:C301(Fnd_Dict_Values ;$2)
	
	C_TEXT:C284(Fnd_Dict_Info ;$0;$1)
	
	C_LONGINT:C283(Fnd_Dict_ItemCount ;$0;$1)
	
	C_TEXT:C284(Fnd_Dict_Key ;$0)
	C_LONGINT:C283(Fnd_Dict_Key ;$1;$2)
	
	C_LONGINT:C283(Fnd_Dict_Remove ;$1)
	C_TEXT:C284(Fnd_Dict_Remove ;$2)
	
	C_LONGINT:C283(Fnd_Dict_SetValue ;$1;$4)
	C_TEXT:C284(Fnd_Dict_SetValue ;$2;$3)
	
	C_TEXT:C284(Fnd_Dict_GetValue ;$0;$2)
	C_LONGINT:C283(Fnd_Dict_GetValue ;$1)
	
	C_LONGINT:C283(Fnd_Dict_Retain ;$1)
	
	C_LONGINT:C283(Fnd_Dict_RetainCount ;$0;$1)
	
	C_TEXT:C284(Fnd_Dict_Name ;$0)
	C_LONGINT:C283(Fnd_Dict_Name ;$1)
	
	C_LONGINT:C283(Fnd_Dict_DataType ;$0;$1)
	C_TEXT:C284(Fnd_Dict_DataType ;$2)
	
	C_LONGINT:C283(Fnd_Dict_SaveToBlob ;$1)
	C_POINTER:C301(Fnd_Dict_SaveToBlob ;$2)
	
	C_LONGINT:C283(Fnd_Dict_Save ;$1)
	C_POINTER:C301(Fnd_Dict_Save ;$2)
	
	C_LONGINT:C283(Fnd_Dict_SaveToFile ;$1)
	C_TEXT:C284(Fnd_Dict_SaveToFile ;$2)
	
	C_LONGINT:C283(Fnd_Dict_LoadFromBlob ;$0)
	C_POINTER:C301(Fnd_Dict_LoadFromBlob ;$1)
	
	C_LONGINT:C283(Fnd_Dict_LoadFromFile ;$0)
	C_TEXT:C284(Fnd_Dict_LoadFromFile ;$1)
	
	C_LONGINT:C283(Fnd_Dict_Load ;$0)
	C_POINTER:C301(Fnd_Dict_Load ;$1)
	
	C_LONGINT:C283(Fnd_Dict_SetArray ;$1)
	C_TEXT:C284(Fnd_Dict_SetArray ;$2)
	C_POINTER:C301(Fnd_Dict_SetArray ;$3)
	
	C_LONGINT:C283(Fnd_Dict_GetArray ;$1)
	C_TEXT:C284(Fnd_Dict_GetArray ;$2)
	C_POINTER:C301(Fnd_Dict_GetArray ;$3)
	
	C_BOOLEAN:C305(Fnd_Dict_LockInternalState ;$1)
End if 
