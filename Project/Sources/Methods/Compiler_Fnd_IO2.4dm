//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_IO2

// Compiler variables related to the Foundation Dialog routines.
// Called by Fnd_IO_Init.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on August 20, 2003
// [1] Added by: John Craig (12/14/09)
// Modified by: Walt Nelson (3/1/10) per DGN
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_IO_Initialized_b)
If (Not:C34(<>Fnd_IO_Initialized_b))  // So we only do this once.
	Compiler_Fnd_IO
	
	C_BOOLEAN:C305(<>Fnd_IO_PassSelectionReceived_b)
End if 


// Process Variables
C_BOOLEAN:C305(Fnd_IO_Initialized_b)
If (Not:C34(Fnd_IO_Initialized_b))
	Compiler_Fnd_IO
	
	C_LONGINT:C283(Fnd_IO_OffscreenButton_i)
	C_LONGINT:C283(Fnd_IO_AddRecordNumber_i; Fnd_IO_CallingProcessNumber_i; Fnd_IO_AddTableNumber_i)
	C_TEXT:C284(Fnd_IO_OutputFormName_t; Fnd_IO_InputFormName_t; Fnd_IO_ReportFormName_t)
	C_TEXT:C284(Fnd_IO_IconGroupName_t)
	C_BOOLEAN:C305(Fnd_IO_DisplayNavButtons_b; Fnd_IO_AddMultipleRecords_b; Fnd_IO_MultiWindow_b)
End if 


// Parameters
If (False:C215)
	C_BOOLEAN:C305(Fnd_IO_AddMultipleRecords; $0; $1)
	
	C_BOOLEAN:C305(Fnd_IO_DisplayNavButtons; $0; $1)
	
	C_LONGINT:C283(Fnd_IO_DisplayRecord; $0)
	C_POINTER:C301(Fnd_IO_DisplayRecord; $1)
	C_LONGINT:C283(Fnd_IO_DisplayRecord; $2)
	C_BOOLEAN:C305(Fnd_IO_DisplayRecord; $3)  // [1] Added by: John Craig (12/14/09)
	
	C_POINTER:C301(Fnd_IO_DisplayRecord2; $1)
	C_LONGINT:C283(Fnd_IO_DisplayRecord2; $2; $3)
	C_BOOLEAN:C305(Fnd_IO_DisplayRecord2; $4)  // [1] Added by: John Craig (12/14/09)
	
	C_LONGINT:C283(Fnd_IO_DisplayRecord3; $1)
	
	C_POINTER:C301(Fnd_IO_DisplayTable; $1)
	C_BOOLEAN:C305(Fnd_IO_DisplayTable; $2)
	
	C_POINTER:C301(Fnd_IO_DisplayTable2; $1)
	
	C_TEXT:C284(Fnd_IO_Info; $0; $1)
	
	C_BOOLEAN:C305(Fnd_IO_InputFormClose; $0)
	
	C_TEXT:C284(Fnd_IO_InputFormName; $0; $1)
	
	C_POINTER:C301(Fnd_IO_InputFormObjectMethod; $1)
	C_TEXT:C284(Fnd_IO_InputFormObjectMethod; $2)  // Modified by: Walt Nelson (3/1/10) per DGN
	
	C_TEXT:C284(Fnd_IO_GetWindowTitle; $0)
	C_POINTER:C301(Fnd_IO_GetWindowTitle; $1)
	C_LONGINT:C283(Fnd_IO_GetWindowTitle; $2)
	
	C_BOOLEAN:C305(Fnd_IO_MultiWindow; $0; $1)
	
	C_TEXT:C284(Fnd_IO_OutputFormName; $0; $1)
	
	C_LONGINT:C283(Fnd_IO_RecordEdited; $0; $2)
	C_POINTER:C301(Fnd_IO_RecordEdited; $1)
	
	C_TEXT:C284(Fnd_IO_ToolbarIconGroup; $0; $1)
End if 
