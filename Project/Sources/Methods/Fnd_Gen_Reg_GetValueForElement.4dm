//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_GetValueForElement (element) --> Text

  // Returns the value for the specified element in the Foundation registration xml file.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The name of the element to find

  // Returns: 
  //   $0 : Text : Its value

  // Created by Dave Batton on Oct 2, 2007
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$value_t;$elementName_t;$productName_t;$LicencePath_t)
C_TEXT:C284($xmlRootRef_t;$elementRef_t;$ignore_t)
C_LONGINT:C283($index_i)

$elementName_t:=$1

$value_t:=""  // Default return value.


  // Modified by: Wayne Stewart 2nd March 2009

$LicencePath_t:=Fnd_Gen_Reg_LicenseFilePath 

If (Test path name:C476($LicencePath_t)=Is a document:K24:1)
	$xmlRootRef_t:=DOM Parse XML source:C719($LicencePath_t)
	
	ARRAY TEXT:C222($productElements_at;0)
	$ignore_t:=DOM Find XML element:C864($xmlRootRef_t;"registration/product";$productElements_at)
	
	For ($index_i;1;Size of array:C274($productElements_at))
		$elementRef_t:=DOM Find XML element:C864($productElements_at{$index_i};"product/product_name")
		
		If (OK=1)  // A match was found.
			DOM GET XML ELEMENT VALUE:C731($elementRef_t;$productName_t)
			If ($productName_t="Foundation")
				$elementRef_t:=DOM Find XML element:C864($productElements_at{$index_i};"product/"+$elementName_t)
				
				If (OK=1)  // A match was found.
					DOM GET XML ELEMENT VALUE:C731($elementRef_t;$value_t)
				End if 
				
			End if 
		End if 
		
	End for 
	
Else 
	  // do I need to change anything?
End if 

$0:=$value_t
