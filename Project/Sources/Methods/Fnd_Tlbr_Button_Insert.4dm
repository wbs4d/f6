//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Insert (beforeButton; name; buttonText; iconName; method{; keystroke})

// Inserts a new button to the toolbar.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The button in the position to insert the new button
//   $2 : Text : A unique name for the button object
//   $3 : Text : The button label
//   $4 : Text : A Picture Library name for the icon
//   $5 : Text : The method to run when clicked
//   $6 : Text : The Command/Ctrl keyboard equivalent (optional)

// Returns:
//   Nothing

// Created by Rob Laveaux on December 24 2005
// ----------------------------------------------------

C_TEXT:C284($1;$2;$3;$4;$5;$6;$beforeButton_t;$buttonName_t;$buttonLabel_t)
C_TEXT:C284($iconName_t;$method_t;$shortcut_t)
C_LONGINT:C283($objectNumber_i;$index_i)

$beforeButton_t:=$1
$buttonName_t:=$2
$buttonLabel_t:=$3
$iconName_t:=$4
$method_t:=$5

If (Count parameters:C259>=6)
	$shortcut_t:=$6
Else 
	$shortcut_t:=""
End if 

Fnd_Tlbr_Init

// Find the index of the button
$objectNumber_i:=Find in array:C230(Fnd_Tlbr_ObjectNames_at;$beforeButton_t)
If ($objectNumber_i=-1)
	// If not found, we insert at the end
	$objectNumber_i:=Fnd_Tlbr_UsedObjects_i+1
End if 

If (Fnd_Tlbr_UsedObjects_i<<>Fnd_Tlbr_MaxObjects_i)
	
	// Shift the existing buttons to the back to make room for the new button
	For ($index_i;Fnd_Tlbr_UsedObjects_i;$objectNumber_i;-1)
		Fnd_Tlbr_ObjectNames_at{$index_i+1}:=Fnd_Tlbr_ObjectNames_at{$index_i}
		Fnd_Tlbr_ObjectLabels_at{$index_i+1}:=Fnd_Tlbr_ObjectLabels_at{$index_i}
		Fnd_Tlbr_ObjectIconNames_at{$index_i+1}:=Fnd_Tlbr_ObjectIconNames_at{$index_i}
		Fnd_Tlbr_ObjectTypes_ai{$index_i+1}:=Fnd_Tlbr_ObjectTypes_ai{$index_i}
		Fnd_Tlbr_ObjectEnabled_ab{$index_i+1}:=Fnd_Tlbr_ObjectEnabled_ab{$index_i}
		Fnd_Tlbr_ObjectMethods_at{$index_i+1}:=Fnd_Tlbr_ObjectMethods_at{$index_i}
		Fnd_Tlbr_ObjectShortcuts_at{$index_i+1}:=Fnd_Tlbr_ObjectShortcuts_at{$index_i}
	End for 
	
	//Insert the button info into the toolbar button info stack
	Fnd_Tlbr_ObjectNames_at{$objectNumber_i}:=$buttonName_t
	Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}:=$buttonLabel_t
	Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i}:=$iconName_t
	Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}:=0
	Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i}:=True:C214  // DB050510
	Fnd_Tlbr_ObjectMethods_at{$objectNumber_i}:=$method_t
	Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i}:=$shortcut_t
	
	Fnd_Tlbr_UsedObjects_i:=Fnd_Tlbr_UsedObjects_i+1
	
	Fnd_Tlbr_ButtonsAreValid_b:=False:C215
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Maximum number of toolbar objects reached.")
End if 
