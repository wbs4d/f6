//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_LicenseFilePath --> Path

  // Returns the path to the license file.

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Text : File path

  // Created by Dave Batton on Oct 2, 2007
  // ----------------------------------------------------

C_TEXT:C284($0)

$0:=Get 4D folder:C485(Current resources folder:K5:16)+"Fnd_License.xml"