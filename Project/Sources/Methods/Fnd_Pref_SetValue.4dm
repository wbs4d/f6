//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetValue (name; type; value; scope)

// Adds a preference item to the arrays.  Doesn't check the semaphore.
//   We assume the calling process has done that, since this method
//   is only called internally by other Fnd_Pref routines.
// Assumes Fnd_Pref_Init has been called.

// Access Type: Private

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Longint : The variable type
//   $3 : Text : The value to save as text
//   $4 : Longint : How to save it

// Returns: Nothing

// Created by Dave Batton on Dec 12, 2003
// Modified by Dave Batton on Nov 6, 2004
//   Added a test so we'll ignore preferences with blank names.
// ----------------------------------------------------

C_TEXT:C284($1; $3; $prefName_t; $prefValue_t)
C_LONGINT:C283($2; $4; $prefType_i; $prefScope_i; $element_i)

$prefName_t:=$1
$prefType_i:=$2
$prefValue_t:=$3
$prefScope_i:=$4

If ($prefName_t#"")  // DB041106 - Added this test.
	If ($prefScope_i=Fnd_Pref_Shared)
		Fnd_Pref_SaveServerPreference(Fnd_Pref_SharedName; $prefName_t; $prefType_i; $prefValue_t; $prefScope_i)
		
	Else 
		$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $prefName_t)
		
		If ($element_i=-1)
			$element_i:=Size of array:C274(<>Fnd_Pref_Names_at)+1
			INSERT IN ARRAY:C227(<>Fnd_Pref_Names_at; $element_i)
			INSERT IN ARRAY:C227(<>Fnd_Pref_Types_ai; $element_i)
			INSERT IN ARRAY:C227(<>Fnd_Pref_Values_at; $element_i)
			INSERT IN ARRAY:C227(<>Fnd_Pref_Scopes_ai; $element_i)
			INSERT IN ARRAY:C227(<>Fnd_Pref_Modified_ab; $element_i)
		End if 
		
		<>Fnd_Pref_Names_at{$element_i}:=$prefName_t
		<>Fnd_Pref_Types_ai{$element_i}:=$prefType_i
		<>Fnd_Pref_Values_at{$element_i}:=$prefValue_t
		<>Fnd_Pref_Scopes_ai{$element_i}:=$prefScope_i
		<>Fnd_Pref_Modified_ab{$element_i}:=True:C214
	End if 
End if 