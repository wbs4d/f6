//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo 
//   method for more information.

// Responds to:
//   version: The current version of the component
//   name: The name of the component
//   minimum_extras_version: The minimum version number of Foundation Extras to compare to the Fnd_Gen_ExtrasVersion constant
//   minimum_extras_version_string: The minimum version number of Foundation Extras as a string

// Access Type: Shared

// Parameters: 
//   $1 : Text : Info desired

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on Dec 27, 2004
// Modified by Dave Batton on Feb 7, 2006
//   Added the new "minimum_extras_version" option.
//   This routine no longer calls Fnd_Pref_Init.
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="version")
		$reply_t:="6.0"
		
	: ($request_t="name")
		$reply_t:="Foundation General"
		
	: ($request_t="language")
		$reply_t:=Fnd_Loc_LanguageCode
		//Case of 
		//: (True)
		//$reply_t:=Fnd_Loc_LanguageCode
		
		//: (Fnd_Gen_ComponentAvailable("Fnd_Loc"))
		//EXECUTE METHOD("Fnd_Loc_LanguageCode"; $reply_t)
		
		//Else 
		//$reply_t:="EN"
		//End case 
		
	: ($request_t="minimum_extras_version")  // See Fnd_Gen_PlugInAvailable.
		$reply_t:="0420"
		
	: ($request_t="minimum_extras_version_string")  // Used by Fnd_Gen_Init.
		$reply_t:="4.2"
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
