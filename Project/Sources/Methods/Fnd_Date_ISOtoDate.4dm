//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_ISOtoDate (date string) --> Date

  // Retrieves the date value from an ISO 8601 formatted date. The input
  //   can be in either of these formats:
  //      YYYY-MM-DD
  //      YYYY-MM-DDThh:mm:ss

  // Access: Shared

  // Parameters: 
  //   $1 : Text : An ISO format date

  // Returns: 
  //   $0 : Date : The date

  // Created by Dave Batton on Jan 7, 2005
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_DATE:C307($0;$date_d)
C_TEXT:C284($1;$isoDate_t)
C_TEXT:C284($year_t;$month_t;$day_t)

$isoDate_t:=$1

If (Length:C16($isoDate_t)>=10)
	$year_t:=Substring:C12($isoDate_t;1;4)
	$month_t:=Substring:C12($isoDate_t;6;2)
	$day_t:=Substring:C12($isoDate_t;9;2)
	$date_d:=Fnd_Date_YearMonthDayToDate (Num:C11($year_t);Num:C11($month_t);Num:C11($day_t))
Else 
	$date_d:=!00-00-00!
End if 

$0:=$date_d