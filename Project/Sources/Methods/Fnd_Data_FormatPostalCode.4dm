//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatPostalCode (postal code{; style}) --> Text

  // Formats a text value into a postal code.  If it can't figure
  //   out how to format it then it returns the postal code unchanged and sets
  //   the Fnd_Data_FormatError_i variable to 1.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The postal code to format
  //   $2 : Longint : The style to use (optional, but not yet implemented or declared)

  // Returns: 
  //   $0 : Text : The formatted phone number

  // Created by Dave Batton on May 12, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Added explanation to $2 description in header
  // ----------------------------------------------------

  // ### - The style parameter is not yet implemented. Just does US and Canadian postal codes.

C_TEXT:C284($1;$0;$postalCode_t)
C_LONGINT:C283($i)

$postalCode_t:=$1

Fnd_Data_Init 
Fnd_Data_FormatError_i:=0

If (Not:C34(Fnd_Data_FormatBypass ))
	$i:=1
	While ($i<=Length:C16($postalCode_t))
		If (($postalCode_t[[$i]]<"a") | ($postalCode_t[[$i]]>"z")) & (($postalCode_t[[$i]]<"0") | ($postalCode_t[[$i]]>"9"))
			$postalCode_t:=Delete string:C232($postalCode_t;$i;1)  // If its not a letter or a number then remove it.
		Else 
			$i:=$i+1
		End if 
	End while 
	
	Case of 
		: ($postalCode_t="")  // no zip was entered so don't do anything
			
		: (Length:C16($postalCode_t)=5) & (Fnd_Text_FormatNumber ($postalCode_t;"00000")=$postalCode_t)  // It's a regular U.S. Zip code.
			$postalCode_t:=Fnd_Text_FormatNumber ($postalCode_t;"00000")
			
		: (Length:C16($postalCode_t)=9)
			$postalCode_t:=Fnd_Text_FormatNumber ($postalCode_t;"00000-0000")
			
		: (Length:C16($postalCode_t)#6)  // So we don't get a compiler runtime Fnd_Data_FormatError_i.
			Fnd_Data_FormatError_i:=1
			
		: (Length:C16($postalCode_t)=6) & (String:C10(Num:C11($postalCode_t[[2]]+$postalCode_t[[4]]+$postalCode_t[[6]]);"000")=($postalCode_t[[2]]+$postalCode_t[[4]]+$postalCode_t[[6]])) & (($postalCode_t[[1]]>="a") | ($postalCode_t[[1]]<="z")) & (($postalCode_t[[3]]>="a") | ($postalCode_t[[3]]<="z")) & (($postalCode_t[[5]]>="a") | ($postalCode_t[[5]]<="z"))  // ***  It's a Canadian postal code.  ***
			$postalCode_t:=Uppercase:C13(Insert string:C231($postalCode_t;" ";4))
			
		Else 
			Fnd_Data_FormatError_i:=1
	End case 
End if 

$0:=$postalCode_t