//%attributes = {"invisible":true,"executedOnServer":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_GetDistinctAttributes (Field Pointer; Array Pointer {; Selection Only})

// This method returns the list of distinct paths from the field listed.
//   By default this will be for all records in the table
//   rather than just the current selection.
// By passing True in the {optional} second parameter
//   only paths for the current selection will be returned.

// Access: Shared

// Parameters:
//   $1 : Pointer : Pointer to an object field
//   $2 : Pointer : Pointer to a text array to retreive the attribute paths
//   $3 : Boolean : (optional) Return only paths for current selection

// Created by Wayne Stewart (2017-03-19)
//     waynestewart@mac.com
// ----------------------------------------------------

C_POINTER:C301($1)
C_POINTER:C301($2)
C_BOOLEAN:C305($3)

C_BOOLEAN:C305($CurrentSelection_b)
C_POINTER:C301($Array_ptr; $Field_ptr)

If (False:C215)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes; $1)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes; $2)
	C_BOOLEAN:C305(Fnd_Find_GetDistinctAttributes; $3)
End if 

$Field_ptr:=$1
$Array_ptr:=$2

If (Count parameters:C259=3)
	$CurrentSelection_b:=False:C215
Else 
	$CurrentSelection_b:=$3
End if 

If ($CurrentSelection_b)  //  This is easy
	DISTINCT ATTRIBUTE PATHS:C1395($Field_ptr->; $Array_ptr->)
	
Else 
	
	Fnd_Find_GetDistinctAttributes2($Field_ptr; $Array_ptr)
	
End if 