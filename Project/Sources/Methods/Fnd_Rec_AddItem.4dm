//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_AddItem (position; label; method{; ->field{; type}})

// Called by the protected "Fnd_Rec_Add" routines to add stuff to the arrays.
// If the label is blank, the field name will be used.

// Access Type: Private

// Parameters: 
//   $1 : Position : The position
//   $2 : Text : The label
//   $3 : Text : The method
//   $4 : Pointer : The field (optional)
//   $5 : Longint : The field's type (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 20, 2003
// ----------------------------------------------------

C_LONGINT:C283($1; $5; $position_i; $fieldType_i; $fieldLength_i)
C_TEXT:C284($2; $3; Fnd_Rec_FieldName_t; $method_t)
C_POINTER:C301($4; Fnd_Rec_Field_ptr; $null_ptr)
C_BOOLEAN:C305($fieldIndexed_b)

$position_i:=$1
Fnd_Rec_FieldName_t:=$2
$method_t:=$3
$fieldType_i:=0

If (Count parameters:C259>=4)
	Fnd_Rec_Field_ptr:=$4  // Otherwise Fnd_Rec_Field_ptr will be null, which is okay.
	If (Fnd_Rec_FieldName_t="")
		Fnd_Rec_FieldName_t:=Fnd_VS_FieldName(Fnd_Rec_Field_ptr)
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_VS"))
		//EXECUTE METHOD("Fnd_VS_FieldName"; Fnd_Rec_FieldName_t; Fnd_Rec_Field_ptr)
		//Else 
		//Fnd_Rec_FieldName_t:=Field name(Fnd_Rec_Field_ptr)
		//End if 
	End if 
	
	If (Count parameters:C259>=5)
		$fieldType_i:=$5
	Else 
		GET FIELD PROPERTIES:C258(Fnd_Rec_Field_ptr; $fieldType_i; $fieldLength_i; $fieldIndexed_b)
	End if 
End if 

Fnd_Rec_Init

Case of 
	: ($position_i<1)
		$position_i:=1
	: ($position_i>(Size of array:C274(Fnd_Rec_Fields_aptr)+1))
		$position_i:=Size of array:C274(Fnd_Rec_Fields_aptr)+1
End case 

INSERT IN ARRAY:C227(Fnd_Rec_FieldNames_at; $position_i)
Fnd_Rec_FieldNames_at{$position_i}:=Fnd_Rec_FieldName_t
INSERT IN ARRAY:C227(Fnd_Rec_Fields_aptr; $position_i)
Fnd_Rec_Fields_aptr{$position_i}:=Fnd_Rec_Field_ptr
INSERT IN ARRAY:C227(Fnd_Rec_FieldTypes_ai; $position_i)
Fnd_Rec_FieldTypes_ai{$position_i}:=$fieldType_i
