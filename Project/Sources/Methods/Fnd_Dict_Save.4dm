//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_Save (dict ID; ->blob or path)

  // Saves a dictionary as either a blob or a file.

  // Access Type: Private

  // Parameters: 
  //   $1 : Longint : Dictionary ID
  //   $2 : Pointer : Blob or path

  // Returns: Nothing

  // Created by Rob Laveaux
  // Modified by Dave Batton on Sep 24, 2007
  //   Wrapped the XML commands in EXECUTE statements so the method can be compiled
  //   with 4D 2003. Also added an error message in case it gets run in 4D 2003.
  // Modified by Dave Batton on Jan 8, 2008
  //   Changed all of the hard-coded 4D commands in EXECUTE statements to use "Command name" instead.
  // ----------------------------------------------------

C_LONGINT:C283($1;$dictionary_i;$index_i)
C_POINTER:C301($2)
C_TEXT:C284($value_t)

$dictionary_i:=$1
Fnd_Dict_Destination_ptr:=$2

Fnd_Dict_LockInternalState (True:C214)

If (Fnd_Dict_IsValid ($dictionary_i))
	
	  // Create the XML document
	Fnd_Dict_RootNode_t:=DOM Create XML Ref:C861("dictionary")
	
	  // Set the version and name attributes for the root node
	DOM SET XML ATTRIBUTE:C866(Fnd_Dict_RootNode_t;"version";"1.0")
	DOM SET XML ATTRIBUTE:C866(Fnd_Dict_RootNode_t;"name";<>Fnd_Dict_Names_at{$dictionary_i})
	
	  // Loop through the items in the dictionary
	For ($index_i;1;Size of array:C274(<>Fnd_Dict_Keys_at{$dictionary_i}))
		
		  // Create the item, key, type and value elements
		Fnd_Dict_ItemNode_t:=DOM Create XML element:C865(Fnd_Dict_RootNode_t;"item")
		
		Fnd_Dict_KeyNode_t:=DOM Create XML element:C865(Fnd_Dict_ItemNode_t;"key")
		$value_t:=<>Fnd_Dict_Keys_at{$dictionary_i}{$index_i}
		DOM SET XML ELEMENT VALUE:C868(Fnd_Dict_KeyNode_t;$value_t)
		
		Fnd_Dict_TypeNode_t:=DOM Create XML element:C865(Fnd_Dict_ItemNode_t;"type")
		$value_t:=String:C10(<>Fnd_Dict_DataTypes_ai{$dictionary_i}{$index_i})
		DOM SET XML ELEMENT VALUE:C868(Fnd_Dict_TypeNode_t;$value_t)
		
		Fnd_Dict_ValueNode_t:=DOM Create XML element:C865(Fnd_Dict_ItemNode_t;"value")
		$value_t:=<>Fnd_Dict_Values_at{$dictionary_i}{$index_i}
		DOM SET XML ELEMENT VALUE:C868(Fnd_Dict_ValueNode_t;$value_t)
		
	End for 
	
	  // Save the XML data to a blob or to a file
	Case of 
		: (Type:C295(Fnd_Dict_Destination_ptr->)=Is BLOB:K8:12)
			DOM EXPORT TO VAR:C863(Fnd_Dict_RootNode_t;Fnd_Dict_Destination_ptr->)
		: (Type:C295(Fnd_Dict_Destination_ptr->)=Is text:K8:3)
			DOM EXPORT TO FILE:C862(Fnd_Dict_RootNode_t;Fnd_Dict_Destination_ptr->)
	End case 
	
	DOM CLOSE XML:C722(Fnd_Dict_RootNode_t)
	
End if 

Fnd_Dict_LockInternalState (False:C215)
