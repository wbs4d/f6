//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_Direction ({direction}) --> Longint

  // Allows the developer to set or get the currently selected sort direction.
  // If a valid direction is specified, this routine will make it the default direction.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The desired sort direction (optional)
  //       1 = ascending
  //      -1 = descending

  // Returns: 
  //   $0 : Longint : The current sort direction

  // Created by Dave Batton on May 21, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$1;$desiredDirection_i)

Fnd_Sort_Init 

If (Count parameters:C259>=1)
	$desiredDirection_i:=$1
	If (($desiredDirection_i=1) | ($desiredDirection_i=-1))
		Fnd_Sort_Direction_i:=$desiredDirection_i
	End if 
End if 

$0:=Fnd_Sort_Direction_i
