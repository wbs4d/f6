//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_Init

// Initializes both the process and interprocess variables used by the Fnd_Shell routines.
// The Shell routines are designed for use only within the Foundation Shell.  They have
//   not been designed to be useful in other projects.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 15, 2003
// Modified by Dave Batton on Jan 24, 2004
//   Updated to version 4.0.2.
// Modified by Dave Batton on Jun 7, 2004
//   Updated to version 4.0.3.
//   Added special handling for the <>Fnd_Shell_NoQuitProcessNames_at array.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Shell_Info method.
// Modified by Wayne Stewart, 2018-04-16 - Remove multiple calls to Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Shell_Initialized_b; <>Fnd_Shell_NoQuitInitialized_b; Fnd_Shell_Initialized_b)

Fnd_Init  //  All init methods should call this method

If (Not:C34(<>Fnd_Shell_NoQuitInitialized_b))
	ARRAY TEXT:C222(<>Fnd_Shell_NoQuitProcessNames_at; 0)
End if 

If (Not:C34(<>Fnd_Shell_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Shell
	
	Fnd_Shell_Localize
	
	<>Fnd_Shell_Running_b:=False:C215  // Gets set to True in Fnd_Shell_OnStartup.
	
	// We intentionally don't initialize <>Fnd_Shell_NoQuitProcessNames_at here.
	
	<>Fnd_Shell_Initialized_b:=True:C214
End if 

If (Storage:C1525.Fnd.Shell=Null:C1517)  // Make certain this is available
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Shell:=New shared object:C1526  // Not certain if anything will be stored in this yet, I'm creating it for consistency
	End use 
	
End if 




If (Not:C34(Fnd_Shell_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Shell
	
	// Set up the arrays used in the Open Table dialog.
	ARRAY POINTER:C280(Fnd_Shell_TablePtrsArray_aptr; 0)
	ARRAY TEXT:C222(Fnd_Shell_TableNamesArray_at; 0)
	
	Fnd_Shell_Initialized_b:=True:C214
End if 
