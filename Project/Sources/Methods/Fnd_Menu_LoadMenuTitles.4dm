//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_LoadMenuTitles

// Loads the localization for the menu bar.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 31, 2005
// Modified by Dave Batton on Dec 8, 2005
//   Added support for the new Modify Records menu item.
// Modified by Doug Hall Feb 7, 2023
//   Added support for Find Related Menu item in Select menu
// ----------------------------------------------------

<>Fnd_Menu_FileTitle_t:=Fnd_Gen_GetString("Fnd_Menu"; "FileMenu")
<>Fnd_Menu_FileOpenItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "FileOpenTable")
<>Fnd_Menu_FileAdminItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "FileAdministration")
<>Fnd_Menu_FilePrintItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "FilePrint")

<>Fnd_Menu_EditPreferencesItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "EditPreferences")

<>Fnd_Menu_EnterTitle_t:=Fnd_Gen_GetString("Fnd_Menu"; "EnterMenu")
<>Fnd_Menu_EnterNewRecordItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "EnterNewRecord")
<>Fnd_Menu_EnterModifyRecsItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "EnterModifyRecords")
<>Fnd_Menu_EnterDeleteSetItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "EnterDeleteSubset")

<>Fnd_Menu_SelectTitle_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectMenu")
<>Fnd_Menu_SelectShowAllItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectShowAll")
<>Fnd_Menu_SelectShowSetItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectShowSubset")
<>Fnd_Menu_SelectOmitSetItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectOmitSubset")
<>Fnd_Menu_SelectFindItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectFind")
<>Fnd_Menu_SelectQueryEdItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectQueryEditor")
<>Fnd_Menu_SelectSortItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectSort")
<>Fnd_Menu_SelectOrderByEdItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectSortEditor")
<>Fnd_Menu_SelectFindReltdItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "SelectFindRelated")  // JDH 20230207

<>Fnd_Menu_ToolsTitle_t:=Fnd_Gen_GetString("Fnd_Menu"; "ToolsMenu")
<>Fnd_Menu_ToolsNavPaletteItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "ToolsNavPalette")
<>Fnd_Menu_ToolsSpecialFnctItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "ToolsSpecialFunctions")

<>Fnd_Menu_WindowTitle_t:=Fnd_Gen_GetString("Fnd_Menu"; "WindowMenu")
<>Fnd_Menu_WindowCloseAllItem_t:=Fnd_Gen_GetString("Fnd_Menu"; "CloseAllWindows")