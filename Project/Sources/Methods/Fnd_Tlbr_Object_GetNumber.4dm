//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Tlbr_Object_GetNumber (object name) --> Number


  // Returns the object number (array element number) for

  //   the specified object name.


  // Access Type: Private


  // Parameters: 

  //   $1 : Text : A toolbar object name


  // Returns: 

  //   $0 : Longint : The array element number


  // Created by Mark Mitchenall on 20/4/02

  //   Oribinally named toolbar_Item_GetID.

  //Toolbar Component - © mitchenall.com 2002

  // ----------------------------------------------------


C_LONGINT:C283($0;$objectNumber_i)
C_TEXT:C284($1;$objectName_t)

$objectName_t:=$1

$objectNumber_i:=0

If (Size of array:C274(Fnd_Tlbr_ObjectNames_at)=0)
	  //No toolbars defined, return an error  

	$objectNumber_i:=-1
Else 
	$objectNumber_i:=Find in array:C230(Fnd_Tlbr_ObjectNames_at;$objectName_t)
	If ($objectNumber_i<1)
		$objectNumber_i:=0
	End if 
End if 

$0:=$objectNumber_i