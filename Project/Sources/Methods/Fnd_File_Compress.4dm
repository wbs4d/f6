//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_File_Compress (Path to file/folder {; Delete Source })

// Creates a zip archive of a file or folder passed to it
// It does this in another thread so as not to 
// slow down execution of main thread

// Access: Private

// Parameters: 
//   $1 : Text : Path to file/folder to compress
//   $2 : Boolean : Delete source document (optional - default is to delete)
//   $3 : Text : Path to file/folder to compress

// Created by Wayne Stewart (2021-08-08)

//     waynestewart@mac.com
// ----------------------------------------------------

var $1 : Variant

var $Path_t; $archivePath_t; $archiveName_t; $Path_t; $desiredProcessName_t : Text
var $Parameters_o; $Source_o; $results_o; $destination_o; $folder_o : Object
var $delete_b; $synchronous_b : Boolean
var $pathType_i; $valueType_i; $Parameters_i : Integer

// Check the parameters
$Parameters_i:=Count parameters:C259

If ($Parameters_i=0)
	CALL WORKER:C1389(1; "Fnd_Gen_BugAlert"; "Fnd_File_Compress"; "Method called without parameters")
	
Else 
	$valueType_i:=Value type:C1509($1)
	
	Case of 
		: ($valueType_i=Is object:K8:27)
			If ($1.sourceDocument=Null:C1517)
				CALL WORKER:C1389(1; "Fnd_Gen_BugAlert"; "Fnd_File_Compress"; "No Source Document")  // Run in application process to prevent interrupting execution
			Else 
				$Path_t:=$1.sourceDocument
				$delete_b:=Choose:C955(($1.deleteSource=Null:C1517); False:C215; $1.deleteSource)
				$synchronous_b:=Choose:C955(($1.synchronous=Null:C1517); False:C215; $1.synchronous)
			End if 
			
		: ($valueType_i=Is text:K8:3)
			$Path_t:=$1
			If ($Parameters_i>1)
				$delete_b:=$2
			Else 
				$delete_b:=True:C214
			End if 
	End case 
	
End if 

If ($synchronous_b)
	$DesiredProcessName_t:=Current process name:C1392
Else 
	$DesiredProcessName_t:="$zip worker"
End if 

If (Length:C16($Path_t)>0)  // Only continue if we have a path
	If (Current process name:C1392#$DesiredProcessName_t)  // Move to a new worker
		If ($valueType_i=Is text:K8:3)
			CALL WORKER:C1389($DesiredProcessName_t; "Fnd_File_Compress"; $Path_t; $delete_b)
		Else 
			CALL WORKER:C1389($DesiredProcessName_t; "Fnd_File_Compress"; $1)
		End if 
	Else 
		
		$pathType_i:=Test path name:C476($Path_t)
		
		Case of 
			: ($pathType_i=Is a document:K24:1)
				$Source_o:=File:C1566($path_t; fk platform path:K87:2)
				$ArchivePath_t:=$Path_t+".zip"
				
			: ($pathType_i=Is a folder:K24:2)
				$folder_o:=Folder:C1567($Path_t)
				If (Not:C34($folder_o.isPackage))  // If it's a package we don't do this
					$ArchivePath_t:=Substring:C12($Path_t; 1; Length:C16($Path_t)-1)+".zip"  // Get rid of the folder separator at the end
				End if 
				
				$Source_o:=Folder:C1567($path_t; fk platform path:K87:2)
				
			Else 
				CALL WORKER:C1389(1; "Fnd_Gen_BugAlert"; "Fnd_File_Compress"; "Incorrect file path\r"+$Path_t)
				
		End case 
		
		If ($Source_o#Null:C1517)
			
			$destination_o:=File:C1566($ArchivePath_t; fk platform path:K87:2)
			$results_o:=ZIP Create archive:C1640($Source_o; $destination_o)
			
			If ($results_o.success) & ($delete_b)  // Get rid of source document?
				If ($pathType_i=Is a document:K24:1)
					DELETE DOCUMENT:C159($Path_t)
				Else 
					DELETE FOLDER:C693($Path_t; Delete with contents:K24:24)
				End if 
			End if 
			
			// Do we want to kill worker? I'm not sure
			//TimetoDie
		End if 
		
		
	End if 
	
End if 
