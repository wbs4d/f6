//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_QuitNow ({quit?}) --> Boolean

// Returns True if the user wants to quit. Pass True to tell Foundation
//   to do its quit stuff now.

// Access Type: Shared

// Parameters: 
//   $1 : Boolean : Cause Foundation to quit now? (optional)

// Returns: 
//   $0 : Boolean : True if it's time to quit

// Created by Dave Batton on Sep 8, 2003
// Modified by Dave Batton on Feb 5, 2004
//   We no longer call Fnd_Gen_Init when we're just checking to see if it's time to quit.
// Modified by Dave Batton on Feb 26, 2006
//   Changed the "Fnd_Shell_Quitter" process from a local to a server process to
//     support the new features in the Preferences component.
// Modified by : Vincent TOURNIER (12/09/11)
// ----------------------------------------------------

C_BOOLEAN:C305($0;$1;<>Fnd_Gen_Quitting_b)  // Modified by : Vincent TOURNIER (12/09/11)
C_LONGINT:C283($processNumber_i)

If (Count parameters:C259>=1)
	Fnd_Gen_Init
	<>Fnd_Gen_Quitting_b:=$1
	If (<>Fnd_Gen_Quitting_b)  // Modified by : Vincent TOURNIER (12/09/11)
		//If (Fnd_Gen_ComponentAvailable("Fnd_Shell"))
		$processNumber_i:=New process:C317("Fnd_Shell_Quitter";Fnd_Gen_DefaultStackSize;"Fnd_Shell_Quitter")  // DB060226
		//End if 
	End if 
End if 

$0:=<>Fnd_Gen_Quitting_b
