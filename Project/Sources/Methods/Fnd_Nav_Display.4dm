//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_Display

// Displays the navigation palette.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 5, 2003
// Modified by Dave Batton on Feb 5, 2004
//   We now clear the arrays that held the setup info as soon as the palette is closed.
//   The window is no longer opened larger than the number of available buttons.
// Modified by Dave Batton on May 22, 2004
//   I was using 4D's "Palette window" constant in an EXECUTE statement, but that
//      didn't work with non-English versions of 4D. Now "1984" is used instead.
//   Changed the maximum number of buttons from 20 to 40.
// Modified by Dave Batton on Nov 9, 2004
//   Modified the preference name slightly. Didn't release an update just for this.
// Modified by Dave Batton on Jan 3, 2005
//   Now uses the window reference returned by the 'Open window' function instead
//      of using the 'Frontmost window' function.
// Modified by Dave Batton on Dec 14, 2005
//   Made an adjustment to account for the lack of Fnd_Wnd_MoveOnScreen getting called with multiple monitors.
// Modified by Dave Batton on Feb 26, 2006
//   Changed the error handler from Fnd_Gen_DummyMethod to Fnd_Wnd_DummyMethod.
// ----------------------------------------------------

C_LONGINT:C283($numButtons_i; $windowWidth_i; $neededWidth_i; $windowRef_i)
C_TEXT:C284($buttonLabel_t)

Fnd_Nav_Init

// If the developer hasn't specified anything for the palette, do it automatically now.
// We must mess with the arrays before we launch the new process.
If (Size of array:C274(<>Fnd_Nav_Labels_at)=0)
	Fnd_Nav_AutoConfigure
End if 

If (Fnd_Gen_LaunchAsNewProcess("Fnd_Nav_Display"; Storage:C1525.Fnd.Nav.ProcessName))
	If (Size of array:C274(<>Fnd_Nav_Labels_at)>0)
		<>Fnd_Nav_Left_i:=0
		<>Fnd_Nav_Top_i:=0
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Pref"))
		//EXECUTE METHOD("Fnd_Pref_GetWindow"; *; "Fnd_Nav: Navigation Palette Window"; -><>Fnd_Nav_Left_i; -><>Fnd_Nav_Top_i; -><>Fnd_Nav_Right_i; -><>Fnd_Nav_Bottom_i)
		//End if 
		Fnd_Pref_GetWindow("Fnd_Nav: Navigation Palette Window"; -><>Fnd_Nav_Left_i; -><>Fnd_Nav_Top_i; -><>Fnd_Nav_Right_i; -><>Fnd_Nav_Bottom_i)
		
		$windowWidth_i:=Fnd_Nav_WindowWidth
		
		// If no prefs exist for the palette position, use this default.
		Case of 
			: (<>Fnd_Nav_Top_i#0)
				// We got what appears to be a valid preference value.
			: (Is Windows:C1573)
				<>Fnd_Nav_Left_i:=Screen width:C187-$windowWidth_i-4
				<>Fnd_Nav_Top_i:=Menu bar height:C440+20  // The constant value is to account for the window's title bar.
			Else   // Mac OS 9 or X.
				<>Fnd_Nav_Left_i:=Screen width:C187-$windowWidth_i
				<>Fnd_Nav_Top_i:=Menu bar height:C440+12  // The constant value is to account for the window's title bar.
		End case 
		
		<>Fnd_Nav_Right_i:=<>Fnd_Nav_Left_i+$windowWidth_i
		<>Fnd_Nav_Bottom_i:=<>Fnd_Nav_Top_i+Fnd_Nav_WindowHeight
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
		//EXECUTE METHOD("Fnd_Wnd_MoveOnScreen"; *; -><>Fnd_Nav_Left_i; -><>Fnd_Nav_Top_i; -><>Fnd_Nav_Right_i; -><>Fnd_Nav_Bottom_i; Palette window)
		//End if 
		Fnd_Wnd_MoveOnScreen(-><>Fnd_Nav_Left_i; -><>Fnd_Nav_Top_i; -><>Fnd_Nav_Right_i; -><>Fnd_Nav_Bottom_i; Palette window:K34:3)
		
		Fnd_Gen_MenuBar  // Disables the Navigation Palette menu item.
		
		
		$windowRef_i:=Open window:C153(<>Fnd_Nav_Left_i; <>Fnd_Nav_Top_i; <>Fnd_Nav_Right_i; <>Fnd_Nav_Bottom_i; -Palette window:K34:3; ""; "Fnd_Nav_DummyMethod")  // DB060226
		
		Use (Storage:C1525.Fnd.Nav)
			Storage:C1525.Fnd.Nav.WindowReference:=$windowRef_i
		End use 
		
		DIALOG:C40("Fnd_Nav_Palette")
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Pref"))
		//EXECUTE METHOD("Fnd_Pref_SetWindow"; *; "Fnd_Nav: Navigation Palette Window"; $windowRef_i)
		//End if 
		Fnd_Pref_SetWindow("Fnd_Nav: Navigation Palette Window"; $windowRef_i)
		
		CLOSE WINDOW:C154
		
		Use (Storage:C1525.Fnd.Nav)
			Storage:C1525.Fnd.Nav.WindowReference:=-1
		End use 
		
	End if 
	
	// Clear the arrays so we start fresh the next time the palette is displayed.
	If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 300)))  // Wait up to 5 seconds.
		ARRAY TEXT:C222(<>Fnd_Nav_Labels_at; 0)  // DB040205 - Added these array decs.
		ARRAY POINTER:C280(<>Fnd_Nav_TablePtrs_aptr; 0)
		ARRAY TEXT:C222(<>Fnd_Nav_Methods_at; 0)
		CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
	End if 
End if 
