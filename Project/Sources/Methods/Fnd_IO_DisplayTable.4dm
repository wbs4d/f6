//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_DisplayTable (->table{; force new window})

  // Displays the output list for the specified table.
  // If the table has already been displayed in a process by this method, it is
  //   brought to the front.  Otherwise a new process is created to display the
  //   table.
  // If the optional parameter is passed and is True, a new table is created
  //   even if one already exists.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the table to display
  //   $2 : Boolean : Open a new window, even if one is already open? (optional)

  // Returns: Nothing

  // Created by Dave Batton on May 2, 2003
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_BOOLEAN:C305($2;$forceNew_b)
C_LONGINT:C283($processNumber_i)
C_TEXT:C284($processName_t)

$table_ptr:=$1

If (Count parameters:C259>=2)
	$forceNew_b:=$2
Else 
	$forceNew_b:=False:C215
End if 

$processName_t:=Substring:C12(Table name:C256($table_ptr)+" List";1;30)  // Process names are limited to 30 characters.

If ($forceNew_b)
	$processNumber_i:=New process:C317("Fnd_IO_DisplayTable2";Fnd_Gen_DefaultStackSize;$processName_t;$table_ptr)
	
Else 
	$processNumber_i:=New process:C317("Fnd_IO_DisplayTable2";Fnd_Gen_DefaultStackSize;$processName_t;$table_ptr;*)
	If ($processNumber_i>0)
		BRING TO FRONT:C326($processNumber_i)
	End if 
End if 

