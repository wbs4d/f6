//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Nav_DummyMethod

  // A generic method with no code.  This is useful for assigning as the close box method
  //   for new windows (handle the close in the form method) or to use with ON ERR CALL
  //   to disable any automatic reaction to errors.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 26, 2006
  // ----------------------------------------------------
