//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Fix (->table)

  // Fixeds the next number for the tables ID field by finding the highest
  //   number used so far and adding one to it.

  // Access: Shared

  // Parameters:
  //   $1 : Pointer : A pointer to the table to fix

  // Returns: Nothing

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Feb 28, 2006
  //   Replaced an unnecessary call to Fnd_VS_TableName with a call to Table name.
  // ----------------------------------------------------

C_POINTER:C301($1;$keyFieldPtr;$tablePtr)
C_BOOLEAN:C305($readOnly)
C_TEXT:C284($seqNoName)
C_LONGINT:C283($nextSeqNo)

  // The code in this method primarily just figures out the last sequence number 
  //   used in the specified table.  The SeqNoReset method does the work of 
  //   updating the sequence number record.

Fnd_SqNo_Init 

$tablePtr:=$1
$keyFieldPtr:=Fnd_SqNo_IDFieldPtr ($tablePtr)

  // Just an extra check to be safe.
If (Type:C295($keyFieldPtr->)#Is longint:K8:6)
	Fnd_Gen_BugAlert ("Fnd_SqNo_Fix";"The ID field type specified for the ["+Table name:C256($tablePtr)+"] table in the Fnd_Hook_SqNo_SetIDField method must be a Long Integer.")
End if 

  // We'll save the current record selection, just to be safe.
CUT NAMED SELECTION:C334($tablePtr->;"Fnd_SqNo_SavedSelection")

  // We'll also need to restore the read-write state.
$readOnly:=Read only state:C362($tablePtr->)

READ ONLY:C145($tablePtr->)
SCAN INDEX:C350($keyFieldPtr->;1;<)
$nextSeqNo:=$keyFieldPtr->+1

$seqNoName:=Fnd_SqNo_TableSeqNoName ($tablePtr)
Fnd_SqNo_Set ($seqNoName;$nextSeqNo)

  // Restore the read-write state.
If (Not:C34($readOnly))
	READ WRITE:C146($tablePtr->)
End if 

  // Restore the selection.
USE NAMED SELECTION:C332("Fnd_SqNo_SavedSelection")