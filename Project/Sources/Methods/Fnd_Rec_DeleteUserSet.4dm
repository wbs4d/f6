//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_DeleteUserSet

// Deletes the highlighted records in the current selection (the "UserSet").
// Asks the user first if they're sure the records should be deleted.
// Designed to be called from the Delete button on the toolbar or from a Delete menu item.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 20, 2003
// Modified by Dave Batton on Feb 20, 2004
//   Define the UserSet by calling Fnd_Grid_MakeSetFromSelection if it's available.
// Modified by Dave Batton on May 30, 2004
//   Changed the Fnd_Loc_GetString calls to Fnd_Gen_GetString calls.
// Modified by Dave Batton on Dec 8, 2005
//   Fixed a problem with the locked records alert. It wasn't displaying the number properly.
//   Replaced the Fnd_Rec_Alert and Fnd_Rec_Confirm calls with matching calls to the Dialog component.
// Modified by Dave Batton on Feb 26, 2006
//   Change the Fnd_Wnd_Set calls to use the new method names.
//   Added support for the Fnd_Out component.
// ----------------------------------------------------

C_LONGINT:C283($totalRecords_i; $lockedRecords_i)
C_BOOLEAN:C305($allowDelete_b; $usingFndOut_b)
C_OBJECT:C1216($listBox_es; $selected_es; $notDropped_es)

Fnd_Rec_Init

TRACE:C157

$usingFndOut_b:=Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active"
$allowDelete_b:=True:C214

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Rec_Delete")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Rec_Delete"; $allowDelete_b)
End if 

If ($allowDelete_b)  // Only if the developer wants Foundation to handle it.
	
	If ($usingFndOut_b)
		$listBox_es:=Form:C1466.listData.copy()
		$selected_es:=Form:C1466.selectedItems.copy()
		$totalRecords_i:=$selected_es.length
	Else 
		$totalRecords_i:=Records in set:C195("UserSet")
	End if 
	
	Fnd_Wnd_Title(Fnd_Gen_GetString("Fnd_Rec"; "DeleteWindowTitle"))
	Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
	Case of 
		: ($totalRecords_i=0)
			Fnd_Dlg_Alert(Fnd_Gen_GetString("Fnd_Rec"; "MustHighlightFirst"))
			OK:=0
		: ($totalRecords_i=1)
			OK:=Fnd_Dlg_Confirm(Fnd_Gen_GetString("Fnd_Rec"; "ConfirmDeleteOne"))
		Else 
			OK:=Fnd_Dlg_Confirm(Fnd_Gen_GetString("Fnd_Rec"; "ConfirmDeleteMany"; String:C10($totalRecords_i)))
	End case 
	
	
	If (OK=1)
		If ($usingFndOut_b)
			$listBox_es:=$listBox_es.minus($selected_es).copy()
			$notDropped_es:=Form:C1466.selectedItems.drop().copy()
			If ($notDropped_es#Null:C1517)
				$lockedRecords_i:=$notDropped_es.length
				If ($lockedRecords_i>0)
					$listBox_es:=$listBox_es.or($notDropped_es).copy()
				End if 
			End if 
			
		Else 
			CREATE SET:C116(Fnd_Gen_CurrentTable->; "Fnd_Rec_OriginalSet")
			DIFFERENCE:C122("Fnd_Rec_OriginalSet"; "UserSet"; "Fnd_Rec_OriginalSet")
			USE SET:C118("UserSet")
			DELETE SELECTION:C66(Fnd_Gen_CurrentTable->)
			USE SET:C118("Fnd_Rec_OriginalSet")
			CLEAR SET:C117("Fnd_Rec_OriginalSet")
			
			// Handle any locked records here.
			$lockedRecords_i:=Records in set:C195("LockedSet")
		End if 
		
		If ($lockedRecords_i#0)
			Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)  // DB060226 - Added this.
			If ($lockedRecords_i=1)
				OK:=Fnd_Dlg_Confirm(Fnd_Gen_GetString("Fnd_Rec"; "ViewOneLocked"))
			Else 
				OK:=Fnd_Dlg_Confirm(Fnd_Gen_GetString("Fnd_Rec"; "ViewManyLocked"; String:C10($lockedRecords_i)))  // DB050731
			End if 
			
			
			If ($usingFndOut_b)
				If (OK=1)
					Form:C1466.listData:=$notDropped_es.copy()
				Else 
					Form:C1466.listData:=$listBox_es.copy()
				End if 
				Fnd_Out_Update  // This will refresh the listbox display but not the 4D current selection
				
			Else 
				If (OK=1)
					USE SET:C118("LockedSet")
				End if 
				CLEAR SET:C117("LockedSet")
				
				Fnd_Gen_SelectionChanged  // Always call this after changing the selection.
			End if 
			
		End if 
		
		
	End if 
End if 
