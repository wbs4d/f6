//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: SetMenuBar

  // Doesn't do anything other than not cause errors.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : An ignored parameter

  // Returns: Nothing

  // Created by Dave Batton on Mar 4, 2004
  // ----------------------------------------------------

C_LONGINT:C283($1)
