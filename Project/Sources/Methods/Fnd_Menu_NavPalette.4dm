//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_NavPalette

// Called from the menu bar.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 29, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Shell component is no longer required.
// ----------------------------------------------------

//If (Fnd_Gen_ComponentAvailable("Fnd_Shell"))
//EXECUTE METHOD("Fnd_Shell_NavigationPalette"; *)
//End if 

Fnd_Shell_NavigationPalette