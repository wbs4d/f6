//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_UpdateToolbar

// Enables and disables the toolbar buttons based on whether or not
//   the window is frontmost, the selection of records, and the highlighted
//   records.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 17, 2004
// Modified by Dave Batton on Jun 26, 2004
//   Made a change to support the Grid component.
//   Changed from a Private to a Protected method for the Grid component.
// Modified by Dave Batton on Jan 30, 2005
//   Removed some code here that's now handled by the Fnd_Tlbr_FormMethod routine.
//   Changed the Fnd_Tlbr_Button_Enabled_Set calls to use the new Fnd_Tlbr_Button_Enabled name.
// Modified by Dave Batton on Jul 9, 2005
//   Added some code to re-enable some buttons in case they were disabled by
//   displaying the input form in the same process as the output form.
// ----------------------------------------------------

C_BOOLEAN:C305($noSelection_b;$noUserSet_b)
C_LONGINT:C283($highlightedRecords_i)

Case of 
	: (Current process:C322#Frontmost process:C327(*))
		// We only want to mess with this when we're the frontmost process.
		
		
	: (Fnd_Gen_CurrentFormType=Fnd_Gen_InputForm)
		// Disable the output buttons.
		Fnd_Tlbr_Button_Enabled("New";False:C215)
		Fnd_Tlbr_Button_Enabled("Delete";False:C215)
		Fnd_Tlbr_Button_Enabled("ShowAll";False:C215)
		Fnd_Tlbr_Button_Enabled("Find";False:C215)
		Fnd_Tlbr_Button_Enabled("Sort";False:C215)
		Fnd_Tlbr_Button_Enabled("Print";True:C214)
		
		//If (Fnd_Tlbr_StyleName="retro")
		//  ` Enable the input buttons.
		//Fnd_Tlbr_Button_Enabled ("Save";True)
		//Fnd_Tlbr_Button_Enabled ("Cancel";True)
		//Fnd_Tlbr_Button_Enabled ("First";True)
		//Fnd_Tlbr_Button_Enabled ("Previous";True)
		//Fnd_Tlbr_Button_Enabled ("Next";True)
		//Fnd_Tlbr_Button_Enabled ("Last";True)
		//End if 
		
		
	: (Fnd_Gen_CurrentFormType=Fnd_Gen_OutputForm)
		If (Records in selection:C76(Fnd_Gen_CurrentTable->)=0)
			$noSelection_b:=True:C214
			$noUserSet_b:=True:C214
		Else 
			$noSelection_b:=False:C215
			
			Case of 
				: (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
					$highlightedRecords_i:=Form:C1466.selectedItems.length
				Else 
					$highlightedRecords_i:=Records in set:C195("UserSet")
			End case 
			
			If ($highlightedRecords_i=0)
				$noUserSet_b:=True:C214
			Else 
				$noUserSet_b:=False:C215
			End if 
		End if 
		
		// Set the output buttons.
		Fnd_Tlbr_Button_Enabled("New";True:C214)  // DB050709 - Added. Needed in case multi-window is False.
		Fnd_Tlbr_Button_Enabled("Delete";($noUserSet_b=False:C215))
		Fnd_Tlbr_Button_Enabled("ShowAll";True:C214)  // DB050709 - Added.
		Fnd_Tlbr_Button_Enabled("Find";True:C214)  // DB050709 - Added.
		Fnd_Tlbr_Button_Enabled("Sort";($noSelection_b=False:C215))
		Fnd_Tlbr_Button_Enabled("Print";True:C214)  // DB050709 - Added.
		
		//If (Fnd_Tlbr_StyleName="retro")
		//  ` Disable the input buttons.
		//Fnd_Tlbr_Button_Enabled ("Save";False)
		//Fnd_Tlbr_Button_Enabled ("Cancel";False)
		//Fnd_Tlbr_Button_Enabled ("First";False)
		//Fnd_Tlbr_Button_Enabled ("Previous";False)
		//Fnd_Tlbr_Button_Enabled ("Next";False)
		//Fnd_Tlbr_Button_Enabled ("Last";False)
		//End if 
End case 
