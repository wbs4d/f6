//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_RegistrationDialog ({feature code})

  // Displays the registration dialog. Works even if the product has been registered.
  // Sets the OK button if a valid registration number was entered.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Feature code (optional)

  // Returns: Nothing

  // Created by Dave Batton on Apr 27, 2005
  // Should not use Fnd_Host_ExecuteFormula since the form Fnd_Reg_RegisterDialog
  // in the component now - fix by - Barclay Barry on Nov 18, 2009
  // ----------------------------------------------------

C_TEXT:C284($1;$featureCode_t;$formula_t)
C_POINTER:C301($nil_ptr)
C_LONGINT:C283($ok_i;$width_i;$height_i)

Fnd_Reg_Init 

  // Currently the feature code is ignored. I'll get back to this in a future version.
If (Count parameters:C259>=1)
	Fnd_Reg_DialogFeatureCode_t:=$1
	If (Fnd_Reg_DialogFeatureCode_t="")
		Fnd_Reg_DialogFeatureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	Fnd_Reg_DialogFeatureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

Fnd_Gen_MenuBar 

If (Fnd_Wnd_Type =-1)
	Fnd_Wnd_Type (Movable form dialog box:K39:8)
End if 

  // old code wwn Nov 18, 2009
  //Fnd_Wnd_OpenFormWindow ($nil_ptr;"Fnd_Reg_RegisterDialog")
  //$formula_t:=Command name(40)+"(\"Fnd_Reg_RegisterDialog\")"  ` 40 = DIALOG
  //EXECUTE METHOD("Fnd_Host_ExecuteFormula";*;$formula_t)
  //
  //EXECUTE METHOD("Fnd_Host_GetIntegerValue";$ok_i;"OK")
  //◊Fnd_Reg_DoneWithDemoDialog_b:=($ok_i=1)

  // new code Nov 18, 2009
FORM GET PROPERTIES:C674("Fnd_Reg_RegisterDialog";$width_i;$height_i)
Fnd_Wnd_OpenWindow ($width_i;$height_i)
DIALOG:C40("Fnd_Reg_RegisterDialog")
<>Fnd_Reg_DoneWithDemoDialog_b:=(<>Fnd_Reg_OKButton_i=1)
  // end new code Nov 18, 2009

CLOSE WINDOW:C154
