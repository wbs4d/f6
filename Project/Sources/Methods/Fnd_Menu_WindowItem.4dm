//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_WindowItem

// This method is called whenever any of the items in the Window menu
//   is selected.  This method moves the associated process to the front.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Oct 21, 2003
// Modified by Dave Batton on Jan 16, 2004
//   Now tests to make sure we don't try to access an array element larger than the array size.
// Modified by Dave Batton on Jan 29, 2004
//   Fixed the bug I introduced in the Jan 16 fix.  None of the Window items worked.
// Modified by Dave Batton on Mar 16, 2006
//   I was calling some other menu routines that used semaphores from within the
//   semaphore that's used here. Bad thing! Moved these so they happen outside
//   of the semaphore.
//   Changed the timeout value to use the new variable rather than being hard-coded.
// ----------------------------------------------------

C_LONGINT:C283($processState_i; $processTime_i; $menuItem_i; $removeFronWindowsMenu_i)
C_TEXT:C284($processName_t)
C_BOOLEAN:C305($updateWindowsMenu_b)

Fnd_Menu_Init

$updateWindowsMenu_b:=False:C215  // DB060316
$removeFronWindowsMenu_i:=0  // DB060316

If (Not:C34(Semaphore:C143(<>Fnd_Menu_Semaphore_t; <>Fnd_Menu_SemaphoreTimeout_i)))  // DB060316
	$menuItem_i:=Menu selected:C152 & 0xFFFF
	
	Case of 
		: ($menuItem_i>(Size of array:C274(<>Fnd_Menu_Window_Processes_ai)+2))  // DB040116 - Added this.  DB040129 - Fixed this.
			// It's not supposed to happen, but during development it's possible that the menu bar
			//   will have items that have been deleted from the matching array.  So this check
			//   prevents runtime errors.
			$updateWindowsMenu_b:=True:C214  // DB060316 - Now we're doing this outside of the semaphore.
			BEEP:C151  // So the user knows something happened.
			
			
		: ($menuItem_i=(Size of array:C274(<>Fnd_Menu_Window_Processes_ai)+2))
			// If the last item was selected, assume it was the Close All Windows item.
			// We don't test the text here, so this will still work if the database is
			//   localized for another language.
			//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
			//EXECUTE METHOD("Fnd_Wnd_CloseAllWindows"; *)
			//End if 
			Fnd_Wnd_CloseAllWindows
			
			
		Else 
			// Otherwise, we should just need to look up the process ID from the array
			//   we created.
			PROCESS PROPERTIES:C336(<>Fnd_Menu_Window_Processes_ai{$menuItem_i}; $processName_t; $processState_i; $processTime_i)
			If ($processState_i>=Executing:K13:4)
				BRING TO FRONT:C326(<>Fnd_Menu_Window_Processes_ai{$menuItem_i})
				// wwn ### when this happens the Menu Title "Window" stays highlighted - cosmetic
				
			Else 
				// If the window doesn't exist, the menu is messed up. Try to fix it.
				// DB060316 - Now we're doing this outside of the semaphore.
				$removeFronWindowsMenu_i:=<>Fnd_Menu_Window_Processes_ai{$menuItem_i}
			End if 
	End case 
	
	CLEAR SEMAPHORE:C144(<>Fnd_Menu_Semaphore_t)
End if 


If ($updateWindowsMenu_b)
	Fnd_Menu_Window_Update  // DB060316 - Moved this call outside of the semaphore.  DB070326 - Changed it to the new method name.
End if 

If ($removeFronWindowsMenu_i#0)
	Fnd_Menu_Window_Remove($removeFronWindowsMenu_i)  // DB060316 - Moved this call outside of the semaphore.
End if 