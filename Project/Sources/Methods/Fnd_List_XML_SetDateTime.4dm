//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_XML_SetDateTime (date-time string)

  // Updates the Fnd_List.xml file with the last use date for this structure file.

  // Access: Private

  // Parameters: 
  //   $1 : Text : A date-time string (e.g.: "2007-10-16T17:55:04")

  // Returns: Nothing

  // Created by Dave Batton on Oct 5, 2007
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($1;$dateTime_t)
C_TEXT:C284($xmlRootRef_t;$dateTimeRef_t)

$dateTime_t:=$1

$xmlRootRef_t:=DOM Create XML Ref:C861("last_exit")

$dateTimeRef_t:=DOM Create XML element:C865($xmlRootRef_t;"date_time")
DOM SET XML ELEMENT VALUE:C868($dateTimeRef_t;$dateTime_t)

DOM EXPORT TO FILE:C862($xmlRootRef_t;Fnd_List_XML_FilePath )

DOM CLOSE XML:C722($xmlRootRef_t)