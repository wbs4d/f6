//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_ArrayToList (->array; list name)

  // A wrapper for 4D's ARRAY TO LIST command. This routine always
  //   appends an asterisk to the list name, so that we don't mess up the original.
  // See also the Fnd_Loc_ListToArray method.

  // Access Type: Private

  // Parameters: 
  //   $1 : Pointer : The text array
  //   $2 : Text : The list name

  // Returns: Nothing

  // Created by Dave Batton on Jun 13, 2004
  // Modified by : Vincent Tournier (6/17/2011)
  // ----------------------------------------------------

C_POINTER:C301($1;$array_ptr)
C_TEXT:C284($2;$listName_t)

$array_ptr:=$1
$listName_t:=$2

$listName_t:=$listName_t+"*"
  // BEGIN Modified by : Vincent Tournier (6/17/2011)
  // ARRAY TO LIST($array_ptr->;$listName_t)
Fnd_Loc_ArrayToList ($array_ptr;$listName_t)
  // END Modified by : Vincent Tournier (6/17/2011)
