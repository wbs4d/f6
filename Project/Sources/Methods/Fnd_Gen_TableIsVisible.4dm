//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_TableIsVisible (Table pointer or number) --> Boolean

// Returns TRUE if table $1-> is visible

// Access: Shared

// Parameters: 
//   $1 : Pointer : the pointer to a table OR table number

// Returns: 
//   $0 : Boolean : True = Visible table

// Created by Doug Hall (20030617)

// ----------------------------------------------------

#DECLARE($Table_v : Variant)->$visibleTable_b : Boolean

var $isInvisible_b : Boolean

GET TABLE PROPERTIES:C687($Table_v; $isInvisible_b)

$visibleTable_b:=Not:C34($isInvisible_b)

