//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_LoadTranslation (Module {; language })

// Loads the Localization Strings for the component
// If the language parameter is not passed then the system language will be used

// Access: Shared

// Parameters: 
//   $1 : TEXT : The Foundation module
//   $2 : TEXT : Language to use (optional)

// Created by Wayne Stewart (2020-09-22)

//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($1)
C_TEXT:C284($2)

C_LONGINT:C283($CurrentItem_i; $NumberOfItems_i)
C_TEXT:C284($component_t; $FilePath_t; $language_t)
C_OBJECT:C1216($Object_o; $TranslationObject_o)

ARRAY TEXT:C222($lookupCodes_at; 0)
ARRAY TEXT:C222($strings_at; 0)

If (False:C215)
	C_TEXT:C284(Fnd_Loc_LoadTranslation; $1; $2)
End if 

$component_t:=$1
If (Count parameters:C259=2)
	$language_t:=$2
Else 
	$language_t:=Fnd_Gen_Info("language")
End if 


$FilePath_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Translations"+Folder separator:K24:12+$component_t+".json"

If (Test path name:C476($FilePath_t)=Is a document:K24:1)
	$Object_o:=Fnd_Obj_LoadFromFile($FilePath_t)
	$TranslationObject_o:=$Object_o[$language_t]
	
	OB GET PROPERTY NAMES:C1232($TranslationObject_o; $lookupCodes_at)
	$NumberOfItems_i:=Size of array:C274($lookupCodes_at)
	
	For ($CurrentItem_i; 1; $NumberOfItems_i)
		APPEND TO ARRAY:C911($strings_at; $TranslationObject_o[$lookupCodes_at{$CurrentItem_i}])
	End for 
End if 


Fnd_Gen_SetLocalizationStrings($component_t; ->$lookupCodes_at; ->$strings_at)
