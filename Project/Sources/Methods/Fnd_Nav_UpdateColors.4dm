//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Nav_UpdateColors

  // Updates the form's object colors.
  // Assumes the calling method handles the necessary semaphore work.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 1, 2004
  // ----------------------------------------------------

C_LONGINT:C283($numButtons_i;$buttonNumber_i;$color_i)
C_TEXT:C284($objectName_t)

$numButtons_i:=Size of array:C274(<>Fnd_Nav_Labels_at)
If ($numButtons_i><>Fnd_Nav_MaxButtons_i)
	$numButtons_i:=<>Fnd_Nav_MaxButtons_i
End if 

For ($buttonNumber_i;1;$numButtons_i)
	$objectName_t:="Fnd_Nav_Label"+String:C10($buttonNumber_i)+"_t"
	OBJECT SET RGB COLORS:C628(*;$objectName_t;<>Fnd_Nav_ForegroundColors_ai{$buttonNumber_i};<>Fnd_Nav_BackgroundColors_ai{$buttonNumber_i})
	
	$objectName_t:="Fnd_Nav_BackgroundRect"+String:C10($buttonNumber_i)
	If (<>Fnd_Nav_BackgroundColors_ai{$buttonNumber_i}=Background color:K23:2)
		$color_i:=0x00DDDDDD  // Our default background color.
	Else 
		$color_i:=<>Fnd_Nav_BackgroundColors_ai{$buttonNumber_i}
	End if 
	OBJECT SET RGB COLORS:C628(*;$objectName_t;$color_i;$color_i)
End for 
