//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dlg_FormAlert

// Displays an alert using a 4D form.  Called from Fnd_Dlg_Display

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 5, 2003
// Modified by Dave Batton on Jan 15, 2005
//   Now calls Fnd_Wnd_OpenWindow rather than Fnd_Gen_CenterWindow.
// Modified by Dave Batton on Feb 14, 2005
//   Now clears the Window component's "use saved position" attribute before displaying the dialog.
// Modified by Dave Batton on May 28, 2005
//   Updated to use the new Fnd_Wnd methods.
// Modified by Dave Batton on Dec 16, 2005
//   Removed some code that would change the window type on Windows.
// Modified by Dave Batton on Sep 15, 2007
//   Updated for 4D v11 SQL.
// ----------------------------------------------------

C_TEXT:C284($formName_t)
C_LONGINT:C283($lines_i; $extraPixels_i; $windowWidth_i; $windowHeight_i)

$extraPixels_i:=0

If (Is Windows:C1573)
	$formName_t:="Fnd_Dlg_OSX_Dialog"  // ### Needs a new form.
	Fnd_Dlg_Text1_t:=Fnd_Dlg_Text1_t+Char:C90(Carriage return:K15:38)+Char:C90(Carriage return:K15:38)+Fnd_Dlg_Text2_t
	Fnd_Dlg_Text2_t:=""
	$lines_i:=Fnd_Ext_TextLines(Fnd_Dlg_Text1_t; 372; <>Fnd_Dlg_FontName_t; <>Fnd_Dlg_FontSize_i; <>Fnd_Dlg_FontStyle_i)
	$extraPixels_i:=$extraPixels_i+(<>Fnd_Dlg_FontHeight_i*($lines_i-1))-8
	// DB051216 - Removed some code that would change the window type on Windows.
Else 
	$formName_t:="Fnd_Dlg_OSX_Dialog"
	$lines_i:=Fnd_Ext_TextLines(Fnd_Dlg_Text1_t; 372; <>Fnd_Dlg_FontName_t; <>Fnd_Dlg_FontSize_i; <>Fnd_Dlg_FontStyle_i)
	$extraPixels_i:=$extraPixels_i+(<>Fnd_Dlg_FontHeight_i*($lines_i-1))-8
	If (Fnd_Dlg_Text2_t#"")
		$extraPixels_i:=$extraPixels_i+<>Fnd_Dlg_SmallFontHeight_i
		$lines_i:=Fnd_Ext_TextLines(Fnd_Dlg_Text2_t; 372; <>Fnd_Dlg_SmallFontName_t; <>Fnd_Dlg_SmallFontSize_i; <>Fnd_Dlg_SmallFontStyle_i)
		$extraPixels_i:=$extraPixels_i+(<>Fnd_Dlg_SmallFontHeight_i*($lines_i))-4
	End if 
End if 

If (Fnd_Dlg_RequestDefault_t#"")
	$extraPixels_i:=$extraPixels_i+34  // Just because this value seems to work well.
End if 

FORM GET PROPERTIES:C674($formName_t; $windowWidth_i; $windowHeight_i)
If ($extraPixels_i<0)
	$extraPixels_i:=0
End if 
$windowHeight_i:=$windowHeight_i+$extraPixels_i

Fnd_Wnd_UseSavedPosition("")  // Clear this value if it was set. The alert should never use this value.
If (Fnd_Wnd_Type=-1)  // If the developer didn't set something...
	Fnd_Wnd_Type(Movable form dialog box:K39:8)
End if 
Fnd_Wnd_OpenWindow($windowWidth_i; $windowHeight_i)

DIALOG:C40($formName_t)  // wwn (07/02/09) v11 DIALOG cannot be executed during the draw of a list.
CLOSE WINDOW:C154

// Set the OK button to 2 if the third button was clicked.
If (Fnd_Dlg_OtherButton_i=1)
	OK:=2
End if 

SET PROCESS VARIABLE:C370(Current process:C322; OK; OK)  // Needed for a component to set the host's version of the variable.