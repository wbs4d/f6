//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Name (index) --> name

// Returns the name of the nth button in the toolbar.

// Access Type: Shared

// Parameters: 
//   $1 : Index :The index number of the buttton (1 - count)

// Returns: 
//   $0 : Text : Button name

// Created by Rob Laveaux on December 24 2005
// ----------------------------------------------------

C_TEXT:C284($0;$buttonName_t)
C_LONGINT:C283($1;$index_i)

$index_i:=$1

Fnd_Tlbr_Init

If ($index_i<=Fnd_Tlbr_UsedObjects_i)
	$buttonName_t:=Fnd_Tlbr_ObjectNames_at{$index_i}
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Index out of range.")
End if 

$0:=$buttonName_t
