//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_EndOfMonth (date) --> Date

  // Returns the last day of the month passed.

  // Access: Shared

  // Parameters: 
  //   $1 : Date : Any date

  // Returns: 
  //   $0 : Date : The last day of the month for the date

  // Created by Dave Batton on Apr 28, 2004
  // ----------------------------------------------------

C_DATE:C307($0;$1;$the_date)

$the_date:=$1

  // There's really no reason to call Fnd_Date_Init here.

  // Add a month to the input date.
$the_date:=Add to date:C393($the_date;0;1;0)

  // Get the first day of that month.
$the_date:=Fnd_Date_YearMonthDayToDate (Year of:C25($the_date);Month of:C24($the_date);1)

  // Get the date of the day before that date.
$the_date:=Add to date:C393($the_date;0;0;-1)

$0:=$the_date