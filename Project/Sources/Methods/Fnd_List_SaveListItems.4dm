//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_SaveListItems

  // Assumes the proper [Fnd_List] record is loaded.
  // Called from the Fnd_List_Editor and ListsCreateSystemLists2 methods.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Dec 15, 2003
  // ----------------------------------------------------

C_TEXT:C284($formula_t)

If (<>Fnd_List_ListItemsModified_b)
	$formula_t:=Command name:C538(384)+"("+String:C10(<>Fnd_List_ListItems_i)+";\""+<>Fnd_List_SelectedListName_t+"\")"  // 384 = SAVE LIST
	EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t)
	
	Fnd_List_ListToRecord (<>Fnd_List_SelectedListName_t)  // Save the list to the [Fnd_List] table so it can survive a structure update.
	<>Fnd_List_ListItemsModified_b:=False:C215
End if 