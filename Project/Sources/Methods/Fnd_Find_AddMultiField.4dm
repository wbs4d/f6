//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Find_AddMultiField (->array of field ptrs; label{; position})


// Lets the developer add one item for searching multiple fields.


// Access Type: Shared


// Parameters: 

//   $1 : Pointer : A pointer to an array of pointers

//   $2 : Text : The label for the popup item

//   $3 : Longint : The position in which to add the first item (optional)


// Returns: Nothing


// Created by Dave Batton on Jul 21, 2003

// ----------------------------------------------------


C_POINTER:C301($1;$field_ptr)
C_TEXT:C284($2;$label_t)
C_LONGINT:C283($3;$position_i)

$field_ptr:=$1
$label_t:=$2

If (Count parameters:C259>=3)
	$position_i:=$3
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Find_AddItem($position_i;$label_t;"";$field_ptr;Pointer array:K8:23)