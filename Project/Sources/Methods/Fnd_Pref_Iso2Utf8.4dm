//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_Iso2Utf8 (text) --> Text

  // Converts ISO text to UTF-8-encoded text.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : ISO text string (use Mac to ISO)

  // Returns: 
  //   $0 : Text : The UTF-8 representation

  // Original method by Scott Vanderbilt, Datagenic Tool & Die, Inc.
  // Copyright 1996-2003 Datagenic Tool & Die, Inc. All Rights Reserved.
  // Variable names changed by Dave Batton to match the Foundation naming conventions.
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$source_t;$destination_t)
C_LONGINT:C283($sourceIndex_i;$destIndex_i;$byte_i;$currentChar_i;$sourceLength_i)

$source_t:=$1

$sourceLength_i:=Length:C16($source_t)

If ($sourceLength_i>0x0000)
	$destination_t:=MAXTEXTLENBEFOREV11:K35:3*Char:C90(Space:K15:42)  // Pre-size destination text var
	$destIndex_i:=0x0001  // Initialize destination text index
	$sourceIndex_i:=0x0001  // Initialize source text index
	  //
	While ($sourceIndex_i<=$sourceLength_i) & ($destIndex_i<MAXTEXTLENBEFOREV11:K35:3)
		$currentChar_i:=Character code:C91($source_t[[$sourceIndex_i]])  // Copy to local for clarity
		  //
		If ($currentChar_i<=0x007F)  // One-byte char (no conversion necessary)
			$destination_t[[$destIndex_i]]:=$source_t[[$sourceIndex_i]]
			$destIndex_i:=$destIndex_i+0x0001  // Increment destination text index
			$sourceIndex_i:=$sourceIndex_i+0x0001  // Increment source text index
			  //
		Else   // Convert to a two byte sequence
			  // All UCS chars between 0x0080 and 0x07FF must be stored in the 5 least
			  // significant bits of the first byte and the 6 least significant bits of the
			  // second byte. Therefore, only the 2 least significant bits of the
			  // first byte will be used in this conversion.
			  //
			  // Construct first byte of two-byte sequence.
			  // 3 leftmost bits of first byte must be 110, therefore, resulting value will be
			  // between 0xC0 and 0xFD. Get 2 most significant bits from char to convert and
			  // store in first byte, along with the multi-byte prefix.
			$byte_i:=0x00C0 | ($currentChar_i >> 6)
			$destination_t[[$destIndex_i]]:=Char:C90($byte_i)
			$destIndex_i:=$destIndex_i+0x0001  // Increment destination text index
			  //
			  // Construct second byte of two-byte sequence.
			  // 2 leftmost bits of second byte must be 10, therefore, resulting value will be
			  // between 0x80 and 0xBF. Get 6 least significant bits from char to convert and
			  // store in second byte, along with the multi-byte prefix.
			$byte_i:=0x0080 | ($currentChar_i & 0x003F)
			$destination_t[[$destIndex_i]]:=Char:C90($byte_i)
			$destIndex_i:=$destIndex_i+0x0001  // Increment destination text index
			  //
			  // Increment source text index
			$sourceIndex_i:=$sourceIndex_i+0x0001
		End if   // ($currentChar_i<=0x007F)
		  //
	End while   // ($sourceIndex_i<=$sourceLength_i) & ($destIndex_i<MAXTEXTLEN )
	  //
	  // Trim destination text to actual size used
	$0:=Substring:C12($destination_t;0x0001;$destIndex_i-0x0001)
	  //
Else   // Source text is empty string.
	$0:=""
End if   // ($sourceLength_i>0x0000)
