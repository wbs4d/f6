//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_UpdateAutoSizeColumns

// Sets the column width for the columns set to auto size.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 24, 2006
// ----------------------------------------------------

/*
C_LONGINT($column_i;$width_i;$arraySize_i;$type_i;$element_i)
C_TEXT($columnObjectName_t)

For ($column_i;1;Size of array(Fnd_Out_ListColumnArrays_aptr))
$columnObjectName_t:="Fnd_Out_Column"+String($column_i)

$width_i:=Fnd_Out_ListColumnWidths_ai{$column_i}
If ($width_i<0)  // Negative = Auto size. Number is the minimum.
$arraySize_i:=Size of array(Fnd_Out_ListColumnArrays_aptr{$column_i}->)
If ($arraySize_i>50)
$arraySize_i:=50
End if 
ARRAY TEXT($widthTestArray_at;$arraySize_i)
$type_i:=Type(Fnd_Out_ListColumnArrays_aptr{$column_i}->)
For ($element_i;1;$arraySize_i)
Case of 
: (($type_i=String array) | ($type_i=Text array))
$widthTestArray_at{$element_i}:=Fnd_Out_ListColumnArrays_aptr{$column_i}->{$element_i}
: (($type_i=LongInt array) | ($type_i=Integer array) | ($type_i=Real array))
$widthTestArray_at{$element_i}:=String(Fnd_Out_ListColumnArrays_aptr{$column_i}->{$element_i};Fnd_Out_ListColumnFormats_at{$column_i})
: ($type_i=Date array)
$widthTestArray_at{$element_i}:=String(Fnd_Out_ListColumnArrays_aptr{$column_i}->{$element_i};Ascii(Fnd_Out_ListColumnFormats_at{$column_i}))
Else 
Fnd_Gen_BugAlert(Current method name;"Unable to handle field type "+String($type_i)+".")
End case 
End for 

$width_i:=Fnd_Gen_GetArrayWidth(->$widthTestArray_at;Fnd.out.columnFontName;Fnd.out.columnFontSize;Plain)+4
If ($width_i<Abs(Fnd_Out_ListColumnWidths_ai{$column_i}))
$width_i:=Abs(Fnd_Out_ListColumnWidths_ai{$column_i})
End if 
SET LISTBOX COLUMN WIDTH(*;$columnObjectName_t;$width_i)
End if 
End for 

*/
