//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetText (name{; default{; scope}}) --> Text

// Retrieves a text value in from user's preferences file.  If an item with the
//   specified name isn't found, the default value is returned (if specified).

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Text : The default value (optional)
//   $3 : Longint : The scope if the default is used (optional)

// Returns: 
//   $0 : Text : The saved value

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Nov 9, 2004
//   Fixed a bug that caused this routine to set the value to the default value
//      if it was already existed, rather than if it didn't exist.
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
//   Added the scope parameter.
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
//   Now calls the corresponding Fnd_Pref_SetText routine to handle default value
// ----------------------------------------------------

C_TEXT:C284($0; $1; $2; $itemName_t; $itemValue_t)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_t:=""
$itemScope_i:=Fnd_Pref_Local

If (Count parameters:C259>=2)
	$itemValue_t:=$2
	If (Count parameters:C259>=3)
		$itemScope_i:=$3
	End if 
End if 

Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $itemName_t)

Case of 
	: ($element_i>0)  // We found it in the local or server prefs.
		If (<>Fnd_Pref_Types_ai{$element_i}=Is text:K8:3)
			$itemValue_t:=<>Fnd_Pref_Values_at{$element_i}
		End if 
		
	: (Fnd_Pref_GetSharedValue($itemName_t; ->$itemValue_t))  // We found it in the shared prefs.
		
	: (Count parameters:C259>=2)  // If a default value was specified, save it.
		Fnd_Pref_SetText($itemName_t; $itemValue_t; $itemScope_i)
End case 


$0:=$itemValue_t
