//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetObject (name{; default{; scope}}) --> Object

// Retrieves an Object in from user's preferences file.  If an item 
//   with the specified name isn't found, the default value is returned (if specified).
//   Otherwise, an empty object is returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Object : The default value (optional)
//   $3 : Longint : The scope if the default is used (optional)

// Returns: 
//   $0 : Longint : The saved value

// Created by Wayne Stewart, (2022-03-17)
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
//   Now calls the corresponding Fnd_Pref_SetObject routine to handle default value
// ----------------------------------------------------

C_OBJECT:C1216($0; $2; $itemValue_o)
C_TEXT:C284($1; $itemName_t)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_o:=New object:C1471
$itemScope_i:=Fnd_Pref_Local

If (Count parameters:C259>=2)
	$itemValue_o:=OB Copy:C1225($2)  // We want to copy it to avoid accidental change later
	If (Count parameters:C259>=3)
		$itemScope_i:=$3
	End if 
End if 

Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $itemName_t)

Case of 
	: ($element_i>0)
		If (<>Fnd_Pref_Types_ai{$element_i}=Is object:K8:27)
			$itemValue_o:=JSON Parse:C1218(<>Fnd_Pref_Values_at{$element_i})
		End if 
		
	: (Fnd_Pref_GetSharedValue($itemName_t; ->$itemValue_o))  // We found it in the shared prefs.
		
	: (Count parameters:C259>=2)  // Only if they specified a default value.
		Fnd_Pref_SetObject($itemName_t; $itemValue_o; $itemScope_i)
		
End case 



$0:=$itemValue_o
