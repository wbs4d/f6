//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_File_DeleteFolder (Path)

// Deletes a folder and all contained files and folders

// Access: Shared

// Parameters: 
//   $1 : TEXT : The Path to the folder

// Created by Wayne Stewart (2016-11-15)
//     waynestewart@mac.com
// ----------------------------------------------------


C_TEXT:C284($1)

C_LONGINT:C283($CurrentDocument_i; $CurrentFolder_i; $NumberOfDocuments_i; $NumberOfFolders_i)
C_TEXT:C284($PathName_t)

ARRAY TEXT:C222($DocumentPaths_at; 0)
ARRAY TEXT:C222($FolderPaths_at; 0)

If (False:C215)
	C_TEXT:C284(Fnd_File_DeleteFolder; $1)
End if 



$PathName_t:=$1

If (Test path name:C476($PathName_t)=Is a folder:K24:2)
	
	DOCUMENT LIST:C474($PathName_t; $DocumentPaths_at; Recursive parsing:K24:13+Absolute path:K24:14)
	
	$NumberOfDocuments_i:=Size of array:C274($DocumentPaths_at)
	For ($CurrentDocument_i; 1; $NumberOfDocuments_i)
		DELETE DOCUMENT:C159($DocumentPaths_at{$CurrentDocument_i})
	End for 
	
	FOLDER LIST:C473($PathName_t; $FolderPaths_at)
	
	$NumberOfFolders_i:=Size of array:C274($FolderPaths_at)
	For ($CurrentFolder_i; 1; $NumberOfFolders_i)
		Fnd_File_DeleteEmptyFolder($PathName_t+$FolderPaths_at{$CurrentFolder_i})
	End for 
	
	Fnd_File_DeleteEmptyFolder($PathName_t)
	
End if 