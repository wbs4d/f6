//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_DoesListExist (List Name) -> Boolean

// Checks if a list exists

// Access: Shared

// Parameters:
//   $1 : Text : A choice list defined in the List Editor

// Returns:
//   $0 : Boolean : True = The list exists

// Created by Wayne Stewart (2021-04-13)
//     waynestewart@mac.com
// ----------------------------------------------------

var $0 : Boolean
var $1 : Text

If (False:C215)
	C_BOOLEAN:C305(Fnd_List_DoesListExist; $0)
	C_TEXT:C284(Fnd_List_DoesListExist; $1)
End if 

var $ListName_t : Text
ARRAY LONGINT:C221($numsArray_ai; 0)
ARRAY TEXT:C222($namesArray_at; 0)

$ListName_t:=$1

LIST OF CHOICE LISTS:C957($numsArray_ai; $namesArray_at)

$0:=(Find in array:C230($namesArray_at; $ListName_t)#-1)
