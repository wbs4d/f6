//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_DemoDays ({feature code{; days}}) --> Number

  // Allows the developer to set and get the number of demo days for the specified feature.
  // This routine does not modify the expiration date if it has already been set.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Feature code (optional)
  //   $2 : Longint : The number of days (optional)

  // Returns: 
  //   $0 : Longint : The number of days

  // Created by Dave Batton on Apr 24, 2005
  // ----------------------------------------------------

C_LONGINT:C283($0;$2;$days_i)
C_TEXT:C284($1;$featureCode_t)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

If (Count parameters:C259>=2)
	$days_i:=$2
	READ WRITE:C146(<>Fnd_Reg_Table_ptr->)
Else 
	$days_i:=<>Fnd_Reg_DefaultDemoDays_i
	READ ONLY:C145(<>Fnd_Reg_Table_ptr->)
End if 

Fnd_Reg_LoadRegistrationRecord ($featureCode_t)

  // Create a registration record for this feature if it doesn't already exist.
Case of 
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=0)
		If (Count parameters:C259>=2)  // Only if we're trying to set it.
			CREATE RECORD:C68(<>Fnd_Reg_Table_ptr->)
			<>Fnd_Reg_IDFld_ptr->:=Sequence number:C244(<>Fnd_Reg_Table_ptr->)
			<>Fnd_Reg_FeatureCodeFld_ptr->:=$featureCode_t
			<>Fnd_Reg_DemoDaysFld_ptr->:=$days_i
			Fnd_Reg_SaveRecord (<>Fnd_Reg_SecretKey_t)
		End if 
		
		
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=1)
		If (Count parameters:C259>=2)  // Only if we're trying to set it.
			If (<>Fnd_Reg_DemoDaysFld_ptr->#$days_i)  // Only if it needs to be modified.
				<>Fnd_Reg_DemoDaysFld_ptr->:=$days_i
				Fnd_Reg_SaveRecord (<>Fnd_Reg_SecretKey_t)
			End if 
		Else 
			$days_i:=<>Fnd_Reg_DemoDaysFld_ptr->
		End if 
End case 

$0:=$days_i

UNLOAD RECORD:C212(<>Fnd_Reg_Table_ptr->)
