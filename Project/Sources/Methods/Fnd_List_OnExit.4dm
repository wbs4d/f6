//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_OnExit

  // Stores the current date and time in both the structure file (as a list) and the
  //   data file (as a record).  We can compare these at startup to determine if the
  //   structure file has been updated.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Mar 20, 2004
  //   Removed the If(False) to enable this method.
  // Modified by Dave Batton on Aug 20, 2004
  //   Fixed a bug. Now the Text without length constant is passed when calling TEXT TO BLOB.
  // ----------------------------------------------------

C_TEXT:C284($date_t;$time_t;$dateTime_t;$problem_t)

Fnd_List_Init 

If (Application type:C494#4D Remote mode:K5:5)
	$problem_t:=""  // We'll stick some text in here if something goes wrong.
	
	$date_t:=String:C10(Current date:C33;ISO date:K1:8)
	$date_t:=Replace string:C233($date_t;"00:00:00";"")
	$time_t:=String:C10(Current time:C178;ISO time:K7:8)
	$time_t:=Replace string:C233($time_t;"0000-00-00T";"")
	$dateTime_t:=$date_t+$time_t
	
	  // Update the date-time stored with the structure.
	Fnd_List_XML_SetDateTime ($dateTime_t)
	
	  // Find the date-time stamp we left in the data file.
	READ WRITE:C146(<>Fnd_List_Table_ptr->)
	QUERY:C277(<>Fnd_List_Table_ptr->;<>Fnd_List_ListNameFld_ptr->=<>Fnd_List_InfoName_t)
	
	Case of 
		: (Records in selection:C76(<>Fnd_List_Table_ptr->)=1)
			If (Fnd_List_RecordLoaded (<>Fnd_List_Table_ptr))
				TEXT TO BLOB:C554($dateTime_t;<>Fnd_List_ListBlobFld_ptr->;Mac text without length:K22:10)  // DB040820
				SAVE RECORD:C53(<>Fnd_List_Table_ptr->)
			Else 
				$problem_t:="Unable to load the Foundation List Info record."
			End if 
			
		: (Records in selection:C76(<>Fnd_List_Table_ptr->)=0)
			CREATE RECORD:C68(<>Fnd_List_Table_ptr->)
			<>Fnd_List_IDFld_ptr->:=Sequence number:C244(<>Fnd_List_Table_ptr->)
			<>Fnd_List_ListNameFld_ptr->:=<>Fnd_List_InfoName_t
			TEXT TO BLOB:C554($dateTime_t;<>Fnd_List_ListBlobFld_ptr->)
			SAVE RECORD:C53(<>Fnd_List_Table_ptr->)
			
		Else 
			$problem_t:=String:C10(Records in selection:C76(<>Fnd_List_Table_ptr->))+"\"Last Exit\" records were found in the ["+Table name:C256(<>Fnd_List_Table_ptr)+"] table."
	End case 
	
	If ($problem_t#"")
		Fnd_Gen_BugAlert (Current method name:C684;$problem_t)
	End if 
	
End if   // (Application type#4D Client)
