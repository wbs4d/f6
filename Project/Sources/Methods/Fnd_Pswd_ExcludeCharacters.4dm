//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_ExcludeCharacters ({characters}) --> Text

  // Pass any characters that should be excluded from generate passwords.
  // Returns the currently specified excluded characters.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Characters to exclude (optional)

  // Returns: 
  //   $0 : Text : The current list of excluded characters

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_ExcludedCharacters_t:=$1
	
	<>Fnd_Pswd_UseExcluded_b:=(<>Fnd_Pswd_ExcludedCharacters_t#"")
End if 

If (Not:C34(<>Fnd_Pswd_UseExcluded_b))
	<>Fnd_Pswd_ExcludedCharacters_t:=""
End if 

$0:=<>Fnd_Pswd_ExcludedCharacters_t
