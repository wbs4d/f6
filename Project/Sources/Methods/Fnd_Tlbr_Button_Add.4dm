//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Add (name; buttonText; iconName; method{; keystroke})

// Adds a new button to the toolbar.

// Access Type: Shared

// Parameters: 
//   $1 : Text : A unique name for the button object
//   $2 : Text : The button label
//   $3 : Text : A Picture Library name for the icon
//   $4 : Text : The method to run when clicked
//   $5 : Text : The Command/Ctrl keyboard equivalent (optional)

// Returns: Nothing

// Created by Mark Mitchenall on 7/2/02
// Toolbar Component - © mitchenall.com 2002
// Modified by Dave Batton on May 10, 2005
//   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
// Modified by Dave Batton on Nov 18, 2005
//   Added support for menu buttons.
// ----------------------------------------------------

C_TEXT:C284($1;$buttonName_t)
C_TEXT:C284($2;$buttonLabel_t)
C_TEXT:C284($3;$iconName_t)
C_TEXT:C284($4;$method_t)
C_TEXT:C284($5;$shortcut_t)
C_LONGINT:C283($objectNumber_i)

$buttonName_t:=$1
$buttonLabel_t:=$2
$iconName_t:=$3
$method_t:=$4

If (Count parameters:C259>=5)
	$shortcut_t:=$5
Else 
	$shortcut_t:=""
End if 

Fnd_Tlbr_Init

$objectNumber_i:=Fnd_Tlbr_UsedObjects_i+1

If ($objectNumber_i<=<>Fnd_Tlbr_MaxObjects_i)
	//input the button info into the toolbar button info stack
	Fnd_Tlbr_ObjectNames_at{$objectNumber_i}:=$buttonName_t
	Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}:=$buttonLabel_t
	Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i}:=$iconName_t
	Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}:=0
	Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i}:=True:C214  // DB050510
	Fnd_Tlbr_ObjectMethods_at{$objectNumber_i}:=$method_t
	Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i}:=$shortcut_t
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectMenuItems_at{$objectNumber_i};0)  // DB051107 - Clear any menu items.
	
	Fnd_Tlbr_UsedObjects_i:=$objectNumber_i
	
	Fnd_Tlbr_ButtonsAreValid_b:=False:C215
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Maximum number of toolbar objects reached.")
End if 