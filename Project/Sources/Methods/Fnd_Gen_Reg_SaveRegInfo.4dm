//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_SaveRegInfo (user name; activation code)

  // Returns the name of the registered user.  Returns an empty string
  //   if the program hasn't been registered yet.

  // Method Type: Private

  // Parameters: 
  //   $1 : Text : User name
  //   $2 : Text : Activation code

  // Returns: Nothing

  // Created by Dave Batton on Mar 14, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$userName_t;$activationCode_t)
C_TEXT:C284($xmlRootRef_t;$productRef_t;$elementRef_t)

$userName_t:=$1
$activationCode_t:=$2

$xmlRootRef_t:=DOM Create XML Ref:C861("registration")

$productRef_t:=DOM Create XML element:C865($xmlRootRef_t;"product")

$elementRef_t:=DOM Create XML element:C865($productRef_t;"product_name")
DOM SET XML ELEMENT VALUE:C868($elementRef_t;"Foundation")

$elementRef_t:=DOM Create XML element:C865($productRef_t;"version")
DOM SET XML ELEMENT VALUE:C868($elementRef_t;"5")

$elementRef_t:=DOM Create XML element:C865($productRef_t;"user_name")
DOM SET XML ELEMENT VALUE:C868($elementRef_t;$userName_t)

$elementRef_t:=DOM Create XML element:C865($productRef_t;"activation_code")
DOM SET XML ELEMENT VALUE:C868($elementRef_t;$activationCode_t)

DOM EXPORT TO FILE:C862($xmlRootRef_t;Fnd_Gen_Reg_LicenseFilePath )

DOM CLOSE XML:C722($xmlRootRef_t)