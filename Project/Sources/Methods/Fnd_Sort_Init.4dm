//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Sort_Init

// Initializes both the process and interprocess variables used by the Fnd_Sort routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 14, 2003
// Modified by Dave Batton on Feb 4, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
// Modified by Dave Batton on May 21, 2004
//   Updated the version number to 4.0.3.
//   Added initialization for the new Fnd_Sort_Direction_i setting.
//   Removed the check for the Localization component.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Sort_Info method.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Sort_Initialized_b; Fnd_Sort_Initialized_b)
Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Sort_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Sort
	
	Fnd_Sort_Localize
	
	<>Fnd_Sort_Initialized_b:=True:C214
End if 


If (Not:C34(Fnd_Sort_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Sort
	
	Fnd_Sort_Table_ptr:=Fnd_Gen_CurrentTable
	
	ARRAY TEXT:C222(Fnd_Sort_FieldNames_at; 0)
	ARRAY POINTER:C280(Fnd_Sort_Fields_aptr; 0)
	ARRAY INTEGER:C220(Fnd_Sort_FieldTypes_ai; 0)
	ARRAY TEXT:C222(Fnd_Sort_MethodNames_at; 0)
	
	// We want to leave Fnd_Sort_SelectedField_ptr = Nil.
	
	Fnd_Sort_Direction_i:=0  // DB041108 - Now has no default. Ascending=1, Descending=-1
	
	Fnd_Sort_Initialized_b:=True:C214
End if 
