//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_MakeButton (icon name; button label; variation) --> Picture

  // Returns a picture of the button * 4 for use as a 4D picture button.
  // Assumes the button style has been previously defined.
  // This isn't called directly. Call Fnd_Bttn_GetPicture. It will check
  //   the cache first, then call this only if necessary.

  // Supports these variations:
  //   "" : A standard button
  //   "current" : The currently selected button (Mac OS X Prefs style)
  //   "menu" : Adds a menu indicator triangle

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The name of the icon to use from the 4D picture library
  //   $2 : Text : The label for the button
  //   $3 : Text : The variation name

  // Returns: 
  //   $0 : Picture : A picture to use for a 4D picture rollover button

  // Created by Mark Mitchenall on 18/9/01
  //   Original Method Name: rollover_MakeButton
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // Modified by Dave Batton on Apr 13, 2005
  //   Added the new "variation" parameter to support "Prefs" buttons.
  // Modified by Dave Batton on Nov 18, 2005
  //   Added support for the menu indicator graphic.
  // Modified: [1] John Craig, 02/07/2014, 13:45:22 Changed | picture operator to COMBINE PICTURES with Superimposition method
  // ----------------------------------------------------

C_PICTURE:C286($0;$icon_pic;$rollover_pic;$text_pic;$menu_pic)
C_TEXT:C284($1;$2;$3;$iconName_t;$buttonText_t;$variation_t)
C_LONGINT:C283($buttonWidth_i;$iconWidth_i;$iconHeight_i;$textWidth_i)
C_LONGINT:C283($contentShift_i;$iconLeftOffset_i;$textHeight_i;$textLeftOffset_i;$menuLeftOffset_i)

$iconName_t:=$1
$buttonText_t:=$2
$variation_t:=$3

$iconWidth_i:=0
$textWidth_i:=0

$iconLeftOffset_i:=Fnd_Bttn_IconLeftOffset_i  // We may temporarily change these values.
$textLeftOffset_i:=Fnd_Bttn_TextLeftOffset_i

  //Get the icon picture for this rollover if applicable
If ($iconName_t#"")
	  //Icon was specified, so create the picture containing the 4 copies of it  
	$icon_pic:=Fnd_Bttn_MakeIconPicture ($iconName_t)
	
	  //Determine the overall width of the icon, including any padding
	PICTURE PROPERTIES:C457($icon_pic;$iconWidth_i;$iconHeight_i)
	$iconWidth_i:=Fnd_Bttn_IconLeftOffset_i+$iconWidth_i+Fnd_Bttn_IconRightOffset_i
End if 

  //Get the text picture for this rollover if applicable
If ($buttonText_t#"")
	  //Text was specified, so create the picture with the 4 copies of it
	$text_pic:=Fnd_Bttn_MakeTextPicture ($buttonText_t)
	
	  //Determine the overall width and offset of the text depending on the actual width
	  //plus any padding also depending on whether there was an icon.
	PICTURE PROPERTIES:C457($text_pic;$textWidth_i;$textHeight_i)
	$textWidth_i:=Fnd_Bttn_TextLeftOffset_i+$textWidth_i+Fnd_Bttn_TextRightOffset_i
End if 


Case of 
	: ((Fnd_Bttn_LabelPlacement_i=On the right:K39:3) & ($buttonText_t=""))  // Just an icon, no text.
		If ($variation_t="menu")
			$menuLeftOffset_i:=$iconLeftOffset_i+$iconWidth_i+Fnd_Bttn_MenuIndicatorHOffset_i
			$buttonWidth_i:=$iconLeftOffset_i+$iconWidth_i+Fnd_Bttn_MenuIndicatorPadding_i
		Else 
			$buttonWidth_i:=$iconWidth_i
		End if 
		
	: ((Fnd_Bttn_LabelPlacement_i=On the right:K39:3) & ($iconName_t=""))  // Just text, no icon.
		If ($variation_t="menu")
			$menuLeftOffset_i:=$textLeftOffset_i+$textWidth_i+Fnd_Bttn_MenuIndicatorHOffset_i
			$buttonWidth_i:=$textLeftOffset_i+$textWidth_i+Fnd_Bttn_MenuIndicatorPadding_i
		Else 
			$buttonWidth_i:=$textWidth_i
		End if 
		
	: (Fnd_Bttn_LabelPlacement_i=On the right:K39:3)  // Text to the right of the button.
		$textLeftOffset_i:=$iconWidth_i
		If ($variation_t="menu")
			$menuLeftOffset_i:=$iconLeftOffset_i+$iconWidth_i+$textWidth_i+Fnd_Bttn_MenuIndicatorHOffset_i-4
			$buttonWidth_i:=$iconWidth_i+$textWidth_i+Fnd_Bttn_MenuIndicatorPadding_i
		Else 
			$buttonWidth_i:=$iconWidth_i+$textWidth_i-Fnd_Bttn_IconRightOffset_i
		End if 
		
	: (Fnd_Bttn_LabelPlacement_i=At the bottom:K39:6)  // Text below the button.
		If ($textWidth_i>$iconWidth_i)
			$buttonWidth_i:=$textWidth_i
			$iconLeftOffset_i:=Fnd_Bttn_IconLeftOffset_i+(($buttonWidth_i-$iconWidth_i)/2)
		Else 
			$buttonWidth_i:=$iconWidth_i
			$textLeftOffset_i:=Fnd_Bttn_TextLeftOffset_i+(($buttonWidth_i-$textWidth_i)/2)
		End if 
		If ($variation_t="menu")
			$menuLeftOffset_i:=$iconLeftOffset_i+$iconWidth_i+Fnd_Bttn_MenuIndicatorHOffset_i
			If ($buttonWidth_i<($iconWidth_i+Fnd_Bttn_MenuIndicatorPadding_i))
				$buttonWidth_i:=$iconWidth_i+Fnd_Bttn_MenuIndicatorPadding_i
			End if 
		End if 
		
	Else   // Label placement = None
		$buttonWidth_i:=$iconWidth_i
End case 


  // Some styles have a minimum button width. Make sure all of the buttons are at least this wide.
If ($buttonWidth_i<Fnd_Bttn_MinButtonWidth_i)
	$contentShift_i:=(Fnd_Bttn_MinButtonWidth_i-$buttonWidth_i)/2
	$iconLeftOffset_i:=$iconLeftOffset_i+$contentShift_i
	$textLeftOffset_i:=$textLeftOffset_i+$contentShift_i
	$menuLeftOffset_i:=$menuLeftOffset_i+$contentShift_i
	$buttonWidth_i:=Fnd_Bttn_MinButtonWidth_i
End if 

  // Mod by Wayne Stewart, 2016-11-17 - Don't bother with background
If (True:C214)
	  //Now that we've determined the width of the button, we can create the button
	  //background and apply the images into the correct places
	If (($variation_t="current") & (Fnd_Bttn_Style_t="Prefs"))  // "current" only works with this style.
		$rollover_pic:=Fnd_Bttn_MakeCrntBackground ($buttonWidth_i)
	Else 
		$rollover_pic:=Fnd_Bttn_MakeBackground ($buttonWidth_i)
	End if 
	
End if 
  // Modified: [1] John Craig, 02/07/2014, 13:45:22 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
  //$rollover_pic:=$rollover_pic | (($icon_pic+$iconLeftOffset_i)/Fnd_Bttn_IconTopOffset_i)
  //$rollover_pic:=$rollover_pic | (($text_pic+$textLeftOffset_i)/Fnd_Bttn_TextTopOffset_i)
COMBINE PICTURES:C987($rollover_pic;$rollover_pic;Superimposition:K61:10;$icon_pic;$iconLeftOffset_i;Fnd_Bttn_IconTopOffset_i)
COMBINE PICTURES:C987($rollover_pic;$rollover_pic;Superimposition:K61:10;$text_pic;$textLeftOffset_i;Fnd_Bttn_TextTopOffset_i)
  // Modified: [1] John Craig, 02/07/2014, 13:45:22 Changed | picture operator to COMBINE PICTURES with Superimposition method <--



If ($variation_t="menu")
	$menu_pic:=Fnd_Bttn_MakeMenuIndicatorPic 
	
	  // Modified: [1] John Craig, 02/07/2014, 13:45:22 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
	  //$rollover_pic:=$rollover_pic | (($menu_pic+$menuLeftOffset_i)/Fnd_Bttn_MenuIndicatorVOffset_i)
	COMBINE PICTURES:C987($rollover_pic;$rollover_pic;Superimposition:K61:10;$menu_pic;$menuLeftOffset_i;Fnd_Bttn_MenuIndicatorVOffset_i)
	  // Modified: [1] John Craig, 02/07/2014, 13:45:22 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
	
End if 

  //Return the picture.
$0:=$rollover_pic

