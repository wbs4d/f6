//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_ShowSubset

// Reduces the current selection to just the highlighted records.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 1, 2003
// Modified by Dave Batton on Feb 20, 2004
//   Added a call to Fnd_Grid_ShowSubset if the Grid component is available.
// Mod by Wayne Stewart, (2022-04-21) - Removed Fnd_Grid and modified Fnd_Out code for ORDA
// ----------------------------------------------------

Case of 
	: (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
		Form:C1466.listData:=Form:C1466.selectedItems.copy()
		Fnd_Out_Update
		
	: (Records in set:C195("UserSet")>0)
		USE SET:C118("UserSet")
		Fnd_Gen_SelectionChanged  // Call this any time the selection is changed.
End case 
