//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_FCS_ConfirmListExists (List Name) --> Boolean

// Returns True if the list exists in the toolbox

// Access: Shared

// Parameters:
//   $1 : Text : The list name in the Toolbox

// Returns:
//   $0 : Boolean : True - the list exists

// Created by Wayne Stewart (2019-01-08)
//     waynestewart@mac.com
// ----------------------------------------------------

C_BOOLEAN:C305($0)
C_TEXT:C284($1)

C_LONGINT:C283($Count_i)
C_TEXT:C284($ListName_t)

ARRAY LONGINT:C221($ListRefNumbers_ai; 0)
ARRAY TEXT:C222($ListNames_at; 0)

If (False:C215)
	C_BOOLEAN:C305(Fnd_FCS_ConfirmListExists; $0)
	C_TEXT:C284(Fnd_FCS_ConfirmListExists; $1)
End if 


$ListName_t:=$1

LIST OF CHOICE LISTS:C957($ListRefNumbers_ai; $ListNames_at)

$Count_i:=Count in array:C907($ListNames_at; $ListName_t)

$0:=($Count_i>0)