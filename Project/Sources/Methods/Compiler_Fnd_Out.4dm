//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Out

// Compiler declarations.
// Just for use by the compiler.

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 15, 2005
// ----------------------------------------------------

//C_BOOLEAN(<>Fnd_Out_Initialized_b)
//If (Not(<>Fnd_Out_Initialized_b))  // So we only do this once per launch.
//C_LONGINT(<>Fnd_Out_OffscreenButton_i)
//C_LONGINT(<>Fnd_Out_MacColumns_i)
//End if 




C_BOOLEAN:C305(Fnd_Out_Initialized_b)
If (Not:C34(Fnd_Out_Initialized_b))  // So we only do this once per process.
	C_LONGINT:C283(Fnd_Out_AddRecordNumber_i; Fnd_Out_RecIDsHdr_i; Fnd_Out_ColumnPic1Hdr_i)
	C_LONGINT:C283(Fnd_Out_TextColumns_i; Fnd_Out_NumColumns_i; Fnd_Out_DateColumns_i; Fnd_Out_PictureColumns_i)
	C_LONGINT:C283(Fnd_Out_LastRecordCount_i)
	C_TEXT:C284(Fnd_Out_IconGroupName_t; Fnd_Mail_RecordsInTable_t)
	C_POINTER:C301(Fnd_Out_RecordIDField_ptr; Fnd_Out_CurrentTable_ptr)
	C_BOOLEAN:C305(Fnd_Out_LoadArrays_b)
	
	ARRAY POINTER:C280(Fnd_Out_ListColumnFields_aptr; 0)
	ARRAY POINTER:C280(Fnd_Out_ListColumnArrays_aptr; 0)
	ARRAY INTEGER:C220(Fnd_Out_ListColumnWidths_ai; 0)
	ARRAY INTEGER:C220(Fnd_Out_ListColumnAlignments_ai; 0)
	ARRAY TEXT:C222(Fnd_Out_ListColumnFormats_at; 0)
	ARRAY TEXT:C222(Fnd_Out_ListColumnLabels_at; 0)
	
	// No longer need arrays
	ARRAY BOOLEAN:C223(Fnd_Out_Listbox_ab; 0)
	
	ARRAY LONGINT:C221(Fnd_Out_RecIDs_ai; 0)
	
	ARRAY TEXT:C222(Fnd_Out_Column1_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column2_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column3_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column4_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column5_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column6_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column7_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column8_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column9_at; 0)
	ARRAY TEXT:C222(Fnd_Out_Column10_at; 0)
	
	ARRAY REAL:C219(Fnd_Out_Column1_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column2_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column3_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column4_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column5_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column6_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column7_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column8_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column9_ar; 0)
	ARRAY REAL:C219(Fnd_Out_Column10_ar; 0)
	
	ARRAY DATE:C224(Fnd_Out_Column1_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column2_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column3_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column4_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column5_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column6_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column7_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column8_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column9_ad; 0)
	ARRAY DATE:C224(Fnd_Out_Column10_ad; 0)
	
	ARRAY BOOLEAN:C223(Fnd_Out_Column1_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column2_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column3_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column4_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column5_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column6_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column7_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column8_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column9_ab; 0)
	ARRAY BOOLEAN:C223(Fnd_Out_Column10_ab; 0)
	
	ARRAY PICTURE:C279(Fnd_Out_Column1_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column2_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column3_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column4_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column5_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column6_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column7_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column8_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column9_apic; 0)
	ARRAY PICTURE:C279(Fnd_Out_Column10_apic; 0)
	
	
End if 



*/

If (False:C215)
	C_BOOLEAN:C305(Fnd_Out_Active; $0; $1)
	
	C_VARIANT:C1683(Fnd_Out_AddField; $1)
	C_LONGINT:C283(Fnd_Out_AddField; $2; $4)
	C_TEXT:C284(Fnd_Out_AddField; $3; $5)
	C_BOOLEAN:C305(Fnd_Out_AddField; $6)
	
	C_LONGINT:C283(Fnd_Out_AddToSelection; $1)
	
	C_TEXT:C284(Fnd_Out_ConfigureList; $1)
	C_LONGINT:C283(Fnd_Out_ConfigureList; $2; $3)
	
	C_TEXT:C284(Fnd_Out_Info; $0; $1)
	
	C_LONGINT:C283(Fnd_Out_getPrimaryKey; $1; $0)
	
	C_VARIANT:C1683(Fnd_Out_fieldType; $1)
	C_LONGINT:C283(Fnd_Out_fieldType; $0)
	
	C_POINTER:C301(Fnd_Out_ORDARelatedField; $0)
	C_OBJECT:C1216(Fnd_Out_ORDARelatedField; $1)
	
	C_LONGINT:C283(Fnd_Out_FormMethod; $1)
	
	C_TEXT:C284(Fnd_Out_fieldName; $1; $0)
	
End if 
