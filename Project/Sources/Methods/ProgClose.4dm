//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: ProgClose


  // Allows an older Foundation based database to use the new Fnd_Dlg routines.

  // Part of the Foundation Compatibility component.


  // Parameters: None


  // Returns: Nothing


  // Created by Dave Batton on Jul 27, 2003

  // ----------------------------------------------------


Fnd_Cmpt_Init 

Fnd_Dlg_MessageClose 