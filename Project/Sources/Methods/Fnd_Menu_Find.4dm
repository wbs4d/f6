//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Find

// Called when the user selects Find from the Select menu.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Shell component is no longer required.
// ----------------------------------------------------


If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Find")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_Find";*)
Else 
	Fnd_Find_Display
End if 
