//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_CreateButton (->button object; ->picture variable; icon name; label)

// Helps the developer create and size a picture button created with the
//   Foundation Buttons component.  
// Creates the image needed from the icon name and label, assigns it to the picture
//   variable, then assigns the picture variable to the button.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : The form button object
//   $2 : Pointer : A picture variable
//   $3 : Text : An icon name
//   $4 : Text : Any text

// Returns: Nothing

// Created by Dave Batton on Feb 20, 2005
// Modified by Gary Boudreaux on Dec 21, 2008
//   Corrected parameter list in header
// ----------------------------------------------------

C_POINTER:C301($1;$2;$button_ptr;$picture_ptr)
C_TEXT:C284($3;$4;$iconName_t;$label_t;$pictureVarName_t)
C_LONGINT:C283($left_i;$top_i;$right_i;$bottom_i;$width_i;$height_i;$tableNumber_i;$fieldNumber_i)

$button_ptr:=$1
$picture_ptr:=$2
$iconName_t:=$3
$label_t:=$4

// Fnd_Bttn_GetPicture will call Fnd_Bttn_Init for us.

// Assign an image to the picture variable.
$picture_ptr->:=Fnd_Bttn_GetPicture($iconName_t;$label_t)

// Resize the button to fit the picture.
PICTURE PROPERTIES:C457($picture_ptr->;$width_i;$height_i)
OBJECT GET COORDINATES:C663($button_ptr->;$left_i;$top_i;$right_i;$bottom_i)
$right_i:=$left_i+$width_i
$bottom_i:=$top_i+($height_i/4)
OBJECT MOVE:C664($button_ptr->;$left_i;$top_i;$right_i;$bottom_i;*)

// Set the object properties to behave properly.
RESOLVE POINTER:C394($picture_ptr;$pictureVarName_t;$tableNumber_i;$fieldNumber_i)

OBJECT SET FORMAT:C236($button_ptr->;"1;4;"+$pictureVarName_t+";176;0")  // cols;lines;picture;flags;ticks