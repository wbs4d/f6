//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_FixButtonTexts

  // Convert "*" in the button texts to local strings.
  // Do this just before displaying the dialog, since they get cleared after that.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 26, 2003
  // ----------------------------------------------------

If ((Fnd_Dlg_Button1_t="") | (Fnd_Dlg_Button1_t="*"))
	If ((Fnd_Dlg_Button1_t="*") & (Fnd_Dlg_Button3_t="*"))  // If this is Don't Save, make OK into Save.
		Fnd_Dlg_Button1_t:=Fnd_Gen_GetString ("Fnd_Gen";"Save")
	Else 
		Fnd_Dlg_Button1_t:=Fnd_Gen_GetString ("Fnd_Gen";"OK")
	End if 
End if 

If (Fnd_Dlg_Button2_t="*")
	Fnd_Dlg_Button2_t:=Fnd_Gen_GetString ("Fnd_Gen";"Cancel")
End if 

If (Fnd_Dlg_Button3_t="*")
	Fnd_Dlg_Button3_t:=Fnd_Gen_GetString ("Fnd_Gen";"DontSave")
End if 
