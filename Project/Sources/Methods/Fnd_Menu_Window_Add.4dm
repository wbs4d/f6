//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Window_Add ({item text})

// Adds the current process to the Window menu.  If no title
//   is specified, the current window title is used.

// Access: Shared

// Parameters: 
//   $1 : Text : The item text to add to the menu (optional)

// Returns: Nothing

// Created by Dave Batton on Sep 15, 2003
// Modified by Dave Batton on May 23, 2004
//   Moved the Fnd_Menu_MenuBar and now pass the optional * parameter.
// Modified by Dave Batton on Jan 30, 2005
//   Now calls the Fnd_Wnd_SendCloseRequests if the Fnd_Wnd component is available.
// Modified by Dave Batton on Mar 16, 2006
//   Changed the timeout value to use the new variable rather than being hard-coded.
// ----------------------------------------------------

C_TEXT:C284($1; $itemText_i)
C_LONGINT:C283($element_i)

If (Count parameters:C259>=1)
	$itemText_i:=$1
Else 
	$itemText_i:=Get window title:C450
End if 

Fnd_Menu_Init

If (Not:C34(Semaphore:C143(<>Fnd_Menu_Semaphore_t; <>Fnd_Menu_SemaphoreTimeout_i)))  // DB060316
	$element_i:=Find in array:C230(<>Fnd_Menu_Window_Processes_ai; Current process:C322)
	
	If ($element_i=-1)
		$element_i:=Size of array:C274(<>Fnd_Menu_Window_Processes_ai)+1
		INSERT IN ARRAY:C227(<>Fnd_Menu_Window_Processes_ai; $element_i)
		INSERT IN ARRAY:C227(<>Fnd_Menu_Window_Items_at; $element_i)
	End if 
	
	<>Fnd_Menu_Window_Processes_ai{$element_i}:=Current process:C322
	<>Fnd_Menu_Window_Items_at{$element_i}:=$itemText_i
	
	//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
	//EXECUTE METHOD("Fnd_Wnd_SendCloseRequests"; *)
	//End if 
	Fnd_Wnd_SendCloseRequests
	
	CLEAR SEMAPHORE:C144(<>Fnd_Menu_Semaphore_t)
End if 

Fnd_Menu_MenuBar("*")
