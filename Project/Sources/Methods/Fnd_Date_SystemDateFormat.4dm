//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_SystemDateFormat --> Text

  // Returns a string that can be used in If and Case statements to test the
  //   current system's date format. It will return Y, M, and D based in a format
  //   like this:
  //      "M/D/Y" = Month/Day/Year
  //      "D/M/Y" = Day/Month/Year

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Text : The date format

  // Created by Dave Batton on May 2, 2004
  // ----------------------------------------------------

C_TEXT:C284($0)

  // There's really no reason to call Fnd_Date_Init here.

$0:=Replace string:C233(Replace string:C233(Replace string:C233(String:C10(!1776-10-20!;Internal date short special:K1:4);"10";"M");"20";"D");"1776";"Y")

