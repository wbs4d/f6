//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatPhone (phone number{; style}) --> Text

  // Formats the phone number in the format 000-000-0000, unless another
  //   style has been specified in the second parameter. 
  // The number is not formatted if the user holds down the Option/Alt
  //   key (or other key that's been specified by the developer).
  // If it can't figure out how to format it then it returns the phone number
  //   unchanged and sets the Fnd_Data_FormatError_i variable to 1.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The phone number to format
  //   $2 : Longint : The number style (optional, but not yet implemented)

  // Returns: 
  //   $0 : Text : The formatted phone number

  // Created by Dave Batton on May 11, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Added note about $2 parameter in header
  // ----------------------------------------------------

  // ### - The style parameter is not yet implemented. Just does US phone numbers.

  // Code from Wayne Stewart for Australian phone numbers.

  //  `  Formats the passed as (00) 0000 0000.
  //  `  A mobile phone is formatted (0400) 000 000
  //Case of 
  //: (($thePhoneNum="") | (Macintosh option down))  ` Don't format international phone numbers.
  //  ` Don't modify it.
  //: (STR_FormatNumber($thePhoneNum;"0000000000")="04@")
  //$thePhoneNum:=STR_FormatNumber($thePhoneNum;"(0000) 000 000")
  //
  //: (Length(STR_FormatNumber($thePhoneNum;""))=9)  `  The leading 0 was 
  //removed
  //$thePhoneNum:=STR_FormatNumber($thePhoneNum;"(00) 0000 0000")
  //
  //: (Length(STR_FormatNumber($thePhoneNum;""))=8)
  //$thePhoneNum:=◊DefaultAreaCode+$thePhoneNum
  //$thePhoneNum:=STR_FormatNumber($thePhoneNum;"(00) 0000 0000")
  //End case 


C_TEXT:C284($1;$0;$phoneNumber_t)
C_LONGINT:C283($2)

$phoneNumber_t:=$1

Fnd_Data_Init 
Fnd_Data_FormatError_i:=0

Case of 
	: (($phoneNumber_t="") | (Fnd_Data_FormatBypass ))
		  // Don't modify it.  
		
	: (Length:C16(Fnd_Text_FormatNumber ($phoneNumber_t;""))=10)
		$phoneNumber_t:=Fnd_Text_FormatNumber ($phoneNumber_t;"000-000-0000")
		
	: (Length:C16(Fnd_Text_FormatNumber ($phoneNumber_t;""))=7)
		$phoneNumber_t:=Fnd_Text_FormatNumber ($phoneNumber_t;"000-0000")
		
	Else 
		Fnd_Data_FormatError_i:=1
End case 

$0:=$phoneNumber_t