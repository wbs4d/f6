//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Enabled (buttonName; state)

//Set the enabled state of the toolbar item.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The button name to modify
//   $2 : Boolean : True to enable the button

// Returns: Nothing

// Created by Mark Mitchenall on 11/3/02
// Toolbar Component - © mitchenall.com 2002
// Modified by Dave Batton on May 10, 2005
//   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_TEXT:C284($1;$objectName_t)
C_BOOLEAN:C305($2;$enabled_b)
C_LONGINT:C283($objectNumber_i)

$objectName_t:=$1
$enabled_b:=$2

Fnd_Tlbr_Init

$objectNumber_i:=Fnd_Tlbr_Object_GetNumber($objectName_t)

If ($objectNumber_i>0)  // Don't worry if we don't find it. There are reasons it may not exist.
	
	If ($enabled_b)
		Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i}:=True:C214
		//ENABLE BUTTON(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
		OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";True:C214)
	Else 
		Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i}:=False:C215
		//DISABLE BUTTON(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
		OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";False:C215)
	End if 
	
End if 