//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_PrefsGet (pref name) --> Text

// Gets a value from the user preferences if the Fnd_Pref component is available.

// Access Type: Private

// Parameters: 
//   $1 : Text : The preference name

// Returns: 
//   $0 : Text : The saved value

// Created by Dave Batton on Nov 6, 2004
// ----------------------------------------------------

C_TEXT:C284($0; $1)

$0:=Fnd_Pref_GetText($1; "")

