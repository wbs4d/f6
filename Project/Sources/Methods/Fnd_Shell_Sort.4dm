//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_Sort

// Called from the Sort menu bar item or toolbar item to display the Sort dialog.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on 21 Jul 2008
// ----------------------------------------------------

Case of 
	: (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Sort")=1)
		EXECUTE METHOD:C1007("Fnd_Hook_Shell_Sort"; *)
		
		//: (Fnd_Gen_ComponentAvailable("Fnd_Sort"))
		//EXECUTE METHOD("Fnd_Sort_Display"; *)
		
		//Else 
		//ORDER BY(Fnd_Gen_CurrentTable->)
	Else 
		
		Fnd_Sort_Display
		
End case 
