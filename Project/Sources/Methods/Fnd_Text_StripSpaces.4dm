//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_StripSpaces (text{; options}) --> Text

  // Strips spaces from the text that is passed to it.
  // If no second parameter is passed, leading and trailing spaces
  //    are stripped from the text.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text from which to strip spaces
  //   $2 : Longint : Options (optional)
  //      1 = Remove leading spaces
  //      2 = Remove trailing spaces 
  //      4 = Remove double spaces within the text 

  // Returns: 
  //   $0 : Text : The text with spaces stripped

  // Created by Dave Batton on Apr 26, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Corrected spelling in description of return value in header
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$theText_t)
C_LONGINT:C283($2;$options_i)

$theText_t:=$1
If (Count parameters:C259>=2)
	$options_i:=$2
Else 
	$options_i:=1+2  // Just strip leading and trailing spaces.
End if 

Fnd_Text_Init 

If ($theText_t#"")
	
	If (($options_i & 1)#0)  // Get rid of leading spaces.
		While (Substring:C12($theText_t;1;1)=" ")
			$theText_t:=Substring:C12($theText_t;2)
		End while 
	End if 
	
	If (($options_i & 2)#0)  // Get rid of trailing spaces.
		While (Substring:C12($theText_t;Length:C16($theText_t);1)=" ")
			$theText_t:=Substring:C12($theText_t;1;Length:C16($theText_t)-1)
		End while 
	End if 
	
	If (($options_i & 4)#0)  // Get rid of double spaces in the text.
		While (Position:C15("  ";$theText_t)>0)
			$theText_t:=Replace string:C233($theText_t;"  ";" ")
		End while 
	End if 
	
End if   // The string is not blank.

$0:=$theText_t