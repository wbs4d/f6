//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_This (Log Entry; Log Folder Path; Log File)

// Instructs the log writer worker to write the log entry

// Access: Private

// Parameters: 
//   $1 : Text : The log entry
//   $2 : Text : The path to the log folder
//   $3 : Text : The path to the log file

// ? I'm confused as why I have both paths? //  NEED TO LOOK AT

// Created by Wayne Stewart (2021-04-15)

//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($logEntry_t; $1)
C_TEXT:C284($PathToLogFolder_t; $2)
C_TEXT:C284($PathToLogFile_t; $3)

C_TIME:C306($docRef_h)
C_LONGINT:C283($Position_i; $dot_i)
C_TEXT:C284($ErrorHandler_t; $TimeStamp_t; $LogFileName_t; $extension_t; $archivedfileName_t; $archivedfilePath_t)

$logEntry_t:=$1
$PathToLogFolder_t:=$2
$PathToLogFile_t:=$3

Fnd_Log_Init


If (False:C215)
	C_TEXT:C284(Fnd_Log_This; $1; $2; $3)
End if 

$ErrorHandler_t:=Method called on error:C704

ON ERR CALL:C155("Fnd_Log_SilentError")
$PathToLogFile_t:=$PathToLogFolder_t+$PathToLogFile_t

$Position_i:=Find in array:C230(Fnd_Log_Paths_at; $PathToLogFile_t)
If ($Position_i>0)
	$docRef_h:=Fnd_Log_DocRefs_ah{$Position_i}
	
Else 
	
	If (Test path name:C476($PathToLogFile_t)=Is a document:K24:1)
		$docRef_h:=Append document:C265($PathToLogFile_t)
	Else 
		If (Test path name:C476($PathToLogFolder_t)#Is a folder:K24:2)
			CREATE FOLDER:C475($PathToLogFolder_t; *)
		End if 
		$docRef_h:=Create document:C266($PathToLogFile_t; "TEXT")
		
	End if 
	
	APPEND TO ARRAY:C911(Fnd_Log_Paths_at; $PathToLogFile_t)
	APPEND TO ARRAY:C911(Fnd_Log_DocRefs_ah; $docRef_h)
	
End if 

If (OK=1)
	SEND PACKET:C103($docRef_h; $logEntry_t)
	If (Storage:C1525.log.maxLogSize#0)\
		 & (Get document size:C479($PathToLogFile_t)>Storage:C1525.log.maxLogSize)
		
		CLOSE DOCUMENT:C267($docRef_h)
		
		$dot_i:=Fnd_Gen_PositionR("."; $LogFileName_t)
		If ($dot_i=0)  // There is no file extension
			$extension_t:=""
			$archivedfileName_t:=$LogFileName_t
		Else 
			$extension_t:="."+Substring:C12($LogFileName_t; $dot_i+1)
			$archivedfileName_t:=Substring:C12($LogFileName_t; 1; $dot_i-1)
		End if 
		
		$TimeStamp_t:=Replace string:C233(Fnd_Date_DateAndTimeToISO(Current date:C33; Current time:C178); ":"; "")
		$TimeStamp_t:=Substring:C12($TimeStamp_t; 1; 19)
		$TimeStamp_t:=" ("+$TimeStamp_t+")"
		
		$archivedfileName_t:=$archivedfileName_t+$TimeStamp_t+$extension_t
		$archivedfilePath_t:=$PathToLogFolder_t+$archivedfileName_t
		
		MOVE DOCUMENT:C540($PathToLogFile_t; $archivedfilePath_t)  // Rename the current file
		
		$docRef_h:=Create document:C266($PathToLogFile_t; "TEXT")  // Create a new document
		Fnd_Log_DocRefs_ah{$Position_i}:=$docRef_h  // Update the array
		Fnd_File_Compress($archivedfilePath_t)  //  Now compress the old file
		
	End if 
	
End if 


ON ERR CALL:C155($ErrorHandler_t)