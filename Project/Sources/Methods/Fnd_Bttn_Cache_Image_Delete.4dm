//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_Cache_Image_Delete (composite image name)

  //Deletes an image from the cache using its composite name as the key.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The image name returned by Fnd_Bttn_GetPictureName

  // Returns: Nothing

  // Created by Mark Mitchenall on 17/9/01
  //   Original Method Name: rollover_Cache_Image_Delete
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // ----------------------------------------------------

C_TEXT:C284($1;$compositeName_t)
C_LONGINT:C283($index_i)

$compositeName_t:=$1

$index_i:=Find in array:C230(<>Fnd_Bttn_CacheNames_at;$compositeName_t)

If ($index_i>0)
	
	  //Don't delete the image if another process is accessing the cache.
	If (Not:C34(Semaphore:C143("$Fnd_Bttn_Cache";300)))  // Wait up to 5 seconds.
		
		  //adjust the cache size  
		<>Fnd_Bttn_CacheTotalSize_i:=<>Fnd_Bttn_CacheTotalSize_i-<>Fnd_Bttn_CachePictSizes_ai{$index_i}
		
		  //delete the item's elements from the cache arrays
		DELETE FROM ARRAY:C228(<>Fnd_Bttn_CacheNames_at;$index_i;1)
		DELETE FROM ARRAY:C228(<>Fnd_Bttn_CachePictures_apic;$index_i;1)
		DELETE FROM ARRAY:C228(<>Fnd_Bttn_PictureWidths_ai;$index_i;1)
		DELETE FROM ARRAY:C228(<>Fnd_Bttn_CachePictSizes_ai;$index_i;1)
		DELETE FROM ARRAY:C228(<>Fnd_Bttn_Tickcounts_ai;$index_i;1)
		
		CLEAR SEMAPHORE:C144("$Fnd_Bttn_Cache")
	End if 
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"Attempted to delete an image not in cache.")
End if 