//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: PrefsPut (pref name; pref value)

  // Allows an older Foundation based database to use the new Fnd_Pref routines.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The preference name
  //   $2 : Text : The value to set

  // Returns: None

  // Created by Dave Batton on Feb 22, 2004
  // ----------------------------------------------------

C_TEXT:C284($1;$2)

Fnd_Cmpt_Init 

Fnd_Pref_SetText ($1;$2)