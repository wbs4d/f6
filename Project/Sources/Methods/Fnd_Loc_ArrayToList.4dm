//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_ArrayToList($array_ptr;$listName_t)

// Updates list in the host database.

// Access: Shared

// Parameters: 
//   $1 : Pointer : The text array
//   $2 : Text : The list name


// Returns: Nothing
// Created by : Vincent Tournier
// Date created : 06/17/11, 15:36:24
// ----------------------------------------------------

C_POINTER:C301($1;$array_ptr)
C_TEXT:C284($2;$listName_t)

$array_ptr:=$1
$listName_t:=$2


EXECUTE METHOD:C1007("Fnd_Host_ArrayToList";*;$listName_t;$array_ptr)



_DeleteMe:=True:C214

