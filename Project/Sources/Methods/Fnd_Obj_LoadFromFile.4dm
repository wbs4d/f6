//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Obj_LoadFromFile (Path to .json file) --> Object

// Description

// Access: Shared

// Parameters: 
//   $1 : Text : Path to .json file (optional)

// Returns: 
//   $0 : Object : The loaded object

// Created by Wayne Stewart (2020-09-21)
//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($1)
C_OBJECT:C1216($0)

C_TEXT:C284($JSON_t; $Filepath_t; $CurrentOnErrMethod_t)
C_OBJECT:C1216($Object_o)

$Filepath_t:=$1

If (Test path name:C476($Filepath_t)=Is a document:K24:1)
	$JSON_t:=Document to text:C1236($Filepath_t)
	$CurrentOnErrMethod_t:=Method called on error:C704
	ON ERR CALL:C155("Fnd_Gen_SilentError")
	$Object_o:=JSON Parse:C1218($JSON_t)
	ON ERR CALL:C155("$CurrentOnErrMethod_t")
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "The path does not refer to a document file")
End if 

$0:=$Object_o
