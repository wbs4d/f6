//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_Delete ({label})

// Deletes the button with the specified label from the navigation palette.
// If no parameter is passed, all buttons are deleted.

// Access: Shared

// Parameters:
//   $1 : Text : The button label (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 1, 2004
// ----------------------------------------------------

C_TEXT:C284($1; $label_t)
C_LONGINT:C283($element_i; $elements_i)

Fnd_Nav_Init

If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 300)))  // Wait up to 5 seconds.
	If (Count parameters:C259=0)
		$elements_i:=Size of array:C274(<>Fnd_Nav_Labels_at)
		If ($elements_i>0)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_Labels_at; 1; $elements_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_TablePtrs_aptr; 1; $elements_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_Methods_at; 1; $elements_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_ForegroundColors_ai; 1; $elements_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_BackgroundColors_ai; 1; $elements_i)
		End if 
		
	Else 
		$label_t:=$1
		$element_i:=Find in array:C230(<>Fnd_Nav_Labels_at; $label_t)
		
		If ($element_i>0)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_Labels_at; $element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_TablePtrs_aptr; $element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_Methods_at; $element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_ForegroundColors_ai; $element_i)
			DELETE FROM ARRAY:C228(<>Fnd_Nav_BackgroundColors_ai; $element_i)
		End if 
	End if 
	
	<>Fnd_Nav_NeedsRedraw_b:=True:C214
	CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
	POST OUTSIDE CALL:C329(Process number:C372(Storage:C1525.Fnd.Nav.ProcessName))
End if 