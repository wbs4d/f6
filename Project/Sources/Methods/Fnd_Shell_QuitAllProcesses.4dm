//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_QuitAllProcesses --> Boolean

// Attempts to end all processes (except for 4D's) by bugging them until they end.
// Assumes they call the Fnd_Gen_QuitNow method to see if they're supposed to end.
// Doesn't try to end the current process.
// This method will give up (and return False) if any process cancels the Quit attempt
//   or if the timeout value is reached.

// Access: Private

// Parameters: None

// Returns: 
//   $0 : Boolean : True if all process ended as expected

// Created by Dave Batton on Sep 12, 2003
// Modified by Dave Batton on Mar 25, 2004
//   Localized the alert message.
// ----------------------------------------------------

C_BOOLEAN:C305($0; $completed_b; $done_b; $timeoutReached_b; $visible_b)
C_LONGINT:C283($processNumber_i; $processState_i; $processTime_i; $timeout_i)
C_LONGINT:C283($processID_i; $processOrigin_i)
C_TEXT:C284($currentProcessName_t; $processName_t; $problemProcess_t)

If (Fnd_Gen_QuitNow)
	$completed_b:=True:C214  // Assume success.
	
	$timeout_i:=Tickcount:C458+(30*60)  // 30 second timout.
	
	PROCESS PROPERTIES:C336(Current process:C322; $currentProcessName_t; $processState_i; $processTime_i)
	
	Repeat 
		$done_b:=True:C214
		
		For ($processNumber_i; 1; Count tasks:C335)
			PROCESS PROPERTIES:C336($processNumber_i; $processName_t; $processState_i; $processTime_i; $visible_b; $processID_i; $processOrigin_i)
			
			If ($processState_i>=0)  // If the process still exists...
				If ($processOrigin_i>=0)  // If it's a process created by the developer's code, not by 4D...
					If ($processName_t#$currentProcessName_t)  // If it's not the current process...
						If (Find in array:C230(<>Fnd_Shell_NoQuitProcessNames_at; $processName_t)=-1)  // If we haven't excluded it by calling Fnd_Shell_ExcludeFromQuit.
							RESUME PROCESS:C320($processNumber_i)  // In case it's sleeping.
							POST OUTSIDE CALL:C329($processNumber_i)  // Ask it to call Fnd_Gen_QuitNow.
							$done_b:=False:C215  // Don't leave this loop yet.
							$problemProcess_t:=$processName_t  // In case something won't quit, we can tell on it.
						End if 
					End if 
				End if 
			End if 
		End for 
		$timeoutReached_b:=(Tickcount:C458>=$timeout_i)  // So we can check this twice with equal results.
	Until (($done_b) | (Not:C34(Fnd_Gen_QuitNow)) | ($timeoutReached_b))
	
	If ($timeoutReached_b)
		ALERT:C41(Fnd_Loc_GetString("Fnd_Gen"; "QuitTimeoutMessage"))  // DB040325 - Localized.
		$completed_b:=False:C215
		Fnd_Gen_CancelQuit
	End if 
	
Else 
	$completed_b:=False:C215  // Since Fnd_Gen_QuitNow returned False.
End if 

$0:=$completed_b