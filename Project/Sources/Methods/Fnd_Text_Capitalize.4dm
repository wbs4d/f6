//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_Capitalize (text{; style}) --> Text

  // Modifies the capitalization of the text value. The options paramter
  //   determines how the text will be modified. If no second parameter
  //   is passed, Book Title capitalization is applied.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text to format
  //   $2 : Longint : Style (optional)
  //      1 = Use Sentence Style capitalization (not yet implemented)
  //      2 = Don't use exclude word list

  // Returns: 
  //   $0 : Text : The formatted text

  // Created by Dave Batton on Apr 26, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$theText_t)
C_LONGINT:C283($2;$options_i;$character_i;$options_i;$character_i)
C_TEXT:C284($characters_t)

$theText_t:=$1
If (Count parameters:C259>=2)
	$options_i:=$2
Else 
	$options_i:=0
End if 

Fnd_Text_Init 

If ($theText_t#"")
	$characters_t:=".,;:/? -_+#"+Char:C90(Double quote:K15:41)  // Characters to uppercase next letter after.
	
	  // Uppercase the first letter of each word.
	$theText_t[[1]]:=Uppercase:C13($theText_t[[1]])
	For ($character_i;1;Length:C16($theText_t)-1)
		If (Position:C15($theText_t[[$character_i]];$characters_t)>0)
			$theText_t[[$character_i+1]]:=Uppercase:C13($theText_t[[$character_i+1]])
		End if 
	End for 
	
	If (Not:C34($options_i ?? 2))
		For ($character_i;1;Size of array:C274(<>Fnd_Text_CapExcludeWords_at))
			$theText_t:=Replace string:C233($theText_t;<>Fnd_Text_CapExcludeWords_at{$character_i};<>Fnd_Text_CapExcludeWords_at{$character_i})
		End for 
	End if 
End if 

$0:=$theText_t