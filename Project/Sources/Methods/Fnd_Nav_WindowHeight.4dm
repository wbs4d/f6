//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Nav_WindowHeight --> Number

  // Returns the height to use for the navigation palette.

  // Access: Private

  // Parameters: Nothing

  // Returns: 
  //   $0 : Longint : The height to use

  // Created by Dave Batton on Jul 1, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$windowHeight_i;$buttonHeight_i;$numButtons_i)

$buttonHeight_i:=20

$numButtons_i:=Size of array:C274(<>Fnd_Nav_Labels_at)

If ($numButtons_i<=<>Fnd_Nav_MaxButtons_i)
	$windowHeight_i:=($numButtons_i*$buttonHeight_i)
Else 
	$windowHeight_i:=(<>Fnd_Nav_MaxButtons_i*$buttonHeight_i)
End if 

$0:=$windowHeight_i