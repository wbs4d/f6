//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_fieldType (Formula or field pointer) --> Long

// Pass either the field pointer or a formula to determine
// the field type to include in the display listbox
// Fnd_Out_fieldType(->[Client]ID) --> 9 (Is longint)
// Fnd_Out_fieldType("invoice.client.FirstName") --> 0 (Is alpha field)

// Access: Shared

// Parameters: 
//   $1 : Variant : A field pointer or a valid formula

// Returns: 
//   $0 : Long : Field Type (-1 indicates error)

// Created by Wayne Stewart (2022-04-20)
// ----------------------------------------------------

Fnd_Out_Init

If (False:C215)
	C_VARIANT:C1683(Fnd_Out_fieldType;$1)
	C_LONGINT:C283(Fnd_Out_fieldType;$0)
End if 

var $1 : Variant
var $0;$fieldType_i;$currItem_i;$numberOfItems_i : Integer
var $theAttribute_t;$theDataClass_t;$theFormula_t : Text
var $relationChain_c : Collection

$fieldType_i:=-1  // Assume an error

Case of 
	: (Value type:C1509($1)=Is pointer:K8:14)  // This is easiest, although probably not very useful
		$fieldType_i:=Type:C295($1->)
		
	: (Value type:C1509($1)=Is text:K8:3)  //
		$theFormula_t:=$1
		
		// This should have been set outside the method
		If (Fnd.out.currentTable=0)
			Fnd.out.currentTable:=Table:C252(Fnd_Gen_CurrentTable)
		End if 
		$theDataClass_t:=Table name:C256(Fnd.out.currentTable)
		
		// Break down the formula
		$relationChain_c:=Split string:C1554($theFormula_t;".";sk ignore empty strings:K86:1)
		
		$numberOfItems_i:=$relationChain_c.length
		
		Case of 
			: ($numberOfItems_i=0)  // They must have passed an empty string
				Fnd_Gen_BugAlert("Fnd_Out_fieldType";"A listbox formula was not passed.")
				
			: ($numberOfItems_i=1)  // They passed a field from this table
				$fieldType_i:=ds:C1482[$theDataClass_t][$relationChain_c[0]].fieldType
				
			: ($numberOfItems_i>1)  // There is a chain of at least one relation specified
				$numberOfItems_i:=$numberOfItems_i-2
				$theDataClass_t:=ds:C1482[$theDataClass_t][$relationChain_c[0]].relatedDataClass
				For ($currItem_i;1;$numberOfItems_i)
					$theDataClass_t:=ds:C1482[$theDataClass_t][$relationChain_c[$currItem_i]].relatedDataClass
				End for 
				$theAttribute_t:=$relationChain_c[$numberOfItems_i+1]
				$fieldType_i:=ds:C1482[$theDataClass_t][$theAttribute_t].fieldType
				
		End case 
		
End case 

$0:=$fieldType_i