//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Wnd_Title ({title}) --> Text


// Gets and sets the window title for the upcoming window.

// This method replaces the original Fnd_Wnd_Title method, which is now obsolete.


// Access Type: Shared


// Parameters: 

//   $1 : Text : Window title (optional)


// Returns: 

//   $0 : Text : Next window title


// Created by Dave Batton on Apr 23, 2005

// ----------------------------------------------------


C_TEXT:C284($0;$1)

Fnd_Wnd_Init

If (Count parameters:C259>=1)
	Fnd_Wnd_WindowTitle_t:=$1
End if 

$0:=Fnd_Wnd_WindowTitle_t