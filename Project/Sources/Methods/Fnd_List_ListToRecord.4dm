//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_ListToRecord (list name)

  // Saves the list as a 4D list and as a record.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The name of the list to save

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Jun 16, 2004
  //   Localized the error message.
  // Modified by Walt Nelson (2/19/10) fixed when component is compiled and host is intrepreted
  // ----------------------------------------------------

C_TEXT:C284($1;$listName_t;$formula_t)
C_LONGINT:C283($listRef_i)
C_BLOB:C604($list_blob)
C_BOOLEAN:C305($wasReadOnly_b)

$listName_t:=$1

$formula_t:="$2->:="+Command name:C538(383)+"(\""+$listName_t+"\")"  // 383 = Load list

  // Modified by: Walt Nelson (2/19/10) - when component is compiled and host is intrepreted ->$listRef_i is gibberish
  // listRef_i is defined in Compiler_Fnd_List
EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t;->listRef_i)
$listRef_i:=listRef_i

If (Is a list:C621($listRef_i))
	LIST TO BLOB:C556($listRef_i;$list_blob)
	If (OK#1)
		Fnd_Gen_BugAlert (Current method name:C684;"Unable to convert the (\""+$listName_t+"\") list to a blob.")
	End if 
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"An invalid list name (\""+$listName_t+"\") was specified.")
End if 

$wasReadOnly_b:=Read only state:C362(<>Fnd_List_Table_ptr->)
READ WRITE:C146(<>Fnd_List_Table_ptr->)

  // Find any existing records with this list name.
QUERY:C277(<>Fnd_List_Table_ptr->;<>Fnd_List_ListNameFld_ptr->=$listName_t)

Case of 
	: (Records in selection:C76(<>Fnd_List_Table_ptr->)=0)  // There's no record to modify, so create a new one.
		CREATE RECORD:C68(<>Fnd_List_Table_ptr->)  // The trigger will assign a value to the ID field.
		<>Fnd_List_IDFld_ptr->:=Sequence number:C244(<>Fnd_List_Table_ptr->)
		<>Fnd_List_ListNameFld_ptr->:=$listName_t
	: (Records in selection:C76(<>Fnd_List_Table_ptr->)>1)  // There are too many records for this list. This should never happen.
		Fnd_Gen_BugAlert (Current method name:C684;"The data file contains "+String:C10(Records in selection:C76(<>Fnd_List_Table_ptr->))+" records for the list named \""+$listName_t+"\".")
	Else 
		LOAD RECORD:C52(<>Fnd_List_Table_ptr->)  // So we can check to see if it's locked.
End case 


If (Fnd_List_RecordLoaded (<>Fnd_List_Table_ptr;15))  // Wait up to 15 seconds for the record to be available.
	<>Fnd_List_ListNameFld_ptr->:=$listName_t  // We do this even for exising records, in case capitalization changed.
	LIST TO BLOB:C556($listRef_i;<>Fnd_List_ListBlobFld_ptr->)
	SAVE RECORD:C53(<>Fnd_List_Table_ptr->)
	UNLOAD RECORD:C212(<>Fnd_List_Table_ptr->)
Else 
	Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
	Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_List";"UnableToSaveRecord";$listName_t))
End if 


  // Return the [Shell_Data] table to its original read/write state.
If ($wasReadOnly_b)
	READ ONLY:C145(<>Fnd_List_Table_ptr->)
End if 