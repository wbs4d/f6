//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_MaximumSize {(Maximum size)} --> Longint

// Allows you set the maximum size of log files
// before they are rolled over.
// Pass 0 to indicate 'do not rollover'

// A log file will be rolled over and renamed with a
// timestamp (local timezone) that indicates the time of rollover
// The rolled over log will then be compressed (zip) and deleted

// Eg. "Sales.log" will become 
//      "Sales (2021-08-11T103809).log.zip"

// Access: Shared

// Parameters: 
//   $1 : Longint : Maximum size in bytes (optional)

// Returns: 
//   $0 : Longint : Maximum size

// Created by Wayne Stewart (2021-08-09)
//     waynestewart@mac.com
// ----------------------------------------------------

Fnd_Log_Init

If (Count parameters:C259=1)
	Use (Storage:C1525.Fnd.Log)
		Storage:C1525.Fnd.Log.maxLogSize:=$1
	End use 
End if 

$0:=Storage:C1525.Fnd.Log.maxLogSize