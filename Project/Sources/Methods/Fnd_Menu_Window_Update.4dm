//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Window_Update

// Updates the Window menu.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Oct 21, 2003
// Modified by Dave Batton on Feb 5, 2004
//   We now test to make sure Count menu items is passed a valid value.
// Modified by Dave Batton on Dec 26, 2004
//   The "Close All Windows" menu item no longer disappears when more windows are open than the menu supports.
// Modified by Dave Batton on Mar 16, 2006
//   Changed the timeout value to use the new variable rather than being hard-coded.
// Modified by Dave Batton on Mar 26, 2007
//   Renamed this method to Fnd_Menu_Window_Update and changed it from Private to Protected.
//   It was previously named Fnd_Menu_UpdateWindowsMenu.
//   Changed all of the <>Fnd_Menu_WindowMenu_i IP variables to process variables.
//   Modified the test to make sure the Window menu exists before trying to use it.
// ----------------------------------------------------

C_LONGINT:C283($menuItem_i)

If (Not:C34(Semaphore:C143(<>Fnd_Menu_Semaphore_t; <>Fnd_Menu_SemaphoreTimeout_i)))  // DB060316
	
	// First we delete any existing items from the menu.
	For ($menuItem_i; Count menu items:C405(<>Fnd_Menu_WindowMenu_t); 1; -1)
		DELETE MENU ITEM:C413(<>Fnd_Menu_WindowMenu_t; $menuItem_i)
	End for 
	
	// Add a menu item for each process that has a window.
	For ($menuItem_i; 1; Size of array:C274(<>Fnd_Menu_Window_Processes_ai))
		APPEND MENU ITEM:C411(<>Fnd_Menu_WindowMenu_t; <>Fnd_Menu_Window_Items_at{$menuItem_i})
		SET MENU ITEM METHOD:C982(<>Fnd_Menu_WindowMenu_t; -1; "Fnd_Menu_WindowItem")
	End for 
	
	If (Count menu items:C405(<>Fnd_Menu_WindowMenu_t)>0)
		//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
		APPEND MENU ITEM:C411(<>Fnd_Menu_WindowMenu_t; "(-")
		APPEND MENU ITEM:C411(<>Fnd_Menu_WindowMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "CloseAllWindows"))
		SET MENU ITEM METHOD:C982(<>Fnd_Menu_WindowMenu_t; -1; "Fnd_Menu_WindowItem")
		//End if 
	End if 
	
	CLEAR SEMAPHORE:C144(<>Fnd_Menu_Semaphore_t)
End if 
