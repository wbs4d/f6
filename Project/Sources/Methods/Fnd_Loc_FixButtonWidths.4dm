//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_FixButtonWidths (->button1; ->button2{; ->buttonN})

// Adjusts all of the button widths of two or more _vertically_aligned_ buttons so
//   they all have the same left and right coordinates of the widest button
//   of the group.  Designed to be called after calling Fnd_Gen_ButtonText on
//   each of the buttons in the group.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to a button
//   $2..$N : Pointer : More button pointers

// Returns: Nothing

// Created by Dave Batton on May 23, 2004
// ----------------------------------------------------

C_POINTER:C301(${1})
C_LONGINT:C283($buttonNumber_i;$left_i;$top_i;$right_i;$bottom_i;$newLeft_i;$newRight_i)

// Fnd_Loc_
// Start by finding the widest button in the group.
$newLeft_i:=0
$newRight_i:=0
For ($buttonNumber_i;1;Count parameters:C259)
	OBJECT GET COORDINATES:C663(${$buttonNumber_i}->;$left_i;$top_i;$right_i;$bottom_i)
	If (($right_i-$left_i)>($newRight_i-$newLeft_i))
		$newLeft_i:=$left_i
		$newRight_i:=$right_i
	End if 
End for 

// Now adjust all of the buttons so they have the same left and right coordinates.
For ($buttonNumber_i;1;Count parameters:C259)
	OBJECT GET COORDINATES:C663(${$buttonNumber_i}->;$left_i;$top_i;$right_i;$bottom_i)
	OBJECT MOVE:C664(${$buttonNumber_i}->;$newLeft_i;$top_i;$newRight_i;$bottom_i;*)
End for 
