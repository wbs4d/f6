//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SavePrefs

// Save's the user's local and server preferences.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 6, 2006
// Renamed the original Fnd_Pref_SavePrefs routine to Fnd_Pref_SaveLocalPrefs.
// ----------------------------------------------------

C_TEXT:C284($processName_t)
C_LONGINT:C283($state_i; $time_i)

Fnd_Pref_Init

Error:=0

// We need to be careful not to access the data file if this is a local process.
PROCESS PROPERTIES:C336(Current process:C322; $processName_t; $state_i; $time_i)

If ($processName_t#"$@")
	If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
		Fnd_Pref_SaveLocalPrefs
		Fnd_Pref_SaveServerPrefs
		
		CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
		
	Else 
		Error:=-9990  // This is a general 4D time-out error number.
	End if 
	
	If (Error#0)
		ALERT:C41("There was a problem saving the preferences file. ["+String:C10(Error)+"]")  // ### Needs improvement.
	End if 
	
Else 
	Fnd_Gen_LaunchAsNewProcess(Current method name:C684; "Fnd_Pref process")
End if 