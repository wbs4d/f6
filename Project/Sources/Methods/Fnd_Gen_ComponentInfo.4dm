//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ComponentInfo (component name; info requested) --> Text

// Pass this method a component name and name for the information
//   you want. If the component exists, Foundation will call its _Info routine,
//   pass it the info label, and return the result. This allows you to get
//   information from a component in a single call, rather than having to 
//   first test to see if the component is available.
// If the component isn't installed, "Component Not Available" will be returned.
// If the component doesn't have an _Info routine,
//   "Component Did Not Recognize Request" will be returned.
// We recommend that if a component doesn't recognize a request it should
//   return the text "Fnd_LabelNotRecognized".  This routine will return this
//   response as "Component Did Not Recognize Request".
// These values will not be localized so the calling method can test for them.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The component name
//   $2 : Text : Info desired

// Returns: 
//   $0 : Text : Response from component

// Created by Dave Batton on Feb 20, 2004
// Modified by Dave Batton on Jun 29, 2004
//   If "Fnd_Loc" and "language" are passed, this routine now returns
//   "EN" if the Localization component isn't installed.
// Modified by Dave Batton on Dec 12, 2004
//   Now returns Component Did Not Respond if the component doesn't have an _Info method.
// Modified by Dave Batton on Jan 30, 2005
//  This routine no longer changes the value of 4D's Error variable.
// ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$component_t;$request_t;$reply_t;$errorMethod_t;$method_t)
C_LONGINT:C283($oldErrorValue_i)

$component_t:=$1
$request_t:=$2

// We'll skip the Fnd_Gen_Init call, since Fnd_Gen_ComponentAvailable will do it anyway.

$reply_t:="Component Not Available"  // The default response.

If (Fnd_Gen_ComponentAvailable($component_t))
	// Install error handling so we don't cause any errors.
	$oldErrorValue_i:=Error  // DB050130
	$errorMethod_t:=Method called on error:C704
	ON ERR CALL:C155("Fnd_Gen_DummyMethod")
	Error:=0
	
	$method_t:=$component_t+"_Info"
	EXECUTE METHOD:C1007($method_t;Fnd_Gen_Result_t;$request_t)
	Case of 
		: (Fnd_Gen_Result_t=$request_t)  // DB041212 - This happens if the component doesn't have an _Info method.
			$reply_t:="Component Did Not Respond"
			
			Case of 
				: ($component_t="Fnd_Loc") & ($request_t="language")
					$reply_t:="EN"
			End case 
			
		: (Error=0)
			$reply_t:=Fnd_Gen_Result_t
			
		Else 
			$reply_t:="Component Did Not Respond"
			
			// Do some special handling.
			Case of 
				: ($component_t="Fnd_Loc") & ($request_t="language")
					$reply_t:="EN"
			End case 
	End case 
	
	// Restore the previous error value and error handler.
	ON ERR CALL:C155($errorMethod_t)
	Error:=$oldErrorValue_i  // DB050130
End if 

$0:=$reply_t