//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Wnd_CloseBoxMethod

  // A generic method with no code. Used by the Fnd_Wnd_OpenWindow method 
  //  as the close box method for new windows (handle the close in the form method).

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 26, 2006
  // ----------------------------------------------------
