//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_RecordLoaded (->table{; timeout in seconds}) --> Boolean

  // Makes sure the current record is not locked.  If it is, it waits a while to see
  //   if the record becomes avaialble.
  // If no timeout period is specified control is returned immediately if
  //   the record is locked.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : The record's table
  //   $2 : Longint : Number of seconds to wait if it's locked (optional)

  // Returns: 
  //   $0 : Boolean : True if the record isn't locked

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Jun 16, 2004
  //   Localized the Waiting for Record message.
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$loaded_b;$messageOpened_b)
C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($2;$secondsToWait_i;$giveUpTime_i)

$table_ptr:=$1

If (Is new record:C668($table_ptr->))
	$loaded_b:=True:C214
	
Else 
	If (Count parameters:C259>1)
		$secondsToWait_i:=$2
	Else 
		$secondsToWait_i:=30
	End if 
	
	LOAD RECORD:C52($table_ptr->)
	
	$messageOpened_b:=False:C215
	$giveUpTime_i:=Tickcount:C458+($secondsToWait_i*60)  // Decide when we'll stop waiting on a locked record. Convert seconds to ticks.
	
	While ((Locked:C147($table_ptr->)) & (Tickcount:C458<$giveUpTime_i))
		$messageOpened_b:=True:C214
		Fnd_Dlg_Message (Fnd_Gen_GetString ("Fnd_List";"WaitingForRecord"))
		LOAD RECORD:C52($table_ptr->)
		DELAY PROCESS:C323(Current process:C322;10)
	End while 
	
	  // We'll only call Fnd_Dlg_MessageClose if we displayed it. This keeps us from messing 
	  //   with a message window that was opened before we got here.
	If ($messageOpened_b)
		Fnd_Dlg_MessageClose 
	End if 
	
	  // If, even after waiting, we still have a locked record, return False.
	If (Locked:C147($table_ptr->))
		$loaded_b:=False:C215
	Else 
		$loaded_b:=True:C214
	End if 
End if 

$0:=$loaded_b
