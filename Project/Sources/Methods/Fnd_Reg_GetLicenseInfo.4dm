//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_GetLicenseInfo ({feature code}) --> Text

  // Returns the name of the registered user.  Returns an empty string
  //   if the program hasn't been registered yet.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: 
  //   $0 : Text : The name of the registered user

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$featureCode_t)

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

READ ONLY:C145(<>Fnd_Reg_Table_ptr->)
Fnd_Reg_LoadRegistrationRecord ($featureCode_t)

$0:=<>Fnd_Reg_LicenseInfoFld_ptr->
