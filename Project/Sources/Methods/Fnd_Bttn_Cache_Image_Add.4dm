//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_Cache_Image_Add (composite name; image)

  // Adds an image to the RAM cache of button images.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The name returned by the Fnd_Bttn_GetPictureName function
  //   $2 : Picture : The picture to add to the cache

  // Returns: Nothing

  // Created by Mark Mitchenall on 20/9/01
  //   Original Method Name: rollover_Cache_Image_Add
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // ----------------------------------------------------


C_TEXT:C284($1;$compositeName_t)
C_PICTURE:C286($2;$image_pic)
C_LONGINT:C283($width_i;$height_i;$index_i)

$compositeName_t:=$1
$image_pic:=$2

If (Not:C34(Semaphore:C143("$Fnd_Bttn_Cache";300)))  // Wait up to 5 seconds.
	
	$index_i:=Find in array:C230(<>Fnd_Bttn_CacheNames_at;$compositeName_t)
	
	If ($index_i<0)
		$index_i:=Size of array:C274(<>Fnd_Bttn_CacheNames_at)+1
		INSERT IN ARRAY:C227(<>Fnd_Bttn_CacheNames_at;$index_i;1)
		INSERT IN ARRAY:C227(<>Fnd_Bttn_CachePictures_apic;$index_i;1)
		INSERT IN ARRAY:C227(<>Fnd_Bttn_PictureWidths_ai;$index_i;1)
		INSERT IN ARRAY:C227(<>Fnd_Bttn_CachePictSizes_ai;$index_i;1)
		INSERT IN ARRAY:C227(<>Fnd_Bttn_Tickcounts_ai;$index_i;1)
	End if 
	
	PICTURE PROPERTIES:C457($image_pic;$width_i;$height_i)
	
	<>Fnd_Bttn_CacheNames_at{$index_i}:=$compositeName_t
	<>Fnd_Bttn_CachePictures_apic{$index_i}:=$image_pic
	<>Fnd_Bttn_PictureWidths_ai{$index_i}:=$width_i
	<>Fnd_Bttn_CachePictSizes_ai{$index_i}:=Picture size:C356($image_pic)
	<>Fnd_Bttn_Tickcounts_ai{$index_i}:=Tickcount:C458
	
	<>Fnd_Bttn_CacheTotalSize_i:=<>Fnd_Bttn_CacheTotalSize_i+<>Fnd_Bttn_CachePictSizes_ai{$index_i}
	<>Fnd_Bttn_CacheTotalMisses_i:=<>Fnd_Bttn_CacheTotalMisses_i+1
	
	  //Clear any old images which now shouldn't be in the cache.  
	Fnd_Bttn_Cache_GarbageCollec 
	
	CLEAR SEMAPHORE:C144("$Fnd_Bttn_Cache")
End if 