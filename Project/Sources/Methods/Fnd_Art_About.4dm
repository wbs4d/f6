//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Art_About

// Displays the About Box dialog in a new process.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 29, 2003
// Modified by Dave Batton on Jan 13, 2004
//   The Fnd_Loc and Fnd_Menu components are now optional.
// Modified by Dave Batton on Jul 1, 2004
//   The window title is now set only if it's shorter than 20 characters.
// Modified by Dave Batton on Jan 30, 2005
//   Tweaked the window title behavior a little more.
// Modified by Dave Batton on Sep 15, 2007
//   Updated for 4D v11 SQL.
// Modified by: Walt Nelson (3/17/10)
//   per Darwin Caverly to check for a custom About form
// Modified by Wayne Stewart (2018-04-12) - removed call to Fnd_Gen_ComponentCheck
// Mod by Wayne Stewart, (2019-10-26) - Fnd-6 modifications
// ----------------------------------------------------

C_TEXT:C284($windowTitle_t)
C_POINTER:C301($nil_ptr)
C_LONGINT:C283($width_i; $height_i)
C_TEXT:C284($formula_t; $FormName_t)  // Modified by: Walt Nelson (3/17/10)

Fnd_Art_Init

If (Fnd_Gen_LaunchAsNewProcess("Fnd_Art_About"; "$Fnd_Art: About Window"))
	
	Fnd_Gen_CurrentFormType(Fnd_Gen_AboutForm)
	
	Fnd_Gen_MenuBar  // Install the menu bar for the current process.
	
	$windowTitle_t:=Fnd_Gen_GetDatabaseInfo("DatabaseName")
	
	// Temporary fix
	$windowTitle_t:="About "+$windowTitle_t
	
	//If (Length($windowTitle_t)<=25)  // DB040701 - Too long and it looks bad.
	//$windowTitle_t:=Fnd_Gen_GetString("Fnd_Art"; "About")+" "+$windowTitle_t
	//Else 
	//$windowTitle_t:=Fnd_Gen_GetString("Fnd_Art"; "About")
	//End if 
	
	Fnd_Wnd_Title($windowTitle_t)
	
	Fnd_Wnd_CloseBox(True:C214)
	Fnd_Wnd_Type(Plain fixed size window:K34:6)
	
	$FormName_t:=Fnd_Art_SetAboutForm
	
	If ($FormName_t="Fnd_Art_About")  // There's no custom About form, open the generic one.
		FORM GET PROPERTIES:C674($FormName_t; $width_i; $height_i)
		Fnd_Wnd_OpenWindow($width_i; $height_i)
	Else   // There's a custom About form.
		EXECUTE METHOD:C1007("Fnd_Host_GetFormProperties"; *; $nil_ptr; $FormName_t; ->$width_i; ->$height_i)
		Fnd_Wnd_OpenFormWindow($nil_ptr; $FormName_t)
	End if 
	
	Fnd_Menu_Window_Add  // Add the About window to the Window menu
	
	If ($FormName_t="Fnd_Art_About")
		DIALOG:C40($FormName_t)
	Else 
		$formula_t:=Command name:C538(40)+"(\""+$FormName_t+"\")"  // 40 = DIALOG
		EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula"; *; $formula_t)
	End if 
	
	Fnd_Menu_Window_Remove  // Remove the About window from the menu
	
	CLOSE WINDOW:C154
End if 