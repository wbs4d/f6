//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Set (sequence number group name; next number)

  // This routine sets the next number in the sequence, and clears out any numbers
  //   in the reuse 

  // Sets the OK variable to 1 if successful.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The group name
  //   $2 : Longint : The new next number in the sequence

  // Returns: Nothing

  // Created by Dave Batton on Nov 5, 2003
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // Modified by Dave Batton on Sep 24, 2007
  //   Changed the $seqNoName_t variable name to $groupName_t.
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_LONGINT:C283($2;$nextSeqNo_i;$giveUpTime_i)

$groupName_t:=$1
$nextSeqNo_i:=$2

Fnd_SqNo_Init 

If (In transaction:C397)
	Fnd_Gen_BugAlert ("Fnd_SqNo_Set";"This method may not be called from within a transaction.")
End if 

READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)
QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_GroupNameFld_ptr->=$groupName_t)  // Find the record for the specified name.

If (Records in selection:C76(<>Fnd_SqNo_Table_ptr->)=0)  // If this is the first time this sequence number name has been used, we must…
	CREATE RECORD:C68(<>Fnd_SqNo_Table_ptr->)  // …create a new record for it.
	<>Fnd_SqNo_IDFld_ptr->:=Sequence number:C244(<>Fnd_SqNo_Table_ptr->)
	<>Fnd_SqNo_GroupNameFld_ptr->:=$groupName_t
	<>Fnd_SqNo_DesignerOnlyFld_ptr->:=False:C215  // By default we allow administrator access to sequence numbers in the editor.
	
Else 
	LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)
	
	If (Locked:C147(<>Fnd_SqNo_Table_ptr->))
		$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
		While ((Locked:C147(<>Fnd_SqNo_Table_ptr->)) & (Tickcount:C458<$giveUpTime_i))
			DELAY PROCESS:C323(Current process:C322;1)  // We're in a hurry here. Be nice, but not too nice.
			LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)  // Try again.
		End while 
	End if 
End if 

If (Locked:C147(<>Fnd_SqNo_Table_ptr->))
	OK:=0  // We're unable to get a number. Let the calling process know.
Else 
	ARRAY LONGINT:C221($aNumbersToReuse;0)
	VARIABLE TO BLOB:C532($aNumbersToReuse;<>Fnd_SqNo_RecycleBinFld_ptr->)  // Wipe out any numbers stored in the blob.
	<>Fnd_SqNo_NextNumberFld_ptr->:=$nextSeqNo_i  // Set the value for the next sequence number to be used.
	SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)  // Either way, we need to save the record.
	OK:=1
End if 

UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure other users can load the record.