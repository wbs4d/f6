//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Gen_CancelQuit


// Call this to end Foundation's attempts to quit.


// Access Type: Shared


// Parameters: None


// Returns: Nothing


// Created by Dave Batton on Sep 17, 2003

// ----------------------------------------------------


Fnd_Gen_Init

<>Fnd_Gen_Quitting_b:=False:C215
