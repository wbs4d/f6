//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_Modify

// Displays a dialog allowing the user to modify all of the records in the current selection.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 4, 2005
// Modified by Dave Batton on Mar 2, 2006
//   Added support for the Fnd_Out component.
// Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
// ----------------------------------------------------

C_LONGINT:C283($width_i; $height_i; $lockedCount_i)
C_BOOLEAN:C305($boolean_b; $wasReadOnly_b)
C_BOOLEAN:C305($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
C_TEXT:C284($windowTitle_t; $msg_t; $oldErrorHandler_t)
C_POINTER:C301($nil_ptr)

Fnd_Rec_Init

// Add the default search fields if the developer didn't add any.
If (Size of array:C274(Fnd_Rec_Fields_aptr)=0)
	Fnd_Rec_AddTable(Fnd_Rec_Table_ptr)  // Load arrays with field pointers and names.
End if 

Fnd_Rec_TableName_t:=Fnd_VS_TableName(Fnd_Rec_Table_ptr)

$windowTitle_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModWindowTitle")

Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
Fnd_Wnd_Type(Movable form dialog box:K39:8)
Fnd_Wnd_Title($windowTitle_t)

FORM GET PROPERTIES:C674("Fnd_Rec_Modify"; $width_i; $height_i)
$width_i:=$width_i+Fnd_Rec_GetExtraWidth
Fnd_Wnd_OpenWindow($width_i; $height_i)

DIALOG:C40("Fnd_Rec_Modify")
CLOSE WINDOW:C154


If (Fnd_Rec_EditorButton_i=1) & (Fnd_Rec_EditorButtonMethod_t#"")  // If the developer has overriden the default behavior.
	EXECUTE FORMULA:C63(Fnd_Rec_EditorButtonMethod_t)
	
Else 
	Case of 
		: ((Fnd_Rec_EditorButton_i=1) & (Application version:C493<"0800"))  // The EDIT FORMULA command wasn't available in 4D 2003.
			Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
			Fnd_Wnd_Type(Movable form dialog box:K39:8)
			Fnd_Wnd_Title($windowTitle_t)
			Fnd_Wnd_OpenFormWindow($nil_ptr; "Fnd_Rec_ModFormula")
			DIALOG:C40("Fnd_Rec_ModFormula")
			CLOSE WINDOW:C154
			
		: (Fnd_Rec_EditorButton_i=1)
			EXECUTE FORMULA:C63(Command name:C538(806)+"(Fnd_Rec_Table_ptr->;Fnd_Rec_Formula_t)")  // 806=EDIT FORMULA  ` EXECUTE METHOD doesn't work here.
			
		: (OK=1)
			// Just use the formula built by the dialog.
	End case 
	
	// Confirm that the user wants to modify the records.
	If ((OK=1) & (Fnd_Rec_Formula_t#""))
		If (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active")  // DB060302
			EXECUTE METHOD:C1007("Fnd_Out_SyncronizeSelection"; *)
		End if 
		
		Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
		Fnd_Wnd_Type(Movable form dialog box:K39:8)
		Fnd_Wnd_Title($windowTitle_t)
		If (Records in selection:C76(Fnd_Rec_Table_ptr->)=1)
			$msg_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModModifyOne")
		Else 
			$msg_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModModifyMany"; String:C10(Records in selection:C76(Fnd_Rec_Table_ptr->)))
		End if 
		Fnd_Dlg_SetText($msg_t; Fnd_Gen_GetString("Fnd_Rec"; "ModConfirm"))
		Fnd_Dlg_SetIcon(Fnd_Dlg_WarnIcon)
		Fnd_Dlg_SetButtons("*"; "*")
		OK:=Fnd_Dlg_Display
	End if 
	
	// Run the formula.
	GET AUTOMATIC RELATIONS:C899($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
	
	If ((OK=1) & (Fnd_Rec_Formula_t#""))
		$wasReadOnly_b:=Read only state:C362(Fnd_Rec_Table_ptr->)
		$oldErrorHandler_t:=Method called on error:C704
		READ WRITE:C146(Fnd_Rec_Table_ptr->)
		SET AUTOMATIC RELATIONS:C310(True:C214; True:C214)
		MESSAGES ON:C181
		START TRANSACTION:C239
		
		ON ERR CALL:C155("Fnd_Rec_ModifyErrorHandler")
		Error:=0
		
		APPLY TO SELECTION:C70(Fnd_Rec_Table_ptr->; EXECUTE FORMULA:C63(Fnd_Rec_Formula_t))
		
		ON ERR CALL:C155($oldErrorHandler_t)
		
		If (Error=0)
			VALIDATE TRANSACTION:C240
			FLUSH CACHE:C297  // Save any changes.
			
			// Handle any locked records here.
			$lockedCount_i:=Records in set:C195("LockedSet")
			If ($lockedCount_i#0)
				Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
				Fnd_Wnd_Type(Movable form dialog box:K39:8)
				Fnd_Wnd_Title("Modify Records")
				If ($lockedCount_i=1)
					$msg_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModOneLocked")
				Else 
					$msg_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModManyLocked"; String:C10($lockedCount_i))
				End if 
				Fnd_Dlg_SetText(Fnd_Gen_GetString("Fnd_Rec"; "ModLockedTitle"); $msg_t)
				Fnd_Dlg_SetButtons("*"; "*")
				Fnd_Dlg_SetIcon(Fnd_Dlg_WarnIcon)
				OK:=Fnd_Dlg_Display
				If (OK=1)
					USE SET:C118("LockedSet")
				End if 
				CLEAR SET:C117("LockedSet")
			End if 
			
			Fnd_Gen_SelectionChanged
			
		Else 
			CANCEL TRANSACTION:C241  // JDH 20230207 Opposes existing Validate Transaction, above.
			Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
			Fnd_Wnd_Type(Movable form dialog box:K39:8)
			Fnd_Dlg_SetText(Fnd_Gen_GetString("Fnd_Rec"; "ModError"; String:C10(Error)))
			Fnd_Dlg_SetIcon(Fnd_Dlg_WarnIcon)
			Fnd_Dlg_Display
		End if 
		
		SET AUTOMATIC RELATIONS:C310($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
		If ($wasReadOnly_b)
			READ ONLY:C145(Fnd_Rec_Table_ptr->)
		End if 
	End if 
End if 
