//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_FileName (path) --> Text

// Returns the file name from the full pathname.  Foundation has the same routine
//   named Fnd_Gen_FileName, but the Logging component has been created as
//   a stand-alone component.

// Access: Private

// Parameters: 
//   $1 : Text : A full pathname

// Returns: 
//   $0 : Text : The file name at the end of the pathname

// Created by Dave Batton on Dec 27, 2003
// Mod by Wayne Stewart (2018-10-30)
//     waynestewart@mac.com
// ----------------------------------------------------


If (False:C215)
	C_TEXT:C284(Fnd_Log_FileName; $1; $0)
End if 

C_TEXT:C284($1; $0)

Fnd_Log_Init

If (Count parameters:C259=1)
	Fnd.Log.file:=$1
End if 

If (Length:C16(Fnd.Log.file)=0)  // If it's not defined 
	Fnd.Log.file:=Storage:C1525.Fnd.Log.defaultLog  // Reset to default
End if 

$0:=Fnd.Log.file
