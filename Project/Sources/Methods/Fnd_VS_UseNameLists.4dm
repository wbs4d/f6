//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_UseNameLists

  // Uses the virtual structure 4D lists.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 7, 2004
  // Modified by Dave Batton on Sep 16, 2004
  //   Fixed a minor bug in the code.
  // Cannot call LIST TO ARRAY to get info from the Host database lists wwn - (10/16/09)
  // Fixed problems with deleted Tables/Fields wwn - (12/07/09) and (12/09/09)
  // Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES & SET FIELD TITLES
  // Modification : Vincent TOURNIER(12/9/11) changed local arrays to interprocess
  // ----------------------------------------------------

C_LONGINT:C283($tableNumber_i;$fieldNumber_i;$fieldCount_i;$existingList_i;$element_i;$tableCounter_i;$fieldCounter_i;$hostTableNumber_i)  // Modified by : Vincent TOURNIER(14/12/11)-$hostTableNumber_i and added


C_TEXT:C284($listName_t;$languageCode_t)
  // BEGIN Modification by : Vincent TOURNIER(9/12/11)
ARRAY TEXT:C222(<>Fnd_VS_HostFieldNames_at;0)
  // END Modification by : Vincent TOURNIER(9/12/11)

Fnd_VS_Init 

$tableNumber_i:=0

If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
	For ($tableCounter_i;1;Get last table number:C254)  // wwn(12/7/9)changed$tableNumber_i to$tableCounter_i-$tableCounter_i includes deleted
		
		If (Is table number valid:C999($tableCounter_i))
			If (Fnd_VS_NbrValidFields ($tableCounter_i)>0)  // Modification : Vincent TOURNIER(9/12/11)
				$tableNumber_i:=$tableNumber_i+1  // wwn(12/7/9)$tableNumber_i increments only w
				
				  // Try to use the localized language.
				$hostTableNumber_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableCounter_i)  // Modification : Vincent TOURNIER(14/12/11)
				
				$listName_t:="VS_Table"+String:C10($tableCounter_i;"000")+"_"+Fnd_Gen_ComponentInfo ("Fnd_Loc";"language")  // use$tableCounter_i here
				  // LIST TO ARRAY($listName_t;◊Fnd_VS_HostFieldNames_at)  ` wwn (10/16/09)
				<>Fnd_VS_HostArray_ptr:=-><>Fnd_VS_HostFieldNames_at
				EXECUTE METHOD:C1007("Fnd_Host_ListToArray";*;$listName_t;<>Fnd_VS_HostArray_ptr)  // this line fails to get the list values(12/7/9)wwn
				
				  // If we don't have lists for the current language, try English.
				If (Size of array:C274(<>Fnd_VS_HostFieldNames_at)=0)
					$listName_t:="VS_Table"+String:C10($tableCounter_i;"000")+"_EN"  // use$tableCounter_i here
					  // LIST TO ARRAY($listName_t;$fieldNames_at)  ` wwn (10/16/09)
					<>Fnd_VS_HostArray_ptr:=-><>Fnd_VS_HostFieldNames_at
					EXECUTE METHOD:C1007("Fnd_Host_ListToArray";*;$listName_t;<>Fnd_VS_HostArray_ptr)
				End if 
				
				If (Size of array:C274(<>Fnd_VS_HostFieldNames_at)>0)
					$fieldCount_i:=Get last field number:C255($tableCounter_i)  // use$tableCounter_i here
					ARRAY TEXT:C222(<>Fnd_VS_HostFieldNames_at;$fieldCount_i+3)  // Resize it to avoid runtime errors.
					
					
					  // BEGIN Modification : Vincent TOURNIER(9/12/11)
					If (<>Fnd_VS_HostFieldNames_at{2}#"")
						<>Fnd_VS_TableNames_at{$hostTableNumber_i}:=<>Fnd_VS_HostFieldNames_at{2}
					Else 
						<>Fnd_VS_TableNames_at{$hostTableNumber_i}:=Table name:C256($tableCounter_i)  // use$tableCounter_i here
					End if 
					  // END Modification : Vincent TOURNIER(9/12/11)
					
					$fieldNumber_i:=0
					
					For ($fieldCounter_i;1;Get last field number:C255($tableCounter_i))  // use$tableCounter_i here
						If (<>Fnd_VS_HostFieldNames_at{$fieldCounter_i+3}#" -- Deleted Field --")  // wwn(12/9/9)Deleted fields are marked as" -- Deleted Field --"must skip them
							
							$fieldNumber_i:=$fieldNumber_i+1  // wwn(12/9/9)$fieldNumber_i increments only w
							
							  // BEGIN Modification : Vincent TOURNIER(9/12/11)
							If (<>Fnd_VS_HostFieldNames_at{$fieldCounter_i+3}#"")
								<>Fnd_VS_FieldNames_at{$hostTableNumber_i}{$fieldNumber_i}:=<>Fnd_VS_HostFieldNames_at{$fieldCounter_i+3}
							Else 
								<>Fnd_VS_FieldNames_at{$hostTableNumber_i}{$fieldNumber_i}:=Field name:C257($tableCounter_i;$fieldCounter_i)  // use$tableCounter_i here
							End if 
							  // END Modification : Vincent TOURNIER(9/12/11)
						End if 
					End for 
					
					  // BEGIN Modification : Vincent TOURNIER(9/12/11)
					  // SET FIELD TITLES((Table($tableCounter_i))->;◊Fnd_VS_FieldNames_at{$tableNumber_i};◊Fnd_VS_FieldNumbers_ai{$tableNumber_i};*)  ` use $tableCounter_i for Table command
					SET FIELD TITLES:C602((Table:C252($tableCounter_i))->;<>Fnd_VS_FieldNames_at{$hostTableNumber_i};<>Fnd_VS_FieldNumbers_ai{$hostTableNumber_i};*)  // use$tableCounter_i for Table comman
					  // END Modification : Vincent TOURNIER(9/12/11)
					
				End if 
			End if 
		End if 
	End for 
	
	SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at;<>Fnd_VS_TableNumbers_ai;*)  // wwn(2/11/9)-added*
	
	CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
End if 