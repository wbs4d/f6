//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Pref

// Compiler variables related to the Foundation preference routines.
// Called by Fnd_Pref_Init.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 16, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Pref_Initialized_b)
If (Not:C34(<>Fnd_Pref_Initialized_b))  // So we only do this once.
	C_TEXT:C284(<>Fnd_Pref_LastKey_t)
	C_TEXT:C284(<>Fnd_Pref_WindowPrefName_t)
	
	C_TEXT:C284(<>Fnd_Pref_UserName_t)
	C_TEXT:C284(<>Fnd_Pref_FormName_t)
	
	C_LONGINT:C283(<>Fnd_Pref_NavPaletteCheckbox_i)
	C_LONGINT:C283(<>Fnd_Pref_OffscreenButton_i)
	C_LONGINT:C283(<>Fnd_Pref_SizingButton_i)
	C_LONGINT:C283(<>Fnd_Pref_Left_i; <>Fnd_Pref_Top_i; <>Fnd_Pref_Right_i; <>Fnd_Pref_Bottom_i)
	
	C_POINTER:C301(<>Fnd_Pref_Table_ptr; <>Fnd_Pref_IDFld_ptr; <>Fnd_Pref_OwnerFld_ptr)
	C_POINTER:C301(<>Fnd_Pref_NameFld_ptr; <>Fnd_Pref_TypeFld_ptr; <>Fnd_Pref_ValueFld_ptr)
	
	ARRAY TEXT:C222(<>Fnd_Pref_Names_at; 0)
	ARRAY INTEGER:C220(<>Fnd_Pref_Scopes_ai; 0)
	ARRAY INTEGER:C220(<>Fnd_Pref_Types_ai; 0)
	ARRAY TEXT:C222(<>Fnd_Pref_Values_at; 0)
	ARRAY BOOLEAN:C223(<>Fnd_Pref_Modified_ab; 0)
End if 


// Process Variables
C_BOOLEAN:C305(Fnd_Pref_Initialized_b)
If (Not:C34(Fnd_Pref_Initialized_b))  // So we only do this once.
	C_TEXT:C284(Fnd_Pref_XMLElement_t)
	C_LONGINT:C283(Fnd_Pref_width_i; Fnd_Pref_height_i)  // Modified by: Walt Nelson (2/19/10)
End if 


// Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Pref_Alert; $1)
	
	C_TEXT:C284(Fnd_Pref_Display; $1)
	
	C_TEXT:C284(Fnd_Pref_GetBoolean; $1)
	C_BOOLEAN:C305(Fnd_Pref_GetBoolean; $0; $2)
	C_LONGINT:C283(Fnd_Pref_GetBoolean; $3)
	
	C_TEXT:C284(Fnd_Pref_GetLongInt; $1)
	C_LONGINT:C283(Fnd_Pref_GetLongInt; $0; $2; $3)
	
	C_LONGINT:C283(Fnd_Pref_GetProcessWindowRef; $0; $1)
	
	C_REAL:C285(Fnd_Pref_GetReal; $0; $2)
	C_TEXT:C284(Fnd_Pref_GetReal; $1)
	C_LONGINT:C283(Fnd_Pref_GetReal; $3)
	
	C_BOOLEAN:C305(Fnd_Pref_GetSharedValue; $0)
	C_TEXT:C284(Fnd_Pref_GetSharedValue; $1)
	C_POINTER:C301(Fnd_Pref_GetSharedValue; $2)
	
	C_TEXT:C284(Fnd_Pref_GetText; $0; $1; $2)
	C_LONGINT:C283(Fnd_Pref_GetText; $3)
	
	C_TEXT:C284(Fnd_Pref_GetWindow; $1)
	C_POINTER:C301(Fnd_Pref_GetWindow; $2; $3; $4; $5)
	
	C_TEXT:C284(Fnd_Pref_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Pref_Iso2Utf8; $0; $1)
	
	C_TEXT:C284(Fnd_Pref_Path; $0)
	
	C_TEXT:C284(Fnd_Pref_SaveServerPreference; $1; $2; $4)
	C_LONGINT:C283(Fnd_Pref_SaveServerPreference; $3; $5)
	
	C_TEXT:C284(Fnd_Pref_SetBoolean; $1)
	C_BOOLEAN:C305(Fnd_Pref_SetBoolean; $2)
	C_LONGINT:C283(Fnd_Pref_SetBoolean; $3)
	
	C_TEXT:C284(Fnd_Pref_SetLongInt; $1)
	C_LONGINT:C283(Fnd_Pref_SetLongInt; $2; $3)
	
	C_TEXT:C284(Fnd_Pref_SetReal; $1)
	C_REAL:C285(Fnd_Pref_SetReal; $2)
	C_LONGINT:C283(Fnd_Pref_SetReal; $3)
	
	C_TEXT:C284(Fnd_Pref_SetText; $1; $2)
	C_LONGINT:C283(Fnd_Pref_SetText; $3)
	
	C_TEXT:C284(Fnd_Pref_SetValue; $1; $3)
	C_LONGINT:C283(Fnd_Pref_SetValue; $2; $4)
	
	C_TEXT:C284(Fnd_Pref_SetWindow; $1)
	C_LONGINT:C283(Fnd_Pref_SetWindow; $2; $3; $4; $5)
	
	C_TEXT:C284(Fnd_Pref_UserName; $0; $1)
	
	C_TEXT:C284(Fnd_Pref_XmlDecode; $0; $1)
	
	C_TEXT:C284(Fnd_Pref_XmlEncode; $0; $1)
	
	C_TEXT:C284(Fnd_Pref_XmlParse; $1)
	
	C_TEXT:C284(Fnd_Pref_GetObject; $1)
	C_OBJECT:C1216(Fnd_Pref_GetObject; $0; $2)
	C_LONGINT:C283(Fnd_Pref_GetObject; $3)
	
	C_TEXT:C284(Fnd_Pref_SetObject; $1)
	C_OBJECT:C1216(Fnd_Pref_SetObject; $2)
	C_LONGINT:C283(Fnd_Pref_SetObject; $3)
	
	
End if 
