//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_GetDatabaseInfo (info type) --> Text

// Gets the specified database information value.
// Use the Fnd_Gen_SetDatabaseInfo to set this information.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The info type label

// Returns: 
//   $0 : Text : The value

// Created by Dave Batton on Aug 27, 2003
// Mod by Wayne Stewart, (2021-04-05) - 
// ----------------------------------------------------

C_TEXT:C284($0;$1;$label_t;$value_t)
C_LONGINT:C283($element_t)

$label_t:=$1

Fnd_Gen_Init

$value_t:=""

If (Storage:C1525.Fnd.Gen[$label_t]#Null:C1517)
	$value_t:=Storage:C1525.Fnd.Gen[$label_t]
End if 

$0:=$value_t