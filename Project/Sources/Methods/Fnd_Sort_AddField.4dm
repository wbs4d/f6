//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_AddField (->field{; position})

  // Lets the developer add a field to the Sort dialog.  If a position isn't
  //   specified, the field is added to the end of the list.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the field to add
  //   $2 : Longint : The position in which to add the item (optional)

  // Returns: Nothing

  // Created by Dave Batton on May 21, 2004
  // ----------------------------------------------------

C_POINTER:C301($1;$field_ptr)
C_LONGINT:C283($2;$position_i)

$field_ptr:=$1

If (Count parameters:C259>=2)
	$position_i:=$2
Else 
	$position_i:=MAXLONG:K35:2
End if 

If (Not:C34(Is nil pointer:C315($field_ptr)))
	Fnd_Sort_AddItem ($position_i;"";"";$field_ptr)
End if 
