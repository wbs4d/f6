//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: YesNoCancel (message{; window title}) --> Text

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.
  // Part of the Foundation Compatibility component.

  // Parameters: 
  //   $1 : Text : The message to display
  //   $5 : Text : The window title (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 13, 2003
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2)

Fnd_Dlg_SetText ($1)
Fnd_Dlg_SetIcon (2)  // Warn icon.
Fnd_Dlg_SetButtons ("*";"*";"*")

If (Count parameters:C259>=2)
	Fnd_Wnd_Title ($2)
End if 

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))
	Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)  // Centered over window.
End if 

BEEP:C151

Fnd_Dlg_Display 

Fnd_Cmpt_SetBooleanValue ("gCenterWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gUpperWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gSafetyOn";False:C215)

Case of 
	: (OK=1)
		$0:="yes"
	: (OK=2)
		$0:="no"
	Else 
		$0:="cancel"
End case 
