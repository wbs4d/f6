//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Cmpt

// Compiler variables related to the Foundation Compatibility routines.
// Part of the Foundation Compatibility component.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 16, 2003
// Modified by: Walt Nelson (12/23/09) added gCenterWind, gSafetyOn, gUpperWind
// ----------------------------------------------------


// Process Variables
If (Fnd.Cmpt.Initialized=Null:C1517)
	C_BOOLEAN:C305(gCenterWind; gSafetyOn; gUpperWind)
	C_LONGINT:C283(Fnd_Cmpt_Longint_i)
	//C_BOOLEAN(Fnd.Cmpt.Boolean)
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Alert2; $1; $3; $4)
	C_LONGINT:C283(Alert2; $2)
	
	C_TEXT:C284(BugAlert; $1; $2)
	
	C_LONGINT:C283(CenterWindow; $1; $2; $3)
	C_TEXT:C284(CenterWindow; $4; $5)
	
	C_LONGINT:C283(ChoiceList; $0; $4)
	C_TEXT:C284(ChoiceList; $1; $3)
	C_POINTER:C301(ChoiceList; $2)
	
	C_TEXT:C284(Confirm2; $1; $3; $4; $5)
	C_LONGINT:C283(Confirm2; $2)
	
	C_BOOLEAN:C305(Fnd_Cmpt_GetBooleanValue; $0)
	C_TEXT:C284(Fnd_Cmpt_GetBooleanValue; $1)
	
	C_LONGINT:C283(Fnd_Cmpt_GetLongintValue; $0)
	C_TEXT:C284(Fnd_Cmpt_GetLongintValue; $1)
	
	C_TEXT:C284(Fnd_Cmpt_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Cmpt_SetBooleanValue; $1)
	C_BOOLEAN:C305(Fnd_Cmpt_SetBooleanValue; $2)
	
	C_TEXT:C284(Fnd_Cmpt_SetLongintValue; $1)
	C_LONGINT:C283(Fnd_Cmpt_SetLongintValue; $2)
	
	C_TEXT:C284(Message2; $1; $2)
	
	C_LONGINT:C283(OutputSort; $1)
	C_POINTER:C301(OutputSort; $2)
	
	C_TEXT:C284(PrefsGet; $0; $1)
	
	C_TEXT:C284(PrefsPut; $1; $2)
	
	C_LONGINT:C283(ProgOpen; $1)
	C_TEXT:C284(ProgOpen; $2; $3; $4)
	
	C_LONGINT:C283(ProgUpdate; $1)
	
	C_TEXT:C284(Request2; $0; $1; $2; $4; $5; $6)
	C_LONGINT:C283(Request2; $3)
	
	C_BOOLEAN:C305(SelfStart; $0)
	C_TEXT:C284(SelfStart; $1; $2)
	
	C_LONGINT:C283(SetMenuBar; $1)
	
	C_TEXT:C284(YesNoCancel; $0; $1; $2)
End if 