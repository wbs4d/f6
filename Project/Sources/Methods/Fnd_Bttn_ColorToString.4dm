//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_ColorToString (rgb color) --> Text

  // Converts a 4D RGB color value to a CSS2-compatible string in the format "#rrggbb".

  // Access Type: Private

  // Parameters: 
  //   $1 : Longint : The 4D RGB color

  // Returns: 
  //   $0 : Text : The color string

  // Created by Dave Batton.
  // ----------------------------------------------------

C_TEXT:C284($0;$color_t)
C_LONGINT:C283($1;$color_i)

$color_i:=$1

$color_t:=String:C10($color_i;"&x")
$color_t:=Substring:C12($color_t;3)
Case of 
	: (Length:C16($color_t)=4)
		$color_t:="00"+$color_t
	: (Length:C16($color_t)=8)
		$color_t:=Substring:C12($color_t;3)
End case 
$color_t:="#"+$color_t

$0:=$color_t