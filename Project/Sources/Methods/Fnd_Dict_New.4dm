//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_New ({name}) --> Longint

  // Creates a new dictionary and returns a refernece to it.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Name (optional)

  // Returns: 
  //   $0 : Longint : Dictionary ID

  // Created by Rob Laveaux
  // ----------------------------------------------------

C_TEXT:C284($1;$name_t)
C_LONGINT:C283($0;$dictionary_i;$count_i)

If (Count parameters:C259>=1)
	$name_t:=$1
End if 

Fnd_Dict_LockInternalState (True:C214)

  // Check if there is an unused dictionary left
$dictionary_i:=Find in array:C230(<>Fnd_Dict_RetainCounts_ai;0)

  // If all dictionaries are in use, create a new one
If ($dictionary_i=-1)
	$dictionary_i:=Size of array:C274(<>Fnd_Dict_Names_at)+1
	INSERT IN ARRAY:C227(<>Fnd_Dict_Names_at;$dictionary_i)
	INSERT IN ARRAY:C227(<>Fnd_Dict_RetainCounts_ai;$dictionary_i)
	INSERT IN ARRAY:C227(<>Fnd_Dict_Keys_at;$dictionary_i)
	INSERT IN ARRAY:C227(<>Fnd_Dict_Values_at;$dictionary_i)
	INSERT IN ARRAY:C227(<>Fnd_Dict_DataTypes_ai;$dictionary_i)
End if 

  // Make sure we have a name
If ($name_t="")
	$name_t:="Dictionary "+String:C10($dictionary_i)
End if 

  // Store the name of the dictionary
<>Fnd_Dict_Names_at{$dictionary_i}:=$name_t

  // Set the retain count to 1
<>Fnd_Dict_RetainCounts_ai{$dictionary_i}:=1

  // Make sure the dictionary is empty (because of a side effect of INSERT ELEMENT)
$count_i:=Size of array:C274(<>Fnd_Dict_Keys_at{$dictionary_i})
If ($count_i>0)
	DELETE FROM ARRAY:C228(<>Fnd_Dict_Keys_at{$dictionary_i};1;$count_i)
End if 
$count_i:=Size of array:C274(<>Fnd_Dict_Values_at{$dictionary_i})
If ($count_i>0)
	DELETE FROM ARRAY:C228(<>Fnd_Dict_Values_at{$dictionary_i};1;$count_i)
End if 
$count_i:=Size of array:C274(<>Fnd_Dict_DataTypes_ai{$dictionary_i})
If ($count_i>0)
	DELETE FROM ARRAY:C228(<>Fnd_Dict_DataTypes_ai{$dictionary_i};1;$count_i)
End if 

Fnd_Dict_LockInternalState (False:C215)

$0:=$dictionary_i
