//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_TextToMD5 (text) --> Text

  // Generates and returns a MD5 message digest based on the text.
  // See RFC 1321 for more information:
  //   <http://www.faqs.org/rfcs/rfc1321.html>

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text from which to create the message digest

  // Returns: 
  //   $0 : Text : The message digest

  // Created by Dave Batton on Mar 6, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Text_Init 

$0:=Fnd_Text_TextToMD5_Calc ("CALC";$1)