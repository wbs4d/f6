//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_MinLength ({min chars to use{; min limitation}}) --> Number

  // Allows the developer to set the Minimum password length, and
  //   optionally the Minimum password length defined in the Generator
  //   dialog. Returns the current value of the Minimum password length.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The Minimum password length setting (optional)
  //   $2 : Longint : The Minimum value displayed in the pop-up menu (optional)

  // Returns: 
  //   $0 : Longint : The currently set Minimum length

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$1;$2)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_MinLength_i:=$1
	
	If (Count parameters:C259>=2)
		<>Fnd_Pswd_MinLengthLimit_i:=$2
	End if 
	
	If (<>Fnd_Pswd_MinLength_i<<>Fnd_Pswd_MinLengthLimit_i)
		<>Fnd_Pswd_MinLength_i:=<>Fnd_Pswd_MinLengthLimit_i
	End if 
End if 

$0:=<>Fnd_Pswd_MinLength_i
