//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_CacheMaximumSize {(New Cache size)}  --> longint

// Allows you to get and set the maximum cache size
//   The default (and minimum) is 2 MB (2048*1024)

// Access: Shared

// Parameters: 
//   $1 : longint : The new cache size

// Returns: 
//   $0 : longint : The cache size

// Created by Wayne Stewart (2016-11-17)
//     waynestewart@mac.com
// ----------------------------------------------------


If (False:C215)
	C_LONGINT:C283(Fnd_Bttn_CacheMaximumSize; $1; $0)
End if 

C_LONGINT:C283($1; $0)
C_LONGINT:C283(<>Fnd_Bttn_CacheMaximumSize_i)


If (Count parameters:C259=1)
	If ($1>(2048*1024))
		<>Fnd_Bttn_CacheMaximumSize_i:=$1
	End if 
End if 

$0:=<>Fnd_Bttn_CacheMaximumSize_i

