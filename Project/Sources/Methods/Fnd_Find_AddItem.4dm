//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_AddItem (position; label; method{; ->field{; type}})

// Called by the protected "Fnd_Find_Add" routines to add stuff to the arrays.
// If the label is blank, the field name will be used.

// Access Type: Private

// Parameters: 
//   $1 : Longint : The position
//   $2 : Text : The label
//   $3 : Text : The method
//   $4 : Pointer : The field (optional)
//   $5 : Longint : The field's type (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 20, 2003
// Mod by Wayne Stewart, 2017-03-14 - Support for object field
// Modified by Wayne Stewart, 2018-04-16 - Removed incomplete support of object fields
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable call 
// ----------------------------------------------------

C_LONGINT:C283($1; $5; $position_i; $fieldType_i; $fieldLength_i; $CurrPath_i)
C_TEXT:C284($2; $3; Fnd_Find_FieldName_t; $method_t)
C_POINTER:C301($4; Fnd_Find_Field_ptr; $null_ptr)
C_BOOLEAN:C305($fieldIndexed_b)
ARRAY TEXT:C222($AttributePath_at; 0)

$position_i:=$1
Fnd_Find_FieldName_t:=$2
$method_t:=$3
$fieldType_i:=0

If (Count parameters:C259>=4)
	Fnd_Find_Field_ptr:=$4  // Otherwise Fnd_Find_Field_ptr will be null, which is okay.
	If (Fnd_Find_FieldName_t="")
		Fnd_Find_FieldName_t:=Fnd_VS_FieldName(Fnd_Find_Field_ptr)
	End if 
	
	If (Count parameters:C259>=5)
		$fieldType_i:=$5
	Else 
		GET FIELD PROPERTIES:C258(Fnd_Find_Field_ptr; $fieldType_i; $fieldLength_i; $fieldIndexed_b)
	End if 
End if 

Fnd_Find_Init

Case of 
	: ($position_i<1)
		$position_i:=1
	: ($position_i>(Size of array:C274(Fnd_Find_Fields_aptr)+1))
		$position_i:=Size of array:C274(Fnd_Find_Fields_aptr)+1
End case 

// Mod by Wayne Stewart, 2017-03-20 - Handle Object Fields
If ($fieldType_i#Is object:K8:27)  //  First of all the old code
	INSERT IN ARRAY:C227(Fnd_Find_FieldNames_at; $position_i)
	Fnd_Find_FieldNames_at{$position_i}:=Fnd_Find_FieldName_t
	INSERT IN ARRAY:C227(Fnd_Find_Fields_aptr; $position_i)
	Fnd_Find_Fields_aptr{$position_i}:=Fnd_Find_Field_ptr
	INSERT IN ARRAY:C227(Fnd_Find_FieldTypes_ai; $position_i)
	Fnd_Find_FieldTypes_ai{$position_i}:=$fieldType_i
	INSERT IN ARRAY:C227(Fnd_Find_MethodNames_at; $position_i)
	Fnd_Find_MethodNames_at{$position_i}:=$method_t
	
Else 
	
	//Fnd_Find_GetDistinctAttributes 
	
	//For ($CurrPath_i;1;Size of array($AttributePath_at))
	//If ($AttributePath_at{$CurrPath_i}="@[]")  //  This is an array, we don't want to include this
	//DELETE FROM ARRAY($AttributePath_at;$CurrPath_i)
	//DELETE FROM ARRAY($AttributePath_at;$CurrPath_i-1)
	//$CurrPath_i:=1
	//End if 
	
	//End for 
	
End if 
