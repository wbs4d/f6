//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_TableSeqNoName (->table) --> Text

  // Returns the group name for the table's key field sequence number.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : A table pointer

  // Returns: 
  //   $0 : Text : The name of the sequence number group

  // Created by Dave Batton on Nov 19, 2003
  // ----------------------------------------------------

C_TEXT:C284($0)
C_POINTER:C301($1)

$0:="*Table #"+String:C10(Table:C252($1))  // Do not localize this! It's not needed, and can cause problems.