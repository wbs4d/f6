//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_MenuBar ({menu bar name})

// If the Menu component is installed, it is called to handle the menu bar.
// If it's not, nothing happens with the menus.

// Access Type: Shared

// Parameters: 
//   $1 : Text : menu bar name (optional)

// Returns: Nothing

// Created by Dave Batton on Aug 16, 2003
// Modified by Gary Boudreaux on Dec 21, 2008
//   Corrected parameter list in header
// ----------------------------------------------------

C_TEXT:C284($1)

Fnd_Gen_Init

If (Count parameters:C259>=1)
	<>Fnd_Gen_MenuName_t:=$1
End if 

If (<>Fnd_Gen_MenuName_t#"")
	SET MENU BAR:C67(<>Fnd_Gen_MenuName_t)
	
Else 
	Fnd_Menu_MenuBar("*")
End if 