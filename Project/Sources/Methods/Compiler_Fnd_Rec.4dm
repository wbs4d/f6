//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Rec

  // Compiler variables related to the Foundation Dialog routines.
  // Called by Fnd_Rec_Init.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on August 20, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Rec_Initialized_b)
If (Not:C34(<>Fnd_Rec_Initialized_b))  // So we only do this once.
	
End if 


  // Process Variables
C_BOOLEAN:C305(Fnd_Rec_Initialized_b)
If (Not:C34(Fnd_Rec_Initialized_b))
	C_TEXT:C284(Fnd_Rec_MenuName_t;Fnd_Rec_Value_t;Fnd_Rec_Formula_t;Fnd_Rec_EditorButtonLabel_t;Fnd_Rec_EditorButtonMethod_t)
	C_TEXT:C284(Fnd_Rec_TableName_t;Fnd_Rec_FieldName_t;Fnd_Rec_Label_t;Fnd_Rec_FieldNameLabel_t;Fnd_Rec_NewValueLabel_t)
	
	C_LONGINT:C283(Fnd_Rec_EditorButton_i;Fnd_Rec_OKButton_i;Fnd_Rec_CancelButton_i;Fnd_Rec_OffscreenButton_i)
	
	C_POINTER:C301(Fnd_Rec_Table_ptr;Fnd_Rec_Field_ptr)
	
	ARRAY POINTER:C280(Fnd_Rec_Fields_aptr;0)
	ARRAY TEXT:C222(Fnd_Rec_FieldNames_at;0)
	ARRAY INTEGER:C220(Fnd_Rec_FieldTypes_ai;0)
	ARRAY TEXT:C222(Fnd_Rec_Values_at;0)
End if 


  // Parameters
If (False:C215)
	C_LONGINT:C283(Fnd_Rec_AddItem ;$1;$5)
	C_TEXT:C284(Fnd_Rec_AddItem ;$2;$3)
	C_POINTER:C301(Fnd_Rec_AddItem ;$4)
	
	C_POINTER:C301(Fnd_Rec_AddTable ;$1)
	C_LONGINT:C283(Fnd_Rec_AddTable ;$2)
	
	C_LONGINT:C283(Fnd_Rec_AddToSelection ;$1)
	
	C_LONGINT:C283(Fnd_Rec_BestPopupSize ;$0)
	C_POINTER:C301(Fnd_Rec_BestPopupSize ;$1)
	
	C_LONGINT:C283(Fnd_Rec_GetExtraWidth ;$0)
	
	C_TEXT:C284(Fnd_Rec_Info ;$0;$1)
	
	C_POINTER:C301(Fnd_Rec_NewRecord ;$1)
	
	C_POINTER:C301(Fnd_Rec_ShowAll ;$1)
	
	C_POINTER:C301(Fnd_Rec_SizeArrayObject ;$1)
	C_LONGINT:C283(Fnd_Rec_SizeArrayObject ;$0;$2)
	C_BOOLEAN:C305(Fnd_Rec_NewRecord ;$0)
End if 
