//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatBypass --> Boolean

  // Returns True if the data entry formatting routine should be bypassed. Usually this
  //   will be because the user is holding down the bypass key, but there may be
  //   other reasons.
  // Sets Fnd_Data_FormatError_i to -1 if True is returned.

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Boolean : True if the routine should be bypassed

  // Created by Dave Batton on May 13, 2004
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$bypass_b)

  // Assumes Fnd_Data_Init has already been called by the calling method. 

Case of 
	: (<>Fnd_Data_UserBypassKey_i=1)
		$bypass_b:=True:C214
		
	: (Macintosh option down:C545 & (<>Fnd_Data_UserBypassKey_i=Option key mask:K16:7))
		$bypass_b:=True:C214
		Fnd_Data_FormatError_i:=-1
		
	: (Macintosh command down:C546 & (<>Fnd_Data_UserBypassKey_i=Command key mask:K16:1))
		$bypass_b:=True:C214
		Fnd_Data_FormatError_i:=-1
		
	: (Macintosh control down:C544 & (<>Fnd_Data_UserBypassKey_i=Control key mask:K16:9))
		$bypass_b:=True:C214
		Fnd_Data_FormatError_i:=-1
		
	: (Shift down:C543 & (<>Fnd_Data_UserBypassKey_i=Shift key mask:K16:3))
		$bypass_b:=True:C214
		Fnd_Data_FormatError_i:=-1
		
	: (Caps lock down:C547 & (<>Fnd_Data_UserBypassKey_i=Caps lock key mask:K16:5))
		$bypass_b:=True:C214
		Fnd_Data_FormatError_i:=-1
		
	Else 
		$bypass_b:=False:C215
End case 

$0:=$bypass_b