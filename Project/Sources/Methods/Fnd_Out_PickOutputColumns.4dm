//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_PickOutputColumns

// This routine automatically selects some columns to display in the output form
//   in case the developer hasn't specified any in the Fnd_aa_Out_DisplayTable hook.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 8, 2005
// Mod by Wayne Stewart, (2022-04-19) - Basically a total rewrite
// ----------------------------------------------------

Fnd_Out_Init


var $columnsToSelect_i; $tableNumber_i; $fieldNumber_i; $type_i; $fieldCount_i : Integer
var $field_ptr : Pointer

$columnsToSelect_i:=5  // Just pick a few to display.

$tableNumber_i:=Fnd.out.currentTable

// First add any alpha fields to the list. They tend to be the most interesting.

$fieldCount_i:=Get last field number:C255($tableNumber_i)

For ($fieldNumber_i; 1; $fieldCount_i)
	If (Is field number valid:C1000($tableNumber_i; $fieldNumber_i))
		If (Fnd.out.lbox.columns.length<Fnd.out.maxColumns)  // We don't have too many yet
			If ($fieldNumber_i#Fnd.out.recordIDField)  // Don't add the primary key
				$field_ptr:=Field:C253($tableNumber_i; $fieldNumber_i)
				$type_i:=Type:C295($field_ptr->)
				If ($type_i=Is alpha field:K8:1)
					Fnd_Out_AddField($field_ptr)
					If (Fnd.out.lbox.columns.length>=Fnd.out.maxColumns)
						$fieldNumber_i:=MAXINT:K35:1-1  // Exit this loop.
					End if 
				End if 
			End if 
		End if 
	End if 
End for 

If (Fnd.out.lbox.columns.length<Fnd.out.maxColumns)  // If we don't have enough fields yet, try some other small field types.
	// Notice we don't pick a longint if it's the first field, since that's probably the ID field.
	For ($fieldNumber_i; 1; $fieldCount_i)
		If (Is field number valid:C1000($tableNumber_i; $fieldNumber_i))
			If (Fnd.out.lbox.columns.length<Fnd.out.maxColumns)  // We don't have too many yet
				If ($fieldNumber_i#Fnd.out.recordIDField)  // Don't add the primary key
					$field_ptr:=Field:C253($tableNumber_i; $fieldNumber_i)
					$type_i:=Type:C295($field_ptr->)
					If (($type_i=Is date:K8:7)\
						 | ($type_i=Is time:K8:8)\
						 | ($type_i=Is longint:K8:6)\
						 | ($type_i=Is integer:K8:5)\
						 | ($type_i=Is real:K8:4)\
						 | ($type_i=Is boolean:K8:9))
						Fnd_Out_AddField($field_ptr)
						If (Fnd.out.lbox.columns.length>=Fnd.out.maxColumns)
							$fieldNumber_i:=MAXINT:K35:1-1  // Exit this loop.
						End if 
					End if 
				End if 
			End if 
		End if 
	End for 
End if 

If (Fnd.out.lbox.columns.length<Fnd.out.maxColumns)  // If we don't have enough fields yet, try some other small field types.
	// Notice we don't pick a longint if it's the first field, since that's probably the ID field.
	For ($fieldNumber_i; 1; $fieldCount_i)
		If (Is field number valid:C1000($tableNumber_i; $fieldNumber_i))
			If (Fnd.out.lbox.columns.length<Fnd.out.maxColumns)  // We don't have too many yet
				If ($fieldNumber_i#Fnd.out.recordIDField)  // Don't add the primary key
					$field_ptr:=Field:C253($tableNumber_i; $fieldNumber_i)
					$type_i:=Type:C295($field_ptr->)
					If ($type_i=Is text:K8:3)
						Fnd_Out_AddField($field_ptr)
						$fieldNumber_i:=MAXINT:K35:1-1  // Exit the loop once we find one.
					End if 
				End if 
			End if 
		End if 
	End for 
End if 

