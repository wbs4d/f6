//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: BugAlert (routine name{; details})

  // Allows an older Foundation based database to use the new Fnd_Gen_BugAlert routine.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The calling method name
  //   $2 : Text : More information about the problem (optional)

  // Returns: Nothing

  // Created by Dave Batton on Feb 22, 2004
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$methodName_t)

Fnd_Cmpt_Init 

$methodName_t:=$1
$methodName_t:=Replace string:C233($methodName_t;" method";"")
$methodName_t:=Replace string:C233($methodName_t;" object method";"")
$methodName_t:=Replace string:C233($methodName_t;" form method";"")
$methodName_t:=Replace string:C233($methodName_t;" trigger";"")

If (Count parameters:C259>=2)
	Fnd_Gen_BugAlert ($methodName_t;$2)
Else 
	Fnd_Gen_BugAlert ($methodName_t)
End if 
