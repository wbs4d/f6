//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Art_StartupDialog

// Displays the startup window in a new process.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 2, 2003
// Modified by Gary Boudreaux on Dec 21, 2008
//   Corrected return parameter definition in header
// Modified by Walt Nelson (2/19/10) fixed when component is compiled and host is intrepreted
// ----------------------------------------------------

C_POINTER:C301($nil_ptr)
C_LONGINT:C283($width_i; $height_i; $windowRef_i)
C_TEXT:C284($formula_t; $FormName_t)


Fnd_Art_Init

$FormName_t:=Fnd_Art_SetStartupDialogForm

If ($FormName_t#"")
	
	If (Fnd_Gen_LaunchAsNewProcess("Fnd_Art_StartupDialog"; Storage:C1525.Fnd.Art.StartupProcessName))
		BRING TO FRONT:C326(Current process:C322)
		Fnd_Gen_MenuBar  // Install the menu bar for the current process
		Fnd_Wnd_Type(Modal form dialog box:K39:7)
		
		
		If ($FormName_t="Fnd_Art_StartupDialog")  // There's no custom startup form, open the generic one.
			FORM GET PROPERTIES:C674($FormName_t; $width_i; $height_i)
			$windowRef_i:=Fnd_Wnd_OpenWindow($width_i; $height_i)
			Use (Storage:C1525.Fnd.Art)  //  Store the Window reference
				Storage:C1525.Fnd.Art.WindowReference:=$windowRef_i
			End use 
			
			DIALOG:C40($FormName_t)
			
		Else   // There's a custom startup form.
			$windowRef_i:=Fnd_Wnd_OpenFormWindow($nil_ptr; $FormName_t)
			
			Use (Storage:C1525.Fnd.Art)  //  Store the Window reference
				Storage:C1525.Fnd.Art.WindowReference:=$windowRef_i
			End use 
			
			$formula_t:="Dialog:C40(\""+$FormName_t+"\")"  // 40 = DIALOG
			EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula"; *; $formula_t)
			
		End if 
		
		
		
		CLOSE WINDOW:C154
		
	Else 
		
		
		
		// Wait for the new process to start before returning control to the calling method.
		Repeat 
			DELAY PROCESS:C323(Current process:C322; 5)
		Until (Storage:C1525.Fnd.Art.WindowReference#-1)
		
	End if 
End if 