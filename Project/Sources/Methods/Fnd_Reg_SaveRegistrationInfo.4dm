//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_SaveRegistrationInfo (feature name; personalization; license info; unlock code)

  // Save's the specified info into a registration record.
  // Doesn't check to make sure the activation code is valid.
  //   We'll assume whoever called us did that.

  // Access: Private

  // Parameters: 
  //   $1 : Text : Feature name
  //   $2 : Text : Personalization
  //   $3 : Text : License info
  //   $4 : Text : Unlock code

  // Returns: Nothing

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$3;$4;$featureCode_t;$userName_t;$licenseInfo_t;$unlockCode_t)

$featureCode_t:=$1
$userName_t:=$2
$licenseInfo_t:=$3
$unlockCode_t:=$4

If ($featureCode_t="")
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

READ WRITE:C146(<>Fnd_Reg_Table_ptr->)

QUERY:C277(<>Fnd_Reg_Table_ptr->;<>Fnd_Reg_FeatureCodeFld_ptr->=$featureCode_t)

Case of 
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=0)
		CREATE RECORD:C68(<>Fnd_Reg_Table_ptr->)
		<>Fnd_Reg_IDFld_ptr->:=Sequence number:C244(<>Fnd_Reg_Table_ptr->)
		<>Fnd_Reg_FeatureCodeFld_ptr->:=$featureCode_t
		<>Fnd_Reg_DemoDaysFld_ptr->:=<>Fnd_Reg_DefaultDemoDays_i
		
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=1)
		Fnd_Reg_VerifyRecord 
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"Too many records found.")
		ABORT:C156
End case 

  // If we got this far we should have one record loaded.
<>Fnd_Reg_UserNameFld_ptr->:=$userName_t
<>Fnd_Reg_LicenseInfoFld_ptr->:=$licenseInfo_t
<>Fnd_Reg_UnlockCodeFld_ptr->:=$unlockCode_t
Fnd_Reg_SaveRecord (<>Fnd_Reg_SecretKey_t)

UNLOAD RECORD:C212(<>Fnd_Reg_Table_ptr->)
