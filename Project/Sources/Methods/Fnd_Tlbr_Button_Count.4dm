//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Count --> Longint

// Returns the number of buttons in the toolbar

// Access Type: Shared

// Parameters:
//   None

// Returns: 
//   $0 : Longint : Count

// Created by Rob Laveaux on December 24 2005
// ----------------------------------------------------

C_LONGINT:C283($0;$count_i)

Fnd_Tlbr_Init

$count_i:=Fnd_Tlbr_UsedObjects_i

$0:=$count_i
