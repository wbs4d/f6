//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_Init

// Initializes both the process and interprocess variables used by the Fnd_Wnd routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 14, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
//   Moved the variable default settings from the old Fnd_Wnd_ResetProcess 
//   method into this method.
//   Updated the version number to 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Wnd_Info method.
// Modified by Dave Batton on Feb 19, 2005
//   Added a variable to hold a new semaphore name.
// Modified by Dave Batton on Apr 23, 2005
//   Sets the default window title, type, and position to values that can be tested  ` for in other components.
// Modified on March 2, 2006 by Jack Delay
//  Setup handling for multi-screen Mac systems (see also Fnd_Wnd_MoveOnScreen & Compiler_Fnd_Wnd)
// Modified by Dave Batton on Mar 2, 2006
//   Now we set the window coordinates to 0. See Fnd_Wnd_Position.
//   Added a semaphore timeout variable.
//   Wrapped the IP initialization code in the semaphore.
// Mod by Wayne Stewart, 2023-07-26
//   Moved the test for the semaphore to inside the initialisation routine
//   previously the semaphore was set and unset every time Fnd_XXX_Init is called
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_Wnd_Initialized_b; Fnd_Wnd_Initialized_b)
C_LONGINT:C283($screenNumber_i; $left_i; $top_i; $right_i; $bottom_i)


If (Not:C34(<>Fnd_Wnd_Initialized_b))  // So we only do this once.
	<>Fnd_Wnd_Semaphore_t:="$Fnd_Wnd_Semaphore"  // DB050219
	<>Fnd_Wnd_SemaphoreTimeout_i:=30*60  // DB060316 - Added - 30 seconds.
	
	If (Not:C34(Semaphore:C143(<>Fnd_Wnd_Semaphore_t; <>Fnd_Wnd_SemaphoreTimeout_i)))
		
		Compiler_Fnd_Wnd
		
		// Setup the variables for stacking windows.
		ARRAY LONGINT:C221(<>Fnd_Wnd_StackWndRefNos_ai; 1)
		ARRAY LONGINT:C221(<>Fnd_Wnd_StackLeftPositions_ai; 1)
		ARRAY LONGINT:C221(<>Fnd_Wnd_StackTopPositions_ai; 1)
		// Preset the first stacked window position.
		<>Fnd_Wnd_StackWndRefNos_ai{1}:=0
		If (Is Windows:C1573)
			<>Fnd_Wnd_StackLeftPositions_ai{1}:=5
			<>Fnd_Wnd_StackTopPositions_ai{1}:=Menu bar height:C440+20+14  // Unfortunately we can't determine the window title height.
		Else 
			<>Fnd_Wnd_StackLeftPositions_ai{1}:=2  // Designed so it doesn't shift when zoomed.
			<>Fnd_Wnd_StackTopPositions_ai{1}:=Menu bar height:C440+20+4  // 20 = window title height.
			
			// Create an array of all current screens.
			ARRAY LONGINT:C221(<>Fnd_Wnd_ScreenMap_ai; 4; Count screens:C437)
			<>Fnd_Wnd_ScreenCount_i:=Count screens:C437
			For ($screenNumber_i; 1; <>Fnd_Wnd_ScreenCount_i)
				SCREEN COORDINATES:C438($left_i; $top_i; $right_i; $bottom_i; $screenNumber_i)
				<>Fnd_Wnd_ScreenMap_ai{1}{$screenNumber_i}:=$left_i
				<>Fnd_Wnd_ScreenMap_ai{2}{$screenNumber_i}:=$top_i
				<>Fnd_Wnd_ScreenMap_ai{3}{$screenNumber_i}:=$right_i
				<>Fnd_Wnd_ScreenMap_ai{4}{$screenNumber_i}:=$bottom_i
			End for 
		End if 
		
		// Define the offsets for all other stacked windows.
		<>Fnd_Wnd_StackHOffset_i:=20
		<>Fnd_Wnd_StackVOffset_i:=20
		
		// Setup the array of process names for closing all windows.
		ARRAY TEXT:C222(<>Fnd_Wnd_CloseProcessNames_at; 0)
		<>Fnd_Wnd_Closing_b:=False:C215  // True while we're trying to close all of the windows.
		
		ARRAY TEXT:C222(<>Fnd_Wnd_MenuItemNames_at; 0)
		ARRAY LONGINT:C221(<>Fnd_Wnd_MenuProcessNumbers_ai; 0)
		
		CLEAR SEMAPHORE:C144(<>Fnd_Wnd_Semaphore_t)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
	End if 
	
	<>Fnd_Wnd_Initialized_b:=True:C214
	
End if 



If (Not:C34(Fnd_Wnd_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Wnd
	// Set the default values that the developer will be able to override.
	Fnd_Wnd_WindowTitle_t:="*"  // DB050423 - A value we can test for in other components.
	Fnd_Wnd_WindowPosition_i:=-1  // DB050423 - A value we can test for in other components.
	Fnd_Wnd_CloseBox_b:=False:C215  // True if the window has a close box.
	Fnd_Wnd_PositionPrefName_t:=""  // The name used to save the window position.
	
	Fnd_Wnd_Initialized_b:=True:C214
End if 

// We'll gradually move the variables down to this section

If (Fnd.Wnd.Initialised=Null:C1517)  // So we only do this once per process.
	Fnd.Wnd:=New object:C1471("Initialised"; True:C214)
	Fnd.Wnd.Left:=0  // DB060301 - Added these coordinates.
	Fnd.Wnd.Top:=0
	Fnd.Wnd.Right:=0
	Fnd.Wnd.Bottom:=0
	Fnd.Wnd.relativeTo:=0
	Fnd.Wnd.windowType:=-1  // DB050423 - A value we can test for in other components.
	
	
End if 

