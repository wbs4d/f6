//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Prnt_LabelEditor

  // Displays 4D's built-in Label Editor

  // Access: Shared

  // Parameters: None

  // Returns: None

  // Created by Dave Batton on Dec 1, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   Added a call to the new Fnd_Prnt_Init command.
  // Modified by Dave Batton on Jun 16, 2004
  //   Localized the error message.
  // Modified by Dave Batton on Dec 16, 2004
  //   Added a call to AUTOMATIC RELATIONS before displaying the label editor.
  // Modified by Dave Batton on Mar 3, 2006
  //   Added support for the Fnd_Out component.
  // Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
  // ----------------------------------------------------

C_BOOLEAN:C305($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

GET AUTOMATIC RELATIONS:C899($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

Fnd_Prnt_Init   // DB040205 - Added this. 

If (Is nil pointer:C315(Fnd_Gen_CurrentTable ))
	Fnd_Prnt_Alert (Fnd_Gen_GetString ("Fnd_Prnt";"SelectTableFirst"))  // DB040616
	
Else 
	If (Fnd_Gen_ComponentInfo ("Fnd_Out";"state")="active")  // DB060303
		EXECUTE FORMULA:C63("Fnd_Out_SyncronizeSelection")
	End if 
	
	SET AUTOMATIC RELATIONS:C310(True:C214;True:C214)  // DB041216
	PRINT LABEL:C39(Fnd_Gen_CurrentTable ->;Char:C90(1))
	SET AUTOMATIC RELATIONS:C310($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)
End if 