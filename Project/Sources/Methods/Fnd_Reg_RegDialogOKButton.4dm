//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_RegDialogOKButton

  // We call this from a protected method because a public form can't
  //   call private methods.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Aug 16, 2003
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Clicked:K2:4)
		  //EXECUTE METHOD("Fnd_Host_ExecuteFormula";*;"$2->:=<>Fnd_Reg_UserName_t";->◊Fnd_Reg_UserName_t)
		  //EXECUTE METHOD("Fnd_Host_ExecuteFormula";*;"$2->:=<>Fnd_Reg_UnlockCode_t";->◊Fnd_Reg_UnlockCode_t)
		
		  // Fnd_Reg_RegDialog form is part of Foundation component, no need for above 2 lines wwn 091209
		Case of 
			: (<>Fnd_Reg_UserName_t="")
				Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"NoUserName"))
				
			: (<>Fnd_Reg_UnlockCode_t="")
				GOTO OBJECT:C206(<>Fnd_Reg_UnlockCode_t)
				Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"NoUnlockCode"))
				
			Else 
				Fnd_Reg_Register (Fnd_Reg_DialogFeatureCode_t;<>Fnd_Reg_UnlockCode_t;<>Fnd_Reg_UserName_t)
				
				Case of 
					: (OK=1)
						Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"RegistrationSuccessful"))
						ACCEPT:C269
						
					: (Error=18901)
						Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"InvalidUnlockCode"))
						
					Else 
						Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"UnexpectedError"))
				End case 
		End case 
End case 