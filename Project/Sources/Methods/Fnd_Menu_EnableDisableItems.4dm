//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_EnableDisableItems

// Here's where we enable or disable menu items based on the contents and
//   state of the frontmost window.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Jan 22, 2004
//   Removed the attempt to disable the non-existant <>Fnd_Menu_EnterModifySelItem_i.
// Modified by Dave Batton on May 23, 2004
//   Added code to enable and disable the Window menu title.
// Modified by Dave Batton on Dec 8, 2005
//   Added support for the new Modify Records menu item.
// Modified by Dave Batton on Mar 26, 2007
//   Changed the <>Fnd_Menu_WindowMenu_i IP variables to process variables.
// Added by: Walt Nelson (3/3/10) Enable all menu items of the Window Menu
// Modified by: Doug Hall (2/7/2023)
//   Check for input form, to allow (enable) Print menu there.
//   Add "Find Related..." to the Select menu.
// ----------------------------------------------------

C_BOOLEAN:C305($noTable_b; $noSelection_b; $noUserSet_b)

// JDH 20230207 : To allow Print menu from an input form.
// --------
C_BOOLEAN:C305($isInputForm_b)
$isInputForm_b:=(Fnd_Gen_CurrentFormType=Fnd_Gen_InputForm)
// --------

If (Fnd_Gen_CurrentFormType#Fnd_Gen_OutputForm)
	// Disable the items that are only valid with an output list.
	$noTable_b:=True:C214
	$noSelection_b:=True:C214
	$noUserSet_b:=True:C214
Else 
	$noTable_b:=False:C215
	If (Records in selection:C76(Fnd_Gen_CurrentTable->)=0)
		$noSelection_b:=True:C214
		$noUserSet_b:=True:C214
	Else 
		$noSelection_b:=False:C215
		Case of 
			: (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active")
				$noUserSet_b:=(Fnd_Gen_ComponentInfo("Fnd_Out"; "highlighted")="0")
			Else 
				$noUserSet_b:=(Records in set:C195("UserSet")=0)
		End case 
	End if 
End if 

Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_FileMenu_t)
Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_EditMenu_t)
Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_ToolsMenu_t)
Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_WindowMenu_t)  // Added by: Walt Nelson (3/3/10)


If ($noTable_b)
	Fnd_Menu_DisableAllMenuItems(<>Fnd_Menu_EnterMenu_t)
	Fnd_Menu_DisableAllMenuItems(<>Fnd_Menu_SelectMenu_t)
Else 
	Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_EnterMenu_t)
	Fnd_Menu_EnableAllMenuItems(<>Fnd_Menu_SelectMenu_t)
End if 

If ($noSelection_b)
	If (Not:C34($isInputForm_b))  // JDH 20230207 Print Menu should be available for Input forms, too! Might be empty, though.
		DISABLE MENU ITEM:C150(<>Fnd_Menu_FileMenu_t; <>Fnd_Menu_FilePrintItem_i)
	End if 
	DISABLE MENU ITEM:C150(<>Fnd_Menu_EnterMenu_t; <>Fnd_Menu_EnterModifyRecsItem_i)  // DB051206
	DISABLE MENU ITEM:C150(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectSortItem_i)
	DISABLE MENU ITEM:C150(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectOrderbyEdItem_i)
	DISABLE MENU ITEM:C150(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectFindReltdItem_i)  // JDH 20230207
Else 
	ENABLE MENU ITEM:C149(<>Fnd_Menu_FileMenu_t; <>Fnd_Menu_FilePrintItem_i)
	ENABLE MENU ITEM:C149(<>Fnd_Menu_EnterMenu_t; <>Fnd_Menu_EnterModifyRecsItem_i)  // DB051206
	ENABLE MENU ITEM:C149(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectSortItem_i)
	ENABLE MENU ITEM:C149(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectOrderbyEdItem_i)
	ENABLE MENU ITEM:C149(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectFindReltdItem_i)  // JDH 20230207
End if 

If ($noUserSet_b)
	DISABLE MENU ITEM:C150(<>Fnd_Menu_EnterMenu_t; <>Fnd_Menu_EnterDeleteSetItem_i)
	DISABLE MENU ITEM:C150(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectShowSetItem_i)
	DISABLE MENU ITEM:C150(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectOmitSetItem_i)
Else 
	ENABLE MENU ITEM:C149(<>Fnd_Menu_EnterMenu_t; <>Fnd_Menu_EnterDeleteSetItem_i)
	ENABLE MENU ITEM:C149(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectShowSetItem_i)
	ENABLE MENU ITEM:C149(<>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_SelectOmitSetItem_i)
End if 

//If (Size of array(◊Fnd_Menu_Window_Items_at)=0)  ` DB040523 - Added
//DISABLE MENU ITEM(◊Fnd_Menu_WindowMenu_s;0)  ` DB070326 - Changed from an IP variable to a process variable.
//Else 
//ENABLE MENU ITEM(◊Fnd_Menu_WindowMenu_s;0)  ` DB070326
//End if 

If (Process number:C372("Fnd_Nav: Navigation Palette")>0)  // ### Not the best idea to hard-code this.
	// This currently isn't working the way I'd like it to.  We'll fix it later.
	DISABLE MENU ITEM:C150(<>Fnd_Menu_ToolsMenu_t; <>Fnd_Menu_ToolsNavPaletteItem_i)
End if 
