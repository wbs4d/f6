//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_ChoiceList (prompt; ->text array) --> Number

// Allows the user to select an element from a list.

// Access: Shared

// Parameters:
//   $1 : Text : The prompt
//   $2 : Pointer : A pointer to a text array

// Returns:
//   $0 : Longint : The selected element number

// Created by Dave Batton on Sep 2, 2003
// Modified by Dave Batton on Mar 4, 2004
//   Added support for the Fnd_Grid component.
// Mod by Wayne Stewart, (2019-11-11) - Remove Gid component call
// Use font details obtained from font discovery
// allowed for type-ahead
// ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1)
C_POINTER:C301($2)

C_BOOLEAN:C305($Maxed_b)
C_LONGINT:C283($adjustHeight_i; $bottom_i; $fontSize_i; $fontStyle_i; $height_i; $left_i; $newButtonWidth_i; $newLabelWidth_i; $ok_i; $right_i)
C_LONGINT:C283($RowHeight_i; $ScreenBottom_i; $ScreenLeft_i; $ScreenRight_i; $ScreenTop_i; $top_i; $width_i; $numRows)
C_TEXT:C284($fontName_t; $formula_t)

If (False:C215)
	C_LONGINT:C283(Fnd_List_ChoiceList; $0)
	C_TEXT:C284(Fnd_List_ChoiceList; $1)
	C_POINTER:C301(Fnd_List_ChoiceList; $2)
End if 


Fnd_List_Init

Fnd.List.Prompt:=$1

ARRAY TEXT:C222(Fnd_List_ChoiceArray1_at; 0)
COPY ARRAY:C226($2->; Fnd_List_ChoiceArray1_at)

// If they want type-ahead the list MUST be sorted
If (Fnd.List.useTypeAhead)
	If (Size of array:C274(Fnd_List_ChoiceArray1_at)=Size of array:C274(Fnd_List_Methods_at))
		SORT ARRAY:C229(Fnd_List_ChoiceArray1_at; Fnd_List_Methods_at; >)
	Else 
		SORT ARRAY:C229(Fnd_List_ChoiceArray1_at; >)
	End if 
	
End if 

$fontName_t:=Storage:C1525.font.largeName
$fontSize_i:=Storage:C1525.font.largeSize
$fontStyle_i:=Storage:C1525.font.largeStyle

$newButtonWidth_i:=Fnd_Ext_TextWidth(Fnd.List.NewButtonLabel; $fontName_t; $fontSize_i; $fontStyle_i)+20  // 20 is the extra width needed for the button caps.
If ($newButtonWidth_i<69)
	$newButtonWidth_i:=69
End if 

// added this code so that the choice list would grow to accomodate the title
$newLabelWidth_i:=Fnd_Ext_TextWidth(Fnd.List.Prompt; $fontName_t; $fontSize_i; $fontStyle_i)  // Guy Algot 02/27/09
If ($newLabelWidth_i<219)
	$newLabelWidth_i:=219
End if 

FORM GET PROPERTIES:C674("Fnd_List_ChoiceList"; $width_i; $height_i)
$width_i:=$width_i+($newButtonWidth_i-62)+($newLabelWidth_i-216)  // Guy Algot, 02/27/09

// interesting line? ` Modified by: Guy Algot (08/22/23)

If (True:C214)  // & (False)  // start Guy's code for resizing choice list window based on contents
	
	$numRows:=8
	// can't use this calculation if its a sheet window on Mac
	If (Size of array:C274(Fnd_List_ChoiceArray1_at)>$numRows)  // should this limit be a pref?
		$Maxed_b:=False:C215
		$left_i:=0
		$top_i:=100
		$right_i:=0
		$bottom_i:=$top_i+$height_i
		$RowHeight_i:=20  // 159/8 = 19.8  ` height of area on dialog / number of rows showing
		
		$adjustHeight_i:=(Size of array:C274(Fnd_List_ChoiceArray1_at)*$RowHeight_i)-($numRows*$RowHeight_i)
		//$adjustHeight_i:=(Size of array(Fnd_List_ChoiceArray1_at)-$numRows)*$RowHeight_i
		
		
		$bottom_i:=$bottom_i+($adjustHeight_i/2)
		$top_i:=$top_i-($adjustHeight_i/2)
		
		SCREEN COORDINATES:C438($ScreenLeft_i; $ScreenTop_i; $ScreenRight_i; $ScreenBottom_i)
		
		If ($bottom_i>$ScreenBottom_i)  // | ($adjustHeight_i>$bottom_i)
			$bottom_i:=$ScreenBottom_i-120
			$Maxed_b:=True:C214
			//If ($vL_adjustHeight<0)
			//$vL_adjustHeight:=0
			//End if 
		Else 
			// $bottom_i:=$SC_bottom_i-20  // adjust the bottom to account for dock
		End if 
		
		If ($top_i<40)  // we have gone above the menu bar
			$top_i:=$ScreenTop_i+100
			$Maxed_b:=True:C214
			//If ($vL_adjustHeight<0)
			//$vL_adjustHeight:=0
			//End if 
		Else 
			// $bottom_i:=$SC_bottom_i-20  // adjust the bottom to account for dock
		End if 
		
		//$bottom_i:=$bottom_i+($adjustHeight_i/3)
		//SCREEN COORDINATES($ScreenLeft_i; $ScreenTop_i; $ScreenRight_i; $ScreenBottom_i)
		
		$height_i:=$bottom_i-$top_i
		
		If ($height_i>1000)  // greater than this and the window goes above the toolbar. Need to check Fnd_Wnd_OpenWindow
			$height_i:=1000
		End if 
		
		//SET WINDOW RECT($vL_left;$vL_top;$vL_right;$vL_bottom;Frontmost window)  // 10;50;950;740 ; this size window fits 1024x768 rez
	End if 
	
	
End if   // end Guy's code

Fnd_Wnd_OpenWindow($width_i; $height_i)

Fnd_Menu_DisableAll

DIALOG:C40("Fnd_List_ChoiceList")

CLOSE WINDOW:C154

If (OK=1)
	If (Fnd.List.newButton=1)
		OK:=2
		$0:=-2
	Else 
		$0:=Fnd.List.SelectedElement
	End if 
Else 
	$0:=0
End if 

// Set the value of the OK variable in the host database to match the
//   value of the OK variable in this component.
If (Fnd_Shell_DoesMethodExist("Fnd_Host_ExecuteFormula")=1)
	$ok_i:=OK  // EXECUTE METHOD will change the value of the OK variable.
	$formula_t:="OK:="+String:C10(OK)
	EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula"; *; $formula_t)
	OK:=$ok_i
End if 

ARRAY TEXT:C222(Fnd_List_ChoiceArray1_at; 0)  // Clear the array.