//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Wnd_CloseAllWindows ({wait?}) --> Boolean


// This process tries to tell all of the other processes to close their windows.

// Called as a new process from Fnd_Shell_Quitter, and when the user Option-clicks

//   a window's close box.

// If the wait parameter is passed and is True, this process will wait until all of the

//   windows have been closed, and then return True.  If it takes too long for the windows

//   to close, or the user cancels the closing, then False is returned.

// Do not pass True to this routine from a process that's displaying a window that

//   needs to be closed.  It won't work properly.


// Access Type: Shared


// Parameters:

//   $1 : Boolean : Wait for all windows to close? (optional)


// Returns:

//   $0 : Boolean : Did all windows close?


// Created by Dave Batton on Sep 8, 2003

// ----------------------------------------------------


// ### Does this process need to use a semaphore?


C_BOOLEAN:C305($0;$1;$wait_b;$completed_b)
C_LONGINT:C283($element_i;$processNumber_i;$processState_i;$processTime_i)
C_TEXT:C284($processName_t)

If (Count parameters:C259>=1)
	$wait_b:=$1
Else 
	$wait_b:=False:C215
End if 

Fnd_Wnd_Init

$completed_b:=True:C214

<>Fnd_Wnd_Closing_b:=True:C214

PROCESS PROPERTIES:C336(Current process:C322;$processName_t;$processState_i;$processTime_i)

$processName_t:=$processName_t+":"+String:C10(Current process:C322)

$processNumber_i:=New process:C317("Fnd_Wnd_CloseAllWindows2";Fnd_Gen_DefaultStackSize;"$Fnd_Wnd_CloseAllWindows";*)

If ($wait_b)  // Wait for the new process to finish.
	
	Repeat 
		DELAY PROCESS:C323(Current process:C322;10)
	Until (<>Fnd_Wnd_Closing_b=False:C215)
	
	If (Size of array:C274(<>Fnd_Wnd_CloseProcessNames_at)#0)
		$completed_b:=False:C215
	End if 
End if 

$0:=$completed_b
