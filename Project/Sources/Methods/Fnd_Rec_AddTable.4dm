//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Rec_AddTable (->table{; position})

  // Loads the visible fields for the current form's table
  //   into an array named Fnd_Rec_Fields_aptr.

  // Access Type: Private

  // Parameters: 
  //   $1 : Pointer : A pointer to the table to add
  //   $2 : Longint : The position in which to add the item (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jun 25, 2003
  // Modified by Gary Boudreaux on Dec 8, 2008
  //   Added check for valid field numbers so that deleted fields are excluded
  // ----------------------------------------------------

C_POINTER:C301($1;$field_ptr)
C_LONGINT:C283($2;$tableNumber_i;$fieldNumber_i;$position_i;$fieldType_i;$fieldLength_i)
C_BOOLEAN:C305($fieldIndexed_b;$fieldUnique_b;$fieldInvisible_b)

$field_ptr:=$1

If (Count parameters:C259>=2)
	$position_i:=$2
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Rec_Init 

Case of 
	: ($position_i<1)
		$position_i:=1
	: ($position_i>(Size of array:C274(Fnd_Rec_Fields_aptr)+1))
		$position_i:=Size of array:C274(Fnd_Rec_Fields_aptr)+1
End case 

If (Not:C34(Is nil pointer:C315($field_ptr)))
	$tableNumber_i:=Table:C252($field_ptr)
	
	For ($fieldNumber_i;1;Get last field number:C255($field_ptr))
		If (Is field number valid:C1000($tableNumber_I;$fieldNumber_i))  //GB20081208 - check to exclude deleted fields
			$field_ptr:=Field:C253($tableNumber_i;$fieldNumber_i)
			GET FIELD PROPERTIES:C258($field_ptr;$fieldType_i;$fieldLength_i;$fieldIndexed_b;$fieldUnique_b;$fieldInvisible_b)
			
			If (Not:C34($fieldInvisible_b))
				If (($fieldType_i#Is picture:K8:10) & ($fieldType_i#Is subtable:K8:11) & ($fieldType_i#Is BLOB:K8:12))
					Fnd_Rec_AddItem ($position_i;"";"";$field_ptr)
					$position_i:=$position_i+1
				End if 
			End if 
		End if 
	End for 
End if 