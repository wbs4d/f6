//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_AddSubfield (->subfield; field type{; position})

// Lets the developer add a subfield to the Find dialog.  The field type
//   must be specified because we can't procedurally determine this like we
//   can for fields.  If a position isn't specified, the field is added to the end
//   of the list.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the field to add
//   $2 : Longint : The type of the subfield
//   $3 : Longint : The position in which to add the item (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 20, 2003
// ----------------------------------------------------

C_POINTER:C301($1;$field_ptr)
C_LONGINT:C283($2;$3;$fieldType_i;$position_i)

$field_ptr:=$1
$fieldType_i:=$2

If (Count parameters:C259>=3)
	$position_i:=$3
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Find_AddItem($position_i;"";"";$field_ptr;$fieldType_i)