//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_RecordEdited ({->table; calling process}) --> Longint

  // Call this when a new record has been added.  This routine will take care of
  //   adding it to the output list (if any).
  // This routine can also be called by the output list process to get the number
  //   of the most recently edited record. Once called to get the record number,
  //   it clears it so you won't accidentally get it again.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the record's table
  //   $2 : Longint : The process number of the output form

  // Returns: 
  //   $0 : Longint : The record number that was edited

  // Created by Dave Batton on Aug 28, 2003
  // Modified by Dave Batton on Mar 18, 2004
  //   Added support for the Fnd_Grid component.
  // Modified by Dave Batton on May 23, 2004
  //   This routine now also returns the added record number.
  //   Made the parameters optional so this can be called just to get the number.
  //   Removed the Fnd_Grid specific code.
  //   Changed the method type from Private to Protected.
  //   Changed the method name from Fnd_IO_RecordAdded to Fnd_IO_RecordEdited.
  //      Now it is called for edited records as well as new records.
  // Modified by Dave Batton on Feb 7, 2007
  //   The table number is now passed to the calling process. The added/modified
  //       record number is now returned only if it is a record from the current table.
  //       This way we won't try loading a record for the wrong table.
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($0;$2;$returnValue_i)

Fnd_IO_Init 

$returnValue_i:=No current record:K29:2

If (Count parameters:C259>=2)
	$table_ptr:=$1
	Fnd_IO_CallingProcessNumber_i:=$2  // We can't use a local variable with the EXECUTE command.
	
	If (Fnd_IO_CallingProcessNumber_i#-1)
		Fnd_IO_AddTableNumber_i:=Table:C252($table_ptr)  // DB070207 - Added
		Fnd_IO_AddRecordNumber_i:=Record number:C243($table_ptr->)
		VARIABLE TO VARIABLE:C635(Fnd_IO_CallingProcessNumber_i;Fnd_IO_AddTableNumber_i;Fnd_IO_AddTableNumber_i;Fnd_IO_AddRecordNumber_i;Fnd_IO_AddRecordNumber_i)  // DB070207 - Modified
		POST OUTSIDE CALL:C329(Fnd_IO_CallingProcessNumber_i)
	End if 
	
Else 
	If (Fnd_IO_AddTableNumber_i=Table:C252(Fnd_Gen_CurrentTable ))  // DB070207 - Added
		$returnValue_i:=Fnd_IO_AddRecordNumber_i
	Else 
		$returnValue_i:=No current record:K29:2  // DB070207  - Added
	End if 
	Fnd_IO_AddTableNumber_i:=0  // DB070207 - Added
	Fnd_IO_AddRecordNumber_i:=No current record:K29:2
End if 

$0:=$returnValue_i
