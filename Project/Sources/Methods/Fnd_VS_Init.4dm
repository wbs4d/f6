//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_VS_Init

// Initializes both the process and interprocess variables used by the Virtual Structure routines.
// Designed to be called at the beginning of any Protected routines to make sure
//   the necessary variables are initialized.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 2, 2003
// Modified by Dave Batton on Jan 24, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
//   Initializes the new table and field arrays.
// Modified by Dave Batton on Sep 16, 2004
//   Updated the version number to 4.0.4.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_VS_Info method.
// Modified by Dave Batton on Feb 13, 2005
//   Added error handling to present a better error message for 4D 2004's strict naming rules.
// Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES & SET FIELD TITLES
// Modification : Vincent TOURNIER(12/9/11) to check for valid last field number
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method


C_BOOLEAN:C305(<>Fnd_VS_Initialized_b)
C_LONGINT:C283($tableNumber_i; $tableElement_i)  // Modification : Vincent TOURNIER(9/12/11)added$tableElement_i

If (Not:C34(<>Fnd_VS_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_VS
	
	<>Fnd_VS_Semaphore_t:="$Fnd_VS_Semaphore"
	
	ARRAY TEXT:C222(<>Fnd_VS_TableNames_at; 0)
	ARRAY INTEGER:C220(<>Fnd_VS_TableNumbers_ai; 0)
	ARRAY TEXT:C222(<>Fnd_VS_FieldNames_at; 0; 0)
	ARRAY INTEGER:C220(<>Fnd_VS_FieldNumbers_ai; 0; 0)
	
	// BEGIN Modification : Vincent TOURNIER(9/12/11)
	ARRAY TEXT:C222(<>Fnd_VS_HostFieldNames_at; 0)
	ARRAY TEXT:C222(<>Fnd_VS_ExistFieldNames_at; 0)
	ARRAY TEXT:C222(<>Fnd_VS_TmpFieldsNames_at; 0)
	// END Modification : Vincent TOURNIER(9/12/11)
	
	Fnd_VS_OldErrorHandler_t:=Method called on error:C704  // DB050213
	ON ERR CALL:C155("Fnd_VS_HandleError")  // DB050213
	
	For ($tableNumber_i; 1; Get last table number:C254)
		If (Is table number valid:C999($tableNumber_i))
			If (Fnd_VS_NbrValidFields($tableNumber_i)>0)  // Modification : Vincent TOURNIER(9/12/11)
				Fnd_VS_AddTable(Table:C252($tableNumber_i))
				Fnd_VS_CurrentTable_t:=Table name:C256($tableNumber_i)  // Used by the error handler.
				$tableElement_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai; $tableNumber_i)  // Modification : Vincent TOURNIER(9/12/11)
				SET FIELD TITLES:C602((Table:C252($tableNumber_i))->; <>Fnd_VS_FieldNames_at{$tableElement_i}; <>Fnd_VS_FieldNumbers_ai{$tableElement_i}; *)  // WN100211-added*
			End if 
		End if 
	End for 
	
	Fnd_VS_CurrentTable_t:=""
	SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at; <>Fnd_VS_TableNumbers_ai; *)  // WN100211-added*
	ON ERR CALL:C155(Fnd_VS_OldErrorHandler_t)  // DB050213
	
	<>Fnd_VS_Initialized_b:=True:C214
End if 