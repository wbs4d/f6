//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_Load (->blob or path)

  // Loads a dictionary from either a blob or a file

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : Dictionary or path

  // Returns: 
  //   $0 : Longint : Dictionary ID

  // Created by Rob Laveaux
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($0;$dictionary_i;$index_i;$count_i;$type_i)
C_POINTER:C301($1;$source_ptr)
C_TEXT:C284($rootNode_t;$itemNode_t;$key_tNode_t;$type_iNode_t;$value_tNode_t)
C_TEXT:C284($tagName_t;$version_t;$name_t;$key_t;$value_t)

$source_ptr:=$1

  // Parse the XML data
Case of 
	: (Type:C295($source_ptr->)=Is BLOB:K8:12)
		$rootNode_t:=DOM Parse XML variable:C720($source_ptr->;False:C215)
	: (Type:C295($source_ptr->)=Is text:K8:3)
		$rootNode_t:=DOM Parse XML source:C719($source_ptr->;False:C215)
	Else 
		OK:=0
End case 

  // If parsed successfully
If (OK=1)
	
	  // Get some info from the root element
	DOM GET XML ELEMENT NAME:C730($rootNode_t;$tagName_t)
	DOM GET XML ATTRIBUTE BY NAME:C728($rootNode_t;"version";$version_t)
	DOM GET XML ATTRIBUTE BY NAME:C728($rootNode_t;"name";$name_t)
	
	  // Check the tag name of the root element and the version to see if it looks valid
	If (($tagName_t="dictionary") & ($version_t="1.0"))
		
		  // Everything looks valid, so let's create the dictionary
		$dictionary_i:=Fnd_Dict_New ($name_t)
		
		  // Get the number of item nodes
		$count_i:=DOM Count XML elements:C726($rootNode_t;"item")
		
		For ($index_i;1;$count_i)
			
			  // Get the key, type and value
			$itemNode_t:=DOM Get XML element:C725($rootNode_t;"item";$index_i)
			$key_tNode_t:=DOM Get XML element:C725($itemNode_t;"key";1;$key_t)
			$type_iNode_t:=DOM Get XML element:C725($itemNode_t;"type";1;$type_i)
			$value_tNode_t:=DOM Get XML element:C725($itemNode_t;"value";1;$value_t)
			
			  // If we have a key and value, add it to the dictionary
			If (($key_t#"") & ($value_t#""))
				Fnd_Dict_SetValue ($dictionary_i;$key_t;$value_t;$type_i)
			End if 
			
		End for 
		
	End if 
	
	DOM CLOSE XML:C722($rootNode_t)
	
End if 

$0:=$dictionary_i