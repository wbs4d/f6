//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_DemoDialog ({feature code})

  // Displays the demo dialog if the feature is not registered.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Feature code (optional)

  // Returns: Nothing

  // Created by Dave Batton on Aug 3, 2003
  // Should not use Fnd_Host_ExecuteFormula since the form Fnd_Reg_DemoDialog
  // in the component now - fix by - Barclay Barry on Nov 18, 2009
  // ----------------------------------------------------

C_TEXT:C284($1;$formula_t)
C_LONGINT:C283($windowType_i;$ok_i;$width_i;$height_i)
C_POINTER:C301($nil_ptr)

Fnd_Reg_Init 

  // Currently the feature code is ignored. I'll get back to this in a future version.
If (Count parameters:C259>=1)
	Fnd_Reg_DialogFeatureCode_t:=$1
	If (Fnd_Reg_DialogFeatureCode_t="")
		Fnd_Reg_DialogFeatureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	Fnd_Reg_DialogFeatureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

Fnd_Gen_MenuBar 

If (Fnd_Reg_RegistrationState (Fnd_Reg_DialogFeatureCode_t)#Fnd_Reg_Registered)
	
	  // Get the window type so we can use it again if we re-open the demo window in this loop.
	$windowType_i:=Fnd_Wnd_Type 
	If ($windowType_i=-1)  // If this is -1, the developer didn't try to set custom value.
		$windowType_i:=Modal form dialog box:K39:7
	End if 
	
	  // If the Shell component is available, call the Fnd_Hook_Shell_Setup routine so the
	  //   database name is available. This is required if Fnd_Reg_Startup is called before
	  //   Fnd_Shell_OnStartup in the On Startup database method.
	If (Fnd_Gen_GetDatabaseInfo ("DatabaseName")="")
		If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_Shell_Setup")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_Shell_Setup";*)
		End if 
	End if 
	
	<>Fnd_Reg_DoneWithDemoDialog_b:=False:C215
	While (Not:C34(<>Fnd_Reg_DoneWithDemoDialog_b))
		Fnd_Wnd_Type ($windowType_i)
		
		  // old code wwn Nov 18, 2009
		  //Fnd_Wnd_OpenFormWindow ($nil_ptr;"Fnd_Reg_DemoDialog")
		  //$formula_t:=Command name(40)+"(\"Fnd_Reg_DemoDialog\")"  ` 40 = DIALOG
		  //EXECUTE METHOD("Fnd_Host_ExecuteFormula";*;$formula_t)
		  //EXECUTE METHOD("Fnd_Host_GetIntegerValue";$ok_i;"OK")
		
		  // new code Nov 18, 2009
		FORM GET PROPERTIES:C674("Fnd_Reg_DemoDialog";$width_i;$height_i)
		Fnd_Wnd_OpenWindow ($width_i;$height_i)
		DIALOG:C40("Fnd_Reg_DemoDialog")
		CLOSE WINDOW:C154
		
		$ok_i:=<>Fnd_Reg_OKButton_i
		  // end new code Nov 18, 2009
		
		If ($ok_i=1)  // Continue was clicked.
			<>Fnd_Reg_DoneWithDemoDialog_b:=True:C214
		Else   // Register was clicked.
			Fnd_Reg_RegistrationDialog (Fnd_Reg_DialogFeatureCode_t)
		End if 
	End while 
End if 
