//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Object_Draw (object number; variation; left offset; top offset)

  // Draws a toolbar object in the specified position. Doesn't actually do any
  //   drawing. It just moves a picture button into the right location and makes
  //   it visible.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The object number
  //   $2 : Text : The object's variation
  //   $3 : Longint : The left offset
  //   $4 : Longint : The top offset

  // Returns: Nothing

  // Created by Dave Batton on Feb 15, 2004
  //   Based on the Mark Mitchenall's toolbar drawing routines.
  // Modified by Dave Batton on May 10, 2005
  //   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
  // Modified by Dave Batton on Nov 18, 2005
  //   Added support for the new variation parameter.
  // Modified by Walt Nelson (9/10/09) to fix problem of passing an array of pointers to host variables in compiled apps
  //[1] Added by: John Craig (8/17/09)
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($1;$3;$4;$objectNumber_i;$leftOffset_i;$topOffset_i;$width_i;$height_i;$iconWidth_i)
C_TEXT:C284($2;$variation_t;$objectName_t)
C_PICTURE:C286($picture)

$objectNumber_i:=$1
$variation_t:=$2
$leftOffset_i:=$3
$topOffset_i:=$4

If ($objectNumber_i<=Size of array:C274(Fnd_Tlbr_HostPictButtons_aptr))
	picture_ptr:=Get pointer:C304("Fnd_Tlbr_ButtonImage"+String:C10($objectNumber_i)+"_pic")  // wwn 9/14/09 was $picture_ptr
	PICTURE PROPERTIES:C457(picture_ptr->;$width_i;$height_i)  // wwn 9/14/09 was $picture_ptr
	$height_i:=$height_i/4  // Because we have 4 images for a rollover button.
	
	OBJECT SET VISIBLE:C603(Fnd_Tlbr_HostPictButtons_aptr{$objectNumber_i}->;True:C214)
	OBJECT MOVE:C664(Fnd_Tlbr_HostPictButtons_aptr{$objectNumber_i}->;$leftOffset_i;$topOffset_i;$leftOffset_i+$width_i;$topOffset_i+$height_i;*)
	
	  // We need to set the host database's picture variable.
	If ($objectNumber_i<=Size of array:C274(Fnd_Tlbr_HostPictureVars_aptr))
		If (Not:C34(Is nil pointer:C315(Fnd_Tlbr_HostPictureVars_aptr{$objectNumber_i})))
			
			  //EXECUTE METHOD("Fnd_HOST_Tlbr_SetBtnPict";*;$objectNumber_i;$picture_ptr->)  `[1] Added by: John Craig (8/17/09) 
			  // wwn (9/14/09) when host is compiled and component is intrepreted $picture_ptr-> is gibberish
			
			$picture:=picture_ptr->  // wwn let's try dereferencing the pointer before passing it as parameter
			EXECUTE METHOD:C1007("Fnd_HOST_Tlbr_SetBtnPict";*;$objectNumber_i;$picture)  //[1] Added by: John Craig (8/17/09) wwn 9/14/09 was $picture_ptr
			
			  //Fnd_Tlbr_HostPictureVars_aptr{$objectNumber_i}->:=$picture_ptr->
		End if 
	End if 
	
	If (Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i})
		  //The button is enabled  
		  //OBJECT SET ENABLED(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
		OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";True:C214)
		
		  // DB051107 - If it's a menu button, put an invisible button on top of the normal button.
		  // Also put a menu indicator beside the button.
		If ($variation_t="menu")
			$objectName_t:="Fnd_Tlbr_InvisibleButton"+String:C10($objectNumber_i)+"_i"
			OBJECT SET VISIBLE:C603(*;$objectName_t;True:C214)
			OBJECT MOVE:C664(*;$objectName_t;$leftOffset_i;$topOffset_i;$leftOffset_i+$width_i;$topOffset_i+$height_i;*)
			OBJECT SET ENABLED:C1123(*;$objectName_t;True:C214)
			
			  //$iconWidth_i:=Fnd_Bttn_GetPictureWidth (Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i};"")  ` Since we use a blank label, we get just the icon width.
			  //$objectName_t:="Fnd_Tlbr_MenuIndicator"+String($objectNumber_i)
			  //SET VISIBLE(*;$objectName_t;True)
			  //$left_i:=$leftOffset_i+Round(($width_i+$iconWidth_i)/2;0)+2+Fnd_Tlbr_MenuIndicatorHOffset_i
			  //$top_i:=$topOffset_i+Fnd_Tlbr_ButtonTopOffset_i+Fnd_Tlbr_MenuIndicatorVOffset_i
			  //MOVE OBJECT(*;$objectName_t;$left_i;$top_i;$left_i+7;$top_i+4;*)  ` 7 and 4 are the width and height of the indicator graphic.
		End if 
		
		If (Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i}#"")
			$objectName_t:="Fnd_Tlbr_Shortcut_"+Fnd_Tlbr_ShortcutKeyName_Get (Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i})
			  //SET VISIBLE(*;$objectName_t;True)
			OBJECT MOVE:C664(*;$objectName_t;$leftOffset_i;$topOffset_i;$leftOffset_i+1;$topOffset_i+1;*)
			OBJECT SET ENABLED:C1123(*;$objectName_t;True:C214)
		End if 
		
	Else 
		  //The button is disabled, so disabled the picture button and hide the shortcut 
		  //button if there is one.  
		  //OBJECT SET ENABLED(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
		OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";False:C215)
		If (Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i}#"")
			$objectName_t:="Fnd_Tlbr_Shortcut_"+Fnd_Tlbr_ShortcutKeyName_Get (Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i})
			OBJECT MOVE:C664(*;$objectName_t;-1;-1;-1;-1;*)
			OBJECT SET ENABLED:C1123(*;$objectName_t;False:C215)
		End if 
	End if 
	
	  //If (Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i})
	  //Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->:=1
	  //Else 
	  //Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->:=0
	  //End if 
	
	  //Fnd_Tlbr_Item_ToolTip_Draw ($objectNumber_i)
End if 