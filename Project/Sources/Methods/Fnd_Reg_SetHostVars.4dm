//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_SetHostVars (->text; ->text; ->text; ->text; ->longint; ->longint; ->longint)

  // Called by the Buy Now button method of the Fnd_Reg_DemoDialog form to
  //   pass some pointers to host variables to the component.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : Host feature name
  //   $2 : Pointer : Host message 1
  //   $3 : Pointer : Host message 2
  //   $4 : Pointer : Host count down
  //   $5 : Pointer : Host OK button
  //   $6 : Pointer : Host Register button
  //   $7 : Pointer : Host Buy Now button

  // Returns: Nothing

  // Created by Dave Batton on Oct 8, 2007
  // Modified by Gary Boudreaux on Dec 22, 2008
  //   Added parameter descriptions and added 7th parameter to line 1 in header info
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$3;$4;$5;$6;$7)

Fnd_Reg_Init 

<>Fnd_Reg_HostFeatureName_ptr:=$1
<>Fnd_Reg_HostMessage1_ptr:=$2
<>Fnd_Reg_HostMessage2_ptr:=$3
<>Fnd_Reg_HostCountDown_ptr:=$4
<>Fnd_Reg_HostOKButton_ptr:=$5
<>Fnd_Reg_HostRegisterButton_ptr:=$6
<>Fnd_Reg_HostBuyNowButton_ptr:=$7
