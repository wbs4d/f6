//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_SetHostVars (->pointer array; ->pointer array; ->text)

  // Called by the first toolbar button method of the Fnd_Tlbr_Toolbar form to
  //   pass some pointers to host variables to the component.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : Host picture variables
  //   $2 : Pointer : Host picture buttons
  //   $3 : Pointer : Host info variables

  // Returns: Nothing

  // Created by Dave Batton on Oct 8, 2007
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Enhanced parameter description in header
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$3)

Fnd_Tlbr_Init 

COPY ARRAY:C226($1->;Fnd_Tlbr_HostPictureVars_aptr)
COPY ARRAY:C226($2->;Fnd_Tlbr_HostPictButtons_aptr)
Fnd_Tlbr_HostInfoVar_ptr:=$3
