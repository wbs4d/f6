//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_RegG_GenerateUnlockCode (secret key; feature code; user name; {license info{; format}}) --> Text

  // Returns a registration/unlock code based on the product number and user name.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The unique product secret key
  //   $2 : Text : The feature code
  //   $3 : Text : The user name
  //   $4 : Text : The license information (optional)
  //   $5 : Text : The unlock code format (optional)

  // Returns: 
  //   $0 : Text : The unlock code

  // Created by Dave Batton on Dec 15, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$3;$4;$5;$secretKey_t;$featureCode_t;$personalization_t;$licenseInfo_t;$format_t;$unlockCode_t)

$secretKey_t:=$1
$featureCode_t:=$2
$personalization_t:=$3
If (Count parameters:C259>=4)
	$licenseInfo_t:=$4
Else 
	$licenseInfo_t:=""
End if 

Fnd_RegG_Init 

If (Count parameters:C259>=5)
	$format_t:=$5
Else 
	$format_t:=<>Fnd_RegG_UnlockCodeFormat_t
End if 

If ($featureCode_t="")
	$featureCode_t:=<>Fnd_RegG_DefaultFeatureCode_t
End if 

If ($format_t="")
	$format_t:=<>Fnd_RegG_UnlockCodeFormat_t
End if 

$unlockCode_t:=""

If (($secretKey_t#"") & ($personalization_t#""))
	$unlockCode_t:=$secretKey_t+"::"+$featureCode_t+"::"+$personalization_t+"::"+$licenseInfo_t
	$unlockCode_t:=Fnd_Text_TextToMD5 ($unlockCode_t)
	$unlockCode_t:=Fnd_RegG_Format ($unlockCode_t;$format_t)
	$unlockCode_t:=Uppercase:C13($unlockCode_t)
	
	If ($licenseInfo_t#"")
		$unlockCode_t:=$unlockCode_t+"/"+$licenseInfo_t
	End if 
End if 

$0:=$unlockCode_t
