//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_FormMethod

// 

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 21, 2005
// ----------------------------------------------------

var $1 : Integer
var $headerName_t; $ColumnName_t; $headerLabel_t; $columnLabel_t; $formula_t; $columnObjectName_t; $headerObjectName_t; $listBoxName_t; $baseLabel_t : Text
var $column_i; $formEvent_i; $maxColumns_i; $bestWidth_i; $bestHeight_i; $NumberofColumns_i; $CurrentColumn_i : Integer
var $headerVariable_ptr; $Nil_ptr : Pointer
var $columnDefinition_o; $Config_o; $thisColumn_o : Object
var $Columns_c : Collection

If (Count parameters:C259=1)
	$formEvent_i:=$1
Else 
	$formEvent_i:=Form event code:C388
End if 

$listBoxName_t:="Fnd_Out_ListBox"
$maxColumns_i:=Fnd.out.lbox.columns.length+1

Case of 
	: ($formEvent_i=On Load:K2:1)
		
		OBJECT SET VISIBLE:C603(*; $listBoxName_t; False:C215)
		
		For each ($columnDefinition_o; Fnd.out.lbox.columns)
			$baseLabel_t:=$columnDefinition_o.fo_field
			If ($columnDefinition_o.fo_field="Formula:@")
				$formula_t:=Replace string:C233($columnDefinition_o.fo_field; "Formula: "; "")
			Else 
				$formula_t:="This."+$baseLabel_t
			End if 
			
			$columnLabel_t:=$baseLabel_t+"_Col"
			$headerLabel_t:=$baseLabel_t+"_Hdr"
			// Replace "." with "_" in case we need to store in an object
			$columnLabel_t:=Replace string:C233($columnLabel_t; "."; "_")
			$headerLabel_t:=Replace string:C233($headerLabel_t; "."; "_")
			
			LISTBOX INSERT COLUMN FORMULA:C970(*; $listBoxName_t; \
				$maxColumns_i; $columnLabel_t; $formula_t; $columnDefinition_o.fo_type; \
				$headerLabel_t; $Nil_ptr)
			OBJECT SET TITLE:C194(*; $headerLabel_t; $columnDefinition_o.fo_title)
			
			OBJECT SET FONT:C164(*; $headerLabel_t; Fnd.out.headerFontName)
			OBJECT SET FONT SIZE:C165(*; $headerLabel_t; Fnd.out.headerFontSize)
			OBJECT SET FONT STYLE:C166(*; $headerLabel_t; Plain:K14:1)
			
			OBJECT SET FONT:C164(*; $columnLabel_t; Fnd.out.columnFontName)
			OBJECT SET FONT SIZE:C165(*; $columnLabel_t; Fnd.out.columnFontSize)
			OBJECT SET FONT STYLE:C166(*; $columnLabel_t; Plain:K14:1)
			
			// What is the "best" size for the column header?
			OBJECT GET BEST SIZE:C717(*; $headerLabel_t; $bestWidth_i; $bestHeight_i)
			
			// Default the column width to 50 if not specified
			If ($columnDefinition_o.fo_width=Null:C1517)
				$columnDefinition_o.fo_width:=50
			End if 
			
			// Default the min column width to "best" size plus 40 for sort arrow (if not specified)
			If ($columnDefinition_o.fo_minWidth=Null:C1517)
				$columnDefinition_o.fo_minWidth:=$bestWidth_i+40
			End if 
			
			// Ensure the min column width is not greater than the specified width
			If ($columnDefinition_o.fo_width<$columnDefinition_o.fo_minWidth)
				$columnDefinition_o.fo_minWidth:=$columnDefinition_o.fo_width
			End if 
			
			// Default the max column width 200 if not specified
			If ($columnDefinition_o.fo_maxWidth=Null:C1517)
				$columnDefinition_o.fo_maxWidth:=200
			End if 
			
			// Ensure the max column width is not less than the specified width
			If ($columnDefinition_o.fo_width>$columnDefinition_o.fo_maxWidth)
				$columnDefinition_o.fo_maxWidth:=$columnDefinition_o.fo_width
			End if 
			
			// Set all these if the appropriate value is present
			OBJECT SET HORIZONTAL ALIGNMENT:C706(*; $columnLabel_t; Choose:C955($columnDefinition_o.fo_HAlign#Null:C1517; $columnDefinition_o.fo_HAlign; Align left:K42:2))
			
			OBJECT SET VISIBLE:C603(*; $columnLabel_t; Choose:C955($columnDefinition_o.fo_visible#Null:C1517; $columnDefinition_o.fo_visible; True:C214))
			OBJECT SET FONT STYLE:C166(*; $columnLabel_t; Choose:C955($columnDefinition_o.fo_style#Null:C1517; $columnDefinition_o.fo_style; Plain:K14:1))
			OBJECT SET ENTERABLE:C238(*; $columnLabel_t; False:C215)
			
			LISTBOX SET PROPERTY:C1440(*; $columnLabel_t; lk truncate:K53:37; lk without ellipsis:K53:64)  //truncate without using "..."
			
			// Now apply formatting, which is a little more complicated
			If ($columnDefinition_o.fo_format#Null:C1517)
				If (($columnDefinition_o.fo_type=Is date:K8:7) | ($columnDefinition_o.fo_type=Is time:K8:8))
					OBJECT SET FORMAT:C236(*; $columnLabel_t; Char:C90($columnDefinition_o.fo_format))
				Else 
					OBJECT SET FORMAT:C236(*; $columnLabel_t; $columnDefinition_o.fo_format)
				End if 
			End if 
			
		End for each 
		
	: ($formEvent_i=(0-On Load:K2:1))  // Set the widths after the form has loaded
		
		// First of all the default settings
		For each ($columnDefinition_o; Fnd.out.lbox.columns)
			$baseLabel_t:=$columnDefinition_o.fo_field
			$columnLabel_t:=$baseLabel_t+"_Col"
			LISTBOX SET COLUMN WIDTH:C833(*; $columnLabel_t; \
				Num:C11($columnDefinition_o.fo_width); \
				Num:C11($columnDefinition_o.fo_minWidth); \
				Num:C11($columnDefinition_o.fo_maxWidth))
		End for each 
		
		
		// Move the listbox to fit under the toolbar
		OBJECT GET COORDINATES:C663(*; $listBoxName_t; $listBoxLeft_i; $listBoxTop_i; $listBoxRight_i; $listBoxBottom_i)
		
		If (Fnd_Tlbr_Style_t="small@")
			$listBoxTop_i:=32+20  // Toolbar height + status bar
		Else 
			$listBoxTop_i:=57+20  // Toolbar height + status bar
		End if 
		
		OBJECT SET COORDINATES:C1248(*; $listBoxName_t; $listBoxLeft_i; $listBoxTop_i; $listBoxRight_i; $listBoxBottom_i)
		
		
		If (Macintosh option down:C545) & (Shift down:C543)  // Reset to default by holding option/shift down
		Else 
			If (Form:C1466.listBoxConfiguration#Null:C1517)  // It shouldn't be possible to be null
				$Config_o:=Form:C1466.listBoxConfiguration
				If (Not:C34(OB Is empty:C1297($Config_o)))
					$Columns_c:=$Config_o.columns
					For each ($thisColumn_o; $Columns_c)
						$ColumnName_t:=$thisColumn_o.columnName
						LISTBOX MOVE COLUMN:C1274(*; $thisColumn_o.columnName; $thisColumn_o.columnNumber)
						LISTBOX SET COLUMN WIDTH:C833(*; $thisColumn_o.columnName; $thisColumn_o.columnWidth)
					End for each 
				End if 
			End if 
		End if 
		
		OBJECT SET VISIBLE:C603(*; $listBoxName_t; True:C214)
		
		Fnd_Gen_SelectionChanged
		
	: ($formEvent_i=On Unload:K2:2)  // Save the column sizes
		Fnd_FCS_Trace
		
		ARRAY BOOLEAN:C223($visibleColumns_ab; 0)
		ARRAY POINTER:C280($columnVars_aptr; 0)
		ARRAY POINTER:C280($headerVars_aptr; 0)
		ARRAY POINTER:C280($styles_aptr; 0)
		ARRAY TEXT:C222($columnNames_at; 0)
		ARRAY TEXT:C222($headerNames_at; 0)
		
		LISTBOX GET ARRAYS:C832(*; $listBoxName_t; $columnNames_at; $headerNames_at; \
			$columnVars_aptr; $headerVars_aptr; \
			$visibleColumns_ab; $styles_aptr; \
			$FooterNames_at; $FooterVars_aptr)
		
		$numberOfColumns_i:=Size of array:C274($columnNames_at)
		
		$Columns_c:=New collection:C1472
		
		For ($CurrentColumn_i; 1; $NumberofColumns_i)
			$thisColumn_o:=New object:C1471
			$thisColumn_o.columnNumber:=$CurrentColumn_i
			$thisColumn_o.columnName:=$columnNames_at{$CurrentColumn_i}
			$thisColumn_o.columnWidth:=LISTBOX Get column width:C834(*; $thisColumn_o.columnName)
			$headerName_t:=$headerNames_at{$CurrentColumn_i}
			$thisColumn_o.headerTitle:=OBJECT Get title:C1068(*; $headerName_t)
			
			$Columns_c.push($thisColumn_o)
			
		End for 
		
		Form:C1466.listBoxConfiguration.columns:=$Columns_c
		
		
		
		
		//: ($formEvent_i=On Timer)  // See Fnd_Out_Update.
		//If (Fnd_Out_LoadArrays_b)
		//Fnd_Out_LoadArrays
		//Fnd_Out_UpdateAutoSizeColumns
		//Fnd_Out_UpdateToolbar
		//SET TIMER(0)
		//Fnd_Out_LoadArrays_b:=False
		//End if 
		
		
	: ($formEvent_i=On Outside Call:K2:11)
		Case of 
			: ((Fnd_Wnd_CloseNow) | (Fnd_Gen_QuitNow))
				CANCEL:C270
				
			Else 
				// If we add a new record, another process may want us to add the
				//   new record to the end of the current selection.
				Fnd_Rec_AddToSelection(Fnd_IO_RecordEdited)
		End case 
		
		
	: ($formEvent_i=On Double Clicked:K2:5)
		
		
	: ($formEvent_i=On Activate:K2:9)
		Fnd_Gen_MenuBar  // DB040522 - Not necessary.
		
		
	: ($formEvent_i=On Close Box:K2:21)
		CANCEL:C270
		If (Macintosh option down:C545)
			Fnd_Wnd_CloseAllWindows
		End if 
End case 


