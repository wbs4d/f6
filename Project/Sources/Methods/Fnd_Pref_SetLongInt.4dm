//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetLongInt (name; value{; scope})

// Saves a long integer value in the user's preferences file.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Longint : The long integer value to save
//   $3 : Longint : How to save it (optional)

// Returns: Nothing

// Created by Dave Batton on Oct 30, 2003
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
//   Added the scope parameter.
// Modified by Wayne Stewart on 2023-07-27:
//   Replaced the Interprocess "constants" with actual constants 
// ----------------------------------------------------

C_TEXT:C284($1; $itemName_t; $itemValue_t)
C_LONGINT:C283($2; $3; $itemValue_i; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_i:=$2
If (Count parameters:C259>=3)
	$itemScope_i:=$3
Else 
	$itemScope_i:=Fnd_Pref_Local
End if 

Fnd_Pref_Init

If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))  // DB060323 - Changed the timeout to a variable.
	$itemValue_t:=String:C10($itemValue_i)
	Fnd_Pref_SetValue($itemName_t; Is longint:K8:6; $itemValue_t; $itemScope_i)
	CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
End if 
