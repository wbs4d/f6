//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetLongInt (name{; default{; scope}}) --> Longint

// Retrieves a long integer value in from user's preferences file.  If an item 
//   with the specified name isn't found, the default value is returned (if specified).
//   Otherwise, 0 is returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Longint : The default value (optional)
//   $3 : Longint : The scope if the default is used (optional)

// Returns: 
//   $0 : Longint : The saved value

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
//   Added the scope parameter.
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
//   Now calls the corresponding Fnd_Pref_SetLongint routine to handle default value
// ----------------------------------------------------

C_LONGINT:C283($0; $2; $3; $itemValue_i; $itemScope_i; $element_i)
C_TEXT:C284($1; $itemName_t)

$itemName_t:=$1
$itemValue_i:=0
$itemScope_i:=Fnd_Pref_Local

If (Count parameters:C259>=2)
	$itemValue_i:=$2
	If (Count parameters:C259>=3)
		$itemScope_i:=$3
	End if 
End if 
Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $itemName_t)

Case of 
	: ($element_i>0)
		If (<>Fnd_Pref_Types_ai{$element_i}=Is longint:K8:6)
			$itemValue_i:=Num:C11(<>Fnd_Pref_Values_at{$element_i})
		End if 
		
	: (Fnd_Pref_GetSharedValue($itemName_t; ->$itemValue_i))  // We found it in the shared prefs.
		
	: (Count parameters:C259>=2)  // Only if they specified a default value.
		Fnd_Pref_SetLongInt($itemName_t; $itemValue_i; $itemScope_i)
End case 

$0:=$itemValue_i
