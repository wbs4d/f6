//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_DeleteThese

// Method to stash variables and methods that can (or should) be deleted
// but have not been deleted yet pending code review

// Access: Private


// Created by Wayne Stewart (2021-07-26)

//     waynestewart@mac.com
// ----------------------------------------------------

C_LONGINT:C283(<>Fnd_Art_MinSecondsOpen_i; <>Fnd_Art_SoonestCloseTime_i)

C_BOOLEAN:C305(<>Fnd_Art_CloseRequested_b)


C_BOOLEAN:C305(_DeleteMe)