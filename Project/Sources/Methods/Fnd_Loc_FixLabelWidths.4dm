//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_FixLabelWidths (->form object; ->enterable object{; ..repeat..})

// Used for aligning one or more sets of labels and enterable fields.
// Start by localizing the label text. Then pass a pointer to it and the field
//   next to it to this routine.  This routine will properly size the label for
//   its new text, and move the left side of the associated field so that
//  it remains an equal distance from the label as it appears on the form.
// If you pass multiple label/field sets to this routine, it will adjust them
//   so they all work with the longest label text width.

// Access Type: Shared

// Parameters: 
//   $1, $3, $5, etc. : Pointer : The name of the label to adjust
//   $2, $4, $6, etc. : Pointer : The field to its right

// Returns: Nothing

// Created by Dave Batton on May 23, 2004
// ----------------------------------------------------

C_POINTER:C301(${1})
C_LONGINT:C283($longest_i;$objectNumber_i;$newLeft_i;$newRight_i;$difference_i)
C_LONGINT:C283($width_i;$height_i;$left_i;$top_i;$right_i;$bottom_i)

// Fnd_Loc_Init isn't necessary.

$longest_i:=0
For ($objectNumber_i;1;Count parameters:C259;2)
	OBJECT GET BEST SIZE:C717(${$objectNumber_i}->;$width_i;$height_i)
	If ($width_i>$longest_i)
		$longest_i:=$width_i
	End if 
End for 


OBJECT GET COORDINATES:C663($1->;$left_i;$top_i;$right_i;$bottom_i)
$newRight_i:=$left_i+$longest_i
$difference_i:=$newRight_i-$right_i

For ($objectNumber_i;1;Count parameters:C259;2)
	OBJECT GET COORDINATES:C663(${$objectNumber_i}->;$left_i;$top_i;$right_i;$bottom_i)
	OBJECT MOVE:C664(${$objectNumber_i}->;$left_i;$top_i;$newRight_i;$bottom_i;*)  // Apply the new right.
End for 


OBJECT GET COORDINATES:C663($2->;$left_i;$top_i;$right_i;$bottom_i)
$newLeft_i:=$left_i+$difference_i

For ($objectNumber_i;2;Count parameters:C259;2)
	OBJECT GET COORDINATES:C663(${$objectNumber_i}->;$left_i;$top_i;$right_i;$bottom_i)
	OBJECT MOVE:C664(${$objectNumber_i}->;$newLeft_i;$top_i;$right_i;$bottom_i;*)
End for 
