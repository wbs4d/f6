//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_BuyNowButton

  // Gets called from the Buy Now buttons.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Mar 6, 2005
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Clicked:K2:4)
		OPEN URL:C673(<>Fnd_Reg_BuyNowURL_t;*)
End case 