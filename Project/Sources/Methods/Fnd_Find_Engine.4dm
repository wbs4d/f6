//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Find_Engine ("Query" or "Query Selection")


  // Does the grunt work.


  // Parameters: 

  //   $1 : Text : Either "Query" or "Query Selection"


  // Returns: Nothing


  // Created by Dave Batton on Jul 15, 2003

  // ----------------------------------------------------


C_TEXT:C284($1;$searchScope_t;$operator_t;$method_t;$label_t;$search_t)
C_POINTER:C301($field_ptr)
C_LONGINT:C283($fieldType_t;$radioButton_i;$i)

$searchScope_t:=$1

$operator_t:=Fnd_Find_OperatorCodes_at{Fnd_Find_OperatorStrs_at}
$method_t:=Fnd_Find_MethodNames_at{Fnd_Find_FieldNames_at}

If ($method_t#"")
	$label_t:=Fnd_Find_FieldNames_at{Fnd_Find_FieldNames_at}
	$search_t:=Replace string:C233(Fnd_Find_SearchString_t;"\"";"\"+"+Command name:C538(90)+"(34)+\"")  // Replace quotes with something we can pass.  90=Char.

	$radioButton_i:=Fnd_Find_AllRadioButton_i+(Fnd_Find_SelectionRadioButton_i*2)+(Fnd_Find_AddRadioButton_i*3)+(Fnd_Find_OmitRadioButton_i*4)
	  // execute the method with these parameters: (label; operator; search string; radio button)

	$method_t:=$method_t+"(\""+$label_t+"\";\""+$operator_t+"\";\""+$search_t+"\";"+String:C10($radioButton_i)+")"
	EXECUTE FORMULA:C63($method_t)
	
Else 
	$field_ptr:=Fnd_Find_Fields_aptr{Fnd_Find_FieldNames_at}
	$fieldType_t:=Fnd_Find_FieldTypes_ai{Fnd_Find_FieldNames_at}
	
	If ($fieldType_t=Pointer array:K8:23)
		Fnd_Find_Engine2 ($searchScope_t;$field_ptr->{1};$operator_t)
		For ($i;2;Size of array:C274($field_ptr->))
			Fnd_Find_Engine3 ($searchScope_t;$field_ptr->{$i};$operator_t)
		End for 
	Else 
		Fnd_Find_Engine2 ($searchScope_t;$field_ptr;$operator_t)
	End if 
	
End if 