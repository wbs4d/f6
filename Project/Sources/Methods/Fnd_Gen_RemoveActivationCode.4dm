//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_RemoveActivationCode

// Removes the activation code from the database so the developer can share
//   the source code with the Foundation components installed.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Mar 14, 2003
// ----------------------------------------------------

C_TEXT:C284($userName_t;$activationCode_t)

$activationCode_t:=Fnd_Gen_Reg_GetActivationCode

Case of 
	: ($activationCode_t="")
		ALERT:C41("This database has not been activated.")
		
	Else 
		Fnd_Gen_Reg_SaveRegInfo(Fnd_Gen_Reg_GetUserName;"")  // We leave the developer's name.
		ALERT:C41("The activation code has been removed.")
End case 
