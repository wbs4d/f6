//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Confirm2 (message{; icon{; OK bttn; Cancel bttn{; window title}}})

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The message to display
  //   $2 : Longint : The icon - 0, 1 or 2 (optional)
  //   $3 : Text : The OK button text (optional)
  //   $4 : Text : The Cancel button text (optional)
  //   $5 : Text : The window title (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 13, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$3;$4;$5;$bttn1_t;$bttn2_t)
C_LONGINT:C283($2)

Fnd_Cmpt_Init 

Fnd_Dlg_SetText ($1)

Case of 
	: (Count parameters:C259<2)
	: ($2=0)  // Stop icon
		Fnd_Dlg_SetIcon (3)
	: ($2=1)  // Warn icon
		Fnd_Dlg_SetIcon (2)
	: ($2=2)  // Note icon
		Fnd_Dlg_SetIcon (1)
End case 

$bttn1_t:="*"
$bttn2_t:="*"
If (Count parameters:C259>=4)
	If ($3#"")
		$bttn1_t:=$3
	End if 
	If ($4#"")
		$bttn2_t:=$4
	End if 
End if 
Fnd_Dlg_SetButtons ($bttn1_t;$bttn2_t)

If (Count parameters:C259>=5)
	Fnd_Wnd_Title ($5)
End if 

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))
	Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)
End if 

BEEP:C151

Fnd_Dlg_Display 

Fnd_Cmpt_SetBooleanValue ("gCenterWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gUpperWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gSafetyOn";False:C215)
