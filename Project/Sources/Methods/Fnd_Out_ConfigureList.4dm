//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_ConfigureList (font{; size{; row height}})

// 

// Access Type: Shared

// Parameters: 
//   $1 : Text : 
//   $2 : Longint : 
//   $3 : Longint : 

// Returns: Nothing

// Created by Dave Batton on Sep 8, 2005
// ----------------------------------------------------

C_TEXT:C284($1)
C_LONGINT:C283($2;$3)

Fnd_Out_Init

If ($1#"")
	Fnd.out.columnFontName:=$1
End if 

If (Count parameters:C259>=2)
	If ($2#0)
		Fnd.out.columnFontSize:=$2
	End if 
	If (Count parameters:C259>=3)
		Fnd.out.columnRowHeight:=$3
	End if 
End if 
