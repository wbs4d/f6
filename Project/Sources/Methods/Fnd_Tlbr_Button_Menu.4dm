//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Menu (button name; ->menu items array)

// Adds a menu to the specified button.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The button name
//   $2 : Pointer : Array of menu items

// Returns: Nothing

// Created by Dave Batton on Nov 7, 2005
// ----------------------------------------------------

C_TEXT:C284($1;$buttonName_t)
C_POINTER:C301($2;$menuItems_ptr)
C_LONGINT:C283($objectNumber_i)

$buttonName_t:=$1
$menuItems_ptr:=$2

Fnd_Tlbr_Init

$objectNumber_i:=Fnd_Tlbr_Object_GetNumber($buttonName_t)

If ($objectNumber_i>0)  // Don't worry if we don't find it. There are reasons it may not exist.
	
	COPY ARRAY:C226($menuItems_ptr->;Fnd_Tlbr_ObjectMenuItems_at{$objectNumber_i})
	
End if 