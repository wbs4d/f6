//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_GetUserName ({feature code}) --> Text

  // Returns the user name that has been saved in the registration record (if any).

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Feature name (optional)

  // Returns: 
  //   $0 : Text : The registered user's name

  // Created by Dave Batton on Apr 27, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$featureCode_t)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

Fnd_Reg_LoadRegistrationRecord ($featureCode_t)

$0:=<>Fnd_Reg_UserNameFld_ptr->
