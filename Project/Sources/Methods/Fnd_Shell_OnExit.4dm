//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_OnExit

  // Called from the shell's On Exit database method.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 17, 2003
  // ----------------------------------------------------

C_LONGINT:C283($timeout_i)

If (Application type:C494=4D Server:K5:6)
	Fnd_List_OnExit   // Save any lists to the data file so we can restore them later.
	
Else 
	Fnd_List_OnExit   // Save any lists to the data file so we can restore them later.
	
	  // If we're in this method because we called Fnd_Gen_QuitNow, calling it again,
	  //   shouldn't be a problem.  If we quit some other way, this is a good thing to run
	  //   to properly close all of the windows and end any processes.
	Fnd_Gen_QuitNow (True:C214)
	
	$timeout_i:=Tickcount:C458+(30*60)  // 30 second timout.
	
	Repeat 
		IDLE:C311
	Until ((Not:C34(Fnd_Gen_QuitNow )) | (Tickcount:C458>$timeout_i))
End if 