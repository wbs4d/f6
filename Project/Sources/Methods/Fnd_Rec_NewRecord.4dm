//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_NewRecord ({->table})

// Creates a new record for the current table.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the table to use (optional)

// Returns: Nothing

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Jan 21, 2004
//   This method now calls the Fnd_Hook_Rec_New as documented.
// Modified by Dave Batton on Jan 23, 2004
//   Rather than just calling Fnd_Hook_Rec_New, we now use the result.
// ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_BOOLEAN:C305($allowNew_b)

If (Count parameters:C259>=1)
	$table_ptr:=$1
Else 
	$table_ptr:=Fnd_Gen_CurrentTable
End if 

//If (Fnd_Gen_ComponentAvailable("Fnd_IO"))
$allowNew_b:=True:C214
If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Rec_New")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Rec_New";$allowNew_b;$table_ptr)
End if 

If ($allowNew_b)
	Fnd_IO_DisplayRecord($table_ptr;New record:K29:1)
	//EXECUTE METHOD("Fnd_IO_DisplayRecord"; *; $table_ptr; New record)
End if 

//Else 
//ADD RECORD($table_ptr->)
//End if 