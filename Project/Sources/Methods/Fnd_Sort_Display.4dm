//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Sort_Display ({->table})

// Displays the simple Sort dialog.  If the Macintosh Option key or Windows Alt
//   key is down, 4D's Order By editor is displayed instead.

// Access: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the table to use (optional)

// Returns: Nothing

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Mar 30, 2004
//   Now uses the optional parameter the documentation says it uses.
//   No longer does anything (including cause errors) if the pointer is nil.
// Modified by Dave Batton on May 30, 2004
//   Changed the Fnd_Loc_GetString calls to Fnd_Gen_GetString calls.
// Modified by Dave Batton on Nov 9, 2004
//   The Fnd_VS component is now optional.
// Modified by Dave Batton on Nov 19, 2004
//   Fixed a problem (4D bug I think) that causes 4D to crash when sorting on a text field.
//Modified by Mike Erickson on Oct 4 2005
//   Added a hook for ASG's QueryPack component. Fixed the dumb parameter declaration problem.
// Modified by Dave Batton on Mar 3, 2006
//   Added support for the Fnd_Out component.
// Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
// Modified by: Walt W Nelson (5/30/17) - to fix memory allocation errors when sorting on Text fields - Dave Nasralla
// Mod by Wayne Stewart, 2018-04-16 - Removed calls to components that no longer exist
// ----------------------------------------------------

C_POINTER:C301($1)
C_LONGINT:C283($fieldType_i; $width_i; $height_i)
C_BOOLEAN:C305($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)

Fnd_Sort_Init

If (Count parameters:C259>=1)
	Fnd_Sort_Table_ptr:=$1  // DB040330
End if 

If (Not:C34(Is nil pointer:C315(Fnd_Sort_Table_ptr)))  // DB040330
	
	// Add the default search fields if the developer didn't add any.
	If (Size of array:C274(Fnd_Sort_Fields_aptr)=0)
		Fnd_Sort_AddTable(Fnd_Sort_Table_ptr)  // Load arrays with field pointers and names.
	End if 
	
	// DB041108 - Get the table name.
	Fnd_Sort_TableName_t:=Fnd_VS_TableName(Fnd_Sort_Table_ptr)
	//If (Fnd_Gen_ComponentAvailable("Fnd_VS"))
	//EXECUTE METHOD("Fnd_VS_TableName"; Fnd_Sort_TableName_t; Fnd_Sort_Table_ptr)
	//Else 
	//Fnd_Sort_TableName_t:=Table name(Fnd_Sort_Table_ptr)
	//End if 
	
	Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)  // Sheet or centered over window.
	Fnd_Wnd_Type(Movable form dialog box:K39:8)
	Fnd_Wnd_Title(Fnd_Gen_GetString("Fnd_Sort"; "WindowTitle"))
	FORM GET PROPERTIES:C674("Fnd_Sort_Dialog"; $width_i; $height_i)
	Fnd_Wnd_OpenWindow($width_i; $height_i)
	
	DIALOG:C40("Fnd_Sort_Dialog")
	CLOSE WINDOW:C154
	
	GET AUTOMATIC RELATIONS:C899($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
	
	If (OK=1)
		If (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active")  // DB060303
			EXECUTE METHOD:C1007("Fnd_Out_SyncronizeSelection"; *)  // We need to synchronize first for every action except for a completely new search.
		End if 
		
		SET AUTOMATIC RELATIONS:C310(True:C214; True:C214)
		
		$fieldType_i:=Fnd_Sort_FieldTypes_ai{Fnd_Sort_FieldNames_at}
		If (Fnd_Sort_AscendingButton_i=1)
			// Modified by: Walt W Nelson (5/30/17)
			//If ($fieldType_i=Is text)  // 4D doesn't do sorts on text, but we can.
			//ORDER BY FORMULA(Fnd_Sort_Table_ptr->;Substring(Fnd_Sort_Fields_aptr{Fnd_Sort_FieldNames_at}->;1;80);>)  // DB041119 - Added the Substring call.
			//Else 
			ORDER BY:C49(Fnd_Sort_Table_ptr->; Fnd_Sort_Fields_aptr{Fnd_Sort_FieldNames_at}->; >)
			//End if `Modified by: Walt W Nelson (5/30/17)
		Else 
			//Modified by: Walt W Nelson (5/30/17)
			//If ($fieldType_i=Is text)
			//ORDER BY FORMULA(Fnd_Sort_Table_ptr->;Substring(Fnd_Sort_Fields_aptr{Fnd_Sort_FieldNames_at}->;1;80);<)  // DB041119 - Added the Substring call.
			//Else 
			ORDER BY:C49(Fnd_Sort_Table_ptr->; Fnd_Sort_Fields_aptr{Fnd_Sort_FieldNames_at}->; <)
			//End if ` Modified by: Walt W Nelson (5/30/17)
		End if 
		
		SET AUTOMATIC RELATIONS:C310($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
		
		// Save the settings in the user's preferences file.
		Fnd_Sort_PrefsPut("Fnd_Sort: "+Fnd_Sort_TableName_t+" Sort Field"; Fnd_Sort_FieldNames_at{Fnd_Sort_FieldNames_at})
		Fnd_Sort_PrefsPut("Fnd_Sort: "+Fnd_Sort_TableName_t+" Sort Order"; ("Ascending"*Fnd_Sort_AscendingButton_i)+("Descending"*Fnd_Sort_DescendingButton_i))
		
		// Save this value so the user can retrieve it from Fnd_Sort_SelectedField,
		//   and so we can use it again next time.
		Fnd_Sort_SelectedField_ptr:=Fnd_Sort_Fields_aptr{Fnd_Sort_FieldNames_at}
		Fnd_Sort_Direction_i:=Fnd_Sort_AscendingButton_i-Fnd_Sort_DescendingButton_i  // Cool, eh? Gives us 1 or -1.
	End if 
	
	
	
End if 

// Reset these items so we have a fresh palette for next time.
Fnd_Sort_Table_ptr:=Fnd_Gen_CurrentTable
ARRAY TEXT:C222(Fnd_Sort_FieldNames_at; 0)
ARRAY POINTER:C280(Fnd_Sort_Fields_aptr; 0)
ARRAY INTEGER:C220(Fnd_Sort_FieldTypes_ai; 0)
ARRAY TEXT:C222(Fnd_Sort_MethodNames_at; 0)
