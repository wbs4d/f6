//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_CalendarEventHandler

  // An event handler for the pop-up calendar. Used to close the calendar
  //   if a click happens outside of the calendar palette window.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Mar 27, 2005
  // ----------------------------------------------------

C_LONGINT:C283(MouseDown;$x_i;$y_i;$buttonState_i;$left_i;$top_i;$right_i;$bottom_i)

If (MouseDown=1)
	GET MOUSE:C468($x_i;$y_i;$buttonState_i;*)
	GET WINDOW RECT:C443($left_i;$top_i;$right_i;$bottom_i;<>Fnd_Date_CalendarWindowRef_i)
	
	If (Is macOS:C1572)
		$top_i:=$top_i-15  // Account for the window title (Mac only).
	End if 
	
	If (($x_i<$left_i) | ($x_i>$right_i) | ($y_i<($top_i)) | ($y_i>$bottom_i))
		POST OUTSIDE CALL:C329(Frontmost process:C327)
	End if 
End if 

