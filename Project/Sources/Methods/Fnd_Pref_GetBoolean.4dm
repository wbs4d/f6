//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetBoolean (name{; default{; scope}}) --> Boolean

// Retrieves a Boolean value in from user's preferences file.  If an item with the
//   specified name isn't found, the default value is returned (if specified).
//   Otherwise False is returned.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Boolean : The default value (optional)
//   $3 : Longint : The scope if the default is used (optional)

// Returns: 
//   $0 : Boolean : The saved value

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
//   Added the scope parameter.
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
//   Now calls the corresponding Fnd_Pref_SetBoolean routine to handle default value
// ----------------------------------------------------

C_BOOLEAN:C305($0; $2; $itemValue_b)
C_TEXT:C284($1; $itemName_t; $itemValue_t)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_b:=False:C215
$itemScope_i:=Fnd_Pref_Local


If (Count parameters:C259>=2)
	$itemValue_b:=$2
	If (Count parameters:C259>=3)
		$itemScope_i:=$3
	End if 
End if 
Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $itemName_t)

Case of 
	: ($element_i>0)
		If (<>Fnd_Pref_Types_ai{$element_i}=Is boolean:K8:9)
			$itemValue_b:=(<>Fnd_Pref_Values_at{$element_i}="true")
		End if 
		
	: (Fnd_Pref_GetSharedValue($itemName_t; ->$itemValue_b))  // We found it in the shared prefs.
		
	: (Count parameters:C259>=2)  // Only if they specified a default value.
		Fnd_Pref_SetBoolean($itemName_t; $itemValue_b; $itemScope_i)
		
End case 



$0:=$itemValue_b
