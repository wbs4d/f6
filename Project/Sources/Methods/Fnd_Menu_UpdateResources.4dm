//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_UpdateResources

  // Updates the 'STR#' resources in the structure file used for localizing menu titles.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 2, 2004
  // Modified by Dave Batton on Dec 11, 2005
  //   Removed the 'Open resource' call to fix an error some users were reporting.
  // ----------------------------------------------------

C_TEXT:C284($menuLanguage_t)

  // DB051211 - Removed some code here.

  // Determine how the resources are currently set.
$menuLanguage_t:=Get indexed string:C510(18723;1)  // DB051211 - Removed the third parameter.

If ($menuLanguage_t#Fnd_Loc_LanguageCode )
	ARRAY TEXT:C222($menuTitles_at;7)
	
	$menuTitles_at{1}:=Fnd_Loc_LanguageCode 
	$menuTitles_at{2}:=Fnd_Loc_GetString ("Fnd_Menu";"FileMenu")
	$menuTitles_at{3}:=Fnd_Loc_GetString ("Fnd_Menu";"EditMenu")
	$menuTitles_at{4}:=Fnd_Loc_GetString ("Fnd_Menu";"EnterMenu")
	$menuTitles_at{5}:=Fnd_Loc_GetString ("Fnd_Menu";"SelectMenu")
	$menuTitles_at{6}:=Fnd_Loc_GetString ("Fnd_Menu";"ToolsMenu")
	$menuTitles_at{7}:=Fnd_Loc_GetString ("Fnd_Menu";"WindowMenu")
	  //ARRAY TO STRING LIST($menuTitles_at;18723)
End if 