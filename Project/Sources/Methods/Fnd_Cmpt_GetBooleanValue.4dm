//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Cmpt_GetBooleanValue (name of a Boolean variable) --> Boolean

// Returns the value of a Boolean variable (without actually using
//   the variable in this component).

// Access: Shared

// Parameters: 
//   $1 : Text : The name of a Boolean variable

// Returns: 
//   $0 : Boolean : True if the Boolean is True

// Created by Dave Batton on Feb 22, 2004
// Foundation 6 version by Wayne Stewart on 2021-03-26

// ----------------------------------------------------

C_BOOLEAN:C305($0)
C_TEXT:C284($1; $variableName_t)

$variableName_t:=$1

Fnd_Cmpt_Init

EXECUTE FORMULA:C63("Fnd.Cmpt.Boolean:="+$variableName_t)

$0:=Fnd.Cmpt.Boolean