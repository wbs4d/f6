//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Msg

  // Compiler variables related to the Foundation localization routines.

  // Access: Private

  // Created by Dave Batton on Nov 27, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Msg_Initialized_b)
If (Not:C34(<>Fnd_Msg_Initialized_b))  // So we only do the IP variables once.
	C_TEXT:C284(<>Fnd_Msg_Semaphore_t)
	C_TEXT:C284(<>Fnd_Msg_BackgroundProcessName_t)
	C_TEXT:C284(<>Fnd_Msg_SemaphoreName_t)
	C_TEXT:C284(<>Fnd_Msg_ParamDelimiter_t)
	
	C_LONGINT:C283(<>Fnd_Msg_NextMsgSlot_i)
	C_LONGINT:C283(<>Fnd_Msg_TimeOutTicks_i)
	
	C_BOOLEAN:C305(<>Fnd_Msg_RunBackgroundProcess_b)
	
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgFromProcNums_ai;0)  // The number of the process sending the message.
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgToProcNums_ai;0)  // The number of the process to receive the message.
	ARRAY BOOLEAN:C223(<>Fnd_Msg_MsgActiveFlags_ab;0)  // True until the message has been received.
	ARRAY TEXT:C222(<>Fnd_Msg_MsgTexts_at;0)  // The text message.
	ARRAY PICTURE:C279(<>Fnd_Msg_MsgBlobs_apic;0)  // For holding the optional blob portion of the message.
	ARRAY LONGINT:C221(<>Fnd_Msg_MsgEndTime_ai;0)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_POINTER:C301(Fnd_Msg_BlobToPicture ;$1;$2)
	
	C_TEXT:C284(Fnd_Msg_Broadcast ;$1;$2)
	
	C_TEXT:C284(Fnd_Msg_Get ;$0)
	C_POINTER:C301(Fnd_Msg_Get ;$1)
	
	C_TEXT:C284(Fnd_Msg_GetParameter ;$0;$1)
	C_LONGINT:C283(Fnd_Msg_GetParameter ;$2)
	
	C_TEXT:C284(Fnd_Msg_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Msg_PackParameters ;$0;$1;${2})
	
	C_POINTER:C301(Fnd_Msg_PictureToBlob ;$1;$2)
	
	C_LONGINT:C283(Fnd_Msg_Send ;$1)
	C_TEXT:C284(Fnd_Msg_Send ;$2)
	C_POINTER:C301(Fnd_Msg_Send ;$3)
End if 
