//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_CloseBox ({add close box?}) --> Boolean

// Adds a close box to the upcoming window. Also returns the current setting.
// This method replaces the original Fnd_Wnd_SetCloseBox method, which is now obsolete.

// Access Type: Shared

// Parameters: 
//   $1 : Boolean : Add a close box? (optional)

// Returns: 
//   $0 : Boolean : Current setting

// Created by Dave Batton on Apr 23, 2005
//   $1 : Text : Info desired ("version" or "name" or "position" or "title")
// ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_Wnd_Init

If (Count parameters:C259>=1)
	Fnd_Wnd_CloseBox_b:=$1
End if 

$0:=Fnd_Wnd_CloseBox_b