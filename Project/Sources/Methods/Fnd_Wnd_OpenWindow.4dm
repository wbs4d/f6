//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_OpenWindow (width; height) --> Number
// Alternate parameters: Fnd_Wnd_OpenWindow (left; top; right; bottom)

// Opens a new window centered on the screen or over the frontmost window
//   (depending on the value of Fnd_Wnd_WindowPosition_i).

// Access Type: Shared

// Parameters:
//   $1 : Longint : Width of the new window
//   $2 : Longint : Height of the new window

// or:
//   $1 : Longint : Global left coordinate
//   $2 : Longint : Global top coordinate
//   $3 : Longint : Global right coordinate
//   $4 : Longint : Global bottom coordinate

// Returns:
//   $0 : Longint : The window reference number

// Created by Dave Batton on Jul 14, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Replaced the call to Fnd_Wnd_ResetProcess with just setting the Fnd_Wnd_Initialized_b
//      variable to False.
//   Now we use the window ref of the frontmost window of the current process to center
//      windows over windows, rather than just the frontmost window of any process.
// Modified by Dave Batton on Dec 5, 2004
//   Opening a window from preferences now works properly with a fixed size window.
// Modified by Dave Batton on Dec 11, 2004
//   When opening a window based on a preference position, the window will be stacked
//      if there's already a window in that position.
//   This component should now compile without the presense of the Fnd_Pref component.
// Modified by Dave Batton on Jan 15, 2005
//   If the position has been set to Fnd_Wnd_MacOSXSheet, and we're using 4D 2004, then we'll
//      change the window type to 33, which is 4D 2004's Sheet form window constant value.
//   Now uses a semaphore when accessing the IP arrays.
// Modified by Dave Batton on Apr 23, 2005
//   Now detects if the title, type, or position hasn't been set, and sets default values.
// Modified by Dave Batton on Dec 16, 2005
//   Added some extra protection to make sure a non-modal window isn't displayed over a modal window on Windows.
// Modified by Dave Batton on Jan 2, 2006
//   Added the option to pass l,t,r,b coordinates instead of just height and width.
//   Changed the error handler from Fnd_Gen_DummyMethod to Fnd_Wnd_DummyMethod.
//   Now won't try to set the window position if Fnd_Wnd_Position has been called with an exact position.
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
// Modified by Wayne Stewart on Sep 14, 2006
//   No longer ignores attempts to set the window type to Resizable sheet window.
// Modified by Guy Algot on (07/02/09) - See ` // Guy Algot (07/02/09)
//   Fixes the problem where a remembered window opens smaller than the size of the layout
// Modified by: Walt Nelson (3/2/10) - Moved Guy's code to bottom of Count parameters=2 end-if
// Mod by Wayne Stewart, (2023-07-26) - Remove check Fnd_Gen_ComponentAvailable("Fnd_Pref")
//   and run Fnd_Pref_GetWindow directly rather than via EXECUTE 
// ----------------------------------------------------

C_LONGINT:C283($0; $1; $2; $3; $4; $windowRef_i; $width_i; $height_i; $element_i; $backLeft_i; $backTop_i; $backRight_i; $backBottom_i; $left_i; $top_i; $right_i; $bottom_i)

Case of 
	: (Count parameters:C259=2)
		$width_i:=$1
		$height_i:=$2
		
	: (Count parameters:C259=4)
		Fnd.Wnd.Left:=$1  // DB060102
		Fnd.Wnd.Top:=$2
		Fnd.Wnd.Right:=$3
		Fnd.Wnd.Bottom:=$4
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "The wrong number of parameters has been passed to this method.")
End case 

Fnd_Wnd_Init

// DB050423 - Set the default window type if none has been set.
If (Fnd.Wnd.windowType=-1)
	Fnd.Wnd.windowType:=Plain window:K34:13
End if 

// DB050423 - Set the default window title if none has been set.
If (Fnd_Wnd_WindowTitle_t="*")
	Fnd_Wnd_WindowTitle_t:=""
End if 

If (Count parameters:C259=2)
	// DB050423 - Set the default window position if none has been set.
	If (Fnd_Wnd_WindowPosition_i=-1)
		Fnd_Wnd_WindowPosition_i:=Fnd_Wnd_CenterOnScreen
	End if 
	
	If (Fnd_Wnd_WindowPosition_i#6)  // Fnd_Wnd_Exact = These next four variables have already been set by a call to Fnd_Wnd_Position.
		Fnd.Wnd.Left:=0
		Fnd.Wnd.Top:=0
		Fnd.Wnd.Right:=0
		Fnd.Wnd.Bottom:=0
		
		// If we're supposed to use the previously saved position, try that first.  If no saved
		//   position is available, this routine will leave the position variables at 0.
		If (Fnd_Wnd_PositionPrefName_t#"")
			Fnd_Pref_GetWindow(Fnd_Wnd_PositionPrefName_t; ->$left_i; ->$top_i; ->$right_i; ->$bottom_i)
			Fnd.Wnd.Left:=$left_i
			Fnd.Wnd.Top:=$top_i
			Fnd.Wnd.Right:=$right_i
			Fnd.Wnd.Bottom:=$bottom_i
			
			If ((Fnd.Wnd.Left#0) & (Fnd.Wnd.Top#0))  // Added this test.
				If (Fnd.Wnd.windowType=Plain fixed size window:K34:6)  // If it's a fixed sized window, use the form size, not the memorized size.
					Fnd.Wnd.Right:=Fnd.Wnd.Left+$width_i
					Fnd.Wnd.Bottom:=Fnd.Wnd.Top+$height_i
				End if 
				If ((Fnd_Wnd_WindowPosition_i=Fnd_Wnd_Stacked)\
					 | (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_StackedOnWindow))  // Only do this if the position is set to Stacked.
					$windowRef_i:=Frontmost window:C447
					While ($windowRef_i>0)
						GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i; $windowRef_i)
						If (($left_i=Fnd.Wnd.Left) & ($top_i=Fnd.Wnd.Top))
							Fnd.Wnd.Left:=Fnd.Wnd.Left+<>Fnd_Wnd_StackHOffset_i
							Fnd.Wnd.Top:=Fnd.Wnd.Top+<>Fnd_Wnd_StackVOffset_i
							Fnd.Wnd.Right:=Fnd.Wnd.Right+<>Fnd_Wnd_StackHOffset_i
							Fnd.Wnd.Bottom:=Fnd.Wnd.Bottom+<>Fnd_Wnd_StackVOffset_i
							Fnd_Wnd_MoveOnScreen  // (->Fnd.Wnd.Left;->Fnd.Wnd.Top;->Fnd.Wnd.Right;->Fnd.Wnd.Bottom;Fnd.Wnd.windowType)
							If ((Fnd.Wnd.Left#$left_i) | (Fnd.Wnd.Top#$top_i))  // Prevent endless loops once we're down in the corner of the screen.
								$windowRef_i:=Frontmost window:C447  // Start the check over again.
							Else 
								$windowRef_i:=0
							End if 
						Else 
							$windowRef_i:=Next window:C448($windowRef_i)
						End if 
					End while 
				End if 
			End if 
		End if 
	End if 
	
	Case of 
		: (((Fnd.Wnd.Bottom-Fnd.Wnd.Top)>10) & ((Fnd.Wnd.Right-Fnd.Wnd.Left)>10))
			// The position variables are okay. Don't mess with them.
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_CenterOnScreen)
			Fnd.Wnd.Left:=Int:C8((Screen width:C187-$width_i)/2)
			Fnd.Wnd.Top:=Int:C8((Screen height:C188-$height_i)/3)
			
		: ((Fnd_Wnd_WindowPosition_i=Fnd_Wnd_CenterOnWindow)\
			 | (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_MacOSXSheet))  // Center it over the window.
			$windowRef_i:=Fnd_Wnd_GetProcessWindowRef(Current process:C322)  // DB040320
			If ($windowRef_i=0)
				$windowRef_i:=Frontmost window:C447
			End if 
			GET WINDOW RECT:C443($backLeft_i; $backTop_i; $backRight_i; $backBottom_i; $windowRef_i)
			Fnd.Wnd.Left:=Int:C8((($backRight_i-$backLeft_i-$width_i)\2))+$backLeft_i
			Fnd.Wnd.Top:=Int:C8((($backBottom_i-$backTop_i-$height_i)\3))+$backTop_i
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_Stacked)
			$element_i:=Fnd_Wnd_GetNextStackElement
			If (Not:C34(Semaphore:C143(<>Fnd_Wnd_Semaphore_t; <>Fnd_Wnd_SemaphoreTimeout_i)))  // DB060323 - Now gets the timeout from an IP variable.
				Fnd.Wnd.Left:=<>Fnd_Wnd_StackLeftPositions_ai{$element_i}
				Fnd.Wnd.Top:=<>Fnd_Wnd_StackTopPositions_ai{$element_i}
				CLEAR SEMAPHORE:C144(<>Fnd_Wnd_Semaphore_t)
			Else 
				Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")  // DB060323 - Added
			End if 
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_StackedOnWindow)
			$windowRef_i:=Fnd_Wnd_GetProcessWindowRef(Current process:C322)  // DB040320
			If ($windowRef_i=0)
				$windowRef_i:=Frontmost window:C447
			End if 
			GET WINDOW RECT:C443($backLeft_i; $backTop_i; $backRight_i; $backBottom_i; $windowRef_i)
			Fnd.Wnd.Left:=$backLeft_i+<>Fnd_Wnd_StackHOffset_i
			Fnd.Wnd.Top:=$backTop_i+<>Fnd_Wnd_StackHOffset_i
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_OnTheLeft)
			Fnd.Wnd.Left:=0
			Fnd.Wnd.Top:=Int:C8((Screen height:C188-$height_i)/2)
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_OnTheLeft+Fnd_Wnd_AtTheTop))
			Fnd.Wnd.Left:=0
			Fnd.Wnd.Top:=Menu bar height:C440+Fnd_Wnd_ToolbarHeight
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_OnTheLeft+Fnd_Wnd_AtTheBottom))
			Fnd.Wnd.Left:=0
			Fnd.Wnd.Top:=Screen height:C188-$height_i
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_OnTheRight)
			Fnd.Wnd.Left:=Screen width:C187-$width_i
			Fnd.Wnd.Top:=(Screen height:C188-$height_i)/2
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_OnTheRight+Fnd_Wnd_AtTheTop))
			Fnd.Wnd.Left:=Screen width:C187-$width_i
			Fnd.Wnd.Top:=Int:C8((Screen height:C188-$height_i)/2)
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_OnTheRight+Fnd_Wnd_AtTheBottom))
			Fnd.Wnd.Left:=Screen width:C187-$width_i
			Fnd.Wnd.Top:=Screen height:C188-$height_i
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_AtTheTop)
			Fnd.Wnd.Left:=Int:C8((Screen width:C187-$width_i)/2)
			Fnd.Wnd.Top:=Menu bar height:C440+Fnd_Wnd_ToolbarHeight
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_AtTheBottom)
			Fnd.Wnd.Left:=Int:C8((Screen width:C187-$width_i)/2)
			Fnd.Wnd.Top:=Screen height:C188-$height_i
			
			
			// Title Bar = 57?
			
			//{
			//"Left" : 0,
			//"Top" : 1181,
			//"Right" : 108,
			//"Bottom" : 1440,
			//"windowWidth" : 108,
			//"windowHeight" : 259,
			//"formWidth" : 108,
			//"formHeight" : 259,
			//"screenWidth" : 2560,
			//"screenHeight" : 1440,
			//"menuBar" : 24,
			//"toolbarHeight" : 87
			//}
			
			
			
			
			
			
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_RelativeOnTheLeft)
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_RelativeOnTheLeft+Fnd_Wnd_RelativeAtTheTop))
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_RelativeOnTheLeft+Fnd_Wnd_RelativeAtTheBottom))
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_RelativeOnTheRight)
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_RelativeOnTheRight+Fnd_Wnd_RelativeAtTheTop))
			
		: (Fnd_Wnd_WindowPosition_i=(Fnd_Wnd_RelativeOnTheRight+Fnd_Wnd_RelativeAtTheBottom))
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_RelativeAtTheTop)
			
		: (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_RelativeAtTheBottom)
			
			
			
			
			
	End case 
	
	If ((Fnd.Wnd.Right=0) & (Fnd.Wnd.Bottom=0))
		Fnd.Wnd.Right:=Fnd.Wnd.Left+$width_i
		Fnd.Wnd.Bottom:=Fnd.Wnd.Top+$height_i
	End if 
	// Modified by: Walt Nelson (3/2/10) Moved here to restore window functioning
	// // Guy Algot (07/02/09)
	If ((Fnd.Wnd.Bottom-Fnd.Wnd.Top)<$height_i)
		Fnd.Wnd.Bottom:=Fnd.Wnd.Top+$height_i
	End if 
	
	If ((Fnd.Wnd.Right-Fnd.Wnd.Left)<$width_i)
		Fnd.Wnd.Right:=Fnd.Wnd.Left+$width_i
	End if 
	// // Guy Algot  end (07/02/09)
	
End if 

If (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_MacOSXSheet)  // DB050114 - Should we display a sheet window if possible?
	If (Application version:C493>="0800")  // DB050114 - Are we using 4D 2004?
		If (Fnd.Wnd.windowType#34)  // WBS060419 - Don't make the change if a resizeable sheet window has been specified.
			Fnd.Wnd.windowType:=33  // DB050114 - 33=Sheet form window - 4D 2004.
		End if 
	End if 
End if 

// DB051216 - Make sure we don't open a non-modal window in front of a modal dialog on Windows.
//   The window will actually open behind the frontmost window, causing all kinds of problems.
If (Is Windows:C1573)
	If (Window kind:C445=Modal dialog:K27:2)
		If ((Fnd.Wnd.windowType#Modal dialog box:K34:2)\
			 & (Fnd.Wnd.windowType#Movable dialog box:K34:7))
			Fnd.Wnd.windowType:=Modal dialog box:K34:2
		End if 
	End if 
End if 

Fnd_Wnd_MoveOnScreen  //(->Fnd.Wnd.Left;->Fnd.Wnd.Top;->Fnd.Wnd.Right;->Fnd.Wnd.Bottom;Fnd.Wnd.windowType)

If (Fnd_Wnd_CloseBox_b)
	$windowRef_i:=Open window:C153(Fnd.Wnd.Left; Fnd.Wnd.Top; Fnd.Wnd.Right; Fnd.Wnd.Bottom; Fnd.Wnd.windowType; Fnd_Wnd_WindowTitle_t; "Fnd_Wnd_CloseBoxMethod")
Else 
	$windowRef_i:=Open window:C153(Fnd.Wnd.Left; Fnd.Wnd.Top; Fnd.Wnd.Right; Fnd.Wnd.Bottom; Fnd.Wnd.windowType; Fnd_Wnd_WindowTitle_t)
End if 

If (Fnd_Wnd_WindowPosition_i=Fnd_Wnd_Stacked)
	If (Not:C34(Semaphore:C143(<>Fnd_Wnd_Semaphore_t; <>Fnd_Wnd_SemaphoreTimeout_i)))  // DB060323 - Now gets the timeout from an IP variable.
		<>Fnd_Wnd_StackWndRefNos_ai{$element_i}:=$windowRef_i
		CLEAR SEMAPHORE:C144(<>Fnd_Wnd_Semaphore_t)
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")  // DB060323 - Added
	End if 
End if 

Fnd_Wnd_Initialized_b:=False:C215  // DB040205 - So the variables get reset to their default values.

$0:=$windowRef_i