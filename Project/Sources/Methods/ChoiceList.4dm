//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: ChoiceList (dialog title; ->text array) --> Number

  // Reroutes ChoiceList calls to the new Fnd_List component.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The dialog title
  //   $2 : Pointer : The options to display

  // Returns: 
  //   $0 : Longint : The selected element

  // Created by Dave Batton on Mar 4, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$4;$position_i)
C_TEXT:C284($1;$3;$dialogTitle_t;$windowTitle_t)
C_POINTER:C301($2;$array_ptr)

$dialogTitle_t:=$1
$array_ptr:=$2
  // We ignore the Help parameter for now.
  // We ignore the Icon parameter.

Fnd_Cmpt_Init 

$position_i:=Position:C15("|";$dialogTitle_t)
If ($position_i=0)
	$windowTitle_t:=Substring:C12($dialogTitle_t;1;($position_i-1))
	Fnd_Wnd_Title ($windowTitle_t)
	$dialogTitle_t:=Substring:C12($dialogTitle_t;($position_i+1))
End if 

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))
	Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)
End if 

$0:=Fnd_List_ChoiceList ($dialogTitle_t;$array_ptr)
