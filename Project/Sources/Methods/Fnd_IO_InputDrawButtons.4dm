//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_InputDrawButtons

  // Called from Fnd_IO_InputFormMethod when the window is first
  //   open to draw the buttons from the inherited form.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 20, 2004
  // Modified by Dave Batton on Jan 6, 2005
  //   Changed <>Fnd_IO_DisplayNavButtons_b to Fnd_IO_DisplayNavButtons_b.
  // Modified: [3] John Craig, 02/07/2014, 14:09:00 Changed the button height to match standard user interface guidelines
  // ----------------------------------------------------

C_LONGINT:C283($bttnLeft_i;$bttnTop_i;$bttnRight_i;$bttnBottom_i;$width_i)

  // First, see if the form was inherited. If not, we don't need to do anything.
OBJECT GET COORDINATES:C663(*;"Fnd_IO_OKButton_i";$bttnLeft_i;$bttnTop_i;$bttnRight_i;$bttnBottom_i)

$width_i:=$bttnRight_i-$bttnLeft_i
If (($width_i>0) & ($width_i<60))  // Don't do this if the form wasn't inherited or we've already modified them.
	  // Localize the OK and Cancel buttons.
	  // First we have to give them a new minumim size, since they're way too small.
	
	  // Modified: [3] John Craig, 02/07/2014, 14:09:00 Changed the button height to match standard user interface guidelines -->
	  //OBJECT MOVE(*;"Fnd_IO_OKButton_i";0;0;69;20;*)
	  //OBJECT MOVE(*;"Fnd_IO_CancelButton_i";0;0;69;20;*)
	OBJECT MOVE:C664(*;"Fnd_IO_OKButton_i";0;0;69;22;*)
	OBJECT MOVE:C664(*;"Fnd_IO_CancelButton_i";0;0;69;22;*)
	  // Modified: [3] John Craig, 02/07/2014, 14:09:00 Changed the button height to match standard user interface guidelines <--
	
	Fnd_Gen_ButtonText (->Fnd_IO_OKButton_i;Fnd_Loc_GetString ("Fnd_IO";"SaveButton");Align right:K42:4)
	Fnd_Gen_ButtonText (->Fnd_IO_CancelButton_i;Fnd_Loc_GetString ("Fnd_IO";"CancelButton");Align right:K42:4)
	
	  // Move the invisible buttons offscreen. These buttons trap
	  //   the Esc key and Command-W.
	OBJECT MOVE:C664(*;"Fnd_IO_OffscreenButton@";-2;-2;-1;-1;*)
	
	If (Not:C34(Fnd_IO_DisplayNavButtons_b))  // DB040909, DB050106
		  // Hide the navigation buttons if we're not using them.
		OBJECT SET VISIBLE:C603(*;"Fnd_IO_FirstButton_i";False:C215)
		OBJECT SET VISIBLE:C603(*;"Fnd_IO_PrevButton_i";False:C215)
		OBJECT SET VISIBLE:C603(*;"Fnd_IO_NextButton_i";False:C215)
		OBJECT SET VISIBLE:C603(*;"Fnd_IO_LastButton_i";False:C215)
	End if 
End if 