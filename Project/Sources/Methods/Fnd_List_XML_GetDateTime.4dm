//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_XML_GetDateTime --> Text

  // Returns the date-time value stored in the Fnd_List.xml file.

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Text : The date-time value

  // Created by Dave Batton on Oct 5, 2007
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($0;$dateTime_t;$productName_t;$Path_t)
C_TEXT:C284($xmlRootRef_t;$dateTimeRef_t)

$dateTime_t:=""  // Default return value.


  // Modified by: Wayne Stewart (Sep 24, 2009)
  //  Checked for the existence of the Fnd_List.xml document prior to reading it
$Path_t:=Fnd_List_XML_FilePath 

If (Test path name:C476($Path_t)=Is a document:K24:1)
	$xmlRootRef_t:=DOM Parse XML source:C719($Path_t)
	$dateTimeRef_t:=DOM Find XML element:C864($xmlRootRef_t;"last_exit/date_time")
	
	If (OK=1)  // A match was found.
		DOM GET XML ELEMENT VALUE:C731($dateTimeRef_t;$dateTime_t)
	End if 
	
End if 
$0:=$dateTime_t
