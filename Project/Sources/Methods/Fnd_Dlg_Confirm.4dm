//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dlg_Confirm (message{; OK bttn{; Cancel bttn}}) --> Longint

// A replacement for 4D's CONFIRM command.

// Access: Shared

// Parameters: 
//   $1 : Text : The message to display
//   $2 : Text : The OK button text (optional)
//   $3 : Text : The Cancel button text (optional)

// Returns: 
//   $0 : Longint : 1 if OK is clicked, 0 if Cancel is clicked

// Created by Dave Batton on Jul 13, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Removed the redundant call to Fnd_Dlg_Reset.
// Modified by Dave Batton on May 15, 2005
//   Removed the setting of the icon. It will now default to the icon set in Fnd_Dlg_Init.
// ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1; $2; $3)

Fnd_Dlg_SetText($1; Fnd_Dlg_Text2_t)  // So we don't change Fnd_Dlg_Text2_t if it was previously set by calling Fnd_Dlg_SetText.

Case of 
	: (Count parameters:C259=1)
		Fnd_Dlg_SetButtons("*"; "*")
	: (Count parameters:C259=2)
		Fnd_Dlg_SetButtons($2; "*")
	: (Count parameters:C259>=3)
		Fnd_Dlg_SetButtons($2; $3)
End case 

$0:=Fnd_Dlg_Display