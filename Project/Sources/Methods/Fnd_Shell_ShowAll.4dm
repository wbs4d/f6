//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_ShowAll

// Called from the Show All menu bar item or toolbar item.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on 21 Jul 2008
// ----------------------------------------------------

Case of 
		//: (Fnd_Gen_ComponentAvailable("Fnd_Rec"))
		//EXECUTE METHOD("Fnd_Rec_ShowAll"; *)
		
	: (Macintosh option down:C545)
		REDUCE SELECTION:C351(Fnd_Gen_CurrentTable->; 0)
		Fnd_Gen_SelectionChanged
		
	Else 
		Fnd_Rec_ShowAll
		//ALL RECORDS(Fnd_Gen_CurrentTable->)
		//Fnd_Gen_SelectionChanged
End case 