//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_PlugInAvailable (plug-in name) --> Boolean

// Returns true if the plug-in is installed. This is done by executing a valid
//   call to the plug-in, then checking to see if 4D set the Error variable to
//   something other than zero.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the plug-in

// Returns: 
//   $0 : Boolean : True if the plug-in is available

// Created by Dave Batton on Jul 27, 2003
// Modified by Dave Batton on Jan 30, 2005
//  This routine no longer changes the value of 4D's Error variable.
// Modified by Dave Batton on Feb 7, 2006
//   Now checks specifically for version 4.2 of the Foundation Extras component.
// ----------------------------------------------------

// ### - This routine still needs support for non-English versions of the plug-ins.

C_BOOLEAN:C305($0;$available_b)
C_TEXT:C284($1;$plugIn_t;$errorMethod_t;$code_t)
C_LONGINT:C283(Error;$oldErrorValue_i)

$plugIn_t:=$1

Fnd_Gen_Init

$available_b:=False:C215

$oldErrorValue_i:=Error  // DB050130
$errorMethod_t:=Method called on error:C704
ON ERR CALL:C155("Fnd_Gen_DummyMethod")
Error:=0

Case of 
		// Modified by: Walt Nelson (11/8/10) Extras has been replaced with 4D code
		//: ($plugIn_t="Foundation Extras")
		//  `$code_t:="Fnd_Gen_Result_i:=Get String Width (\"Test\";\"Helvetica\";12;0)"
		//$code_t:="Fnd_Gen_Result_i:=Fnd_Gen_ExtrasVersion "  ` Just get the value of a constant in the plug-in. DB060207: Updated to look at a new constant.
		//EXECUTE FORMULA($code_t)  ` If the plug-in isn't there, 4D will set the Error variable.
		//If (Fnd_Gen_Result_i<Num(Fnd_Gen_Info ("minimum_extras_version")))
		//Error:=1  ` An older version is installed.
		//End if 
		
	: ($plugIn_t="TextProperties")  // Not currently documented, but it works.
		$code_t:="Fnd_Gen_Result_i:=TP_TextProperties (\"Test\";Fnd_Gen_Result_i;Fnd_Gen_Result_i;\""+"Helvetica\";12;0)"
		EXECUTE FORMULA:C63($code_t)  // If the plug-in isn't there, 4D will set the Error variable.
		
	: ($plugIn_t="ToolBoxPack")
		$code_t:="Fnd_Gen_Result_i:=tp_EqualString (\"a\";\"b\";1;1)"
		EXECUTE FORMULA:C63($code_t)  // If the plug-in isn't there, 4D will set the Error variable.
		
	: ($plugIn_t="4D View")
		$code_t:="Fnd_Gen_Result_i:=PV Index to color (10)"
		EXECUTE FORMULA:C63($code_t)  // If the plug-in isn't there, 4D will set the Error variable.
		
	: ($plugIn_t="4D Write")
		$code_t:="Fnd_Gen_Result_t:=WR Error text (-43)"
		EXECUTE FORMULA:C63($code_t)  // If the plug-in isn't there, 4D will set the Error variable.
		
	: ($plugIn_t="4D Draw")
		$code_t:="Fnd_Gen_Result_i:=DR Index to color (10)"
		EXECUTE FORMULA:C63($code_t)  // If the plug-in isn't there, 4D will set the Error variable.
End case 

$available_b:=(Error=0)

// Restore the previous error value and error handler.
ON ERR CALL:C155($errorMethod_t)
Error:=$oldErrorValue_i  // DB050130

$0:=$available_b