//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_CreateNameLists

  // Creates the 4D lists that can be used by the virtual structure routines.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 7, 2004
  // Modified by: Walt Nelson (12/08/09) cannot use LIST TO ARRAY from component
  // Modified by : Vincent TOURNIER(12/9/11) to use interprocess arrays instead of local arrays
  // ----------------------------------------------------

C_LONGINT:C283($tableNumber_i;$fieldNumber_i;$fieldCount_i;$existingList_i)
C_TEXT:C284($languageCode_t;$listName_t)
ARRAY TEXT:C222(<>Fnd_VS_ExistFieldNames_at;0)  // Modified by : Vincent TOURNIER(9/12/11)

CONFIRM:C162("This method will generate lists that can be used by the "+Fnd_VS_Info ("name")+" component. One list will be created for each table in the database. Existing lists will not be replaced.";"Continue";"Cancel")

If (OK=1)
	$languageCode_t:=Request:C163("Enter the language code for the new lists:";"XX")
	
	If (OK=1)
		For ($tableNumber_i;1;Get last table number:C254)
			If (Is table number valid:C999($tableNumber_i))
				$listName_t:="VS_Table"+String:C10($tableNumber_i;"000")+"_"+$languageCode_t
				$fieldCount_i:=Get last field number:C255($tableNumber_i)
				
				  // LIST TO ARRAY($listName_t;◊Fnd_VS_ExistFieldNames_at)
				  // cannot use this command in a component to get host Lists-(12/8/9)wwn
				<>Fnd_VS_HostArray_ptr:=-><>Fnd_VS_ExistFieldNames_at
				
				EXECUTE METHOD:C1007("Fnd_Host_ListToArray";*;$listName_t;<>Fnd_VS_HostArray_ptr)
				
				ARRAY TEXT:C222(<>Fnd_VS_ExistFieldNames_at;$fieldCount_i+3)  // Resize it to avoid errors below.
				
				  // TABLEAU TEXTE($fieldNames_at;$fieldCount_i+3)
				ARRAY TEXT:C222(<>Fnd_VS_TmpFieldsNames_at;$fieldCount_i+3)
				<>Fnd_VS_TmpFieldsNames_at{1}:="  ` Table Name"
				If (<>Fnd_VS_ExistFieldNames_at{2}#"")
					<>Fnd_VS_TmpFieldsNames_at{2}:=<>Fnd_VS_ExistFieldNames_at{2}
				Else 
					<>Fnd_VS_TmpFieldsNames_at{2}:=Table name:C256($tableNumber_i)
				End if 
				<>Fnd_VS_TmpFieldsNames_at{3}:="  ` Field Names"
				
				For ($fieldNumber_i;1;$fieldCount_i)
					Case of 
						: (Not:C34(Is field number valid:C1000($tableNumber_i;$fieldNumber_i)))
							<>Fnd_VS_TmpFieldsNames_at{$fieldNumber_i+3}:=" -- Deleted Field --"
						: (<>Fnd_VS_ExistFieldNames_at{$fieldNumber_i+3}#"")
							<>Fnd_VS_TmpFieldsNames_at{$fieldNumber_i+3}:=<>Fnd_VS_ExistFieldNames_at{$fieldNumber_i+3}
						Else 
							<>Fnd_VS_TmpFieldsNames_at{$fieldNumber_i+3}:=Field name:C257($tableNumber_i;$fieldNumber_i)
					End case 
				End for 
				
				  // ARRAY TO LIST($fieldNames_at;$listName_t)  ` Cannot call from component in v11 - (12/08/09) wwn
				  // Use Fnd_Host_ArrayToList to execute ARRAY TO LIST
				<>Fnd_VS_HostArray_ptr:=-><>Fnd_VS_TmpFieldsNames_at
				
				EXECUTE METHOD:C1007("Fnd_Host_ArrayToList";*;<>Fnd_VS_HostArray_ptr;$listName_t)
				
				Fnd_List_ListToRecord ($listName_t)  // Save the list to a record(7/1/09)wwn
			End if 
		End for 
		
		ALERT:C41("The virtual structure lists have been created/updated.")
	End if 
End if 
