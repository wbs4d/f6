//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Msg_Init

// Initializes both the process and interprocess variables used by
//   the interprocess messaging routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 27, 2004
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Msg_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Msg_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Msg
	
	// Initialize the message arrays.
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgFromProcNums_ai; 0)  // The number of the process sending the message.
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgToProcNums_ai; 0)  // The number of the process to receive the message.
	ARRAY BOOLEAN:C223(<>Fnd_Msg_MsgActiveFlags_ab; 0)  // True until the message has been received.
	ARRAY TEXT:C222(<>Fnd_Msg_MsgTexts_at; 0)  // The text message.
	ARRAY PICTURE:C279(<>Fnd_Msg_MsgBlobs_apic; 0)  // For holding the optional blob portion of the message.
	ARRAY LONGINT:C221(<>Fnd_Msg_MsgEndTime_ai; 0)  // When we stop trying to send the message.
	
	<>Fnd_Msg_RunBackgroundProcess_b:=False:C215
	<>Fnd_Msg_BackgroundProcessName_t:="$Fnd_Msg_BackgroundProcess"
	<>Fnd_Msg_SemaphoreName_t:="$Fnd_Msg_Semaphore"
	<>Fnd_Msg_NextMsgSlot_i:=1
	<>Fnd_Msg_ParamDelimiter_t:=" *|* "
	<>Fnd_Msg_TimeOutTicks_i:=30*60  // 30 second time-out
	
	<>Fnd_Msg_Initialized_b:=True:C214
End if 
