//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_Message (message text)

  // Displays a message window.   Stays open until it's called again with a blank string.
  // Designed to be a replacement for 4D's MESSAGE command.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The message to display

  // Returns: Nothing

  // Created by Dave Batton on Jul 25, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$message_t)

$message_t:=$1

Fnd_Dlg_Init 

If ($message_t#"")
	Fnd_Dlg_SetText ($message_t)
	Fnd_Dlg_MessageOpen 
Else 
	Fnd_Dlg_MessageClose 
End if 
