//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_Path --> Text

// Returns the path to the preferences file (or at least where it should be).
// The file name is slightly different for each platform, to conform to the
//   norms for each platform.

// Access Type: Private

// Parameters: None

// Returns: 
//   $0 : Text : The path to the prefs file

// Created by Dave Batton on Sep 16, 2003
//Modified by: Walt Nelson (2/16/10) - added parameter *
// Mod by Wayne Stewart, (2021-04-14) - Rather than recalculate call Fnd_Gen_GetDatabaseInfo
// ----------------------------------------------------

C_TEXT:C284($0; $path_t)

$path_t:=Fnd_Gen_GetDatabaseInfo("PrefsFolder")  // Get the path
$path_t:=$path_t+Fnd_Gen_GetDatabaseInfo("DatabaseName")  // Add the database name

If (Is Windows:C1573)  // Add the preferences extension.
	$path_t:=$path_t+".xml"
	
Else   // Mac OS X
	$path_t:=$path_t+".plist"
	
End if 

$0:=$path_t
