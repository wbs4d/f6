//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_PadSpaces (text; length)

  // Pads the passed text with spaces.  If the length of the text is greater than
  //   the length paramter, it will be truncated.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text to pad
  //   $2 : Longint : The desired length

  // Returns: 
  //   $0 : Text : The modified text

  // Created by Dave Batton on May 21, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Corrected notation of parameter $2 in header
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$text_t)
C_LONGINT:C283($2;$length_t)

$text_t:=$1
$length_t:=$2

$text_t:=$text_t+(" "*($length_t-Length:C16($text_t)))
$text_t:=Substring:C12($text_t;1;$length_t)

$0:=$text_t
