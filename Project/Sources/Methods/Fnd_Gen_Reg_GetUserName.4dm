//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_GetUserName --> Text

  // Returns the name of the registered user.  Returns an empty string
  //   if the program hasn't been registered yet.

  // Method Type: Private

  // Parameters: None

  // Returns: 
  //   $0 : Text : The name of the registered user

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_TEXT:C284($0)

$0:=Fnd_Gen_Reg_GetValueForElement ("user_name")
