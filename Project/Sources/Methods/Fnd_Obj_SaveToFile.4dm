//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: OBJ_IO_SaveToFile (Object; File Path {; Pretty Print?})

  // Takes an object and writes it to disk
  //   as a JSON text document
  // NB. will overwrite existing doc, without checking

  // Access: Shared

  // Parameters:
  //   $1 : Object : Description
  //   $2 : Text : File path
  //   $3 : Boolean {Optional} : True (default) = Use Pretty Print

  // Based on Cannon Smith's Object module
  // ----------------------------------------------------

C_OBJECT:C1216($1)
C_TEXT:C284($2)
C_BOOLEAN:C305($3)

C_BOOLEAN:C305($UsePrettyPrint_b)
C_TEXT:C284($CurrentOnErrMethod_t;$Filepath_t;$JSONString_t)
C_OBJECT:C1216($Object_o)

If (False:C215)
	C_OBJECT:C1216(Fnd_Obj_SaveToFile ;$1)
	C_TEXT:C284(Fnd_Obj_SaveToFile ;$2)
	C_BOOLEAN:C305(Fnd_Obj_SaveToFile ;$3)
End if 


$Object_o:=$1
$Filepath_t:=$2

If (Count parameters:C259>2)
	$UsePrettyPrint_b:=$3
Else 
	$UsePrettyPrint_b:=True:C214
End if 

$CurrentOnErrMethod_t:=Method called on error:C704

If ($UsePrettyPrint_b)
	$JSONString_t:=JSON Stringify:C1217($Object_o;*)
Else 
	$JSONString_t:=JSON Stringify:C1217($Object_o)
End if 

ON ERR CALL:C155("Fnd_Gen_SilentError")

TEXT TO DOCUMENT:C1237($Filepath_t;$JSONString_t;UTF8 text without length:K22:17)

ON ERR CALL:C155($CurrentOnErrMethod_t)  //Restore previous error handler