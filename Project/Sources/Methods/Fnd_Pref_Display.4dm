//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_Display (preferences form name)

// Displays the Preferences window in a new process.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the project form to display

// Returns: Nothing

// Created by Dave Batton on Sep 21, 2003
// Modified by Dave Batton on Jan 16, 2004
//   Moved the CLOSE WINDOW command to after the Fnd_Menu_Window_Remove call.
// Modified by Dave Batton on May 22, 2004
//   Changed the complex call to Fnd_Loc_GetString to a simpler call to Fnd_Gen_GetString.
// Modified by Dave Batton on Dec 5, 2004
//   Moved the call to Fnd_Pref_Init into the new process.
//   Now compiles without the Windows component.
// Modified by Dave Batton on Feb 24, 2006
//   The process name has been changed so that the window is displayed
//     in a server process rather than in a local process.
//   Updated the "Fnd_Wnd_Set@" calls to use the new method names.
// Modified by Dave Batton on Aug 24, 2007
//   Rewritten for 4D v11 SQL.
// Modified by Walt Nelson (2/19/10) fixed when component is compiled and host is intrepreted
// ----------------------------------------------------

C_TEXT:C284($1;$windowTitle_t;$windowPrefName_t;$formula_t)
C_LONGINT:C283($width_i;$height_i)
C_POINTER:C301($nil_ptr)

If (Count parameters:C259>=1)
	<>Fnd_Pref_FormName_t:=$1
End if 

If (Fnd_Gen_LaunchAsNewProcess("Fnd_Pref_Display";"Fnd_Pref: Preferences Window"))
	Fnd_Pref_Init  // DB041205
	
	Fnd_Gen_CurrentFormType(Fnd_Gen_PreferencesForm)
	
	Fnd_Gen_MenuBar  // Install the menu bar for the current process.
	
	// Localize the window title if the Localization component is available.
	$windowTitle_t:=Fnd_Gen_GetString("Fnd_Pref";"PrefsWindowTitle")  // DB040522 - Simplified this code.
	
	$windowPrefName_t:="Fnd_Pref: Preferences Window"
	
	// Modified by: Walt Nelson (2/19/10) - when component is compiled and host is intrepreted ->$width_i & ->$height_i are gibberish
	// Fnd_Pref_width_i & Fnd_Pref_height_i are defined in Compiler_Fnd_Pref
	EXECUTE METHOD:C1007("Fnd_Host_GetFormProperties";*;$nil_ptr;<>Fnd_Pref_FormName_t;->Fnd_Pref_width_i;->Fnd_Pref_height_i)
	$width_i:=Fnd_Pref_width_i
	$height_i:=Fnd_Pref_height_i
	
	Fnd_Wnd_UseSavedPosition($windowPrefName_t)
	Fnd_Wnd_CloseBox(True:C214)
	Fnd_Wnd_Type(Plain fixed size window:K34:6)
	Fnd_Wnd_Title($windowTitle_t)
	Fnd_Wnd_OpenWindow($width_i;$height_i)
	Fnd_Wnd_SendCloseRequests
	Fnd_Menu_Window_Add
	
	//If (Fnd_Gen_ComponentAvailable("Fnd_Wnd"))
	//EXECUTE METHOD("Fnd_Wnd_UseSavedPosition"; *; $windowPrefName_t)
	//EXECUTE METHOD("Fnd_Wnd_CloseBox"; *; True)
	//EXECUTE METHOD("Fnd_Wnd_Type"; *; Plain fixed size window)
	//EXECUTE METHOD("Fnd_Wnd_Title"; *; $windowTitle_t)
	//EXECUTE METHOD("Fnd_Wnd_OpenWindow"; *; $width_i; $height_i)
	//EXECUTE METHOD("Fnd_Wnd_SendCloseRequests"; *)
	//Else 
	//Fnd_Gen_CenterWindow($width_i; $height_i; Plain fixed size window; Fnd_Wnd_CenterOnScreen; $windowTitle_t; True)
	//End if 
	
	//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))
	//EXECUTE METHOD("Fnd_Menu_Window_Add")
	//End if 
	
	$formula_t:=Command name:C538(40)+"(\""+<>Fnd_Pref_FormName_t+"\")"  // 40 = DIALOG
	EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t)
	Fnd_Pref_SetWindow($windowPrefName_t)
	
	Fnd_Menu_Window_Remove
	
	//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))
	//EXECUTE METHOD("Fnd_Menu_Window_Remove")
	//End if 
	
	CLOSE WINDOW:C154  // DB040116 - Moved this so it's called after Fnd_Menu_Window_Remove.
End if 