//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_ShowSet (table Pointer {; Set Name to Use})

// Create and show the proper related set for table $1
// Use the current selection unless $2 is given.

// Access: Shared

// Parameters: 
//   $1 : Pointer : Table pointer
//   $2 : Text : A set name to use

// Created by Doug Hall (20030317)

// ----------------------------------------------------
C_POINTER:C301($1; $RelatedTable_ptr)
C_TEXT:C284($2; $SetToUse_t)
$RelatedTable_ptr:=$1

If (Count parameters:C259>1)
	$SetToUse_t:=$2
	USE SET:C118($SetToUse_t)
	// Generally, things should be destroyed at the same level they're created.
	// However, since we're opening a separate process with a copy of this set, I'm clearing it here.
	CLEAR SET:C117($SetToUse_t)
End if 

If (Records in selection:C76($RelatedTable_ptr->)>0)
	CREATE SET:C116($RelatedTable_ptr->; Fnd_Reltd_SetName($RelatedTable_ptr))
	UNLOAD RECORD:C212($RelatedTable_ptr->)
	Fnd_IO_DisplayTable($RelatedTable_ptr; True:C214)
Else 
	Fnd_Dlg_Alert("There are no related "+Fnd_VS_TableName($RelatedTable_ptr)+" records to display.")
End if 