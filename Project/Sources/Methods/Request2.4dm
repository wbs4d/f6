//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Request2 (message{; default{; icon{; OK bttn; Cancel bttn{; title}}}}) --> Text

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.
  // Part of the Foundation Compatibility component.

  // Parameters: 
  //   $1 : Text : The message to display
  //   $2 : Text : The default response
  //   $3 : Longint : The icon - 0, 1 or 2 (optional)
  //   $4 : Text : The OK button text (optional)
  //   $5 : Text : The Cancel button text (optional)
  //   $6 : Text : The window title (optional)

  // Returns: 
  //   $0 : Text : The user's response

  // Created by Dave Batton on Jul 13, 2003
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$4;$5;$6;$bttn1_t;$bttn2_t)
C_LONGINT:C283($3)

Fnd_Cmpt_Init 

Fnd_Dlg_SetText ($1)

If (Count parameters:C259>=2)
	Fnd_Dlg_SetRequest ($2)
End if 

Case of 
	: (Count parameters:C259<3)
	: ($3=0)  // Stop icon
		Fnd_Dlg_SetIcon (3)
	: ($3=1)  // Warn icon
		Fnd_Dlg_SetIcon (2)
	: ($3=2)  // Note icon
		Fnd_Dlg_SetIcon (1)
End case 

$bttn1_t:="*"
$bttn2_t:="*"
If (Count parameters:C259>=5)
	If ($4#"")
		$bttn1_t:=$4
	End if 
	If ($5#"")
		$bttn2_t:=$5
	End if 
End if 
Fnd_Dlg_SetButtons ($bttn1_t;$bttn2_t)

If (Count parameters:C259>=6)
	Fnd_Wnd_Title ($6)
End if 

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))
	Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)
End if 

Fnd_Dlg_Display 

Fnd_Cmpt_SetBooleanValue ("gCenterWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gUpperWind";False:C215)
Fnd_Cmpt_SetBooleanValue ("gSafetyOn";False:C215)

$0:=Fnd_Dlg_GetRequest 

