//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_ErrorHandler

  // Installed as an error handler by our preferences loading routines. Usually
  //   called because of an XML parsing error.  Displays an alert to the user.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 20, 2003
  // Modified by Dave Batton on May 30, 2004
  //   replaced the Fnd_Loc_GetString call with a Fnd_Gen_GetString call.
  // ----------------------------------------------------

C_TEXT:C284($errorMessage_t)
C_LONGINT:C283($row_i;$column_i;$error_i)

$error_i:=Error  // Displaying the error message may change the Error variable.

Case of 
	: ($error_i=9925)  // Null element error. We get these when we loop through the XML.  
		$error_i:=0  // This isn't a problem, so don't do anything.  But clear the Error variable.
		
	Else 
		XML GET ERROR:C732(Fnd_Pref_XMLElement_t;$errorMessage_t;$row_i;$column_i)
		If ($errorMessage_t#"")
			$errorMessage_t:=$errorMessage_t+". "  // 4D leaves the period off.
		End if 
		$errorMessage_t:=Fnd_Gen_GetString ("Fnd_Pref";"ErrorOpeningPrefsFile";$errorMessage_t)
		Fnd_Pref_Alert ($errorMessage_t+"  ["+String:C10($error_i)+"]")
End case 

Error:=$error_i
OK:=Num:C11(Error=0)  // For our code that checks the OK variable.