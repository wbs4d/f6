//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_ORDARelatedField --> ReturnType

// Returns a pointer 

// Access: Shared

// Parameters: 
//   $1 : Object : Parameter block
//   $1.dataclass : Text : name of dataclass from which the relation starts
//   $1.path : Text : Path to bound attribute

// Returns: 
//   $0 : Pointer : Pointer to attribute field

// Created by Arnaud 17/10/2021 13:24:23
// ----------------------------------------------------

#DECLARE($in_o : Object)->$out_ptr : Pointer

var $path_t; $rel_t; $relatedDataclass_t; $relatedPath_t : Text
var $pos_i; $table_i; $field_i : Integer
var $dc_o : Object
var $relatedEntity_c : Collection

$pos_i:=Position:C15("."; $in_o.path)
If ($pos_i>0)  // search in a linked dataclass
	$rel_t:=Substring:C12($in_o.path; 1; $pos_i-1)
	$dc_o:=ds:C1482[$in_o.dataclass]
	
	ASSERT:C1129(OB Class:C1730($dc_o).superclass.name="DataClass"; Current method name:C684+" $1 not a dataclass")
	
	$relatedEntity_c:=OB Values:C1718($dc_o).query("name = :1"; $rel_t)
	If ($relatedEntity_c.length>0)
		
		$relatedDataclass_t:=$relatedEntity_c[0].relatedDataClass
		$relatedPath_t:=Substring:C12($in_o.path; $pos_i+1)
		$out_ptr:=Fnd_Out_ORDARelatedField(New object:C1471("dataclass"; $relatedDataclass_t; "path"; $relatedPath_t))  // recursive call
	End if 
	
Else   // Search in the current dataclass
	
	$table_i:=ds:C1482[$in_o.dataclass].getInfo().tableNumber
	$field_i:=ds:C1482[$in_o.dataclass][$in_o.path].fieldNumber
	If (Is field number valid:C1000($table_i; $field_i))
		$out_ptr:=Field:C253($table_i; $field_i)
	End if 
End if 

