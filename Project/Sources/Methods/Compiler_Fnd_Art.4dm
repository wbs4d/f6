//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Art

// Compiler variables related to the Foundation Art routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 28, 2003
// Mod by Wayne Stewart, (2021-03-26) - Fnd-6 Update

// ----------------------------------------------------

// Interprocess Variables




// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Fnd_Art_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Art_SetAboutForm; $1; $0)
	
	C_TEXT:C284(Fnd_Art_SetBestFontSize; $1)
	C_LONGINT:C283(Fnd_Art_SetBestFontSize; $2)
	
	C_TEXT:C284(Fnd_Art_SetStartupDialogForm; $1; $0)
End if 