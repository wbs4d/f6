//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_Init

// Initializes both the process and interprocess variables used by the Fnd_Pref routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 25, 2003
// Modified by Dave Batton on Feb 4, 2004
//   Updated to version 4.0.2.
// Modified by Dave Batton on May 30, 2004
//   Updated to version 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Pref_Info method.
// Modified by Dave Batton on Mar 23, 2006
//   Added a semaphore timeout variable.
//   Added support for storing preferences in the data file.
// Mod by Wayne Stewart, 2023-07-26
//   Moved the test for the semaphore to inside the initialisation routine
//   previously the semaphore was set and unset every time Fnd_XXX_Init is called
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_Pref_Initialized_b; Fnd_Pref_Initialized_b)

If (Not:C34(<>Fnd_Pref_Initialized_b))  // So we only do this once per session.
	
	If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
		
		Compiler_Fnd_Pref
		
		// Modified by: Wayne Stewart (27/02/09)
		Fnd_Pref_CreateTable  //  Build the table
		
		// Set up pointers to the structure. The table we use is not installed with the component.
		<>Fnd_Pref_Table_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]")
		<>Fnd_Pref_IDFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]ID"; Is longint:K8:6)
		<>Fnd_Pref_OwnerFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]Owner"; Is alpha field:K8:1; True:C214)
		<>Fnd_Pref_NameFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]Name"; Is alpha field:K8:1; True:C214)
		<>Fnd_Pref_TypeFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]Type"; Is integer:K8:5)
		<>Fnd_Pref_ValueFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Pref"; "[Fnd_Pref]Value"; Is text:K8:3)
		
		Fnd_Pref_Localize
		
		If (<>Fnd_Pref_UserName_t="")  // See Fnd_Pref_UserName.
			<>Fnd_Pref_UserName_t:=Current user:C182
		End if 
		
		// These values are checked in Fnd_Pref_XMLParse.
		<>Fnd_Pref_Left_i:=-999
		<>Fnd_Pref_Top_i:=-999
		<>Fnd_Pref_Bottom_i:=-999
		<>Fnd_Pref_Bottom_i:=-999
		
		ARRAY TEXT:C222(<>Fnd_Pref_Names_at; 0)
		ARRAY INTEGER:C220(<>Fnd_Pref_Scopes_ai; 0)
		ARRAY INTEGER:C220(<>Fnd_Pref_Types_ai; 0)
		ARRAY TEXT:C222(<>Fnd_Pref_Values_at; 0)
		ARRAY BOOLEAN:C223(<>Fnd_Pref_Modified_ab; 0)
		
		<>Fnd_Pref_Initialized_b:=True:C214
		
		// Now load the preferences file from the disk.
		Fnd_Pref_LoadPrefs
		CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
		
	End if 
	
End if 


If (Not:C34(Fnd_Pref_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Pref
	
	Fnd_Pref_Initialized_b:=True:C214
End if 
