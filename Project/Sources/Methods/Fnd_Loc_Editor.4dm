//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_Editor ({number})

// Displays the Localization List Editor window in a new process.

// Access Type: Shared

// Parameters: 
//   $1 : Longint : Used internally by this method

// Returns: Nothing

// Created by Dave Batton on Jun 11, 2004
// Modified by Dave Batton on Nov 23, 2004
//   Now compiles without the Fnd_Shell component.
// ----------------------------------------------------

C_LONGINT:C283($1;$processNumber_i;$width_i;$height_i)
C_TEXT:C284($processName_t)

Fnd_Loc_Init

If (Count parameters:C259>=1)
	//SET MENU BAR(1)
	FORM GET PROPERTIES:C674("Fnd_Loc_Editor";$width_i;$height_i)
	Fnd_Gen_CenterWindow($width_i;$height_i;Plain form window:K39:10;Fnd_Wnd_CenterOnScreen;"Foundation Localization Editor";True:C214)
	DIALOG:C40("Fnd_Loc_Editor")
	CLOSE WINDOW:C154
	
Else 
	If (Count users:C342=1)
		If (Fnd_Loc_Editor_EditorClosed)  // Make sure the List Editor window isn't open.
			$processName_t:="$Localization Editor"
			$processNumber_i:=New process:C317(Current method name:C684;Fnd_Gen_DefaultStackSize;$processName_t;1;*)
			BRING TO FRONT:C326($processNumber_i)
			Fnd_Shell_ExcludeFromQuit($processName_t)
			//If (Fnd_Gen_ComponentAvailable("Fnd_Shell"))
			//EXECUTE METHOD("Fnd_Shell_ExcludeFromQuit"; *; $processName_t)
			//End if 
		End if 
		
	Else 
		ALERT:C41("The Localization Editor cannot be used while other users are online.")
	End if 
End if 
