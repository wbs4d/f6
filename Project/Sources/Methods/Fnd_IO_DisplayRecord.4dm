//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_DisplayRecord ({->table{; record number}{; ShowLockedMessage}})

// Displays the specified record of the table in a new process.  If no table is passed, then the
//   current form's table is used (if available).  If no record number is passed, then the current
//   record will be displayed.  Pass 4D's New record constant to create a new record.

// Access: Shared

// Parameters:
//   $1 : Pointer : Pointer to the record's table (optional)
//   $2 : Longint : Record number (optional)
//   $3 : Boolean : Show Locked Message (optional)

// Returns: Process number of the (new?) process displaying the record.

// Created by Dave Batton on May 2, 2003
//   No longer does anything (including cause errors) if the pointer is nil.
// Modified by Dave Batton on Sep 21, 2004
//   Made a bunch of modifications so we can pass a named selection
//   to the new process that displays the input form.
// Modified by Dave Batton on Jan 6, 2005
//   Changed <>Fnd_IO_DisplayNavButtons_b to Fnd_IO_DisplayNavButtons_b.
//   Changed <>Fnd_IO_MultiWindow_b to a process variable.
// Modified by Dave Batton on Feb 27, 2005
//   We now must always create a set containing the current selection since we don't
//      know whether or not the receiving process will want it.
//   New records are now given a unique name so we can create more than one at a time.
// Modified by Dave Batton on Dec 29, 2005
//   We no longer bother setting up a named selection if the input form is already open.
// [1] Added by: John Craig (12/14/09) - ShowLockedMessage
// Modified by Doug Hall (02/07/2023) to return the process number of the displayed record.
// ----------------------------------------------------
C_LONGINT:C283($0; $processNumber_i)  // JDH 20230207
C_POINTER:C301($1; $table_ptr)
C_LONGINT:C283($2; $recordNumber_i; $timeout_i)
C_BOOLEAN:C305($3)  // [1]
C_TEXT:C284($processName_t)
C_BOOLEAN:C305($ignore_b)
$processNumber_i:=0  // default

Fnd_IO_Init

If (Count parameters:C259>=1)
	$table_ptr:=$1
Else 
	$table_ptr:=Current form table:C627
End if 

If (Not:C34(Is nil pointer:C315($table_ptr)))  // DB040330
	If (Count parameters:C259>=2)
		$recordNumber_i:=$2
	Else 
		$recordNumber_i:=Record number:C243($table_ptr->)
		UNLOAD RECORD:C212($table_ptr->)
	End if 
	
	If (Count parameters:C259>=3)  // [1]
		Fnd_IO_ShowLockedMessage_b:=$3  // [1]
	End if   // [1]
	
	If ((Fnd_IO_MultiWindow_b) | (Fnd_Gen_CurrentFormType#Fnd_Gen_OutputForm))
		// DB051229 - Restructured everything in this If statement.
		If ($recordNumber_i=New record:K29:1)  // DB050301
			$processName_t:="Fnd_IO "+Table name:C256($table_ptr)+" New Record "+String:C10(Tickcount:C458)
		Else 
			$processName_t:="Fnd_IO "+Table name:C256($table_ptr)+" Record "+String:C10($recordNumber_i)
		End if 
		
		$processNumber_i:=Process number:C372($processName_t)  // See if the process already exists. We'll use this in a moment.
		
		If ($processNumber_i=0)
			// We're going to create a named selection to pass off to the new process.
			//    We'll try to use a semaphore to protect it, but we'll still launch the new
			//   process even if we have problems with the semaphore. That's why the
			//   semaphore call isn't in an If statement.
			// DB050227 - Removed the If test here.
			<>Fnd_IO_PassSelectionReceived_b:=False:C215
			$ignore_b:=Semaphore:C143("$Fnd_IO_PassSelectionSemaphore"; 300)
			COPY NAMED SELECTION:C331($table_ptr->; "<>Fnd_IO_PassSelection")
			$processNumber_i:=New process:C317("Fnd_IO_DisplayRecord2"; Fnd_Gen_DefaultStackSize; $processName_t; $table_ptr; $recordNumber_i; Current process:C322; Fnd_IO_ShowLockedMessage_b; *)
			// Now wait to make sure the new process has the selection, and then
			//   clean up our named selection and semaphore.
			// DB050227 - Removed the If test here.
			$timeout_i:=Tickcount:C458+(60*10)  // Timeout after 10 seconds.
			While ((Not:C34(<>Fnd_IO_PassSelectionReceived_b)) & (Tickcount:C458<=$timeout_i))
				DELAY PROCESS:C323(Current process:C322; 5)
			End while 
			CLEAR NAMED SELECTION:C333("<>Fnd_IO_PassSelection")
			CLEAR SEMAPHORE:C144("$Fnd_IO_PassSelectionSemaphore")
			
		Else 
			BRING TO FRONT:C326($processNumber_i)
		End if 
		
		
	Else 
		Fnd_IO_DisplayRecord3($recordNumber_i)  // We're staying in this process.
	End if 
	
	Fnd_IO_ShowLockedMessage_b:=True:C214  // [1]
	
End if 
$0:=$processNumber_i  // JDH 20230207