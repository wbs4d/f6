//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_SaveServerPrefs

  // Saves the user's server preferences to the data file.
  // Called from Fnd_Pref_SavePrefs, which handles the semaphore.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 6, 2006
  // ----------------------------------------------------

C_LONGINT:C283($element_i)

For ($element_i;1;Size of array:C274(<>Fnd_Pref_Names_at))
	If (<>Fnd_Pref_Modified_ab{$element_i})
		If (<>Fnd_Pref_Names_at{$element_i}#"")  // Don't save a pref without a name.
			Fnd_Pref_SaveServerPreference (<>Fnd_Pref_UserName_t;<>Fnd_Pref_Names_at{$element_i};<>Fnd_Pref_Types_ai{$element_i};<>Fnd_Pref_Values_at{$element_i};<>Fnd_Pref_Scopes_ai{$element_i})
		End if 
	End if 
End for 
