//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Sort_AddTable (->table{; position})

  // Loads the visible, sortable fields for the specified table
  //   into an array named Fnd_Sort_Fields_aptr.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the table to add
  //   $2 : Longint : The position in which to add the item (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jun 25, 2003
  // Modified by Dave Batton on May 21, 2004
  //   Changed the method from Private to Protected.
  // Modified by Gary Boudreaux on Dec 21, 2008
  //  Added checks to omit deleted tables and fields
  //  Used new $table_ptr variable (instead of $field_ptr) to properly reflect usage of table pointer
  //  Corrected header comments to accurately describe table to use
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr;$field_ptr)
C_LONGINT:C283($2;$tableNumber_i;$fieldNumber_i;$position_i;$fieldType_i;$fieldLength_i)
C_BOOLEAN:C305($fieldIndexed_b;$fieldUnique_b;$fieldInvisible_b)

$table_ptr:=$1

If (Count parameters:C259>=2)
	$position_i:=$2
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Sort_Init 

Case of 
	: ($position_i<1)
		$position_i:=1
	: ($position_i>(Size of array:C274(Fnd_Sort_Fields_aptr)+1))
		$position_i:=Size of array:C274(Fnd_Sort_Fields_aptr)+1
End case 

If (Not:C34(Is nil pointer:C315($table_ptr)))
	$tableNumber_i:=Table:C252($table_ptr)
	  //GB20081221 - added check to exclude processing of deleted tables
	If (Is table number valid:C999($tableNumber_i))
		For ($fieldNumber_i;1;Get last field number:C255($table_ptr))
			  //GB20081121 - added check to exclude processing of deleted fields
			If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))
				$field_ptr:=Field:C253($tableNumber_i;$fieldNumber_i)
				GET FIELD PROPERTIES:C258($field_ptr;$fieldType_i;$fieldLength_i;$fieldIndexed_b;$fieldUnique_b;$fieldInvisible_b)
				
				If (Not:C34($fieldInvisible_b))
					If (($fieldType_i#Is picture:K8:10) & ($fieldType_i#Is subtable:K8:11) & ($fieldType_i#Is BLOB:K8:12))
						Fnd_Sort_AddItem ($position_i;"";"";$field_ptr)
						$position_i:=$position_i+1
					End if 
				End if 
			End if 
		End for 
	End if 
End if 