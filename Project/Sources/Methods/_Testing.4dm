//%attributes = {}
Fnd_Prnt_LabelEditor()


// Modified by: Guy Algot (08/23/23)

//ARRAY TEXT($array; 0)
//// 20 = 16
//// 30 = 21
//// 40 = 26 rows

//For ($i; 1; 80)
//APPEND TO ARRAY($array; "foo"+String($i))
//End for 
//ChoiceList("Testing"; ->$array)

//Fnd_Out_Init
//$active:=fnd.out.isActive()




//// _Testing
//// Created by Wayne Stewart (2022-04-24)
////  Method is an autostart type
////     waynestewart@mac.com
//// ----------------------------------------------------

//var $ProcessID_i; $StackSize_i; $WindowID_i; $WindowID_1_i; $WindowID_2_i; $w; $h : Integer
//var $Form_t; $DesiredProcessName_t : Text
//var $Form_o : Object



//Fnd_Gen_FormMethod
//Fnd_Tlbr_FormMethod
//Fnd_Out_FormMethod



//// ----------------------------------------------------

//$StackSize_i:=0
//$Form_t:="___TestForm"
//$DesiredProcessName_t:="$"+$Form_t

//If (Current process name=$DesiredProcessName_t)
//$Form_o:=New object()

//Fnd_Wnd_Init

//$WindowID_1_i:=Open form window($Form_t; Plain form window; On the left)  //;At the bottom)
//DIALOG($Form_t; $Form_o; *)


//Fnd_Wnd_Position(On the left)
//$w:=108
//$h:=259

//$WindowID_2_i:=Fnd_Wnd_OpenWindow($w; $h)
////;At the bottom)
//DIALOG($Form_t; $Form_o; *)

////CLOSE WINDOW



//Else 
//// This version allows for any number of processes
//// $ProcessID_i:=New Process(Current method name;$StackSize_i;$DesiredProcessName_t)

//// On the other hand, this version allows for one unique process
//$ProcessID_i:=New process(Current method name; $StackSize_i; $DesiredProcessName_t; *)

//RESUME PROCESS($ProcessID_i)
//SHOW PROCESS($ProcessID_i)
//BRING TO FRONT($ProcessID_i)
//End if 
