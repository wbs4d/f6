//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_GetOneKeys (table pointer; array pointer) --> integer

// Description
//   Fills array $2-> with the foreign keys (source of related One relations) for table $1->
//   Returns the number of related one associations for table $1->.

// Access: Shared

// Parameters: 
//   $1 : Pointer : Table Pointer
//   $2 : Pointer : Relating keys of related one tables

// Returns: 
//   $0 : Integer : the number of related one associations

// Created by Doug Hall (20030617)
// ----------------------------------------------------

#DECLARE($table_ptr : Pointer; $RelatedOneKeysArray_ptr : Pointer)->$numberOfRelatedOneTables_i : Integer
var $tableNum_i : Integer

Fnd_Reltd_Init  // Need to call this to ensure the arrays have been set up.

$tableNum_i:=Table:C252($table_ptr)

COPY ARRAY:C226(<>Fnd_Reltd_One_aptr{$tableNum_i}; $RelatedOneKeysArray_ptr->)

$numberOfRelatedOneTables_i:=Size of array:C274(<>Fnd_Reltd_One_aptr{$tableNum_i})


