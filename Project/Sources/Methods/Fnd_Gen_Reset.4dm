//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Reset

// Resets all of the available Foundation components.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 5, 2004
// Modified by : Vincent Tournier (5/7/2011)
// ----------------------------------------------------

C_TEXT:C284($errorMethod_t;$componentName_t;$code_t)
C_LONGINT:C283($element_i)
ARRAY TEXT:C222($componentNames_at;0)  //Modified by : Vincent Tournier (5/7/2011)

LIST TO ARRAY:C288("Fnd_Gen_Components";$componentNames_at)

// We're going to try to reset components, even if they're not installed.
//   So we'll ignore any errors.
$errorMethod_t:=Method called on error:C704
ON ERR CALL:C155("Fnd_Gen_DummyMethod")

// Now set the "initialized" process and interprocess variables to false for each component.
For ($element_i;Size of array:C274($componentNames_at);1;-1)
	$componentName_t:=$componentNames_at{$element_i}
	$code_t:=$componentName_t+"_Initialized_b:="+Command name:C538(215)  // 215="False"
	EXECUTE FORMULA:C63($code_t)  // Clear the "initialized" process variable.
	$code_t:="<>"+$code_t
	EXECUTE FORMULA:C63($code_t)  // Now do the IP variable.
End for 


// Restore the previous error handler.
ON ERR CALL:C155($errorMethod_t)

// Pretend we didn't cause an error.
Error:=0
