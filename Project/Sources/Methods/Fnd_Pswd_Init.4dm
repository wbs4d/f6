//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pswd_Init

// Initializes both the process and interprocess variables used by the Fnd_Pswd routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Mar 16, 2004
// Modified by Dave Batton on May 30, 2004
//   Updated the version number to 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Pswd_Info method.
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Pswd_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Pswd_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Pswd
	
	Fnd_Pswd_Localize
	
	If (Is Windows:C1573)
		<>Fnd_Pswd_BulletChar_t:="*"
	Else 
		<>Fnd_Pswd_BulletChar_t:="•"  // For those of you viewing this on a PC, • is a bullet character.
	End if 
	
	// Set the default settings.
	<>Fnd_Pswd_UseUppercase_b:=True:C214
	<>Fnd_Pswd_UseLowercase_b:=True:C214
	<>Fnd_Pswd_UseNumbers_b:=True:C214
	<>Fnd_Pswd_UseSymbols_b:=False:C215
	
	<>Fnd_Pswd_UseCustom_b:=False:C215
	<>Fnd_Pswd_CustomCharacters_t:=""
	<>Fnd_Pswd_UseExcluded_b:=True:C214
	<>Fnd_Pswd_ExcludedCharacters_t:="l I 1 O 0"  // Avoid confusing characters.
	
	<>Fnd_Pswd_MinLength_i:=6  // Minimum password length.
	<>Fnd_Pswd_MaxLength_i:=10  // Maximum password length.
	<>Fnd_Pswd_MinLengthLimit_i:=4  // Minimum password length allowed by pop-up menu.
	<>Fnd_Pswd_MaxLengthLimit_i:=20  // Maximum password length allowed by pop-up menu.
	
	<>Fnd_Pswd_Initialized_b:=True:C214
End if 
