//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_CurrentFormType ({form type}) --> Number

// If no parameters are passed, this method returns the type of the
//   currently displayed form.  Pass a form type to set the type of the
//   currently displayed form.
// Designed specifically so we can tell if the window is displaying an input
//   form or an output form.
// 
// Can be one of these:
//   0 = Fnd_Gen_NoForm = No window
//   1 = Fnd_Gen_UnknownForm = Unknown form type
//   2 = Fnd_Gen_OutputForm = An output form
//   3 = Fnd_Gen_InputForm = An input form
//   4 = Fnd_Gen_PreferencesForm = The Preferences window
//   5 = Fnd_Gen_AboutForm = The About box

// Access Type: Shared

// Parameters: 
//   $1 : Longint : The form type constant

// Returns: 
//   $0 : Longint : The form type constant

// Created by Dave Batton on Nov 29, 2003
// ----------------------------------------------------

C_LONGINT:C283($0;$1;$formType_i)

Fnd_Gen_Init

If (Count parameters:C259>=1)
	Fnd_Gen_CurrentFormType_i:=$1
End if 

$formType_i:=Fnd_Gen_CurrentFormType_i

If (Window kind:C445(Frontmost window:C447)=0)
	$formType_i:=Fnd_Gen_CurrentFormType_i
End if 

$0:=$formType_i