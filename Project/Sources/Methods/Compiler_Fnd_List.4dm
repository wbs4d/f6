//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_List

// Compiler variables related to the Foundation List routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 2, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_List_Initialized_b)
If (Not:C34(<>Fnd_List_Initialized_b))
	C_TEXT:C284(<>Fnd_List_InfoName_t)
	C_TEXT:C284(<>Fnd_List_SelectedListName_t)
	C_TEXT:C284(<>Fnd_List_Semaphore_t)
	C_TEXT:C284(<>Fnd_List_Label1_t; <>Fnd_List_Label2_t)
	C_LONGINT:C283(<>Fnd_List_EditOneListItems_i)
	C_LONGINT:C283(<>Fnd_List_ListOfLists_i; <>Fnd_List_ListItems_i)
	C_LONGINT:C283(<>Fnd_List_OffscreenButton_i)
	C_LONGINT:C283(<>Fnd_List_SemaphoreTimeout_i)
	C_BOOLEAN:C305(<>Fnd_List_ListItemsModified_b)
	
	C_POINTER:C301(<>Fnd_List_Table_ptr)
	C_POINTER:C301(<>Fnd_List_IDFld_ptr)
	C_POINTER:C301(<>Fnd_List_ListNameFld_ptr)
	C_POINTER:C301(<>Fnd_List_ListBlobFld_ptr)
End if 


// Process variables
C_BOOLEAN:C305(Fnd_List_Initialized_b)
If (Not:C34(Fnd_List_Initialized_b))
	
	//C_TEXT(Fnd_List_NewButtonLabel_t)
	//C_LONGINT(Fnd_List_OKButton_i;Fnd_List_CancelButton_i)
	//C_LONGINT(Fnd_List_OffscreenButton_i)
	
	//C_LONGINT(Fnd_List_AddButton_i;Fnd_List_DeleteButton_i;Fnd_List_SortButton_i)
	C_LONGINT:C283(Fnd_List_Splitter_i)  //;Fnd_List_NewButton_i)
	C_LONGINT:C283(listRef_i)  // Modified by: Walt Nelson (2/19/10)
	
	ARRAY TEXT:C222(Fnd_List_Commands_at; 0)
	ARRAY TEXT:C222(Fnd_List_Methods_at; 0)
	ARRAY TEXT:C222(Fnd_List_ChoiceArray1_at; 0)
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Fnd_List_AddButton; $1)
	
	C_TEXT:C284(Fnd_List_AddItem; $1; $2)
	
	C_TEXT:C284(Fnd_List_AddToListEditor; $1)
	
	C_LONGINT:C283(Fnd_List_ChoiceList; $0)
	C_TEXT:C284(Fnd_List_ChoiceList; $1)
	C_POINTER:C301(Fnd_List_ChoiceList; $2)
	
	C_TEXT:C284(Fnd_List_EditOne; $1)
	
	C_TEXT:C284(Fnd_List_EditOne2; $1)
	
	C_TEXT:C284(Fnd_List_XML_GetDateTime; $0)
	
	C_TEXT:C284(Fnd_List_Info; $0; $1)
	
	C_TEXT:C284(Fnd_List_ListToRecord; $1)
	
	C_BOOLEAN:C305(Fnd_List_RecordLoaded; $0)
	C_POINTER:C301(Fnd_List_RecordLoaded; $1)
	C_LONGINT:C283(Fnd_List_RecordLoaded; $2)
	
	C_TEXT:C284(Fnd_List_XML_SetDateTime; $1)
	
	C_TEXT:C284(Fnd_List_XML_FilePath; $0)
	
	C_BOOLEAN:C305(Fnd_List_UseTypeAhead; $1; $0)
	C_TEXT:C284(Fnd_List_TypeAhead; $1)
	
	C_BOOLEAN:C305(Fnd_List_DoesListExist; $0)
	C_TEXT:C284(Fnd_List_DoesListExist; $1)
	
End if 