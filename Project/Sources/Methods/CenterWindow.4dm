//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: CenterWindow (width; height{; type{; title{; close method}}})

  // Allows an older Foundation based database to use the new Fnd_Gen_CenterWindow routine.
  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Description

  // Returns: 
  //   $0 : Longint : Description

  // Created by Dave Batton on Mar 3, 2004
  // ----------------------------------------------------

C_LONGINT:C283($1;$2;$3;$width_i;$height_i;$type_i;$position_i)
C_TEXT:C284($4;$5;$title_t)
C_BOOLEAN:C305($closeBox_b)

$width_i:=$1
$height_i:=$2

If (Count parameters:C259>=3)  // A window type was specified.
	$type_i:=$3
Else 
	$type_i:=Modal form dialog box:K39:7
End if 

If (Count parameters:C259>=4)  // Make sure we have a window title.
	$title_t:=$4
Else 
	$title_t:=Fnd_Gen_GetDatabaseInfo ("DatabaseName")
End if 

If (Count parameters:C259>=5)  // Close box?
	$closeBox_b:=($5#"")
Else 
	$closeBox_b:=False:C215
End if 

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))
	$position_i:=Fnd_Dlg_CenterOnWindow
Else 
	$position_i:=Fnd_Dlg_CenterOnScreen
End if 

Fnd_Gen_CenterWindow ($width_i;$height_i;$type_i;$position_i;$title_t;$closeBox_b)