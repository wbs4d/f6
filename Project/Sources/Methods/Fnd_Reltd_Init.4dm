//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_Init

// Initializes arrays used for storing relationships with other tables..

// Access: Private

// Created by Doug Hall (030217)
//     waynestewart@mac.com
// ----------------------------------------------------


C_BOOLEAN:C305(<>Fnd_Reltd_Initialized_b)

If (Not:C34(<>Fnd_Reltd_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Reltd
	
	var $fieldNum_i; $numFields_i; $OneTable_i; $OneField_i; $tableNum_i; $numTables_i : Integer
	var $Table_ptr : Pointer
	
	$numTables_i:=Get last table number:C254
	
	// Relation Arrays. Primary index is the table number
	ARRAY POINTER:C280(<>Fnd_Reltd_Many_aptr; $numTables_i; 0)  // Pointers to foreign keys (related many) of table {$tableNum}.
	ARRAY POINTER:C280(<>Fnd_Reltd_One_aptr; $numTables_i; 0)  // Pointers to primary keys of related one tables of table {$tableNum}.
	
	
	For ($tableNum_i; 1; $numTables_i)
		If (Is table number valid:C999($tableNum_i))
			$numFields_i:=Get last field number:C255($tableNum_i)
			For ($fieldNum_i; 1; $numFields_i)
				If (Is field number valid:C1000($tableNum_i; $fieldNum_i))
					GET RELATION PROPERTIES:C686($tableNum_i; $fieldNum_i; $OneTable_i; $OneField_i)
					If (($OneTable_i#0) & ($OneTable_i#$tableNum_i))  // If I'm related to a table, but NOT the current table...
						If (Is table number valid:C999($OneTable_i) & Is field number valid:C1000($OneTable_i; $OneField_i))
							APPEND TO ARRAY:C911(<>Fnd_Reltd_One_aptr{$tableNum_i}; Field:C253($OneTable_i; $OneField_i))
							APPEND TO ARRAY:C911(<>Fnd_Reltd_Many_aptr{$OneTable_i}; Field:C253($tableNum_i; $fieldNum_i))
						End if 
					End if 
				End if 
			End for 
		End if 
	End for 
	
	<>Fnd_Reltd_Initialized_b:=True:C214
End if 
