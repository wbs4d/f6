//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo 
//   method for more information.

// Access: Protected

// Parameters: 
//   $1 : Text : Info desired

// Returns: 
//   $0 : Text : Response

// Created by Wayne Stewart (2023-02-27)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE($request_t : Text)->$reply_t : Text

Case of 
	: ($request_t="version")
		$reply_t:="1.0"
		
	: ($request_t="name")
		$reply_t:="Fnd_Reltd"
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
		
End case 

