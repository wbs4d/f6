//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_MessageCanceled

  // Returns True if the user clicked the Cancel button.

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Boolean : True if the user clicked Cancel

  // Created by Dave Batton on Jul 26, 2003
  // ----------------------------------------------------

C_BOOLEAN:C305($0)

Fnd_Dlg_Init 

$0:=<>Fnd_Dlg_ProgAborted_b
