//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_NewRecord

// Creates and displays a new record for the current table.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Rec component is no longer required.
// ----------------------------------------------------

//If (Fnd_Gen_ComponentAvailable("Fnd_Rec"))
//EXECUTE METHOD("Fnd_Rec_NewRecord"; *)
//End if 

Fnd_Rec_NewRecord