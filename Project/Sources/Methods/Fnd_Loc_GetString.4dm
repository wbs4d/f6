//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_GetString (module name; internal string{; param 1{; param 2...}}) --> Text

// Gets the localized string based on the internal string or label.  Also replaces any
//   parameters.  For  example, "<<1>>" gets replaced with the first optional parameter,
//   "<<2>>" with the second optional parameter, etc.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The Foundation module name
//   $2 : Text : The internal string label
//   $3..$n : Text : Replacement strings (optional)

// Returns: 
//   $0 : Text : The localized string

// Created by Dave Batton on Jul 15, 2003
// Modified by Dave Batton on May 21, 2004
//   Now checks for a semaphore before working with the IP arrays.
// ----------------------------------------------------

C_TEXT:C284($0;$1;$2;${3};$moduleName_t;$internalLabel_t;$localString_t)
C_LONGINT:C283($moduleElement_i;$labelElement_i)

$moduleName_t:=$1
$internalLabel_t:=$2

Fnd_Loc_Init

$localString_t:=""

If (Not:C34(Semaphore:C143(<>Fnd_Loc_Semaphore_t;300)))  // Wait up to 5 seconds.
	$moduleElement_i:=Find in array:C230(<>Fnd_Loc_Modules_at;$moduleName_t)
	If ($moduleElement_i=-1)
		Fnd_Loc_LoadStringList($moduleName_t)
		$moduleElement_i:=Find in array:C230(<>Fnd_Loc_Modules_at;$moduleName_t)
	End if 
	
	If ($moduleElement_i>0)
		$labelElement_i:=Find in array:C230(<>Fnd_Loc_LookupCodes_at{$moduleElement_i};$internalLabel_t)
		If ($labelElement_i>0)
			$localString_t:=<>Fnd_Loc_Strings_at{$moduleElement_i}{$labelElement_i}
			
			For ($labelElement_i;1;Count parameters:C259-2)
				$localString_t:=Replace string:C233($localString_t;"<<"+String:C10($labelElement_i)+">>";${$labelElement_i+2})
			End for 
			
		Else 
			Fnd_Gen_BugAlert(Current method name:C684;"Unable to find the \""+$internalLabel_t+"\" string for localization.")
		End if 
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684;"Unable to load the \""+$moduleName_t+"\" module strings for localization.")
	End if 
	
	CLEAR SEMAPHORE:C144(<>Fnd_Loc_Semaphore_t)
End if 

$0:=$localString_t
