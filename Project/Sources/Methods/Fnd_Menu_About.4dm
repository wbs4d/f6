//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_About

// Called from the menu when the About box command is selected.
// Installed by the Fnd_Shell_OnStartup method.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 19, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Art component is no longer required.
// ----------------------------------------------------

//If (Fnd_Gen_ComponentAvailable("Fnd_Art"))
//EXECUTE METHOD("Fnd_Art_About"; *)
//End if 

Fnd_Art_About