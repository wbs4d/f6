//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_MultiWindow ({multi-window?}) --> Boolean

  // Allows the developer to get and set the multi-window setting.
  // Multi-window means the input forms are displayed in windows separate from
  //   the output lists. If multi-window is false, the input form is displayed in the
  //   same window as the output form.
  // The setting is for the current process.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : True to begin multi-window mode (optional)

  // Returns: 
  //   $0 : Boolean : True if we're in multi-window mode

  // Created by Dave Batton on Sep 9, 2004
  // Modified by Dave Batton on Feb 23, 2005
  //   Changed the IP var to a process var so this setting is now for the current
  //   process, not for all processes.
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_IO_Init 

If (Count parameters:C259>=1)
	Fnd_IO_MultiWindow_b:=$1
End if 

$0:=Fnd_IO_MultiWindow_b