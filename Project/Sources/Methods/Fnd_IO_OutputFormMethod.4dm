//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_OutputFormMethod

  // Standard form method for output forms.  Call this from every output form
  //   method.  Make sure the form events checked here are enabled by the form.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on May 2, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   If the developer has set a width other than 450 pixels wide for the form
  //   that inherits this form, we need to resize some objects to the new size.
  //   Moved the Toolbar handling routines into the new Fnd_IO_SetupToolbar method.
  //   Now the info text just under the toolbar is updated by calling the new Fnd_Tlbr_StatusMessage method.
  // Modified by Dave Batton on May 22, 2004
  //   Removed the Fnd_Gen_MenuBar from the On Activate event. It was redundant.
  // Modified by Dave Batton on Sep 10, 2004
  //   Now we set the window title in the On Header event, since we may be coming
  //     back from an input form in this same menu.
  //   This method now moves two invisible buttons offscreen. They trap the Enter key
  //     and Command-Period to prevent these keys from closing the output window.
  //     This is so cheating. We shouldn't mess with Fnd_Tlbr variables like this. But
  //     it's just not worth writing a protected method to do this properly. I may end
  //     up eating these words.
  // Modified by Dave Batton on Dec 10, 2004
  //   We no mess with the window title. This is now handled in the Fnd_IO_DisplayRecord3 method.
  // Modified by Dave Batton on Jul 22, 2005
  //   Added a call to the new Fnd_aa_IO_OutputFormMethod hook.
  // Modified by Dave Batton on Jan 29, 2007
  //   The status message is now set only if it's not already blank.
  //[1] Added by: John Craig (8/24/09) The status message is only updated if it needs to be changed
  //fixed recurring problem since 4D v2004, especially in compiled apps, where the header would update constantly
  //dragging 4D Server to it's knees - Many Thanks, John
  // ----------------------------------------------------

C_TEXT:C284($highlightedRecords_t;$recordsInSelection_t;$recordsInTable_t;$prefix_t)

Fnd_IO_Init 

Case of 
	: (Current process:C322=1)  // Don't do this stuff in the User process.
		
		
	: (Form event:C388=On Load:K2:1)
		OBJECT SET VISIBLE:C603(*;"Fnd_IO_HideThis@";False:C215)  // Hide our white background, marker guide positioning lines, and instructions.
		
		  // We have invisible buttons that trap Enter, Esc, and Command-Period.
		  //   Move them offscreen so they can't be accidentally clicked.
		OBJECT MOVE:C664(*;"Fnd_Tlbr_OffscreenButton@";-2;-2;-1;-1;*)
		
		Fnd_Gen_SelectionChanged 
		
		
	: ((Form event:C388=On Header:K2:17) | (Form event:C388=On Clicked:K2:4))
		Fnd_Gen_CurrentFormType (Fnd_Gen_OutputForm)  // It might have been switched if we displayed an input form.
		
		  // Update the status message under the toolbar.
		If (Fnd_Tlbr_StatusMessage #"")  // DB070129 - Added this test.
			
			$highlightedRecords_t:=String:C10(Records in set:C195("UserSet"))
			$recordsInSelection_t:=String:C10(Records in selection:C76(Current form table:C627->))
			$recordsInTable_t:=String:C10(Records in table:C83(Current form table:C627->))
			If (($highlightedRecords_t#Fnd_IO_highlightedRecords_t) | ($recordsInSelection_t#Fnd_IO_recordsInSelection_t) | ($recordsInTable_t#Fnd_IO_recordsInTable_t))  //[1] Added by: John Craig (8/24/09)
				Fnd_Tlbr_StatusMessage (Fnd_Loc_GetString ("Fnd_IO";"SelectionDescription";$highlightedRecords_t;$recordsInSelection_t;$recordsInTable_t))
				Fnd_IO_highlightedRecords_t:=String:C10(Records in set:C195("UserSet"))  //[1] Added by: John Craig (8/24/09)
				Fnd_IO_recordsInSelection_t:=String:C10(Records in selection:C76(Current form table:C627->))  //[1] Added by: John Craig (8/24/09)
				Fnd_IO_recordsInTable_t:=String:C10(Records in table:C83(Current form table:C627->))  //[1] Added by: John Craig (8/24/09)
			End if   //[1] Added by: John Craig (8/24/09)
		End if   // DB070129
		
		Fnd_IO_UpdateToolbar 
		Fnd_Menu_MenuBar   // Update the items that are enabled/disabled.
		
		
	: (Form event:C388=On Outside Call:K2:11)
		Case of 
			: ((Fnd_Wnd_CloseNow ) | (Fnd_Gen_QuitNow ))
				CANCEL:C270
				
			Else 
				  // If we add a new record, another process may want us to add the
				  //   new record to the end of the current selection.
				Fnd_Rec_AddToSelection (Fnd_IO_RecordEdited )
		End case 
		
		
	: (Form event:C388=On Double Clicked:K2:5)
		If ((Record number:C243(Current form table:C627->)>=0) & (Records in set:C195("UserSet")=1))  // Make sure it was a record that was double-clicked.
			FILTER EVENT:C321  // Tell 4D we want to handle this.
			Fnd_IO_DisplayRecord (Current form table:C627)
		End if 
		
		
	: (Form event:C388=On Activate:K2:9)
		  //Fnd_Gen_MenuBar  ` DB040522 - Not necessary.
		
		
	: (Form event:C388=On Close Box:K2:21)
		CANCEL:C270
		If (Macintosh option down:C545)
			Fnd_Wnd_CloseAllWindows 
		End if 
End case 

Fnd_Tlbr_FormMethod 

If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_IO_OutputFormMethod")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_IO_OutputFormMethod";*)
End if 
