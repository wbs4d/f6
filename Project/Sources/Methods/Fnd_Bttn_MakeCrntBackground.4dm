//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_MakeCrntBackground (width) --> Picture

  // Returns the background to use with the currently selected "Pref" style button.
  // Similar to Fnd_Bttn_MakeBackground, but designed for the background
  //   of the currently selected button (Mac OS X Prefs style).
  // This routine also isn't as optimized as Fnd_Bttn_MakeBackground.

  // Access Type: Private

  // Parameters: 
  //   $1 : Longint : The required width of the background

  // Returns: 
  //   $0 : Picture : The background picture

  // Created by Dave Batton on Apr 13, 2005
  // Modified by Dave Batton on Nov 18, 2005
  //   Generated backgrounds are now added to the image cache.
  // Modified: [1] John Craig, 02/07/2014, 13:48:18 Changed | picture operator to COMBINE PICTURES with Superimposition method
  // ----------------------------------------------------

C_PICTURE:C286($0;$background_pic)
C_LONGINT:C283($1;$width_i;$index_i;$bit_i;$background_picWidth_i;$leftEdgeSize_i;$rightEdgeSize_i;$height_i;$pixel_i)
C_TEXT:C284($cacheName_t)

$width_i:=$1

If ($width_i>0)
	  //Check to see if the image is the in cache
	$cacheName_t:=Fnd_Bttn_GetPictureName ("background";String:C10($width_i);"current")
	$index_i:=Find in array:C230(<>Fnd_Bttn_CacheNames_at;$cacheName_t)
	
	If ($index_i>0)
		$background_pic:=Fnd_Bttn_Cache_Image_Get ($cacheName_t)
		
	Else 
		  // Get the widths of the end caps.
		PICTURE PROPERTIES:C457(Fnd_Bttn_BG_LeftCrnt_pic;$leftEdgeSize_i;$height_i)
		PICTURE PROPERTIES:C457(Fnd_Bttn_BG_RightCrnt_pic;$rightEdgeSize_i;$height_i)
		
		  //Decrement the width by the size of the end pieces
		If ($width_i>($leftEdgeSize_i+$rightEdgeSize_i))
			$width_i:=$width_i-($leftEdgeSize_i+$rightEdgeSize_i)
		End if 
		
		  //Add the left hand edge of the rollover picture.
		$background_pic:=Fnd_Bttn_BG_LeftCrnt_pic
		$background_picWidth_i:=$leftEdgeSize_i
		
		  //Build the background of the rollover to the correct width
		For ($pixel_i;1;$width_i)
			  // Modified: [1] John Craig, 02/07/2014, 13:48:18 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
			  //$background_pic:=$background_pic | (Fnd_Bttn_BG_MidCrnt_pic+$background_picWidth_i)
			COMBINE PICTURES:C987($background_pic;$background_pic;Superimposition:K61:10;Fnd_Bttn_BG_MidCrnt_pic;$background_picWidth_i;0)
			  // Modified: [1] John Craig, 02/07/2014, 13:48:18 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
			
			$background_picWidth_i:=$background_picWidth_i+1
		End for 
		
		  //Add the right hand edge to the rollover picture.
		  // Modified: [1] John Craig, 02/07/2014, 13:48:18 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
		  //$background_pic:=$background_pic | (Fnd_Bttn_BG_RightCrnt_pic+$background_picWidth_i)
		COMBINE PICTURES:C987($background_pic;$background_pic;Superimposition:K61:10;Fnd_Bttn_BG_RightCrnt_pic;$background_picWidth_i;0)
		  // Modified: [1] John Craig, 02/07/2014, 13:48:18 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
		
	End if 
End if 

  //Return the background image.
$0:=$background_pic
