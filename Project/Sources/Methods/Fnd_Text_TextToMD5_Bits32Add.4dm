//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_TextToMD5_Bits32Add (?) --> Longint

  // Part of the MD5 hash generating code provided by 4D SA.
  // This code was provided to me by 4D SA and is used with permission.
  // I've left the original code untouched, except for this header.

  // Access: Private

  // Parameters: Undocumented

  // Returns: 
  //   $0 : Longint : Undocumented

  // Created by Dave Batton on Mar 6, 2005
  // Based on code from 4D SA.
  // ----------------------------------------------------

  //--- extracted from 4D Business Kit 2.0 with permission / 4D SA 2002 ---

  //addition of words (modulo 2^32)

C_LONGINT:C283($1;$2)
C_LONGINT:C283($0)

C_LONGINT:C283($2bytes_low)
$2bytes_low:=($1 & 0xFFFF)+($2 & 0xFFFF)

C_LONGINT:C283($2bytes_high)
$2bytes_high:=($1 >> 16)+($2 >> 16)+($2bytes_low >> 16)

$0:=($2bytes_high << 16) | ($2bytes_low & 0xFFFF)