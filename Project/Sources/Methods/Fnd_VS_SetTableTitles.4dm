//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_SetTableTitles (->table titles; ->table numbers)

  // Allows the developer to set virtual table titles. Designed to be called
  //   as a replacement to 4D's SET TABLE TITLES command.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : Array of table titles
  //   $2 : Pointer : Array of table numbers

  // Returns: None

  // Created by Dave Batton on Jan 24, 2004
  // Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$tableNamesArray_ptr;$tableNumbersArray_ptr)

$tableNamesArray_ptr:=$1
$tableNumbersArray_ptr:=$2

Fnd_VS_Init 

  // Make sure the arrays passed in are valid.
Case of 
	: (Size of array:C274($tableNamesArray_ptr->)#Size of array:C274($tableNumbersArray_ptr->))
		Fnd_Gen_BugAlert (Current method name:C684;"The table name and number array sizes do not match.")
		
	Else 
		If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
			  // Copy the user's arrays into Foundation's internal arrays.
			COPY ARRAY:C226($tableNamesArray_ptr->;<>Fnd_VS_TableNames_at)
			COPY ARRAY:C226($tableNumbersArray_ptr->;<>Fnd_VS_TableNumbers_ai)
			
			SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at;<>Fnd_VS_TableNumbers_ai;*)  // WN100211 - added *
			
			CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
			
		Else 
			Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
		End if 
End case 