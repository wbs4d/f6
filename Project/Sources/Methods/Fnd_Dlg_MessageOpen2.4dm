//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_MessageOpen2 (window title)

  // Called as a new process from Fnd_Dlg_MessageOpen to display the progress indicator window.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The window title

  // Returns: Nothing

  // Created by Dave Batton on Jul 25, 2003
  // Modified by Dave Batton on Sep 4, 2004
  //   The <>Fnd_Dlg_ProgFrom_i and <>Fnd_Dlg_ProgTo_i
  //      variables are now reset to 0 after closing the window.
  // Modified by Dave Batton on Jan 15, 2005
  //   Now expects the window title to be passed as a parameter.
  // Modified by Dave Batton on Sep 15, 2007
  //   Updated for 4D v11 SQL,
  // ----------------------------------------------------

C_TEXT:C284($1;$windowTitle_t)
C_LONGINT:C283(OK;$windowType_i;$left_i;$top_i;$width_i;$height_i;$lines_i)

$windowTitle_t:=$1  // DB050115 - Added.

<>Fnd_Dlg_MessageClose_b:=False:C215

$windowType_i:=0-(Palette window:K34:3+1)  // +1 gets rid of the minimize button.

FORM GET PROPERTIES:C674("Fnd_Dlg_MessageDialog";$width_i;$height_i)

$lines_i:=Fnd_Ext_TextLines (<>Fnd_Dlg_MessageText_t;300;<>Fnd_Dlg_MessageFontName_t;<>Fnd_Dlg_MessageFontSize_i;<>Fnd_Dlg_MessageFontStyle_i)
<>Fnd_Dlg_TextHeightAdjust_i:=(<>Fnd_Dlg_MessageFontHeight_i*($lines_i-1))

$height_i:=$height_i+<>Fnd_Dlg_TextHeightAdjust_i  // This will also be used in the form method.

  // If we won't be showing the progress bar, reduce the window height.
If ((<>Fnd_Dlg_ProgFrom_i=0) & (<>Fnd_Dlg_ProgTo_i=0))
	<>Fnd_Dlg_ProgBarHeightAdjust_i:=-33  // This number is also used in the dialog's form method.
	$height_i:=$height_i+<>Fnd_Dlg_ProgBarHeightAdjust_i
Else 
	<>Fnd_Dlg_ProgBarHeightAdjust_i:=0
End if 

  // If we won't be showing the stop button, reduce the window height.
If (<>Fnd_Dlg_CancelButton_t="")
	$height_i:=$height_i-40  // This number is also used in the dialog's form method.
End if 

$left_i:=(Screen width:C187\2)-($width_i\2)
$top_i:=60  // Just to match 4D's progress indicator position.
Open window:C153($left_i;$top_i;$left_i+$width_i;$top_i+$height_i;$windowType_i;$windowTitle_t)

<>Fnd_Dlg_ProgAborted_b:=False:C215

DIALOG:C40("Fnd_Dlg_MessageDialog")

CLOSE WINDOW:C154

If (OK=1)
	  // Cancel or Escape was pressed or the Stop button was clicked.
	<>Fnd_Dlg_ProgAborted_b:=True:C214
End if 

<>Fnd_Dlg_ProgFrom_i:=0
<>Fnd_Dlg_ProgTo_i:=0

