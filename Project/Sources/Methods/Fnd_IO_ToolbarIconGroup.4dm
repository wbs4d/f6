//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_ToolbarIconGroup ({style name}) --> Text

  // Changes the icons in the IO forms to the specified style.
  //   Bold (default)
  //   Card
  //   Native
  //   Mac
  //   Win

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The style name to use (optional)

  // Returns: 
  //   $0 : Text : The current style

  // Created by Dave Batton on Jan 8, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$suggestedStyle_t;$prefix_t)

Fnd_IO_Init 

If (Count parameters:C259>=1)
	$suggestedStyle_t:=$1
	
	Case of 
		: ($suggestedStyle_t=Fnd_IO_IconGroupName_t)
			  // No change needed.
			
		: ($suggestedStyle_t="Card")
			Fnd_Tlbr_Button_Icon ("New";"Fnd_Bttn_CardPlus")
			Fnd_Tlbr_Button_Icon ("Delete";"Fnd_Bttn_CardMinus")
			Fnd_Tlbr_Button_Icon ("ShowAll";"Fnd_Bttn_Cards")
			Fnd_Tlbr_Button_Icon ("Find";"Fnd_Bttn_CardMagnifier")
			Fnd_Tlbr_Button_Icon ("Sort";"Fnd_Bttn_CardBlocks")
			Fnd_Tlbr_Button_Icon ("Print";"Fnd_Bttn_CardPrinter")
			
		: (($suggestedStyle_t="Win") | (($suggestedStyle_t="Native") & (Fnd_Tlbr_Platform ="Win")))
			$suggestedStyle_t:="Win"
			Fnd_Tlbr_Button_Icon ("New";"Fnd_Bttn_WinNew")
			Fnd_Tlbr_Button_Icon ("Delete";"Fnd_Bttn_WinDelete")
			Fnd_Tlbr_Button_Icon ("ShowAll";"Fnd_Bttn_WinShowAll")
			Fnd_Tlbr_Button_Icon ("Find";"Fnd_Bttn_WinFind")
			Fnd_Tlbr_Button_Icon ("Sort";"Fnd_Bttn_WinSort")
			Fnd_Tlbr_Button_Icon ("Print";"Fnd_Bttn_WinPrint")
			
		: (($suggestedStyle_t="Mac") | (($suggestedStyle_t="Native") & (Fnd_Tlbr_Platform ="Mac")))
			$suggestedStyle_t:="Mac"
			Fnd_Tlbr_Button_Icon ("New";"Fnd_Bttn_MacNew")
			Fnd_Tlbr_Button_Icon ("Delete";"Fnd_Bttn_MacDelete")
			Fnd_Tlbr_Button_Icon ("ShowAll";"Fnd_Bttn_MacShowAll")
			Fnd_Tlbr_Button_Icon ("Find";"Fnd_Bttn_MacFind")
			Fnd_Tlbr_Button_Icon ("Sort";"Fnd_Bttn_MacSort")
			Fnd_Tlbr_Button_Icon ("Print";"Fnd_Bttn_MacPrint")
			
		Else 
			$suggestedStyle_t:="Bold"
			Fnd_Tlbr_Button_Icon ("New";"Fnd_Bttn_GreenPlus")
			Fnd_Tlbr_Button_Icon ("Delete";"Fnd_Bttn_RedX")
			Fnd_Tlbr_Button_Icon ("ShowAll";"Fnd_Bttn_IndexCards")
			Fnd_Tlbr_Button_Icon ("Find";"Fnd_Bttn_Magnifier")
			Fnd_Tlbr_Button_Icon ("Sort";"Fnd_Bttn_Arrows")
			Fnd_Tlbr_Button_Icon ("Print";"Fnd_Bttn_Printer")
	End case 
	
	
	Fnd_IO_IconGroupName_t:=$1
End if 

$0:=Fnd_IO_IconGroupName_t