//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_Init

// Initializes both the process and interprocess variables used by the Fnd_Find routines.

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 14, 2003
// Modified by Dave Batton on Jan 24, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
// Modified by Dave Batton on May 30, 2004
//   Updated the version to 4.0.3.
//   Removed the component check for "Fnd_Loc".
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Find_Info method.
// Modified by Dave Batton on Jan 15, 2005
//   Moved the window setting commands from the Fnd_Find_Display method to here
//      so they can be overridden by the developer.
// Modified by Dave Batton on May 28, 2005
//   Updated to use the new Fnd_Wnd methods.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Find_Initialized_b; Fnd_Find_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Find_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Find
	
	Fnd_Find_Localize
	
	<>Fnd_Find_Initialized_b:=True:C214
End if 


If (Not:C34(Fnd_Find_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Find
	
	Fnd_Find_Table_ptr:=Fnd_Gen_CurrentTable
	Fnd_Find_SearchString_t:=""
	Fnd_Find_EditorButtonLabel_t:=Fnd_Gen_GetString("Fnd_Find"; "EditorButton")
	Fnd_Find_EditorButtonMethod_t:=""
	
	ARRAY TEXT:C222(Fnd_Find_FieldNames_at; 0)
	ARRAY POINTER:C280(Fnd_Find_Fields_aptr; 0)
	ARRAY INTEGER:C220(Fnd_Find_FieldTypes_ai; 0)
	ARRAY TEXT:C222(Fnd_Find_MethodNames_at; 0)
	
	// DB050115 - Moved these routines here from the Fnd_Find_Display method.
	Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
	Fnd_Wnd_Type(Movable form dialog box:K39:8)  // DB050528 - Changed from Fnd_Wnd_SetType.
	Fnd_Wnd_Title(Fnd_Gen_GetString("Fnd_Find"; "WindowTitle"))  // DB050528 - Changed from Fnd_Wnd_SetTitle.
	
	Fnd_Find_Initialized_b:=True:C214
End if 
