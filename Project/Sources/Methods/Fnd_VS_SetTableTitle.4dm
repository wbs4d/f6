//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_SetTableTitle (->table; title)

  // Allows the developer to set a virtual table title for a single table.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : The table to rename
  //   $2 : Text : The virtual title

  // Returns: None

  // Created by Dave Batton on Jan 24, 2004
  // Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_TEXT:C284($2;$title_t)
C_LONGINT:C283($tableNumber_i;$element_i)

$table_ptr:=$1
$title_t:=$2

Fnd_VS_Init 

$tableNumber_i:=Table:C252($table_ptr)

If (Is table number valid:C999($tableNumber_i))
	If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
		$element_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)
		If ($element_i>0)
			<>Fnd_VS_TableNames_at{$element_i}:=$title_t
		End if 
		
		SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at;<>Fnd_VS_TableNumbers_ai;*)  // WN100211 - added *
		
		CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
	End if 
End if 