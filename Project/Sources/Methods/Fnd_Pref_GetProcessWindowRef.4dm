//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_GetProcessWindowRef (process number) --> Number

  // Returns the window ref for the frontmost window of the specified process.
  // There's also a Fnd_Wnd_GetProcessWindowRef method, but it may not be installed.

  // Access Type: Private

  // Parameters: 
  //   $1 : Longint : A process number

  // Returns: 
  //   $0 : Longint : A window reference or 0

  // Created by Dave Batton on Jul 3, 2003
  // ----------------------------------------------------

C_LONGINT:C283($0;$1;$processNumber_i;$windowRef;$foundRef_i)

$processNumber_i:=$1

$foundRef_i:=0

$windowRef:=Frontmost window:C447

Repeat 
	If (Window process:C446($windowRef)=$processNumber_i)
		$foundRef_i:=$windowRef
	End if 
	$windowRef:=Next window:C448($windowRef)
Until (($windowRef=0) | ($foundRef_i#0))

$0:=$foundRef_i
