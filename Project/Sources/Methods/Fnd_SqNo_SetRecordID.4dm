//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_SetRecordID ({->table})

  // Assigns a sequential number to the specified table's ID field. Assumes the number
  //   will not be seen by the end user, so the number can't be returned for reuse.
  // If no table is specified, either the current form table or the trigger table will be used.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the table for which to return a number (optional)

  // Returns: Nothing

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Jun 30, 2007
  //   Fixed an incorrect parameter that was being passed to the Fnd_SqNo_TableSeqNoName method.
  // Modified by Dave Batton on Sep 24, 2007
  //   Changed the $seqNoName_t variable name to $groupName_t.
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr;$keyField_ptr)
C_TEXT:C284($groupName_t)
C_LONGINT:C283($level_i;$event_i;$table_i;$record_i)

Case of 
	: (Count parameters:C259>=1)
		$table_ptr:=$1
	: (Trigger level:C398>0)
		TRIGGER PROPERTIES:C399(Trigger level:C398;$event_i;$table_i;$record_i)
		$table_ptr:=Table:C252($table_i)
	Else 
		$table_ptr:=Current form table:C627
End case 

Fnd_SqNo_Init 

$keyField_ptr:=Fnd_SqNo_IDFieldPtr ($table_ptr)

If ($keyField_ptr->=0)  // Don't modify this value if it's anything other than zero.
	$groupName_t:=Fnd_SqNo_TableSeqNoName ($table_ptr)  // DB070630 - I was passing $keyField_ptr here rather than $table_ptr. Oops!
	$keyField_ptr->:=Fnd_SqNo_Get ($groupName_t)
End if 