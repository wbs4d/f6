//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_ClickOnCodesList

  // Called from the localization editor when a lookup code is clicked, or
  //   when a higher-level list is clicked.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 11, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

If (<>Fnd_Loc_EditorCodes_at=0)
	<>Fnd_Loc_EditorLookupCode_t:=""
	<>Fnd_Loc_EditorStringEN_t:=""
	<>Fnd_Loc_EditorStringXX_t:=""
	
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorLookupCode_t;False:C215)
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorStringEN_t;False:C215)
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorStringXX_t;False:C215)
	
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteCodeButton_;False:C215)
	
Else 
	<>Fnd_Loc_EditorLookupCode_t:=<>Fnd_Loc_EditorCodes_at{<>Fnd_Loc_EditorCodes_at}
	<>Fnd_Loc_EditorStringEN_t:=<>Fnd_Loc_EditorStringsEN_at{<>Fnd_Loc_EditorGroups_at}{<>Fnd_Loc_EditorCodes_at}
	<>Fnd_Loc_EditorStringXX_t:=<>Fnd_Loc_EditorStringsXX_at{<>Fnd_Loc_EditorGroups_at}{<>Fnd_Loc_EditorCodes_at}
	
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorLookupCode_t;True:C214)
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorStringEN_t;True:C214)
	OBJECT SET ENTERABLE:C238(<>Fnd_Loc_EditorStringXX_t;True:C214)
	
	HIGHLIGHT TEXT:C210(<>Fnd_Loc_EditorStringEN_t;1;MAXTEXTLENBEFOREV11:K35:3)
	
	OBJECT SET ENABLED:C1123(<>Fnd_Loc_EditorDeleteCodeButton_;True:C214)
End if 
