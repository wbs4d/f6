//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Menu

// Compiler variables related to the Foundation Menu routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 31, 2003
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------


// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Menu_Initialized_b)
If (Not:C34(<>Fnd_Menu_Initialized_b))
	C_LONGINT:C283(<>Fnd_Menu_FileMenu_i; <>Fnd_Menu_FileOpenItem_i; <>Fnd_Menu_FileAdminItem_i; <>Fnd_Menu_FilePrintItem_i)
	C_LONGINT:C283(<>Fnd_Menu_EditMenu_i; <>Fnd_Menu_EditPreferencesItem_i)
	C_LONGINT:C283(<>Fnd_Menu_EnterMenu_i; <>Fnd_Menu_EnterNewRecordItem_i; <>Fnd_Menu_EnterModifyRecsItem_i; <>Fnd_Menu_EnterDeleteSetItem_i)
	C_LONGINT:C283(<>Fnd_Menu_SelectMenu_i; <>Fnd_Menu_SelectShowAllItem_i; <>Fnd_Menu_SelectShowSetItem_i; <>Fnd_Menu_SelectOmitSetItem_i; <>Fnd_Menu_SelectFindItem_i; <>Fnd_Menu_SelectQueryEdItem_i; <>Fnd_Menu_SelectSortItem_i; <>Fnd_Menu_SelectOrderByEdItem_i; <>Fnd_Menu_SelectFindReltdItem_i)
	C_LONGINT:C283(<>Fnd_Menu_ToolsMenu_i; <>Fnd_Menu_ToolsNavPaletteItem_i; <>Fnd_Menu_ToolsSpecialFnctItem_i; <>Fnd_Menu_ToolsViewOptionsItem_i)
	C_LONGINT:C283(<>Fnd_Menu_WindowMenu_i; <>Fnd_Menu_SemaphoreTimeout_i)
	
	C_TEXT:C284(<>Fnd_Menu_Semaphore_t)
	C_TEXT:C284(<>Fnd_Menu_FileTitle_t; <>Fnd_Menu_FileOpenItem_t; <>Fnd_Menu_FileAdminItem_t; <>Fnd_Menu_FilePrintItem_t)
	C_TEXT:C284(<>Fnd_Menu_EditPreferencesItem_t; <>Fnd_Menu_EnterTitle_t; <>Fnd_Menu_EnterModifyRecsItem_t; <>Fnd_Menu_EnterDeleteSetItem_t; <>Fnd_Menu_SelectTitle_t)
	C_TEXT:C284(<>Fnd_Menu_SelectShowAllItem_t; <>Fnd_Menu_SelectShowSetItem_t; <>Fnd_Menu_SelectOmitSetItem_t)
	C_TEXT:C284(<>Fnd_Menu_SelectFindItem_t; <>Fnd_Menu_SelectQueryEdItem_t; <>Fnd_Menu_SelectSortItem_t; <>Fnd_Menu_SelectOrderByEdItem_t; <>Fnd_Menu_SelectFindReltdItem_t)
	C_TEXT:C284(<>Fnd_Menu_ToolsTitle_t; <>Fnd_Menu_ToolsNavPaletteItem_t; <>Fnd_Menu_ToolsSpecialFnctItem_t)
	C_TEXT:C284(<>Fnd_Menu_WindowTitle_t; <>Fnd_Menu_WindowCloseAllItem_t; <>Fnd_Menu_EnterNewRecordItem_t)
	
	C_TEXT:C284(<>Fnd_Menu_MenuBar_t; <>Fnd_Menu_FileMenu_t; <>Fnd_Menu_EditMenu_t; <>Fnd_Menu_EnterMenu_t; <>Fnd_Menu_SelectMenu_t; <>Fnd_Menu_ToolsMenu_t; <>Fnd_Menu_WindowMenu_t)
	C_TEXT:C284(<>Fnd_Menu_FileOpenItem_t; <>Fnd_Menu_FileAdminItem_t; <>Fnd_Menu_FilePrintItem_t)
	//C_TEXT(<>Fnd_Menu_EditPreferencesItem_t)
	C_TEXT:C284(<>Fnd_Menu_EnterNewRecordItem_t; <>Fnd_Menu_EnterModifyRecsItem_t; <>Fnd_Menu_EnterDeleteSetItem_t)
	C_TEXT:C284(<>Fnd_Menu_SelectShowAllItem_t; <>Fnd_Menu_SelectShowSetItem_t; <>Fnd_Menu_SelectOmitSetItem_t; <>Fnd_Menu_SelectFindItem_t; <>Fnd_Menu_SelectQueryEdItem_t; <>Fnd_Menu_SelectSortItem_t; <>Fnd_Menu_SelectOrderByEdItem_t)
	C_TEXT:C284(<>Fnd_Menu_ToolsNavPaletteItem_t; <>Fnd_Menu_ToolsSpecialFnctItem_t)
	
	C_BOOLEAN:C305(<>Fnd_Menu_MenuNumbersLoaded_b)
	
	ARRAY TEXT:C222(<>Fnd_Menu_Window_Items_at; 0)
	ARRAY INTEGER:C220(<>Fnd_Menu_Window_Processes_ai; 0)
End if 


// Process variables
C_BOOLEAN:C305(Fnd_Menu_Initialized_b)
If (Not:C34(Fnd_Menu_Initialized_b))
	C_TEXT:C284(Fnd_Menu_MenuName_t; Fnd_Menu_LastMenuBarName_t)
	C_LONGINT:C283(Fnd_Menu_Records_i; Fnd_Menu_WindowMenu_i)
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Fnd_Menu_BuildMenuBar; $0)
	
	C_TEXT:C284(Fnd_Menu_DisableAllMenuItems; $1)
	
	C_TEXT:C284(Fnd_Menu_EnableAllMenuItems; $1)
	
	C_TEXT:C284(Fnd_Menu_Info; $0; $1)
	
	C_TEXT:C284(Fnd_Menu_MenuBar; $1)
	
	C_TEXT:C284(Fnd_Menu_MenuBarName; $0; $1)
	C_LONGINT:C283(Fnd_Menu_MenuBarName; $2)
	
	C_LONGINT:C283(Fnd_Menu_SetMenuItem; $1; $2)
	C_TEXT:C284(Fnd_Menu_SetMenuItem; $3)
	
	C_TEXT:C284(Fnd_Menu_Window_Add; $1)
	
	C_LONGINT:C283(Fnd_Menu_Window_Remove; $1)
End if 