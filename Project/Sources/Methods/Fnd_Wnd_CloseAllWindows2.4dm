//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Wnd_CloseAllWindows2


  // This process tries to tell all of the other processes to close their windows.

  // Called as a new process from Fnd_Wnd_CloseAllWindows.


  // Access Type: Private


  // Parameters: None


  // Returns: Nothing


  // Created by Dave Batton on Sep 8, 2003

  // ----------------------------------------------------


C_BOOLEAN:C305($done_b)
C_LONGINT:C283($timeout_i;$closeProcessNumber_i;$processState_i;$processTime_i;$element_i;$colonPosition_i)
C_TEXT:C284($closeProcessName_t;$actualProcessName_t)

$timeout_i:=Tickcount:C458+(30*60)  // 30 seconds


Repeat 
	$done_b:=True:C214
	
	  // Do this backwards, so deleting elements doesn't mess up the For counter.

	For ($element_i;Size of array:C274(<>Fnd_Wnd_CloseProcessNames_at);1;-1)
		$colonPosition_i:=Fnd_Gen_PositionR (":";<>Fnd_Wnd_CloseProcessNames_at{$element_i})
		$closeProcessName_t:=Substring:C12(<>Fnd_Wnd_CloseProcessNames_at{$element_i};1;$colonPosition_i-1)
		$closeProcessNumber_i:=Num:C11(Substring:C12(<>Fnd_Wnd_CloseProcessNames_at{$element_i};$colonPosition_i+1))
		
		PROCESS PROPERTIES:C336($closeProcessNumber_i;$actualProcessName_t;$processState_i;$processTime_i)
		
		If (($actualProcessName_t=$closeProcessName_t) & ($processState_i>=0))  // If it has the name we expect and is still running...

			POST OUTSIDE CALL:C329($closeProcessNumber_i)  // Nudge it so it will check it see if it's time to quit.

		Else 
			DELETE FROM ARRAY:C228(<>Fnd_Wnd_CloseProcessNames_at;$element_i)  // It's not running, so remove it from the array.

		End if 
	End for 
	
Until ((Size of array:C274(<>Fnd_Wnd_CloseProcessNames_at)=0) | (Not:C34(<>Fnd_Wnd_Closing_b)) | (Tickcount:C458>$timeout_i))

<>Fnd_Wnd_Closing_b:=False:C215
