//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ToolbarDisplayed --> Boolean

// Returns true if the design toolbar is visible

// Access: Shared

// Returns: 
//   $0 : Boolean : Description

// Created by Wayne Stewart (2022-04-26)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE()->$toolbarIsDisplayed_b : Boolean

If (False:C215)
	C_BOOLEAN:C305(Fnd_Gen_ToolbarDisplayed; $0)
End if 

var $Document_t; $path_t : Text
var $position_i : Integer

$path_t:=Get 4D folder:C485(Database folder:K5:14; *)+"Project"+Folder separator:K24:12+"Sources"+Folder separator:K24:12+"settings.4DSettings"

If (Test path name:C476($path_t)=Is a document:K24:1)
	$Document_t:=Document to text:C1236($path_t; "UTF-8")
End if 
$position_i:=Position:C15("display_toolbar=\"false\""; $Document_t)

$toolbarIsDisplayed_b:=($position_i=0)
