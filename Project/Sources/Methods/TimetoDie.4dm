//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: TimetoDie

// I’ve seen things you people wouldn’t believe.
// Attack ships on fire off the shoulder of Orion.
// I watched C-beams glitter in the dark near the Tannhäuser Gate.
// All those moments will be lost in time, like tears in rain.
//
// Time to die.

// Created by Wayne Stewart (2021-08-09)
//     waynestewart@mac.com
// ----------------------------------------------------

If (False:C215)
	TimetoDie
End if 

KILL WORKER:C1390

