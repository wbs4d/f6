//%attributes = {"invisible":true,"executedOnServer":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_GetDistinctAttributes3

// Description

// Access: Shared

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Created by Wayne Stewart (2017-03-24)
//     waynestewart@mac.com
// ----------------------------------------------------



// Fnd_Find_GetDistinctAttributes3
// Created by Wayne Stewart (2017-03-24)
//  Method is an autostart type
//     waynestewart@mac.com
// ----------------------------------------------------

C_LONGINT:C283($2; $ProcessID_i; $StackSize_i; $WindowID_i)

If (False:C215)  //  Copy this to your Compiler Method!
	
	//  Called by:
	Fnd_Find_GetDistinctAttributes2
	
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes3; $1)
	C_LONGINT:C283(Fnd_Find_GetDistinctAttributes3; $2)
End if 

// ----------------------------------------------------

$StackSize_i:=0

If (Count parameters:C259=1)
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;$StackSize_i;Current method name;0)
	
	// On the other hand, this version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; $StackSize_i; Current method name:C684; 0; *)
	
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 
