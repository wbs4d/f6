//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_RegistrationState ({feature code}) --> Longint

  // Returns the current registration state.
  // It can be one of these constants:
  //   Fnd_Reg_PreDemo 
  //   Fnd_Reg_Demo 
  //   Fnd_Reg_Expired 
  //   Fnd_Reg_Registered 

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: 
  //   $0 : Longint : The current registration state

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_LONGINT:C283($0;$state_i)
C_TEXT:C284($1;$featureCode_t)
C_DATE:C307($expirationDate_d)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

$state_i:=Fnd_Reg_Expired  // The default.

READ ONLY:C145(<>Fnd_Reg_Table_ptr->)
$expirationDate_d:=Fnd_Reg_GetExpirationDate ($featureCode_t)  // Fnd_Reg_GetExpirationDate is loading the ◊Fnd_Reg_Table_ptr-> record for us.

Case of 
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=0)
		$state_i:=Fnd_Reg_PreDemo
		
	: (Fnd_Reg_ValidateUnlockCode (<>Fnd_Reg_SecretKey_t;$featureCode_t;<>Fnd_Reg_UserNameFld_ptr->;<>Fnd_Reg_LicenseInfoFld_ptr->;<>Fnd_Reg_UnlockCodeFld_ptr->;<>Fnd_Reg_UnlockCodeFormat_t))
		$state_i:=Fnd_Reg_Registered
		
	: (<>Fnd_Reg_DemoStartDateFld_ptr->=!00-00-00!)
		$state_i:=Fnd_Reg_PreDemo
		
	: (Current date:C33<<>Fnd_Reg_DemoStartDateFld_ptr->)
		  // This prevents the user from setting the clock forward before starting
		  //   the demo period to get an extended demo period.
		$state_i:=Fnd_Reg_Expired
		
	: (Current date:C33<$expirationDate_d)  // Did < rather than <= on purpose, so it matches other code.
		$state_i:=Fnd_Reg_Demo
End case 

$0:=$state_i