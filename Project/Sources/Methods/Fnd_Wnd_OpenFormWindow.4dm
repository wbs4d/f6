//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_OpenFormWindow (->table; form name)

// Opens a new window using the specified form's width and height.

// Access: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the form's table
//   $2 : Text : The name of the form

// Returns: 
//   $0 : Longint : The window reference

// Created by Dave Batton on Jul 14, 2003
// Modified by Dave Batton on Mar 23, 2004
//   The call to GET FORM PROPERTIES was using $1 and $2 directly, instead
//   of using the local variables. This was fixed.
// Modified by Dave Batton on Apr 23, 2005
//   Now detects if the window type hasn't been set, and sets default value.
// Modified by Walt Nelson 9/11/09 fixed when component is compiled and host is intrepreted
// ----------------------------------------------------

C_LONGINT:C283($0; $windowRef_i; $width_i; $height_i; $numPages_i)
C_POINTER:C301($1; $formTable_ptr)
C_TEXT:C284($2; $formName_t)
C_BOOLEAN:C305($fixedWidth_b; $fixedHeight_b)

$formTable_ptr:=$1
$formName_t:=$2

Fnd_Wnd_Init

$windowRef_i:=0

If (Fnd.Wnd.windowType=-1)  // If no window type has been set, set it to a plain window.
	Fnd.Wnd.windowType:=Plain window:K34:13
End if 

//EXECUTE METHOD("Fnd_Host_GetFormProperties";*;$formTable_ptr;$formName_t;->$width_i;->$height_i)
// wwn 9/11/09 when component is compiled and host is intrepreted ->$width_i & ->$height_i are gibberish
EXECUTE METHOD:C1007("Fnd_Host_GetFormProperties"; *; $formTable_ptr; $formName_t; ->Fnd_Wnd_width_i; ->Fnd_Wnd_height_i)
$width_i:=Fnd_Wnd_width_i
$height_i:=Fnd_Wnd_height_i  // width_i & height_i are defined in Compiler_Fnd_Wnd

If ((Fnd.Wnd.windowType=Regular window:K27:1) | (Fnd.Wnd.windowType=Plain window:K34:13))
	If (($fixedWidth_b) & ($fixedHeight_b))  // These are currently ignored in Fnd5.
		Fnd.Wnd.windowType:=Plain fixed size window:K34:6  // So there's no resize object in the window corner.
	End if 
End if 

If (($width_i=0) | ($height_i=0))
	Fnd_Gen_BugAlert(Current method name:C684; "The \""+$formName_t+"\" form does not exist.")
Else 
	$windowRef_i:=Fnd_Wnd_OpenWindow($width_i; $height_i)
End if 

$0:=$windowRef_i