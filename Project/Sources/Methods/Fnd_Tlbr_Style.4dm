//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Style ({style name}) --> Text

// Sets and gets the toolbar style name.
// Can be:
//   "Large"
//   "Prefs"
//   "Small1"
//   "Small2"

// Access Type: Shared

// Parameters: 
//   $1 : Text : The style to use (optional)

// Returns: 
//   $0 : Text : The selected style

// Created by Dave Batton on Jan 16, 2005
// Modified by Dave Batton on Apr 13, 2005
//   Supports the new Prefs toolbar style.
// ----------------------------------------------------

C_TEXT:C284($0;$1;$requestedStyle_t)

Fnd_Tlbr_Init

If (Count parameters:C259>=1)
	$requestedStyle_t:=$1
	
	Case of 
		: ($requestedStyle_t="Large")
			Fnd_Tlbr_Style_t:=$requestedStyle_t
			
		: ($requestedStyle_t="Prefs")
			Fnd_Tlbr_Style_t:=$requestedStyle_t
			
		: ($requestedStyle_t="Small1")
			Fnd_Tlbr_Style_t:=$requestedStyle_t
			
		: ($requestedStyle_t="Small2")
			Fnd_Tlbr_Style_t:=$requestedStyle_t
			
		Else 
			Fnd_Gen_BugAlert(Current method name:C684;"Invalid style name: "+$requestedStyle_t)
	End case 
	
	Fnd_Tlbr_SetVariables
	Fnd_Tlbr_BackgroundIsValid_b:=False:C215
End if 

$0:=Fnd_Tlbr_Style_t
