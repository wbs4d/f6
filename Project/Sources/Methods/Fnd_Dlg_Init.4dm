//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dlg_Init

// Initializes both the process and interprocess variables used by the Fnd_Dlg routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 10, 2003
// Modified by Dave Batton on Feb 5, 2004
//   Updated the version to 4.0.2 (skipped 4.0.1).
// Modified by Dave Batton on May 22, 2004
//   Updated the version number to 4.0.3.
//   The localization component is no longer required.
// Modified by Dave Batton on May 30, 2004
//   Updated the version number to 4.0.3.
// Modified by Dave Batton on Dec 26, 2004
//   Updated the version number to 4.0.5 (skipped 4.0.4).
//   Changed the font style for Mac OS X dialogs from bold to plain.
//   Moved the version number to the new Fnd_Dlg_Info method.
// Modified by Dave Batton on May 12, 2005
//   Fixed some font information that didn't match the style sheets being used.
//   Removed the Fnd_Dlg_IconNumber_i variable and replaced it with the new Fnd_Dlg_IconPictureName_t variable.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_Dlg_Initialized_b; Fnd_Dlg_Initialized_b)
C_LONGINT:C283(OK; $ok_i)

// We want to take care to make sure we don't change the value of the OK variable.
$ok_i:=OK

If (Not:C34(<>Fnd_Dlg_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Dlg
	
	Fnd_Dlg_Localize
	
	<>Fnd_Dlg_MenuBarName_t:=""  // To implement later?
	<>Fnd_Dlg_MessageProcessName_t:="$Progress process"
	
	// These are local, since we can have only one message dialog for all processes.
	<>Fnd_Dlg_WindowTitle_t:=""
	<>Fnd_Dlg_MessageText_t:=""
	<>Fnd_Dlg_ProgFrom_i:=0
	<>Fnd_Dlg_ProgTo_i:=0
	<>Fnd_Dlg_CancelButton_t:="*"  // Default to "Stop".
	<>Fnd_Dlg_CanBeCancelled_b:=False:C215  // Can't cancel unless the developer makes an effort.
	<>Fnd_Dlg_ProgAborted_b:=False:C215
	
	// Define the fonts we'll be using in the alert dialogs.
	Case of 
		: (True:C214)
			<>Fnd_Dlg_FontName_t:=Storage:C1525.font.largeName
			<>Fnd_Dlg_FontSize_i:=Storage:C1525.font.largeSize
			<>Fnd_Dlg_FontStyle_i:=Storage:C1525.font.largeStyle
			<>Fnd_Dlg_FontHeight_i:=Storage:C1525.font.largeHeight
			
			<>Fnd_Dlg_MessageFontName_t:=Storage:C1525.font.smallName
			<>Fnd_Dlg_MessageFontSize_i:=Storage:C1525.font.smallSize
			<>Fnd_Dlg_MessageFontStyle_i:=Storage:C1525.font.smallStyle
			<>Fnd_Dlg_MessageFontHeight_i:=Storage:C1525.font.smallHeight
			
			<>Fnd_Dlg_SmallFontName_t:=Storage:C1525.font.smallName
			<>Fnd_Dlg_SmallFontSize_i:=Storage:C1525.font.smallSize
			<>Fnd_Dlg_SmallFontStyle_i:=Storage:C1525.font.smallStyle
			<>Fnd_Dlg_SmallFontHeight_i:=Storage:C1525.font.smallHeight
			
			
		: (Is Windows:C1573)
			<>Fnd_Dlg_FontName_t:="Tahoma"  // Font info for alert dialogs.  ` DB050512 - Changed from ASI_System to Tahoma.
			<>Fnd_Dlg_FontSize_i:=12
			<>Fnd_Dlg_FontStyle_i:=Plain:K14:1
			<>Fnd_Dlg_FontHeight_i:=14
			
			<>Fnd_Dlg_MessageFontName_t:="Tahoma"  // Font info for message dialogs. ` DB050512 - Changed from ASI_System to Tahoma.
			<>Fnd_Dlg_MessageFontSize_i:=12
			<>Fnd_Dlg_MessageFontStyle_i:=Plain:K14:1
			<>Fnd_Dlg_MessageFontHeight_i:=14
			
		: (Is macOS:C1572)  // Mac OS X.
			<>Fnd_Dlg_FontName_t:="Lucida Grande"  // Usually Lucida Grande
			<>Fnd_Dlg_FontSize_i:=13
			<>Fnd_Dlg_FontStyle_i:=Bold:K14:2  // DB041226 - Changed from Bold.  ` DB050512 - Changed back to bold.
			<>Fnd_Dlg_FontHeight_i:=16
			
			<>Fnd_Dlg_SmallFontName_t:="Lucida Grande"
			<>Fnd_Dlg_SmallFontSize_i:=11
			<>Fnd_Dlg_SmallFontStyle_i:=Plain:K14:1
			<>Fnd_Dlg_SmallFontHeight_i:=13
			
			<>Fnd_Dlg_MessageFontName_t:="Lucida Grande"  // Font info for message dialogs.
			<>Fnd_Dlg_MessageFontSize_i:=11
			<>Fnd_Dlg_MessageFontStyle_i:=Plain:K14:1
			<>Fnd_Dlg_MessageFontHeight_i:=13
			
			//Else   // Mac OS 9.
			//<>Fnd_Dlg_FontName_t:="Chicago"  // ### Is this right?
			//<>Fnd_Dlg_FontSize_i:=12
			//<>Fnd_Dlg_FontStyle_i:=Plain
			//<>Fnd_Dlg_FontHeight_i:=14
			
			//<>Fnd_Dlg_MessageFontName_t:="Geneva"  // Font info for message dialogs.
			//<>Fnd_Dlg_MessageFontSize_i:=9
			//<>Fnd_Dlg_MessageFontStyle_i:=Plain
			//<>Fnd_Dlg_MessageFontHeight_i:=11
	End case 
	
	<>Fnd_Dlg_Initialized_b:=True:C214
End if 


If (Not:C34(Fnd_Dlg_Initialized_b))  // So we only do this once per process.
	Fnd_Dlg_Initialized_b:=True:C214
	
	Fnd.Dlg:=New object:C1471
	
	// Set the default values that the developer will be able to override.
	Fnd_Dlg_Text1_t:=""  // See Fnd_Dlg_SetText.
	Fnd_Dlg_Text2_t:=""
	Fnd_Dlg_Button1_t:=""  // Default
	Fnd_Dlg_Button2_t:=""
	Fnd_Dlg_Button3_t:=""
	Fnd_Dlg_IconPictureName_t:="Fnd_Dlg_OSX_NoteIcon"  // DB050512
	Fnd_Dlg_RequestDefault_t:=""  // No request enterable area.
	Fnd_Dlg_CanBeCancelled_b:=False:C215
	Fnd_Dlg_OKButton_i:=0
	Fnd_Dlg_CancelButton_i:=0
	Fnd_Dlg_OtherButton_i:=0
	Fnd_Dlg_SetProgress(0; 0)
	
	C_PICTURE:C286($pic)
	Fnd.Dlg.IconPicture:=$pic
	
	// Fnd_Dlg_RequestValue_t  ` Don't set this!  If you do, Fnd_Dlg_GetRequest won't work.
End if 

//If (Not(Fnd_Dlg_Initialized_b))  // So we only do this once per process.
//Fnd_Dlg_Initialized_b:=True

//Fnd.Dlg:=New object


//// Set the default values that the developer will be able to override.
//Fnd_Dlg_Text1_t:=""  // See Fnd_Dlg_SetText.
//Fnd_Dlg_Text2_t:=""
//Fnd.Dlg.Button1_t:=""  // Default
//Fnd.Dlg.Button2_t:=""
//Fnd.Dlg.Button3_t:=""
//Fnd.Dlg.IconPictureName_t:="Fnd_Dlg_OSX_NoteIcon"  // DB050512
//Fnd.Dlg.RequestDefault_t:=""  // No request enterable area.
//Fnd.Dlg.CanBeCancelled_b:=False
//Fnd.Dlg.OKButton_i:=0
//Fnd.Dlg.CancelButton_i:=0
//Fnd.Dlg.OtherButton_i:=0
//C_PICTURE($pic)
//Fnd.Dlg.IconPicture:=$pic


//Fnd_Dlg_SetProgress(0; 0)

// Fnd_Dlg_RequestValue_t  ` Don't set this!  If you do, Fnd_Dlg_GetRequest won't work.
//End if 

OK:=$ok_i
