//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_GetFndObject --> ReturnType

// Returns a copy of the Fnd object

// Returns: 
//   $0 : Object : The Fnd objects

// Created by Wayne Stewart (2023-04-24)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE()->$Fnd : Object

Fnd_Init

$Fnd:=OB Copy:C1225(Fnd)