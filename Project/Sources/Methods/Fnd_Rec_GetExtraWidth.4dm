//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Rec_GetExtraWidth --> Number

  // Returns the extra width needed to display the Modify Records dialog so none of the text in the 
  //   pop-up menus gets truncated.
  // Lots of stuff is hard-coded here because we can't get it procedurally.  :-(

  // Access Type: Private

  // Parameters: None

  // Returns: 
  //   $0 : Longint : The extra width required

  // Created by Dave Batton on Dec 4, 2005
  // ----------------------------------------------------

C_LONGINT:C283($0;$popupObjectWidth_i;$extraWidth1_i;$extraWidth2_i;$bestWidth_i)

  // Remember, this is called before the window is displayed, so we can't
  //   use BEST OBJECT SIZE or GET OBJECT RECT.
$popupObjectWidth_i:=265  // Any longer than this and the window needs to be enlarged.

$extraWidth1_i:=0
$extraWidth2_i:=0

  // First for the field names pop-up object.
$bestWidth_i:=Fnd_Rec_BestPopupSize (->Fnd_Rec_FieldNames_at)
If ($bestWidth_i>$popupObjectWidth_i)
	$extraWidth1_i:=$bestWidth_i-$popupObjectWidth_i
End if 

  // Then for the values pop-up object.
$bestWidth_i:=Fnd_Rec_BestPopupSize (->Fnd_Rec_FieldNames_at)
If ($bestWidth_i>$popupObjectWidth_i)
	$extraWidth2_i:=$bestWidth_i-$popupObjectWidth_i
End if 

If ($extraWidth1_i>=$extraWidth2_i)
	$0:=$extraWidth1_i
Else 
	$0:=$extraWidth2_i
End if 
