//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Bttn_MakeIconPicture (icon name) --> Picture


  // Creates a picture containing the 4 icons arranged for the rollover button,

  //   spaced according to the button height.


  // Access Type: Private


  // Parameters: 

  //   $1 : Text : The name of the icon


  // Returns: 

  //   $0 : Picture : A picture of the icon over the background


  // Created by Mark Mitchenall on 17/9/01

  //   Original Method Name: rollover_MakeIconPicture

  //   Part of the toolbar_Component, ©2001 mitchenall.com

  //   Used with permission.

  // Modified by Dave Batton on Feb 20, 2004

  //   Updated specifically for use by the Foundation Shell.

  // Modified by Dave Batton on Jan 8, 2005

  //   Removed the calls to Fnd_Bttn_GetIcon and Fnd_Bttn_GetIcon and replaced

  //      them with a call to the modified Fnd_Bttn_GetIcon method.
  // Modified: [1] John Craig, 02/07/2014, 13:51:17 Changed | picture operator to COMBINE PICTURES with Superimposition method

  // ----------------------------------------------------


C_PICTURE:C286($0;$icons_pic;$iconNormal_pic;$iconSelected_pic;$iconDisabled_pic)
C_TEXT:C284($1;$iconName_t)

$iconName_t:=$1

  //Get the 2 pictures for the icon into local pictures variables


$iconNormal_pic:=Fnd_Bttn_GetIcon ($iconName_t)
If (Fnd_Bttn_Platform_t="Win")
	$iconSelected_pic:=Fnd_Bttn_GetIcon ($iconName_t)
Else 
	$iconSelected_pic:=Fnd_Bttn_GetIcon ($iconName_t;"selected")
End if 
$iconDisabled_pic:=Fnd_Bttn_GetIcon ($iconName_t;"disabled")

  //Now create a picture containing the 4 icons at the correct distances appart

  // Modified: [1] John Craig, 02/07/2014, 13:51:17 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
  //$icons_pic:=$iconNormal_pic+(Fnd_Bttn_HorizonalShift_ai{1})/(Fnd_Bttn_VerticalShift_ai{1})
  //$icons_pic:=$icons_pic | (($iconSelected_pic+(Fnd_Bttn_HorizonalShift_ai{2}))/((Fnd_Bttn_ButtonHeight_i*1)+(Fnd_Bttn_VerticalShift_ai{2})))
  //$icons_pic:=$icons_pic | (($iconNormal_pic+(Fnd_Bttn_HorizonalShift_ai{3}))/((Fnd_Bttn_ButtonHeight_i*2)+(Fnd_Bttn_VerticalShift_ai{3})))
  //$icons_pic:=$icons_pic | (($iconDisabled_pic+(Fnd_Bttn_HorizonalShift_ai{4}))/((Fnd_Bttn_ButtonHeight_i*3)+(Fnd_Bttn_VerticalShift_ai{4})))
COMBINE PICTURES:C987($icons_pic;$icons_pic;Superimposition:K61:10;$iconNormal_pic;Fnd_Bttn_HorizonalShift_ai{1};Fnd_Bttn_VerticalShift_ai{1})
COMBINE PICTURES:C987($icons_pic;$icons_pic;Superimposition:K61:10;$iconSelected_pic;Fnd_Bttn_HorizonalShift_ai{2};(Fnd_Bttn_ButtonHeight_i*1)+(Fnd_Bttn_VerticalShift_ai{2}))
COMBINE PICTURES:C987($icons_pic;$icons_pic;Superimposition:K61:10;$iconNormal_pic;Fnd_Bttn_HorizonalShift_ai{3};(Fnd_Bttn_ButtonHeight_i*2)+(Fnd_Bttn_VerticalShift_ai{3}))
COMBINE PICTURES:C987($icons_pic;$icons_pic;Superimposition:K61:10;$iconDisabled_pic;Fnd_Bttn_HorizonalShift_ai{4};(Fnd_Bttn_ButtonHeight_i*3)+(Fnd_Bttn_VerticalShift_ai{4}))
  // Modified: [1] John Craig, 02/07/2014, 13:51:17 Changed | picture operator to COMBINE PICTURES with Superimposition method <--

  //Return the 4 icons in one picture

$0:=$icons_pic
