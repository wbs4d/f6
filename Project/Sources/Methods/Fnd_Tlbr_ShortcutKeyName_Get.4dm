//%attributes = {"invisible":true,"shared":true}
  //Method: toolbar_ShortcutKeyName_Get (shortcutKeyCode) --> Name

  //Author: Mark Mitchenall

  //Created: 16/4/02 at 2:20 AM

  //Toolbar Component - © mitchenall.com 2002


  //Because it's not easy putting the actual keycode into the buttons on the toolbar

  //Master form for trapping shortcuts, we have names for some of them, and this 

  //routine converts the single character keycode into that name.


C_TEXT:C284($1;$keyCodeChar)
$keyCodeChar:=$1

C_TEXT:C284($0;$keyCodeName)

ARRAY TEXT:C222($keyCodeNames;127)
$keyCodeNames{3}:="Enter"
$keyCodeNames{127}:="Del"
$keyCodeNames{28}:="Left"
$keyCodeNames{29}:="Right"
$keyCodeNames{30}:="Up"
$keyCodeNames{31}:="Down"
$keyCodeNames{8}:="Backspace"
$keyCodeNames{27}:="Esc"
$keyCodeNames{Character code:C91(".")}:="Period"

If ((Character code:C91($keyCodeChar[[1]])>0) & (Character code:C91($keyCodeChar[[1]])<128))
	If ($keyCodeNames{Character code:C91($keyCodeChar[[1]])}#"")
		$keyCodeName:=$keyCodeNames{Character code:C91($keyCodeChar[[1]])}
	Else 
		$keyCodeName:=$keyCodeChar
	End if 
Else 
	$keyCodeName:=$keyCodeChar
End if 

$0:=$keyCodeName