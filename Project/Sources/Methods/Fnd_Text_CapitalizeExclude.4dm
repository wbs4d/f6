//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_CapitalizeExclude (->exclude array)

  // Use this method to specify or determine the words that will not be
  //   capitalized using the Fnd_Text_Capitalize method.
  // To get a list of words that will be excluded from capitalization, pass an empty text array.
  // To set the words to exclude from the capitalization routine, pass an
  //   array with at least one element.
  // To clear the list of words to exclude from the capitalization routines,
  //   pass an array with one element consisting of a blank string.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : Text array of words to exclude

  // Returns: Nothing

  // Created by Dave Batton on May 12, 2004
  // Modified by Dave Batton on Dec 29, 2005
  //   Dereferenced the array pointer that wasn't previously getting dereferenced (dumb bug).
  // ----------------------------------------------------

C_POINTER:C301($1;$excludeWordsArray_ptr)

Fnd_Text_Init 

$excludeWordsArray_ptr:=$1

If (Size of array:C274($excludeWordsArray_ptr->)=0)  // DB051229 - Fixed a bug here.
	COPY ARRAY:C226(<>Fnd_Text_CapExcludeWords_at;$excludeWordsArray_ptr->)
Else 
	If ($excludeWordsArray_ptr->{1}="")
		ARRAY TEXT:C222(<>Fnd_Text_CapExcludeWords_at;0)
	Else 
		COPY ARRAY:C226($excludeWordsArray_ptr->;<>Fnd_Text_CapExcludeWords_at)
	End if 
End if 
