//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Find

  // Compiler variables related to the Foundation Find routines.
  // Called by Fnd_Find_Init.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 16, 2003
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Find_Initialized_b)
If (Not:C34(<>Fnd_Find_Initialized_b))
	ARRAY TEXT:C222(<>Fnd_Find_DistinctPaths_at;0)
	
End if 

C_BOOLEAN:C305(Fnd_Find_Initialized_b)
If (Not:C34(Fnd_Find_Initialized_b))
	C_POINTER:C301(Fnd_Find_Table_ptr;Fnd_Find_Field_ptr)
	
	C_LONGINT:C283(Fnd_Find_OKButton_i;Fnd_Find_CancelButton_i;Fnd_Find_EditorButton_i;Fnd_Find_OffscreenButton_i)
	C_LONGINT:C283(Fnd_Find_AllRadioButton_i;Fnd_Find_SelectionRadioButton_i;Fnd_Find_AddRadioButton_i;Fnd_Find_OmitRadioButton_i)
	C_LONGINT:C283(Fnd_Find_Left_i;Fnd_Find_Top_i;Fnd_Find_Right_i;Fnd_Find_Bottom_i)
	
	C_BOOLEAN:C305(Fnd_Find_Initialized_b;Fnd_Find_Reset_b)
	
	C_TEXT:C284(Fnd_Find_Label_t;Fnd_Find_SearchString_t;Fnd_Find_FieldName_t;Fnd_Find_TableName_t)
	C_TEXT:C284(Fnd_Find_EditorButtonLabel_t;Fnd_Find_EditorButtonMethod_t;Fnd_Find_PrefValue_t)
	
	ARRAY TEXT:C222(Fnd_Find_FieldNames_at;0)
	ARRAY POINTER:C280(Fnd_Find_Fields_aptr;0)
	ARRAY INTEGER:C220(Fnd_Find_FieldTypes_ai;0)
	ARRAY TEXT:C222(Fnd_Find_MethodNames_at;0)
	
	ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;0)
	ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;0)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(Fnd_Find_AddCustom ;$1;$2)
	C_LONGINT:C283(Fnd_Find_AddCustom ;$3)
	
	C_POINTER:C301(Fnd_Find_AddField ;$1)
	C_LONGINT:C283(Fnd_Find_AddField ;$2)
	
	C_LONGINT:C283(Fnd_Find_AddItem ;$1;$5)
	C_TEXT:C284(Fnd_Find_AddItem ;$2;$3)
	C_POINTER:C301(Fnd_Find_AddItem ;$4)
	
	C_POINTER:C301(Fnd_Find_AddMultiField ;$1)
	C_TEXT:C284(Fnd_Find_AddMultiField ;$2)
	C_LONGINT:C283(Fnd_Find_AddMultiField ;$3)
	
	C_LONGINT:C283(Fnd_Find_AddSeparator ;$1)
	
	C_POINTER:C301(Fnd_Find_AddSubfield ;$1)
	C_LONGINT:C283(Fnd_Find_AddSubfield ;$2;$3)
	
	C_POINTER:C301(Fnd_Find_AddTable ;$1)
	C_LONGINT:C283(Fnd_Find_AddTable ;$2)
	
	C_LONGINT:C283(Fnd_Find_BestPopupSize ;$0)
	C_POINTER:C301(Fnd_Find_BestPopupSize ;$1)
	
	C_LONGINT:C283(Fnd_Find_GetExtraWidth ;$0)
	
	C_TEXT:C284(Fnd_Find_Engine ;$1)
	
	C_TEXT:C284(Fnd_Find_Engine2 ;$1;$3)
	C_POINTER:C301(Fnd_Find_Engine2 ;$2)
	
	C_TEXT:C284(Fnd_Find_Engine3 ;$1;$3)
	C_POINTER:C301(Fnd_Find_Engine3 ;$2)
	
	C_TEXT:C284(Fnd_Find_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Find_Now ;$1)
	
	C_TEXT:C284(Fnd_Find_PrefsGet ;$0;$1)
	
	C_TEXT:C284(Fnd_Find_PrefsPut ;$1;$2)
	
	C_TEXT:C284(Fnd_Find_SetEditorButton ;$1;$2)
	
	C_LONGINT:C283(Fnd_Find_SizeArrayObject ;$0;$2)
	C_POINTER:C301(Fnd_Find_SizeArrayObject ;$1)
	
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes ;$1)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes ;$2)
	C_BOOLEAN:C305(Fnd_Find_GetDistinctAttributes ;$3)
	
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes2 ;$1)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes2 ;$2)
	
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes3 ;$1)
	C_LONGINT:C283(Fnd_Find_GetDistinctAttributes3 ;$2)
End if 