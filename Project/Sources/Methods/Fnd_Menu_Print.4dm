//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Print

// Called when Print is selected from the File menu.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 29, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Print component is no longer required.
// ----------------------------------------------------

//If (Fnd_Gen_ComponentAvailable("Fnd_Prnt"))
//EXECUTE METHOD("Fnd_Shell_Print"; *)
//End if 
Fnd_Shell_Print