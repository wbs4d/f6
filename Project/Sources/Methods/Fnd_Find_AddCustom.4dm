//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Find_AddCustom (label; method name{; position})


  // Adds a custom field to the search dialog.


  // Parameters: 

  //   $1 : Text : The label to use

  //   $2 : Text : The method to call

  //   $3 : Longint : The position (optional)


  // Returns: Nothing


  // Created by Dave Batton on Jul 19, 2003

  // ----------------------------------------------------


C_TEXT:C284($1;$2;$label_t;$method_t)
C_LONGINT:C283($3;$position_i)
C_POINTER:C301($null_ptr)

$label_t:=$1
$method_t:=$2

If (Count parameters:C259>=3)
	$position_i:=$3
Else 
	$position_i:=MAXLONG:K35:2
End if 

Fnd_Find_AddItem ($position_i;$label_t;$method_t)