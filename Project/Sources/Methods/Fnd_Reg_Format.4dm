//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_Format (MD5; format) --> Text

  // Returns an unlock code by formatting the MD5 value using the specified format.
  // The # characters in the format string are replaced by characters from the MD5.

  // Example:
  //   Input: ("35bf6fd6b73c1e29ac15a50316c4cc6c";"Test ###-##/###*")
  //   Output: "Test 35b-f6/fd6*"

  // Access: Private

  // Parameters: 
  //   $1 : Text : An MD5 value
  //   $2 : Text : A format description

  // Returns: 
  //   $0 : Text : The formatted value

  // Created by Dave Batton on Mar 6, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$md5_t;$format_t)
C_LONGINT:C283($formatPosition_i;$md5Position_i)

$md5_t:=$1
$format_t:=$2

$md5Position_i:=1

For ($formatPosition_i;1;Length:C16($format_t))
	If ($format_t[[$formatPosition_i]]="#")
		$format_t:=Change string:C234($format_t;$md5_t[[$md5Position_i]];$formatPosition_i)
		$md5Position_i:=$md5Position_i+1
	End if 
End for 

$0:=$format_t
