//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_BuildMenuBar

// Testing

// Access: Private

// Parameters:
//   $1 : Longint : Description

// Returns:
//   $0 : Longint : Description

// Created by Dave Batton on Sep 26, 2007
// wwn (07/02/09) uncommented this line because ◊Fnd_Menu_WindowMenu_s was blank
// wwn 10/20/09 uncommented line 81 to call Foundation Preferences instead of 4D's
// SET MENU ITEM METHOD(◊Fnd_Menu_EditMenu_s;-1;"Fnd_Menu_Preferences")
// Uncommented line 49 to put Quit command under the Application menu instead of File menu - Barclay Barry
// SET MENU ITEM PROPERTY(◊Fnd_Menu_FileMenu_s;-1;Associated Standard Action ;Quit Action )
// Modified by Ed Heckman, October 27, 2015
// Modified by: Dave Nasralla (5/1/18) - SET MENU ITEM PROPERTY calls updated with new ak constants (Standard actions fail with old constants if method is modified)
// JDH 20230207 Find Related menu Item added
// ----------------------------------------------------

C_TEXT:C284($0)


//Create the File menu.
<>Fnd_Menu_FileMenu_t:=Create menu:C408

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "FileOpenTable"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_FileMenu_t; -1; "Fnd_Menu_OpenTable")
//◊Fnd_Menu_FileOpenItem_s:=Get menu item reference(◊Fnd_Menu_FileMenu_s;-1)
<>Fnd_Menu_FileOpenItem_i:=Count menu items:C405(<>Fnd_Menu_FileMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "FileAdministration"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_FileMenu_t; -1; "Fnd_Menu_Administration")
//◊Fnd_Menu_FileAdminItem_s:=Get menu item reference(◊Fnd_Menu_FileMenu_s;-1)
<>Fnd_Menu_FileAdminItem_i:=Count menu items:C405(<>Fnd_Menu_FileMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "FilePrint"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_FileMenu_t; -1; "Fnd_Menu_Print")
//◊Fnd_Menu_FilePrintItem_s:=Get menu item reference(◊Fnd_Menu_FileMenu_s;-1)
<>Fnd_Menu_FilePrintItem_i:=Count menu items:C405(<>Fnd_Menu_FileMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_FileMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "FileQuit"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_FileMenu_t; -1; Associated standard action name:K28:8; ak quit:K76:61)
SET MENU ITEM METHOD:C982(<>Fnd_Menu_FileMenu_t; -1; "Fnd_Menu_Quit")

//Create the Edit menu.
<>Fnd_Menu_EditMenu_t:=Create menu:C408

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditUndo"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak undo:K76:51)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditCut"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak cut:K76:53)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditCopy"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak copy:K76:54)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditPaste"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak paste:K76:55)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditClear"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak clear:K76:56)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditSelectAll"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak select all:K76:57)

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_EditMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EditPreferences"))
SET MENU ITEM PROPERTY:C973(<>Fnd_Menu_EditMenu_t; -1; Associated standard action name:K28:8; ak database settings:K76:59)
SET MENU ITEM METHOD:C982(<>Fnd_Menu_EditMenu_t; -1; "Fnd_Menu_Preferences")
<>Fnd_Menu_EditPreferencesItem_t:=Get menu item parameter:C1003(<>Fnd_Menu_EditMenu_t; -1)


//Create the Enter menu.
<>Fnd_Menu_EnterMenu_t:=Create menu:C408

APPEND MENU ITEM:C411(<>Fnd_Menu_EnterMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EnterNewRecord"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_EnterMenu_t; -1; "Fnd_Menu_NewRecord")
//◊Fnd_Menu_EnterNewRecordItem_s:=Get menu item reference(◊Fnd_Menu_EnterMenu_s;-1)
<>Fnd_Menu_EnterNewRecordItem_i:=Count menu items:C405(<>Fnd_Menu_EnterMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_EnterMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_EnterMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EnterModifyRecords"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_EnterMenu_t; -1; "Fnd_Menu_ModifyRecords")
//◊Fnd_Menu_EnterModifyRecsItem_s:=Get menu item reference(◊Fnd_Menu_EnterMenu_s;-1)
<>Fnd_Menu_EnterModifyRecsItem_i:=Count menu items:C405(<>Fnd_Menu_EnterMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_EnterMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_EnterMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "EnterDeleteSubset"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_EnterMenu_t; -1; "Fnd_Menu_Delete")
//◊Fnd_Menu_EnterDeleteSetItem_s:=Get menu item reference(◊Fnd_Menu_EnterMenu_s;-1)
<>Fnd_Menu_EnterDeleteSetItem_i:=Count menu items:C405(<>Fnd_Menu_EnterMenu_t)


//Create the Select menu.
<>Fnd_Menu_SelectMenu_t:=Create menu:C408

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectShowAll"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_ShowAll")
//◊Fnd_Menu_SelectShowAllItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectShowAllItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectShowSubset"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_ShowSubset")
//◊Fnd_Menu_SelectShowSetItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectShowSetItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectOmitSubset"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_OmitSubset")
//◊Fnd_Menu_SelectOmitSetItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectOmitSetItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectFind"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_Find")
//◊Fnd_Menu_SelectFindItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectFindItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectQueryEditor"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_QueryEditor")
//◊Fnd_Menu_SelectQueryEdItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectQueryEdItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectSort"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_Sort")
//◊Fnd_Menu_SelectSortItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectSortItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectSortEditor"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_SortOrderEditor")
//◊Fnd_Menu_SelectOrderByEdItem_s:=Get menu item reference(◊Fnd_Menu_SelectMenu_s;-1)
<>Fnd_Menu_SelectOrderByEdItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

// JDH 20230207 Find Related menu Item -------------------------
APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_SelectMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectFindRelated"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_SelectMenu_t; -1; "Fnd_Menu_FindRelated")
<>Fnd_Menu_SelectFindReltdItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)
// -------------------------------------------------------------

//Create the Tools menu.
<>Fnd_Menu_ToolsMenu_t:=Create menu:C408

APPEND MENU ITEM:C411(<>Fnd_Menu_ToolsMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "ToolsNavPalette"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_ToolsMenu_t; -1; "Fnd_Menu_NavPalette")
//◊Fnd_Menu_ToolsNavPaletteItem_s:=Get menu item reference(◊Fnd_Menu_ToolsMenu_s;-1)
<>Fnd_Menu_ToolsNavPaletteItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_ToolsMenu_t; "(-")  // Separator line

APPEND MENU ITEM:C411(<>Fnd_Menu_ToolsMenu_t; Fnd_Loc_GetString("Fnd_Menu"; "ToolsSpecialFunctions"))
SET MENU ITEM METHOD:C982(<>Fnd_Menu_ToolsMenu_t; -1; "Fnd_Menu_SpecialFunctions")
//◊Fnd_Menu_ToolsSpecialFnctItem_s:=Get menu item reference(◊Fnd_Menu_ToolsMenu_s;-1)
<>Fnd_Menu_ToolsSpecialFnctItem_i:=Count menu items:C405(<>Fnd_Menu_SelectMenu_t)


//Create the Window menu.
<>Fnd_Menu_WindowMenu_t:=Create menu:C408
// wwn (07/02/09) uncommented this line because ◊Fnd_Menu_WindowMenu_s was blank

// The following block of commands add the menus to the menu bar
// Create the menu bar made up of other menus.
<>Fnd_Menu_MenuBar_t:=Create menu:C408
APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "FileMenu"); <>Fnd_Menu_FileMenu_t)
APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "EditMenu"); <>Fnd_Menu_EditMenu_t)
APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "EnterMenu"); <>Fnd_Menu_EnterMenu_t)
APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "SelectMenu"); <>Fnd_Menu_SelectMenu_t)
APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "ToolsMenu"); <>Fnd_Menu_ToolsMenu_t)

APPEND MENU ITEM:C411(<>Fnd_Menu_MenuBar_t; Fnd_Loc_GetString("Fnd_Menu"; "WindowMenu"); <>Fnd_Menu_WindowMenu_t)
// wwn (07/02/09) uncommented this line because ◊Fnd_Menu_WindowMenu_s was blank

$0:=<>Fnd_Menu_MenuBar_t