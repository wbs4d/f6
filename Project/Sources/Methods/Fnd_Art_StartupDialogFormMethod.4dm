//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Art_StartupDialogFormMethod

  // Must be called from the startup dialog's form method, even
  //   if a custom form is used.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Dec 21, 2003
  // ----------------------------------------------------

Case of 
	: (Form event code:C388=On Load:K2:1)
		Form:C1466.MinSecondsOpen:=3
		Form:C1466.Art_SoonestCloseTime:=Tickcount:C458+(Form:C1466.MinSecondsOpen*60)
		Form:C1466.CloseRequested:=False:C215
		SET TIMER:C645(60)
		
		
	: (Form event code:C388=On Timer:K2:25)
		Case of 
			: ((Tickcount:C458>Form:C1466.Art_SoonestCloseTime) & (Form:C1466.CloseRequested))
				CANCEL:C270
				
			: (Tickcount:C458>(Form:C1466.Art_SoonestCloseTime+1800))  // Just in case, close after three minutes
				CANCEL:C270
				
		End case 
		
End case 
