//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_MenuBar

  // A (hopefully) temporary method that asks the menu bar component to
  //   install the menu bar after a short delay. Must be run from a new process,
  //   but this method handles that.
  // This is necessary because a menu bar can't be installed at startup with
  //   the first version of 4D v11 SQL.

  // Access: Private

  // Parameters: 
  //   $1 : Longint : Used internally

  // Returns: Nothing

  // Created by Dave Batton on Oct 24, 2007
  // ----------------------------------------------------

C_LONGINT:C283($1;$processID_i)

If (Count parameters:C259=1)
	While (Process number:C372("$Fnd_Art_StartupDialog")>0)
		DELAY PROCESS:C323(Current process:C322;10)
	End while 
	
	  //Fnd_Menu_MenuBar ("*")  ` Thought this would work. It doesn't.
	POST KEY:C465(Character code:C91("s");Command key mask:K16:1;1)  // Trigger a menu item that calls Fnd_Shell_OnStartup.
	
Else 
	$processID_i:=New process:C317(Current method name:C684;Fnd_Gen_DefaultStackSize;"$Fnd_Shell menu bar installer";1)
End if 
