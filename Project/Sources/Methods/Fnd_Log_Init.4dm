//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_Init

// Initializes both the process and interprocess variables used by
//   the logging routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 25, 2004
// Mod by Wayne Stewart, (2021-04-05) - Discarded the platform stuff
// Mod by Wayne Stewart, (2021-04-15) - Implement new log system
// ----------------------------------------------------

Fnd_Init

// Run once per launch

If (Storage:C1525.Fnd.Log=Null:C1517)
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Log:=New shared object:C1526(\
			"tab";Char:C90(Tab:K15:37);\
			"return";Char:C90(Carriage return:K15:38)+Char:C90(Line feed:K15:40);\
			"logsFolder";Get 4D folder:C485(Logs folder:K5:19);\
			"maxLogSize";0;\
			"defaultLog";Fnd_Gen_GetDatabaseInfo("DatabaseName")+" log.txt")
	End use 
End if 

// Run every process
If (Fnd.Log=Null:C1517)
	
	Compiler_Fnd_Log
	
	Fnd.Log:=New object:C1471
	Fnd.Log.Enabled:=False:C215
	Fnd.Log.file:=Storage:C1525.Fnd.Log.defaultLog
	Fnd.Log.folder:=Storage:C1525.Fnd.Log.logsFolder
	
End if 