//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Sort_PrefsGet (pref name) --> Text

// Gets a value from the user preferences if the Fnd_Pref component is available.

// Access: Private

// Parameters: 
//   $1 : Text : The preference name

// Returns: 
//   $0 : Text : The saved value

// Created by Dave Batton on Nov 8, 2004
// ----------------------------------------------------

C_TEXT:C284($0; $1; $prefValue_t)

$prefValue_t:=""

//If (Fnd_Gen_ComponentAvailable("Fnd_Pref"))
//EXECUTE METHOD("Fnd_Pref_GetText"; $prefValue_t; $1)
//End if 
$prefValue_t:=Fnd_Pref_GetText($1)

$0:=$prefValue_t

$prefValue_t:=""
