//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_SqNo_CreateTable

// Creates the 

// Access: Private


// Created by Wayne Stewart (2017-03-24)
//     waynestewart@mac.com
// ----------------------------------------------------


// ----------------------------------------------------
// Project Method: Fnd_SqNo_CreateTable

// Global and IP variables accessed:
C_POINTER:C301(<>Fnd_SqNo_Table_ptr; <>Fnd_SqNo_GroupNameFld_ptr; <>Fnd_SqNo_DesignerOnlyFld_ptr)

// Method Type:    Private

// Parameters:    None Used

// Local Variables:
C_BOOLEAN:C305($Found_b)
C_LONGINT:C283($CurrentTableNumber_i; $Delay_i; $ProcessNumber_i)
C_TEXT:C284($SQLCommandText_t)
ARRAY POINTER:C280($FieldsToIndex_aptr; 0)

// Returns:    Nothing

// Created by Wayne Stewart (Feb 27, 2009)
//     waynestewart@mac.com
//  Wayne Stewart (Jun 18, 2009) Added code to delay until table is created
//   Moved table detection to same process, previously this created a new process every time
// ----------------------------------------------------




$Found_b:=False:C215
$Delay_i:=180

For ($CurrentTableNumber_i; 1; Get last table number:C254)
	If (Is table number valid:C999($CurrentTableNumber_i))
		If (Table name:C256($CurrentTableNumber_i)="Fnd_SqNo")
			$Found_b:=True:C214
			$CurrentTableNumber_i:=Get last table number:C254+1
		End if 
	End if 
End for 

If ($Found_b)
Else 
	If (Current process:C322=Process number:C372("Application Process"))
		Fnd_Gen_WaitToContinue(True:C214)
		$ProcessNumber_i:=New process:C317(Current method name:C684; 128*1024; "Create Sequence Number Table")
		While (Fnd_Gen_WaitToContinue)
		End while 
	Else 
		
		If (Not:C34($Found_b))
			$SQLCommandText_t:="CREATE TABLE Fnd_SqNo ("
			$SQLCommandText_t:=$SQLCommandText_t+"ID INT32, "
			$SQLCommandText_t:=$SQLCommandText_t+"Group_Name VARCHAR(80), "
			$SQLCommandText_t:=$SQLCommandText_t+"Next_Number INT32, "
			$SQLCommandText_t:=$SQLCommandText_t+"Recycle_Bin BLOB, "
			$SQLCommandText_t:=$SQLCommandText_t+"Designer_Only BOOLEAN"
			$SQLCommandText_t:=$SQLCommandText_t+")"
			
			Begin SQL
				EXECUTE IMMEDIATE :$SQLCommandText_t;
			End SQL
			
			
			DELAY PROCESS:C323(Current process:C322; $Delay_i)
			
			
			EXECUTE FORMULA:C63("<>Fnd_SqNo_Table_ptr:=->[Fnd_SqNo]")
			EXECUTE FORMULA:C63("<>Fnd_SqNo_GroupNameFld_ptr:=->[Fnd_SqNo]Group_Name")
			EXECUTE FORMULA:C63("<>Fnd_SqNo_DesignerOnlyFld_ptr:=->[Fnd_SqNo]Designer_Only")
			
			ARRAY POINTER:C280($FieldsToIndex_aptr; 1)
			$FieldsToIndex_aptr{1}:=<>Fnd_SqNo_DesignerOnlyFld_ptr
			CREATE INDEX:C966(<>Fnd_SqNo_Table_ptr->; $FieldsToIndex_aptr; Cluster BTree index:K58:4; "Fnd_SqNo_DesignerOnly_Cluster")
			
			ARRAY POINTER:C280($FieldsToIndex_aptr; 1)
			$FieldsToIndex_aptr{1}:=<>Fnd_SqNo_GroupNameFld_ptr
			CREATE INDEX:C966(<>Fnd_SqNo_Table_ptr->; $FieldsToIndex_aptr; Standard BTree index:K58:3; "Fnd_SqNo_GroupName_BTree")
			
			
		End if 
		Fnd_Gen_WaitToContinue(False:C215)
		
	End if 
End if 
