//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_LibraryToResource

// Takes all the pictures in the 4D Picture Library and writes them to
//  a separate .png file for each picture in the library in the
//  4D Resources folder in a folder named Fnd_Bttn_Pictures.
// Used by Fnd_Bttn subcomponent to allow developer to easily edit
//  or replace the icons used throughout Foundation.
// Special thanks to John Craig for this implementation.

// Access Type: Shared

// Parameters: 

// Returns: 

// Created by John Craig, 12/10/2009
// Modified by Walt Nelson on 02/11/10 to match Foundation naming convention
// Modified: [1] John Craig, 02/07/2014, 13:39:15 Changed | picture operator to COMBINE PICTURES with Superimposition method
// ----------------------------------------------------


C_PICTURE:C286($picture_pic)
C_TEXT:C284($path_t; $separator_t; $bttn_Name_t)
C_LONGINT:C283($count_i; $bttn_Size_i; $found_i; $icon_Num_i)

ARRAY LONGINT:C221($picRef_ai; 0)
ARRAY TEXT:C222($picName_at; 0)
PICTURE LIBRARY LIST:C564($picRef_ai; $picName_at)
SORT ARRAY:C229($picName_at; $picRef_ai; >)

ARRAY TEXT:C222($bttn_Name_at; 0)
//Horizontal and vertical shift of the icon and text for each frame.
ARRAY INTEGER:C220($Fnd_Bttn_HorizonalShift_ai; 4)
ARRAY INTEGER:C220($Fnd_Bttn_VerticalShift_ai; 4)

$Fnd_Bttn_HorizonalShift_ai{1}:=0  //Normal
$Fnd_Bttn_VerticalShift_ai{1}:=0
$Fnd_Bttn_HorizonalShift_ai{2}:=0  //Clicked
$Fnd_Bttn_VerticalShift_ai{2}:=0
$Fnd_Bttn_HorizonalShift_ai{3}:=0  //MouseOver
$Fnd_Bttn_VerticalShift_ai{3}:=0
$Fnd_Bttn_HorizonalShift_ai{4}:=0  //Disabled
$Fnd_Bttn_VerticalShift_ai{4}:=0

For ($count_i; 1; Size of array:C274($picName_at))
	$bttn_Name_t:=""
	Case of 
		: ($picName_at{$count_i}="Fnd_Bttn_16_@")
			$bttn_Name_t:=Substring:C12($picName_at{$count_i}; 13)
			$bttn_Name_t:=Substring:C12($bttn_Name_t; 1; Length:C16($bttn_Name_t)-2)
			$bttn_Size_i:=16
		: ($picName_at{$count_i}="Fnd_Bttn_24_@")
			$bttn_Name_t:=Substring:C12($picName_at{$count_i}; 13)
			$bttn_Name_t:=Substring:C12($bttn_Name_t; 1; Length:C16($bttn_Name_t)-2)
			$bttn_Size_i:=24
		: ($picName_at{$count_i}="Fnd_Bttn_32_@")
			$bttn_Name_t:=Substring:C12($picName_at{$count_i}; 13)
			$bttn_Name_t:=Substring:C12($bttn_Name_t; 1; Length:C16($bttn_Name_t)-2)
			$bttn_Size_i:=32
	End case 
	If ($bttn_Name_t#"")
		$found_i:=Find in array:C230($bttn_Name_at; $bttn_Name_t+"_"+String:C10($bttn_Size_i))
		If ($found_i=-1)
			APPEND TO ARRAY:C911($bttn_Name_at; $bttn_Name_t+"_"+String:C10($bttn_Size_i))
		End if 
	End if 
	
End for 

SORT ARRAY:C229($bttn_Name_at; >)
C_PICTURE:C286($Icon_a_pic; $Icon_b_pic; $Icon_c_pic; $Bttn_pic)
$separator_t:=Folder separator:K24:12
$path_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Fnd_Bttn_Pictures"

If (Not:C34(Test path name:C476($path_t)=Is a folder:K24:2))
	CREATE FOLDER:C475($path_t)
End if 

$icon_Num_i:=0
ARRAY TEXT:C222($icon_Name_at; 0)
For ($count_i; 1; Size of array:C274($bttn_Name_at))
	$bttn_Size_i:=Num:C11(Substring:C12($bttn_Name_at{$count_i}; Length:C16($bttn_Name_at{$count_i})-1; 2))
	$bttn_Name_t:=Substring:C12($bttn_Name_at{$count_i}; 1; Length:C16($bttn_Name_at{$count_i})-3)
	
	GET PICTURE FROM LIBRARY:C565("Fnd_Bttn_"+String:C10($bttn_Size_i)+"_"+$bttn_Name_t+"_a"; $Icon_a_pic)
	GET PICTURE FROM LIBRARY:C565("Fnd_Bttn_"+String:C10($bttn_Size_i)+"_"+$bttn_Name_t+"_b"; $Icon_b_pic)
	GET PICTURE FROM LIBRARY:C565("Fnd_Bttn_"+String:C10($bttn_Size_i)+"_"+$bttn_Name_t+"_c"; $Icon_c_pic)
	
	// Modified: [1] John Craig, 02/07/2014, 13:39:15 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
	
	//$Bttn_pic:=$Icon_a_pic+($Fnd_Bttn_HorizonalShift_ai{1})/($Fnd_Bttn_VerticalShift_ai{1})
	//$Bttn_pic:=$Bttn_pic | (($Icon_b_pic+($Fnd_Bttn_HorizonalShift_ai{2}))/(($bttn_Size_i*1)+($Fnd_Bttn_VerticalShift_ai{2})))
	//$Bttn_pic:=$Bttn_pic | (($Icon_a_pic+($Fnd_Bttn_HorizonalShift_ai{3}))/(($bttn_Size_i*2)+($Fnd_Bttn_VerticalShift_ai{3})))
	//$Bttn_pic:=$Bttn_pic | (($Icon_c_pic+($Fnd_Bttn_HorizonalShift_ai{4}))/(($bttn_Size_i*3)+($Fnd_Bttn_VerticalShift_ai{4})))
	
	COMBINE PICTURES:C987($Bttn_pic; $Bttn_pic; Superimposition:K61:10; $Icon_a_pic; $Fnd_Bttn_HorizonalShift_ai{1}; $Fnd_Bttn_VerticalShift_ai{1})
	COMBINE PICTURES:C987($Bttn_pic; $Bttn_pic; Superimposition:K61:10; $Icon_b_pic; $Fnd_Bttn_HorizonalShift_ai{2}; ($bttn_Size_i*1)+($Fnd_Bttn_VerticalShift_ai{2}))
	COMBINE PICTURES:C987($Bttn_pic; $Bttn_pic; Superimposition:K61:10; $Icon_a_pic; $Fnd_Bttn_HorizonalShift_ai{3}; ($bttn_Size_i*2)+($Fnd_Bttn_VerticalShift_ai{3}))
	COMBINE PICTURES:C987($Bttn_pic; $Bttn_pic; Superimposition:K61:10; $Icon_c_pic; $Fnd_Bttn_HorizonalShift_ai{4}; ($bttn_Size_i*3)+($Fnd_Bttn_VerticalShift_ai{4}))
	
	// Modified: [1] John Craig, 02/07/2014, 13:39:15 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
	
	$found_i:=Find in array:C230($icon_Name_at; $bttn_Name_t)
	If ($found_i=-1)
		$icon_Num_i:=$icon_Num_i+1
		APPEND TO ARRAY:C911($icon_Name_at; $bttn_Name_t)
	End if 
	
	SET PICTURE TO LIBRARY:C566($Bttn_pic; ($bttn_Size_i*1000)+$icon_Num_i; "Fnd_Bttn_"+$bttn_Name_t+"_"+String:C10($bttn_Size_i))
	WRITE PICTURE FILE:C680($path_t+$separator_t+"Fnd_Bttn_"+$bttn_Name_t+"_"+String:C10($bttn_Size_i)+".png"; $Bttn_pic; ".png")
End for 


ARRAY LONGINT:C221($picRef_ai; 0)
ARRAY TEXT:C222($picName_at; 0)

PICTURE LIBRARY LIST:C564($picRef_ai; $picName_at)

For ($count_i; 1; Size of array:C274($picName_at))
	GET PICTURE FROM LIBRARY:C565($picRef_ai{$count_i}; $picture_pic)
	If (OK=1)
		WRITE PICTURE FILE:C680($path_t+$separator_t+$picName_at{$count_i}+".png"; $picture_pic; ".png")
	End if 
End for 

Alert2("Finished writing all Picture Library items to Folder: "+$path_t+".")