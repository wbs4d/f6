//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_SetCancelable (cancelable?)

  // Specify whether or not the upcoming dialog can be canceled.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : True if the message button should be enabled.

  // Returns: Nothing

  // Created by Dave Batton on Jul 26, 2003
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Removed extraneous return description in header
  // ----------------------------------------------------

C_BOOLEAN:C305($1)

Fnd_Dlg_Init 

Fnd_Dlg_CanBeCancelled_b:=$1

If ((Fnd_Dlg_CanBeCancelled_b) & (Fnd_Dlg_Button1_t=""))
	Fnd_Dlg_Button1_t:="*"  // Set the Cancel button to the default text.
End if 
