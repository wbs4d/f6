//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Gen_Reg_Register (activation code; user name)


  // Call to register the product.  Sets the OK variable to 1 if the product gets registered.

  //   Sets the Error variable if there's a problem.

  // This doesn't check to see if it's already been registered, so it can be called

  //   to change the registration at any time.  But only does the update if the

  //   user name and activation code are valid.


  // Method Type: Private


  // Parameters: 

  //   $1 : Text : Activation code

  //   $2 : Text : User name


  // Returns: 

  //   $0 : Longint : Description


  // Created by Dave Batton on Mar 14, 2003

  // ----------------------------------------------------


C_TEXT:C284($1;$2;$activationCode_t;$userName_t)

$activationCode_t:=$1
$userName_t:=$2

Error:=0

If (Fnd_Gen_Reg_Validate ($userName_t;$activationCode_t))
	Fnd_Gen_Reg_SaveRegInfo ($userName_t;$activationCode_t)
	Fnd_Gen_Reg_UpdateState 
	
Else 
	Error:=18901  // Incorrect registration info entered.

End if 

If (Error=0)
	OK:=1
Else 
	OK:=0
End if 
