//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_CancelCloseAll

// Call this to end Foundation's attempts to close all windows.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 17, 2003
// ----------------------------------------------------

Fnd_Wnd_Init

<>Fnd_Wnd_Closing_b:=False:C215
