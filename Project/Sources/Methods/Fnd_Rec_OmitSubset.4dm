//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_OmitSubset

// Omits the highlighted records from the current selection.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 1, 2003
// Modified by Dave Batton on Feb 20, 2004
//   Added a call to Fnd_Grid_OmitSubset if the Grid component is available.
// Mod by Wayne Stewart, (2022-04-21) - Removed Fnd_Grid and modified Fnd_Out code for ORDA
// ----------------------------------------------------


Case of 
	: (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
		Form:C1466.listData:=Form:C1466.listData.minus(Form:C1466.selectedItems).copy()
		Fnd_Out_Update
		
	: (Records in set:C195("UserSet")>0)
		CREATE SET:C116(Fnd_Gen_CurrentTable->;"Fnd_Rec_NewSet")
		CREATE EMPTY SET:C140(Fnd_Gen_CurrentTable->;"Fnd_Rec_CopyUserSet")
		COPY SET:C600("UserSet";"Fnd_Rec_CopyUserSet")
		DIFFERENCE:C122("Fnd_Rec_NewSet";"Fnd_Rec_CopyUserSet";"Fnd_Rec_NewSet")
		USE SET:C118("Fnd_Rec_NewSet")
		CLEAR SET:C117("Fnd_Rec_NewSet")
		CLEAR SET:C117("Fnd_Rec_CopyUserSet")
		Fnd_Gen_SelectionChanged  // Call this any time the selection is changed.
End case 