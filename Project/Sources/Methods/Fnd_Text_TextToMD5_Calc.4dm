//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_TextToMD5_Calc ("one_of_7_commands") --> Text

  // The routine that calculates the MD5 hash.
  // This code was provided to me by 4D SA and is used with permission.
  // I've left it basically untouched, other than the method and variable names.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The command ("CALC" is the only one needed)
  //   $2 : Text : The text to manipulate
  //   $3..$9 : Undocumented

  // Returns: 
  //   $0 : Text : The MD5 hash

  // Created by Dave Batton on Mar 6, 2005
  // Based on code from 4D SA.
  // ----------------------------------------------------

  //--- extracted from 4D Business Kit 2.0 with permission / 4D SA 2002 ---

  //calculate the MD5 signature of a string

C_TEXT:C284($1)  //command
C_TEXT:C284($2)
C_POINTER:C301($3)
C_LONGINT:C283($4;$5;$6)
C_LONGINT:C283($7)
C_LONGINT:C283($8)
C_LONGINT:C283($9)

C_TEXT:C284($0)

C_LONGINT:C283($FFFFFFFF)  // FF FF FF FF
$FFFFFFFF:=(255 << 0) | (255 << 8) | (255 << 16) | (255 << 24)

  // NB: not($1) =  ($1 ^| $FFFFFFFF)

Case of 
		
	: ($1="INIT")
		ARRAY LONGINT:C221(<>Fnd_Text_Text2MD5_SINUS_ai;64)
		  // Use a 64-element table constructed from the sine function.
		  // Let T[i] denote the i-th element of the table, which is equal to
		  // the integer part of (2^32) * abs (sin (i)), where i is in radians.
		
		C_LONGINT:C283($k)
		For ($k;1;64)
			If (Int:C8((Abs:C99(Sin:C17($k)))*(2^32))>(2^31))
				<>Fnd_Text_Text2MD5_SINUS_ai{$k}:=Int:C8((Abs:C99(Sin:C17($k)))*(2^32))-(2^32)
			Else 
				<>Fnd_Text_Text2MD5_SINUS_ai{$k}:=Int:C8((Abs:C99(Sin:C17($k)))*(2^32))
			End if 
		End for 
		
	: ($1="CALC")  //the main call
		  //$2:string we want to calculate MD5
		
		  //beyond 56 chars, we need 2 blocks (56 * 8 = 448)
		
		If (Undefined:C82(<>Fnd_Text_Text2MD5_SINUS_ai))
			Fnd_Text_TextToMD5_Calc ("INIT")
		End if 
		
		C_LONGINT:C283($L_Nb_BLOCs)
		$L_Nb_BLOCs:=((Length:C16($2)+8) >> 6)+1
		
		If (Undefined:C82(<>Fnd_Text_Text2MD5_SINUS_ai))
			Fnd_Text_TextToMD5_Calc ("INIT")
		End if 
		
		ARRAY LONGINT:C221(Fnd_Text_TextToMD5_M_WORDs_ai;0)
		
		For ($i;1;$L_Nb_BLOCs*16)
			
			INSERT IN ARRAY:C227(Fnd_Text_TextToMD5_M_WORDs_ai;Size of array:C274(Fnd_Text_TextToMD5_M_WORDs_ai)+1)
			Fnd_Text_TextToMD5_M_WORDs_ai{Size of array:C274(Fnd_Text_TextToMD5_M_WORDs_ai)}:=0
			
		End for 
		
		If (Length:C16($2)>0)
			C_LONGINT:C283($LL)
			For ($i;1;Length:C16($2))
				
				$LL:=(($i-1) >> 2)+1
				Fnd_Text_TextToMD5_M_WORDs_ai{$LL}:=Fnd_Text_TextToMD5_M_WORDs_ai{$LL} | (Character code:C91($2[[$i]]) << ((($i-1)%4)*8))
				
			End for 
			
			$LL:=(($i-1) >> 2)+1
			  //a single "1" bit is appended to the message
			Fnd_Text_TextToMD5_M_WORDs_ai{$LL}:=Fnd_Text_TextToMD5_M_WORDs_ai{$LL} | (0x0080 << ((($i-1)%4)*8))
			
			  // Step 2. Append length (in bits)
			Fnd_Text_TextToMD5_M_WORDs_ai{($L_Nb_BLOCs*16)-1}:=Length:C16($2)*8
			
		Else 
			Fnd_Text_TextToMD5_M_WORDs_ai{1}:=0x0080  //a single "1" bit is appended to the message.
		End if 
		
		  // Step 3. Initialize MD Buffer
		C_LONGINT:C283(Fnd_Text_TextToMD5_wd_A_i)  // 01 23 45 67
		Fnd_Text_TextToMD5_wd_A_i:=(1 << 0) | (35 << 8) | (69 << 16) | (103 << 24)
		
		C_LONGINT:C283(Fnd_Text_TextToMD5_wd_B_i)  // 89 AB CD EF
		Fnd_Text_TextToMD5_wd_B_i:=(137 << 0) | (171 << 8) | (205 << 16) | (239 << 24)
		
		C_LONGINT:C283(Fnd_Text_TextToMD5_wd_C_i)  // FE DC BA 98
		Fnd_Text_TextToMD5_wd_C_i:=(254 << 0) | (220 << 8) | (186 << 16) | (152 << 24)
		
		C_LONGINT:C283(Fnd_Text_TextToMD5_wd_D_i)  // 76 54 32 10
		Fnd_Text_TextToMD5_wd_D_i:=(118 << 0) | (84 << 8) | (50 << 16) | (16 << 24)
		
		C_LONGINT:C283($i;$j)
		
		  //loops on 512 blocks
		For ($i;0;$L_Nb_BLOCs-1)
			
			
			ARRAY LONGINT:C221(Fnd_Text_TextToMD5_X_WORDs_ai;0)
			
			  //loops on each of the 16 words of the 512 block
			For ($j;1;16)
				
				INSERT IN ARRAY:C227(Fnd_Text_TextToMD5_X_WORDs_ai;Size of array:C274(Fnd_Text_TextToMD5_X_WORDs_ai)+1)
				Fnd_Text_TextToMD5_X_WORDs_ai{Size of array:C274(Fnd_Text_TextToMD5_X_WORDs_ai)}:=Fnd_Text_TextToMD5_M_WORDs_ai{($i*16)+$j}
				
			End for 
			
			
			C_LONGINT:C283($L_OLD_A)
			$L_OLD_A:=Fnd_Text_TextToMD5_wd_A_i
			
			C_LONGINT:C283($L_OLD_B)
			$L_OLD_B:=Fnd_Text_TextToMD5_wd_B_i
			
			C_LONGINT:C283($L_OLD_C)
			$L_OLD_C:=Fnd_Text_TextToMD5_wd_C_i
			
			C_LONGINT:C283($L_OLD_D)
			$L_OLD_D:=Fnd_Text_TextToMD5_wd_D_i
			
			
			  //Round 1
			
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;0;7;1)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;1;12;2)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;2;17;3)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;3;22;4)
			
			
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;4;7;5)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;5;12;6)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;6;17;7)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;7;22;8)
			
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;8;7;9)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;9;12;10)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;10;17;11)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;11;22;12)
			
			
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;12;7;13)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;13;12;14)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;14;17;15)
			Fnd_Text_TextToMD5_Calc ("ROUND1";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;15;22;16)
			
			
			  //Round 2
			
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;1;5;17)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;6;9;18)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;11;14;19)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;0;20;20)
			
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;5;5;21)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;10;9;22)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;15;14;23)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;4;20;24)
			
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;9;5;25)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;14;9;26)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;3;14;27)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;8;20;28)
			
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;13;5;29)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;2;9;30)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;7;14;31)
			Fnd_Text_TextToMD5_Calc ("ROUND2";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;12;20;32)
			
			
			  //Round 3
			
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;5;4;33)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;8;11;34)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;11;16;35)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;14;23;36)
			
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;1;4;37)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;4;11;38)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;7;16;39)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;10;23;40)
			
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;13;4;41)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;0;11;42)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;3;16;43)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;6;23;44)
			
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;9;4;45)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;12;11;46)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;15;16;47)
			Fnd_Text_TextToMD5_Calc ("ROUND3";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;2;23;48)
			
			
			  //Round 4
			
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;0;6;49)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;7;10;50)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;14;15;51)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;5;21;52)
			
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;12;6;53)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;3;10;54)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;10;15;55)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;1;21;56)
			
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;8;6;57)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;15;10;58)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;6;15;59)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;13;21;60)
			
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;4;6;61)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;11;10;62)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;2;15;63)
			Fnd_Text_TextToMD5_Calc ("ROUND4";"";->Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i;Fnd_Text_TextToMD5_wd_A_i;9;21;64)
			
			
			Fnd_Text_TextToMD5_wd_A_i:=Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_wd_A_i;$L_OLD_A)
			Fnd_Text_TextToMD5_wd_B_i:=Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_wd_B_i;$L_OLD_B)
			Fnd_Text_TextToMD5_wd_C_i:=Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_wd_C_i;$L_OLD_C)
			Fnd_Text_TextToMD5_wd_D_i:=Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_wd_D_i;$L_OLD_D)
			
		End for 
		
		$0:=Fnd_Text_TextToMD5_Calc ("WORDTOHEXA";"";->Fnd_Text_TextToMD5_wd_A_i)+Fnd_Text_TextToMD5_Calc ("WORDTOHEXA";"";->Fnd_Text_TextToMD5_wd_B_i)+Fnd_Text_TextToMD5_Calc ("WORDTOHEXA";"";->Fnd_Text_TextToMD5_wd_C_i)+Fnd_Text_TextToMD5_Calc ("WORDTOHEXA";"";->Fnd_Text_TextToMD5_wd_D_i)
		
		
	: ($1="ROUND1")
		$3->:=Fnd_Text_TextToMD5_Bits32Add ($4;(Fnd_Text_TextToMD5_Bits32Left (Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_Bits32Add ($3->;(($4 & $5) | (($4 ^| $FFFFFFFF) & $6)));Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_X_WORDs_ai{$7+1};<>Fnd_Text_Text2MD5_SINUS_ai{$9}));$8)))
		
	: ($1="ROUND2")
		$3->:=Fnd_Text_TextToMD5_Bits32Add ($4;(Fnd_Text_TextToMD5_Bits32Left (Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_Bits32Add ($3->;(($4 & $6) | ($5 & ($6 ^| $FFFFFFFF))));Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_X_WORDs_ai{$7+1};<>Fnd_Text_Text2MD5_SINUS_ai{$9}));$8)))
		
	: ($1="ROUND3")
		$3->:=Fnd_Text_TextToMD5_Bits32Add ($4;(Fnd_Text_TextToMD5_Bits32Left (Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_Bits32Add ($3->;(($4) ^| ($5) ^| ($6)));Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_X_WORDs_ai{$7+1};<>Fnd_Text_Text2MD5_SINUS_ai{$9}));$8)))
		
	: ($1="ROUND4")
		$3->:=Fnd_Text_TextToMD5_Bits32Add ($4;(Fnd_Text_TextToMD5_Bits32Left (Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_Bits32Add ($3->;($5 ^| ($4 | ($6 ^| $FFFFFFFF))));Fnd_Text_TextToMD5_Bits32Add (Fnd_Text_TextToMD5_X_WORDs_ai{$7+1};<>Fnd_Text_Text2MD5_SINUS_ai{$9}));$8)))
		
	: ($1="WORDTOHEXA")
		  // Convert a 32-bit number to a hex string
		
		C_TEXT:C284($T_ALPHABET)
		$T_ALPHABET:="0123456789abcdef"
		
		$0:=""
		For ($k;0;3)
			$0:=$0+Substring:C12($T_ALPHABET;(($3-> >> (($k*8)+4)) & 0x000F)+1;1)+Substring:C12($T_ALPHABET;(($3-> >> ($k*8)) & 0x000F)+1;1)
		End for 
		
End case 

