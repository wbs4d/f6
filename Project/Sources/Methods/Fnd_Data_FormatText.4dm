//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatText (text) --> Text

  // Strips any extra spaces before and after the text,removed double spaces
  //   within the text, then capitalizes the first letter of each word, unless the
  //   user holds down the Option/Alt key (or other bypass key that's been
  //   specified by the developer).

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text to capitalize

  // Returns: 
  //   $0 : Text : The capitalized text

  // Created by Dave Batton on May 11, 2004
  // ----------------------------------------------------

C_TEXT:C284($1;$0;$theText_t)

$theText_t:=$1

Fnd_Data_Init 
Fnd_Data_FormatError_i:=0

If (Not:C34(Fnd_Data_FormatBypass ))
	$theText_t:=Fnd_Text_StripSpaces ($theText_t;1+2+4)  // Strip leading, trailing, and inline spaces.
	$theText_t:=Fnd_Text_Capitalize ($theText_t)
End if 

$0:=$theText_t