//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Platform --> Number

// Returns the platform number.  
// Returns 10 if we're running on Mac OS X.
// Returns 3 if we're running on Windows.

// Access Type: Shared

// Parameters: None

// Returns: 
//   $0 : Longint : The platform number

// Created by Dave Batton on Jul 24, 2003
// Mod by Wayne Stewart, (2019-10-30) - Removed dependence on IP Var
// <>Fnd_Gen_Platform_i
// ----------------------------------------------------

C_LONGINT:C283($0)

Fnd_Gen_Init

If (Is Windows:C1573)
	$0:=3
Else 
	$0:=10
End if 

