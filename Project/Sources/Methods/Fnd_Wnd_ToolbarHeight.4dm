//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_ToolbarHeight --> longint

// Returns the toolbar height (if it is currently being displayed)

// Access: Shared

// Returns:
//   $0 : longint : The toolbar height

// Created by Wayne Stewart (2019-11-14)

//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE()->$toolbarHeight_i : Integer

If (False:C215)
	C_LONGINT:C283(Fnd_Wnd_ToolbarHeight; $0)
End if 

$0:=58*Num:C11(Fnd_Gen_ToolbarDisplayed)



