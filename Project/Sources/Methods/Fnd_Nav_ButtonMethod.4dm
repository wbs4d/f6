//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_ButtonMethod (button number)

// Called from the navigation palette buttons.

// Access: Private

// Parameters: 
//   $1 : Longint : The button number

// Returns: Nothing

// Created by Dave Batton on Sep 3, 2003
// Modified by Dave Batton on May 22, 2004
//   Added a test for Form event=On Clicked.
// Modified by Dave Batton on Jul 1, 2004
//   Fnd_IO_DisplayTable is now called via EXECUTE so this component
//   will compile even if the IO component isn't available.
// ----------------------------------------------------

C_LONGINT:C283($1; $buttonNumber_i)

$buttonNumber_i:=$1

Case of 
	: (Form event code:C388=On Clicked:K2:4)
		If (<>Fnd_Nav_Methods_at{$buttonNumber_i}#"")
			EXECUTE FORMULA:C63(<>Fnd_Nav_Methods_at{$buttonNumber_i})
		Else 
			Fnd_IO_DisplayTable(<>Fnd_Nav_TablePtrs_aptr{$buttonNumber_i}; Macintosh option down:C545)
			//If (Fnd_Gen_ComponentAvailable("Fnd_IO"))
			//EXECUTE METHOD("Fnd_IO_DisplayTable"; *; <>Fnd_Nav_TablePtrs_aptr{$buttonNumber_i}; Macintosh option down)
			//Else 
			//Fnd_Gen_BugAlert(Current method name; "The Foundation IO component is required to use this method.")
			//End if 
		End if 
End case 