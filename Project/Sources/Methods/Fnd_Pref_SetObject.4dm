//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetObject (name; value{; scope})

// Saves a Object value in the user's preferences file.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Object : The Object value to save
//   $3 : Longint : How to save it (optional)

// Returns: Nothing

// Created by Wayne Stewart, (2022-03-17)
// Modified by Wayne Stewart on 2023-07-27:
//   Replaced the Interprocess "constants" with actual constants
// ----------------------------------------------------

C_TEXT:C284($1; $itemName_t; $itemValue_t)
C_OBJECT:C1216($2; $itemValue_o)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_o:=$2
If (Count parameters:C259>=3)
	$itemScope_i:=$3
Else 
	$itemScope_i:=Fnd_Pref_Local
End if 

Fnd_Pref_Init

If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
	$itemValue_t:=JSON Stringify:C1217($itemValue_o; *)
	Fnd_Pref_SetValue($itemName_t; Is object:K8:27; $itemValue_t; $itemScope_i)
	CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
End if 
