//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_PrefsPut (pref name; text value)

// Adds an item to the user's preferences if the Fnd_Pref component is available.

// Access Type: Private

// Parameters: 
//   $1 : Text : The preference name
//   $2 : Text : The value to store

// Returns: Nothing

// Created by Dave Batton on Nov 6, 2004
// Modified by Dave Batton on Dec 14, 2005
//   Quote characters in the value are now encoded so they'll get past the EXECUTE statement.
// Mod by Wayne Stewart, (2021-04-13) - Removed Fnd_Gen_ComponentAvailable check
// ----------------------------------------------------

C_TEXT:C284($1; $2; $name_t; $value_t)


$name_t:=$1
$value_t:=$2

Fnd_Pref_SetText($name_t; $value_t)
