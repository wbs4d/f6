//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Rec_SizeArrayObject (->object; alignment) --> Number

  // Sets the best size for an array based form object based on the contents it displays.
  // Rather than using BEST OBJECT SIZE, we calculate the size the same way we
  //   did before opening the dialog using the Fnd_Find_GetExtraWidth method.

  // Access Type: Private

  // Parameters: 
  //   $1 : Pointer : A pointer to the form object
  //   $2 : Longint : The alignment (side not to move)

  // Returns: 
  //   $0 : Longint : The increase in the button size

  // Created by Dave Batton on Dec 5, 2005
  // ----------------------------------------------------

C_POINTER:C301($1;$object_ptr)
C_LONGINT:C283($0;$2;$alignment_i;$left_i;$top_i;$right_i;$bottom_i)
C_LONGINT:C283($originalWidth_i;$newWidth_i;$diffWidth_i;$bestWidth_i)

$object_ptr:=$1
$alignment_i:=$2

OBJECT GET COORDINATES:C663($object_ptr->;$left_i;$top_i;$right_i;$bottom_i)

$originalWidth_i:=$right_i-$left_i
$newWidth_i:=$originalWidth_i

$bestWidth_i:=Fnd_Rec_BestPopupSize ($object_ptr)
If ($bestWidth_i>$originalWidth_i)
	$newWidth_i:=$bestWidth_i
Else 
	$newWidth_i:=$originalWidth_i
End if 

$diffWidth_i:=$newWidth_i-$originalWidth_i

Case of 
	: ($alignment_i=Align left:K42:2)
		OBJECT MOVE:C664($object_ptr->;0;0;$diffWidth_i;0)
	: ($alignment_i=Align right:K42:4)
		OBJECT MOVE:C664($object_ptr->;0-$diffWidth_i;0;$diffWidth_i;0)
End case 

$0:=$diffWidth_i