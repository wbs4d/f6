//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_Quit

  // Called from the menu bar when the user selects Quit from the File menu.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 8, 2003
  // ----------------------------------------------------

Fnd_Gen_QuitNow (True:C214)
