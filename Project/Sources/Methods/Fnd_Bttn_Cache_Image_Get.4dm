//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Bttn_Cache_Image_Get (composite name) --> Picture


  // Returns an image from the cache using its composite name.

  // Modified from Mark's original design. This routine no longer displays an alert

  //   if the image isn't found in the cache.  It just returns a blank image, so the 

  //   developer needs to check to see if an image was returned.


  // Access Type: Private


  // Parameters: 

  //   $1 : Text : The name returned by the Fnd_Bttn_GetPictureName function


  // Returns: 

  //   $0 : Picture : The image from the cache


  // Created by Mark Mitchenall on 17/9/01

  //   Original Method Name: rollover_Cache_Image_Get

  //   Part of the toolbar_Component, ©2001 mitchenall.com

  //   Used with permission.

  // Modified by Dave Batton on Feb 20, 2004

  //   Updated specifically for use by the Foundation Shell.

  // ----------------------------------------------------


C_PICTURE:C286($0;$image_pic)
C_TEXT:C284($1;$name_t)
C_LONGINT:C283($element_i)

$name_t:=$1

  //Find the composite name in the cache.

$element_i:=Find in array:C230(<>Fnd_Bttn_CacheNames_at;$name_t)

If ($element_i>0)
	
	  //Get the picture from the cache  

	$image_pic:=<>Fnd_Bttn_CachePictures_apic{$element_i}
	
	  //Update the last tickcount used value for the 

	  //item so that it doesn't get garbage collected too early.

	<>Fnd_Bttn_Tickcounts_ai{$element_i}:=Tickcount:C458
	
	  //Update the cache hit value (for our own stats)

	<>Fnd_Bttn_CacheTotalHits_i:=<>Fnd_Bttn_CacheTotalHits_i+1
End if 

  //Return the image 

$0:=$image_pic