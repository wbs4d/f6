//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_SetupToolbar

// Handles the toolbar setup for the input and output forms.

// Method Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 17, 2004
// Modified by Dave Batton on Sep 20, 2004
// Modified by Dave Batton on Jan 8, 2005
//   Now sets the toolbar icon group.
// ----------------------------------------------------

// Create the output form toolbar buttons and divider lines.
// We just pass blank button names because these will be handled by the Fnd_Tlbr_StyleName call.
Fnd_Tlbr_Button_Add("New";Fnd_Loc_GetString("Fnd_IO";"NewButton");"";"Fnd_Rec_NewRecord";"N")
Fnd_Tlbr_Button_Add("Delete";Fnd_Loc_GetString("Fnd_IO";"DeleteButton");"";"Fnd_Out_DeleteUserSet";"Backspace")
Fnd_Tlbr_Divider_Add
Fnd_Tlbr_Button_Add("ShowAll";Fnd_Loc_GetString("Fnd_IO";"ShowAllButton");"";"Fnd_Rec_ShowAll";"G")
Fnd_Tlbr_Button_Add("Find";Fnd_Loc_GetString("Fnd_IO";"FindButton");"";"Fnd_aa_Shell_Find";"F")
Fnd_Tlbr_Button_Add("Sort";Fnd_Loc_GetString("Fnd_IO";"SortButton");"";"Fnd_aa_Shell_Sort";"T")
Fnd_Tlbr_Divider_Add
Fnd_Tlbr_Button_Add("Print";Fnd_Loc_GetString("Fnd_IO";"PrintButton");"";"Fnd_Shell_Print";"P")

Fnd_IO_ToolbarIconGroup("Bold")  // DB050208
