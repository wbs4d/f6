//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Reg

  // Compiler variables related to the Foundation registration routines.
  // Called by Fnd_Dlg_Reg.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 16, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Reg_Initialized_b)
If (Not:C34(<>Fnd_Reg_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(<>Fnd_Reg_FinishTime_i;<>Fnd_Reg_DefaultDemoDays_i)
	C_LONGINT:C283(<>Fnd_Reg_OKButton_i;<>Fnd_Reg_CancelButton_i)
	C_LONGINT:C283(<>Fnd_Reg_RegisterButton_i;<>Fnd_Reg_BuyNowButton_i;<>Fnd_Reg_OffscreenButton_i)
	
	C_BOOLEAN:C305(<>Fnd_Reg_DoneWithDemoDialog_b)
	
	C_TEXT:C284(<>Fnd_Reg_DefaultFeatureCode_t;<>Fnd_Reg_FeatureName_t)
	C_TEXT:C284(<>Fnd_Reg_LicenseInfo_t;<>Fnd_Reg_UnlockCode_t)
	C_TEXT:C284(<>Fnd_Reg_SecretKey_t;<>Fnd_Reg_BuyNowURL_t)
	C_TEXT:C284(<>Fnd_Reg_Message1_t;<>Fnd_Reg_Message2_t;<>Fnd_Reg_DlgMessage_t;<>Fnd_Reg_CountDown_t)
	C_TEXT:C284(<>Fnd_Reg_Label1_t;<>Fnd_Reg_Label2_t)
	C_TEXT:C284(<>Fnd_Reg_UserName_t;<>Fnd_Reg_Semaphore_t)
	C_TEXT:C284(<>Fnd_Reg_UnlockCodeFormat_t)
	C_TEXT:C284(<>Fnd_Reg_Semaphore_t)
	
	C_POINTER:C301(<>Fnd_Reg_Table_ptr)
	C_POINTER:C301(<>Fnd_Reg_IDFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_FeatureCodeFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_DemoDaysFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_DemoStartDateFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_UnlockCodeFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_UserNameFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_LicenseInfoFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_ChecksumFld_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostFeatureName_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostMessage1_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostMessage2_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostCountDown_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostOKButton_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostRegisterButton_ptr)
	C_POINTER:C301(<>Fnd_Reg_HostBuyNowButton_ptr)
	
	ARRAY TEXT:C222(<>Fnd_Reg_FeatureCodes_at;0)
	ARRAY TEXT:C222(<>Fnd_Reg_FeatureNames_at;0)
	ARRAY TEXT:C222(<>Fnd_Reg_BuyNowURLs_at;0)
	ARRAY TEXT:C222(<>Fnd_Reg_DemoMessage_at;0)
End if 


  // Process Variables
C_BOOLEAN:C305(Fnd_Reg_Initialized_b)
If (Not:C34(Fnd_Reg_Initialized_b))  // So we only do this once per process.
	C_TEXT:C284(Fnd_Reg_DialogFeatureCode_t)
End if 


  // Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Reg_Alert ;$1)
	
	C_TEXT:C284(Fnd_Reg_BuyNowURL ;$0;$1;$2)
	
	C_LONGINT:C283(Fnd_Reg_DemoDays ;$0;$2)
	C_TEXT:C284(Fnd_Reg_DemoDays ;$1)
	
	C_TEXT:C284(Fnd_Reg_DemoDialog ;$1)
	
	C_TEXT:C284(Fnd_Reg_DemoMessage ;$0;$1;$2)
	
	C_LONGINT:C283(Fnd_Reg_FeatureCodeIndex ;$0)
	C_TEXT:C284(Fnd_Reg_FeatureCodeIndex ;$1)
	
	C_TEXT:C284(Fnd_Reg_FeatureName ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Reg_Format ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Reg_GetUnlockCode ;$0;$1)
	
	C_LONGINT:C283(Fnd_Reg_GetDemoDaysRemaining ;$0)
	C_TEXT:C284(Fnd_Reg_GetDemoDaysRemaining ;$1)
	
	C_DATE:C307(Fnd_Reg_GetExpirationDate ;$0)
	C_TEXT:C284(Fnd_Reg_GetExpirationDate ;$1)
	
	C_TEXT:C284(Fnd_Reg_GetLicenseInfo ;$0;$1)
	
	C_TEXT:C284(Fnd_Reg_GetUserName ;$0;$1)
	
	C_TEXT:C284(Fnd_Reg_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Reg_LoadRegistrationRecord ;$1)
	
	C_TEXT:C284(Fnd_Reg_Register ;$1;$2;$3)
	
	C_TEXT:C284(Fnd_Reg_RegistrationDialog ;$1)
	
	C_LONGINT:C283(Fnd_Reg_RegistrationState ;$0)
	C_TEXT:C284(Fnd_Reg_RegistrationState ;$1)
	
	C_TEXT:C284(Fnd_Reg_Reset ;$1)
	
	C_TEXT:C284(Fnd_Reg_SaveRecord ;$1)
	
	C_TEXT:C284(Fnd_Reg_SaveRegistrationInfo ;$1;$2;$3;$4)
	
	C_POINTER:C301(Fnd_Reg_SetHostVars ;$1;$2;$3;$4;$5;$6;$7)
	
	C_TEXT:C284(Fnd_Reg_SetSecretKey ;$1)
	
	C_TEXT:C284(Fnd_Reg_StartDemoPeriod ;$1)
	
	C_BOOLEAN:C305(Fnd_Reg_ValidateUnlockCode ;$0)
	C_TEXT:C284(Fnd_Reg_ValidateUnlockCode ;$1;$2;$3;$4;$5;$6)
End if 

