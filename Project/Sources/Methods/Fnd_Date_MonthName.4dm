//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Date_MonthName (month number) --> Text

// Returns the name of the month with the specified number.
// This routine will use Foundation's Localization routine, if it's available,
//   to return the localized month name.  If the Fnd_Loc component is
//   not available, the name will be localized using 4D's language.

// Access: Shared

// Parameters: 
//   $1 : Longint : 1 thorugh 12

// Returns: 
//   $0 : Text : The name of the month

// Created by Dave Batton on May 2, 2004
// Modified by Dave Batton on Sep 27, 2004
//   Fixed a stupid error that caused this routine to always return "Error."
// ----------------------------------------------------

C_TEXT:C284($0; $monthName_t; $internalLabel_t)
C_LONGINT:C283($1; $monthNum_i)

$monthNum_i:=$1

Fnd_Date_Init

Case of 
	: (($monthNum_i<1) | ($monthNum_i>12))  // DB040927
		$monthName_t:="Error"
		
	: (Fnd_List_DoesListExist("Fnd_Date_"+Fnd_Loc_LanguageCode))  // Check that a localised version of the months exist
		// Get the localized version.
		// 4D stores these in the resource fork, but we can't use that
		//   and use Foundation's localization routines too.  So we do this.
		$internalLabel_t:="Month"+String:C10($monthNum_i; "00")
		$monthName_t:=Fnd_Loc_GetString("Fnd_Date";$internalLabel_t)
	Else 
		// Just get it from the 4D resource fork.
		$monthName_t:=Get indexed string:C510(11; 12+$monthNum_i)
End case 

$0:=$monthName_t