//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_Editor

// Displays the List Editor window in a new process.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 15, 2003
// Modified by Dave Batton on Jan 16, 2004
//   Moved the Fnd_Menu_Window_Remove method to after the CLOSE WINDOW call.
// Modified by Dave Batton on May 20, 2004
//   Now tests for the Menus component before calling menu routines.
//   Changed the Fnd_Loc_GetString calls to Fnd_Gen_GetString calls.
// Modified by Dave Batton on Jan 15, 2005
//   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition when displaying alerts and confirms.
// Modified by Dave Batton on Apr 27, 2005
//   Changed the Fnd_Pref_SetWindow call to Fnd_Wnd_SavePosition so the List component remains optional.
// Modified by Dave Batton on May 28, 2005
//   Updated to use the new Fnd_Wnd methods.
// ----------------------------------------------------

C_TEXT:C284($windowPrefName_t)
C_POINTER:C301($nil_ptr)
C_LONGINT:C283($width_i; $height_i)

Fnd_List_Init

If (Fnd_Gen_LaunchAsNewProcess("Fnd_List_Editor"; "Fnd_List: Editor window"))
	If (Semaphore:C143("<>Fnd_List_Editor"))
		Fnd_Dlg_SetIcon(Fnd_Dlg_StopIcon)
		Fnd_Wnd_Position(Fnd_Wnd_CenterOnScreen)
		Fnd_Dlg_Alert(Fnd_Gen_GetString("Fnd_List"; "InUseMessage"))
		
	Else 
		Fnd_Gen_CurrentFormType(Fnd_Gen_ListEditorForm)
		
		Fnd_Gen_MenuBar
		
		If (Is a list:C621(<>Fnd_List_ListOfLists_i))
			CLEAR LIST:C377(<>Fnd_List_ListOfLists_i; *)
		End if 
		<>Fnd_List_ListOfLists_i:=New list:C375
		
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_List_SetEditableLists")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_List_SetEditableLists"; *)  // Get the names of all editable lists.
		End if 
		
		$windowPrefName_t:="Fnd_List: List Editor Window"
		
		Fnd_Wnd_Title(Fnd_Gen_GetString("Fnd_List"; "EditorWindowTitle"))
		Fnd_Wnd_UseSavedPosition($windowPrefName_t)
		Fnd_Wnd_CloseBox(True:C214)
		FORM GET PROPERTIES:C674("Fnd_List_Editor"; $width_i; $height_i)
		Fnd_Wnd_OpenWindow($width_i; $height_i)
		
		
		Fnd_Menu_Window_Add
		
		Fnd_Wnd_SendCloseRequests
		
		DIALOG:C40("Fnd_List_Editor")
		Fnd_Wnd_SavePosition($windowPrefName_t)
		
		Fnd_Menu_Window_Remove
		
		
		CLOSE WINDOW:C154
		
		Fnd_List_SaveListItems
		
		CLEAR LIST:C377(<>Fnd_List_ListOfLists_i; *)
		CLEAR LIST:C377(<>Fnd_List_ListItems_i; *)
		
		CLEAR SEMAPHORE:C144("<>Fnd_List_Editor")
	End if 
End if 