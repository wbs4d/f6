//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Distributor

  // Launched as a new process from Fnd_SqNo_Get to get sequence numbers.
  //   Sequence numbers must be gotten from a new process for any process
  //   currently in a transaction.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 5, 2003
  // Modified by Dave Batton on Sep 27, 2007
  //   Added support for the new Fnd_SqNo_GetMany command.
  // ----------------------------------------------------

C_TEXT:C284(<>Fnd_SqNo_Name_t)
C_LONGINT:C283(<>Fnd_SqNo_Number_i;$giveUpTime_i)

Repeat 
	If (<>Fnd_SqNo_Name_t#"")
		If (<>Fnd_SqNo_Number_i=-1)
			<>Fnd_SqNo_Number_i:=Fnd_SqNo_Get2 (<>Fnd_SqNo_Name_t)
		Else 
			ARRAY LONGINT:C221(<>Fnd_SqNo_Numbers_ai;Abs:C99(<>Fnd_SqNo_Number_i))
			Fnd_SqNo_GetMany2 (<>Fnd_SqNo_Name_t;-><>Fnd_SqNo_Numbers_ai)
			<>Fnd_SqNo_Number_i:=0
		End if 
		
		<>Fnd_SqNo_Name_t:=""
		
		  // Now wait here until we receive a message that the Fnd_SqNo_Get method has got the 
		  //   new number.  But we still will timeout in case something goes wrong.
		$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
		While ((<>Fnd_SqNo_Number_i#-2) & (Tickcount:C458<$giveUpTime_i))
			DELAY PROCESS:C323(Current process:C322;1)
		End while 
		
		  // Send a message back to Fnd_SqNo_Get to let it know it can release the semaphore.
		<>Fnd_SqNo_Number_i:=-1
	End if 
	
	PAUSE PROCESS:C319(Current process:C322)  // Go to sleep until somebody needs us again.
Until (Fnd_Gen_QuitNow )