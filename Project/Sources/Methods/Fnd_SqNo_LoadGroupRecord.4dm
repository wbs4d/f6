//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_LoadGroupRecord (group name)

  // Called from Fnd_SqNo_Get2 and Fnd_SqNo_GetMany2 to load the specified group record.
  // Creates it if it doesn't exist.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The group name

  // Returns: Nothing

  // Created by Dave Batton on Sep 26, 2007
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_LONGINT:C283($giveUpTime_i)

$groupName_t:=$1

QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_GroupNameFld_ptr->=$groupName_t)

If (Records in selection:C76(<>Fnd_SqNo_Table_ptr->)=0)  // If this is the first time this sequence number name has been used, we must…
	CREATE RECORD:C68(<>Fnd_SqNo_Table_ptr->)  // …create a new record for it.
	<>Fnd_SqNo_IDFld_ptr->:=Sequence number:C244(<>Fnd_SqNo_Table_ptr->)
	<>Fnd_SqNo_GroupNameFld_ptr->:=$groupName_t
	<>Fnd_SqNo_DesignerOnlyFld_ptr->:=False:C215  // By default we allow administrator access to sequence numbers in the editor.
	<>Fnd_SqNo_NextNumberFld_ptr->:=1  // Start it out at 1.
	
Else 
	READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)  //In case READ ONLY(*) gets called.
	LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)
	$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
	While ((Locked:C147(<>Fnd_SqNo_Table_ptr->)) & (Tickcount:C458<$giveUpTime_i))
		DELAY PROCESS:C323(Current process:C322;1)
		LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)
	End while 
End if 

