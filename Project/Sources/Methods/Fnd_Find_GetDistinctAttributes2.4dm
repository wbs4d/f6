//%attributes = {"invisible":true,"executedOnServer":true,"preemptive":"incapable"}
// ----------------------------------------------------
// Project Method: Fnd_Find_GetDistinctAttributes2 (Field Pointer; Array Pointer)

// This method returns the list of distinct paths from the field listed.
//   This method Executes on the server
//   By default this will be for all records in the table
//   rather than just the current selection.

// Access: Private

// Parameters:
//   $1 : Pointer : Pointer to an object field
//   $2 : Pointer : Pointer to a text array to retreive the attribute paths

// Created by Wayne Stewart (2017-03-19)
//     waynestewart@mac.com
// ----------------------------------------------------

C_POINTER:C301($1)
C_POINTER:C301($2)

C_LONGINT:C283($giveUpTime_i)

If (False:C215)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes2; $1)
	C_POINTER:C301(Fnd_Find_GetDistinctAttributes2; $2)
End if 

Fnd_Find_Init

If (Not:C34(Semaphore:C143(Fnd_Find_Semaphore; 300)))
	ARRAY TEXT:C222(<>Fnd_Find_DistinctPaths_at; 0)
	
	Fnd_Find_GetDistinctAttributes3($1)
	
	Repeat 
		DELAY PROCESS:C323(Current process:C322; 1)  // We're in a hurry. Don't wait long.
	Until ((Size of array:C274(<>Fnd_Find_DistinctPaths_at)>0) | (Tickcount:C458>$giveUpTime_i))
	
	// Check that the array has real data in it
	If (Size of array:C274(<>Fnd_Find_DistinctPaths_at)>0)
		//
		If (<>Fnd_Find_DistinctPaths_at{1}=Fnd_Find_DummyAttribute)
			ARRAY TEXT:C222(<>Fnd_Find_DistinctPaths_at; 0)
		End if 
		
		//  Now copy it back
		COPY ARRAY:C226(<>Fnd_Find_DistinctPaths_at; $2->)
		
		// and clear it
		ARRAY TEXT:C222(<>Fnd_Find_DistinctPaths_at; 0)
		
	End if 
	
	CLEAR SEMAPHORE:C144(Fnd_Find_Semaphore)
	
End if 