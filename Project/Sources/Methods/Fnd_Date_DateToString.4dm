//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_DateToString (date{; relative to date{; format}}) --> Text

  // Returns a blank string if the date = !00/00/00!
  // Returns "Today", "Tomorrow", or "Yesterday" if appropriate. This is localized
  //   if the Localization component is available.
  // Otherwise returns a short date string.

  // Access: Shared

  // Parameters: 
  //   $1 : Date : The date to convert to a string
  //   $2 : Date : The relative date (optional)
  //   $3 : Text : The date format (eg. "M/D/Y") (optional)

  // Returns: 
  //   $0 : Text : The date as a string

  // Created by Dave Batton on Apr 28, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$3;$outputDate_t;$dateFormat_t;$month_t;$day_t;$year_t)
C_DATE:C307($1;$2;$input_date;$relativeTo_date)

$input_date:=$1

Fnd_Date_Init 

If (Count parameters:C259>=2)
	$relativeTo_date:=$2
	If (Count parameters:C259>=3)
		$dateFormat_t:=$3
	Else 
		$dateFormat_t:=Fnd_Date_SystemDateFormat 
	End if 
Else 
	$relativeTo_date:=Current date:C33
	$dateFormat_t:=Fnd_Date_SystemDateFormat 
End if 

Case of 
	: ($input_date=$relativeTo_date)
		$outputDate_t:=Fnd_Gen_GetString ("Fnd_Date";"Today")
		
	: ($input_date=($relativeTo_date+1))
		$outputDate_t:=Fnd_Gen_GetString ("Fnd_Date";"Tomorrow")
		
	: ($input_date=($relativeTo_date-1))
		$outputDate_t:=Fnd_Gen_GetString ("Fnd_Date";"Yesterday")
		
	: ($input_date=!00-00-00!)
		$outputDate_t:=""
		
	Else 
		$year_t:=String:C10(Year of:C25($input_date))
		$month_t:=String:C10(Month of:C24($input_date))
		$day_t:=String:C10(Day of:C23($input_date))
		$outputDate_t:=Replace string:C233(Replace string:C233(Replace string:C233($dateFormat_t;"M";$month_t);"D";$day_t);"Y";$year_t)
End case 

$0:=$outputDate_t