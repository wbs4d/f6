//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_Administration

// Displays Foundation's Administration dialog.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 19, 2003
// Modified by: Walt Nelson (2/4/10) - added Fnd_Gen_MenuBar to Fnd_Shell_Administration to fix bug with grayed out Menu after calling
// File->Administration on Win XP - Paul Norwood
// ----------------------------------------------------

Fnd_List_Clear
Fnd_Wnd_Title(Fnd_Loc_GetString("Fnd_Shell"; "AdministrationWindowTitle"))

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Administration")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_Administration"; *)
	
Else 
	If ((Current user:C182="Designer") | (Current user:C182="Administrator") | (User in group:C338(Current user:C182; "Administration")))
		// Access to 4D's Password Editor
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "PasswordEditor"); Command name:C538(281))  // 281 = EDIT ACCESS
		
		// Foundation's List Editor
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "ListEditor"); "Fnd_List_Editor")
		
		// Foundation's Sequence Number Editor
		//If (Fnd_Gen_ComponentAvailable("Fnd_SqNo"))
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "SequenceNumberEditor"); "Fnd_SqNo_Editor")
		//End if 
		
		// Import & Export Editors
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "ImportEditor"); Command name:C538(665)+"(\"\")")  // 665 = IMPORT TEXT
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "ExportEditor"); Command name:C538(666)+"(\"\")")  // 666 = EXPORT TEXT
		
		// Display the selection dialog.
		Fnd_List_CommandDialog
		
	Else 
		Fnd_Dlg_SetText(Fnd_Loc_GetString("Fnd_Shell"; "AccessDenied"); Fnd_Loc_GetString("Fnd_Shell"; "AccessDeniedAdmin"))
		Fnd_Dlg_SetIcon(Fnd_Dlg_StopIcon)
		Fnd_Dlg_Display
	End if 
End if 

Fnd_Gen_MenuBar  // Walt Nelson (2/4/10)