//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_GetRequest --> Text

  // Returns the value entered by the user after displaying a Request dialog.

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Text : The value entered by the user

  // Created by Dave Batton on Jul 17, 2003
  // ----------------------------------------------------

C_TEXT:C284($0;Fnd_Dlg_RequestValue_t)

Fnd_Dlg_Init 

$0:=Fnd_Dlg_RequestValue_t
