//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_NavigationPalette

// Displays Foundation's Navigation Palette.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 29, 2003
// ----------------------------------------------------

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_NavPalette")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_NavPalette"; *)
Else 
	Fnd_Nav_Display
	//If (Fnd_Gen_ComponentAvailable("Fnd_Nav"))
	//EXECUTE METHOD("Fnd_Nav_Display"; *)
	//End if 
End if 
