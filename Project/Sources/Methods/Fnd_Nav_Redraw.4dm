//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_Redraw

// This redraws the Nav Palette

// Access: Private

// Created by Wayne Stewart (2021-04-04)

//     waynestewart@mac.com
// ----------------------------------------------------

var $bottom_i; $left_i; $newHeight_i; $newWidth_i; $right_i; $top_i : Integer


GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i)
$newWidth_i:=Fnd_Nav_WindowWidth
$newHeight_i:=Fnd_Nav_WindowHeight

If ($newHeight_i=0)  // If there are no buttons, close the window.
	CANCEL:C270
	
Else   // Otherwise, resize it.
	// If it's anchored to the right side of the screen, keep it that way.
	If ($right_i=Screen width:C187)
		$left_i:=Screen width:C187-$newWidth_i
		
	Else 
		// Make sure we don't resize it so it's partially offscreen.
		$right_i:=$left_i+$newWidth_i
		If ($right_i>Screen width:C187)
			$left_i:=Screen width:C187-$newWidth_i
			$right_i:=$left_i+$newWidth_i
		End if 
	End if 
	
	$bottom_i:=$top_i+$newHeight_i
	If ($bottom_i>Screen height:C188)
		$top_i:=Screen height:C188-$newHeight_i
		$bottom_i:=$top_i+$newHeight_i
	End if 
	
	SET WINDOW RECT:C444($left_i; $top_i; $right_i; $bottom_i)
End if 