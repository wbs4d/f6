//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Tbl
// Compiler variables related to the Fnd_Tbl routines.
// Method Type: Private
// Parameters: None
// Returns: Nothing
// Created by Wayne Stewart (2023-02-27)
//     waynestewart@mac.com
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Tbl_Initialised_b)
If (Not:C34(<>Fnd_Tbl_Initialised_b))  // So we only do this once.
	ARRAY BOOLEAN:C223(<>Fnd_Tbl_UserCanAccess_ab; 0)
	
End if 


// Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Tbl_Info; $1; $0)
	
	C_POINTER:C301(Fnd_Tbl_UserCanAccess; $1)
	C_BOOLEAN:C305(Fnd_Tbl_UserCanAccess; $0)
	
	
	
End if 