//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_EncodeBase64Blob (->blob{; line breaks?{; line break character(s)}})

  // Converts the blob to Base64.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a blob
  //   $2 : Boolean : Add linebreaks? (optional)
  //   $3 : String[2] : Linebreak characters (optional)

  // Returns: Nothing

  // Based on code submitted to 4DToday.com by Peter Bozek.
  // Modified by Dave Batton on Apr 10, 2005
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_POINTER:C301($1;$pPassedBlob)  // Pointer to a BLOB
C_BOOLEAN:C305($2;$bInsertLineBr)  // Insert line breaks? (optional)
C_TEXT:C284($3;$EOL_t)  // Line break character(s) (optional)

C_LONGINT:C283($iPos;$iDest;$iWritten;$iWhere)
C_LONGINT:C283($lBlobSize;$lNewSize;$lEOLLength)
C_BLOB:C604($oTempBlob)
C_TEXT:C284($encoding_t)

$pPassedBlob:=$1
$lBlobSize:=BLOB size:C605($pPassedBlob->)

If ($lBlobSize>0)
	
	  //load parameters
	If (Count parameters:C259>1)
		$bInsertLineBr:=$2
	Else 
		$bInsertLineBr:=True:C214
	End if 
	
	$lEOLLength:=0
	If ($bInsertLineBr)
		If (Count parameters:C259>2)
			$EOL_t:=$3
		End if 
		If ($EOL_t="")
			$EOL_t:=Char:C90(Carriage return:K15:38)
		End if 
		$lEOLLength:=Length:C16($EOL_t)
	End if 
	
	  //prepare data 
	COPY BLOB:C558($pPassedBlob->;$oTempBlob;0;0;$lBlobSize)
	SET BLOB SIZE:C606($oTempBlob;(($lBlobSize+2)\3)*3;0x0000)
	
	SET BLOB SIZE:C606($pPassedBlob->;0)
	
	$lNewSize:=(($lBlobSize+2)\3)*4
	If ($bInsertLineBr)
		$lNewSize:=$lNewSize+(($lNewSize+75)\76)*$lEOLLength
	End if 
	SET BLOB SIZE:C606($pPassedBlob->;$lNewSize;0x0000)
	
	$encoding_t:="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	$encoding_t:=$encoding_t+"abcdefghijklmnopqrstuvwxyz0123456789+/"
	
	$iPos:=0
	$iDest:=0
	
	If ($bInsertLineBr)  //start with NL
		TEXT TO BLOB:C554($EOL_t;$pPassedBlob->;Mac text without length:K22:10;$iDest)
		  //$iDest:=$iDest+$lEOLLength
	End if 
	
	While ($iPos<$lBlobSize)
		$iWhere:=($oTempBlob{$iPos} >> 2)
		$pPassedBlob->{$iDest}:=Character code:C91($encoding_t[[$iWhere+1]])
		$iDest:=$iDest+1
		
		$iWhere:=(($oTempBlob{$iPos} << 4) & 0x0030) | ($oTempBlob{$iPos+1} >> 4)
		$pPassedBlob->{$iDest}:=Character code:C91($encoding_t[[$iWhere+1]])
		$iDest:=$iDest+1
		
		
		$iWhere:=(($oTempBlob{$iPos+1} << 2) & 0x003C) | (($oTempBlob{$iPos+2} >> 6) & 0x0003)
		$pPassedBlob->{$iDest}:=Character code:C91($encoding_t[[$iWhere+1]])
		$iDest:=$iDest+1
		
		$iWhere:=$oTempBlob{$iPos+2} & 0x003F
		$pPassedBlob->{$iDest}:=Character code:C91($encoding_t[[$iWhere+1]])
		$iDest:=$iDest+1
		
		$iPos:=$iPos+3
		$iWritten:=$iWritten+4
		
		Case of 
			: ($iPos=($lBlobSize+1))
				$pPassedBlob->{$iDest-1}:=Character code:C91("=")
			: ($iPos=($lBlobSize+2))
				$pPassedBlob->{$iDest-1}:=Character code:C91("=")
				$pPassedBlob->{$iDest-2}:=Character code:C91("=")
		End case 
		
		If ($bInsertLineBr & ($iWritten%76=0))
			TEXT TO BLOB:C554($EOL_t;$pPassedBlob->;Mac text without length:K22:10;$iDest)
		End if 
		
	End while 
End if 