//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_IsRunning --> Boolean

  // Returns True if the shell is running.

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Boolean : Is the shell running?

  // Created by Dave Batton on Sep 29, 2003
  // ----------------------------------------------------

C_BOOLEAN:C305($0)

Fnd_Shell_Init 

$0:=<>Fnd_Shell_Running_b