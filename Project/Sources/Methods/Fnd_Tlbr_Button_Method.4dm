//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Button_Method (button name{; method name}) --> Text

  // Call this method to specify the name of the method to call when the button is clicked.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the button to modify
  //   $2 : Text : The name of the method to call (optional)

  // Returns: 
  //   $0 : Text : The name of the method called by the button

  // Toolbar Component - © mitchenall.com 2002
  // Created by Dave Batton on Jan 6, 2005
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Enhanced parameter description in header
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$buttonName_t;$methodName_t)
C_LONGINT:C283($buttonNumber_i)

$buttonName_t:=$1

Fnd_Tlbr_Init 

$buttonNumber_i:=Fnd_Tlbr_Object_GetNumber ($buttonName_t)

If ($buttonNumber_i>0)
	If (Count parameters:C259>=2)
		Fnd_Tlbr_ObjectMethods_at{$buttonNumber_i}:=$2
	End if 
	$methodName_t:=Fnd_Tlbr_ObjectMethods_at{$buttonNumber_i}
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"Button name not found: "+$buttonName_t)
End if 

$0:=$methodName_t