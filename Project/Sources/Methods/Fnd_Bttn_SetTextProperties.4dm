//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Bttn_SetTextProperties (font name; font size; font style)


// Sets the text properties for the button labels.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The font name

//   $2 : Longint : The font size

//   $3 : Longint : The font style (use 4D constants)


// Returns: Nothing


// Created by Mark Mitchenall on 12/5/02

//   Original Method Name: rollover_SetTextProperties

//   Part of the toolbar_Component, ©2001 mitchenall.com

//   Used with permission.

// Modified by Dave Batton on Feb 20, 2004

//   Updated specifically for use by the Foundation Shell.

// ----------------------------------------------------


C_TEXT:C284($1)
C_LONGINT:C283($2;$3)

Fnd_Bttn_Init

Fnd_Bttn_FontName_t:=$1
Fnd_Bttn_FontSize_i:=$2
Fnd_Bttn_FontStyle_i:=$3
