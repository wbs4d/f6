//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_OpenTableDialog

  // Displays a dialog with the names of all visible tables.  If the user selects
  //   one it's displayed in a new window.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 2, 2003
  // ----------------------------------------------------

C_POINTER:C301($table_ptr)

Fnd_Shell_Init 

$table_ptr:=Fnd_Shell_SelectTable 

If (Not:C34(Is nil pointer:C315($table_ptr)))
	Fnd_IO_DisplayTable ($table_ptr;True:C214)
End if 
