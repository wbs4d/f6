//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_CurrentButton ({button name}) --> Text

// Sets and gets the button specified as the "current" button.
// This is for the new Mac OS X style "Prefs" style button.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the button to make the current button (optional)

// Returns: 
//   $0 : Text : The name of the current button

// Created by Dave Batton on Apr 12, 2005
// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header
// ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Tlbr_Init

If (Count parameters:C259>=1)
	Fnd_Tlbr_CurrentButton_t:=$1
	Fnd_Tlbr_ButtonsAreValid_b:=False:C215
	Fnd_Tlbr_UpdateButtons
End if 

$0:=Fnd_Tlbr_CurrentButton_t
