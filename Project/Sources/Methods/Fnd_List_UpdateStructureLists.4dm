//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_UpdateStructureLists

  // Copies the Foundation maintained lists to the 4D structure file.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Mar 20, 2004
  //   The semaphores now use IP variables.
  //   Localized the "Updating Lists" message.
  //   Now properly restores the read/write state of the ◊Fnd_List_Table_ptr-> table.
  // Modified by Dave Batton on May 30, 2004
  //   Changed the Fnd_Loc_GetString calls to Fnd_Gen_GetString calls.
  // Modified by Dave Batton on Jan 15, 2005
  //   Displays the Updating Lists message only if there are lists to update.
  //   Displays the message for an addtional 2 seconds so it can be read.
  // Modified by Dave Batton on Feb 18, 2006
  //   Removed an unused parameter.
  // Modified by: Walt Nelson (2/3/11) - now calls Fnd_Host_SaveList, lists weren't updated at all
  // ----------------------------------------------------

C_LONGINT:C283($recordNumber_i;$list_i)
C_BOOLEAN:C305($wasReadOnly_b)
C_TEXT:C284($formula_t)

Fnd_List_Init 

If (Not:C34(Semaphore:C143(<>Fnd_List_Semaphore_t)))
	If (Is a list:C621(<>Fnd_List_ListOfLists_i))
		CLEAR LIST:C377(<>Fnd_List_ListOfLists_i;*)
	End if 
	<>Fnd_List_ListOfLists_i:=New list:C375
	
	If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_List_SetEditableLists")=1)
		EXECUTE METHOD:C1007("Fnd_Hook_List_SetEditableLists";*)  // Get the names of all editable lists.
	End if 
	
	If (Application type:C494#4D Server:K5:6)
		If (Count list items:C380(<>Fnd_List_ListOfLists_i)>0)  // DB050115 - Added
			Fnd_Dlg_Message (Fnd_Gen_GetString ("Fnd_List";"UpdatingMessage"))  // DB060218
			DELAY PROCESS:C323(Current process:C322;120)  // DB050115 - Added
		End if 
	End if 
	
	$wasReadOnly_b:=Read only state:C362(<>Fnd_List_Table_ptr->)
	READ ONLY:C145(<>Fnd_List_Table_ptr->)
	QUERY:C277(<>Fnd_List_Table_ptr->;<>Fnd_List_ListNameFld_ptr->#<>Fnd_List_InfoName_t)
	
	For ($recordNumber_i;1;Records in selection:C76(<>Fnd_List_Table_ptr->))
		EXECUTE METHOD:C1007("Fnd_Host_SaveList";*;<>Fnd_List_ListNameFld_ptr->;<>Fnd_List_ListBlobFld_ptr->)  // Modified by: Walt Nelson (2/3/11)
		NEXT RECORD:C51(<>Fnd_List_Table_ptr->)
	End for 
	
	If (Not:C34($wasReadOnly_b))
		READ WRITE:C146(<>Fnd_List_Table_ptr->)
	End if 
	
	CLEAR SEMAPHORE:C144(<>Fnd_List_Semaphore_t)
	If (Application type:C494#4D Server:K5:6)
		Fnd_Dlg_MessageClose 
	End if 
End if 