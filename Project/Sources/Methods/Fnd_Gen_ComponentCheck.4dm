//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ComponentCheck (calling component; required component)

// Alerts the developer if the specified component isn't available.  Then ends execution.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The internal name of the calling component
//   $2 : Text : The internal name of the required component

// Returns: Nothing

// Created by Dave Batton on Jul 28, 2003
// ----------------------------------------------------

C_TEXT:C284($1; $2; $callingComponent_t; $requiredComponent_t)

$callingComponent_t:=$1
$requiredComponent_t:=$2

// We don't need to call Fnd_Gen_Init here just because the call to Fnd_Gen_ComponentAvailable will do it.

If (Not:C34(Fnd_Gen_ComponentAvailable($requiredComponent_t)))
	ALERT:C41("The "+$callingComponent_t+" component requires the "+$requiredComponent_t+" component. "+" Please install this component.")
	ABORT:C156
End if 
