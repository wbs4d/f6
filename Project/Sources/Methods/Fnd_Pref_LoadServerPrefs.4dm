//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_LoadServerPrefs

  // Loads the user's preferences from the database.
  // Does not load the shared preferences, since we need to get those 
  //  when they're needed, since they may get changed by other users.
  // Called from Fnd_Pref_LoadPrefs, which handles the semaphore.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 6, 2006
  // ----------------------------------------------------

C_TEXT:C284($processName_t)
C_LONGINT:C283($element_i;$state_i;$time_i)

  // We need to be careful not to access the data file if this is a local process.
PROCESS PROPERTIES:C336(Current process:C322;$processName_t;$state_i;$time_i)

If ($processName_t#"$@")
	READ ONLY:C145(<>Fnd_Pref_Table_ptr->)
	QUERY:C277(<>Fnd_Pref_Table_ptr->;<>Fnd_Pref_OwnerFld_ptr->=<>Fnd_Pref_UserName_t)
	
	SELECTION TO ARRAY:C260(<>Fnd_Pref_NameFld_ptr->;<>Fnd_Pref_Names_at;<>Fnd_Pref_TypeFld_ptr->;<>Fnd_Pref_Types_ai;<>Fnd_Pref_ValueFld_ptr->;<>Fnd_Pref_Values_at)
	
	ARRAY INTEGER:C220(<>Fnd_Pref_Scopes_ai;Size of array:C274(<>Fnd_Pref_Names_at))
	ARRAY BOOLEAN:C223(<>Fnd_Pref_Modified_ab;Size of array:C274(<>Fnd_Pref_Names_at))
	For ($element_i;1;Size of array:C274(<>Fnd_Pref_Scopes_ai))
		<>Fnd_Pref_Scopes_ai{$element_i}:=Fnd_Pref_Server
		<>Fnd_Pref_Modified_ab{$element_i}:=False:C215
	End for 
End if 