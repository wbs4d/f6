//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: OutputSort (column; ->field)

  // Part of the Foundation Compatibility component.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The column number
  //   $2 : Pointer : The field to sort

  // Returns: Nothing

  // Created by Dave Batton on Mar 10, 2004
  // ----------------------------------------------------

C_LONGINT:C283($1)
C_POINTER:C301($2)

If (Macintosh option down:C545)
	ORDER BY:C49(Fnd_Gen_CurrentTable ->;$2->;<)
Else 
	ORDER BY:C49(Fnd_Gen_CurrentTable ->;$2->;>)
End if 

OBJECT SET FONT STYLE:C166(*;"Title@";Plain:K14:1)  // Remove the underline from all column titles.
OBJECT SET FONT STYLE:C166(*;"Title"+String:C10($1);Underline:K14:4)