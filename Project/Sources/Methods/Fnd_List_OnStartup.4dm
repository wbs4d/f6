//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_OnStartup

  // Called when the database is launched to determine if the lists in the structure file
  //   neeed to be updated from the lists in the data file.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // ----------------------------------------------------

C_TEXT:C284($structLastExit_t;$dataLastExit_t)

Fnd_List_Init 

If (Application type:C494#4D Remote mode:K5:5)  // Run this with standalone and 4D Server.
	$structLastExit_t:=""
	$dataLastExit_t:=""
	
	
	  // Get the last exit information from the Fnd_List.xml file stored with the structure.
	$structLastExit_t:=Fnd_List_XML_GetDateTime 
	
	  // Get the matching entry from the data file.
	READ ONLY:C145(<>Fnd_List_Table_ptr->)  // We intend on leaving this table in read-only mode.
	QUERY:C277(<>Fnd_List_Table_ptr->;<>Fnd_List_ListNameFld_ptr->=<>Fnd_List_InfoName_t)
	$dataLastExit_t:=BLOB to text:C555(<>Fnd_List_ListBlobFld_ptr->;Mac text without length:K22:10)
	
	
	  // If the date-time stamp from the data and structure file don't match, we need to
	  //   update the lists in the structure using the lists in the data file.
	If ($structLastExit_t#$dataLastExit_t)
		Fnd_List_UpdateStructureLists 
	End if 
	
	  // The date-time stamp in the structure and data file will be updated when the
	  //   On Quit database method runs.
End if   // (Application type#4D Client)
