//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_BackgroundProcess

  // Runs in a background process that diligently tries to make sure everybody receives their messages.
  // Called as a new process from the Fnd_Msg_Send method.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_LONGINT:C283($element_i;$state_i;$msgCount_i)

<>Fnd_Msg_RunBackgroundProcess_b:=Not:C34(Fnd_Gen_QuitNow )

Repeat 
	Repeat 
		
		If (Not:C34(Semaphore:C143(<>Fnd_Msg_SemaphoreName_t;300)))  // Wait up to 5 seconds if the semaphore already exists
			
			$msgCount_i:=0  // Keep track of messages that need to be sent.
			For ($element_i;1;Size of array:C274(<>Fnd_Msg_MsgToProcNums_ai))
				
				If (<>Fnd_Msg_MsgActiveFlags_ab{$element_i})
					$state_i:=Process state:C330(<>Fnd_Msg_MsgToProcNums_ai{$element_i})
					If (($state_i>=0) & (Tickcount:C458<<>Fnd_Msg_MsgEndTime_ai{$element_i}))
						If (($state_i=Delayed:K13:2) | ($state_i=Paused:K13:6))  // If the process is delayed or paused…
							RESUME PROCESS:C320(<>Fnd_Msg_MsgToProcNums_ai{$element_i})  // …get it going again.
						End if 
						POST OUTSIDE CALL:C329(<>Fnd_Msg_MsgToProcNums_ai{$element_i})  // Tell it to check for messages.
						$msgCount_i:=$msgCount_i+1
					Else 
						  // The process is dead or the message has timed-out.  Free up the message slot.
						<>Fnd_Msg_MsgActiveFlags_ab{$element_i}:=False:C215
					End if 
				End if 
				
			End for 
			
			CLEAR SEMAPHORE:C144(<>Fnd_Msg_SemaphoreName_t)
		End if 
		
		  // If (Application type#4D Server )
		  //   CALL PROCESS(Process number(◊Fnd_kMsgMonProcessName))  ` To update the message monitor dialog.
		  // End if 
		DELAY PROCESS:C323(Current process:C322;15)  // Give recipients a chance to get the msg.
	Until (($msgCount_i=0) | (Not:C34(<>Fnd_Msg_RunBackgroundProcess_b)))
	
	If (<>Fnd_Msg_RunBackgroundProcess_b)
		If (Not:C34(Semaphore:C143(<>Fnd_Msg_SemaphoreName_t;300)))  // Wait up to 5 seconds if the semaphore already exists.
			$element_i:=Find in array:C230(<>Fnd_Msg_MsgActiveFlags_ab;True:C214)  // Check again for any active messages.
			CLEAR SEMAPHORE:C144(<>Fnd_Msg_SemaphoreName_t)
			If ($element_i=-1)  // If none exist…
				PAUSE PROCESS:C319(Current process:C322)  // …this process can go to sleep until needed again.
			End if 
		End if 
	End if 
Until ((Not:C34(<>Fnd_Msg_RunBackgroundProcess_b)) | (Fnd_Gen_QuitNow ))

  // Update the message monitor dialog one last time.
  // If (Application type#4D Server )
  //   CALL PROCESS(Process number(◊Fnd_kMsgMonProcessName))
  // End if 
