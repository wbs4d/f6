//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Find_BestPopupSize (->popup array) --> Number

  // Returns the best object width to use for a pop-up menu form object.

  // Access Type: Private

  // Parameters: 
  //   $1 : Pointer : The array associated with the popup

  // Returns: 
  //   $0 : Longint : The best object size for the pop-up

  // Created by Dave Batton on May 13, 2005
  // Modified by Dave Batton on Jun 2, 2005
  //   Added a fix so that calculating the size for the Operators pop-up
  //   won't mess up the current element in the Field Names array.
  // Modified by Dave Batton on Dec 8, 2005
  //   Replaced the call to Fnd_Find_GetArrayWidth with a call to Fnd_Gen_GetArrayWidth..
  // ----------------------------------------------------

C_LONGINT:C283($0;$fontSize_i;$fieldType_i;$element_i;$textWidth_i;$maxTextWidth_i;$originalValue_i;$endCapSize_i;$originalFieldNamesValue_i)
C_POINTER:C301($1;$array_ptr)
C_TEXT:C284($fontName_t;$fieldTypesCalculated_t)

$array_ptr:=$1

$originalValue_i:=$array_ptr->  // So we preserve the selected element.

If (Is Windows:C1573)
	$fontName_t:="Tahoma"
	$fontSize_i:=12
	$endCapSize_i:=40
Else 
	$fontName_t:="Lucida Grande"
	$fontSize_i:=13
	$endCapSize_i:=40
End if 

$maxTextWidth_i:=0

If ($array_ptr=(->Fnd_Find_OperatorStrs_at))  // We need special handling for this array, since its values change.
	$originalFieldNamesValue_i:=Fnd_Find_FieldNames_at  // DB050602 - So we can restore this to its original value.
	$fieldTypesCalculated_t:=""  // We build a string of field type numbers as we calculate them.
	For ($element_i;1;Size of array:C274(Fnd_Find_FieldTypes_ai))
		$fieldType_i:=Fnd_Find_FieldTypes_ai{$element_i}
		If (Position:C15(String:C10($fieldType_i);$fieldTypesCalculated_t)=0)  // If we haven't already done this one...
			Fnd_Find_FieldNames_at:=$element_i
			Fnd_Find_UpdateOperators 
			$textWidth_i:=Fnd_Gen_GetArrayWidth (->Fnd_Find_OperatorStrs_at;$fontName_t;$fontSize_i;Plain:K14:1)
			If ($textWidth_i>$maxTextWidth_i)
				$maxTextWidth_i:=$textWidth_i
			End if 
			Case of 
				: (($fieldType_i=Is alpha field:K8:1) | ($fieldType_i=Is text:K8:3))  // These types use the same operators.
					$fieldTypesCalculated_t:=$fieldTypesCalculated_t+" 0 2"  // Save some time by not repeating the other.
				: (($fieldType_i=Is integer:K8:5) | ($fieldType_i=Is longint:K8:6) | ($fieldType_i=Is real:K8:4))
					$fieldTypesCalculated_t:=$fieldTypesCalculated_t+" 1 8 9"
				: (($fieldType_i=Is date:K8:7) | ($fieldType_i=Is time:K8:8))
					$fieldTypesCalculated_t:=$fieldTypesCalculated_t+" 4 11"
				Else 
					$fieldTypesCalculated_t:=$fieldTypesCalculated_t+" "+String:C10($fieldType_i)
			End case 
		End if 
	End for 
	Fnd_Find_FieldNames_at:=$originalFieldNamesValue_i  // DB050602 - Restore the array's original value.
	
Else   // Any other pop-up can be handled with this simple code.
	$textWidth_i:=Fnd_Gen_GetArrayWidth (->Fnd_Find_FieldNames_at;$fontName_t;$fontSize_i;Plain:K14:1)
	If ($textWidth_i>$maxTextWidth_i)
		$maxTextWidth_i:=$textWidth_i
	End if 
End if 

$array_ptr->:=$originalValue_i  // Restore the original value.

$0:=$maxTextWidth_i+$endCapSize_i