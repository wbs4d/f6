//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetSharedValue (pref name; ->value) --> Boolean

// Returns a shared preference value from the database.

// Access Type: Private

// Parameters: 
//   $1 : Text : The preference name
//   $2 : Pointer : Where to put the value if found

// Returns: 
//   $0 : Boolean : True if found

// Created by Dave Batton on Feb 6, 2006
// ----------------------------------------------------

C_BOOLEAN:C305($0; $found_b)
C_TEXT:C284($1; $itemName_t; $processName_t)
C_POINTER:C301($2; $itemValue_ptr)
C_LONGINT:C283($state_i; $time_i)

$itemName_t:=$1
$itemValue_ptr:=$2

$found_b:=False:C215

// We need to be careful not to access the data file if this is a local process.
PROCESS PROPERTIES:C336(Current process:C322; $processName_t; $state_i; $time_i)

If ($processName_t#"$@")
	READ ONLY:C145(<>Fnd_Pref_Table_ptr->)
	QUERY:C277(<>Fnd_Pref_Table_ptr->; <>Fnd_Pref_OwnerFld_ptr->=Fnd_Pref_SharedName; *)
	QUERY:C277(<>Fnd_Pref_Table_ptr->;  & ; <>Fnd_Pref_NameFld_ptr->=$itemName_t)
	
	If (Records in selection:C76(<>Fnd_Pref_Table_ptr->)>0)
		If (Type:C295($itemValue_ptr->)=<>Fnd_Pref_TypeFld_ptr->)  // Bad to stuff it in the variable if the types don't match.
			Case of 
				: (<>Fnd_Pref_TypeFld_ptr->=Is text:K8:3)
					$itemValue_ptr->:=<>Fnd_Pref_ValueFld_ptr->
				: ((<>Fnd_Pref_TypeFld_ptr->=Is longint:K8:6) | (<>Fnd_Pref_TypeFld_ptr->=Is real:K8:4))
					$itemValue_ptr->:=Num:C11(<>Fnd_Pref_ValueFld_ptr->)
				: (<>Fnd_Pref_TypeFld_ptr->=Is boolean:K8:9)
					$itemValue_ptr->:=(<>Fnd_Pref_ValueFld_ptr->="true")
				: (<>Fnd_Pref_TypeFld_ptr->=Is object:K8:27)
					$itemValue_ptr->:=JSON Parse:C1218(<>Fnd_Pref_ValueFld_ptr->)
				Else 
					Fnd_Gen_BugAlert(Current method name:C684; "Unexpected data type.")
			End case 
			$found_b:=True:C214
		End if 
	End if 
End if 

$0:=$found_b