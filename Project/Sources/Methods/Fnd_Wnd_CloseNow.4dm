//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_CloseNow --> Boolean

// Returns True if Foundation is trying to close all windows due to
//   the user Option-clicking a window's close box.

// Access Type: Shared

// Parameters: None

// Returns: 
//   $0 : Boolean : True if it's time to close the window

// Created by Dave Batton on Sep 11, 2003
// ----------------------------------------------------

C_BOOLEAN:C305($0)

Fnd_Wnd_Init

$0:=<>Fnd_Wnd_Closing_b
