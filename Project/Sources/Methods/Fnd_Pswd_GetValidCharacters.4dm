//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_GetValidCharacters --> Text

  // Returns a string with all of the allowed password characters
  //   (based on the user's preference settings).

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Text : A string of all valid characters

  // Created by Dave Batton on Mar 16, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$validChars_t;$excludeChars_t;$invalidChar_t)
C_LONGINT:C283($position_i)

$validChars_t:=""

If (<>Fnd_Pswd_UseUppercase_b)
	$validChars_t:=$validChars_t+"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
End if 

If (<>Fnd_Pswd_UseLowercase_b)
	$validChars_t:=$validChars_t+"abcdefghijklmnopqrstuvwxyz"
End if 

If (<>Fnd_Pswd_UseNumbers_b)
	$validChars_t:=$validChars_t+"0123456789"
End if 

If (<>Fnd_Pswd_UseSymbols_b)
	$validChars_t:=$validChars_t+"!@#$%^&*()-+=/\\[]{}?"
End if 

If (<>Fnd_Pswd_UseCustom_b)
	$validChars_t:=$validChars_t+<>Fnd_Pswd_CustomCharacters_t
End if 

If (<>Fnd_Pswd_UseExcluded_b)
	$excludeChars_t:=<>Fnd_Pswd_ExcludedCharacters_t
	For ($position_i;1;Length:C16($excludeChars_t))
		$invalidChar_t:=$excludeChars_t[[$position_i]]
		$validChars_t:=Replace string:C233($validChars_t;$invalidChar_t;"")
	End for 
End if 

$0:=$validChars_t