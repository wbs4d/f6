//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_AddButtonMethod (method; label{; foreground color; background color})

// Adds a new button to the palette and assigns the method as its action.
// If a button already exists with that label, it is updated.

// Access: Shared

// Parameters:
//   $1 : Text : The method name to execute
//   $2 : Text : The button label
//   $3 : Longint : The text color (optional)
//   $4 : Longint : The background color (optional)

// Returns: Nothing

// Created by Dave Batton on Sep 11, 2003
// Modified by Dave Batton on Jul 1, 2004
//   This method can now be called to update the palette while it's displayed.
// ----------------------------------------------------

C_TEXT:C284($1; $2; $methodName_t; $label_t)
C_LONGINT:C283($3; $4; $buttonNumber_i)
C_POINTER:C301($null_ptr)

$methodName_t:=$1
$label_t:=$2

Fnd_Nav_Init

If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 300)))  // Wait up to 5 seconds.
	$buttonNumber_i:=Fnd_Nav_Add($label_t)
	
	<>Fnd_Nav_Methods_at{$buttonNumber_i}:=$methodName_t
	
	If (Count parameters:C259>=3)
		<>Fnd_Nav_ForegroundColors_ai{$buttonNumber_i}:=$3
		<>Fnd_Nav_BackgroundColors_ai{$buttonNumber_i}:=$4
	End if 
	
	CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
End if 