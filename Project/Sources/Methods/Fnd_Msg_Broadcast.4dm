//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_Broadcast (process name; message)

  // Sends the message out to all processes with the specified name.
  // The name can contain wildcard characters.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The process name to send to
  //   $2 : Text : The message to send

  // Returns: Nothing

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$broadcastName_t;$message_t;$processName_t)
C_LONGINT:C283($processNumber_i;$processState_i;$processTime_i;$uniqueID_i;$origin_i)
C_BOOLEAN:C305($processVisible_b)

$broadcastName_t:=$1
$message_t:=$2

For ($processNumber_i;1;Count tasks:C335)
	PROCESS PROPERTIES:C336($processNumber_i;$processName_t;$processState_i;$processTime_i;$processVisible_b;$uniqueID_i;$origin_i)
	If ($processState_i>0)
		If ($origin_i>0)  // Don't broadcast to 4D created processes.
			If ($processName_t=$broadcastName_t)
				Fnd_Msg_Send ($processNumber_i;$message_t)
			End if 
		End if 
	End if 
End for 