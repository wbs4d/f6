//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_EditorClosed --> Boolean

  // Makes sure 4D's List Editor window isn't open. If it is, the user is asked to close it.

  // Access Type: Private

  // Parameters: 

  // Returns: 
  //   $0 : Boolean : True if the List Editor is not open

  // Created by Dave Batton on Jun 11, 2004
  // Modified by : Vincent Tournier (06/16/2011)
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$closed_b;<>Fnd_Loc_EditorSkipNextCheck_b)
C_TEXT:C284($localizedWindowTitle_t)
C_LONGINT:C283($windowNumber_i)

$closed_b:=True:C214

If (<>Fnd_Loc_EditorSkipNextCheck_b)  // Let's us to this check in an On Activate form event.
	<>Fnd_Loc_EditorSkipNextCheck_b:=False:C215
	
Else 
	
	  // BEGIN Modified by : Vincent Tournier (6/16/2011)
	  //$localizedWindowTitle_t:=Lire chaine dans liste(1046;1)
	$localizedWindowTitle_t:=Fnd_Loc_GetString ("Fnd_List";"EditorWindowTitle")
	  // END Modified by : Vincent Tournier (6/16/2011)
	
	WINDOW LIST:C442($windows_ai)
	
	For ($windowNumber_i;1;Size of array:C274($windows_ai))
		If (Get window title:C450($windows_ai{$windowNumber_i})=$localizedWindowTitle_t)
			$closed_b:=False:C215
			ALERT:C41("To avoid list corruption, close the 4D List Editor window before "+"using the Foundation Localization Editor.")
			If (Form event:C388=On Activate:K2:9)
				<>Fnd_Loc_EditorSkipNextCheck_b:=True:C214  // So we don't get stuck in an endless loop.
			End if 
		End if 
	End for 
End if 

$0:=$closed_b
