//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_GetWindow (name; ->left; ->top; ->right; ->bottom)

// Retrieves a text value in from user's preferences file.  If an item with the
//   specified name isn't found, the default value is returned (if specified).

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Pointer : The left coordinate
//   $3 : Pointer : The top coordinate
//   $4 : Pointer : The right coordinate
//   $5 : Pointer : The bottom coordinate

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
// Modified by Wayne Stewart on 2023-07-27:
//   Remove the semaphore for read only access
// ----------------------------------------------------

C_TEXT:C284($1; $prefName_t; $prefValue_t)
C_POINTER:C301($2; $3; $4; $5; $left_ptr; $top_ptr; $right_ptr; $bottom_ptr)
C_LONGINT:C283($element_i; $left_i; $top_i; $right_i; $bottom_i; $separator_i)

$prefName_t:=$1
$left_ptr:=$2
$top_ptr:=$3
$right_ptr:=$4
$bottom_ptr:=$5

Fnd_Pref_Init

$element_i:=Find in array:C230(<>Fnd_Pref_Names_at; $prefName_t)

If ($element_i>0)
	If (<>Fnd_Pref_Types_ai{$element_i}=-1)
		$prefValue_t:=<>Fnd_Pref_Values_at{$element_i}
		
		$separator_i:=(Position:C15(", "; $prefValue_t))-1
		$left_i:=Num:C11(Substring:C12($prefValue_t; 1; $separator_i))
		$prefValue_t:=Substring:C12($prefValue_t; $separator_i+3)
		$separator_i:=(Position:C15(", "; $prefValue_t))-1
		$top_i:=Num:C11(Substring:C12($prefValue_t; 1; $separator_i))
		$prefValue_t:=Substring:C12($prefValue_t; $separator_i+3)
		$separator_i:=(Position:C15(", "; $prefValue_t))-1
		$right_i:=Num:C11(Substring:C12($prefValue_t; 1; $separator_i))
		$prefValue_t:=Substring:C12($prefValue_t; $separator_i+3)
		$bottom_i:=Num:C11(Substring:C12($prefValue_t; 1))
		
		// Only muck with the pointers if we got valid values.
		If (($left_i>0) | ($top_i>0) | ($right_i>0) | ($bottom_i>0))
			$left_ptr->:=$left_i
			$top_ptr->:=$top_i
			$right_ptr->:=$right_i
			$bottom_ptr->:=$bottom_i
		End if 
	End if 
End if 

