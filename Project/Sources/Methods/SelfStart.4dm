//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: SelfStart (method name; process name)

  // Converts a call to Foundation 3's SelfStart routine to the new
  //   Fnd_Gen_LaunchAsNewProcess routine.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the calling method
  //   $2 : Text : The process name

  // Returns: 
  //   $0 : Boolean : True if we're in a new process

  // Created by Dave Batton on Mar 10, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Corrected parameter list in header

  // ----------------------------------------------------

C_BOOLEAN:C305($0)
C_TEXT:C284($1;$2)

$0:=Fnd_Gen_LaunchAsNewProcess ($1;$2)