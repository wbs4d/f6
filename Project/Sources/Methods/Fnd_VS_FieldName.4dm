//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_FieldName (->field) --> Text

  // Returns the virtual field name of the specified field.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a field

  // Returns: 
  //   $0 : Text : The virtual field name

  // Created by Dave Batton on Sep 8, 2003
  // Modified by Dave Batton on Jan 24, 2004
  //   Now returns virtual field names.
  // ----------------------------------------------------

C_TEXT:C284($0;$fieldName_t)
C_POINTER:C301($1;$field_ptr)
C_LONGINT:C283($fieldNumber_i;$tableNumber_i;$fieldElement_i;$tableElement_i)

Fnd_VS_Init 

$field_ptr:=$1

$fieldName_t:=""

$fieldNumber_i:=Field:C253($field_ptr)
$tableNumber_i:=Table:C252($field_ptr)

If (Is table number valid:C999($tableNumber_i))
	If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))
		$fieldName_t:=Field name:C257($field_ptr)  // The default value.
		
		If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
			$tableElement_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)
			If ($tableElement_i>0)
				$fieldElement_i:=Find in array:C230(<>Fnd_VS_FieldNumbers_ai{$tableElement_i};$fieldNumber_i)
				If ($fieldElement_i>0)
					$fieldName_t:=<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i}
				End if 
			End if 
			CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
		End if 
	End if 
End if 

$0:=$fieldName_t