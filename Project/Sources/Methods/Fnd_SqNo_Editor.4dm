//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_SqNo_Editor

// Displays the Sequence Number Editor dialog.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 19, 2003
// Modified by Dave Batton on Jan 16, 2004
//   Moved the CLOSE WINDOW command to after the Fnd_Menu_Window_Remove call.
// Modified by Dave Batton on May 20, 2004
//   Removed an ORDER BY call and added a SORT LIST call.
//   Now even the record number based sequence numbers are sorted properly.
//   Now tests for the Menus component before calling menu routines.
// Modified by Dave Batton on Nov 9, 2004
//   Updated the window position preference name to include "Fnd_SqNo:".
// Modified by Dave Batton on May 28, 2005
//   Updated to use the new Fnd_Wnd methods.
// Modified by Dave Batton on Mar 23, 2006
//   This routine now references tables and fields using pointers.
//   Changed the call to Fnd_VS_TableName to Fnd_SqNo_TableName.
// ----------------------------------------------------

C_TEXT:C284($groupName_t; $windowPrefName_t; $tableName_t)
C_LONGINT:C283($tableNumber_i)

Fnd_SqNo_Init

If (Fnd_Gen_LaunchAsNewProcess(Current method name:C684; "Fnd_SqNo: Editor Window"))
	Fnd_Gen_CurrentFormType(Fnd_Gen_SqNoEditorForm)
	
	Fnd_Gen_MenuBar
	
	OK:=1  // In case we don't display the Confirm dialog.
	If (Count users:C342>1)  // If other users are online…
		Fnd_Dlg_SetIcon(Fnd_Dlg_WarnIcon)
		Fnd_Dlg_Confirm(Fnd_Gen_GetString("Fnd_SqNo"; "OtherUsersOnline"))  // ...display a warning.
	End if 
	
	If (OK=1)
		// Find the sequence number records to display.
		READ ONLY:C145(<>Fnd_SqNo_Table_ptr->)  // We don't want to lock any records yet, since other users may need them.
		If (Not:C34(Fnd_SqNo_IsInDesignGroup))
			QUERY:C277(<>Fnd_SqNo_Table_ptr->; <>Fnd_SqNo_DesignerOnlyFld_ptr->=False:C215)  // Give them access only to Administrator level sequence numbers.
		Else 
			ALL RECORDS:C47(<>Fnd_SqNo_Table_ptr->)
		End if 
		
		If ((Records in selection:C76(<>Fnd_SqNo_Table_ptr->)=0) & (Not:C34(Fnd_SqNo_IsInDesignGroup)))
			Fnd_Dlg_SetIcon(Fnd_Dlg_StopIcon)
			Fnd_Dlg_Alert(Fnd_Gen_GetString("Fnd_SqNo"; "NothingToEdit"))
			
		Else 
			// Put the names of the records into a hierarchical list.
			<>Fnd_SqNo_NamesList_i:=New list:C375
			While (Not:C34(End selection:C36(<>Fnd_SqNo_Table_ptr->)))
				$groupName_t:=<>Fnd_SqNo_GroupNameFld_ptr->
				If ($groupName_t="*Table #@")
					$tableNumber_i:=Num:C11(Substring:C12($groupName_t; (Position:C15("#"; $groupName_t)+1)))
					$tableName_t:=Fnd_SqNo_TableName(Table:C252($tableNumber_i))
					$groupName_t:=Fnd_Gen_GetString("Fnd_SqNo"; "IDFieldGroupName"; $tableName_t)
				End if 
				APPEND TO LIST:C376(<>Fnd_SqNo_NamesList_i; $groupName_t; <>Fnd_SqNo_IDFld_ptr->)
				SET LIST ITEM PROPERTIES:C386(<>Fnd_SqNo_NamesList_i; <>Fnd_SqNo_IDFld_ptr->; True:C214; Plain:K14:1; 0)  // Make the item enterable.
				NEXT RECORD:C51(<>Fnd_SqNo_Table_ptr->)
			End while 
			
			SORT LIST:C391(<>Fnd_SqNo_NamesList_i; >)  // DB040520 -  Added
			
			Fnd_Wnd_Type(Plain window:K34:13)  // DB050528 - Changed from Fnd_Wnd_SetType.
			Fnd_Wnd_Title(Fnd_Gen_GetString("Fnd_SqNo"; "WindowTitle"))  // DB050528 - Changed from Fnd_Wnd_SetTitle.
			Fnd_Wnd_CloseBox(True:C214)  // DB050528 - Changed from Fnd_Wnd_SetCloseBox.
			$windowPrefName_t:="Fnd_SqNo: Sequence Number Editor Window"
			Fnd_Wnd_UseSavedPosition($windowPrefName_t)  // DB041109 - Added the preference name.
			Fnd_Wnd_OpenWindow(400; 210)
			
			//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))  // DB040520 - Added this test.
			//EXECUTE METHOD("Fnd_Menu_Window_Add"; *)
			//End if 
			Fnd_Menu_Window_Add
			Fnd_Wnd_SendCloseRequests
			
			DIALOG:C40("Fnd_SqNo_Editor")
			
			Fnd_Wnd_SavePosition($windowPrefName_t)  // DB041109 - Added the preference name.
			//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))  // DB040520 - Added this test.
			//EXECUTE METHOD("Fnd_Menu_Window_Remove"; *)
			//End if 
			Fnd_Menu_Window_Remove
			
			CLOSE WINDOW:C154  // DB040116 - Moved this so it's called after Fnd_Menu_Window_Remove.
			
			UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure we don't leave any records locked.
			CLEAR LIST:C377(<>Fnd_SqNo_NamesList_i; *)
		End if 
	End if 
End if 
