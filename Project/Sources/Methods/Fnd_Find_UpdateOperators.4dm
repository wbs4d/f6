//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Find_UpdateOperators


  // Updates the Search Operators pop-up menu depending on the type of the selected field.


  //Access Type: Private


  // Parameters: None


  // Returns: Nothing


  // Created by Dave Batton on Jul 14, 2003

  // Modified by: Walt W Nelson (12/28/15) - added check for 64-bit integer type


  // ----------------------------------------------------


C_LONGINT:C283($fieldType_i;$element_i)
C_TEXT:C284($oldOperator)

$oldOperator:=Fnd_Find_OperatorStrs_at{0}  // Save this so we can try to preserve it.


$fieldType_i:=Fnd_Find_FieldTypes_ai{Fnd_Find_FieldNames_at}

Case of 
	: ($fieldType_i=Pointer array:K8:23)  // It's a multi-field search.
		
		ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;4)
		Fnd_Find_OperatorStrs_at{1}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpContains")
		Fnd_Find_OperatorStrs_at{2}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpIs")
		Fnd_Find_OperatorStrs_at{3}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpStartsWith")
		Fnd_Find_OperatorStrs_at{4}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpEndsWith")
		
		ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;4)
		Fnd_Find_OperatorCodes_at{1}:="C"
		Fnd_Find_OperatorCodes_at{2}:="="
		Fnd_Find_OperatorCodes_at{3}:="SW"
		Fnd_Find_OperatorCodes_at{4}:="EW"
		
		Fnd_Find_OperatorStrs_at:=1  // Default to "contains" search.
		
		
		
	: (($fieldType_i=Is alpha field:K8:1) | ($fieldType_i=Is text:K8:3))
		ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;8)
		Fnd_Find_OperatorStrs_at{1}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpContains")
		Fnd_Find_OperatorStrs_at{2}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpDoesntContain")
		Fnd_Find_OperatorStrs_at{3}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpIs")
		Fnd_Find_OperatorStrs_at{4}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpIsNot")
		Fnd_Find_OperatorStrs_at{5}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpStartsWith")
		Fnd_Find_OperatorStrs_at{6}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpEndsWith")
		Fnd_Find_OperatorStrs_at{7}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpIsBefore")
		Fnd_Find_OperatorStrs_at{8}:=Fnd_Gen_GetString ("Fnd_Find";"StringOpIsAfter")
		
		ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;8)
		Fnd_Find_OperatorCodes_at{1}:="C"
		Fnd_Find_OperatorCodes_at{2}:="DC"
		Fnd_Find_OperatorCodes_at{3}:="="
		Fnd_Find_OperatorCodes_at{4}:="#"
		Fnd_Find_OperatorCodes_at{5}:="SW"
		Fnd_Find_OperatorCodes_at{6}:="EW"
		Fnd_Find_OperatorCodes_at{7}:="<"
		Fnd_Find_OperatorCodes_at{8}:=">"
		
		Fnd_Find_OperatorStrs_at:=1  // Default to "contains" search.
		
		
		
	: (($fieldType_i=Is integer:K8:5) | ($fieldType_i=Is longint:K8:6) | ($fieldType_i=Is real:K8:4) | ($fieldType_i=Is integer 64 bits:K8:25))  // December 28, 2015: added 64-bit integer
		ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;6)
		Fnd_Find_OperatorStrs_at{1}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpEquals")
		Fnd_Find_OperatorStrs_at{2}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpNotEqual")
		Fnd_Find_OperatorStrs_at{3}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpIsLessThan")
		Fnd_Find_OperatorStrs_at{4}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpIsMoreThan")
		Fnd_Find_OperatorStrs_at{5}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpIsLessThanOrEquals")
		Fnd_Find_OperatorStrs_at{6}:=Fnd_Gen_GetString ("Fnd_Find";"NumOpIsMoreThanOrEquals")
		
		ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;6)
		Fnd_Find_OperatorCodes_at{1}:="N="
		Fnd_Find_OperatorCodes_at{2}:="N#"
		Fnd_Find_OperatorCodes_at{3}:="N<"
		Fnd_Find_OperatorCodes_at{4}:="N>"
		Fnd_Find_OperatorCodes_at{5}:="N<="
		Fnd_Find_OperatorCodes_at{6}:="N>="
		
		Fnd_Find_OperatorStrs_at:=1  // Default to "equals" search.
		
		
		
	: (($fieldType_i=Is date:K8:7) | ($fieldType_i=Is time:K8:8))
		ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;4)
		Fnd_Find_OperatorStrs_at{1}:=Fnd_Gen_GetString ("Fnd_Find";"DateOpIs")
		Fnd_Find_OperatorStrs_at{2}:=Fnd_Gen_GetString ("Fnd_Find";"DateOpIsBefore")
		Fnd_Find_OperatorStrs_at{3}:=Fnd_Gen_GetString ("Fnd_Find";"DateOpIsAfter")
		Fnd_Find_OperatorStrs_at{4}:=Fnd_Gen_GetString ("Fnd_Find";"DateOpIsNot")
		
		ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;4)
		Fnd_Find_OperatorCodes_at{1}:="="
		Fnd_Find_OperatorCodes_at{2}:="<"
		Fnd_Find_OperatorCodes_at{3}:=">"
		Fnd_Find_OperatorCodes_at{4}:="#"
		
		Fnd_Find_OperatorStrs_at:=1  // Default to "is" search.
		
		
		
	: ($fieldType_i=Is boolean:K8:9)
		ARRAY TEXT:C222(Fnd_Find_OperatorStrs_at;2)
		Fnd_Find_OperatorStrs_at{1}:=Fnd_Gen_GetString ("Fnd_Find";"BooleanOpIsTrue")
		Fnd_Find_OperatorStrs_at{2}:=Fnd_Gen_GetString ("Fnd_Find";"BooleanOpIsFalse")
		
		ARRAY TEXT:C222(Fnd_Find_OperatorCodes_at;2)
		Fnd_Find_OperatorCodes_at{1}:="T"
		Fnd_Find_OperatorCodes_at{2}:="F"
		
		Fnd_Find_OperatorStrs_at:=1  // Default to "True".
		
		
		
	: ($fieldType_i=Is subtable:K8:11)
		Fnd_Gen_BugAlert ("Fnd_Find_UpdateOperators";"The Fnd_Find_AddSubfield routine must be used to add subfields to the Find dialog"+".")
		
		
	Else 
		Fnd_Gen_BugAlert ("Fnd_Find_UpdateOperators";"Unknown field type: "+String:C10($fieldType_i))
End case 

If ($fieldType_i=Is boolean:K8:9)  // It's a Boolean.
	
	Fnd_Find_SearchString_t:=""
	OBJECT SET VISIBLE:C603(Fnd_Find_SearchString_t;False:C215)  // No need to enter value for Boolean.
	
Else 
	OBJECT SET VISIBLE:C603(Fnd_Find_SearchString_t;True:C214)
End if 

$element_i:=Find in array:C230(Fnd_Find_OperatorStrs_at;$oldOperator)
If ($element_i>0)
	Fnd_Find_OperatorStrs_at:=$element_i
End if 

