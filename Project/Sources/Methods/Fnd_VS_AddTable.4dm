//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_AddTable (->table)

  // Adds the specified table, and all of its fields, to the virtual structure arrays.
  // Doesn't call 4D's SET TABLE TITLES command.
  // This is a private method. To add a table the developer will normally call
  //   the Fnd_VS_ShowTable method.
  // We don't use semaphores here. We assume the calling process is
  //   using the semaphore.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : The table to add

  // Returns: Nothing

  // Created by Dave Batton on Mar 4, 2004
  // Modified by Dave Batton on Feb 13, 2005
  //   Added some code to deal with problem characters in the names of deleted tables.
  // [1] Modified by: John Craig (5/13/2009) - Modifications to avoid issues with deleted tables and fields
  // [2] Added by: John Craig (7/2/09) - This prevents blank entries from being added to the array which 4D does not like in the SET FIELD TITLES method
  // Modification : Vincent TOURNIER (12/9/11)
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($element_i;$tableNumber_i;$fieldNumber_i)
C_TEXT:C284($tableName_t)

$table_ptr:=$1

$tableNumber_i:=Table:C252($table_ptr)

If (Is table number valid:C999($tableNumber_i))
	
	If (Fnd_VS_NbrValidFields ($tableNumber_i)>0)  // any valid field in this table? Modification : Vincent TOURNIER(9/12/11)
		
		  // This routine sometimes gets deleted tables from 4D 2004. These start with"("which
		  // isn't a valid table name. So these must be changed to valid table names
		  // before we can continue.
		  // We only handle deleted tables here. Any other errors are up to the developer to 
		$tableName_t:=Table name:C256($tableNumber_i)
		If ($tableName_t="(@")
			$tableName_t:=Replace string:C233($tableName_t;"(";"x")
			$tableName_t:=Replace string:C233($tableName_t;")";"")
			$tableName_t:=Replace string:C233($tableName_t;".";"")
			$tableName_t:=Replace string:C233($tableName_t;"-";"")
		End if 
		
		$element_i:=Size of array:C274(<>Fnd_VS_TableNames_at)+1
		
		INSERT IN ARRAY:C227(<>Fnd_VS_TableNames_at;$element_i)
		INSERT IN ARRAY:C227(<>Fnd_VS_TableNumbers_ai;$element_i)
		<>Fnd_VS_TableNames_at{$element_i}:=$tableName_t
		<>Fnd_VS_TableNumbers_ai{$element_i}:=$tableNumber_i
		
		INSERT IN ARRAY:C227(<>Fnd_VS_FieldNames_at;$element_i)
		INSERT IN ARRAY:C227(<>Fnd_VS_FieldNumbers_ai;$element_i)
		  // [1] Modified by: John Craig(5/13/9)Commented out the following two lines
		  // [1] INSERT IN ARRAY(◊Fnd_VS_FieldNames_at{$element_i};1;Get last field number($tableNumber_i))
		  // [1] INSERT IN ARRAY(◊Fnd_VS_FieldNumbers_ai{$element_i};1;Get last field number($tableNumber_i))
		
		For ($fieldNumber_i;1;Get last field number:C255($tableNumber_i))
			If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))  // [2] Added by: John Craig(7/2/9)-This prevents blank entries from being added to the array
				
				  // [1] Modified by: John Craig(5/13/9)Commented out the following two lines
				  // [1]◊Fnd_VS_FieldNames_at{$element_i}{$fieldNumber_i}:=Field name($tableNumber_i;$fieldNumber_i)
				  // [1]◊Fnd_VS_FieldNumbers_ai{$element_i}{$fieldNumber_i}:=$fieldNumber_i
				
				  // [1] Modified by: John Craig(5/13/9)Added the following two lines
				APPEND TO ARRAY:C911(<>Fnd_VS_FieldNames_at{$element_i};Field name:C257($tableNumber_i;$fieldNumber_i))  // [1]
				APPEND TO ARRAY:C911(<>Fnd_VS_FieldNumbers_ai{$element_i};$fieldNumber_i)  // [1]
				
			End if   // [2] Added by: John Craig(7/2/9)-This prevents blank entries from being added to the array
			
		End for 
	End if 
End if 