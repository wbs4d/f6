//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_VerifyRecord

  // Makes sure the current [Fnd_Reg] record's Checksum field is correct.
  // If not, the record is updated so that it is expired. Theoretically this will
  //   happen only if the user tampers with a [Fnd_Reg] record. Otherwise
  //   processing is allowed to continue uninterrupted.
  // Assumes that there is exactly one record in the current selection.
  // Fnd_Reg_SaveRecord is called to set the checksum.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Apr 24, 2005
  // Modified by Gary Boudreaux on Dec 22, 2008
  //   Removed extraneous description of $1 in header info
  //   NOTE: does this method need to check for deleted fields?
  // Modified by: Walt Nelson (3/16/10) per Barclay Berry 
  //  mac string(!1/1/2010!) will return 1/1/10 and on windows it will return 1/1/2010
  // ----------------------------------------------------

C_TEXT:C284($text_t)
C_LONGINT:C283($field_i)
C_POINTER:C301($field_ptr)

If (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=1)  // We're supposed to make sure we have just one record before calling this method.
	$text_t:=<>Fnd_Reg_SecretKey_t
	For ($field_i;1;Get last field number:C255(<>Fnd_Reg_Table_ptr))
		$field_ptr:=Field:C253(Table:C252(<>Fnd_Reg_Table_ptr);$field_i)
		
		Case of 
			: ($field_ptr=(<>Fnd_Reg_ChecksumFld_ptr))
				  // Don't use this in the calculation.
				
			: (Type:C295($field_ptr->)=Is date:K8:7)  // Modified by: Walt Nelson (3/16/10)
				$text_t:=$text_t+String:C10(Year of:C25($field_ptr->))+String:C10(Month of:C24($field_ptr->))+String:C10(Day of:C23($field_ptr->))
				  //End Modified by: Walt Nelson (3/16/10)
			: (Type:C295($field_ptr->)#Is alpha field:K8:1)  // We're not using text fields or Booleans.
				$text_t:=$text_t+String:C10($field_ptr->)  // So this works on all other data types.
				
			Else 
				$text_t:=$text_t+$field_ptr->
		End case 
		
	End for 
	
	If (<>Fnd_Reg_ChecksumFld_ptr->#Fnd_Text_TextToMD5 ($text_t))
		  // The checksum didn't match. Update the record so that it is expired.
		  // This should never happen, but we handle it just in case.
		READ WRITE:C146(<>Fnd_Reg_Table_ptr->)
		LOAD RECORD:C52(<>Fnd_Reg_Table_ptr->)
		<>Fnd_Reg_UnlockCodeFld_ptr->:=""
		<>Fnd_Reg_UserNameFld_ptr->:=""
		<>Fnd_Reg_LicenseInfoFld_ptr->:=""
		<>Fnd_Reg_DemoDaysFld_ptr->:=<>Fnd_Reg_DefaultDemoDays_i
		<>Fnd_Reg_DemoStartDateFld_ptr->:=Current date:C33-<>Fnd_Reg_DefaultDemoDays_i-1
		Fnd_Reg_SaveRecord (<>Fnd_Reg_SecretKey_t)
		Fnd_Reg_Alert (Fnd_Gen_GetString ("Fnd_Reg";"BadRegRecord"))
	End if 
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"There are "+String:C10(Records in selection:C76(<>Fnd_Reg_Table_ptr->))+" records in the selection. There should be only one.")
	ABORT:C156  // So the user can't use the product/feature.
End if 