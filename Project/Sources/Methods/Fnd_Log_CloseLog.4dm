//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_CloseLog {(Log Label)}

// Pass the log label to save and close that log
// If you call this without any parameters all the logs will be closed and saved

// Access: Shared

// Parameters: 
//   $1 : Text : The log label (optional)

// Created by Wayne Stewart (2018-08-05)
//     waynestewart@mac.com
// ----------------------------------------------------

C_TEXT:C284($1)

If (Count parameters:C259=1)
	CALL WORKER:C1389(Fnd_Log_Writer; "Fnd_Log_CloseLog2"; $1)
Else 
	CALL WORKER:C1389(Fnd_Log_Writer; "Fnd_Log_CloseLog2")
End if 



