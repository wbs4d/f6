//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Pswd

  // Compiler variables related to the Foundation Pswd routines.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Mar 16, 2004
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Pswd_Initialized_b)
If (Not:C34(<>Fnd_Pswd_Initialized_b))
	C_TEXT:C284(<>Fnd_Pswd_GeneratedPassword_t)
	C_TEXT:C284(<>Fnd_Pswd_BulletChar_t;<>Fnd_Pswd_CustomCharacters_t;<>Fnd_Pswd_ExcludedCharacters_t)
	C_TEXT:C284(<>Fnd_Pswd_Label1_t;<>Fnd_Pswd_Label2_t;<>Fnd_Pswd_Label3_t;<>Fnd_Pswd_Label4_t)
	C_TEXT:C284(<>Fnd_Pswd_WindowTitle_t)
	C_LONGINT:C283(<>Fnd_Pswd_UppercaseCheckbox_i;<>Fnd_Pswd_LowercaseCheckbox_i;<>Fnd_Pswd_NumbersCheckbox_i)
	C_LONGINT:C283(<>Fnd_Pswd_SymbolsCheckbox_i;<>Fnd_Pswd_CustomCheckbox_i;<>Fnd_Pswd_ExcludeCheckbox_i)
	C_LONGINT:C283(<>Fnd_Pswd_GenerateButton_i;<>Fnd_Pswd_OKButton_i;<>Fnd_Pswd_CancelButton_i;<>Fnd_Pswd_OffscreenButton_i)
	C_LONGINT:C283(<>Fnd_Pswd_MinLength_i;<>Fnd_Pswd_MaxLength_i;<>Fnd_Pswd_MinLengthLimit_i;<>Fnd_Pswd_MaxLengthLimit_i)
	C_BOOLEAN:C305(<>Fnd_Pswd_UseUppercase_b;<>Fnd_Pswd_UseLowercase_b;<>Fnd_Pswd_UseNumbers_b;<>Fnd_Pswd_UseSymbols_b)
	C_BOOLEAN:C305(<>Fnd_Pswd_UseCustom_b;<>Fnd_Pswd_UseExcluded_b)
	
	ARRAY INTEGER:C220(<>Fnd_Pswd_MinLengthPopup_ai;0)
	ARRAY INTEGER:C220(<>Fnd_Pswd_MaxLengthPopup_ai;0)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_POINTER:C301(Fnd_Pswd_BulletEntry ;$1;$2)
	
	C_TEXT:C284(Fnd_Pswd_CustomCharacters ;$0;$1)
	
	C_TEXT:C284(Fnd_Pswd_ExcludeCharacters ;$0;$1)
	
	C_TEXT:C284(Fnd_Pswd_GetValidCharacters ;$0)
	
	C_TEXT:C284(Fnd_Pswd_GeneratePassword ;$0)
	
	C_TEXT:C284(Fnd_Pswd_GeneratorDialog ;$0)
	
	C_TEXT:C284(Fnd_Pswd_Info ;$0;$1)
	
	C_LONGINT:C283(Fnd_Pswd_MaxLength ;$0;$1;$2)
	
	C_LONGINT:C283(Fnd_Pswd_MinLength ;$0;$1;$2)
	
	C_BOOLEAN:C305(Fnd_Pswd_UseLowercase ;$0;$1)
	
	C_BOOLEAN:C305(Fnd_Pswd_UseNumbers ;$0;$1)
	
	C_BOOLEAN:C305(Fnd_Pswd_UseSymbols ;$0;$1)
	
	C_BOOLEAN:C305(Fnd_Pswd_UseUppercase ;$0;$1)
End if 