//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Sort

  // Compiler variables related to the Foundation Sort routines.
  // Called by Fnd_Sort_Init.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 16, 2003
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Sort_Initialized_b)
If (Not:C34(<>Fnd_Sort_Initialized_b))
	
End if 


C_BOOLEAN:C305(Fnd_Sort_Initialized_b)
If (Not:C34(Fnd_Sort_Initialized_b))
	C_POINTER:C301(Fnd_Sort_Table_ptr;Fnd_Sort_Field_ptr;Fnd_Sort_SelectedField_ptr)
	
	C_TEXT:C284(Fnd_Sort_Label_t)
	C_TEXT:C284(Fnd_Sort_TableName_t)
	
	C_LONGINT:C283(Fnd_Sort_AscendingButton_i;Fnd_Sort_DescendingButton_i)
	C_LONGINT:C283(Fnd_Sort_OKButton_i;Fnd_Sort_CancelButton_i;Fnd_Sort_OffscreenButton_i)
	C_LONGINT:C283(Fnd_Sort_Direction_i)
	
	ARRAY TEXT:C222(Fnd_Sort_FieldNames_at;0)
	ARRAY POINTER:C280(Fnd_Sort_Fields_aptr;0)
	ARRAY INTEGER:C220(Fnd_Sort_FieldTypes_ai;0)
	ARRAY TEXT:C222(Fnd_Sort_MethodNames_at;0)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_POINTER:C301(Fnd_Sort_AddField ;$1)
	C_LONGINT:C283(Fnd_Sort_AddField ;$2)
	
	C_LONGINT:C283(Fnd_Sort_AddItem ;$1;$5)
	C_TEXT:C284(Fnd_Sort_AddItem ;$2;$3)
	C_POINTER:C301(Fnd_Sort_AddItem ;$4)
	
	C_LONGINT:C283(Fnd_Sort_AddSeparator ;$1)
	
	C_POINTER:C301(Fnd_Sort_AddTable ;$1)
	C_LONGINT:C283(Fnd_Sort_AddTable ;$2)
	
	C_LONGINT:C283(Fnd_Sort_Direction ;$0;$1)
	
	C_POINTER:C301(Fnd_Sort_Display ;$1)
	
	C_TEXT:C284(Fnd_Sort_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Sort_PrefsGet ;$0;$1)
	
	C_TEXT:C284(Fnd_Sort_PrefsPut ;$1;$2)
	
	C_POINTER:C301(Fnd_Sort_SelectedField ;$0;$1)
End if 