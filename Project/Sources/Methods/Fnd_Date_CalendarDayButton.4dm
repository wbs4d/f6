//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_CalendarDayButton (->form object)

  // The object method for the day buttons.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : The form object

  // Returns: Nothing

  // Based on Tom Dillon's Date Entry component.
  // Modified by Dave Batton on Mar 26, 2005
  // ----------------------------------------------------

C_POINTER:C301($1;$object_ptr)

$object_ptr:=$1

Case of 
	: (Form event:C388=On Clicked:K2:4)
		Fnd_Date_CalendarSetDate ("calendar";String:C10($object_ptr->))
		If ($object_ptr->#0)
			SET TIMER:C645(10)  // This allows the highlight to draw before the window closes. Just a better visual effect.
		End if 
End case 