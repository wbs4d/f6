//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Gen_LaunchAsNewProcess (method name; process name) --> Boolean


// Launches the specified process as a new process.  If the process is already

//   running, it's just brought to the front.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The method name to launch

//   $2 : Text : The name to give the new process


// Returns:

//   $0 : Boolean : True if we're in a new process


// Created by Dave Batton on Nov 19, 2003

// ----------------------------------------------------


C_BOOLEAN:C305($0;$go_b)
C_TEXT:C284($1;$2;$methodName_t;$newProcessName_t;$currentProcessName_t)
C_LONGINT:C283($processState_i;$processTime_i;$processNumber_i)

$methodName_t:=$1
$newProcessName_t:=$2

Fnd_Gen_Init

PROCESS PROPERTIES:C336(Current process:C322;$currentProcessName_t;$processState_i;$processTime_i)

Case of 
	: (($currentProcessName_t=$newProcessName_t) & (Not:C34(Fnd_Gen_ProcessLaunched_b)))
		$go_b:=True:C214
		Fnd_Gen_ProcessLaunched_b:=True:C214
		
	: (Process number:C372($newProcessName_t)>0)
		BRING TO FRONT:C326(Process number:C372($newProcessName_t))
		$go_b:=False:C215
		
	: ($newProcessName_t#$currentProcessName_t)  // This is not in its own process
		
		Fnd_Gen_ProcessLaunched_b:=False:C215
		$processNumber_i:=New process:C317($methodName_t;Fnd_Gen_DefaultStackSize;$newProcessName_t)
		$go_b:=False:C215
		
	Else 
		TRACE:C157
End case 

$0:=$go_b