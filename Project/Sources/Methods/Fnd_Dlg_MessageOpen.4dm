//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_MessageOpen

  // Opens the message window.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 25, 2003
  // Modified by Dave Batton on Dec 26, 2004
  //   Now sets <>Fnd_Dlg_ProgAborted_b to False before launching the new process.
  // Modified by Dave Batton on Jan 15, 2005
  //   Removed the window title related variables.  Now passes the window title as a
  //      parameter.  Gets the window title from the Fnd_Wnd component.
  // ----------------------------------------------------

C_LONGINT:C283($processID_i)
C_TEXT:C284($windowTitle_t)

Fnd_Dlg_Init 

  // Since we'll open the window in a new process, we need a way to get the settings to the new process.
  // We could pass them as parameters, but this works just as well.
<>Fnd_Dlg_MessageText_t:=Fnd_Dlg_Text1_t
<>Fnd_Dlg_CancelButton_t:=Fnd_Dlg_Button1_t
<>Fnd_Dlg_CanBeCancelled_b:=Fnd_Dlg_CanBeCancelled_b
<>Fnd_Dlg_ProgAborted_b:=False:C215  // DB041226 - Had to add this in case the current process checked it before the dialog opened.

$windowTitle_t:=Fnd_Wnd_Info ("title")
Fnd_Wnd_Title ("")  // Clear the title so it's not used elsewhere.

$processID_i:=New process:C317("Fnd_Dlg_MessageOpen2";32*1024;<>Fnd_Dlg_MessageProcessName_t;$windowTitle_t;*)

Fnd_Dlg_MessageUpdate (<>Fnd_Dlg_ProgFrom_i)

  // If the process was already running, call it so it updates its contents.
POST OUTSIDE CALL:C329($processID_i)
IDLE:C311  // Give the dialog an opportunity to update.
