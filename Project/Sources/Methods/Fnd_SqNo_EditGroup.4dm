//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_EditGroup (record ID)

  // Displays a dialog for editing the specified sequence number group.

  // Access: Private

  // Parameters: 
  //   $1 : Longint : A sequence number ID

  // Returns: Nothing

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Jan 15, 2005
  //   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition when displaying alerts and confirms.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

C_LONGINT:C283($1;$id_i;$width_i;$height_i)

$id_i:=$1

Fnd_SqNo_Init 

OK:=1  // In case we don't display the Confirm dialog.
If (Count users:C342>1)  // If other users are online…
	Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050115 - Modified  ` DB050528 - Changed from Fnd_Wnd_SetPosition.
	Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
	Fnd_Dlg_SetButtons (Fnd_Gen_GetString ("Fnd_SqNo";"ContinueButton");Fnd_Gen_GetString ("Fnd_SqNo";"CancelButton"))
	Fnd_Dlg_Confirm (Fnd_Gen_GetString ("Fnd_SqNo";"EditWithUsersOnline"))
End if 

If (OK=1)
	READ WRITE:C146(<>Fnd_SqNo_Table_ptr->)
	QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_IDFld_ptr->=$id_i)
	LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)  // So we lock it to other users.
	
	If (Not:C34(Locked:C147(<>Fnd_SqNo_Table_ptr->)))
		Fnd_Wnd_Title (Fnd_Gen_GetString ("Fnd_SqNo";"GroupWindowTitle"))  // DB050528 - Changed from Fnd_Wnd_SetTitle.
		Fnd_Wnd_Type (Movable dialog box:K34:7)  // DB050528 - Changed from Fnd_Wnd_SetType.
		FORM GET PROPERTIES:C674("Fnd_SqNo_EditGroup";$width_i;$height_i)
		Fnd_Wnd_OpenWindow ($width_i;$height_i)
		DIALOG:C40("Fnd_SqNo_EditGroup")
		CLOSE WINDOW:C154
		
		If (OK=1)
			  // Convert the list of resuse numbers to an array of longints.      
			VARIABLE TO BLOB:C532(<>Fnd_SqNo_ReuseNumbersArray_ai;<>Fnd_SqNo_RecycleBinFld_ptr->)
			SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)
			  // Don't unload the record here. We need some info from it outside of this method.
		End if 
		
		
	Else   // The record is locked.
		Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050115 - Modified  ` DB050528 - Changed from Fnd_Wnd_SetPosition.
		Fnd_Dlg_SetIcon (Fnd_Dlg_StopIcon)
		Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"RecordInUse"))
	End if 
End if 