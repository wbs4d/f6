//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Button_Label (button name{; label}) --> Text

// Returns the label for the button with the specified name.
// If a label is passed, this method first sets the button label.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The button name
//   $2 : Text : The new button label (optional)

// Returns: 
//   $0 : Text : The button label

// Created by Mark Mitchenall on 11/2/02
// Toolbar Component - © mitchenall.com 2002
// ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$objectName_t;$label_t)
C_LONGINT:C283($objectNumber_i)

$objectName_t:=$1
If (Count parameters:C259>=2)
	$label_t:=$2
End if 

Fnd_Tlbr_Init

$objectNumber_i:=Fnd_Tlbr_Object_GetNumber($objectName_t)

If ($objectNumber_i>0)
	
	If (Count parameters:C259>=2)
		// Only change the button text and invalidate the toolbar if the button text
		//   has actually changed.
		If (Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}#$label_t)
			Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}:=$label_t
			Fnd_Tlbr_ButtonsAreValid_b:=False:C215
		End if 
	End if 
	
	$label_t:=Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684;"Button name not found: "+$objectName_t)
End if 

$0:=$label_t