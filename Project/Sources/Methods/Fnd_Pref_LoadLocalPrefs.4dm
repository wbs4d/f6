//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_LoadLocalPrefs

// Loads the preferences stored in the local external preference file into the preference arrays.
// Called from Fnd_Pref_LoadPrefs, which handles the semaphore.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 6, 2006
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Pref_Initialized_b)
C_TEXT:C284($element_t; $path_t; $xmlRef_t; $oldErrorHandler_t; $errorMsg_t)
C_LONGINT:C283(Error; $oldErrorValue_i)
C_BLOB:C604($xml_blob)

// Don't call Fnd_Pref_Init here.  This method gets called by it. 
// But make sure it has been called.
If (<>Fnd_Pref_Initialized_b)
	
	If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
		$path_t:=Fnd_Pref_Path
		
		If (Test path name:C476($path_t)=Is a document:K24:1)
			DOCUMENT TO BLOB:C525($path_t; $xml_blob)
			
			If (OK=1)
				// Install a do-nothing error handler so 4D doesn't display its
				//   error message if there's a problem parsing the XML.
				$oldErrorValue_i:=Error  // DB050130
				$oldErrorHandler_t:=Method called on error:C704
				ON ERR CALL:C155("Fnd_Pref_ErrorHandler")
				
				$element_t:=DOM Parse XML variable:C720($xml_blob)
				Fnd_Pref_XMLElement_t:=$element_t  // Set this for the Fnd_Pref_ErrorHandler method.
				
				If ((OK=1) & (Error=0))
					Fnd_Pref_XmlParse($element_t)
				End if 
				DOM CLOSE XML:C722($element_t)
				
				// Restore the previous error value and error handler.
				ON ERR CALL:C155($oldErrorHandler_t)
				Error:=$oldErrorValue_i  // DB050130
			End if 
		End if 
		
		CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
	End if 
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "This method should not be called directly.")
End if 