//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_RegG

  // Compiler variables related to the Foundation registration generation routines.
  // Called by Fnd_Dlg_RegG.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 16, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_RegG_Initialized_b)
If (Not:C34(<>Fnd_RegG_Initialized_b))  // So we only do this once.
	C_TEXT:C284(<>Fnd_RegG_UnlockCodeFormat_t;<>Fnd_RegG_DefaultFeatureCode_t)
End if 

  // Parameters
If (False:C215)
	C_TEXT:C284(Fnd_RegG_GenerateUnlockCode ;$0;$1;$2;$3;$4;$5)
	
	C_TEXT:C284(Fnd_RegG_Format ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_RegG_Info ;$0;$1)
End if 