//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_InputFormMethod

// Standard form method for input forms. 
// Handles the window title and close box on an input form.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Jun 16, 2004
//   Localized the Save can Cancel buttons.
//   Now tests to see if the record is locked, and if it is an alert is displayed.
// Modified by Dave Batton on Aug 2, 2004
//   Changed the Fnd_Loc_ButtonText calls to Fnd_Gen_ButtonText calls, since the
//   Fnd_Loc_ButtonText method no longer exists.
// Modified by Dave Batton on Sep 9, 2004
//   Added code to place the navigation buttons.
//   Now we always set the window title in the On Load phase if we're in single-window mode.
// Modified by Dave Batton on Sep 20, 2004
//   Broke the resize code into another method so it can also be called in the On Load event for 4D 2004.
// Modified by Dave Batton on Jul 22, 2005
//   Added a call to the new Fnd_aa_IO_InputFormMethod hook.
// Modified by Dave Batton on Nov 6, 2007
//   When checking to see if the record is locked, 'Current form table' is now used instead of
//   Fnd_Gen_CurrentTable. This allows this form method to work properly when drilling down.
// [1] Modified by: John Craig (12/13/09) - added process name to locked alert
// Modified by: Walt Nelson (6/22/11) - added Ken Spence and Guy Algot's changes for On Close action
// ----------------------------------------------------

C_LONGINT:C283($processNumber_i)
C_TEXT:C284($user_t; $machine_t; $processName_t)
C_BOOLEAN:C305($optionKeyDown_b; $allow_b)
C_POINTER:C301($table_ptr)

Case of 
	: (Form event code:C388=On Resize:K2:27)
		Fnd_IO_InputFormResize
		
		
	: (Current process:C322=1)  // Don't do the stuff below in the User process.
		
		
	: (Form event code:C388=On Load:K2:1)
		Fnd_IO_Init  // ### DB050407 - I think this can be removed. But I'll do it later.
		Case of 
			: (Get window title:C450="")  // If no window title has been set yet, set one.
				SET WINDOW TITLE:C213(Fnd_IO_GetWindowTitle)
			: (Get window title:C450="@ ")  // DB040922 - If we set the window title last time, update it.
				SET WINDOW TITLE:C213(Fnd_IO_GetWindowTitle)
				Fnd_Menu_Window_Add  // And update the Window menu.
		End case 
		
		OBJECT SET VISIBLE:C603(*; "Fnd_IO_HideThis@"; False:C215)  // Hide our button hiding rect.
		
		Fnd_IO_UpdateToolbar
		Fnd_IO_InputDrawButtons  // Label and size the inherited buttons.
		Fnd_IO_InputFormResize  // Move the inherited buttons into place.
		
		If (Locked:C147(Current form table:C627->))
			$table_ptr:=Current form table:C627  // Required temp variable for 4D v11.2.
			LOCKED BY:C353($table_ptr->; $processNumber_i; $user_t; $machine_t; $processName_t)  // DB071106
			If (Fnd_IO_ShowLockedMessage_b)
				If ($user_t="")  // It's locked on this machine.
					Fnd_Dlg_Alert(Fnd_Gen_GetString("Fnd_IO"; "RecordLockedLocally")+"\r\rProcess: "+$processName_t)  //[1]
				Else   // It's locked by another user.
					Fnd_Dlg_Alert(Fnd_Gen_GetString("Fnd_IO"; "RecordLockedRemotely"; $user_t)+"\r\rProcess: "+$processName_t)  //[1]
				End if 
			End if 
			OBJECT SET ENABLED:C1123(*; "Fnd_IO_OKButton_i"; False:C215)
		End if 
		
		
	: (Form event code:C388=On Outside Call:K2:11)
		If ((Fnd_Wnd_CloseNow) | (Fnd_Gen_QuitNow))
			Fnd_IO_InputFormClose
		End if 
		
		
	: (Form event code:C388=On Activate:K2:9)
		Fnd_Gen_MenuBar
		
		
		
	: (Form event code:C388=On Close Box:K2:21)
		$optionKeyDown_b:=Macintosh option down:C545
		// Modified by: Walt Nelson (6/22/11)
		// Original code
		//$allow_b:=True
		//If (Fnd_DoesMethodExist ("Fnd_Hook_IO_InputFormButton")=1)
		//EXECUTE METHOD("Fnd_Hook_IO_InputFormButton";$allow_b;"close")
		//End if 
		//If ($allow_b)
		If (($optionKeyDown_b) & (Fnd_IO_InputFormClose))
			Fnd_Wnd_CloseAllWindows
		End if 
		//End if 
End case 


Fnd_Tlbr_FormMethod

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_InputFormMethod")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_IO_InputFormMethod"; *)
End if 