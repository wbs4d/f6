//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_AddField (->field{; width{; header{; alignment{; format}}}})

// Allows the developer to specify the columns to display in the output list.
//   Call multiple times to display multiple columns.
// Pass a negative width to auto-size the column. The number is the minimum size.
// Pass a column width of 0 to use the default size.

// Access Type: Shared

// Parameters: Original - via a pointer
//   $1 : Pointer : A field pointer
//   $2 : Longint : The width for the column (optional)
//   $3 : Text : The column header (optional)
//   $4 : Longint : The column alignment (optional)
//   $5 : Text : The column display format (optional)
//   $6 : Boolean : Visible column (optional)

// Parameters: Alternative Two - via an object
//   $1.fo_field : Text : The field name (or formula showing relations to field)
//   $1.fo_format : Text : Formatting to be applied (optional)
//   $1.fo_hAlign : Longint : Align default | Align left | Align right | Align center (optional)
//   $1.fo_maxWidth : Longint : Maximum width of column (optional)
//   $1.fo_minWidth : Longint : Minimum width of column (optional)
//   $1.fo_style : Longint : Font style (optional)
//   $1.fo_title : Text : Header title (optional)
//   $1.fo_type : Longint : Field type (optional)
//   $1.fo_visible : Boolean : Visible column (optional)
//   $1.fo_width : Longint : The width for the column (optional)

// Parameters: Alternative Three - via text
//   $1 : Text : The formula specifying a field or caluclation
//   $2 : Longint : The width for the column (optional)
//   $3 : Text : The column header (optional)
//   $4 : Longint : The column alignment (optional)
//   $5 : Text : The column display format (optional)
//   $6 : Boolean : Visible column (optional)

// Returns: Nothing

// Created by Dave Batton on Feb 8, 2005
// Mod by Wayne Stewart, (2022-04-20) - Rewritten 
// ----------------------------------------------------

var $1 : Variant
var $field_ptr : Pointer
var $2; $4; $width_i; $estimatedWidth_i; $alignment_i; $fieldType_i; $fieldLength_i; \
$numberOfColumns_i; $IDfieldType_i; $numberOfParameters_i; \
$maxWidth_i; $minWidth_i; $style_i : Integer
var $3; $header_t; $fieldName_t; $5; $format_t; $defaultFormat_t; $formula_t : Text
var $columnDefinition_o : Object
var $6; $visible_b : Boolean

Fnd_Out_Init

$numberOfParameters_i:=Count parameters:C259

// Verify these are set up
If (Fnd.out.lbox=Null:C1517)
	Fnd.out.lbox:=New object:C1471("columns"; New collection:C1472)  // add the object and collection
End if 

If (Fnd.out.currentTable=0)
	Fnd.out.currentTable:=Table:C252(Fnd_Gen_CurrentTable)
	
End if 

If (Fnd.out.recordIDField=0)
	Fnd.out.recordIDField:=Fnd_Out_getPrimaryKey(Fnd.out.currentTable)
End if 

$numberOfColumns_i:=Fnd.out.lbox.columns.length  // The number of columns already in the listbox

// Set the defaults
$alignment_i:=Align left:K42:2  // Default doesn't work like I want, because it centers the header labels.

$maxWidth_i:=32000
$minWidth_i:=10
$style_i:=Plain:K14:1

If ($numberOfParameters_i>=2)
	If ($2#0)
		$width_i:=$2
	End if 
	If ($numberOfParameters_i>=3)
		If (Length:C16($3)#0)
			$header_t:=$3
		End if 
		If ($numberOfParameters_i>=4)
			$alignment_i:=$4
			If ($numberOfParameters_i>=5)
				$format_t:=$5
				If ($numberOfParameters_i>=6)
					$visible_b:=$6
				End if 
			End if 
		End if 
	End if 
End if 


Case of 
	: (Value type:C1509($1)=Is pointer:K8:14)
		$field_ptr:=$1
		$formula_t:=Field name:C257($field_ptr)
		$fieldType_i:=Type:C295($field_ptr->)
		If (Length:C16($header_t)=0)
			$header_t:=Fnd_VS_FieldName($field_ptr)
		End if 
		
	: (Value type:C1509($1)=Is object:K8:27)
		$formula_t:=$1[fo_field]  // This is the only compulsory parameter
		$fieldType_i:=Choose:C955($1[fo_type]#Null:C1517; $1[fo_type]; Fnd_Out_fieldType($formula_t))
		$defaultFormat_t:=Choose:C955($1[fo_format]#Null:C1517; $1[fo_format]; "")
		$alignment_i:=Choose:C955($1[fo_hAlign]#Null:C1517; $1[fo_hAlign]; Align default:K42:1)
		
		$maxWidth_i:=Choose:C955($1[fo_maxWidth]#Null:C1517; $1[fo_maxWidth]; 0)
		$minWidth_i:=Choose:C955($1[fo_minWidth]#Null:C1517; $1[fo_minWidth]; 10)
		$width_i:=Choose:C955($1[fo_width]#Null:C1517; $1[fo_width]; 0)
		
		$style_i:=Choose:C955($1[fo_style]#Null:C1517; $1[fo_style]; Plain:K14:1)
		$header_t:=Choose:C955($1[fo_title]#Null:C1517; $1[fo_title]; Fnd_Out_fieldName($formula_t))
		$visible_b:=Choose:C955($1[fo_visible]#Null:C1517; $1[fo_visible]; $visible_b)
		
	: (Value type:C1509($1)=Is text:K8:3)
		$formula_t:=$1  // This is the only compulsory parameter
		$fieldType_i:=Fnd_Out_fieldType($formula_t)
		If (Length:C16($header_t)=0)
			$header_t:=Fnd_Out_fieldName($formula_t)
		End if 
		
End case 


Case of 
	: ($fieldType_i=Is alpha field:K8:1) | ($fieldType_i=Is text:K8:3)
		$estimatedWidth_i:=100
		
	: (($fieldType_i=Is integer:K8:5) | ($fieldType_i=Is longint:K8:6))
		$estimatedWidth_i:=65
		$alignment_i:=Align right:K42:4
		
	: ($fieldType_i=Is real:K8:4)
		$estimatedWidth_i:=65
		$alignment_i:=Align right:K42:4
		$defaultFormat_t:="###,###,###,##0.00"
		
	: ($fieldType_i=Is boolean:K8:9)
		$estimatedWidth_i:=19
		
	: ($fieldType_i=Is date:K8:7)
		$defaultFormat_t:=String:C10(System date short:K1:1)
		$estimatedWidth_i:=65
		
	: ($fieldType_i=Is time:K8:8)
		$defaultFormat_t:=String:C10(HH MM AM PM:K7:5)
		$estimatedWidth_i:=65
		
	: ($fieldType_i=Is picture:K8:10)
		$estimatedWidth_i:=64
		$defaultFormat_t:=String:C10(Scaled to fit proportional:K6:5)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Unable to handle field type "+String:C10($fieldType_i)+".")
End case 

$width_i:=Choose:C955(($width_i=0); $estimatedWidth_i; $width_i)
$format_t:=Choose:C955(Length:C16($format_t)=0; $defaultFormat_t; $format_t)

If ($numberOfColumns_i=0)  // No ID field yet in Column 0
	
	$columnDefinition_o:=New object:C1471
	$columnDefinition_o[fo_column]:=$numberOfColumns_i
	$field_ptr:=Field:C253(Fnd.out.currentTable; Fnd.out.recordIDField)
	$columnDefinition_o.fo_field:=Field name:C257($field_ptr)
	$IDfieldType_i:=Fnd_Out_fieldType($field_ptr)
	$columnDefinition_o.fo_hAlign:=Align left:K42:2
	$columnDefinition_o.fo_maxWidth:=32000
	$columnDefinition_o.fo_minWidth:=50
	$columnDefinition_o.fo_title:=$columnDefinition_o.fo_field
	$columnDefinition_o.fo_type:=$IDfieldType_i
	$columnDefinition_o.fo_visible:=False:C215
	$columnDefinition_o.fo_width:=100
	
	Fnd.out.lbox.columns.push($columnDefinition_o)
	
	$numberOfColumns_i:=Fnd.out.lbox.columns.length  // The number of columns already in the listbox
End if 

$visible_b:=($numberOfColumns_i>0)  // The first column should be invisible, all others visible


$columnDefinition_o:=New object:C1471
// Note the constant is exactly the same as the string it represents
$columnDefinition_o[fo_column]:=$numberOfColumns_i
$columnDefinition_o.fo_field:=$formula_t

If ($fieldType_i=Is time:K8:8) | ($fieldType_i=Is date:K8:7) | ($fieldType_i=Is picture:K8:10)
	$columnDefinition_o.fo_format:=Num:C11($format_t)
Else 
	$columnDefinition_o.fo_format:=$format_t
End if 

$columnDefinition_o.fo_hAlign:=$alignment_i
$columnDefinition_o.fo_maxWidth:=32000
$columnDefinition_o.fo_minWidth:=$minWidth_i
$columnDefinition_o.fo_style:=$style_i
$columnDefinition_o.fo_title:=$header_t
$columnDefinition_o.fo_type:=$fieldType_i
$columnDefinition_o.fo_visible:=$visible_b
$columnDefinition_o.fo_width:=$width_i

Fnd.out.lbox.columns.push($columnDefinition_o)


// original Array code
/*
If (False)
$header_t:=Fnd_VS_FieldName($field_ptr)
$alignment_i:=Align left  // Default doesn't work like I want, because it centers the header labels.
$format_t:=""

$fieldType_i:=Type($field_ptr->)


Case of 
: (Fnd_Out_TextColumns_i>=<>Fnd_Out_MacColumns_i)
TRACE  // No more arrays declared. We can't add the column.

: (($fieldType_i=Is alpha field) | ($fieldType_i=Is text))
Fnd_Out_TextColumns_i:=Fnd_Out_TextColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_TextColumns_i)+"_at")
$width_i:=100

: (($fieldType_i=Is real) | ($fieldType_i=Is integer) | ($fieldType_i=Is longint))
Fnd_Out_NumColumns_i:=Fnd_Out_NumColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_NumColumns_i)+"_ar")
$width_i:=65
$alignment_i:=Align right

: ($fieldType_i=Is boolean)
Fnd_Out_NumColumns_i:=Fnd_Out_NumColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_NumColumns_i)+"_ab")
$width_i:=19

: ($fieldType_i=Is date)
Fnd_Out_DateColumns_i:=Fnd_Out_DateColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_DateColumns_i)+"_ad")
$format_t:=Char(Short)
$width_i:=65

: ($fieldType_i=Is time)  // No, this doesn't work yet.  :-(
Fnd_Out_DateColumns_i:=Fnd_Out_DateColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_DateColumns_i)+"_ar")
$format_t:=Char(HH MM AM PM)
$width_i:=65

: ($fieldType_i=Is picture)
Fnd_Out_PictureColumns_i:=Fnd_Out_PictureColumns_i+1
$columnArray_ptr:=Get pointer("Fnd_Out_Column"+String(Fnd_Out_PictureColumns_i)+"_apic")
$width_i:=20
$format_t:=Char(Truncated centered)
$header_t:=""

Else 
Fnd_Gen_BugAlert(Current method name;"Unable to handle field type "+String($fieldType_i)+".")
End case 

If (Count parameters>=2)
If ($2#0)
$width_i:=$2
End if 
If (Count parameters>=3)
If ($3#"")
$header_t:=$3
End if 
If (Count parameters>=4)
$alignment_i:=$4
If (Count parameters>=5)
$format_t:=$5
End if 
End if 
End if 
End if 

APPEND TO ARRAY(Fnd_Out_ListColumnFields_aptr;$field_ptr)
APPEND TO ARRAY(Fnd_Out_ListColumnArrays_aptr;$columnArray_ptr)
APPEND TO ARRAY(Fnd_Out_ListColumnWidths_ai;$width_i)
APPEND TO ARRAY(Fnd_Out_ListColumnAlignments_ai;$alignment_i)
APPEND TO ARRAY(Fnd_Out_ListColumnFormats_at;$format_t)
APPEND TO ARRAY(Fnd_Out_ListColumnLabels_at;$header_t)
End if 
*/