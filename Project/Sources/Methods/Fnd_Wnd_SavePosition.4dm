//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_SavePosition ({name{; window ref}})

// Saves the frontmost window of the current process in the user's prefernces
//   file (if the Foundation Preferences component is available).
// This is basically the same as calling Fnd_Pref_SetWindow, but you don't have
//   to check first to see if the Preferences component is available.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item (optional)
//   $2 : Longint : The window to save (optional)

// Returns: Nothing

// Created by Dave Batton on May 20, 2004
// Mod by Wayne Stewart, 2023-07-26: remove check to see if Prefs component is available 
// ----------------------------------------------------

C_TEXT:C284($1; $command_t)
C_LONGINT:C283($2)

Case of 
	: (Count parameters:C259=0)
		Fnd_Pref_SetWindow
		
	: (Count parameters:C259=1)
		Fnd_Pref_SetWindow($1)
		
	: (Count parameters:C259=2)
		Fnd_Pref_SetWindow($1; $2)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "An incorrect number of parameters was passed.")
End case 
