//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_Get ({->blob}) --> Text

  // Gets the next message queued up for the current process.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : For receiving an optional blob (optional)

  // Returns: 
  //   $0 : Text : The message

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$message_t)
C_POINTER:C301($1)
C_LONGINT:C283($element_i)

Fnd_Msg_Init 

$message_t:=""  // In case no message is found.

  // Make sure no other process is messing with the message arrays.
If (Not:C34(Semaphore:C143(<>Fnd_Msg_SemaphoreName_t;300)))  // Wait up to 5 seconds if the semaphore already exists
	
	$element_i:=1
	Repeat 
		$element_i:=Find in array:C230(<>Fnd_Msg_MsgActiveFlags_ab;True:C214;$element_i)
		
		If ($element_i>0)
			If (<>Fnd_Msg_MsgToProcNums_ai{$element_i}=Current process:C322)
				  // This message is for us.
				$message_t:=<>Fnd_Msg_MsgTexts_at{$element_i}
				<>Fnd_Msg_MsgActiveFlags_ab{$element_i}:=False:C215  // Free up this message slot.
				If (Count parameters:C259>=1)  // DB030805 - If a pointer to a blob was passed, return the blob (if any).
					Fnd_Msg_PictureToBlob (-><>Fnd_Msg_MsgBlobs_apic{$element_i};$1)
				End if 
				<>Fnd_Msg_MsgBlobs_apic{$element_i}:=<>Fnd_Msg_MsgBlobs_apic{$element_i}*0  // DB030805 - Clear the blob to free up the RAM.
				$element_i:=-1  // So we don't go through the loop again.
			Else 
				$element_i:=$element_i+1  // Try searching again at the next element.
			End if 
		End if 
	Until ($element_i<=0)
	
	CLEAR SEMAPHORE:C144(<>Fnd_Msg_SemaphoreName_t)
	
	  // Update the message monitor dialog (unless this was called from that dialog).
	  // If (Current process#Process number(◊Fnd_kMsgMonProcessName))
	  //   If (Application type#4D Server )
	  //     CALL PROCESS(Process number(◊Fnd_kMsgMonProcessName))
	  //   End if 
	  // End if 
	
Else   // The semaphore never cleared.
	Fnd_Gen_BugAlert ("Fnd_Msg_Get";"The semaphore was not properly cleared.")
End if 

$0:=$message_t