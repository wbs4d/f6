//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Message2 (message text{; window title})

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.

  // Parameters: 
  //   $1 : Text : The message to display
  //   $2 : Text : The window title (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 26, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$2)

Fnd_Cmpt_Init 

Fnd_Dlg_SetText ($1)

If (Count parameters:C259>=2)
	Fnd_Wnd_Title ($2)
End if 

Fnd_Dlg_MessageOpen 