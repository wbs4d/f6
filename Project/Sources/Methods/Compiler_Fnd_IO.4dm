//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_IO

  // Compiler directives for the Fnd_IO_InputForm form objects.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 30, 2007
  //[1] Added by: John Craig (8/24/09)
  //[2] Added by: John Craig (12/14/09)
  // ----------------------------------------------------

C_LONGINT:C283(Fnd_IO_OKButton_i;Fnd_IO_CancelButton_i)
C_LONGINT:C283(Fnd_IO_FirstButton_i;Fnd_IO_PrevButton_i;Fnd_IO_NextButton_i;Fnd_IO_LastButton_i)
C_LONGINT:C283(Fnd_IO_CloseButton_i;Fnd_IO_EscButton_i)

C_TEXT:C284(Fnd_IO_HighlightedRecords_t)  //[1] Added by: John Craig (8/24/09)
C_TEXT:C284(Fnd_IO_RecordsInSelection_t)  //[1] Added by: John Craig (8/24/09)
C_TEXT:C284(Fnd_IO_RecordsInTable_t)  //[1] Added by: John Craig (8/24/09)
C_BOOLEAN:C305(Fnd_IO_ShowLockedMessage_b)  //[2] Added by: John Craig (12/14/09)

C_TEXT:C284(Fnd_IO_Buffer)  // Added by:  Mike Erickson(20110107)