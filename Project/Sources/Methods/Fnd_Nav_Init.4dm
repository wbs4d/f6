//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_Init

// Initializes both the process and interprocess variables used by the Fnd_Nav routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 5, 2003
// Modified by Dave Batton on Feb 4, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
// Modified by Dave Batton on May 22, 2004
//   Updated the version number to 4.0.3.
//   Added initialization of the new <>Fnd_Nav_NeedsRedraw_b,
//   <>Fnd_Nav_Semaphore_t, and <>Fnd_Nav_MaxButtons_i variables.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Nav_Info method.
// ----------------------------------------------------

Fnd_Init

C_BOOLEAN:C305(<>Fnd_Nav_Initialized_b)

If (Storage:C1525.Fnd.Nav=Null:C1517)
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Nav:=New shared object:C1526("WindowReference"; -1; \
			"ProcessName"; "$Fnd_Nav: Navigation Palette"; \
			"Semaphore"; "$Fnd_Nav_Semaphore")
	End use 
End if 



If (Not:C34(<>Fnd_Nav_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Nav
	
	ARRAY TEXT:C222(<>Fnd_Nav_Labels_at; 0)
	ARRAY POINTER:C280(<>Fnd_Nav_TablePtrs_aptr; 0)
	ARRAY TEXT:C222(<>Fnd_Nav_Methods_at; 0)
	ARRAY LONGINT:C221(<>Fnd_Nav_ForegroundColors_ai; 0)
	ARRAY LONGINT:C221(<>Fnd_Nav_BackgroundColors_ai; 0)
	
	<>Fnd_Nav_MaxButtons_i:=40  // The most buttons the palette form can handle.
	<>Fnd_Nav_NeedsRedraw_b:=False:C215  // Gets set to True when we need to update the palette.
	
	<>Fnd_Nav_Initialized_b:=True:C214
End if 


