//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_GetMany2 (group name; ->longint array)

  // Just like Fnd_SqNo_Get2, but grabs multiple numbers.
  // Determines how many numbers to get by the size of the array.
  // Assumes element 1 was filled elsewhere, and starts with element 2.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The group name
  //   $2 : Pointer : A longint array to receive the numbers

  // Returns: Nothing

  // Created by Dave Batton on Sep 24, 2007
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_POINTER:C301($2;$numbers_ptr)
C_LONGINT:C283($numbersNeeded_i;$numbersElement_i;$reuseElement_i;$giveUpTime_i;$seqNo_i)

$groupName_t:=$1
$numbers_ptr:=$2

$numbersNeeded_i:=Size of array:C274($numbers_ptr->)

Fnd_SqNo_LoadGroupRecord ($groupName_t)

If (Not:C34(Locked:C147(<>Fnd_SqNo_Table_ptr->)))
	$numbersElement_i:=1  // The next element we need to fill.
	
	  // First we'll try to reuse any out-of-sequence numbers from the reuse array.
	ARRAY LONGINT:C221($numbersToReuse_ai;0)
	BLOB TO VARIABLE:C533(<>Fnd_SqNo_RecycleBinFld_ptr->;$numbersToReuse_ai)
	
	If (Size of array:C274($numbersToReuse_ai)>0)  // If there are elements, we'll get some of the numbers from here.
		SORT ARRAY:C229($numbersToReuse_ai;<)  // A decending sort. We gotta loop backwards to delete elements.
		
		$reuseElement_i:=Size of array:C274($numbersToReuse_ai)
		While (($reuseElement_i>0) & ($numbersElement_i<=$numbersNeeded_i))
			$numbers_ptr->{$numbersElement_i}:=$numbersToReuse_ai{$reuseElement_i}
			$numbersElement_i:=$numbersElement_i+1
			DELETE FROM ARRAY:C228($numbersToReuse_ai;$reuseElement_i)
			$reuseElement_i:=$reuseElement_i-1
		End while 
		
		VARIABLE TO BLOB:C532($numbersToReuse_ai;<>Fnd_SqNo_RecycleBinFld_ptr->)  // ...and return the remaining elements to the blob.
	End if 
	
	  // Now we'll just generate the rest of the numbers from the next number value.
	If ($numbersElement_i<=$numbersNeeded_i)
		$seqNo_i:=<>Fnd_SqNo_NextNumberFld_ptr->  // This is where we store the next number to use.
		For ($numbersElement_i;$numbersElement_i;$numbersNeeded_i)
			$numbers_ptr->{$numbersElement_i}:=$seqNo_i
			$seqNo_i:=$seqNo_i+1
		End for 
		<>Fnd_SqNo_NextNumberFld_ptr->:=$seqNo_i
	End if 
	
	SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)  // Either way, we need to save the record.
	UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure other users can load the record.
End if 
