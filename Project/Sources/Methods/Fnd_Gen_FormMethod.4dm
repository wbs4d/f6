//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_FormMethod

// Standard form method for non-modal forms. 
// Handles the close box and quitting the application.
//  Responds to
//    On Activate
//    On Close Box
//    Fnd_Gen_QuitNow
//    Fnd_Wnd_CloseAllWindows

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 27, 2003
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable calls 
// ----------------------------------------------------


Fnd_Gen_CloseNow_b:=Fnd_Wnd_CloseNow

Case of 
	: ((Fnd_Gen_CloseNow_b) | (Fnd_Gen_QuitNow))
		CANCEL:C270
		
	: (Form event code:C388=On Activate:K2:9)
		Fnd_Gen_MenuBar
		
		
	: (Form event code:C388=On Close Box:K2:21)
		If (Macintosh option down:C545)
			Fnd_Wnd_CloseAllWindows
		End if 
		CANCEL:C270  // Close this window.
End case 
