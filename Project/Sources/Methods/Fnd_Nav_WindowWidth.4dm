//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Nav_WindowWidth --> Number

  // Returns the width to use for the navigation palette.

  // Access: Private

  // Parameters: Nothing

  // Returns: 
  //   $0 : Longint : The width to use

  // Created by Dave Batton on Jul 1, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0;$windowWidth_i;$numButtons_i;$buttonNumber_i;$fontSize_i;$fontStyle_i;$neededWidth_i)
C_TEXT:C284($fontName_t;$buttonLabel_t)

$windowWidth_i:=0  // You can set this to a positive number to be the minimum palette window width.

$numButtons_i:=Size of array:C274(<>Fnd_Nav_Labels_at)
If ($numButtons_i><>Fnd_Nav_MaxButtons_i)
	$numButtons_i:=<>Fnd_Nav_MaxButtons_i
End if 

Case of 
:(true)
		$fontName_t:=Storage:C1525.font.smallName  // Try to match what we've picked in the style sheet.
		$fontSize_i:=Storage:C1525.font.smallSize+1
		$fontStyle_i:=Storage:C1525.font.smallStyle

	: (Is Windows:C1573)
		$fontName_t:="Tahoma"  // Try to match what we've picked in the style sheet.
		$fontSize_i:=12
		$fontStyle_i:=Plain:K14:1
	Else   // Mac OS X
		$fontName_t:="Lucida Grande"  // Try to match what we've picked in the style sheet.
		$fontSize_i:=11
		$fontStyle_i:=Plain:K14:1
End case 

For ($buttonNumber_i;1;$numButtons_i)
	$buttonLabel_t:=<>Fnd_Nav_Labels_at{$buttonNumber_i}
	$neededWidth_i:=Fnd_Ext_TextWidth ($buttonLabel_t;$fontName_t;$fontSize_i;$fontStyle_i)+16  // 16 is the margin. 
	If ($neededWidth_i>$windowWidth_i)
		$windowWidth_i:=$neededWidth_i
	End if 
	((Get pointer:C304("<>Fnd_Nav_Label"+String:C10($buttonNumber_i)+"_t"))->):=$buttonLabel_t
End for 

$0:=$windowWidth_i

