//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_LoadRegistrationRecord ({feature code})

  // Searches for the registration record for the specified feature. If one is found,
  //   it is checked to make sure it hasn't been tampered with.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: Nothing

  // Created by Dave Batton on Apr 27, 2005
  // ----------------------------------------------------

C_TEXT:C284($1;$featureCode_t)

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

QUERY:C277(<>Fnd_Reg_Table_ptr->;<>Fnd_Reg_FeatureCodeFld_ptr->=$featureCode_t)

Case of 
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=0)
		  // No problem.
		
	: (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=1)
		Fnd_Reg_VerifyRecord   // Make sure it's good.
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"Too many records found.")
		ABORT:C156  // Bad thing!
End case 
