//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Tlbr_Button_Position (button name; ->left; ->top; ->right; ->bottom)


// Returns the coordinates of the specified button.


// Access Type: Shared


// Parameters: 

//   $1 : Text : The name of the button

//   $2 : Pointer : Longint to receive the left coordinate

//   $3 : Pointer : Longint to receive the top coordinate

//   $4 : Pointer : Longint to receive the right coordinate

//   $5 : Pointer : Longint to receive the bottom coordinate


// Returns: Nothing


// Created by Dave Batton on Mar 13, 2005

// ----------------------------------------------------


C_TEXT:C284($1;$buttonName_t)
C_POINTER:C301($2;$3;$4;$5;$left_ptr;$top_ptr;$right_ptr;$bottom_ptr)
C_LONGINT:C283($buttonNumber_i;$left_i;$top_i;$right_i;$bottom_i)

$buttonName_t:=$1
$left_ptr:=$2
$top_ptr:=$3
$right_ptr:=$4
$bottom_ptr:=$5

Fnd_Tlbr_Init

$buttonNumber_i:=Fnd_Tlbr_Object_GetNumber($buttonName_t)

If ($buttonNumber_i>0)
	OBJECT GET COORDINATES:C663(Fnd_Tlbr_PictureButtons_aptr{$buttonNumber_i}->;$left_i;$top_i;$right_i;$bottom_i)
	$left_ptr->:=$left_i
	$top_ptr->:=$top_i
	$right_ptr->:=$right_i
	$bottom_ptr->:=$bottom_i
	
Else 
	$left_ptr->:=0
	$top_ptr->:=0
	$right_ptr->:=0
	$bottom_ptr->:=0
End if 