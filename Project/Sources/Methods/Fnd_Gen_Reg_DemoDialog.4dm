//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_DemoDialog

  // Displays the demo message at startup.  Called as a new process
  //   from Fnd_Reg_Startup

  // Method Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Aug 3, 2003
  // ----------------------------------------------------

C_LONGINT:C283($width_i;$height_i)

Fnd_Gen_MenuBar 

OK:=0
While (OK=0)
	FORM GET PROPERTIES:C674("Fnd_Gen_Reg_DemoDialog";$width_i;$height_i)
	Fnd_Gen_CenterWindow ($width_i;$height_i;Plain dialog box:K34:4)
	DIALOG:C40("Fnd_Gen_Reg_DemoDialog")
	CLOSE WINDOW:C154
	
	If (OK=0)  // Register was clicked.
		FORM GET PROPERTIES:C674("Fnd_Gen_Reg_RegisterDialog";$width_i;$height_i)
		Fnd_Gen_CenterWindow ($width_i;$height_i;Movable form dialog box:K39:8)
		DIALOG:C40("Fnd_Gen_Reg_RegisterDialog")
		CLOSE WINDOW:C154
	End if 
End while 

Fnd_Gen_Reg_UpdateState 