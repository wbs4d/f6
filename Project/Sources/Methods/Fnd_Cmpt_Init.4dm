//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Cmpt_Init

// Initializes both the process and interprocess variables used by
//   the Foundation compatibility routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 27, 2003
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Cmpt_Info method.
// Modified by Wayne Stewart, 2018-04-16 - Remove call to Fnd_Gen_ComponentCheck 
// ----------------------------------------------------

Fnd_Init  // All INIT methods need to start with this

If (Fnd.Cmpt.Initialized=Null:C1517)
	Fnd.Cmpt:=New object:C1471(\
		"Boolean"; False:C215; \
		"Longint"; 0)
	
	Compiler_Fnd_Cmpt
	
	Fnd.Cmpt.Initialized:=True:C214
	
End if 

If (Undefined:C82(gCenterWind) | Undefined:C82(gSafetyOn) | Undefined:C82(gUpperWind))
	//   This will cause us problems, so we give them a value here.
	gCenterWind:=False:C215
	gSafetyOn:=False:C215
	gUpperWind:=False:C215
	
End if 

