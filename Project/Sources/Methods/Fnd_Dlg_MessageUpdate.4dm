//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_MessageUpdate (number)

  // Updates the message dialog's progress indicator.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The next number in the sequence.

  // Returns: Nothing

  // Created by Dave Batton on Jul 25, 2003
  // ----------------------------------------------------

C_LONGINT:C283($1;$current_i;$numerator_i;$denominator_i)

$current_i:=$1

Fnd_Dlg_Init 

If ((<>Fnd_Dlg_ProgTo_i#0) | (<>Fnd_Dlg_ProgFrom_i#0))
	$numerator_i:=$current_i-<>Fnd_Dlg_ProgFrom_i
	$denominator_i:=<>Fnd_Dlg_ProgTo_i-<>Fnd_Dlg_ProgFrom_i
	If ($denominator_i#0)  // A safety feature, so we don't divide by zero.
		<>Fnd_Dlg_ProgPercent_r:=$numerator_i/$denominator_i
	End if 
	
	  // If we're counting backwards, make the indicator go backwards.
	If (<>Fnd_Dlg_ProgTo_i<<>Fnd_Dlg_ProgFrom_i)
		<>Fnd_Dlg_ProgPercent_r:=1-<>Fnd_Dlg_ProgPercent_r
	End if 
	
	POST OUTSIDE CALL:C329(Process number:C372(<>Fnd_Dlg_MessageProcessName_t))
	IDLE:C311  // Give the indicator an opportunity to update.
End if 