//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_TextToMD5_Bits32Left (?) --> Longint

  // Part of the MD5 hash generating code provided by 4D SA.
  // This code was provided to me by 4D SA and is used with permission.
  // I've left the original code untouched, except for this header.

  // Access: Private

  // Parameters: Undocumented

  // Returns: 
  //   $0 : Longint : Undocumented

  // Created by Dave Batton on Mar 6, 2005
  // Based on code from 4D SA.
  // ----------------------------------------------------

  //--- extracted from 4D Business Kit 2.0 with permission / 4D SA 2002 ---

  //Bitwise rotate x times a 32-bit number to the left

C_LONGINT:C283($1)
C_LONGINT:C283($2)

C_LONGINT:C283($0)

$0:=($1 << $2) | ($1 >> (32-$2))