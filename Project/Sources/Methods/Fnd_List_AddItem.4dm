//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_AddItem (command name; method name)

  // Adds a command to the Command Dialog.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The command name to add to the dialog
  //   $2 : Text : The method name to run if it's selected

  // Returns: Nothing

  // Created by Dave Batton on Sep 30, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$commandName_t;$commandMethod_t)
C_LONGINT:C283($element_i)

$commandName_t:=$1
$commandMethod_t:=$2

Fnd_List_Init 

$element_i:=Find in array:C230(Fnd_List_Commands_at;$commandName_t)

If ($element_i=-1)
	$element_i:=Size of array:C274(Fnd_List_Commands_at)+1
	INSERT IN ARRAY:C227(Fnd_List_Commands_at;$element_i)
	INSERT IN ARRAY:C227(Fnd_List_Methods_at;$element_i)
End if 

Fnd_List_Commands_at{$element_i}:=$commandName_t
Fnd_List_Methods_at{$element_i}:=$commandMethod_t
