//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_Init

// Initializes both the process and interprocess variables used by the Fnd_Rec routines.
// Designed to be called at the beginning of any Protected routines to make sure
//   the necessary variables are initialized.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on August 20, 2003
// Modified by Dave Batton on Feb 4, 2004
//   Updated to version 4.0.2.
// Modified by Dave Batton on May 30, 2004
//   Updated the version number to 4.0.3.
//   Removed the check for the Localization component.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Rec_Info method.
// Modified by Dave Batton on Dec 5, 2005
//   Added support for the new Modify routines.
//   Added a test for the Dialogs component.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Rec_Initialized_b; Fnd_Rec_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Rec_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Rec
	
	Fnd_Rec_Localize
	
	<>Fnd_Rec_Initialized_b:=True:C214
End if 

If (Not:C34(Fnd_Rec_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Rec
	
	ARRAY POINTER:C280(Fnd_Rec_Fields_aptr; 0)
	ARRAY TEXT:C222(Fnd_Rec_FieldNames_at; 0)
	ARRAY INTEGER:C220(Fnd_Rec_FieldTypes_ai; 0)
	
	ARRAY TEXT:C222(Fnd_Rec_Values_at; 2)
	Fnd_Rec_Values_at{1}:=Fnd_Gen_GetString("Fnd_Rec"; "ModTrue")
	Fnd_Rec_Values_at{2}:=Fnd_Gen_GetString("Fnd_Rec"; "ModFalse")
	Fnd_Rec_Values_at:=1
	
	Fnd_Rec_Table_ptr:=Fnd_Gen_CurrentTable
	Fnd_Rec_Value_t:=""
	
	Fnd_Rec_Formula_t:=""
	Fnd_Rec_EditorButtonLabel_t:=Fnd_Gen_GetString("Fnd_Rec"; "ModEditorButton")
	Fnd_Rec_EditorButtonMethod_t:=""
	
	Fnd_Rec_Initialized_b:=True:C214
End if 
