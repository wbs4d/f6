//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Cmpt_GetLongintValue (name of an integer variable) --> Longint

// Returns the value of a long integer (without actually using
//   the variable in this component).

// Access: Shared

// Parameters: 
//   $1 : Text : The name of a Longint variable

// Returns: 
//   $0 : Longint : Returns the value of the longint

// Created by Dave Batton on Feb 22, 2004
// Foundation 6 version by Wayne Stewart on 2021-03-26
// ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1; $variableName_t)

$variableName_t:=$1

Fnd_Cmpt_Init

EXECUTE FORMULA:C63("Fnd.Cmpt.Longint:="+$variableName_t)

$0:=Fnd.Cmpt.Longint