//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Put2 (sequence number group name; number to return)

  // Called as a new process from Fnd_SqNo_Put to put a number 
  //   back into the sequence number group.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The group name
  //   $2 : Longint : The number to reuse

  // Returns: Nothing

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Jan 30, 2004
  //   No longer displays an alert on 4D Server if the timeout is reached.
  //   Ignores an attempt to return zero to the list.
  //   No longer adds duplicate numbers to the reuse list.
  // Modified by Dave Batton on May 12, 2005
  //   No longer displays an error message if the group name doesn't exist.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // ----------------------------------------------------

C_TEXT:C284($1;$seqNoName)
C_LONGINT:C283($2;$seqNo;$element)

$seqNoName:=$1
$seqNo:=$2

QUERY:C277(<>Fnd_SqNo_Table_ptr->;<>Fnd_SqNo_GroupNameFld_ptr->=$seqNoName)

If (Records in selection:C76(<>Fnd_SqNo_Table_ptr->)=1)  // If this is the first time this sequence number name has been used, we must…
	LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)
	While ((Locked:C147(<>Fnd_SqNo_Table_ptr->)) & (Not:C34(Fnd_Gen_QuitNow )))
		DELAY PROCESS:C323(Current process:C322;30)  // We can afford to wait a long time - we're not in a hurry.
		LOAD RECORD:C52(<>Fnd_SqNo_Table_ptr->)
	End while 
	  //Else ` DB050512 - Commented out.
	  //Fnd_Gen_BugAlert ("Fnd_SqNo_Put2";"Unable to find the \""+$seqNoName+"\" group record.") ` DB050512 - Commented out and moved the End if to the bottom.
	
	
	Case of 
		: (Locked:C147(<>Fnd_SqNo_Table_ptr->))
			If (Application type:C494#4D Server:K5:6)  // DB040130 - Added this test.
				Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
				Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_SqNo";"CannotPutNumber";String:C10($seqNo);$seqNoName))
			End if 
			
		: ($seqNo<=0)  // DB040130 - Changed this to less than or equal to zero.
			  // This might possibly happen when canceling out of a record for which we were 
			  //   not able to get a sequence number.  Just ignore it.
			
		: ($seqNo>=<>Fnd_SqNo_NextNumberFld_ptr->)
			  // This might possibly happen if multiple users or processes are messing with 
			  //   sequence numbers.  Just leave without making any modifications.
			
		: ($seqNo=(<>Fnd_SqNo_NextNumberFld_ptr->-1))  // If the number is just one less than the next number…
			<>Fnd_SqNo_NextNumberFld_ptr->:=$seqNo  //   …just adjust the next number.
			SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)
			
		Else   // This is a number we need to store in the blob.
			ARRAY LONGINT:C221($aNumbersToReuse;0)
			BLOB TO VARIABLE:C533(<>Fnd_SqNo_RecycleBinFld_ptr->;$aNumbersToReuse)
			$element:=Find in array:C230($aNumbersToReuse;$seqNo)
			If ($element=-1)  // DB040130 - Now we make sure the number's not already in the list.
				$element:=Size of array:C274($aNumbersToReuse)+1
				INSERT IN ARRAY:C227($aNumbersToReuse;$element)
				$aNumbersToReuse{$element}:=$seqNo
				VARIABLE TO BLOB:C532($aNumbersToReuse;<>Fnd_SqNo_RecycleBinFld_ptr->)
				SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)
			End if 
	End case 
End if 


UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure other users & processes can load the record.
