//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Data_Init

// Initializes both the process and interprocess variables used by the Text routines.
// Designed to be called at the beginning of any shared methods to make sure
//   the necessary variables are initialized.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 27, 2004
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the Fnd_Data_Info method.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck 
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Data_Initialized_b; Fnd_Data_Initialized_b)
C_TEXT:C284($language_t; $listName_t)
C_LONGINT:C283($element_i)

Fnd_Init  // All Init methods must include this method


If (Not:C34(<>Fnd_Data_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Data
	
	<>Fnd_Data_UserBypassKey_i:=Option key mask:K16:7
	
	// Load the name titles used by the Fnd_Data_ParseName method.
	$language_t:=Fnd_Gen_ComponentInfo("Fnd_Loc"; "language")
	If (Length:C16($language_t)=2)
		$listName_t:="Fnd_Data_NameTitles_"+$language_t
	Else 
		$listName_t:="Fnd_Data_NameTitles_EN"
	End if 
	ARRAY TEXT:C222(<>Fnd_Data_NameTitles_at; 0)
	LIST TO ARRAY:C288($listName_t; <>Fnd_Data_NameTitles_at)
	For ($element_i; 1; Size of array:C274(<>Fnd_Data_NameTitles_at))
		<>Fnd_Data_NameTitles_at{$element_i}:=" "+<>Fnd_Data_NameTitles_at{$element_i}+" "
	End for 
	
	<>Fnd_Data_Initialized_b:=True:C214
End if 


If (Not:C34(Fnd_Data_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Data
	
	Fnd_Data_FormatError_i:=0
	
	Fnd_Data_Initialized_b:=True:C214
End if 
