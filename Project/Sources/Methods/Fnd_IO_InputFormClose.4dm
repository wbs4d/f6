//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_InputFormClose --> Boolean

  // Called from the Fnd_IO_InputFormMethod when the user clicks the close box
  //   or quits the database.  Basically we need to ask the user if the record
  //   should be saved if it has been modified.
  // Returns True if the window gets closed.

  // Access: Private

  // Parameters: None

  // Returns:
  //   $0 : Boolean : True if the window will be closed.

  // Created by Dave Batton on Sep 8, 2003
  // Modified by Dave Batton on Mar 20, 2004
  //   Localized the Save Changes dialog.
  //   Removed the Boolean value returned by this method. It wasn't used anyway.
  //   This method now calls the Fnd_Gen_CancelQuit and Fnd_Wnd_CancelCloseAll
  //      if the user clicks Cancel.
  // Modified by Dave Batton on Aug 23, 2004
  //   No longer checks to make sure the Fnd_Wnd component is installed before
  //      calling Fnd_Wnd_CancelCloseAll, since the component is required.
  // Modified by Dave Batton on Nov 3, 2004
  //   Added support for the new Fnd_aa_IO_InputFormButton hook.
  // Modified by Dave Batton on Jan 15, 2005
  //   Now calls Fnd_Wnd_SetPosition rather than Fnd_Dlg_SetPosition.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by: Walt Nelson (2/2/11)
  //  When user clicked close box, OK variable was set from 2 to 1 when EXECUTE METHOD ran
  //  Fnd_Hook_IO_InputFormButton, cannot rely on value in OK variable
  // Modified by: Walt Nelson (6/22/11) - added Ken Spence and Guy Algot's changes for On Close action
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$closed_b;$allow_b)
C_LONGINT:C283($saveCancelDialog_i)

$closed_b:=True:C214  // Assume we're going to close the window.

If (Modified record:C314(Current form table:C627->))
	  // Wait until this process is frontmost.
	If (Current process:C322#Frontmost process:C327(*))
		BRING TO FRONT:C326(Current process:C322)
	End if 
	While (Current process:C322#Frontmost process:C327(*))
		DELAY PROCESS:C323(Current process:C322;5)
	End while 
	
	BEEP:C151
	Fnd_Wnd_Position (Fnd_Wnd_MacOSXSheet)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
	Fnd_Dlg_SetText (Fnd_Loc_GetString ("Fnd_IO";"SaveChangesMsg1");Fnd_Loc_GetString ("Fnd_IO";"SaveChangesMsg2"))
	Fnd_Dlg_SetButtons ("*";"*";"*")
	Fnd_Dlg_SetIcon (Fnd_Dlg_WarnIcon)
	$saveCancelDialog_i:=Fnd_Dlg_Display 
	
	$allow_b:=True:C214
	  //If (Fnd_DoesMethodExist ("Fnd_Hook_IO_InputFormButton")=1)
	  //EXECUTE METHOD("Fnd_Hook_IO_InputFormButton";$allow_b;"save")
	  //End if 
	
	Case of 
		: ($saveCancelDialog_i=1)  // Save
			If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_IO_InputFormButton")=1)  // Modified by: Walt Nelson (6/22/11)
				EXECUTE METHOD:C1007("Fnd_Hook_IO_InputFormButton";$allow_b;"save_close")
			End if   // Modified by: Walt Nelson (6/22/11)
			If ($allow_b)
				ACCEPT:C269
			End if 
		: ($saveCancelDialog_i=2)  // Don't Save
			If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_IO_InputFormButton")=1)  // Modified by: Walt Nelson (6/22/11)
				EXECUTE METHOD:C1007("Fnd_Hook_IO_InputFormButton";$allow_b;"cancel_close")
			End if   // Modified by: Walt Nelson (6/22/11)
			If ($allow_b)  // DB041103 - Added.
				CANCEL:C270
			End if 
		Else   // Cancel
			Fnd_Gen_CancelQuit 
			Fnd_Wnd_CancelCloseAll   // DB040823
			$closed_b:=False:C215
	End case 
	
Else 
	CANCEL:C270
End if 

$0:=$closed_b
