//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo 
//   method for more information.

// Access: Shared

// Parameters: 
//   $1 : Text : Info desired ("version" or "name")

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on Dec 27, 2004
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="version")
		$reply_t:="6.0"
		
	: ($request_t="name")
		$reply_t:="Foundation Lists"
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
