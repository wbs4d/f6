//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo method
//   for more information.

// Access Type: Shared

// Parameters: 
//   $1 : Text : Info desired

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on Nov 19, 2005
// ----------------------------------------------------

C_TEXT:C284($0; $1; $request_t; $reply_t)

$request_t:=$1

Fnd_Out_Init

Case of 
	: ($request_t="version")
		$reply_t:="4.2 beta 1"
		
	: ($request_t="name")
		$reply_t:="Foundation Output"
		
	: ($request_t="state")
		If (Fnd.out.active)
			$reply_t:="active"
		Else 
			$reply_t:="inactive"
		End if 
		
	: ($request_t="highlighted")  // Returns the number of records in the UserSet.
		If (Form:C1466.selectedItems#Null:C1517)
			$reply_t:=String:C10(Form:C1466.selectedItems.length)
		Else 
			$reply_t:="0"
		End if 
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
