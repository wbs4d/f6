//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_Init

// Initializes both the process and interprocess variables used by the localization routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 26, 2003
// Modified by Dave Batton on Jan 19, 2004
//   Updated to version 4.0.1.
//   No longer changes the value of <>Fnd_Loc_CurrentLanguage_t for the French
//      version of 4D.  This fixes some runtime bugs until we get it fully localized for French.
// Modified by Dave Batton on Jan 24, 2004
//   Updated to version 4.0.2.
// Modified by Dave Batton on May 19, 2004
//   Updated to version 4.0.3.
//   Enabled the tests for French and German.
//   Added initialization of the <>Fnd_Loc_Semaphore_t variable.
// Modified by Dave Batton on Dec 27, 2004
//   Removed the version number, since it's handled by the Fnd_Loc_Info method.
// Modified by : Vincent Tournier (6/16/2011)
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_Loc_Initialized_b)
C_TEXT:C284($fileMenuName_t)

If (Not:C34(<>Fnd_Loc_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Loc
	
	If (Length:C16(<>Fnd_Loc_CurrentLanguage_t)#2)  // We may have already set it in Fnd_Loc_LanguageCode.
		// BEGIN Modified by : Vincent Tournier (6/16/2011)
		//$fileMenuName_t:=Get indexed string(79;1)  ` 4D stores the name of the "File" menu here.
		//Case of 
		//: ($fileMenuName_t="Fichier")
		//◊Fnd_Loc_CurrentLanguage_t:="FR"
		//
		//: ($fileMenuName_t="Ablage")
		//◊Fnd_Loc_CurrentLanguage_t:="GR"
		//
		//Else 
		//◊Fnd_Loc_CurrentLanguage_t:="EN"
		//End case 
		Case of 
			: (Get database localization:C1009="@fr@")  // use Get current database localization(2) for v12
				<>Fnd_Loc_CurrentLanguage_t:="FR"
				
			: (Get database localization:C1009="@de@")  // use Get current database localization(2) for v12
				<>Fnd_Loc_CurrentLanguage_t:="GR"
				
			Else 
				<>Fnd_Loc_CurrentLanguage_t:="EN"
		End case 
		// END Modified by : Vincent Tournier (6/16/2011)
	End if 
	
	ARRAY TEXT:C222(<>Fnd_Loc_Modules_at; 0)  // Tracks the 2D array element for each module.
	ARRAY TEXT:C222(<>Fnd_Loc_LookupCodes_at; 0; 0)  // The internal strings.
	ARRAY TEXT:C222(<>Fnd_Loc_Strings_at; 0; 0)  // The localized strings.
	
	<>Fnd_Loc_Semaphore_t:="$Fnd_Loc_Semaphore"  // DB040521 - Added
	
	<>Fnd_Loc_Initialized_b:=True:C214
End if 
