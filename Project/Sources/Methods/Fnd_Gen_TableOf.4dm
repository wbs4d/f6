//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_TableOf (table pointer) --> field pointer

//  Returns a pointer to the table that the field passed in belongs to

// Access: Shared

// Parameters: 
//   $1 : Pointer : A field Pointer

// Returns: 
//   $0 : Pointer : The table the field belongs to

// Created by Doug Hall(20030317)

// ----------------------------------------------------

#DECLARE($field_ptr : Pointer)->$table_ptr : Pointer

$table_ptr:=Table:C252(Table:C252($field_ptr))