//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_OmitSubset

// Called from the "Omit Subset" item in the "Select" menu.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 1, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Rec component is no longer required.
// ----------------------------------------------------

Fnd_Rec_OmitSubset