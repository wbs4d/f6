//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_LockInternalState (lock?)

  // Locks the internal state for the dictionary module.

  // Access: Private

  // Parameters: 
  //   $1 : Boolean : True to Lock, False to Unlock

  // Returns: Nothing

  // Created by Rob Laveaux
  // Modified by Dave Batton on Sep 21, 2007
  //   Replaced the Assert_IsTrue command with a Fnd_Gen_BugAlert call.
  // ----------------------------------------------------

C_BOOLEAN:C305($1;$lock_b)
C_TEXT:C284($semaphore_t)

$lock_b:=$1

Fnd_Dict_Init 

$semaphore_t:="$Fnd_Dict_InternalState"

If ($lock_b)
	If (<>Fnd_Dict_LockCount_ai=0)
		While (Semaphore:C143($semaphore_t;10))
			IDLE:C311
		End while 
	End if 
	
	<>Fnd_Dict_LockCount_ai:=<>Fnd_Dict_LockCount_ai+1
Else 
	
	<>Fnd_Dict_LockCount_ai:=<>Fnd_Dict_LockCount_ai-1
	If (<>Fnd_Dict_LockCount_ai=0)
		CLEAR SEMAPHORE:C144($semaphore_t)
	End if 
	
End if 

If (<>Fnd_Dict_LockCount_ai<0)
	Fnd_Gen_BugAlert (Current method name:C684;"Calls to this method are out of balance")
End if 