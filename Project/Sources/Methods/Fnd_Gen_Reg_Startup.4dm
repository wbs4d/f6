//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_Startup

  // Call when launching the database to initialize the registration routines.

  // Method Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 31, 2003
  // modified by : Vincent Tournier (6/16/2011)
  // ----------------------------------------------------

  // Don't call Fnd_Gen_Init from here, since it calls us.

<>Fnd_Gen_Reg_ListName_t:="Fnd_Gen_Reg_Code"

  // Set the language code.
  // BEGIN modified by : Vincent Tournier (6/16/2011)
Case of 
	: (Get database localization:C1009="@fr@")  // use Get current database localization(2) for v12
		<>Fnd_Loc_CurrentLanguage_t:="FR"
		
	: (Get database localization:C1009="@de@")  // use Get current database localization(2) for v12
		<>Fnd_Loc_CurrentLanguage_t:="GR"
		
	Else 
		<>Fnd_Loc_CurrentLanguage_t:="EN"
End case 
  // END modified by : Vincent Tournier (6/16/2011)

Fnd_Gen_Reg_UpdateState 

If ((<>Fnd_Gen_Reg_State_i#1) & (<>Fnd_Gen_Reg_Status_i#<>Fnd_Gen_Reg_State_i))
	Fnd_Gen_Reg_DemoDialog 
	<>Fnd_Gen_Reg_Status_i:=<>Fnd_Gen_Reg_State_i
	
	  // If <>Fnd_Gen_Reg_State_i = 1, the timeout doesn't matter.
	If (<>Fnd_Gen_Reg_State_i#1)
		If (Is compiled mode:C492)
			<>Fnd_Gen_Reg_Timeout_i:=Tickcount:C458+(10*60*60)  // 10 minutes
		Else 
			<>Fnd_Gen_Reg_Timeout_i:=Tickcount:C458+(30*60*60)  // 30 minutes
		End if 
	End if 
End if 