//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pref_CreateTable

  // Global and IP variables accessed:
C_POINTER:C301(<>Fnd_Pref_Table_ptr;<>Fnd_Pref_OwnerFld_ptr;<>Fnd_Pref_NameFld_ptr)

  // Method Type:    Private

  // Parameters:    None Used

  // Local Variables:
C_BOOLEAN:C305($Found_b)
C_LONGINT:C283($CurrentTableNumber_i;$Delay_i;$ProcessNumber_i)
C_TEXT:C284($SQLCommandText_t)
ARRAY POINTER:C280($FieldsToIndex_aptr;0)

  // Returns:    Nothing

  // Created by Wayne Stewart (Feb 27, 2009)
  //     waynestewart@mac.com
  //  Wayne Stewart (Jun 18, 2009) Added code to delay until table is created
  //   Moved table detection to same process, previously this created a new process every time
  // ----------------------------------------------------

$Found_b:=False:C215
$Delay_i:=180

For ($CurrentTableNumber_i;1;Get last table number:C254)
	If (Is table number valid:C999($CurrentTableNumber_i))
		If (Table name:C256($CurrentTableNumber_i)="Fnd_Pref")
			$Found_b:=True:C214
			$CurrentTableNumber_i:=Get last table number:C254+1
		End if 
	End if 
End for 

If ($Found_b)
Else 
	If (Current process:C322=Process number:C372("Application Process"))  //  Always do this in a new process
		Fnd_Gen_WaitToContinue (True:C214)
		$ProcessNumber_i:=New process:C317(Current method name:C684;128*1024;"Create Preferences Table")
		While (Fnd_Gen_WaitToContinue )
		End while 
	Else 
		If (Not:C34($Found_b))
			$SQLCommandText_t:="CREATE TABLE Fnd_Pref ("
			$SQLCommandText_t:=$SQLCommandText_t+"ID INT32, "
			$SQLCommandText_t:=$SQLCommandText_t+"Owner VARCHAR(80), "
			$SQLCommandText_t:=$SQLCommandText_t+"Name VARCHAR(80), "
			$SQLCommandText_t:=$SQLCommandText_t+"Type INT16, "
			$SQLCommandText_t:=$SQLCommandText_t+"Value VARCHAR"
			$SQLCommandText_t:=$SQLCommandText_t+")"
			
			Begin SQL
				    EXECUTE IMMEDIATE :$SQLCommandText_t;
			End SQL
			
			
			DELAY PROCESS:C323(Current process:C322;$Delay_i)
			
			
			EXECUTE FORMULA:C63("<>Fnd_Pref_Table_ptr:=->[Fnd_Pref]")
			EXECUTE FORMULA:C63("<>Fnd_Pref_OwnerFld_ptr:=->[Fnd_Pref]Owner")
			EXECUTE FORMULA:C63("<>Fnd_Pref_NameFld_ptr:=->[Fnd_Pref]Name")
			
			ARRAY POINTER:C280($FieldsToIndex_aptr;1)
			$FieldsToIndex_aptr{1}:=<>Fnd_Pref_OwnerFld_ptr
			CREATE INDEX:C966(<>Fnd_Pref_Table_ptr->;$FieldsToIndex_aptr;Cluster BTree index:K58:4;"Fnd_Pref_Owner_Cluster")
			
			ARRAY POINTER:C280($FieldsToIndex_aptr;1)
			$FieldsToIndex_aptr{1}:=<>Fnd_Pref_NameFld_ptr
			CREATE INDEX:C966(<>Fnd_Pref_Table_ptr->;$FieldsToIndex_aptr;Standard BTree index:K58:3;"Fnd_Pref_Name_BTree")
			
			ARRAY POINTER:C280($FieldsToIndex_aptr;2)
			$FieldsToIndex_aptr{1}:=<>Fnd_Pref_OwnerFld_ptr
			$FieldsToIndex_aptr{2}:=<>Fnd_Pref_NameFld_ptr
			CREATE INDEX:C966(<>Fnd_Pref_Table_ptr->;$FieldsToIndex_aptr;Standard BTree index:K58:3;"Fnd_Pref_OwnerName_Btree")
			
		End if 
		Fnd_Gen_WaitToContinue (False:C215)
	End if 
End if 
