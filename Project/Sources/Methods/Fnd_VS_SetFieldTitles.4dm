//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_SetFieldTitles (->table; ->field titles; ->field numbers)

  // Allows the developer to set virtual field titles. Designed to be called
  //   as a replacement to 4D's SET FIELD TITLES command.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : The table to use
  //   $2 : Pointer : Array of field titles
  //   $3 : Pointer : Array of field numbers

  // Returns: None

  // Created by Dave Batton on Jan 24, 2004
  //[1] Modified by: John Craig (7/2/09) moved $tableNumber_i:=Table($table_ptr) outside the If (Is table number valid($tableNumber_i)) call so it would actually get set.
  // Modification : Vincent TOURNIER(12/9/11)
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$3;$table_ptr;$fieldNamesArray_ptr;$fieldNumbersArray_ptr)
C_LONGINT:C283($tableNumber_i;$tableElement_i)

$table_ptr:=$1
$fieldNamesArray_ptr:=$2
$fieldNumbersArray_ptr:=$3
$tableNumber_i:=Table:C252($table_ptr)  //[1] Modified by: John Craig (7/2/09) moved this outside the If (Is table number valid($tableNumber_i)) call

Fnd_VS_Init 
  // TRACE

$tableNumber_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)  //[1] Code added (7/3/09) John Craig the arrays of table numbers may no longer be sequential, so find it in the table array

If ($tableNumber_i>-1)  //[1]
	
	  // Make sure the arrays passed in are reasonably valid.
	Case of 
		: (Size of array:C274($fieldNamesArray_ptr->)#Size of array:C274($fieldNumbersArray_ptr->))
			Fnd_Gen_BugAlert (Current method name:C684;"The field name and field number array sizes do not match.")
			
		Else 
			  // Copy the user's arrays into Foundation's internal arrays.
			
			
			  // problem at the next line returning false when tables are deleted and there is a gap in the table numbers 120405: wwn
			  // see email from Diego - Test Localization module 550
			If (Is table number valid:C999($tableNumber_i))
				If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
					  // BEGIN Modification by : Vincent TOURNIER(9/12/11)
					  //$tableElement_i:=Find in array(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)  // Modification : Vincent TOURNIER(9/12/11)
					  // return to previous way 120405: wwn
					COPY ARRAY:C226($fieldNamesArray_ptr->;<>Fnd_VS_FieldNames_at{$tableNumber_i})
					COPY ARRAY:C226($fieldNumbersArray_ptr->;<>Fnd_VS_FieldNumbers_ai{$tableNumber_i})
					  //COPY ARRAY($fieldNamesArray_ptr->;<>Fnd_VS_FieldNames_at{$tableElement_i})
					  //COPY ARRAY($fieldNumbersArray_ptr->;<>Fnd_VS_FieldNumbers_ai{$tableElement_i})
					  // END Modification by : Vincent TOURNIER(9/12/11)
					
					SET FIELD TITLES:C602($table_ptr->;<>Fnd_VS_FieldNames_at{$tableNumber_i};<>Fnd_VS_FieldNumbers_ai{$tableNumber_i};*)  // WN100211 - added *
					
					CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
					
				Else 
					Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
				End if 
			End if 
	End case 
	
End if   //[1]
