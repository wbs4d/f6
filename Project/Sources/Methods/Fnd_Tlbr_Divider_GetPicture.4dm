//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Divider_GetPicture (divider picture name) --> Picture

  // Returns the toolbar divider picture * 4 so it works as a button object.
  // This is the divider version of the Fnd_Bttn_GetPicture method.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The name of the toolbar divider picture

  // Returns: 
  //   $0 : Picture : The buttonized toolbar divider picture

  // Created by Dave Batton on Feb 15, 2004
  // [1] Added by: John Craig (12/10/09) - to use external image library
  // ----------------------------------------------------

C_PICTURE:C286($0;$divider_pic)
C_TEXT:C284($1;$pictureName_t)

$pictureName_t:=$1

Fnd_Bttn_Init   // [1]

If (<>Fnd_Bttn_PicturePath_t#"")  // [1]
	READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$pictureName_t+".png";$divider_pic)  // [1]
Else 
	GET PICTURE FROM LIBRARY:C565($pictureName_t;$divider_pic)
	
End if 

$divider_pic:=$divider_pic/$divider_pic/$divider_pic/$divider_pic

$0:=$divider_pic