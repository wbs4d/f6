//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Art_StartupDialogClose

// Closes the startup message.  Safe to call even if no startup message was displayed.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 2, 2003
// ----------------------------------------------------


CALL FORM:C1391(Storage:C1525.Fnd.Art.WindowReference; "Fnd_Wnd_Cancel")

Use (Storage:C1525.Fnd.Art)  //  Store the Window reference
	Storage:C1525.Fnd.Art.WindowReference:=-1
End use 

