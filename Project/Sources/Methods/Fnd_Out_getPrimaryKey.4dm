//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_getPrimaryKey (Table Number)--> Integer

// Returns the primary key field for a table

// Access: Shared

// Parameters: 
//   $1 : Long : Table Number

// Returns: 
//   $0 : Long : Field Number

// Created by Wayne Stewart (2022-04-18)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE($tableNumber_i : Integer)->$fieldNumber_i : Integer

var $tableName_t; $fieldName_t : Text

If (False:C215)
	C_LONGINT:C283(Fnd_Out_getPrimaryKey; $1; $0)
End if 

$tableName_t:=Table name:C256($tableNumber_i)
$fieldName_t:=ds:C1482[$tableName_t].getInfo().primaryKey
$fieldNumber_i:=ds:C1482[$tableName_t][$fieldName_t].fieldNumber

