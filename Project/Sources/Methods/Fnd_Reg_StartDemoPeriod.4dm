//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_StartDemoPeriod ({feature code})

  // Call this to begin the demo period.  If the demo period has already
  //   been started, this routine will do nothing.

  // Access: Shared

  // Parameters:
  //   $1 : Text : Feature code (optional)

  // Returns: Nothing

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$featureCode_t)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

If (Fnd_Reg_RegistrationState ($featureCode_t)=Fnd_Reg_PreDemo)
	If (Records in selection:C76(<>Fnd_Reg_Table_ptr->)=0)
		CREATE RECORD:C68(<>Fnd_Reg_Table_ptr->)
		<>Fnd_Reg_IDFld_ptr->:=Sequence number:C244(<>Fnd_Reg_Table_ptr->)
		<>Fnd_Reg_FeatureCodeFld_ptr->:=$featureCode_t
		<>Fnd_Reg_DemoDaysFld_ptr->:=<>Fnd_Reg_DefaultDemoDays_i  // If the developer set something different, this record already exists.
		
	Else 
		READ WRITE:C146(<>Fnd_Reg_Table_ptr->)
		LOAD RECORD:C52(<>Fnd_Reg_Table_ptr->)
	End if 
	
	<>Fnd_Reg_DemoStartDateFld_ptr->:=Current date:C33
	Fnd_Reg_SaveRecord (<>Fnd_Reg_SecretKey_t)
End if 