//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_SyncronizeSelection

// Updates the current record selection to match the displayed entity selection.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 4, 2005
// Mod by Wayne Stewart, (2022-04-21) - Rewrite for ORDA
// ----------------------------------------------------

Fnd_Out_Init

USE ENTITY SELECTION:C1513(Form:C1466.listData)  // Create the selection

// No longer bother with User Set
// Fnd_Out_CreateUserSet
