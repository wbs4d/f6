//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_MenuBar ({"*"})

  // Updates the menu bar.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Pass "*" to force a full update (optional)

  // Returns: Nothing

  // Created by Dave Batton on Aug 16, 2003
  // Modified by Dave Batton on Feb 22, 2004
  //   We're now calling the Foundation menu bar by its name rather than by its number.
  // Modified by Dave Batton on May 22, 2004
  //   We no longer load the menu bar and localize it each time this
  //   method is called.  Instead, if the same menu bar name is passed
  //   as last time, we just update the existing menu.
  //   Added the optional "*" parameter to force a full update.
  // ----------------------------------------------------

C_TEXT:C284($1;$menuBarName_t;$formula_t)

Fnd_Menu_Init 

If (Count parameters:C259>=1)
	If ($1="*")
		Fnd_Menu_LastMenuBarName_t:=""  // Forces a full redraw.
	End if 
End if 

$menuBarName_t:=Fnd_Menu_MenuBarName 
If ($menuBarName_t#Fnd_Menu_LastMenuBarName_t)
	SET MENU BAR:C67(<>Fnd_Menu_MenuBar_t)  //;Current process;*)
	If (Current process:C322=Frontmost process:C327(*))  // Only the frontmost process gets to mess with the menu bar.
		If ($menuBarName_t="Fnd_Menu")  // The default Foundation menu.
			SET MENU BAR:C67(<>Fnd_Menu_MenuBar_t)  //;Current process)
			Fnd_Menu_Window_Update 
			If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_Menu_UpdateMenuBar")=1)
				EXECUTE METHOD:C1007("Fnd_Hook_Menu_UpdateMenuBar";*)
			End if 
		End if 
		Fnd_Menu_LastMenuBarName_t:=$menuBarName_t
	End if 
End if 

If ($menuBarName_t="Fnd_Menu")  // The default Foundation menu.
	If (Current process:C322=Frontmost process:C327(*))  // Only the frontmost process gets to mess with the menu bar.
		Fnd_Menu_EnableDisableItems   // Enable/disable items based on the frontmost window.
	End if 
End if 
