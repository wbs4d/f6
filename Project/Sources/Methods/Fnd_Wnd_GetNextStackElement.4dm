//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Wnd_GetNextStackElement --> Number

  // Gets (and reserves) the element number of the next available
  //   position in the window stack arrays.

  // Access Type: Private

  // Parameters: None

  // Returns: 
  //   $0 : Longint : The array element number to use

  // Created by Dave Batton on Sep 9, 2003
  // Modified by Dave Batton on Feb 19, 2005
  //   Now uses a semaphore when accessing the IP arrays.
  // Modified by Dave Batton on Mar 23, 2006
  //   Changed the semaphore timeout to a variable.
  //   Now displays a BugAlert if the semaphore times-out.
  // ----------------------------------------------------

C_LONGINT:C283($0;$element_i;$previousLeft_i;$previousTop_i)

If (Not:C34(Semaphore:C143(<>Fnd_Wnd_Semaphore_t;<>Fnd_Wnd_SemaphoreTimeout_i)))  // DB060323 - Now gets the timeout from an IP variable.
	  // Find the last element in the arrays that's still being used by a window.
	$element_i:=Size of array:C274(<>Fnd_Wnd_StackWndRefNos_ai)
	While ((Window kind:C445(<>Fnd_Wnd_StackWndRefNos_ai{$element_i})=0) & ($element_i>0))
		$element_i:=$element_i-1
	End while 
	
	$previousLeft_i:=<>Fnd_Wnd_StackLeftPositions_ai{$element_i}
	$previousTop_i:=<>Fnd_Wnd_StackTopPositions_ai{$element_i}
	
	$element_i:=$element_i+1  // This is the number for the next window.
	
	If ($element_i>Size of array:C274(<>Fnd_Wnd_StackWndRefNos_ai))
		INSERT IN ARRAY:C227(<>Fnd_Wnd_StackWndRefNos_ai;$element_i)
		INSERT IN ARRAY:C227(<>Fnd_Wnd_StackLeftPositions_ai;$element_i)
		INSERT IN ARRAY:C227(<>Fnd_Wnd_StackTopPositions_ai;$element_i)
		
		<>Fnd_Wnd_StackWndRefNos_ai{$element_i}:=0
		<>Fnd_Wnd_StackLeftPositions_ai{$element_i}:=$previousLeft_i+<>Fnd_Wnd_StackHOffset_i
		<>Fnd_Wnd_StackTopPositions_ai{$element_i}:=$previousTop_i+<>Fnd_Wnd_StackVOffset_i
	End if 
	CLEAR SEMAPHORE:C144(<>Fnd_Wnd_Semaphore_t)
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"Timed-out waiting for the semaphore.")  // DB060323 - Added
End if 

$0:=$element_i