//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_MakeBackground (width) --> Picture

  // Builds a button background to the specified width.

  // Access Type: Private

  // Parameters: 
  //   $1 : Longint : The width needed

  // Returns: 
  //   $0 : Picture : The buton background image of the right width

  // Created by Mark Mitchenall on 18/9/01
  //   Original Method Name: rollover_MakeBackground
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // Modified by Dave Batton on Jan 9, 2005
  //   Now gets the width of the endcaps here dynamically rather than from variables. 
  // Modified by Dave Batton on Apr 13, 2005
  //   Removed the call to Fnd_Bttn_CreateBackgrounds. It's being done elsewhere.
  // Modified by Dave Batton on Nov 18, 2005
  //   Generated backgrounds are now added to the image cache.
  // Modified: [1] John Craig, 02/07/2014, 13:43:02 Changed | picture operator to COMBINE PICTURES with Superimposition method
  // Modified: [2] John Craig, 02/08/2014, 17:05:27 Changed | picture operator to COMBINE PICTURES with Superimposition method
  // ----------------------------------------------------

C_PICTURE:C286($0;$background_pic)
C_LONGINT:C283($1;$width_i;$bit_i;$backgroundPicWidth_i;$leftEdgeSize_i;$rightEdgeSize_i;$height_i)
C_TEXT:C284($cacheName_t)

$width_i:=$1

If ($width_i>0)
	
	  //Check to see if the image is the in cache
	$cacheName_t:=Fnd_Bttn_GetPictureName ("background";String:C10($width_i))
	$background_pic:=Fnd_Bttn_Cache_Image_Get ($cacheName_t)
	
	If (Picture size:C356($background_pic)=0)
		  // Get the widths of the end caps.
		PICTURE PROPERTIES:C457(Fnd_Bttn_BG_Left_pic;$leftEdgeSize_i;$height_i)
		PICTURE PROPERTIES:C457(Fnd_Bttn_BG_Right_pic;$rightEdgeSize_i;$height_i)
		
		  //Decrement the width by the size of the end pieces
		If ($width_i>($leftEdgeSize_i+$rightEdgeSize_i))
			$width_i:=$width_i-($leftEdgeSize_i+$rightEdgeSize_i)
		End if 
		
		  //Add the left hand edge of the rollover picture.
		$background_pic:=Fnd_Bttn_BG_Left_pic
		
		  //Build the background of the rollover to the correct width by testing each bit of
		  //the width variable.  If the bit is set, then the corresponding picture of that 
		  //width is added.  This is the fastest way of building the background.
		$backgroundPicWidth_i:=$leftEdgeSize_i
		
		For ($bit_i;8;0;-1)
			If ($width_i ?? $bit_i)
				  //This bit is set so add the slice with the size corresponding to the bit value.
				  //Note, we're using the OR '|' operator with a horizontal shift, instead of the 
				  //plus operator '+'
				
				  // Modified: [1] John Craig, 02/07/2014, 13:43:02 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
				  //$background_pic:=$background_pic | (Fnd_Bttn_BG_Middles_apic{$bit_i}+$backgroundPicWidth_i)
				COMBINE PICTURES:C987($background_pic;$background_pic;Superimposition:K61:10;Fnd_Bttn_BG_Middles_apic{$bit_i};$backgroundPicWidth_i;0)
				  // Modified: [1] John Craig, 02/07/2014, 13:43:02 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
				
				$backgroundPicWidth_i:=$backgroundPicWidth_i+(0 ?+ $bit_i)
			End if 
		End for 
		
		  //Add the right hand edge to the rollover picture.
		  // Modified: [2] John Craig, 02/08/2014, 17:05:27 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
		  //$background_pic:=$background_pic | (Fnd_Bttn_BG_Right_pic+$backgroundPicWidth_i)
		COMBINE PICTURES:C987($background_pic;$background_pic;Superimposition:K61:10;Fnd_Bttn_BG_Right_pic;$backgroundPicWidth_i;0)
		  // Modified: [2] John Craig, 02/08/2014, 17:05:27 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
		
		  //Add the new image to the cache.
		Fnd_Bttn_Cache_Image_Add ($cacheName_t;$background_pic)
	End if 
End if 

  //Return the background image.
$0:=$background_pic
