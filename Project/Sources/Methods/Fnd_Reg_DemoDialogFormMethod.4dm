//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reg_DemoDialogFormMethod

// We call this from a protected method because a public form can't
//   call private methods.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 16, 2003
// ◊Fnd_Reg_FeatureName_t was not getting properly set
// fix by - Barclay Barry on Nov 18, 2009
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_LONGINT:C283($secondsLeft_i; $daysLeft_i; $left_i; $top_i; $right_i; $bottom_i; $ignore_i; $regState_i)

Case of 
	: (Form event code:C388=On Load:K2:1)
		// This method may get called before Fnd_Shell can set the About Box.
		//If (Fnd_Gen_ComponentAvailable ("Fnd_Shell"))
		//SET ABOUT(Fnd_Gen_GetString ("Fnd_Shell";"AboutMenu")+" "+Fnd_Gen_GetDatabaseInfo ("DatabaseName");"Fnd_Menu_About")  ` Install the custom About Box.
		//End if 
		
		SET WINDOW TITLE:C213(Fnd_Gen_GetString("Fnd_Reg"; "DemoWindowTitle"))
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))
		//EXECUTE METHOD("Fnd_Menu_DisableAll"; *)
		//End if 
		Fnd_Menu_DisableAll
		
		//◊Fnd_Reg_FeatureName_t:=Fnd_Reg_FeatureName (Fnd_Reg_DialogFeatureCode_t)
		// new code Nov 18, 2009
		<>Fnd_Reg_HostFeatureName_ptr->:=Fnd_Reg_FeatureName(Fnd_Reg_DialogFeatureCode_t)
		// end new code Nov 18, 2009
		
		<>Fnd_Reg_HostMessage1_ptr->:=Fnd_Reg_DemoMessage(Fnd_Reg_DialogFeatureCode_t)
		If (<>Fnd_Reg_HostMessage1_ptr->="")
			If (Fnd_Reg_DialogFeatureCode_t=<>Fnd_Reg_DefaultFeatureCode_t)
				<>Fnd_Reg_HostMessage1_ptr->:=Fnd_Gen_GetString("Fnd_Reg"; "ApplicationDemoMessage"; String:C10(Fnd_Reg_DemoDays(Fnd_Reg_DialogFeatureCode_t)))
			Else 
				<>Fnd_Reg_HostMessage1_ptr->:=Fnd_Gen_GetString("Fnd_Reg"; "FeatureDemoMessage"; String:C10(Fnd_Reg_DemoDays(Fnd_Reg_DialogFeatureCode_t)))
			End if 
		End if 
		
		// Set the button texts and move the buttons if needed.
		Fnd_Gen_ButtonText(<>Fnd_Reg_HostOKButton_ptr; Fnd_Gen_GetString("Fnd_Reg"; "DemoContinueButton"); Align right:K42:4; -><>Fnd_Reg_RegisterButton_i)
		OBJECT GET COORDINATES:C663(<>Fnd_Reg_HostOKButton_ptr->; $left_i; $ignore_i; $right_i; $ignore_i)
		OBJECT GET COORDINATES:C663(<>Fnd_Reg_HostCountDown_ptr->; $ignore_i; $top_i; $ignore_i; $bottom_i)
		OBJECT MOVE:C664(<>Fnd_Reg_HostCountDown_ptr; $left_i; $top_i; $right_i; $bottom_i; *)
		
		Fnd_Gen_ButtonText(<>Fnd_Reg_HostRegisterButton_ptr; Fnd_Gen_GetString("Fnd_Reg"; "DemoRegisterButton"); Align right:K42:4)
		
		<>Fnd_Reg_BuyNowURL_t:=Fnd_Reg_BuyNowURL(Fnd_Reg_DialogFeatureCode_t)
		If (<>Fnd_Reg_BuyNowURL_t="")
			OBJECT SET VISIBLE:C603(<>Fnd_Reg_HostBuyNowButton_ptr->; False:C215)
		Else 
			Fnd_Gen_ButtonText(<>Fnd_Reg_HostBuyNowButton_ptr; Fnd_Gen_GetString("Fnd_Reg"; "DemoBuyNowButton"); Align left:K42:2)
		End if 
		
		// Start a timer to count down for the delay.
		SET TIMER:C645(5)
		<>Fnd_Reg_HostCountDown_ptr->:=""
		
		$daysLeft_i:=Fnd_Reg_GetDemoDaysRemaining(Fnd_Reg_DialogFeatureCode_t)
		$regState_i:=Fnd_Reg_RegistrationState(Fnd_Reg_DialogFeatureCode_t)
		
		Case of 
			: ($regState_i=Fnd_Reg_PreDemo)
				<>Fnd_Reg_HostMessage2_ptr->:=""
			: ($daysLeft_i=1)
				<>Fnd_Reg_HostMessage2_ptr->:=Fnd_Gen_GetString("Fnd_Reg"; "DemoExpiresTomorrow")
			: ($regState_i=Fnd_Reg_Expired)
				<>Fnd_Reg_HostMessage2_ptr->:=Fnd_Gen_GetString("Fnd_Reg"; "DemoHasExpired")
			Else 
				<>Fnd_Reg_HostMessage2_ptr->:=Fnd_Gen_GetString("Fnd_Reg"; "DemoExpiresInDays"; String:C10($daysLeft_i))
		End case 
		
		Case of 
			: ($regState_i=Fnd_Reg_PreDemo)
				<>Fnd_Reg_FinishTime_i:=Tickcount:C458  // No delay.
			: (($daysLeft_i<=0) | ($regState_i=Fnd_Reg_Expired))
				_O_OBJECT SET COLOR:C271(<>Fnd_Reg_HostMessage2_ptr->; (0-(Red:K11:4+(256*White:K11:1))))
				<>Fnd_Reg_FinishTime_i:=Tickcount:C458+(8*60)  // 8 second delay.
				OBJECT SET TITLE:C194(<>Fnd_Reg_HostOKButton_ptr->; "")
				OBJECT SET ENABLED:C1123(<>Fnd_Reg_HostOKButton_ptr->; False:C215)
			: ($daysLeft_i<=7)
				_O_OBJECT SET COLOR:C271(<>Fnd_Reg_HostMessage2_ptr->; (0-(Red:K11:4+(256*White:K11:1))))
				<>Fnd_Reg_FinishTime_i:=Tickcount:C458+(5*60)  // 5 second delay.
				OBJECT SET TITLE:C194(<>Fnd_Reg_HostOKButton_ptr->; "")
				OBJECT SET ENABLED:C1123(<>Fnd_Reg_HostOKButton_ptr->; False:C215)
			Else 
				<>Fnd_Reg_FinishTime_i:=Tickcount:C458  // No delay.
		End case 
		
		
	: (Form event code:C388=On Timer:K2:25)
		$secondsLeft_i:=(<>Fnd_Reg_FinishTime_i-Tickcount:C458)/60
		Case of 
			: ($secondsLeft_i<=0)
				OBJECT SET VISIBLE:C603(<>Fnd_Reg_HostCountDown_ptr->; False:C215)
				OBJECT SET TITLE:C194(<>Fnd_Reg_HostOKButton_ptr->; Fnd_Gen_GetString("Fnd_Reg"; "DemoContinueButton"))
				OBJECT SET ENABLED:C1123(<>Fnd_Reg_HostOKButton_ptr->; True:C214)
				SET TIMER:C645(0)
			: ($secondsLeft_i<=7)
				<>Fnd_Reg_HostCountDown_ptr->:=String:C10($secondsLeft_i)
		End case 
End case 

Fnd_Gen_FormMethod
