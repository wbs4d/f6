//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_TableName (->table) --> Text

  // Returns the virtual table name of the specified table.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a table

  // Returns: 
  //   $0 : Text : The virtual table name

  // Created by Dave Batton on Sep 8, 2003
  // Modified by Dave Batton on Jan 24, 2004
  //   Now returns virtual table names.
  // ----------------------------------------------------

C_TEXT:C284($0;$tableName_t)
C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($tableNumber_i;$element_i)

Fnd_VS_Init 

$table_ptr:=$1

$tableName_t:=""

$tableNumber_i:=Table:C252($table_ptr)

If (Is table number valid:C999($tableNumber_i))
	If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
		$element_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)
		If ($element_i>0)
			$tableName_t:=<>Fnd_VS_TableNames_at{$element_i}
		Else 
			$tableName_t:=Table name:C256($table_ptr)  // Just in case.
		End if 
		
		CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
	End if 
End if 

$0:=$tableName_t
