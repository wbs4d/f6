//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_GetWindowTitle ({->table{; record number}}) -> Text

  // Returns the suggested title for the current window for input forms.

  // Access: Private

  // Parameters:
  //   $1 : Pointer : The table (optional)
  //   $2 : Longint : The record number (optional)

  // Returns: 
  //   $0 : Text : Suggested title

  // Created by Dave Batton on May 2, 2003
  // Modified by Dave Batton on Sep 22, 2004
  //   Added a space after the window title so we can check for it in
  //   the Fnd_IO_InputFormMethod method.
  // [1] Added by: John Craig (12/14/09)
  // ----------------------------------------------------

C_TEXT:C284($0;$title_t;$tableName_t)
C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($2;$recordNumber_i)

If (Count parameters:C259>=1)
	$table_ptr:=$1
Else 
	$table_ptr:=Fnd_Gen_CurrentTable 
End if 

If (Count parameters:C259>=2)
	$recordNumber_i:=$2
Else 
	$recordNumber_i:=Record number:C243($table_ptr->)
End if 

Fnd_IO_Init   // ### DB050407 - Since this is a Private method, I think this can be removed.

$tableName_t:=Fnd_VS_TableName ($table_ptr)

Case of 
	: ($recordNumber_i=No current record:K29:2)
		$title_t:=Fnd_Loc_GetString ("Fnd_IO";"WindowTitleNoRecord";$tableName_t)
		
	: ($recordNumber_i=New record:K29:1)
		$title_t:=Fnd_Loc_GetString ("Fnd_IO";"WindowTitleNewRecord";$tableName_t)
		
	Else 
		
		If (Locked:C147($table_ptr->))  // [1]
			$title_t:=Fnd_Loc_GetString ("Fnd_IO";"WindowTitleRecord";$tableName_t;String:C10($recordNumber_i)+" (Locked)")  // [1]
		Else   // [1]
			$title_t:=Fnd_Loc_GetString ("Fnd_IO";"WindowTitleRecord";$tableName_t;String:C10($recordNumber_i))
		End if   // [1]
End case 

$0:=$title_t+" "  // DB040922 - Added the space so we can check for it in Fnd_IO_InputFormMethod
