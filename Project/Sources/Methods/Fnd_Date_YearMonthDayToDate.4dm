//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_YearMonthDayToDate (year; month; day) --> Date

  // Converts the year, month, and day into a 4D date.
  // Works with either a two or four digit year.
  // Works regardless of the system's date format or the default century setting.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Year (2 or 4 digits)
  //   $2 : Longint : Month
  //   $3 : Longint : Day

  // Returns: 
  //   $0 : Date : The date

  // Created by Dave Batton on May 2, 2004
  // ----------------------------------------------------

C_DATE:C307($0;$result_date)
C_LONGINT:C283($1;$2;$3;$year_i;$month_i;$day_i)
C_TEXT:C284($dateStr_t)

$year_i:=$1
$month_i:=$2
$day_i:=$3

  // There's really no reason to call Fnd_Date_Init here.

  // Handle 2 digit dates here. Make it a 4 digit date.
  // This technique works for any system date format.
If ($year_i<100)
	$dateStr_t:=String:C10(!1776-01-02!;Internal date short:K1:7)
	$dateStr_t:=Replace string:C233($dateStr_t;"1776";String:C10($year_i;"00"))
	$year_i:=Year of:C25(Date:C102($dateStr_t))
End if 

$result_date:=Add to date:C393(!1900-01-01!;$year_i-1900;$month_i-1;$day_i-1)

$0:=$result_date