//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_GetExpirationDate ({feature code}) --> Date

  // Gets the feature's expiration date. This is calculated by adding the
  //   number of demo days to the date the demo period was started.
  // If the demo hasn't been started, the number of demo days is
  //   added to the current date, and the resulting date is returned.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: 
  //   $0 : Date : The expiration date

  // Created by Dave Batton on Jul 31, 2003
  // ----------------------------------------------------

C_DATE:C307($0;$expirationDate_d)
C_TEXT:C284($1;$featureCode_t)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

READ ONLY:C145(<>Fnd_Reg_Table_ptr->)
Fnd_Reg_LoadRegistrationRecord ($featureCode_t)

Case of 
	: (<>Fnd_Reg_DemoStartDateFld_ptr->#!00-00-00!)
		$expirationDate_d:=Add to date:C393(<>Fnd_Reg_DemoStartDateFld_ptr->;0;0;<>Fnd_Reg_DemoDaysFld_ptr->)
	Else   // No record was found. 
		$expirationDate_d:=Add to date:C393(Current date:C33;0;0;<>Fnd_Reg_DefaultDemoDays_i)
End case 

$0:=$expirationDate_d
