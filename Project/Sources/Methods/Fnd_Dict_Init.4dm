//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dict_Init

// Initialises the dictionary module.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Rob Laveaux
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Dict_Initialized_b; Fnd_Dict_Initialized_b)
C_LONGINT:C283(<>Fnd_Dict_LockCount_ai)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Dict_Initialized_b))
	Compiler_Fnd_Dict
	
	<>Fnd_Dict_LockCount_ai:=0
	
	ARRAY TEXT:C222(<>Fnd_Dict_Names_at; 0)
	ARRAY LONGINT:C221(<>Fnd_Dict_RetainCounts_ai; 0)
	
	ARRAY TEXT:C222(<>Fnd_Dict_Keys_at; 0; 0)
	ARRAY TEXT:C222(<>Fnd_Dict_Values_at; 0; 0)
	ARRAY LONGINT:C221(<>Fnd_Dict_DataTypes_ai; 0; 0)
	
	<>Fnd_Dict_Initialized_b:=True:C214
End if 

If (Not:C34(Fnd_Dict_Initialized_b))
	Compiler_Fnd_Dict
	Fnd_Dict_Initialized_b:=True:C214
End if 
