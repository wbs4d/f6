//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Reg_Alert (message)

// Displays Foundation's Alert dialog if available. Otherwise displays a 4D alert.

// Method Type: Private

// Parameters: 
//   $1 : Text : The text message to display

// Returns: Nothing

// Created by Dave Batton on Dec 30, 2003
// ----------------------------------------------------

C_TEXT:C284($1; $message_t)

$message_t:=$1

//If (Fnd_Gen_ComponentAvailable("Fnd_Dlg"))

//EXECUTE METHOD("Fnd_Wnd_Position"; *; Fnd_Wnd_MacOSXSheet)
//EXECUTE METHOD("Fnd_Dlg_Alert"; *; $message_t)
//Else 
//ALERT($message_t)
//End if 

Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
Fnd_Dlg_Alert($message_t)