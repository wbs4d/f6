//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_GetPictureWidth (icon; text{; variation}) --> Number

// Returns the width of the rollover picture that uses the specified icon and text.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the icon
//   $2 : Text : The button label
//   $3 : Text : The button variation ("current" or "menu")

// Returns: 
//   $0 : Longint : The width of the picture button.

// Created by Mark Mitchenall on 11/2/02
//   Original Method Name: rollover_GetIconDisabled
//   Part of the toolbar_Component, ©2001 mitchenall.com
//   Used with permission.
// Modified by Dave Batton on Feb 20, 2004
//   Updated specifically for use by the Foundation Shell.
// Modified by Dave Batton on Nov 18, 2005
//   Added the new variation parameter.
// ----------------------------------------------------

C_LONGINT:C283($0;$width_i;$element_i;$height_i)
C_TEXT:C284($1;$2;$3;$iconName_t;$buttonLabel_t;$rolloverPictureName_t;$styleName_t;$variation_t)
C_PICTURE:C286($rolloverButton_pic)

$iconName_t:=$1
$buttonLabel_t:=$2
If (Count parameters:C259>=3)
	$variation_t:=$3
End if 

Fnd_Bttn_Init

$width_i:=0
$iconName_t:=Fnd_Bttn_GetSizedIconName($iconName_t)

$rolloverPictureName_t:=Fnd_Bttn_GetPictureName($iconName_t;$buttonLabel_t;$variation_t)

//Now find out if the picture has been created already
$element_i:=Find in array:C230(<>Fnd_Bttn_CacheNames_at;$rolloverPictureName_t)

If ($element_i>0)
	$width_i:=<>Fnd_Bttn_PictureWidths_ai{$element_i}
Else 
	$rolloverButton_pic:=Fnd_Bttn_GetPicture($iconName_t;$buttonLabel_t;$variation_t)
	PICTURE PROPERTIES:C457($rolloverButton_pic;$width_i;$height_i)
End if 

$0:=$width_i