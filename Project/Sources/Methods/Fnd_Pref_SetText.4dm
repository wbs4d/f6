//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetText (name; value{; scope})

// Saves a text value in the user's preferences file.
// The scope can be one of these:
//   0 = store locally (Fnd_Pref_Local)
//   1 = store privately on server (Fnd_Pref_Server)
//   2 = store shared on server (Fnd_Pref_Shared)

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Text : The text value to save
//   $3 : Longint : How to save it (optional)

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Added the scope parameter.
//   Now displays a BugAlert if the semaphore times-out.
// Modified by Wayne Stewart on 2023-07-27:
//   Replaced the Interprocess "constants" with actual constants
// ----------------------------------------------------

C_TEXT:C284($1; $2; $itemName_t; $itemValue_t)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_t:=$2
If (Count parameters:C259>=3)
	$itemScope_i:=$3
Else 
	$itemScope_i:=Fnd_Pref_Local
End if 

Fnd_Pref_Init

If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
	Fnd_Pref_SetValue($itemName_t; Is text:K8:3; $itemValue_t; $itemScope_i)
	CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
End if 