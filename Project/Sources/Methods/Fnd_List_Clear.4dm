//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_Clear

  // Clears the Command List elements.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 1, 2003
  // ----------------------------------------------------

Fnd_List_Init 

ARRAY TEXT:C222(Fnd_List_Commands_at;0)
ARRAY TEXT:C222(Fnd_List_Methods_at;0)
