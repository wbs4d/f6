//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetWindow ({name{; window ref}})
// Option 2:  Fnd_Pref_SetWindow (name; left; top; right; bottom)

// Saves the coordinates of the current window in the user's preferences file.
// If no name is specified, the current process name is used.
// If no window is specified, the frontmost window of the current process is used.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item (optional)
//   $2 : Longint : The window to save (optional)

// Or: 
//   $1 : Text : The name of the preference item
//   $2 : Longint : The left coordinate
//   $3 : Longint : The top coordinate
//   $4 : Longint : The right coordinate
//   $5 : Longint : The bottom coordinate

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Mar 17, 2004
//   Now the preference name is optional.
// Modified by Dave Batton on Mar 23, 2006
//   Changed the semaphore timeout to a variable.
//   Now displays a BugAlert if the semaphore times-out.
// Modified by Wayne Stewart on 2023-07-27:
//   Replaced the Interprocess "constants" with actual constants
// ----------------------------------------------------

C_TEXT:C284($1; $prefName_t; $prefValue_t)
C_LONGINT:C283($2; $3; $4; $5; $windowRef_i; $left_i; $top_i; $right_i; $bottom_i; $element_i; $processState_i; $processTime_i)

Case of 
	: (Count parameters:C259=0)
		PROCESS PROPERTIES:C336(Current process:C322; $prefName_t; $processState_i; $processTime_i)
		GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i; Fnd_Pref_GetProcessWindowRef(Current process:C322))
		
	: (Count parameters:C259=1)
		$prefName_t:=$1
		GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i; Fnd_Pref_GetProcessWindowRef(Current process:C322))
		
	: (Count parameters:C259=2)
		$prefName_t:=$1
		GET WINDOW RECT:C443($left_i; $top_i; $right_i; $bottom_i; $2)
		
	: (Count parameters:C259=5)
		$prefName_t:=$1
		$left_i:=$2
		$top_i:=$3
		$right_i:=$4
		$bottom_i:=$5
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "An incorrect number of parameters was passed.")
End case 

Fnd_Pref_Init

If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))  // DB060323 - Changed the timeout to a variable.
	$prefValue_t:=String:C10($left_i)+", "+String:C10($top_i)+", "+String:C10($right_i)+", "+String:C10($bottom_i)
	Fnd_Pref_SetValue($prefName_t; -1; $prefValue_t; Fnd_Pref_Local)
	CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
End if 
