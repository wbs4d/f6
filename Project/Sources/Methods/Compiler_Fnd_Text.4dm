//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Text

  // Compiler variables related to the Foundation Text routines.
  // Called by Fnd_Text_Init.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Apr 27, 2004
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Text_Initialized_b)
If (Not:C34(<>Fnd_Text_Initialized_b))  // So we only do this once.
	ARRAY TEXT:C222(<>Fnd_Text_CapExcludeWords_at;0)
	
	ARRAY LONGINT:C221(<>Fnd_Text_Text2MD5_SINUS_ai;64)
End if 


C_BOOLEAN:C305(Fnd_Text_Initialized_b)
If (Not:C34(Fnd_Text_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(Fnd_Text_TextToMD5_wd_A_i;Fnd_Text_TextToMD5_wd_B_i;Fnd_Text_TextToMD5_wd_C_i;Fnd_Text_TextToMD5_wd_D_i)
	C_BLOB:C604(Fnd_Text_TempBlob_blob)
	
	ARRAY LONGINT:C221(Fnd_Text_TextToMD5_M_WORDs_ai;0)
	ARRAY LONGINT:C221(Fnd_Text_TextToMD5_X_WORDs_ai;0)
End if 


  // Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Text_Base64ToText ;$0;$1)
	
	C_POINTER:C301(Fnd_Text_CapitalizeExclude ;$1)
	
	C_TEXT:C284(Fnd_Text_Capitalize ;$0;$1)
	C_LONGINT:C283(Fnd_Text_Capitalize ;$2)
	
	C_POINTER:C301(Fnd_Text_DecodeBase64Blob ;$1)
	
	C_POINTER:C301(Fnd_Text_EncodeBase64Blob ;$1)
	C_BOOLEAN:C305(Fnd_Text_EncodeBase64Blob ;$2)
	C_TEXT:C284(Fnd_Text_EncodeBase64Blob ;$3)
	
	C_TEXT:C284(Fnd_Text_FormatNumber ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Text_Info ;$0;$1)
	
	C_LONGINT:C283(Fnd_Text_TextToMD5_Bits32Add ;$1;$2)
	C_LONGINT:C283(Fnd_Text_TextToMD5_Bits32Add ;$0)
	
	C_LONGINT:C283(Fnd_Text_TextToMD5_Bits32Left ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Text_TextToMD5_Calc ;$0;$1;$2)
	C_POINTER:C301(Fnd_Text_TextToMD5_Calc ;$3)
	C_LONGINT:C283(Fnd_Text_TextToMD5_Calc ;$4;$5;$6;$7;$8;$9)
	
	C_TEXT:C284(Fnd_Text_TextToBase64 ;$0;$1)
	
	C_TEXT:C284(Fnd_Text_TextToMD5 ;$0;$1)
	
	C_TEXT:C284(Fnd_Text_PadSpaces ;$0;$1)
	C_LONGINT:C283(Fnd_Text_PadSpaces ;$2)
	
	C_TEXT:C284(Fnd_Text_StripSpaces ;$0;$1)
	C_LONGINT:C283(Fnd_Text_StripSpaces ;$2)
	
	C_TEXT:C284(Fnd_Text_Wrap ;$0;$1)
	C_LONGINT:C283(Fnd_Text_Wrap ;$2)
End if 
