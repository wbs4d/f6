//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_InputFormResize

  // Called by the Fnd_IO_InputFormMethod to move the inherited buttons around.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Sep 20, 2004
  // Modified by Dave Batton on Jan 6, 2005
  //   Changed <>Fnd_IO_DisplayNavButtons_b to Fnd_IO_DisplayNavButtons_b.
  // Modification : Vincent TOURNIER(12/14/11) use BEST OBJECT SIZE to size the renamed buttons properly
  // Modification by John Craig November 27, 2012: to set minimum size of Cancel and Save buttons
  // ----------------------------------------------------

C_BOOLEAN:C305(Fnd_IO_DisplayNavButtons_b)  // The only var we really need below - don't bother wind Fnd_IO_Init here.
C_LONGINT:C283($winLeft_i;$winTop_i;$winRight_i;$winBottom_i;$winWidth_i;$winHeight_i)
C_LONGINT:C283($left_i;$top_i;$right_i;$bottom_i;$bttnWidth_i;$bttnHeight_i)

  // First, see if the form was inherited. If not, we don't need to do anything.
OBJECT GET COORDINATES:C663(*;"Fnd_IO_OKButton_i";$bttnLeft_i;$bttnTop_i;$bttnRight_i;$bttnBottom_i)

If (($bttnRight_i>0) | ($bttnBottom_i>0))
	  // Get the window size.
	GET WINDOW RECT:C443($winLeft_i;$winTop_i;$winRight_i;$winBottom_i)
	$winWidth_i:=$winRight_i-$winLeft_i
	$winHeight_i:=$winBottom_i-$winTop_i
	
	  // Move the OK button.
	OBJECT GET COORDINATES:C663(*;"Fnd_IO_OKButton_i";$bttnLeft_i;$bttnTop_i;$bttnRight_i;$bttnBottom_i)
	
	  // BEGIN Modification : Vincent TOURNIER(14/12/11)
	OBJECT GET BEST SIZE:C717(*;"Fnd_IO_OKButton_i";$bttnWidth_i;$bttnHeight_i)
	  // END Modification : Vincent TOURNIER(14/12/11)
	If ($bttnHeight_i<22)  // [4] Added: John Craig, 11/27/2012, 09:05:35 - OKButton height should not be less than 22 
		$bttnHeight_i:=22
	End if 
	If ($bttnWidth_i<80)  // [5] Added: John Craig, 11/27/2012, 09:05:35 - OKButton width should not be less than 80 
		$bttnWidth_i:=80
	End if 
	
	$right_i:=$winWidth_i-20
	$bottom_i:=$winHeight_i-20
	$left_i:=$right_i-$bttnWidth_i
	$top_i:=$bottom_i-$bttnHeight_i
	OBJECT MOVE:C664(*;"Fnd_IO_OKButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
	
	  // Now place the Cancel button.
	OBJECT GET COORDINATES:C663(*;"Fnd_IO_CancelButton_i";$bttnLeft_i;$bttnTop_i;$bttnRight_i;$bttnBottom_i)
	  // BEGIN Modification : Vincent TOURNIER(14/12/11)
	OBJECT GET BEST SIZE:C717(*;"Fnd_IO_CancelButton_i";$bttnWidth_i;$bttnHeight_i)
	  // END Modification : Vincent TOURNIER(14/12/11)
	If ($bttnHeight_i<22)  // [6] Added: John Craig, 11/27/2012, 09:05:35 - CancelButton height should not be less than 22 
		$bttnHeight_i:=22
	End if 
	If ($bttnWidth_i<80)  // [7] Added: John Craig, 11/27/2012, 09:05:35 - CancelButton width should not be less than 80 
		$bttnWidth_i:=80
	End if 
	
	$right_i:=$left_i-12
	$left_i:=$right_i-$bttnWidth_i
	OBJECT MOVE:C664(*;"Fnd_IO_CancelButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
	
	  // This stuff is only needed to display the navigation buttons.
	If (Fnd_IO_DisplayNavButtons_b)  // DB050106
		  // Place the navigation buttons.
		$left_i:=20
		  // $top_i is good as-is.
		
		OBJECT GET BEST SIZE:C717(*;"Fnd_IO_FirstButton_i";$bttnWidth_i;$bttnHeight_i)
		$right_i:=$left_i+$bttnWidth_i
		$bottom_i:=$top_i+$bttnHeight_i
		OBJECT MOVE:C664(*;"Fnd_IO_FirstButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
		
		OBJECT GET BEST SIZE:C717(*;"Fnd_IO_PrevButton_i";$bttnWidth_i;$bttnHeight_i)
		$left_i:=$right_i
		$right_i:=$left_i+$bttnWidth_i
		OBJECT MOVE:C664(*;"Fnd_IO_PrevButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
		
		OBJECT GET BEST SIZE:C717(*;"Fnd_IO_NextButton_i";$bttnWidth_i;$bttnHeight_i)
		$left_i:=$right_i
		$right_i:=$left_i+$bttnWidth_i
		OBJECT MOVE:C664(*;"Fnd_IO_NextButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
		
		OBJECT GET BEST SIZE:C717(*;"Fnd_IO_LastButton_i";$bttnWidth_i;$bttnHeight_i)
		$left_i:=$right_i
		$right_i:=$left_i+$bttnWidth_i
		OBJECT MOVE:C664(*;"Fnd_IO_LastButton_i";$left_i;$top_i;$right_i;$bottom_i;*)
	End if 
End if 