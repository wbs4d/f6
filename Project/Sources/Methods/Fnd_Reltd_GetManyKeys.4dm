//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reltd_GetManyKeys (table pointer; array pointer) --> integer

// Fills array $2-> with the primary keys of related many tables to table $1->
// Returns the number of related many associations for table $1->

// Access: Shared

// Parameters: 
//   $1 : Pointer : Table Pointer
//   $2 : Pointer : Primary keys of related many tables

// Returns: 
//   $0 : Integer : the number of related many associations

// Created by Doug Hall (2003-06-17)
// ----------------------------------------------------

#DECLARE($table_ptr : Pointer; $primaryKeysArray_ptr : Pointer)->$numberOfRelatedManyTables_i : Integer
var $tableNum_i : Integer

Fnd_Reltd_Init  // Need to call this to ensure the arrays have been set up.

$tableNum_i:=Table:C252($table_ptr)

COPY ARRAY:C226(<>Fnd_Reltd_Many_aptr{$tableNum_i}; $primaryKeysArray_ptr->)

$numberOfRelatedManyTables_i:=Size of array:C274(<>Fnd_Reltd_Many_aptr{$tableNum_i})
