//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_ValidateUnlockCode (secret key; feature code; user name; license info; unlock code; format) --> Boolean

  // Returns True if a valid unlock code was passed for the specified
  //   product number and user name.
  // Strip any license info before calling.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The unique product secret key
  //   $2 : Text : The feature code
  //   $3 : Text : The user name
  //   $4 : Text : The license information
  //   $5 : Text : The user-entered unlock code
  //   $6 : Text : The license code format string

  // Returns: 
  //   $0 : Boolean : True if the entered unlock code equals the generated unlock code

  // Created by Dave Batton on Mar 14, 2003
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$valid_b)
C_TEXT:C284($1;$2;$3;$4;$5;$6;$secretKey_t;$featureCode_t;$personalization_t;$enteredUnlockCode_t;$expectedUnlockCode_t;$licenseInfo_t;$format_t)
C_TEXT:C284($character_t)
C_LONGINT:C283($position_i)

$secretKey_t:=$1
$featureCode_t:=$2
$personalization_t:=$3
$licenseInfo_t:=$4
$enteredUnlockCode_t:=$5
$format_t:=$6

  // We require the developer to call Fnd_Reg_SetSecretKey first, so <>Fnd_Reg_SecretKey_t (which we pass to this method) should have a value.
If ($secretKey_t="")
	Fnd_Gen_BugAlert (Current method name:C684;"Fnd_Reg_SetSecretKey must be called before calling any other "+"Foundation Registration routines.")
End if 

If ($enteredUnlockCode_t#"")
	$expectedUnlockCode_t:=$secretKey_t+"::"+$featureCode_t+"::"+$personalization_t+"::"+$licenseInfo_t
	$expectedUnlockCode_t:=Fnd_Text_TextToMD5 ($expectedUnlockCode_t)
	$expectedUnlockCode_t:=Fnd_Reg_Format ($expectedUnlockCode_t;$format_t)
	
	$valid_b:=($expectedUnlockCode_t=$enteredUnlockCode_t)
	
	
Else 
	$valid_b:=False:C215
End if 

$0:=$valid_b