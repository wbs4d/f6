//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Bttn_GetPicture (icon name; button label{; variation}) --> Picture


//This routine returns a picture of a rollover based on the parameters
//   you pass for the icon, text, size and type.  If the picture already
//   exists in the Cache, it is retreived from there instead of being
//   generated again.

// Supports these variations:
//   "current" : The currently selected button (Mac OS X Prefs style)

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the icon to use from the 4D picture library
//   $2 : Text : The label for the button
//   $3 : Text : The variaton name (optional)

// Returns: 
//   $0 : Picture : A picture to use for a 4D picture rollover button

// Created by Mark Mitchenall on 17/9/01
//   Original Method Name: rollover_GetIconDisabled
//   Part of the toolbar_Component, ©2001 mitchenall.com
//   Used with permission.
// Modified by Dave Batton on Feb 20, 2004
//   Updated specifically for use by the Foundation Shell.
// Modified by Dave Batton on Apr 13, 2005
//   Added the new "variation" parameter to support Pref buttons.
// ----------------------------------------------------

C_PICTURE:C286($0;$rolloverImage_pic)
C_TEXT:C284($1;$2;$3;$iconName_t;$buttonLabel_t;$rolloverImageName_t;$variation_t)

$iconName_t:=$1
$buttonLabel_t:=$2

If (Count parameters:C259>=3)
	$variation_t:=$3
Else 
	$variation_t:=""
End if 

Fnd_Bttn_Init

$iconName_t:=Fnd_Bttn_GetSizedIconName($iconName_t)

$rolloverImageName_t:=Fnd_Bttn_GetPictureName($iconName_t;$buttonLabel_t;$variation_t)
$rolloverImage_pic:=Fnd_Bttn_Cache_Image_Get($rolloverImageName_t)

If (Picture size:C356($rolloverImage_pic)=0)
	//  The picture hasn't already been created, so create it.  
	$rolloverImage_pic:=Fnd_Bttn_MakeButton($iconName_t;$buttonLabel_t;$variation_t)
	
	// Add the new image to the cache.
	Fnd_Bttn_Cache_Image_Add($rolloverImageName_t;$rolloverImage_pic)
End if 

// Return the picture.
$0:=$rolloverImage_pic