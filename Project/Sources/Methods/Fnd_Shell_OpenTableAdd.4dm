//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_OpenTableAdd (->table{; ->table2...})

  // Adds the table(s) to the list of tables displayed in the Open Table dialog.
  // Tables are displayed in the order they are passed to this method.

  // Access: Shared

  // Parameters: 
  //   $1..$N : Pointer : The table to add

  // Returns: Nothing

  // Created by Dave Batton on Mar 23, 2004
  // ----------------------------------------------------

C_POINTER:C301(${1})
C_LONGINT:C283($firstElement_i;$element_i;$parameter_i)

Fnd_Shell_Init 

$firstElement_i:=Size of array:C274(Fnd_Shell_TablePtrsArray_aptr)+1

INSERT IN ARRAY:C227(Fnd_Shell_TablePtrsArray_aptr;$firstElement_i;Count parameters:C259)
INSERT IN ARRAY:C227(Fnd_Shell_TableNamesArray_at;$firstElement_i;Count parameters:C259)

For ($parameter_i;1;Count parameters:C259)
	$element_i:=$firstElement_i+$parameter_i-1
	Fnd_Shell_TablePtrsArray_aptr{$element_i}:=${$parameter_i}
	Fnd_Shell_TableNamesArray_at{$element_i}:=Fnd_VS_TableName (${$parameter_i})
End for 
