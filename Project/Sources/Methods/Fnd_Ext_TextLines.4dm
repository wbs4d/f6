//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Ext_TextLines
// $lines_i:=Fnd_Ext_TextLines (Fnd_Dlg_Text1_t;372;◊Fnd_Dlg_FontName_t;◊Fnd_Dlg_FontSize_i;◊Fnd_Dlg_FontStyle_i)

// Replaces functionality of plugin Extras

// Access Type: Public

// Parameters: $widthOfObject_i;◊Fnd_Dlg_FontName_t;◊Fnd_Dlg_FontSize_i;◊Fnd_Dlg_FontStyle_i

// Returns:
//   $0 : Long Integer : number of lines of text

// Created by Walt Nelson on 10/27/10
// 120125: wwn modified to do away with 4D Chart which is deprecated in v13
// 2021-05-22 WBS : Remove call to Fnd_SVG_GetStringWidth which doesn't account for embedded carraiage returns
// ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1)
C_LONGINT:C283($2)
C_TEXT:C284($3)
C_LONGINT:C283($4)
C_LONGINT:C283($5)
ARRAY TEXT:C222($Temp_at; 0)

C_LONGINT:C283($Fnd_Bttn_FontSize_i; $Fnd_Bttn_FontStyle_i; $lengthOfText_i; $linesOfText_i; $widthOfObject_i)
C_TEXT:C284($theText_t; $Fnd_Bttn_FontName_t)

If (False:C215)
	C_LONGINT:C283(Fnd_Ext_TextLines; $0)
	C_TEXT:C284(Fnd_Ext_TextLines; $1)
	C_LONGINT:C283(Fnd_Ext_TextLines; $2)
	C_TEXT:C284(Fnd_Ext_TextLines; $3)
	C_LONGINT:C283(Fnd_Ext_TextLines; $4)
	C_LONGINT:C283(Fnd_Ext_TextLines; $5)
End if 


$theText_t:=$1
$widthOfObject_i:=$2
$Fnd_Bttn_FontName_t:=$3
$Fnd_Bttn_FontSize_i:=$4
$Fnd_Bttn_FontStyle_i:=$5


TEXT TO ARRAY:C1149($theText_t; $Temp_at; $widthOfObject_i; $Fnd_Bttn_FontName_t; $Fnd_Bttn_FontSize_i; $Fnd_Bttn_FontStyle_i)
$linesOfText_i:=Size of array:C274($Temp_at)


$0:=$linesOfText_i

// End of code