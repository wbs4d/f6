//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_UseLowercase ({use Lowercase?}) --> Boolean

  // Pass True to tell the password generator to use lowercase letters in passwords.
  // Returns the current setting.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : True to use lowercase letters (optional)

  // Returns: 
  //   $0 : Boolean : True if lowercase letters can be used

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_UseLowercase_b:=$1
End if 

$0:=<>Fnd_Pswd_UseLowercase_b
