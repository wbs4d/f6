//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_SelectTable --> Pointer

  // Displays a choice list of available tables.  Returns a pointer to the
  //   selected table (if any).  Sets the OK variable to 1 if a table is selected.

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Pointer : A pointer to the selected table

  // Created by Dave Batton on Sep 2, 2003
  // Modified by Dave Batton on Jan 1, 2005
  //   The list of tables is now sorted alphabetically.
  // Modified by Dave Batton on May 28, 2005
  //   Updated to use the new Fnd_Wnd methods.
  // Modified by Guy Algot on (07/02/09)
  //   Allow the choice list window to be resized.
  // ----------------------------------------------------

C_POINTER:C301($0;$table_ptr)
C_LONGINT:C283($element_i)
C_TEXT:C284($prompt_t)

  // We can assume Fnd_Shell_Init has been called by one of our protected methods. 

  // If the developer didn't specify the tables in the Fnd_aa_Shell_OpenTable
  //   hook, use all of the visible tables.
If (Size of array:C274(Fnd_Shell_TablePtrsArray_aptr)=0)
	Fnd_VS_GetTables (->Fnd_Shell_TablePtrsArray_aptr;->Fnd_Shell_TableNamesArray_at)
	SORT ARRAY:C229(Fnd_Shell_TableNamesArray_at;Fnd_Shell_TablePtrsArray_aptr;>)
End if 

Fnd_Wnd_Title (Fnd_Loc_GetString ("Fnd_Shell";"OpenTableWindowTitle"))
$prompt_t:=Fnd_Loc_GetString ("Fnd_Shell";"OpenTablePrompt")

Fnd_Wnd_Type (Movable dialog box:K34:7)  //  Guy Algot (07/02/09) this allows the choice list window to be resized

$element_i:=Fnd_List_ChoiceList ($prompt_t;->Fnd_Shell_TableNamesArray_at)

If ($element_i>0)  // OK isn't set since we called another component.
	$table_ptr:=Fnd_Shell_TablePtrsArray_aptr{$element_i}
End if 

  // Clear the arrays.  We're done with them.
ARRAY POINTER:C280(Fnd_Shell_TablePtrsArray_aptr;0)
ARRAY TEXT:C222(Fnd_Shell_TableNamesArray_at;0)

$0:=$table_ptr
