//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_SetIcon ({icon number})

  // Sets the icon for the upcoming alert.
  //   0 = Default icon (Note)
  //   1 = Note Icon
  //   2 = Warn Icon
  //   3 = Stop Icon

  // These Foundation constants are available:
  //   Fnd_Dlg_NoteIcon = Note Icon
  //   Fnd_Dlg_WarnIcon = Warn Icon
  //   Fnd_Dlg_StopIcon = Stop Icon

  // See also Fnd_Dlg_CustomIcon.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Icon number (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 4, 2003
  // Modified by Dave Batton on May 15, 2005
  //   Rewritten to save the Picture Library icon name rather than the number.
  // ----------------------------------------------------

C_LONGINT:C283($1;$iconNumber_i)

Fnd_Dlg_Init 

Case of 
	: (Count parameters:C259=0)
		Fnd_Dlg_IconPictureName_t:="Fnd_Dlg_OSX_NoteIcon"
		
	: ($1=Fnd_Dlg_NoteIcon)
		Fnd_Dlg_IconPictureName_t:="Fnd_Dlg_OSX_NoteIcon"
		
	: ($1=Fnd_Dlg_WarnIcon)
		Fnd_Dlg_IconPictureName_t:="Fnd_Dlg_OSX_WarnIcon"
		
	: ($1=Fnd_Dlg_StopIcon)
		Fnd_Dlg_IconPictureName_t:="Fnd_Dlg_OSX_StopIcon"
		
	Else 
		$iconNumber_i:=0  // (or 1, but 0 means the user didn't set it) Default - Note icon
End case 
