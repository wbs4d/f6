//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_MakeTextPicture (button label) --> Picture

  // Creates a picture of the specified label.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The label for the button

  // Returns: 
  //   $0 : Picture : A picture of the label (x4)

  // Created by Mark Mitchenall on 18/9/01
  //   Original Method Name: rollover_MakeTextPicture
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // ----------------------------------------------------

C_TEXT:C284($1;$buttonText_t)
C_PICTURE:C286($0;$combinedText_pic;$singleText_pic;$singleTextDown_pic;$singleTextDisabled_pic)
C_LONGINT:C283($index_i;$width_i;$height_i)

$buttonText_t:=$1

If (Fnd_Bttn_LabelPlacement_i#None:K36:11)
	  //Determine the size of the button text.
	$width_i:=Fnd_Ext_TextWidth ($buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i)
	$width_i:=$width_i+Fnd_Bttn_TextRightPadding_i
	
	  //$height_i:=Fnd_CT_Get_FontHeight (Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i)
	  // Modified by: Walt Nelson (1/21/11) replace with Wayne's way
	$height_i:=Fnd_SVG_GetStringHeight ($buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i)
	  // Modified by: Walt Nelson (11/20/10)
	$height_i:=$height_i+Fnd_Bttn_TextBottomPadding_i
	
	  //Create the normal version of the text in the default colour
	$singleText_pic:=Fnd_Bttn_TextToPict ($buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i;$width_i;$height_i)
	$singleTextDisabled_pic:=Fnd_Bttn_TextToPict ($buttonText_t;Fnd_Bttn_FontName_t;Fnd_Bttn_FontSize_i;Fnd_Bttn_FontStyle_i;$width_i;$height_i;0x00888888)  //Create the disabled version of the text which is 50% grey
	
	Case of 
		: (Fnd_Bttn_TextDownStyle_i=Bold:K14:2)
			COMBINE PICTURES:C987($singleTextDown_pic;$singleTextDisabled_pic;Superimposition:K61:10;$singleText_pic;0;-1)
		Else 
			$singleTextDown_pic:=$singleText_pic  // Plain
	End case 
	
	  //Arrange the normal text in the picture.
	$combinedText_pic:=$singleText_pic
	TRANSFORM PICTURE:C988($combinedText_pic;Translate:K61:3;Fnd_Bttn_HorizonalShift_ai{1};Fnd_Bttn_VerticalShift_ai{1})
	
	COMBINE PICTURES:C987($combinedText_pic;$combinedText_pic;Superimposition:K61:10;$singleTextDown_pic;Fnd_Bttn_HorizonalShift_ai{2};((Fnd_Bttn_ButtonHeight_i*1)+(Fnd_Bttn_VerticalShift_ai{2})))
	COMBINE PICTURES:C987($combinedText_pic;$combinedText_pic;Superimposition:K61:10;$singleText_pic;Fnd_Bttn_HorizonalShift_ai{3};((Fnd_Bttn_ButtonHeight_i*2)+(Fnd_Bttn_VerticalShift_ai{3})))
	COMBINE PICTURES:C987($combinedText_pic;$combinedText_pic;Superimposition:K61:10;$singleTextDisabled_pic;Fnd_Bttn_HorizonalShift_ai{4};((Fnd_Bttn_ButtonHeight_i*3)+(Fnd_Bttn_VerticalShift_ai{4})))
	
Else 
	$combinedText_pic:=$combinedText_pic*0  // No labels = no picture.
End if 

$0:=$combinedText_pic