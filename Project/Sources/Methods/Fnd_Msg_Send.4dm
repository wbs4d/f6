//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_Send (target process; message; {->blob})

  // Call this method to send a message to another process.  

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The process to send the message to
  //   $1 : Text : The message to send
  //   $1 : Blob : A blob (optional)

  // Returns: Nothing

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_LONGINT:C283($1;$targetProcessNumber_i;$element_i;$otherElement_i;$backgroundProcessNumber_i;$processState_i;$processTime_i)
C_TEXT:C284($2;$textMessage_t;$processName_t)
C_POINTER:C301($3)

$targetProcessNumber_i:=$1
$textMessage_t:=$2

Fnd_Msg_Init 

PROCESS PROPERTIES:C336($targetProcessNumber_i;$processName_t;$processState_i;$processTime_i)

$backgroundProcessNumber_i:=Process number:C372(<>Fnd_Msg_BackgroundProcessName_t)

  // Don't bother sending messages to processes that don't exist, or to the
  //   message loop process (that just causes problems).
If (($targetProcessNumber_i>0) & ($targetProcessNumber_i#$backgroundProcessNumber_i))
	  // Make sure no other process is messing with the message arrays.
	If (Not:C34(Semaphore:C143(<>Fnd_Msg_SemaphoreName_t;300)))  // Wait up to 5 seconds if the semaphore already exists    
		
		If (<>Fnd_Msg_NextMsgSlot_i>Size of array:C274(<>Fnd_Msg_MsgActiveFlags_ab))
			<>Fnd_Msg_NextMsgSlot_i:=1
		End if 
		
		$element_i:=Find in array:C230(<>Fnd_Msg_MsgActiveFlags_ab;False:C215;<>Fnd_Msg_NextMsgSlot_i)
		<>Fnd_Msg_NextMsgSlot_i:=<>Fnd_Msg_NextMsgSlot_i+1  // For the next time throught the loop.
		
		If ($element_i=-1)  // No free array elements - create a bunch of new ones.
			$element_i:=Size of array:C274(<>Fnd_Msg_MsgToProcNums_ai)+1
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgFromProcNums_ai;$element_i;5)
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgToProcNums_ai;$element_i;5)
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgActiveFlags_ab;$element_i;5)
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgTexts_at;$element_i;5)
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgBlobs_apic;$element_i;5)
			INSERT IN ARRAY:C227(<>Fnd_Msg_MsgEndTime_ai;$element_i;5)
			
			For ($otherElement_i;$element_i+1;$element_i+4)  // Initialize the four we're not about to use.
				<>Fnd_Msg_MsgActiveFlags_ab{$otherElement_i}:=False:C215
			End for 
		End if 
		
		  // Add the message
		<>Fnd_Msg_MsgFromProcNums_ai{$element_i}:=Current process:C322
		<>Fnd_Msg_MsgToProcNums_ai{$element_i}:=$targetProcessNumber_i
		<>Fnd_Msg_MsgActiveFlags_ab{$element_i}:=True:C214  // Gets set to false when the message is received.
		<>Fnd_Msg_MsgTexts_at{$element_i}:=$textMessage_t
		If (Count parameters:C259>=3)
			Fnd_Msg_BlobToPicture ($3;-><>Fnd_Msg_MsgBlobs_apic{$element_i})
		End if 
		<>Fnd_Msg_MsgEndTime_ai{$element_i}:=Tickcount:C458+<>Fnd_Msg_TimeOutTicks_i
		
		CLEAR SEMAPHORE:C144(<>Fnd_Msg_SemaphoreName_t)
		
		  // Update the message monitor dialog.
		  // If (Application type#4D Server )
		  //   CALL PROCESS(Process number(◊Fnd_kMsgMonProcessName))
		  // End if 
		
		  // Wake up the message handling loop.
		If ($backgroundProcessNumber_i>0)
			RESUME PROCESS:C320($backgroundProcessNumber_i)
		Else 
			$backgroundProcessNumber_i:=New process:C317("Fnd_Msg_BackgroundProcess";Fnd_Gen_DefaultStackSize;<>Fnd_Msg_BackgroundProcessName_t;*)
		End if 
		
	Else   // The semaphore never cleared.
		Fnd_Gen_BugAlert ("Fnd_Msg_Send";"The semaphore was not properly cleared.")
	End if 
	
End if 