//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_SelectionChanged

// Call this whenever the current selection of records displayed in the output list changes.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 30, 2003
// Modified by Dave Batton on Feb 20, 2004
//   Added a call to Fnd_Grid_Update_Display if the Grid component is installed.
// Modified by Mike Erickson on Sep 28, 2005
//   Added a call to AL_TBL_UpdateArea if the AL_TBL component is installed.
// Modified by Dave Batton on Mar 3, 2006
//   Added a call to Fnd_Out_Update if the Fnd_Out component is installed.
// Modified by Mike Erickson on Sep 14, 2006
//   Added a call to Fnd_aa_IO_SelectionChanged before the AL_TBL_UpdateArea call.
//Modified by Mike Erickson on Sept 30, 2006
//   Added a call to AL_TBL_SetupSelection before Fnd_aa_IO_SelectionChanged.
// Mod by Wayne Stewart, (2022-04-21) - Removed irrelevant code
// ----------------------------------------------------

var $es : Object

Case of 
		
	: (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_SelectionChanged")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_IO_SelectionChanged";*)
		End if 
		$es:=Create entity selection:C1512(Fnd_Gen_CurrentTable_ptr->)
		Form:C1466.listData:=$es.copy()
		Fnd_Out_Update
		
		
	Else 
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_SelectionChanged")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_IO_SelectionChanged";*)
		End if 
End case 
