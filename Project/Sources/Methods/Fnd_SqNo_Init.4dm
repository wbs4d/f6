//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_SqNo_Init

// Initializes both the process and interprocess variables used by the
//   Foundation Sequence Number routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 5, 2003
// Modified by Dave Batton on Jan 30, 2004
//   Updated the version number.
//   Removed the unused Fnd_SqNo_Initialized_b references.
// Modified by Dave Batton on May 20, 2004
//   Updated the version number to 4.0.3.
//   Removed the check for the Localization component.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_SqNo_Info method.
// Modified by Dave Batton on Feb 28, 2006
//   Fixed the two calls to Fnd_Gen_ComponentCheck where the component was
//     identifying itself as "Fnd_Reg" instead of "Fnd_SqNo".
//   Added table and field pointer variables so the table can be removed from the component.
//   Increased the semaphore time-out to 15 seconds.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_SqNo_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_SqNo_Initialized_b))  // So we only do this once.
	Compiler_Fnd_SqNo
	
	// Modified by: Wayne Stewart (27/02/09)
	Fnd_SqNo_CreateTable  //  Will now create the Sequences table if it isn't there already 
	
	// Set up pointers to the structure. The table we use is not installed with the component.
	<>Fnd_SqNo_Table_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]")
	<>Fnd_SqNo_IDFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]ID"; Is longint:K8:6)
	<>Fnd_SqNo_GroupNameFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]Group_Name"; Is alpha field:K8:1; True:C214)
	<>Fnd_SqNo_NextNumberFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]Next_Number"; Is longint:K8:6)
	<>Fnd_SqNo_RecycleBinFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]Recycle_Bin"; Is BLOB:K8:12)
	<>Fnd_SqNo_DesignerOnlyFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_SqNo"; "[Fnd_SqNo]Designer_Only"; Is boolean:K8:9; True:C214)
	
	Fnd_SqNo_Localize
	
	<>Fnd_SqNo_Semaphore_t:="Fnd_SqNo_Semaphore"
	<>Fnd_SqNo_Timeout_i:=15*60  // DB060323 - Wait up to 15 seconds for a new sequence number.
	
	<>Fnd_SqNo_Initialized_b:=True:C214
End if 
