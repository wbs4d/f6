//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reg_RegDialogFormMethod

// We call this from a protected method because a public form can't
//   call private methods.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 16, 2003
// ----------------------------------------------------

Case of 
	: (Form event code:C388=On Load:K2:1)
		// Set the window title and main message.
		SET WINDOW TITLE:C213(Fnd_Gen_GetString("Fnd_Reg"; "RegWindowTitle"))
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Menu"))
		//EXECUTE METHOD("Fnd_Menu_DisableAll"; *)
		//End if 
		
		Fnd_Menu_DisableAll
		
		<>Fnd_Reg_Message1_t:=Fnd_Gen_GetString("Fnd_Reg"; "RegMessage"; Fnd_Reg_FeatureName(Fnd_Reg_DialogFeatureCode_t))
		
		// Set the field label texts.
		<>Fnd_Reg_Label1_t:=Fnd_Gen_GetString("Fnd_Reg"; "RegUserNameLabel")
		<>Fnd_Reg_Label2_t:=Fnd_Gen_GetString("Fnd_Reg"; "RegUnlockCodeLabel")
		
		//If (Fnd_Gen_ComponentAvailable("Fnd_Loc"))
		//EXECUTE METHOD("Fnd_Loc_FixLabelWidths"; *; -><>Fnd_Reg_Label1_t; -><>Fnd_Reg_UserName_t; -><>Fnd_Reg_Label2_t; -><>Fnd_Reg_UnlockCode_t)
		//End if 
		Fnd_Loc_FixLabelWidths(-><>Fnd_Reg_Label1_t; -><>Fnd_Reg_UserName_t; -><>Fnd_Reg_Label2_t; -><>Fnd_Reg_UnlockCode_t)
		
		// Set the default values for the fields.
		<>Fnd_Reg_UserName_t:=""
		<>Fnd_Reg_UnlockCode_t:=""
		
		// Set the button texts and move is necessary.
		Fnd_Gen_ButtonText(-><>Fnd_Reg_OKButton_i; Fnd_Gen_GetString("Fnd_Reg"; "RegOKButton"); Align right:K42:4; -><>Fnd_Reg_CancelButton_i)
		Fnd_Gen_ButtonText(-><>Fnd_Reg_CancelButton_i; Fnd_Gen_GetString("Fnd_Reg"; "RegCancelButton"); Align right:K42:4)
		
		<>Fnd_Reg_BuyNowURL_t:=Fnd_Reg_BuyNowURL(Fnd_Reg_DialogFeatureCode_t)
		If (<>Fnd_Reg_BuyNowURL_t="")
			OBJECT SET VISIBLE:C603(<>Fnd_Reg_BuyNowButton_i; False:C215)
		Else 
			Fnd_Gen_ButtonText(-><>Fnd_Reg_BuyNowButton_i; Fnd_Gen_GetString("Fnd_Reg"; "RegBuyNowButton"); Align left:K42:2)
		End if 
End case 

Fnd_Gen_FormMethod

