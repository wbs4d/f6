//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_SortOrderEditor

// Displays 4D's Order By editor.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 30, 2003
// Modified by Dave Batton on Mar 30, 2004
//   Now uses Fnd_Gen_CurrentTable rather than Current form table.
//   No longer does anything(including cause errors) if the pointer is nil.
// Modified by Mike Erickson on Sep 30 2005
//   Added a hook for ASG's QueryPack component.
// Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
// ----------------------------------------------------

C_POINTER:C301($table_ptr)
C_BOOLEAN:C305($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

GET AUTOMATIC RELATIONS:C899($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

$table_ptr:=Fnd_Gen_CurrentTable

If (Not:C34(Is nil pointer:C315($table_ptr)))  // DB040330
	SET AUTOMATIC RELATIONS:C310(True:C214;True:C214)
	
	If (Fnd_Gen_ComponentInfo("Fnd_Out";"state")="active")
		Fnd_Out_SynchroniseSelection
		ORDER BY:C49($table_ptr->)
		Form:C1466.listData:=Create entity selection:C1512(Fnd_Gen_CurrentTable->).copy()
		Fnd_Out_Update
	Else 
		ORDER BY:C49($table_ptr->)
	End if 
	
	SET AUTOMATIC RELATIONS:C310($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)
End if 
