//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_List_Init

// Initializes both the process and interprocess variables used by the Fnd_List routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 2, 2003
// Modified by Dave Batton on Mar 20, 2004
//   Added a variable for the semaphore name.
//   Updated the version.
// Modified by Dave Batton on May 20, 2004
//   Updated the version number to 4.0.3.
//   Removed the check for the Localization component.
//   Moved the version number to the new Fnd_List_Info method.
// Modified by Dave Batton on Apr 5, 2005
//   Initializes the new Choice List button variable.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// Mod by Wayne Stewart, (2019-11-13) - Modify to use Fnd.List object
// Mod by Wayne Stewart, 2023-07-26
//   Moved the test for the semaphore to inside the initialisation routine
//   previously the semaphore was set and unset every time Fnd_XXX_Init is called
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_List_Initialized_b)

If (Not:C34(<>Fnd_List_Initialized_b))  // So we only do this once.
	
	<>Fnd_List_Semaphore_t:="$Fnd_List_Semaphore"
	<>Fnd_List_SemaphoreTimeout_i:=30*60  // DB060323 - Added - 30 seconds.
	
	If (Not:C34(Semaphore:C143(<>Fnd_List_Semaphore_t; <>Fnd_List_SemaphoreTimeout_i)))
		
		Compiler_Fnd_List
		
		
		// Modified by: Wayne Stewart (27/02/09)
		Fnd_List_CreateTable
		
		<>Fnd_List_Table_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_List"; "[Fnd_List]")
		<>Fnd_List_IDFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_List"; "[Fnd_List]ID"; Is longint:K8:6)
		<>Fnd_List_ListNameFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_List"; "[Fnd_List]List_Name"; Is alpha field:K8:1; True:C214)
		<>Fnd_List_ListBlobFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_List"; "[Fnd_List]List_Blob"; Is BLOB:K8:12)
		
		Fnd_List_Localize
		
		<>Fnd_List_InfoName_t:="Fnd_List_LastExit"  // The name of the list we add to the structure.
		CLEAR SEMAPHORE:C144(<>Fnd_List_Semaphore_t)
		
	Else 
		Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
	End if 
	
	<>Fnd_List_Initialized_b:=True:C214
End if 


If (Fnd.List.Initialised=Null:C1517)  // So we only do this once per process.
	Compiler_Fnd_List
	
	Fnd.List:=New object:C1471(\
		"Initialised"; True:C214; \
		"NewButtonLabel"; ""; \
		"useTypeAhead"; False:C215; \
		"newButton"; 0; \
		"CancelButton"; 0; \
		"OKButton"; 0)
	
	ARRAY TEXT:C222(Fnd_List_Commands_at; 0)
	ARRAY TEXT:C222(Fnd_List_Methods_at; 0)
	
End if 
