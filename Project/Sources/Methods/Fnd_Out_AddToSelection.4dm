//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_AddToSelection (record number)

// Called from Fnd_Rec_AddToSelection

// Access Type: Private

// Parameters: 
//   $1 : Longint : Record number

// Returns: Nothing

// Created by Dave Batton on Mar 2, 2006
// Mod by Wayne Stewart, (2022-04-21) - Rewritten for ORDA
// ----------------------------------------------------

#DECLARE($recordNumber_i : Integer)

var $row_i : Integer
var $currentTable_ptr : Pointer
var $wasReadOnly_b : Boolean
var $es;$thisRecord_e : Object

Fnd_Out_Init

$currentTable_ptr:=Table:C252(Fnd.out.currentTable)

If ($recordNumber_i#No current record:K29:2)
	$wasReadOnly_b:=Read only state:C362($currentTable_ptr->)
	READ ONLY:C145($currentTable_ptr->)
	GOTO RECORD:C242($currentTable_ptr->;$recordNumber_i)
	
	$thisRecord_e:=Create entity selection:C1512($currentTable_ptr->).first()
	
	$es:=Form:C1466.listData
	
	If (Not:C34($es.contains($thisRecord_e)))
		$es:=$es.copy()
		$es.add($thisRecord_e)
		Form:C1466.listData:=$es
	End if 
	
	Fnd_Out_Update
	
	$row_i:=$thisRecord_e.indexOf($es)+1
	
	LISTBOX SELECT ROW:C912(*;"Fnd_Out_ListBox";$row_i;lk replace selection:K53:1)
	
	Fnd_Out_UpdateAutoSizeColumns
	
	If (Not:C34($wasReadOnly_b))
		READ WRITE:C146($currentTable_ptr->)
	End if 
End if 