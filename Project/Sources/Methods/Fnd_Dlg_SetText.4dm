//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Dlg_SetText (text 1{; text 2})


// Sets the message texts for the upcoming alert.


// Parameters: 

//   $1 : Text : The primary alert text

//   $2 : Text : The smaller alert text (optional)


// Returns: Nothing


// Created by Dave Batton on Jul 4, 2003

// ----------------------------------------------------


C_TEXT:C284($1; $2)

Fnd_Dlg_Init

Fnd_Dlg_Text1_t:=$1

If (Count parameters:C259>=2)
	Fnd_Dlg_Text2_t:=$2
Else 
	Fnd_Dlg_Text2_t:=""
End if 
