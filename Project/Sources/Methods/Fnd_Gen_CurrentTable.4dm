//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_CurrentTable ({->table}) --> Pointer

// Returns a pointer to the current table. If no table is current, a nil
//   pointer is returned.
// Pass a pointer to a table to this routine to make it the current table.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the table to make current (optional)

// Returns: 
//   $0 : Pointer : The current table

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Jan 24, 2004
//   Now returns a pointer to the current form table or the current trigger
//   table if no other table has been specified.
// ----------------------------------------------------

C_POINTER:C301($0;$1)
C_LONGINT:C283($event_i;$table_i;$record_i)

Fnd_Gen_Init

If (Count parameters:C259>=1)
	Fnd_Gen_CurrentTable_ptr:=$1
End if 

// If we don't have a valid pointer, use the Current form table (if there is one).
If (Is nil pointer:C315(Fnd_Gen_CurrentTable_ptr))
	Fnd_Gen_CurrentTable_ptr:=Current form table:C627
End if 

// If we still don't have a valid pointer, use the triggger table (if there is one).
If (Is nil pointer:C315(Fnd_Gen_CurrentTable_ptr))
	If (Trigger level:C398>0)
		TRIGGER PROPERTIES:C399(Trigger level:C398;$event_i;$table_i;$record_i)
		Fnd_Gen_CurrentTable_ptr:=Table:C252($table_i)
	End if 
End if 

$0:=Fnd_Gen_CurrentTable_ptr
