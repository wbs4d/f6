//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_GeneratePassword --> Text

  // Generates a password.

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Text : The generated password

  // Created by Dave Batton on Mar 16, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$availableChars_t;$availableChars_t;$password_t)
C_LONGINT:C283($length_i;$position_i;$character_i)

Fnd_Pswd_Init 

  // Get a string that contains all of the valid characters.
$availableChars_t:=Fnd_Pswd_GetValidCharacters 

If ($availableChars_t="")
	$password_t:=""
	
Else 
	  // Pick a length for the new password.
	$length_i:=(Random:C100%(<>Fnd_Pswd_MaxLength_i-<>Fnd_Pswd_MinLength_i+1))+<>Fnd_Pswd_MinLength_i
	
	  // Create the password.
	$password_t:=""
	For ($character_i;1;$length_i)
		$position_i:=(Random:C100%(Length:C16($availableChars_t)))+1
		$password_t:=$password_t+$availableChars_t[[$position_i]]
	End for 
End if 

$0:=$password_t
