//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Loc_DuplicateList

// Duplicates a 4D list.  Used for creating new localizations.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 16, 2003
// Modified by : Vincent Tournier (06/17/2011)
// ----------------------------------------------------


C_TEXT:C284($listName_t;$newName_t)
ARRAY TEXT:C222(<>Fnd_Loc_TempArray_at;0)  // Modification by : Vincent TOURNIER(9/12/11)

$listName_t:=Request:C163("Name of List to Duplicate:";"Fnd_Find_EN")

If (OK=1)
	// BEGIN Modified by : Vincent Tournier(17/6/2011)
	// LIST TO ARRAY($listName_t;◊Fnd_Loc_TempArray_at)
	Fnd_Loc_ListToArray($listName_t;-><>Fnd_Loc_TempArray_at)
	// END Modified by : Vincent Tournier(17/6/2011)
	
	If (Size of array:C274(<>Fnd_Loc_TempArray_at)>0)
		$newName_t:=Request:C163("New List Name:";$listName_t)
		
		If (OK=1)
			
			// BEGIN Modified by : Vincent Tournier(17/6/2011)
			// ARRAY TO LIST(◊Fnd_Loc_TempArray_at;$newName_t)
			Fnd_Loc_ArrayToList(-><>Fnd_Loc_TempArray_at;$newName_t)
			// LIST TO ARRAY($listName_t;◊Fnd_Loc_TempArray_at)
			Fnd_Loc_ListToArray($listName_t;-><>Fnd_Loc_TempArray_at)
			// END Modified by : Vincent Tournier(17/6/2011)
			
			If (Size of array:C274(<>Fnd_Loc_TempArray_at)>0)
				ALERT:C41("The list has been created.")
			Else 
				ALERT:C41("Unable to create the list.")
			End if 
		End if 
		
	Else 
		ALERT:C41("The list named \""+$listName_t+"\" could not be found.")
	End if 
End if 
