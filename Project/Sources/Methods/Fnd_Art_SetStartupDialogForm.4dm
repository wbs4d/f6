//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Art_SetStartupDialogForm (form name) -> form name

  // Allows the developer to specify the form to display during startup.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the form

  // Returns: Nothing

  // Created by Dave Batton on Dec 15, 2003
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Corrected parameter list in header
  // Mod by Wayne Stewart, (2019-10-26) - Fnd-6
  // Also now returns name
  // ----------------------------------------------------

C_TEXT:C284($1;$0)

Fnd_Art_Init 

If (Count parameters:C259=1)
	Use (Storage:C1525.Fnd.Art)
		Storage:C1525.Fnd.Art.StartupFormName:=$1
	End use 
End if 

$0:=Storage:C1525.Fnd.StartupFormName


