//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_SetLocalizationStrings (component code; ->lookup codes; ->strings)

  // Allows a component to use its Fnd4-style localization strings.
  // Similar to Fnd_Loc_LoadStringList, but designed to work with Fnd5 components.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The component code (e.g.: "Fnd_Menu")
  //   $2 : Pointer : A text array of translation lookup codes
  //   $3 : Pointer : A text array of localized strings

  // Returns: Nothing

  // Created by Dave Batton on Sep 25, 2007
  // ----------------------------------------------------

C_TEXT:C284($1;$component_t)
C_POINTER:C301($2;$3;$lookupCodes_ptr;$strings_ptr)
C_LONGINT:C283($element_i)

$component_t:=$1
$lookupCodes_ptr:=$2
$strings_ptr:=$3

Fnd_Loc_Init 

$element_i:=Find in array:C230(<>Fnd_Loc_Modules_at;$component_t)
If ($element_i=-1)
	$element_i:=Size of array:C274(<>Fnd_Loc_Modules_at)+1
	INSERT IN ARRAY:C227(<>Fnd_Loc_Modules_at;$element_i)
	INSERT IN ARRAY:C227(<>Fnd_Loc_LookupCodes_at;$element_i)
	INSERT IN ARRAY:C227(<>Fnd_Loc_Strings_at;$element_i)
End if 

<>Fnd_Loc_Modules_at{$element_i}:=$component_t
COPY ARRAY:C226($lookupCodes_ptr->;<>Fnd_Loc_LookupCodes_at{$element_i})
COPY ARRAY:C226($strings_ptr->;<>Fnd_Loc_Strings_at{$element_i})

If ((Size of array:C274(<>Fnd_Loc_Strings_at{$element_i})=0) | (Size of array:C274(<>Fnd_Loc_Strings_at{$element_i})#Size of array:C274(<>Fnd_Loc_LookupCodes_at{$element_i})))
	Fnd_Gen_BugAlert (Current method name:C684;"There is a problem with the \""+$component_t+"\" localization list.")
End if 
