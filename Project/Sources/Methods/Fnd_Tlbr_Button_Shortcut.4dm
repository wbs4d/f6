//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Button_Shortcut (buttonName; shortcutKey)

  // Associates a Command key shortcut with the button.

  // Access: Private

  // Parameters: 
  //   $1 : Text : A button name
  //   $2 : Text : The shortcut letter to assign

  // Returns: nothing

  // Created by Mark Mitchenall on 11/2/02
  // Toolbar Component - © mitchenall.com 2002
  // Modified by Dave Batton on Jan 6, 2005
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$buttonName_t;$shortcutKey_t)
C_LONGINT:C283($buttonNumber)

$buttonName_t:=$1
$shortcutKey_t:=$2

Fnd_Tlbr_Init 

$buttonNumber:=Fnd_Tlbr_Object_GetNumber ($buttonName_t)

If ($buttonNumber>0)
	Fnd_Tlbr_ObjectShortcuts_at{$buttonNumber}:=$shortcutKey_t
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"Button name not found: "+$buttonName_t)
End if 