//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_MakeMenuIndicatorPic --> Picture

  // Creates a picture of the specified label.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 16, 2005
  // Modified: [1] John Craig, 02/07/2014, 13:53:11 Changed | picture operator to COMBINE PICTURES with Superimposition method
  // ----------------------------------------------------

C_PICTURE:C286($0)

C_PICTURE:C286($indicator_pic;$menuDisabled_pic;$menuEnabled_pic)
C_TEXT:C284($cacheName_t;$ImageName_t;$Path_t)

  //Check to see if the image is the in cache.
$cacheName_t:=Fnd_Bttn_GetPictureName ("menuIndicator";Fnd_Bttn_Style_t;Fnd_Bttn_Platform_t)
$indicator_pic:=Fnd_Bttn_Cache_Image_Get ($cacheName_t)

If (Picture size:C356($indicator_pic)=0)
	  // Mod by Wayne Stewart, 2016-11-24
	  //  Now retrieve images from the resources folder
	$Path_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Fnd_Tlbr_Images"+Folder separator:K24:12
	
	$ImageName_t:="Fnd_Bttn_Menu_"+Fnd_Bttn_Platform_t+"_Enabled.png"
	READ PICTURE FILE:C678($Path_t+$ImageName_t;$menuEnabled_pic)
	
	$ImageName_t:="Fnd_Bttn_Menu_"+Fnd_Bttn_Platform_t+"_Disabled.png"
	READ PICTURE FILE:C678($Path_t+$ImageName_t;$menuDisabled_pic)
	
	COMBINE PICTURES:C987($indicator_pic;$indicator_pic;Superimposition:K61:10;$menuEnabled_pic;Fnd_Bttn_HorizonalShift_ai{1};Fnd_Bttn_VerticalShift_ai{1})
	COMBINE PICTURES:C987($indicator_pic;$indicator_pic;Superimposition:K61:10;$menuEnabled_pic;Fnd_Bttn_HorizonalShift_ai{2};(Fnd_Bttn_ButtonHeight_i*1)+(Fnd_Bttn_VerticalShift_ai{2}))
	COMBINE PICTURES:C987($indicator_pic;$indicator_pic;Superimposition:K61:10;$menuEnabled_pic;Fnd_Bttn_HorizonalShift_ai{3};(Fnd_Bttn_ButtonHeight_i*2)+(Fnd_Bttn_VerticalShift_ai{3}))
	COMBINE PICTURES:C987($indicator_pic;$indicator_pic;Superimposition:K61:10;$menuDisabled_pic;Fnd_Bttn_HorizonalShift_ai{4};(Fnd_Bttn_ButtonHeight_i*3)+(Fnd_Bttn_VerticalShift_ai{4}))
	  // Modified: [1] John Craig, 02/07/2014, 13:53:11 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
	
	  // Add the new image to the cache.
	Fnd_Bttn_Cache_Image_Add ($cacheName_t;$indicator_pic)
End if 

$0:=$indicator_pic