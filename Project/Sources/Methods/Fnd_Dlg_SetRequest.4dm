//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_SetRequest ({default})

  // Sets up the next dialog to allow the user to enter a value.  Optionally sets
  //   the default value of the value.  Pass "*" as the default value to cause
  //   the entry area to display bullets rather than text.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The default value (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 10, 2003
  // ----------------------------------------------------

C_TEXT:C284($1)

Fnd_Dlg_Init 

Fnd_Dlg_RequestDefault_t:="**"  // Leave it blank.
If (Count parameters:C259>=1)
	If ($1#"")
		Fnd_Dlg_RequestDefault_t:=$1
	End if 
End if 
