//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_AddButton (label)

  // Allows the developer to add an additional button to the Choice List dialog.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The new button's label

  // Returns: Nothing

  // Created by Dave Batton on Apr 5, 2005

  // Mod by Wayne Stewart, (2019-11-13) - Modify to use Fnd.List object

  // ----------------------------------------------------

C_TEXT:C284($1)

Fnd_List_Init 

Fnd.List.NewButtonLabel:=$1

