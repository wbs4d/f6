//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: ProgUpdate (number)

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.
  // Part of the Foundation Compatibility component.

  // Parameters: 
  //   $1 : Longint : The current progress

  // Returns: Nothing

  // Created by Dave Batton on Jul 27, 2003
  // ----------------------------------------------------

C_LONGINT:C283($1)

Fnd_Cmpt_Init 

Fnd_Dlg_MessageUpdate ($1)

If (Fnd_Dlg_MessageCanceled )
	Fnd_Cmpt_SetBooleanValue ("ThermoAbort";True:C214)
End if 
