//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_UpdateToolbar

// Enables and disables the toolbar buttons based on whether or not
//   the window is frontmost, the selection of records, and the highlighted
//   records.

// Method Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 25, 2005
// ----------------------------------------------------

Fnd_FCS_Trace

C_BOOLEAN:C305($noSelection_b;$noUserSet_b)
C_LONGINT:C283($recordsInSelection_i;$highlightedRecords_i;$recordsInTable_i)
C_TEXT:C284($recordsInSelection_t;$highlightedRecords_t;$recordsInTable_t)

$recordsInSelection_i:=Form:C1466.listData.length
$highlightedRecords_i:=Form:C1466.selectedItems.length
$recordsInTable_i:=ds:C1482[Table name:C256(Form:C1466.tableNum)].all().length

//$recordsInSelection_i:=Size of array(Fnd_Out_RecIDs_ai)
//$highlightedRecords_i:=Count in array(Fnd_Out_Listbox_ab;True)

If ($recordsInSelection_i=0)
	$noSelection_b:=True:C214
	$noUserSet_b:=True:C214
Else 
	$noSelection_b:=False:C215
	If ($highlightedRecords_i=0)
		$noUserSet_b:=True:C214
	Else 
		$noUserSet_b:=False:C215
	End if 
End if 

// Set the output buttons.
Fnd_Tlbr_Button_Enabled("Delete";($noUserSet_b=False:C215))
Fnd_Tlbr_Button_Enabled("Sort";($noSelection_b=False:C215))

// Update the status message under the toolbar.
$highlightedRecords_t:=String:C10($highlightedRecords_i)
$recordsInSelection_t:=String:C10($recordsInSelection_i)
$recordsInTable_t:=String:C10($recordsInTable_i)

Fnd_Tlbr_StatusMessage(Fnd_Loc_GetString("Fnd_IO";"SelectionDescription";\
$highlightedRecords_t;\
$recordsInSelection_t;\
$recordsInTable_t))

