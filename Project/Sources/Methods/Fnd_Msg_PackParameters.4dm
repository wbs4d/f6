//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_PackParameters (msg; param 1 {... param N}) --> Text

  // Adds the text parameters to the messasge using the same delimiter expected by
  //   the Fnd_Msg_GetParameter routine.  This isn't rocket science, it's just
  //   easier to maintain than creating the message string in multiple methods.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The message to append parameter to
  //   $2..$n : Text : The parameters to add to the message

  // Returns: 
  //   $0 : Text : The delimited paramters

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1;${2};$message_t)
C_LONGINT:C283($parameter_i)

Fnd_Msg_Init 

$message_t:=$1

For ($parameter_i;2;Count parameters:C259)
	$message_t:=$message_t+<>Fnd_Msg_ParamDelimiter_t+${$parameter_i}
End for 

$0:=$message_t