//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Shell_ExcludeFromQuit (process name)

  // Tells Foundation not to worry about the specified process when trying to quit.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the process

  // Returns: Nothing

  // Created by Dave Batton on Sep 17, 2003
  // Modified by Dave Batton on Jun 13, 2004
  //   Removed the call to Fnd_Shell_Init.
  // ----------------------------------------------------

C_TEXT:C284($1;$processName_t)
C_BOOLEAN:C305(<>Fnd_Shell_NoQuitInitialized_b)
C_LONGINT:C283($element_i)

$processName_t:=$1

  // We don't call Fnd_Shell_Init here, because this might be called from a local process
  //   when the shell component isn't initialized, and that causes all kinds of problems.

  // But we still need to make sure this array is initialized.
If (Not:C34(<>Fnd_Shell_NoQuitInitialized_b))
	<>Fnd_Shell_NoQuitInitialized_b:=True:C214
	ARRAY TEXT:C222(<>Fnd_Shell_NoQuitProcessNames_at;0)
End if 

$element_i:=Find in array:C230(<>Fnd_Shell_NoQuitProcessNames_at;$processName_t)
If ($element_i=-1)
	$element_i:=Size of array:C274(<>Fnd_Shell_NoQuitProcessNames_at)+1
	INSERT IN ARRAY:C227(<>Fnd_Shell_NoQuitProcessNames_at;$element_i)
End if 

<>Fnd_Shell_NoQuitProcessNames_at{$element_i}:=$processName_t
