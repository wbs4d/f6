//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_IsInDesignGroup --> Boolean

  // Returns True if the current user is the Designer or in a group
  //    with a name that implies he's a designer.

  // Access: Private

  // Parameters: None

  // Returns: 
  //   $0 : Boolean : True if the user is a Designer

  // Created by Dave Batton on Nov 19, 2003
  // Modified by Dave Batton on Jun 16, 2004
  //   Localized the "Designer" current user name.
  //   Simplified the Case statement by using wildcards.
  // Modified by Gary Boudreaux on Mar 2, 2009
  //  Use localized Lists to check for Designer
  //  Add entry to 3 Lists: Fnd_SqNo, Fnd_SqNo_EN, Fnd_SqNo_FR to define the words for Designer
  // ----------------------------------------------------

  // Add some localized group names.

C_BOOLEAN:C305($0)

Case of 
	: (Current user:C182=Fnd_Gen_GetString ("Fnd_SqNo";"Designer"))  //Gary Boudreaux - Mar 2, 2009  `was Get indexed string(133;1))  ` DB040616
		$0:=True:C214
		
	: (User in group:C338(Current user:C182;"Design @"))
		$0:=True:C214
		
	: (User in group:C338(Current user:C182;"Development @"))
		$0:=True:C214
		
	Else 
		$0:=False:C215
End case 
