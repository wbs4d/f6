//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_TextToBase64 (text) --> Text

  // Converts the text to Base64.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text to encode

  // Returns: 
  //   $0 : Text : The encoded text

  // Created by Dave Batton on Apr 10, 2005
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

TEXT TO BLOB:C554($1;Fnd_Text_TempBlob_blob;Mac text without length:K22:10)

Fnd_Text_EncodeBase64Blob (->Fnd_Text_TempBlob_blob)

$0:=BLOB to text:C555(Fnd_Text_TempBlob_blob;Mac text without length:K22:10)