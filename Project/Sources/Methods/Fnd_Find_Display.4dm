//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_Display

// Displays a Find dialog and performs the query.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by David Robbins many years ago.
// Modified by Dave Batton on Jun 25, 2003.
// Modified by Dave Batton on Jan 24, 2004
//   Now uses Fnd_VS_TableName if the VS component is available.
//   No longer does anything (including cause errors) if the pointer is nil.
// Modified by Dave Batton on May 30, 2004
//   Changed the Fnd_Loc_GetString calls to Fnd_Gen_GetString calls.
// Modified by Dave Batton on Nov 6, 2004
//   Now saves and retrieves information from preferences if possible.
// Modified by Dave Batton on Jan 15, 2005
//   Moved the window setting commands into the Fnd_Init method so they
//      can be overridden by the developer before calling this command.
// Modified by Dave Batton on May 13, 2005
//   Moved a few lines of code aorund to fix a problem with resetting the previously used field name.
// Modified by Dave Batton on Jun 22, 2005
//   Moved the initialization of Fnd_Find_SearchString_t into the Find dialog's On Load phase.
//Modified by Mike Erickson on Oct 4 2005
//   Added a hook for ASG's QueryPack component.
// Modified by Dave Batton on Mar 3, 2006
//   Added support for the Fnd_Out component.
// Modified by Dave Batton on May 14, 2006
//   Set manipulation is now done for the "Search Selection" option. It isn't needed for Foundation's
//   search routines, but I realized it should be done for custom searches, since the other set
//   manipulations are being done for custom searches.
// Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable call before Fnd_VS

// ----------------------------------------------------

C_LONGINT:C283($width_i; $height_i)
C_BOOLEAN:C305($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)

Fnd_Find_Init

If (Not:C34(Is nil pointer:C315(Fnd_Find_Table_ptr)))  // DB040330
	
	If (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active")  // If we are using Fnd_Out make certain the current selection matches the entity selection
		Fnd_Out_SynchroniseSelection
	End if 
	
	// Add the default search fields if the developer didn't add any.
	If (Size of array:C274(Fnd_Find_Fields_aptr)=0)
		Fnd_Find_AddTable(Fnd_Find_Table_ptr)  // Load arrays with field pointers and names.
	End if 
	
	Fnd_Find_TableName_t:=Fnd_VS_TableName(Fnd_Find_Table_ptr)  // WBS 2021-05-05 Remove call to Fnd_Gen_ComponentAvailable
	
	FORM GET PROPERTIES:C674("Fnd_Find_Dialog"; $width_i; $height_i)  // DB050512 - Moved this.
	$width_i:=600  // The real default or minimum size.
	$width_i:=$width_i+Fnd_Find_GetExtraWidth
	
	Fnd_Find_FieldNames_at:=Abs:C99(Find in array:C230(Fnd_Find_FieldNames_at; Fnd_Find_PrefsGet("Fnd_Find: "+Fnd_Find_TableName_t+" Search Field")))  // DB041106 - Added
	
	Fnd_Find_Label_t:=Fnd_Gen_GetString("Fnd_Find"; "FindDialogLabel"; Fnd_Find_TableName_t)
	// DB050115 - Moved the window setting commands from here into the Fnd_Init routine.
	
	Fnd_Wnd_OpenWindow($width_i; $height_i)
	
	DIALOG:C40("Fnd_Find_Dialog")
	CLOSE WINDOW:C154
	
	If (OK=1)
		If ((Fnd_Find_AllRadioButton_i#1) & (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active"))  // DB060303
			EXECUTE FORMULA:C63("Fnd_Out_SyncronizeSelection")  // We need to synchronize first for every action except for a completely new search.
		End if 
		
		Case of 
			: (Fnd_Find_EditorButton_i=1)
				If (Fnd_Find_EditorButtonMethod_t="")  // If the developer hasn't overriden the default behavior.
					GET AUTOMATIC RELATIONS:C899($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
					SET AUTOMATIC RELATIONS:C310(True:C214; True:C214)
					QUERY:C277(Fnd_Find_Table_ptr->)
					SET AUTOMATIC RELATIONS:C310($one_b; $many_b)  // Modified by: Walt Nelson (2/23/10)
				Else 
					EXECUTE FORMULA:C63(Fnd_Find_EditorButtonMethod_t)
				End if 
				
			: ((Fnd_Find_AllRadioButton_i=1) | (Fnd_Find_AddRadioButton_i=1))  // Search entire table or add to selection
				If (Fnd_Find_AddRadioButton_i=1)  // Add to selection.
					CREATE SET:C116(Fnd_Find_Table_ptr->; "Fnd_Find_CurrentSelection")  // Save current selection
				End if 
				Fnd_Find_Engine("Query")
				Fnd_Find_Now("Query")
				If (Fnd_Find_AddRadioButton_i=1)  // Add to selection.
					CREATE SET:C116(Fnd_Find_Table_ptr->; "Fnd_Find_AddSelection")  // Save new selection
					UNION:C120("Fnd_Find_AddSelection"; "Fnd_Find_CurrentSelection"; "Fnd_Find_AddSelection")
					USE SET:C118("Fnd_Find_AddSelection")
					CLEAR SET:C117("Fnd_Find_AddSelection")
					CLEAR SET:C117("Fnd_Find_CurrentSelection")
				End if 
				
			: ((Fnd_Find_SelectionRadioButton_i=1) | (Fnd_Find_OmitRadioButton_i=1))  // Search in selection or omit from selection
				// If (Fnd_Find_OmitRadioButton_i=1)  ` Omit from selection. ` DB060514 - We now do the set manipluation with both of these radio buttons.
				CREATE SET:C116(Fnd_Find_Table_ptr->; "Fnd_Find_CurrentSelection")  // Save current selection
				// End if
				Fnd_Find_Engine("Query Selection")
				Fnd_Find_Now("Query Selection")
				CREATE SET:C116(Fnd_Find_Table_ptr->; "Fnd_Find_FoundSelection")  // Save new selection `DB060514 - Changed the set name.
				Case of 
					: (Fnd_Find_OmitRadioButton_i=1)  // Omit from selection.
						DIFFERENCE:C122("Fnd_Find_CurrentSelection"; "Fnd_Find_FoundSelection"; "Fnd_Find_CurrentSelection")
					: (Fnd_Find_SelectionRadioButton_i=1)  // DB060514 - Added this to better support custom search methods.
						INTERSECTION:C121("Fnd_Find_CurrentSelection"; "Fnd_Find_FoundSelection"; "Fnd_Find_CurrentSelection")
				End case 
				USE SET:C118("Fnd_Find_CurrentSelection")
				CLEAR SET:C117("Fnd_Find_FoundSelection")
				CLEAR SET:C117("Fnd_Find_CurrentSelection")
		End case 
		
		Fnd_Gen_SelectionChanged  // Always call this after changing the selection.
		
		// Save find preferences for this table.
		Fnd_Find_PrefsPut("Fnd_Find: "+Fnd_Find_TableName_t+" Search Field"; Fnd_Find_FieldNames_at{Fnd_Find_FieldNames_at})  // DB041106 - Added
		Fnd_Find_PrefsPut("Fnd_Find: "+Fnd_Find_TableName_t+" Search Operator"; Fnd_Find_OperatorStrs_at{Fnd_Find_OperatorStrs_at})  // DB041106 - Added
		Fnd_Find_PrefsPut("Fnd_Find: "+Fnd_Find_TableName_t+" Search Value"; Fnd_Find_SearchString_t)  // DB041106 - Added
		//Fnd_Find_PrefsPut ("Search Option";String(Fnd_Find_AllRadioButton_i+(Fnd_Find_SelectionRadioButton_i*2)+(Fnd_Find_AddRadioButton_i*3)+(Fnd_Find_OmitRadioButton_i*4)))  ` Saves radio button selection as 1, 2, 3 or 4.
	End if   // (OK=1)
	
End if 

Fnd_Find_Initialized_b:=False:C215  // So the arrays get set up again next time.
