//%attributes = {"invisible":true,"shared":true}
/* Fnd_Wnd_Cancel
Project Method: Fnd_Wnd_Cancel

Calls CANCEL command, useful from CALL FORM

Access: Shared

Created by Wayne Stewart (2019-10-26)
     waynestewart@mac.com
*/

CANCEL:C270