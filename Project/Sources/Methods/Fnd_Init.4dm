//%attributes = {"invisible":true,"shared":true}
/* Fnd_Init

   Project Method: Fnd_Init

   Initialises the Fnd objects

   Access: Shared

   Created by Wayne Stewart (2019-10-26)
       waynestewart@mac.com
*/
C_LONGINT:C283($pid_i)

Compiler_Fnd

// Create the Storage object if it doesn't exist
If (Storage:C1525.Fnd.Initialized=Null:C1517)
	Use (Storage:C1525)
		Storage:C1525.Fnd:=New shared object:C1526("Initialized"; False:C215)
	End use 
End if 

// Now initialise it
If (Not:C34(Storage:C1525.Fnd.Initialized))
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Initialized:=True:C214
	End use 
End if 

// We also check for the fonts here as well, however it must run a new process
If (Storage:C1525.font=Null:C1517)
	$pid_i:=New process:C317("Fnd_FontDiscovery"; 0; "Fnd_FontDiscovery")
End if 

// Now the object for this process
If (Fnd.Initialized=Null:C1517)
	If (Fnd=Null:C1517)
		Fnd:=New object:C1471("Initialized"; True:C214)
	End if 
End if 



