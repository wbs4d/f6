//%attributes = {"invisible":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_Validate (user name; activation code) --> Boolean

  // Returns True if a valid activation code was passed for the specified
  //   product number and user name.

  // Method Type: Private

  // Parameters: 
  //   $1 : Text : The user's name
  //   $2 : Text : The entered activation code

  // Returns: 
  //   $0 : Boolean : True if the user name and activation code are valid for this product.

  // Created by Dave Batton on Mar 14, 2003
  // Modified by Dave Batton on Sep 30, 2006
  //   Fixed an error caused by Mac OS X 10.4.8 on Intel Macs.
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$valid_b)
C_TEXT:C284($1;$2;$productCode_t;$activationCode_t;$userName_t;$enteredActivationCode_t)
C_LONGINT:C283($position_i;$start_i)
C_REAL:C285($checksum_r)
$productCode_t:="dede7CTe"
$userName_t:=$1
$enteredActivationCode_t:=$2

$valid_b:=False:C215  // Assume failure.

$activationCode_t:=$productCode_t+$userName_t

$checksum_r:=0
For ($position_i;1;Length:C16($activationCode_t))
	$checksum_r:=$checksum_r+(Character code:C91($activationCode_t[[$position_i]])*(2^$position_i))
End for 

$activationCode_t:=String:C10($checksum_r)

  // $start_i:=(16-Length($activationCode_t))\2  ` DB060930 - Might return the wrong value on Mac OS X 10.4.8 with an Intel chip.
$start_i:=Int:C8((16-Length:C16($activationCode_t))/2)  // DB060930 - The  fix for the line above.

$activationCode_t:=Change string:C234("720392641536";$activationCode_t;$start_i)
$activationCode_t:=Insert string:C231($activationCode_t;"-";5)
$activationCode_t:=Insert string:C231($activationCode_t;"-";10)

$0:=($enteredActivationCode_t=$activationCode_t)
