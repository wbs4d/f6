//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_ReplaceString (old string; new string)

  // Does a find and replace in all of the virtual table titles and field titles.
  // Designed specifically to quickly replace underscores with spaces, but 
  //   could also be used for other global changes.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The string to replace
  //   $2 : Text : The string to replace it with

  // Returns: Nothing

  // Created by Dave Batton on Jan 24, 2004
  // [1] Modified by: John Craig (5/13/2009) - Modifications to avoid issues with deleted tables and fields
  // Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES & SET FIELD TITLES
  // Modified by: wayne Stewart (25 May 11) - will no longer try to place an initial space in a field or table name


  // ----------------------------------------------------

C_TEXT:C284($1;$2;$old_t;$new_t;$Prefix_t;$Suffix_t)
C_LONGINT:C283($tableNumber_i;$fieldNumber_i)
C_LONGINT:C283($tableElement_i;$fieldElement_i)
C_BOOLEAN:C305($ReplaceWithSpace_b)

$old_t:=$1
$new_t:=$2

Fnd_VS_Init 

$ReplaceWithSpace_b:=($new_t=" ")

If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
	For ($tableNumber_i;1;Get last table number:C254)
		If (Is table number valid:C999($tableNumber_i))
			$tableElement_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;$tableNumber_i)  // [1] Modified by: John Craig (5/13/2009)
			If ($tableElement_i>-1)  // [1]
				If ($ReplaceWithSpace_b) & (Substring:C12(<>Fnd_VS_TableNames_at{$tableElement_i};1;1)=$old_t)  // Modified by: wayne Stewart (25 May 11),  If we are trying to replace first char with space
					$Prefix_t:=Substring:C12(<>Fnd_VS_TableNames_at{$tableElement_i};1;1)  //                       Preserve the first character
					$Suffix_t:=Substring:C12(<>Fnd_VS_TableNames_at{$tableElement_i};2;MAXTEXTLENBEFOREV11:K35:3)  //     Get the rest of the string
					$Suffix_t:=Replace string:C233($Suffix_t;$old_t;$new_t)  //                                       Then do the substituion
					<>Fnd_VS_TableNames_at{$tableElement_i}:=$Prefix_t+$Suffix_t
				Else 
					<>Fnd_VS_TableNames_at{$tableElement_i}:=Replace string:C233(<>Fnd_VS_TableNames_at{$tableElement_i};$old_t;$new_t)  // [1] $tableElement_i replaces $tableNumber_i
				End if 
				
				For ($fieldNumber_i;1;Get last field number:C255($tableNumber_i))
					If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))  // [1]
						$fieldElement_i:=Find in array:C230(<>Fnd_VS_FieldNumbers_ai{$tableElement_i};$fieldNumber_i)  // [1] $fieldElement_i replaces $fieldNumber_i
						If ($fieldElement_i>-1)  // [1]
							If ($ReplaceWithSpace_b) & (Substring:C12(<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i};1;1)=$old_t)  // Modified by: wayne Stewart (25/05/11)
								$Prefix_t:=Substring:C12(<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i};1;1)  //                       Preserve the first character
								$Suffix_t:=Substring:C12(<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i};2;MAXTEXTLENBEFOREV11:K35:3)  //     Get the rest of the string
								$Suffix_t:=Replace string:C233($Suffix_t;$old_t;$new_t)  //                                       Then do the substituion
								<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i}:=$Prefix_t+$Suffix_t
							Else 
								<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i}:=Replace string:C233(<>Fnd_VS_FieldNames_at{$tableElement_i}{$fieldElement_i};$old_t;$new_t)  // [1]
							End if 
						End if   // [1]
					End if   // [1]
				End for 
				SET FIELD TITLES:C602((Table:C252($tableNumber_i))->;<>Fnd_VS_FieldNames_at{$tableElement_i};<>Fnd_VS_FieldNumbers_ai{$tableElement_i};*)  // [1] WN100211 - added *
			End if 
		End if 
	End for 
	SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at;<>Fnd_VS_TableNumbers_ai;*)  // WN100211 - added *
	
	CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
End if 
