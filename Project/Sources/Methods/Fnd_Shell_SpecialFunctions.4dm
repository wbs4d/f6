//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_SpecialFunctions

// Displays Foundation's Special Functions dialog.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 29, 2003
// Modified by: Walt Nelson (12/17/10) - added Fnd_Gen_MenuBar to Fnd_Shell_Administration to fix bug with grayed out Menu after calling
// File->Special Functions on Win XP - Paul Norwood
// ----------------------------------------------------

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_SpecialFunctions")=1)
	// Set our window title.  The developer can override this in the hook.
	Fnd_Wnd_Title(Fnd_Loc_GetString("Fnd_Shell";"SpecialFunctionsWindowTitle"))
	
	// Clear the list entries, and let the developer add their stuff through the hook.
	Fnd_List_Clear
	
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_SpecialFunctions";*)
	
	
Else 
	Fnd_Dlg_Alert(Fnd_Loc_GetString("Fnd_Shell";"NoSpecialFunctions"))
End if 

Fnd_Gen_MenuBar  // Walt Nelson (2/4/10)