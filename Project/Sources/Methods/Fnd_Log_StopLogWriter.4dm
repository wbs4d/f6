//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_StopLogWriter

// Call this method to stop the Log Writer worker
// The worker will restart if needed automatically

// Access: Shared

// Created by Wayne Stewart (2018-08-06)
//     waynestewart@mac.com
// Mod by Wayne Stewart, (2021-08-10) - Close logs here now rather than in terminating method
// ----------------------------------------------------
C_LONGINT:C283($1)

If (False:C215)
	C_LONGINT:C283(Fnd_Log_StopLogWriter; $1)
End if 

If (Count parameters:C259=1)
	Fnd_Log_CloseLog2  // Close the logs
	TimetoDie  // Once that's done kill the worker
	
Else 
	CALL WORKER:C1389(Fnd_Log_Writer; "Fnd_Log_StopLogWriter"; 0)
End if 



