//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_List_TypeAhead (CharacterTyped)

// Multi-character type-ahead on choice list

// Parameters: 
//   $1 : Text : The character just typed

// Created by Wayne Stewart (2019-11-12)
//     waynestewart@mac.com
// ----------------------------------------------------

Fnd_List_Init

C_TEXT:C284($1)

C_LONGINT:C283($ArrayPosition_i; $asciiValue_i; $SortColumn_i; $TypeOfSortedArray_i)
C_POINTER:C301($SortedArray_Ptr; $Header_Ptr)
C_TEXT:C284($Character_t)

If (False:C215)
	C_TEXT:C284(Fnd_List_TypeAhead; $1)
End if 

If (Fnd.List.useTypeAhead)
	
	If (Form:C1466.TypeAheadString=Null:C1517)
		Form:C1466.TypeAheadString:=""
	End if 
	
	Form:C1466.TypeAheadString:=Form:C1466.TypeAheadString+$1  // Add this character to the existing type-ahead buffer.
	$Character_t:=Uppercase:C13(Form:C1466.TypeAheadString)  //  Makes no sense for searching but when I am decrementing ascii values it will be a time saver
	
	Repeat 
		$ArrayPosition_i:=Find in array:C230(Fnd_List_ChoiceArray1_at; $Character_t+"@")
		If (Length:C16($Character_t)=1)  // DB060319 - Added
			$asciiValue_i:=Character code:C91($Character_t)-1
			$Character_t:=Char:C90($asciiValue_i)
		Else 
			$asciiValue_i:=49  // Not used, but tested below.
			$Character_t:=Substring:C12($Character_t; 1; Length:C16($Character_t)-1)  // Try just the first letter if we couldn't match all letters.
		End if 
	Until (($ArrayPosition_i>0) | ($asciiValue_i<48))  //  i.e. 0
	
	If ($ArrayPosition_i>0)
		LISTBOX SELECT ROW:C912(*; "Fnd_List_ListBox"; $ArrayPosition_i; lk replace selection:K53:1)  // DB060319 - Added
		OBJECT SET SCROLL POSITION:C906(*; "Fnd_List_ListBox"; $ArrayPosition_i)  // DB060319 - Added
	End if 
	
	SET TIMER:C645(60)  // Clears the type-ahead buffer after one second of inactivity. See SLCT_FormMethod.
	
	
End if 



