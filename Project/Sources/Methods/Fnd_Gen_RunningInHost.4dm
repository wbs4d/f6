//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_RunningInHost

  // Global and IP variables accessed:
C_BOOLEAN:C305(<>Fnd_Gen_RunningInHost_b)

If (False:C215)
	C_BOOLEAN:C305(Fnd_Gen_RunningInHost ;$1;$0)
End if 

  // Method Type:    Protected

  // Parameters:
C_BOOLEAN:C305($1)

  // Local Variables:     None Used

  // Returns:
C_BOOLEAN:C305($0)

  // Created by Wayne Stewart 2009-03-02T00:00:00
  //     waynestewart@mac.com
  // ----------------------------------------------------

Fnd_Gen_Init 

If (Count parameters:C259=1)
	<>Fnd_Gen_RunningInHost_b:=$1
End if 

$0:=<>Fnd_Gen_RunningInHost_b

