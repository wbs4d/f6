//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_DisplayRecord3 (record number)

// Similar to Fnd_IO_DisplayRecord2, but assumes we're displaying the record
//   in the current process rather than in a new one.

// Called from Fnd_IO_DisplayRecord.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 20, 2004
// Modified by Dave Batton on Dec 10, 2004
//   We now save and restore the output window title here.
//   The last record viewed by the user is now highlighted in the output list.
// Modified by Dave Batton on Jan 25, 2005
//   Modified so multiple new records will be added if Fnd_IO_AddMultipleRecords_b is True.
// Modified by Dave Batton on Feb 23, 2005
//   Added additional support for the Grid component.
//   Now updates the Window menu after returning from the input form.
// Modified by Dave Batton on Apr 22, 2005
//   This method will now display the record even if it's in read-only mode.
// ----------------------------------------------------

C_LONGINT:C283($1;$recordNumber_i)
C_TEXT:C284($windowTitle_t)

$recordNumber_i:=$1

Fnd_Gen_CurrentFormType(Fnd_Gen_InputForm)

Fnd_Gen_MenuBar  // Update the menu bar.

$windowTitle_t:=Get window title:C450  // DB041210 - So we can restore it.
SET WINDOW TITLE:C213(Fnd_IO_GetWindowTitle(Fnd_Gen_CurrentTable;$recordNumber_i))


// The developer hook. Note that you can't set the form in the
//   hook - it's too late.  Set it in Fnd_Hook_IO_DisplayTable.
If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_DisplayRecord")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_IO_DisplayRecord";*)
End if 

CREATE EMPTY SET:C140(Fnd_Gen_CurrentTable->;"Fnd_IO_HighlightSet")

If ($recordNumber_i=New record:K29:1)
	CREATE SET:C116(Fnd_Gen_CurrentTable->;"Fnd_IO_OriginalSet")
	Repeat   // DB050124 - Added this loop.
		ADD RECORD:C56(Fnd_Gen_CurrentTable->;*)
		ADD TO SET:C119(Fnd_Gen_CurrentTable->;"Fnd_IO_OriginalSet")
		ADD TO SET:C119(Fnd_Gen_CurrentTable->;"Fnd_IO_HighlightSet")
	Until ((OK=0) | (Not:C34(Fnd_IO_AddMultipleRecords_b)))
	USE SET:C118("Fnd_IO_OriginalSet")
	CLEAR SET:C117("Fnd_IO_OriginalSet")
Else 
	If ((Locked:C147(Fnd_Gen_CurrentTable->)) | (Read only state:C362(Fnd_Gen_CurrentTable->)))  // DB050422 - Added this to display locked records.
		DIALOG:C40(Fnd_Gen_CurrentTable->;Fnd_IO_InputFormName_t)
	Else 
		MODIFY RECORD:C57(Fnd_Gen_CurrentTable->;*)
		ADD TO SET:C119(Fnd_Gen_CurrentTable->;"Fnd_IO_HighlightSet")
	End if 
End if 

SET WINDOW TITLE:C213($windowTitle_t)  // DB041210 - Added
Fnd_Menu_Window_Add  // DB050223 - Added

HIGHLIGHT RECORDS:C656("Fnd_IO_HighlightSet")
CLEAR SET:C117("Fnd_IO_HighlightSet")
