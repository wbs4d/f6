//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_ShowTable (->table)

  // Adds a table to the virtual structure arrays.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the table to let the user see

  // Returns: Nothing

  // Created by Dave Batton on Mar 4, 2004
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Added comment below to question whether an If test is correct
  // Modified by: Walt Nelson (2/11/10) - added * to SET TABLE TITLES & SET FIELD TITLES
  // Modification : Vincent TOURNIER(12/9/11)
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($element_i;$nbrValidTables_i)  // Modification : Vincent TOURNIER(9/12/11)-added$nbrValidTables_i

$table_ptr:=$1

Fnd_VS_Init 

If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
	  // If all of the tables are currently included in the array, then this is the
	  // first time this routine has been called.  Hide all of the tables so we
	  // can add them back in one at a time.
	  // GB20081221: is this test valid in light of possible deleted tables?
	
	  // BEGIN Modification : Vincent TOURNIER(9/12/11)
	$nbrValidTables_i:=0
	For ($element_i;1;Get last table number:C254)
		If (Is table number valid:C999($element_i))
			$nbrValidTables_i:=$nbrValidTables_i+1
		End if 
	End for 
	  // END Modification : Vincent TOURNIER(9/12/11)
	
	If (Size of array:C274(<>Fnd_VS_TableNumbers_ai)=$nbrValidTables_i)  // Modification : Vincent TOURNIER(9/12/11)
		ARRAY TEXT:C222(<>Fnd_VS_TableNames_at;0)
		ARRAY INTEGER:C220(<>Fnd_VS_TableNumbers_ai;0)
		ARRAY TEXT:C222(<>Fnd_VS_FieldNames_at;0;0)
		ARRAY INTEGER:C220(<>Fnd_VS_FieldNumbers_ai;0;0)
		  // BEGIN Modification : Vincent TOURNIER(9/12/11)
		ARRAY TEXT:C222(<>Fnd_VS_HostFieldNames_at;0)
		ARRAY TEXT:C222(<>Fnd_VS_ExistFieldNames_at;0)
		ARRAY TEXT:C222(<>Fnd_VS_TmpFieldsNames_at;0)
		  // END Modification : Vincent TOURNIER(9/12/11)
	End if 
	
	Fnd_VS_AddTable ($table_ptr)
	
	SET TABLE TITLES:C601(<>Fnd_VS_TableNames_at;<>Fnd_VS_TableNumbers_ai;*)  // WN100211-added*
	
	$element_i:=Find in array:C230(<>Fnd_VS_TableNumbers_ai;Table:C252($table_ptr))
	If ($element_i>0)
		SET FIELD TITLES:C602($table_ptr->;<>Fnd_VS_FieldNames_at{$element_i};<>Fnd_VS_FieldNumbers_ai{$element_i};*)  // WN100211-added*
	End if 
	
	CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
End if 