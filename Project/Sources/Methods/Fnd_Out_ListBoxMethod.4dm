//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_ListBoxMethod

//

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 3, 2005
// ----------------------------------------------------


var $es : Object
var $formEvent_i; $recordCount_i : Integer

$formEvent_i:=Form event code:C388

Case of 
		
		
	: ($formEvent_i=On Selection Change:K2:29)
		Fnd_Out_UpdateToolbar
		Fnd_Menu_MenuBar
		
	: ($formEvent_i=On Double Clicked:K2:5)
		$es:=Form:C1466.listData
		USE ENTITY SELECTION:C1513($es)
		GOTO SELECTED RECORD:C245(Fnd_Gen_CurrentTable_ptr->; Form:C1466.currentItemPosition)
		Fnd_IO_DisplayRecord(Fnd_Gen_CurrentTable_ptr)
		UNLOAD RECORD:C212(Fnd_Gen_CurrentTable_ptr->)
		
End case 
