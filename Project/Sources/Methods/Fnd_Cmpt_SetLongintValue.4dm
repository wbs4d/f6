//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Cmpt_SetLongintValue (variable name; longint value)

// Allows us to set a Longint variable without actually using the variable name
//   in this component.

// Access: Shared

// Parameters: 
//   $1 : Text : The name of the variable
//   $2 : Longint : The value to set it to

// Returns: Nothing

// Created by Dave Batton on Feb 22, 2004
// Foundation 6 version by Wayne Stewart on 2021-03-26
// ----------------------------------------------------

C_TEXT:C284($1; $variableName_t)
C_LONGINT:C283($2)

$variableName_t:=$1

Fnd_Cmpt_Init

Fnd.Cmpt.Longint:=$2

EXECUTE FORMULA:C63($variableName_t+":=Fnd.Cmpt.Longint")
