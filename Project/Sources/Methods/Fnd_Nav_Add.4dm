//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_Add (label) --> Number

// Adds a button with the specified label to the navigation palette arrays.
// If a button with that label already exists, no new button is added.
// Assumes the semaphore work is handled by the calling method.
// Returns the number of the new or found button.

// Access: Private

// Parameters: 
//   $1 : Text : The new button's label

// Returns: 
//   $0 : Longint : The new button's number

// Created by Dave Batton on Jul 1, 2004
// ----------------------------------------------------

C_LONGINT:C283($0; $element_i)
C_TEXT:C284($1; $label_t)
C_POINTER:C301($null_ptr)

$label_t:=$1

$element_i:=Find in array:C230(<>Fnd_Nav_Labels_at; $label_t)

If ($element_i=-1)
	$element_i:=Size of array:C274(<>Fnd_Nav_Labels_at)+1
	INSERT IN ARRAY:C227(<>Fnd_Nav_Labels_at; $element_i)
	INSERT IN ARRAY:C227(<>Fnd_Nav_TablePtrs_aptr; $element_i)
	INSERT IN ARRAY:C227(<>Fnd_Nav_Methods_at; $element_i)
	INSERT IN ARRAY:C227(<>Fnd_Nav_ForegroundColors_ai; $element_i)
	INSERT IN ARRAY:C227(<>Fnd_Nav_BackgroundColors_ai; $element_i)
	<>Fnd_Nav_TablePtrs_aptr{$element_i}:=$null_ptr
	<>Fnd_Nav_Methods_at{$element_i}:=""
	<>Fnd_Nav_ForegroundColors_ai{$element_i}:=Foreground color:K23:1
	<>Fnd_Nav_BackgroundColors_ai{$element_i}:=Background color:K23:2
End if 

<>Fnd_Nav_Labels_at{$element_i}:=$label_t

<>Fnd_Nav_NeedsRedraw_b:=True:C214
POST OUTSIDE CALL:C329(Process number:C372(Storage:C1525.Fnd.Nav.ProcessName))

$0:=$element_i