//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Date_EntryFilter (->date field or variable)

// Interprets what has been entered as a date
//    And checks to see if a "function" key is hit and acts accordingly

// Place in the object method of a date field or variable:
//     Fnd_Date_EntryFilter(Self)

// Required: Enable the field's On Before Keystroke and On Data Change events. 

// Access: Shared

// Parameters: 
//   $1 : Pointer : The date entry field

// Returns: Nothing

// Created by Tom Dillon, DataCraft
// Modified by Dave Batton on Apr 28, 2004
// Modified by Dave Batton on Apr 13, 2005
//   The date modified by the keyboard commands is now based on the displayed date, not the current date.
//   Now supports 'n' for the middle of the moNth.
// Modified by Dave Batton on Aug 12, 2005
//   Fixed a minor bug. Was assigning the start date to the real date.
// Modified by Dave Batton on Jan 11, 2007
//   With 4D 2003.8 and 4D 2004.x, 'Get edited text' no longer returns the
//     edited text with date fields (but it works fine with date variables). Added
//     a work-around.
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_POINTER:C301($1; $dateField_ptr)
C_DATE:C307($startDate_d)
C_TEXT:C284($keystroke_t)

$dateField_ptr:=$1

Fnd_Date_Init

Case of 
	: (Form event code:C388=On After Keystroke:K2:26)
		Fnd.Date.EntryFilterValue:=Get edited text:C655
		
		
	: (Form event code:C388=On Before Keystroke:K2:6)
		Fnd.Date.EntryFilterValue:=""  // DB070111 - If we need this value we'll get an On After Keystoke even to add it.
		$keystroke_t:=Keystroke:C390
		
		$startDate_d:=Fnd_Date_StringToDate(Get edited text:C655)  // DB070111 - No longer works like it used to with 4D 2003.7.
		If ($startDate_d=!00-00-00!)  // DB050812 - Fixed.
			$startDate_d:=Current date:C33  // DB050812 - Fixed.
		End if 
		
		
		Case of 
				// Check to see if a "function" key is hit
			: (($keystroke_t="+") | ($keystroke_t="="))  // Add a day
				$dateField_ptr->:=Add to date:C393($startDate_d; 0; 0; 1)
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="-")  // Subtract a day
				$dateField_ptr->:=Add to date:C393($startDate_d; 0; 0; -1)
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="T")  // Today
				$dateField_ptr->:=Current date:C33
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="M")  // First of Month
				$dateField_ptr->:=Add to date:C393($startDate_d; 0; 0; -Day of:C23($startDate_d)+1)
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="N")  // Middle of the moNth (actually the 15th)
				$dateField_ptr->:=Add to date:C393($startDate_d; 0; 0; -Day of:C23($startDate_d)+15)
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="H")  // End of montH
				$dateField_ptr->:=Add to date:C393($startDate_d; 0; 1; -Day of:C23($startDate_d))
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				Fnd.Date.EntryFilterValue:=String:C10($dateField_ptr->)
				
			: ($keystroke_t="Y")  // First of Year
				$dateField_ptr->:=Date:C102("1/1/"+String:C10(Year of:C25($startDate_d)))
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="R")  // End of yeaR
				$dateField_ptr->:=Date:C102("1/1/"+String:C10(Year of:C25($startDate_d)+1))-1
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
				
			: ($keystroke_t="C")  // Calendar
				Fnd_Date_Calendar($dateField_ptr)
				FILTER KEYSTROKE:C389("")
				HIGHLIGHT TEXT:C210($dateField_ptr->; 1; MAXTEXTLENBEFOREV11:K35:3)
		End case 
		
		
	: (Form event code:C388=On Data Change:K2:15)  // Interpret the entry
		If (Fnd.Date.EntryFilterValue#"")  // DB070111 - All new stuff here.
			$dateField_ptr->:=Fnd_Date_StringToDate(Fnd.Date.EntryFilterValue)  // DB070111 - Get edited text no longer contains the visible value with 4D 2003.8 and later.
			Fnd.Date.EntryFilterValue:=""
		End if 
End case 