//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_Request (msg{; default{; OK button{; Cancel button}}}) --> Text

  // Displays a Request dialog.  Returns the user's response and sets the OK variable.
  // Use Fnd_Dlg_GetRequest to find out what the user typed.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The message to display
  //   $2 : Text : The default response text (optional)
  //   $3 : Text : The OK button text (optional)
  //   $4 : Text : The Cancel button text (optional)

  // Returns: 
  //   $0 : Text : Text user entered in Request dialog

  // Created by Dave Batton on Jul 10, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   Removed the call to Fnd_Dlg_Reset.  It was redundant.
  // Modified by Walt Nelson on Nov 16, 2009 to actually return the text instead of the OK variable
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$3;$4)
C_LONGINT:C283($bOK)

Fnd_Dlg_Init 

Fnd_Dlg_SetText ($1;"")

If (Count parameters:C259>=2)
	Fnd_Dlg_SetRequest ($2)
Else 
	Fnd_Dlg_SetRequest 
End if 

Case of 
	: (Count parameters:C259<=2)
		Fnd_Dlg_SetButtons ("*";"*")
	: (Count parameters:C259=3)
		Fnd_Dlg_SetButtons ($3;"*")
	: (Count parameters:C259>=4)
		Fnd_Dlg_SetButtons ($3;$4)
End case 

$bOK:=Fnd_Dlg_Display 

If ($bOK=1)
	$0:=Fnd_Dlg_GetRequest 
Else 
	$0:=""
End if 

