//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_WaitToContinue (boolean)

// Causes a process to wait

// Access Type: Shared

// Parameters: C_BOOLEAN($1)

// Returns: 
//   $0 : Boolean : whether or not to wait

// Added by Walt Nelson on 2/19/10
// Created by Wayne Stewart Jun 18, 2009
//     waynestewart@mac.com
// ----------------------------------------------------

If (Count parameters:C259=1)
	<>Fnd_Gen_WaitToContinue_b:=$1
End if 

IDLE:C311

$0:=<>Fnd_Gen_WaitToContinue_b
