//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_StatusMessage ({new text}) --> Text

// Allows for the getting and setting of the information text line displayed
//   in the standard toolbar output form.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The text to set for the info line (optional)

// Returns: 
//   $0 : Text : The text that's displayed in the info line.

// Created by Dave Batton on Feb 21, 2004
// Modified by Dave Batton on Feb 27, 2007
//   The status text object is now hidden if it contains no text.
// Modified 9/10/09 to fix passing pointers to host variables in compiled apps
//[1] Added by: John Craig (8/17/09)
// ----------------------------------------------------

C_TEXT:C284($0;$1;$statusbarObjectName_t)
C_BOOLEAN:C305($visible_b)

Fnd_Tlbr_Init

If (Count parameters:C259>=1)
	//If (Not(Nil(Fnd_Tlbr_HostInfoVar_ptr)))
	Fnd_Tlbr_Info_t:=$1
	If (Not:C34(Is nil pointer:C315(Fnd_Tlbr_HostInfoVar_ptr)))
		EXECUTE METHOD:C1007("Fnd_HOST_Tlbr_SetMessage";*;Fnd_Tlbr_Info_t)  //[1] Added by: John Craig (8/17/09)
		//Fnd_Tlbr_HostInfoVar_ptr->:=Fnd_Tlbr_Info_t
	End if 
	
	$visible_b:=(Fnd_Tlbr_Info_t#"")
	
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_StatusText";$visible_b)
	$statusbarObjectName_t:="Fnd_Tlbr_Info_"+Fnd_Tlbr_Platform_t
	OBJECT SET VISIBLE:C603(*;$statusbarObjectName_t;$visible_b)
	//End if 
End if 

$0:=Fnd_Tlbr_Info_t
