//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_BlobToPicture (->blob; ->picture)

  // Converts a blob to a picture.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : A pointer to a blob
  //   $2 : Pointer : The same blob as a picture variable

  // Returns: Nothing

  // Created by JPR around 2000
  // Modified by Ed Fraser on April 2, 2002
  //   JPR's code didn't work on Windows.  The fix consisted of inverting the 5th through 8th bytes. 
  // Modified by Dave Batton on Aug 5, 2003
  //   Changed the blob IP variable to a dereferenced pointer to the passed-in blob.
  //   Return the resulting picture.
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$blob_ptr;$picture_ptr)
C_LONGINT:C283($blobSize_i)

$blob_ptr:=$1
$picture_ptr:=$2

$blobSize_i:=BLOB size:C605($blob_ptr->)+6  //Increase the end by 6 bytes (we don't care about the content of these 6 bytes)

INSERT IN BLOB:C559($blob_ptr->;0;9;0)  //adds info at the beginning

If (Is macOS:C1572)
	$blob_ptr->{0}:=Character code:C91("B")  //For Mac
	$blob_ptr->{1}:=Character code:C91("L")
	$blob_ptr->{2}:=Character code:C91("V")
	$blob_ptr->{3}:=Character code:C91("R")
	$blob_ptr->{4}:=0x000A  //for the variable type
	$blob_ptr->{5}:=($blobSize_i\16777216)
	$blob_ptr->{6}:=($blobSize_i\65536)%256
	$blob_ptr->{7}:=($blobSize_i\256)%256
	$blob_ptr->{8}:=$blobSize_i%256
	
Else 
	$blob_ptr->{0}:=Character code:C91("R")  //for Win
	$blob_ptr->{1}:=Character code:C91("V")
	$blob_ptr->{2}:=Character code:C91("L")
	$blob_ptr->{3}:=Character code:C91("B")
	$blob_ptr->{4}:=0x000A  //for the variable type
	$blob_ptr->{5}:=$blobSize_i%256
	$blob_ptr->{6}:=($blobSize_i\256)%256
	$blob_ptr->{7}:=($blobSize_i\65536)%256
	$blob_ptr->{8}:=($blobSize_i\16777216)
	
End if 

INTEGER TO BLOB:C548(0x0000;$blob_ptr->;Macintosh byte ordering:K22:2;*)
INTEGER TO BLOB:C548(0x0000;$blob_ptr->;Macintosh byte ordering:K22:2;*)
INTEGER TO BLOB:C548(0x0000;$blob_ptr->;Macintosh byte ordering:K22:2;*)
  //These values are not used
  //From here, our blob has been disguised and looks like a picture in a blob
  //4D is naive, and will think this 'picture' is fine. We know it is not True
  //but we will not tell to 4D.
  //And we swear we will not try to display this picture

BLOB TO VARIABLE:C533($blob_ptr->;$picture_ptr->)  //now we can move the blob into the picture 
