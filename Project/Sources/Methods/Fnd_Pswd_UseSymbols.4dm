//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_UseSymbols ({use symbols?}) --> Boolean

  // Pass True to tell the password generator to use symbols in passwords.
  // Returns the current setting.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : True to use symbols (optional)

  // Returns: 
  //   $0 : Boolean : True if symbols can be used

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_UseSymbols_b:=$1
End if 

$0:=<>Fnd_Pswd_UseSymbols_b
