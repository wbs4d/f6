//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_AddMultipleRecords (enable?) --> Boolean

  // Allows the developer to get and set the value of the Fnd_IO_AddMultipleRecords_b variable.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : The new value of the setting to enable adding multiple records (optional)

  // Returns: 
  //   $0 : Boolean : The current setting of the variable to indicate if the feature is enabled

  // Created by Dave Batton on Jan 25, 2005
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Enhanced parameter descriptions in header
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_IO_Init 

If (Count parameters:C259>=1)
	Fnd_IO_AddMultipleRecords_b:=$1
End if 

$0:=Fnd_IO_AddMultipleRecords_b
