//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_TextToPict (text;fontName;fontSize;fontFace;width;height{;color}) --> Picture

  // Converts a text string to a picture of the text.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : Text to make into picture

  // Returns: 
  //   $0 : Picture : A picture of the text

  // Created by Jean Pierre Ribreau.
  //   Original Method Name: JPR_TextToPict on 5/5/01
  // Modified by Dave Batton on March 31, 2008
  //   Completely rewritten to use SVG. Contains none of the original code.
  // Modified by: Walt Nelson (2/11/10) added to parameters to line 48 to render labels correctly with v11.6 MC1
  // ----------------------------------------------------

  //This Picture can be used for Tabs, menus or any other use in 4D
C_PICTURE:C286($0;$image_pic)
C_TEXT:C284($1;$2;$text_t;$fontName_t;$fontWeight_t;$fill_t;$xmlRoot_t;$elementRef_t)
C_LONGINT:C283($3;$4;$5;$6;$7;$fontSize_i;$fontStyle_i;$fontColor_i;$width_i;$height_i;$color_t)

$text_t:=$1  //String of up to 255 chars.
$fontName_t:=$2
$fontSize_i:=$3
$fontStyle_i:=$4  //can be the constants Bold, Italic...
$width_i:=$5  //Picture size in Pixels
$height_i:=$6
If (Count parameters:C259>6)
	$fontColor_i:=$7
Else 
	$fontColor_i:=0x0000  // Default to black.
End if 

Case of 
	: ($fontStyle_i=Bold:K14:2)
		$fontWeight_t:="bold"
	Else 
		$fontWeight_t:="normal"
End case 

$fill_t:=Fnd_Bttn_ColorToString ($fontColor_i)

$xmlRoot_t:=DOM Create XML Ref:C861("svg";"http://www.w3.org/2000/svg")
$elementRef_t:=DOM Create XML element:C865($xmlRoot_t;"text";"font-family";$fontName_t;"font-size";$fontSize_i;"font-weight";$fontWeight_t;"fill";$fill_t;"y";"1em")  // Modified by: Walt Nelson (2/11/10)
  //$elementRef_t:=DOM Create XML element($xmlRoot_t;"text";"font-family";$fontName_t;"font-size";$fontSize_i;"font-weight";$fontWeight_t;"fill";$fill_t)
DOM SET XML ELEMENT VALUE:C868($elementRef_t;$text_t)
SVG EXPORT TO PICTURE:C1017($xmlRoot_t;$image_pic;Get XML data source:K45:16)
DOM CLOSE XML:C722($xmlRoot_t)

$0:=$image_pic