//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Gen_DummyMethod


  // A generic method with no code.  This is useful for assigning as the close box method

  //   for new windows (handle the close in the form method) or to use with ON ERR CALL

  //   to disable any automatic reaction to errors.


  // Access Type: Private


  // Parameters: None


  // Returns: Nothing


  // Created by Dave Batton on Sep 15, 2003

  // ----------------------------------------------------

