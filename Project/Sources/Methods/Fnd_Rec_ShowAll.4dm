//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Rec_ShowAll ({->table})

// Updates the selection to include all records.  If the Macintosh Option
//   key or Windows Alt key is down, the selection is reduced to no records.

// Access Type: Shared

// Parameters: 
//   $1 : Pointer : A pointer to the table to use (optional)

// Returns: Nothing

// Created by Dave Batton on Nov 28, 2003
// Modified by Dave Batton on Mar 30, 2004
//   No longer does anything (including cause errors) if the pointer is nil.
// ----------------------------------------------------

C_POINTER:C301($1;$table_ptr)

If (Count parameters:C259>=1)
	$table_ptr:=$1
Else 
	$table_ptr:=Fnd_Gen_CurrentTable
End if 

If (Not:C34(Is nil pointer:C315($table_ptr)))  // DB040330
	If (Macintosh option down:C545)
		REDUCE SELECTION:C351($table_ptr->;0)
	Else 
		ALL RECORDS:C47($table_ptr->)
	End if 
	
	Fnd_Gen_SelectionChanged  // Always call this after changing the selection.
End if 
