//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Log_Path --> Text

// Returns the path to use for logging.

// Access: Private

// Parameters: None

// Returns: 
//   $0 : Text : The path to the log file

// Created by Dave Batton on May 25, 2004
//Modified by: Walt Nelson (2/16/10) - added parameter *
// Mod by Wayne Stewart, (2018-10-30) - New system
// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(Fnd_Log_Path; $1; $0)
End if 

C_TEXT:C284($1; $0)

Fnd_Log_Init

If (Count parameters:C259=1)
	Fnd.Log.folder:=$1
End if 

If (Length:C16(Fnd.Log.folder)=0)
	Fnd.Log.folder:=Storage:C1525.Fnd.Log.logsFolder  // Reset to default
End if 

$0:=Fnd.Log.folder

