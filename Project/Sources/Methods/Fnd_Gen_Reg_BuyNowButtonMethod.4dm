//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Gen_Reg_BuyNowButtonMethod


  // Called when one of the Buy Now buttons is clicked.


  // Method Type: Private


  // Parameters: None


  // Returns: Nothing


  // Created by Dave Batton on Jan 1, 2004
  // Mod by Wayne Stewart, (2019-10-30) - Removed Fnd_Gen_Platform
  // ----------------------------------------------------


C_TEXT:C284($platform_t;$version_t)

Case of 
	: (Form event code:C388=On Clicked:K2:4)
		Case of 
			: (Is Windows:C1573)
				$platform_t:="Win"
				
			: (Is macOS:C1572)
				$platform_t:="OSX"
				
			Else 
				$platform_t:="Mac"
		End case 
		
		$version_t:=Fnd_Gen_GetDatabaseInfo ("Fnd_GenVersion")
		$version_t:=Replace string:C233($version_t;" ";"%20")
		
		OPEN URL:C673("http://www.FoundationShell.com/orderbutton.php?product=Foundation&platform="+$platform_t+"&vers="+$version_t+"&lang="+Lowercase:C14(<>Fnd_Gen_Reg_LanguageCode_t);*)
End case 
