//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Find_Now ("Query" or "Query Selection")

  // Finishes up the built search.  This is separate from the Fnd_Find_Engine routine
  //   so it's possible to change the dialog to a multi-criteria find.

  // Parameters: 
  //   $1 : Text : Either "Query" or "Query Selection"

  // Returns: Nothing

  // Created by David Robbins many years ago.
  // Modified by Dave Batton on June 25, 2003.
  // Modified by: Walt Nelson (2/23/10) - to use preserve automatic relations status
  // ----------------------------------------------------

C_TEXT:C284($1;$searchScope_t;$method_t)
C_BOOLEAN:C305($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

GET AUTOMATIC RELATIONS:C899($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)

$searchScope_t:=$1

$method_t:=Fnd_Find_MethodNames_at{Fnd_Find_FieldNames_at}

If ($method_t="")
	SET AUTOMATIC RELATIONS:C310(True:C214;True:C214)
	
	If ($searchScope_t="Query")
		QUERY:C277(Fnd_Find_Table_ptr->)
	Else 
		QUERY SELECTION:C341(Fnd_Find_Table_ptr->)
	End if 
	
	SET AUTOMATIC RELATIONS:C310($one_b;$many_b)  // Modified by: Walt Nelson (2/23/10)
End if 