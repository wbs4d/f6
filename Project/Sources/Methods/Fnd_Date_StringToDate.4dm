//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_StringToDate (date as text{; date format}) --> Date

  // Returns the date equivalent of the string passed.

  // This works for M/D/Y, D/M/Y, Y/M/D and Y/D/M style dates. It will not
  //   work for any date with the Y in the middle (does anyone do that?).
  // Two digit years are resolved as per the SET DEFAULT CENTURY command.

  // Dates can be enter with any character as a delimiter.
  // If no year is entered, the current year is assumed.
  // If only a one or two digit number is entered, the current month is assumed.

  // If no delimiter is used, the following formats are supported (M/D/Y format):
  //   070476 Returns a date of 07/04/76
  //   07041976 Returns a date of 07/04/1976
  //   0704 Returns 07/04/00 (If this is in fact the year 2000)
  //   04 or 4 Returns the 4th day of the current month
  //   74, 704 and 074 are not supported.
  //   "Today" returns the $today_date.
  //   "Yesterday" returns the $today_date -1.
  //   "Tomorrow" returns the $today_date +1.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : A text representation of a date
  //   $2 : Text : The assumed date format, such as "M/D/Y" (optional)

  // Returns: 
  //   $0 : Date : The date

  // Created by Tom Dillon, DataCraft
  // Modified by Dave Batton on Apr 28, 2004
  //   Changed the variable names.
  //   Added support for different date formats.
  // ----------------------------------------------------

C_DATE:C307($0;$suggested_date;$today_date)
C_TEXT:C284($1;$2;$input_t;$dateFormat_t;$datePart1_t;$datePart2_t;$datePart3_t)
C_LONGINT:C283($inputLength_i;$position_i;$firstSlashPosition_i;$secondSlashPosition_i;$year_i;$month_i;$day_i)

$input_t:=$1

If (Count parameters:C259>=2)
	$dateFormat_t:=$2
Else 
	$dateFormat_t:=Fnd_Date_SystemDateFormat 
End if 

$suggested_date:=!00-00-00!  // The default answer if we can't guess.

  // The first thing we do is grab the $today_date, and then use the variable rather
  //   than 4D's Current date function.  This way we get good results even
  //   if this routine is called a few miliseconds before midnight (where a second
  //   call to $today_date might return a different value than the first call).
$today_date:=Current date:C33

Case of 
	: ($input_t=Fnd_Gen_GetString ("Fnd_Date";"Today"))  // Just test for a localized version of "Today".
		$suggested_date:=$today_date
		
	: ($input_t=Fnd_Gen_GetString ("Fnd_Date";"Yesterday"))
		$suggested_date:=Add to date:C393($today_date;0;0;-1)
		
	: ($input_t=Fnd_Gen_GetString ("Fnd_Date";"Tomorrow"))
		$suggested_date:=Add to date:C393($today_date;0;0;1)
		
	Else 
		For ($position_i;1;Length:C16($input_t))
			If (($input_t[[$position_i]]<"0") | ($input_t[[$position_i]]>"9"))  // if it's not 0-9, it must be a delimiter
				$input_t[[$position_i]]:="/"
			End if 
		End for 
		
		  // Since we replaced everthing but numbers with "/", remove any duplicates.
		Repeat 
			$input_t:=Replace string:C233($input_t;"//";"/")
		Until (Position:C15("//";$input_t)=0)
		
		$inputLength_i:=Length:C16($input_t)
		If ($inputLength_i>0)
			If ($input_t[[1]]="/")  // If the first character is a slash, drop it
				$input_t:=Substring:C12($input_t;2)
				$inputLength_i:=$inputLength_i-1
			End if 
		End if 
		
		If ($inputLength_i>0)
			If ($input_t[[$inputLength_i]]="/")  // If the last character is a slash, drop it
				$input_t:=Substring:C12($input_t;1;$inputLength_i-1)
				$inputLength_i:=$inputLength_i-1
			End if 
		End if 
		
		$firstSlashPosition_i:=Position:C15("/";$input_t)
		If (($firstSlashPosition_i>0) & ($firstSlashPosition_i<$inputLength_i))
			$secondSlashPosition_i:=Position:C15("/";Substring:C12($input_t;$firstSlashPosition_i+1))
			If ($secondSlashPosition_i>0)
				$secondSlashPosition_i:=$secondSlashPosition_i+$firstSlashPosition_i
			End if 
		Else 
			$secondSlashPosition_i:=0
		End if   // ($firstSlashPosition_i>0)
		
		$datePart1_t:=""
		$datePart2_t:=""
		$datePart3_t:=""
		
		Case of 
			: ($input_t="")  // Skip the rest if the string is blank
				
			: ($secondSlashPosition_i>0)  // There are two slashes, so we should have three valid numbers.
				$datePart1_t:=Substring:C12($input_t;1;$firstSlashPosition_i-1)
				$datePart2_t:=Substring:C12($input_t;$firstSlashPosition_i+1;$secondSlashPosition_i-$firstSlashPosition_i-1)
				$datePart3_t:=Substring:C12($input_t;$secondSlashPosition_i+1)
				
			: ($firstSlashPosition_i>0)  // Only one slash, so we've got just two numbers.
				$datePart1_t:=Substring:C12($input_t;1;$firstSlashPosition_i-1)
				$datePart2_t:=Substring:C12($input_t;$firstSlashPosition_i+1)
				
				  // No slashes (there are still many possibilities).
			: ($inputLength_i<=2)  // Just the day was entered. We need to supply the month, and put it in the right position.
				If ($dateFormat_t="@D/M@")
					$datePart1_t:=$input_t
					$datePart2_t:=String:C10(Month of:C24($today_date))
				Else 
					$datePart1_t:=String:C10(Month of:C24($today_date))
					$datePart2_t:=$input_t
				End if 
				
			: ($inputLength_i=4)  // Month and day were entered (4 characters required - 3 is too tough).
				$datePart1_t:=Substring:C12($input_t;1;2)
				$datePart2_t:=Substring:C12($input_t;3;2)
				
			: ($inputLength_i=6)  // Month, day, and a two character year were entered.
				$datePart1_t:=Substring:C12($input_t;1;2)
				$datePart2_t:=Substring:C12($input_t;3;2)
				$datePart3_t:=Substring:C12($input_t;5;2)
				
			: ($inputLength_i=8)  // Month, day, and a four character year were entered.
				Case of 
					: ($dateFormat_t="Y/@")  // It starts with the year.
						$datePart1_t:=Substring:C12($input_t;1;4)
						$datePart2_t:=Substring:C12($input_t;5;2)
						$datePart3_t:=Substring:C12($input_t;7;2)
					Else   // It must end with the year.
						$datePart1_t:=Substring:C12($input_t;1;2)
						$datePart2_t:=Substring:C12($input_t;3;2)
						$datePart3_t:=Substring:C12($input_t;5;4)
				End case 
		End case 
		
		Case of 
			: ($datePart2_t="")  // We got just one date part.  It must be the day.
				$day_i:=Num:C11($datePart1_t)
				$month_i:=Month of:C24($today_date)
				$year_i:=Year of:C25($today_date)
				
			: ($datePart3_t="")  // We got two parts.  We'll supply the year.
				If ($dateFormat_t="@D/M@")
					$day_i:=Num:C11($datePart1_t)
					$month_i:=Num:C11($datePart2_t)
				Else 
					$month_i:=Num:C11($datePart1_t)
					$day_i:=Num:C11($datePart2_t)
				End if 
				$year_i:=Year of:C25($today_date)
				
			Else   // We got all three parts.
				Case of   // I know there's a better way. I just can't figure it out!  :-(
					: ($dateFormat_t="Y/M/D")
						$year_i:=Num:C11($datePart1_t)
						$month_i:=Num:C11($datePart2_t)
						$day_i:=Num:C11($datePart3_t)
					: ($dateFormat_t="Y/D/M")
						$year_i:=Num:C11($datePart1_t)
						$day_i:=Num:C11($datePart2_t)
						$month_i:=Num:C11($datePart3_t)
					: ($dateFormat_t="D/M/Y")
						$day_i:=Num:C11($datePart1_t)
						$month_i:=Num:C11($datePart2_t)
						$year_i:=Num:C11($datePart3_t)
					: ($dateFormat_t="M/D/Y")
						$month_i:=Num:C11($datePart1_t)
						$day_i:=Num:C11($datePart2_t)
						$year_i:=Num:C11($datePart3_t)
				End case 
		End case 
		
		If (($day_i>=1) & ($day_i<=31) & ($month_i>=1) & ($month_i<=12))  // If the data appears to be valid...
			$suggested_date:=Fnd_Date_YearMonthDayToDate ($year_i;$month_i;$day_i)
		End if 
End case 

$0:=$suggested_date