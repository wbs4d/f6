//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Init

// Initializes both the process and interprocess variables used by the Fnd_Gen routines.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 21, 2003
// Modified by Dave Batton on Jan 20, 2004
//   Updated to version 4.0.1.
// Modified by Dave Batton on Jan 24, 2004
//   Updated to version 4.0.2.
//   Doesn't change the timeout value if it already has a value.
// Modified by Dave Batton on May 23, 2004
//   Updated the version number to 4.0.3.
//   Added the <>Fnd_Gen_GetStrSemaphore_t semaphore initialization.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Gen_Info method.
// Modified by Dave Batton on Feb 7, 2006
//   Now mentions the version number of the Foundation Extras component that's required.
//   Changed the line where <>Fnd_Gen_Initialized_b gets set to True.
// Mod by Wayne Stewart, (2021-04-05) - Removed Registration requirement
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Gen_Initialized_b; Fnd_Gen_Initialized_b)
C_LONGINT:C283($system_i)
C_TEXT:C284($name_t; $Path_t)

Fnd_Init  // All Init methods need to start with this

If (Storage:C1525.Fnd.Gen=Null:C1517)  // Make certain we only do this once
	Use (Storage:C1525.Fnd)
		Storage:C1525.Fnd.Gen:=New shared object:C1526
	End use 
End if 



If (Not:C34(<>Fnd_Gen_Initialized_b))  // So we only do this once.
	Compiler_Fnd_Gen
	Compiler_Fnd_Gen_Reg
	
	ARRAY TEXT:C222(<>Fnd_Gen_InstalledComponents_at; 0)
	ARRAY TEXT:C222(<>Fnd_Gen_UnavailableComponents_a; 0)
	COMPONENT LIST:C1001(<>Fnd_Gen_InstalledComponents_at)
	
	// Let's just add these upfront
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Art")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Bttn")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Cmpt")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Data")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Date")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Dict")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Dlg")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Ext")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_FCS")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_File")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Find")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Gen")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Gen_Reg")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_IO")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_IO2")
	
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_List")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Loc")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Log")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Menu")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Msg")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Nav")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Obj")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Pref")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Prnt")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Pswd")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Rec")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Reg")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_RegG")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Shell")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Sort")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_SqNo")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Text")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Tlbr")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Tlbr2")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_VS")
	APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; "Fnd_Wnd")
	
	// DB060207 - Normally we do this at the end of this If, but in this case we need to call
	//   it _after_ the compiler declarations and before calling any other Fnd_Gen routines.
	<>Fnd_Gen_Initialized_b:=True:C214
	
	If (<>Fnd_Gen_Reg_Timeout_i=0)  // DB040205 - Added this test.
		<>Fnd_Gen_Reg_Timeout_i:=Tickcount:C458+(30*60*60)  // The default timeout period (thirty minutes).
	End if 
	
	// wwn Add the Hook Methods, if necessary, won't overwrite existing ones
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_DisplayRecord")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_DisplayTable")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_InputFormButton")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_InputFormMethod")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_OutputFormMethod")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_IO_SelectionChanged")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_List_SetEditableLists")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Menu_UpdateMenuBar")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Rec_Delete")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Rec_New")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Administration")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Find")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_InitPlugins")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_ModifyRecords")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_NavPalette")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_OpenTable")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Print")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_QueryEditor")  // Added Walt Nelson (2/18/10)
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Quit")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Setup")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Sort")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_SpecialFunctions")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_Startup")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_Shell_VirtualStructure")  // Added Walt Nelson Apr 25 2009
	Fnd_Gen_RequiredHookMethod("Fnd_Hook_SqNo_SetIDField")  // Added Walt Nelson Apr 25 2009
	
	Fnd_Gen_Localize
	
	$name_t:=Fnd_Gen_FileName(Structure file:C489(*))
	<>Fnd_Gen_RunningInHost_b:=($name_t#"Foundation_@")  // WN100211: was "Fnd_@ 
	
	If (Position:C15("."; $name_t)>0)
		$name_t:=Substring:C12($name_t; 1; Position:C15("."; $name_t)-1)
	End if 
	
	Fnd_Gen_SetDatabaseInfo("DatabaseName"; $name_t)
	
	$Path_t:=Get 4D folder:C485(Active 4D Folder:K5:10)
	$Path_t:=$Path_t+$name_t+Folder separator:K24:12
	
	Fnd_Gen_SetDatabaseInfo("PrefsFolder"; $Path_t)
	If (Test path name:C476($Path_t)#Is a folder:K24:2)
		CREATE FOLDER:C475($Path_t)
	End if 
	
	
	<>Fnd_Gen_MenuName_t:=""  // See Fnd_Gen_MenuBar.
	
	<>Fnd_Gen_LocSemaphore_t:="$Fnd_Gen_LocSemaphore"
	
	<>Fnd_Gen_Quitting_b:=False:C215  // Gets set to True while we're trying to quit.
	
	<>Fnd_Gen_Reg_State_i:=1  // bypass registration
	
	//Fnd_Gen_Reg_Startup
	
End if 

//Fnd_Gen_Reg_CheckTimout

If (Not:C34(Fnd_Gen_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Gen
	
	Fnd_Gen_CurrentFormType_i:=0  // 0 = Fnd_Gen_UnknownForm (don't use constant here)
	Fnd_Gen_ProcessLaunched_b:=False:C215
	
	Fnd_Gen_Initialized_b:=True:C214
End if 
