//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_Tlbr_Clear


// Clears all items from the toolbar.


// Access Type: Shared


// Parameters: None


// Returns: Nothing


// Created by Dave Batton on Jan 27, 2005

// ----------------------------------------------------


C_LONGINT:C283($objectNumber_i)

Fnd_Tlbr_Init

For ($objectNumber_i;1;<>Fnd_Tlbr_MaxObjects_i)
	Fnd_Tlbr_ObjectNames_at{$objectNumber_i}:=""
	Fnd_Tlbr_ObjectLabels_at{$objectNumber_i}:=""
	Fnd_Tlbr_ObjectIconNames_at{$objectNumber_i}:=""
	Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}:=0
	Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i}:=False:C215
	Fnd_Tlbr_ObjectMethods_at{$objectNumber_i}:=""
	Fnd_Tlbr_ObjectShortcuts_at{$objectNumber_i}:=""
End for 

Fnd_Tlbr_UsedObjects_i:=0
Fnd_Tlbr_ButtonsAreValid_b:=False:C215
