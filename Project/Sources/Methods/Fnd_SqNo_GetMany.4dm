//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_GetMany (group name; how many; ->longint array)

  // Returns multiple sequence numbers for the specified group. If the group
  //   doesn't already exist, it is created.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The group name
  //   $2 : Longint : The number of numbers to return
  //   $3 : Pointer : A longint array to receive the numbers

  // Returns: Nothing

  // Created by Dave Batton on Sep 24, 2007
  // ----------------------------------------------------

C_TEXT:C284($1;$groupName_t)
C_LONGINT:C283($2;$count_i;$processNumber_i;$giveUpTime_i)
C_POINTER:C301($3;$numbers_ptr)

$groupName_t:=$1
$count_i:=$2
$numbers_ptr:=$3

ARRAY LONGINT:C221($numbers_ptr->;0)  // Clear any existing crap from the array.
ARRAY LONGINT:C221($numbers_ptr->;$count_i)  // Resize the array.

Fnd_SqNo_Init 

If (Trigger level:C398>0)
	  // If we're in a trigger, get the numbers directly, not using the Fnd_SqNo_Distributor process.
	Fnd_SqNo_GetMany2 ($groupName_t;$numbers_ptr)
	
Else 
	If (Not:C34(Semaphore:C143(<>Fnd_SqNo_Semaphore_t;300)))  // Wait up to 5 seconds.
		
		<>Fnd_SqNo_Name_t:=$groupName_t
		<>Fnd_SqNo_Number_i:=0-$count_i  // This tells the distributor to get many numbers.
		
		  // Start the Fnd_SqNo_Distributor process if it's not already running.
		  // DB040129 - We must use a different name if we're running this code on
		  //   4D Server, since all of the clients that are logged in will have a matching 
		  //   process running on 4D Server named "Fnd_SqNo_Distributor process".  By 
		  //   using a different name for the server, we won't get confused by these others.
		If (Application type:C494=4D Server:K5:6)
			$processNumber_i:=New process:C317("Fnd_SqNo_Distributor";Fnd_Gen_DefaultStackSize;"Fnd_SqNo_Distributor server";*)  // DB040520
		Else 
			$processNumber_i:=New process:C317("Fnd_SqNo_Distributor";Fnd_Gen_DefaultStackSize;"Fnd_SqNo_Distributor process";*)
		End if 
		
		If ($processNumber_i=0)
			Fnd_Gen_BugAlert (Current method name:C684;"Unable to start the sequence number distributor process.")
			
		Else 
			RESUME PROCESS:C320($processNumber_i)  // If it's already running, it may be paused. Wake it.
			
			  // Now wait for the Fnd_SqNo_Distributor process to modify the value of <>Fnd_SqNo_Number_i
			$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
			Repeat 
				DELAY PROCESS:C323(Current process:C322;1)  // We're in a hurry. Don't wait long.
			Until ((<>Fnd_SqNo_Number_i>=0) | (Tickcount:C458>$giveUpTime_i))
			
			  // Get the new sequence numbers.
			COPY ARRAY:C226(<>Fnd_SqNo_Numbers_ai;$numbers_ptr->)
			ARRAY LONGINT:C221(<>Fnd_SqNo_Numbers_ai;0)
			
			  // Tell the SeqNoDistributor process we're done with it - we have our values.
			<>Fnd_SqNo_Number_i:=-2
			
			  // Wait for confirmation that the Fnd_SqNo_Distributor process got our message before 
			  //   releasing the semaphore.
			$giveUpTime_i:=Tickcount:C458+<>Fnd_SqNo_Timeout_i
			While ((<>Fnd_SqNo_Number_i#-1) & (Tickcount:C458<$giveUpTime_i))
				DELAY PROCESS:C323(Current process:C322;1)
			End while 
		End if 
		
		CLEAR SEMAPHORE:C144(<>Fnd_SqNo_Semaphore_t)
	End if 
	
End if 
