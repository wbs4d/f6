//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Loc

  // Compiler variables related to the Foundation localization routines.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 26, 2003
  // Modified by : Vincent Tournier(6/17/2011)
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Loc_Initialized_b)
If (Not:C34(<>Fnd_Loc_Initialized_b))  // So we only do the IP variables once.
	C_TEXT:C284(<>Fnd_Loc_CurrentLanguage_t;<>Fnd_Loc_Semaphore_t)
	
	C_TEXT:C284(<>Fnd_Loc_EditorLookupCode_t;<>Fnd_Loc_EditorStringEN_t;<>Fnd_Loc_EditorStringXX_t)
	C_TEXT:C284(<>Fnd_Loc_EditorModuleName_t;<>Fnd_Loc_EditorOriginalModName_t)
	C_TEXT:C284(<>Fnd_Loc_EditorLanguage2Code_t;<>Fnd_Loc_EditorGroupName_t)
	C_LONGINT:C283(<>Fnd_Loc_EditorAddModuleButton_i;<>Fnd_Loc_EditorDeleteModButton_i)
	C_LONGINT:C283(<>Fnd_Loc_EditorAddGroupButton_i;<>Fnd_Loc_EditorDeleteGroupButton)
	C_LONGINT:C283(<>Fnd_Loc_EditorAddCodeButton_i;<>Fnd_Loc_EditorDeleteCodeButton_)
	C_LONGINT:C283(<>Fnd_Loc_EditorExportButton_i;<>Fnd_Loc_EditorImportButton_i)
	C_BOOLEAN:C305(<>Fnd_Loc_EditorModuleModified_b;<>Fnd_Loc_EditorSkipNextCheck_b)
	
	ARRAY TEXT:C222(<>Fnd_Loc_Modules_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_LookupCodes_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Loc_Strings_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorLanguageCodes_at;0)
	
	ARRAY TEXT:C222(<>Fnd_Loc_EditorModules_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorGroups_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorCodes_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorGroupCodes_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorStringsEN_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorStringsXX_at;0;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempCodes_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsXX_at;0)
	  //BEGIN Modified by : Vincent Tournier(6/17/2011)
	ARRAY TEXT:C222(<>Fnd_Loc_ModuleNames_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_ListStrings_at;0)
	ARRAY TEXT:C222(<>Fnd_Loc_TempArray_at;0)
	  //END Modified by : Vincent Tournier(6/17/2011)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_POINTER:C301(Fnd_Loc_FixButtonWidths ;${1})
	
	C_LONGINT:C283(Fnd_Loc_Editor ;$1)
	
	C_POINTER:C301(Fnd_Loc_Editor_ArrayToList ;$1)
	C_TEXT:C284(Fnd_Loc_Editor_ArrayToList ;$2)
	
	C_BOOLEAN:C305(Fnd_Loc_Editor_EditorClosed ;$0)
	
	C_POINTER:C301(Fnd_Loc_FixLabelWidths ;${1})
	
	C_TEXT:C284(Fnd_Loc_GetString ;$0;$1;$2;${3})
	
	C_TEXT:C284(Fnd_Loc_ListToArray ;$1)
	C_POINTER:C301(Fnd_Loc_ListToArray ;$2)
	
	C_TEXT:C284(Fnd_Loc_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Loc_LanguageCode ;$0;$1)
	
	C_TEXT:C284(Fnd_Loc_LoadStringList ;$1)
	
	C_TEXT:C284(Fnd_Loc_SetLocalizationStrings ;$1)
	C_POINTER:C301(Fnd_Loc_SetLocalizationStrings ;$2;$3)
	
	C_POINTER:C301(Fnd_Loc_ArrayToList ;$1)
	C_TEXT:C284(Fnd_Loc_ArrayToList ;$2)
	
	C_TEXT:C284(Fnd_Loc_LoadTranslation ;$1;$2)
	
End if 
