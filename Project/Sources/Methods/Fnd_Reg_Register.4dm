//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_Register (feature code; unlock code; user name)

  // Call to register the product.  Sets the OK variable to 1 if the product gets registered.
  //   Sets the Error variable if there's a problem.
  // This doesn't check to see if it's already been registered, so it can be called
  //   to change the registration at any time.  But only does the update if the
  //   user name and unlock code are valid.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The feature code
  //   $2 : Text : Unlock code
  //   $3 : Text : User name

  // Returns: Nothing

  // Created by Dave Batton on Mar 14, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$3;$featureCode_t;$unlockCode_t;$userName_t;$licenseInfo_t)
C_LONGINT:C283($position_i)

$featureCode_t:=$1
$unlockCode_t:=$2
$userName_t:=$3

If ($featureCode_t="")
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

Error:=0

$position_i:=Position:C15("/";$unlockCode_t)
If ($position_i>0)
	$licenseInfo_t:=Substring:C12($unlockCode_t;$position_i+1)
	$unlockCode_t:=Substring:C12($unlockCode_t;1;$position_i-1)
End if 

If (Fnd_Reg_ValidateUnlockCode (<>Fnd_Reg_SecretKey_t;$featureCode_t;$userName_t;$licenseInfo_t;$unlockCode_t;<>Fnd_Reg_UnlockCodeFormat_t))
	Fnd_Reg_SaveRegistrationInfo ($featureCode_t;$userName_t;$licenseInfo_t;$unlockCode_t)
	
Else 
	Error:=18901  // Incorrect registration info entered.
End if 

If (Error=0)
	OK:=1
Else 
	OK:=0
End if 
