//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_FeatureName ({feature code{; feature name}}) --> Text

  // Sets and gets the feature name for the specified feature code.
  // This name is used in the registration dialogs.
  // These names can be localized, so they're not stored in the with the registration
  //   information. If you want to use something other than the default values, you'll
  //   need to call this method to set up the names before trying to retrieve them or
  //   before displaying the registration dialogs.
  // The Application code returns the database name.
  // Other feature codes return "Feature: codename"

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)
  //   $2 : Text : The new feature name (optional)

  // Returns: 
  //   $0 : Text : The current feature name

  // Created by Dave Batton on Jan 2, 2006
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$featureCode_t;$featureName_t)
C_LONGINT:C283($index_i)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

$featureName_t:=""

If (Not:C34(Semaphore:C143(<>Fnd_Reg_Semaphore_t;300)))  // Wait up to 5 seconds.
	$index_i:=Fnd_Reg_FeatureCodeIndex ($featureCode_t)
	
	If (Count parameters:C259>=2)
		<>Fnd_Reg_FeatureNames_at{$index_i}:=$2
	End if 
	
	$featureName_t:=<>Fnd_Reg_FeatureNames_at{$index_i}
	
	CLEAR SEMAPHORE:C144(<>Fnd_Reg_Semaphore_t)
End if 

$0:=$featureName_t
