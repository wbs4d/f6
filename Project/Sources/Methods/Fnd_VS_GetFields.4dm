//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_GetFields (->table; ->pointer array{; ->text array})

  // Returns in the passed arrays the pointers to and the field names of all
  //   of the visible fields for the specified table.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a table
  //   $2 : Pointer : A pointer to a pointer array to receive the field pointers
  //   $3 : Pointer : A pointer to a text array to receive the field names (optional)

  // Returns: Nothing

  // Created by Dave Batton on Sep 2, 2003
  // Modified by Dave Batton on Jan 24, 2004
  //   Now returns virtual table names.
  // Modified by Gary Boudreaux on Dec 2, 2008
  //   Corrected typing of $indexed_b and $unique_b
  // ----------------------------------------------------

C_POINTER:C301($1;$2;$3;$table_ptr;$fieldPtrsArray_ptr;$fieldNamesArray_ptr;$field_ptr)
C_LONGINT:C283($tableNumber_i;$fieldNumber_i;$type_i;$length_i;$element_i)
  //GB20081202 - moved $indexed_b and $unique_b declarations from C_LONGINT to C_BOOLEAN
C_BOOLEAN:C305($invisible_b;$indexed_b;$unique_b)

$table_ptr:=$1
$fieldPtrsArray_ptr:=$2

If (Count parameters:C259>=3)
	$fieldNamesArray_ptr:=$3
	ARRAY TEXT:C222($fieldNamesArray_ptr->;0)
End if 

$tableNumber_i:=Table:C252($table_ptr)

If (Is table number valid:C999($tableNumber_i))
	For ($fieldNumber_i;1;Get last field number:C255($table_ptr))
		If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))
			GET FIELD PROPERTIES:C258($tableNumber_i;$fieldNumber_i;$type_i;$length_i;$indexed_b;$unique_b;$invisible_b)
			If (Not:C34($invisible_b))  // Include only visible fields.
				$element_i:=Size of array:C274($fieldPtrsArray_ptr->)+1
				INSERT IN ARRAY:C227($fieldPtrsArray_ptr->;$element_i)
				$fieldPtrsArray_ptr->{$element_i}:=Field:C253($tableNumber_i;$fieldNumber_i)
				If (Count parameters:C259>=3)
					INSERT IN ARRAY:C227($fieldNamesArray_ptr->;$element_i)
					$field_ptr:=Field:C253($tableNumber_i;$fieldNumber_i)
					$fieldNamesArray_ptr->{$element_i}:=Fnd_VS_FieldName ($field_ptr)
				End if 
			End if 
		End if 
	End for 
End if 