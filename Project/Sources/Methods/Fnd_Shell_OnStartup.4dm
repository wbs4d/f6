//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_OnStartup

// The startup routine for the Foundation Shell.  Called from the On Startup database method.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// Modified by Dave Batton on Jan 24, 2004
//   Added a call to the new virtual structure hook.
// Modified by Dave Batton on Aug 2, 2004
//   Added the missing call to Fnd_List_OnStartup.
// Modified by Walt Nelson on Sep 30, 2009
//   Added calls to Fnd_Gen_RequiredHostMethod.
// Modified by: Walt Nelson (3/1/10) added Fnd_Host_Tlbr_ButtonClck
// Modified by Doug Hall on 2/7/2023 to call Fnd_Tbl_CreateAccessArray and Fnd_Reltd_Init
// ----------------------------------------------------

If ((Macintosh option down:C545) & (Macintosh command down:C546))
	// In case we get in trouble, this might help us get out of it.
	TRACE:C157
End if 

Fnd_Shell_Init

// setup any required Host methods
Fnd_Gen_RequiredHostMethod("Fnd_Host_ExecuteFormula")
Fnd_Gen_RequiredHostMethod("Fnd_Host_GetIntegerValue")
Fnd_Gen_RequiredHostMethod("Fnd_Host_GetFormProperties")
Fnd_Gen_RequiredHostMethod("Fnd_Host_ArrayToList")  // added wwn (07/01/09)
Fnd_Gen_RequiredHostMethod("Fnd_Host_ListToArray")
Fnd_Gen_RequiredHostMethod("Fnd_Host_GetPictureFromLibrary")
Fnd_Gen_RequiredHostMethod("Fnd_Host_Tlbr_SetBtnPict")  //[1] Added by: John Craig (8/17/09)
Fnd_Gen_RequiredHostMethod("Fnd_Host_Tlbr_SetMessage")  //[1] Added by: John Craig (8/17/09)
Fnd_Gen_RequiredHostMethod("Fnd_Host_Tlbr_ButtonClck")  // Modified by: Walt Nelson (3/1/10)
Fnd_Gen_RequiredHostMethod("Fnd_Host_IO_DisplayDialog")  // Jun 11, 2010 15:11:06 -- I.Barclay Berry for iLBListMagic
Fnd_Gen_RequiredHostMethod("Fnd_Host_SaveList")  // Modified by: Walt Nelson (2/3/11) for proper updating of 4D Lists



If (Application type:C494=4D Server:K5:6)
	If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_InitPlugIns")=1)
		EXECUTE METHOD:C1007("Fnd_Hook_Shell_InitPlugIns"; *)
	End if 
	
	Fnd_List_OnStartup
	
Else 
	If (<>Fnd_Shell_Running_b)
		Fnd_Menu_MenuBar("*")  // Just activate the Foundation menu bar.
		
	Else 
		<>Fnd_Shell_Running_b:=True:C214  // This will remain True until we quit.  Important to know during development.
		
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Setup")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_Shell_Setup"; *)  // Get the developers personalization for this database.
		End if 
		
		Fnd_Art_StartupDialog
		
		Fnd_List_OnStartup  // DB040802 - Update the user's lists if necessary.
		
		// Fnd_IO_Init  ` Pre-load the toolbar images so we don't have to wait for them later. ###
		
		HIDE TOOL BAR:C434  // This shell was not designed to support 4D's toolbar.
		SET WINDOW TITLE:C213(Fnd_Gen_GetDatabaseInfo("DatabaseName"))  // Set the splash screen window title.
		SET ABOUT:C316(Fnd_Loc_GetString("Fnd_Shell"; "AboutMenu")+" "+Fnd_Gen_GetDatabaseInfo("DatabaseName"); "Fnd_Menu_About")  // Install the custom About Box.
		
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_InitPlugIns")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_Shell_InitPlugIns"; *)  // The developer hook to initialize the plug-ins.
		End if 
		
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_VirtualStructure")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_Shell_VirtualStructure"; *)  // Let the developer set up the virtual structure.
		End if 
		
		If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Startup")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_Shell_Startup"; *)  // Run the developer's startup code.
		End if 
		
		If (Fnd_Pref_GetBoolean("Fnd_Nav: Open at Startup"; True:C214))
			Fnd_Shell_NavigationPalette  // This calls the hook.  Fnd_Nav_Display doesn't.
		End if 
		
		// ---- JDH 20230207 To initialize Find Related, and dependent access array.
		Fnd_Tbl_Init
		Fnd_Reltd_Init
		// ----
		
		Fnd_Art_StartupDialogClose
		
		Fnd_Shell_MenuBar  // Fnd_Menu_MenuBar doesn't display the menu bar as expected at startup. 
	End if 
End if 

