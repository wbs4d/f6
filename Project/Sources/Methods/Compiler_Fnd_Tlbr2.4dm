//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Tlbr2

  // Compiler declarations for the toolbar component.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Original Method Name: COMPILER_toolbar
  // Author: Mark Mitchenall
  // Created: 7/2/02 at 7:07 PM
  // Part of the toolbar_Component, © mitchenall.com, 2001
  // Modified by Dave Batton on Nov 29, 2003
  //   Updated specifically for use by the Foundation Shell.
  // wwn 9/14/09 added declaration for picture_ptr
  // Modified by: Walt Nelson (3/1/10) added second text parameter to Fnd_Tlbr_Button_OnClicked
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Tlbr_Initialized_b)
If (Not:C34(<>Fnd_Tlbr_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(<>Fnd_Tlbr_MaxObjects_i)
End if 


C_BOOLEAN:C305(Fnd_Tlbr_Initialized_b)
If (Not:C34(Fnd_Tlbr_Initialized_b))
	Compiler_Fnd_Tlbr   // The compiler declarations specifically for the Fnd_Tlbr_Toolbar form.
	
	C_POINTER:C301(picture_ptr)  // wwn 9/14/09 was $picture_ptr
	
	C_LONGINT:C283(Fnd_Tlbr_UsedObjects_i)
	C_LONGINT:C283(Fnd_Tlbr_ButtonLeftOffset_i;Fnd_Tlbr_ButtonTopOffset_i;Fnd_Tlbr_MenuTopOffset_i)
	C_LONGINT:C283(Fnd_Tlbr_ButtonPadding_i;Fnd_Tlbr_Result_i)
	C_LONGINT:C283(Fnd_Tlbr_DividerPadding_i;Fnd_Tlbr_DividerTopOffset_i)
	
	C_TEXT:C284(Fnd_Tlbr_Style_t;Fnd_Tlbr_Platform_t)
	C_TEXT:C284(Fnd_Tlbr_DividerImageName_t;Fnd_Tlbr_CurrentButton_t)
	
	C_BOOLEAN:C305(Fnd_Tlbr_ButtonsAreValid_b;Fnd_Tlbr_BackgroundIsValid_b)
	
	C_POINTER:C301(Fnd_Tlbr_HostInfoVar_ptr)
	
	  //2D arrays for toolbar button information stack
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectNames_at;0)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectLabels_at;0)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectIconNames_at;0)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectMethods_at;0)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectShortcuts_at;0)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectMenuItems_at;0;0)
	
	ARRAY INTEGER:C220(Fnd_Tlbr_ObjectTypes_ai;0)
	ARRAY BOOLEAN:C223(Fnd_Tlbr_ObjectEnabled_ab;0)
	
	ARRAY POINTER:C280(Fnd_Tlbr_Pictures_aptr;0)
	ARRAY POINTER:C280(Fnd_Tlbr_PictureButtons_aptr;0)
	
	ARRAY POINTER:C280(Fnd_Tlbr_HostPictureVars_aptr;0)
	ARRAY POINTER:C280(Fnd_Tlbr_HostPictButtons_aptr;0)
	
	
End if 


  // Wrapped in "If(False)...End If" so that the compiler method can be called
  //   without getting syntax errors.
If (False:C215)
	C_TEXT:C284(Fnd_Tlbr_Button_Add ;$1;$2;$3;$4;$5)
	
	C_LONGINT:C283(Fnd_Tlbr_Button_Count ;$0)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Enabled ;$1)
	C_BOOLEAN:C305(Fnd_Tlbr_Button_Enabled ;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Icon ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Insert ;$1;$2;$3;$4;$5;$6)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Label ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Menu ;$1)
	C_POINTER:C301(Fnd_Tlbr_Button_Menu ;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Method ;$0;$1;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Name ;$0)
	C_LONGINT:C283(Fnd_Tlbr_Button_Name ;$1)
	
	C_POINTER:C301(Fnd_Tlbr_Button_OnClicked ;$1)
	C_TEXT:C284(Fnd_Tlbr_Button_OnClicked ;$2)  // Modified by: Walt Nelson (3/1/10)
	
	C_TEXT:C284(Fnd_Tlbr_Button_OnShortcut ;$1)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Position ;$1)
	C_POINTER:C301(Fnd_Tlbr_Button_Position ;$2;$3;$4;$5)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Remove ;$1)
	
	C_TEXT:C284(Fnd_Tlbr_Button_Shortcut ;$1;$2)
	
	C_TEXT:C284(Fnd_Tlbr_CurrentButton ;$0;$1)
	
	C_PICTURE:C286(Fnd_Tlbr_Divider_GetPicture ;$0)
	C_TEXT:C284(Fnd_Tlbr_Divider_GetPicture ;$1)
	
	C_TEXT:C284(Fnd_Tlbr_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Tlbr_StatusMessage ;$0;$1)
	
	C_LONGINT:C283(Fnd_Tlbr_Object_Draw ;$1;$3;$4)
	C_TEXT:C284(Fnd_Tlbr_Object_Draw ;$2)
	
	C_TEXT:C284(Fnd_Tlbr_Object_GetNumber ;$1)
	C_LONGINT:C283(Fnd_Tlbr_Object_GetNumber ;$0)
	
	C_TEXT:C284(Fnd_Tlbr_Platform ;$0;$1)
	
	C_POINTER:C301(Fnd_Tlbr_SetHostVars ;$1;$2;$3)
	
	C_TEXT:C284(Fnd_Tlbr_ShortcutKeyName_Get ;$1)
	C_TEXT:C284(Fnd_Tlbr_ShortcutKeyName_Get ;$0)
	
	C_TEXT:C284(Fnd_Tlbr_Style ;$0;$1)
End if 