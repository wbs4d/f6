//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Tlbr_UpdateBackground

  // Updates the toolbar background.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jan 26, 2005
  // Modified by Dave Batton on Apr 13, 2005
  //   Added support for the new "Prefs" toolbar style.
  //v5.2 091023 - Barclay Berry fix for resizing window losing display
  // ----------------------------------------------------

C_TEXT:C284($toolbarObjectName_t;$statusbarObjectName_t;$size_t)
C_LONGINT:C283($windowLeft_i;$windowTop_i;$windowRight_i;$windowBottom_i;$height_i)
C_BOOLEAN:C305($visible_b)

If (Not:C34(Fnd_Tlbr_BackgroundIsValid_b))
	Fnd_Tlbr_BackgroundIsValid_b:=True:C214
	
	
	  // Figure out the name of the toolbar background form object we want to display.
	If ((Fnd_Tlbr_Style_t="Large") | (Fnd_Tlbr_Style_t="Prefs"))  // DB050413
		$size_t:="Large"
	Else 
		$size_t:="Small"  // The current options are "Small1" and "Small2".
	End if 
	$toolbarObjectName_t:="Fnd_Tlbr_Tlbr_"+Fnd_Tlbr_Platform_t+$size_t
	$statusbarObjectName_t:="Fnd_Tlbr_Info_"+Fnd_Tlbr_Platform_t
	
	
	  // Hide all of the toolbar background form objects.
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_Tlbr@";False:C215)
	OBJECT SET VISIBLE:C603(*;$toolbarObjectName_t;True:C214)
	
	
	  // Resize the toolbar and info objects for the current window size.
	GET WINDOW RECT:C443($windowLeft_i;$windowTop_i;$windowRight_i;$windowBottom_i)
	
	OBJECT GET COORDINATES:C663(*;$toolbarObjectName_t;$toolbarLeft_i;$toolbarTop_i;$toolbarRight_i;$toolbarBottom_i)
	OBJECT MOVE:C664(*;$toolbarObjectName_t;0;$toolbarTop_i;$windowRight_i-$windowLeft_i;$toolbarBottom_i;*)
	
	  // Move the status bar background into place.
	OBJECT GET COORDINATES:C663(*;$statusbarObjectName_t;$statusbarLeft_i;$statusbarTop_i;$statusbarRight_i;$statusbarBottom_i)
	$height_i:=$statusbarBottom_i-$statusbarTop_i
	$statusbarTop_i:=$toolbarBottom_i
	$statusbarBottom_i:=$statusbarTop_i+$height_i
	OBJECT MOVE:C664(*;$statusbarObjectName_t;0;$statusbarTop_i;$windowRight_i-$windowLeft_i;$statusbarBottom_i;*)
	
	  // Move the status bar text into place.
	$statusbarTop_i:=$statusbarTop_i+3
	OBJECT MOVE:C664(*;"Fnd_Tlbr_StatusText";0;$statusbarTop_i;$windowRight_i-$windowLeft_i;$statusbarBottom_i;*)
	
	  // Make the status bar visible based on its content. This is also done from Fnd_Tlbr_StatusMessage.
	  //SET VISIBLE(*;"Fnd_Tlbr_StatusText";False)
	  //SET VISIBLE(*;$statusbarObjectName_t;(Fnd_Tlbr_Info_t#""))
	  //v5.2 091023 - Barclay Berry fix for resizing window losing display
	$visible_b:=(Fnd_Tlbr_Info_t#"")
	OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_StatusText";$visible_b)
	OBJECT SET VISIBLE:C603(*;$statusbarObjectName_t;$visible_b)
	
End if 