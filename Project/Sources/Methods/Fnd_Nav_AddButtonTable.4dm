//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_AddButtonTable (->table{; label{; foreground color; background color}})

// Adds a new button to the palette and assigns the specified table to it.
// If no label or a blank label name is passed, this routine will use the table
//   name for the label.
// If a button already exists with an identical label, it is updated.
// This routine is designed to be called only if the Foundation IO
//   component is installed.

// Access: Shared

// Parameters:
//   $1 : Pointer : The table
//   $2 : Text : The button label (optional)

// Returns: Nothing

// Created by Dave Batton on Sep 5, 2003
// Modified by Dave Batton on Jul 1, 2004
//   This method can now be called to update the palette while it's displayed.
//   The Virtual Structure component is now called only if it's availble.
// ----------------------------------------------------

C_POINTER:C301($1; $table_ptr)
C_TEXT:C284($2; $label_t)
C_LONGINT:C283($3; $4; $buttonNumber_i)

$table_ptr:=$1

If (Count parameters:C259>=2)
	$label_t:=$2
Else 
	$label_t:=""
End if 

Case of 
	: ($label_t#"")
		
		//: (Fnd_Gen_ComponentAvailable("Fnd_VS"))
		//EXECUTE METHOD("Fnd_VS_TableName"; $label_t; $table_ptr)
	Else 
		$label_t:=Fnd_VS_TableName($table_ptr)
		//$label_t:=Table name($table_ptr)
		
End case 

Fnd_Nav_Init

If (Not:C34(Semaphore:C143(Storage:C1525.Fnd.Nav.Semaphore; 300)))  // Wait up to 5 seconds.
	$buttonNumber_i:=Fnd_Nav_Add($label_t)
	
	<>Fnd_Nav_TablePtrs_aptr{$buttonNumber_i}:=$table_ptr
	
	If (Count parameters:C259>=3)
		<>Fnd_Nav_ForegroundColors_ai{$buttonNumber_i}:=$3
		<>Fnd_Nav_BackgroundColors_ai{$buttonNumber_i}:=$4
	End if 
	
	CLEAR SEMAPHORE:C144(Storage:C1525.Fnd.Nav.Semaphore)
End if 