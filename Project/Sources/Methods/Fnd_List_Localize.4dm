//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_Localize

  // Passed the localization strings to the General and (if available) Localization components.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Oct 4, 2007
  // ----------------------------------------------------

C_TEXT:C284($component_t;$language_t)

$component_t:="Fnd_List"
$language_t:=Fnd_Gen_Info ("language")

ARRAY TEXT:C222($lookupCodes_at;0)
LIST TO ARRAY:C288($component_t;$lookupCodes_at)

ARRAY TEXT:C222($strings_at;0)
LIST TO ARRAY:C288($component_t+"_"+$language_t;$strings_at)

Fnd_Gen_SetLocalizationStrings ($component_t;->$lookupCodes_at;->$strings_at)
