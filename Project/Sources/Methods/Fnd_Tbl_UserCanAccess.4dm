//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tbl_UserCanAccess (table pointer) --> boolean

// Returns TRUE if the current user can access table $1->

// Access: Shared

// Parameters: 
//   $1 : Pointer : Table pointer

// Returns: 
//   $0 : Type : Description

// Created by Doug Hall (20030617)
// ----------------------------------------------------

#DECLARE($table_ptr : Pointer)->$mayAccess_b : Boolean

var $tableNum_i : Integer

Fnd_Tbl_Init  // Make certain the arrays have been declared

$tableNum_i:=Table:C252($table_ptr)
$mayAccess_b:=<>Fnd_Tbl_UserCanAccess_ab{$tableNum_i}