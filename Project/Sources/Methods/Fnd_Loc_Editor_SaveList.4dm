//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_SaveList

  // Saves changes back to the 4D Lists.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 11, 2004
  // Modified by : Vincent Tournier (06/17/2011)
  // ----------------------------------------------------

C_LONGINT:C283($groupNumber_i;$codeNumber_i;$element_i;$elementsToAdd_i;$xxLength_i)
C_TEXT:C284($groupName_t)

If (<>Fnd_Loc_EditorModuleName_t#"")
	If (<>Fnd_Loc_EditorModuleModified_b)
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempCodes_at;0)
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsXX_at;0)
		
		$xxLength_i:=0
		$element_i:=0
		For ($groupNumber_i;1;Size of array:C274(<>Fnd_Loc_EditorGroups_at))
			$element_i:=$element_i+1
			$elementsToAdd_i:=Size of array:C274(<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i})+1
			
			$groupName_t:="  ` "+<>Fnd_Loc_EditorGroups_at{$groupNumber_i}
			
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempCodes_at;$element_i;$elementsToAdd_i)
			<>Fnd_Loc_EditorTempCodes_at{$element_i}:=$groupName_t
			
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempStringsEN_at;$element_i;$elementsToAdd_i)
			<>Fnd_Loc_EditorTempStringsEN_at{$element_i}:=$groupName_t
			
			INSERT IN ARRAY:C227(<>Fnd_Loc_EditorTempStringsXX_at;$element_i;$elementsToAdd_i)
			<>Fnd_Loc_EditorTempStringsXX_at{$element_i}:=$groupName_t
			
			For ($codeNumber_i;1;Size of array:C274(<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i}))
				$element_i:=$element_i+1
				<>Fnd_Loc_EditorTempCodes_at{$element_i}:=<>Fnd_Loc_EditorGroupCodes_at{$groupNumber_i}{$codeNumber_i}
				<>Fnd_Loc_EditorTempStringsEN_at{$element_i}:=<>Fnd_Loc_EditorStringsEN_at{$groupNumber_i}{$codeNumber_i}
				<>Fnd_Loc_EditorTempStringsXX_at{$element_i}:=<>Fnd_Loc_EditorStringsXX_at{$groupNumber_i}{$codeNumber_i}
				$xxLength_i:=$xxLength_i+Length:C16(<>Fnd_Loc_EditorTempStringsXX_at{$element_i})
			End for 
		End for 
		
		Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempCodes_at;<>Fnd_Loc_EditorModuleName_t)
		Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempStringsEN_at;<>Fnd_Loc_EditorModuleName_t+"_EN")
		If ($xxLength_i>0)  // We only save this list if it contains something.
			Fnd_Loc_Editor_ArrayToList (-><>Fnd_Loc_EditorTempStringsXX_at;<>Fnd_Loc_EditorModuleName_t+"_"+<>Fnd_Loc_EditorLanguage2Code_t)
		End if 
		
		  // If the list name was changed, mark the old list for deletion.
		If (Error=0)
			If (<>Fnd_Loc_EditorModuleName_t#<>Fnd_Loc_EditorOriginalModName_t)
				ARRAY TEXT:C222(<>Fnd_Loc_EditorTempCodes_at;1)
				<>Fnd_Loc_EditorTempCodes_at{1}:="  ` Please delete this list."
				
				  // BEGIN Modified by : Vincent Tournier (17/06/2011)
				  //LIST TO ARRAY(<>Fnd_Loc_EditorTempCodes_at;<>Fnd_Loc_EditorOriginalModName_t)
				  //SORT ARRAY(<>Fnd_Loc_EditorModules_at;>)
				  //LIST TO ARRAY(<>Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
				Fnd_Loc_ArrayToList (-><>Fnd_Loc_EditorTempCodes_at;<>Fnd_Loc_EditorOriginalModName_t)
				SORT ARRAY:C229(<>Fnd_Loc_EditorModules_at;>)
				Fnd_Loc_ArrayToList (-><>Fnd_Loc_EditorModules_at;"Fnd_Loc_EditorModules*")
				  // END Modified by : Vincent Tournier (17/06/2011)
				
			End if 
		End if 
		
		  // Clear the temporary arrays we used.
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempCodes_at;0)
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)
		ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsXX_at;0)
	End if 
	<>Fnd_Loc_EditorModuleModified_b:=False:C215
End if 