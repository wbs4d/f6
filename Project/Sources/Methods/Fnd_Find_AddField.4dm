//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Find_AddField (->field{; position})

// Lets the developer add a field to the search dialog.  If a position isn't
//   specified, the field is added to the end of the list.

// Access Type: Protected

// Parameters:
//   $1 : Pointer : A pointer to the field to add
//   $2 : Longint : The position in which to add the item (optional)

// Returns: Nothing

// Created by Dave Batton on Jul 20, 2003
// Modified by Doug Hall on Feb 7, 2023 to include a field's table name, if not the current table.
// ----------------------------------------------------

C_POINTER:C301($1; $field_ptr)
C_LONGINT:C283($2; $position_i)

C_POINTER:C301($FieldTable_ptr)
C_TEXT:C284($tableName_t; $fieldName_t)

$field_ptr:=$1
If (Count parameters:C259>=2)
	$position_i:=$2
Else 
	$position_i:=MAXLONG:K35:2
End if 

// JDH 20230207: Include the field's table name, if the field
// is not in the current table, or it's already in the name of the field.

$FieldTable_ptr:=Fnd_Gen_TableOf($field_ptr)
If ($FieldTable_ptr#Fnd_Gen_CurrentTable)
	
	$tableName_t:=Fnd_VS_TableName($FieldTable_ptr)
	$fieldName_t:=Fnd_VS_FieldName($field_ptr)
	// Don't duplicate the table name, if already there!
	If (Position:C15($tableName_t; $fieldName_t)>0)
		Fnd_Find_AddItem($position_i; ""; ""; $field_ptr)
	Else 
		Fnd_Find_AddItem($position_i; $tableName_t+" "+$fieldName_t; ""; $field_ptr)
	End if 
Else 
	Fnd_Find_AddItem($position_i; ""; ""; $field_ptr)
End if 