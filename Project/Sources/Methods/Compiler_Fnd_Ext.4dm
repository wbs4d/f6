//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Ext 

  // Compiler variables related to the Foundation Extra plugin routines.
  // which are now native 4D commands

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Walt Nelson on 10/27/10
  // ----------------------------------------------------

  // Interprocess Variables

  // Process Variables

  // Parameters
If (False:C215)  // So we never run this as code.
	
	C_TEXT:C284(Fnd_Ext_TextLines ;$1;$3)
	C_LONGINT:C283(Fnd_Ext_TextLines ;$0;$2;$4;$5)
	
	C_LONGINT:C283(Fnd_Ext_TextWidth ;$0)
	C_TEXT:C284(Fnd_Ext_TextWidth ;$1;$2)
	C_LONGINT:C283(Fnd_Ext_TextWidth ;$3;$4)
	
	C_REAL:C285(Fnd_SVG_GetStringWidth ;$0)
	C_TEXT:C284(Fnd_SVG_GetStringWidth ;$1)
	C_TEXT:C284(Fnd_SVG_GetStringWidth ;$2)
	C_LONGINT:C283(Fnd_SVG_GetStringWidth ;$3)
	C_LONGINT:C283(Fnd_SVG_GetStringWidth ;$4)
	
	C_REAL:C285(Fnd_SVG_GetStringHeight ;$0)
	C_TEXT:C284(Fnd_SVG_GetStringHeight ;$1)
	C_TEXT:C284(Fnd_SVG_GetStringHeight ;$2)
	C_LONGINT:C283(Fnd_SVG_GetStringHeight ;$3)
	C_LONGINT:C283(Fnd_SVG_GetStringHeight ;$4)
	
End if 