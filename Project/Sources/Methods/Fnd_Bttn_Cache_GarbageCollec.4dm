//%attributes = {"invisible":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_Cache_GarbageCollec

  //If the cache goes over it's maximum size, this method deletes the oldest images 
  //until it's back within it's limit.  This is called whenever an image is added to
  //the cache.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Mark Mitchenall on 20/9/01
  //   Original Method Name: rollover_Cache_GarbageCollect
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // ----------------------------------------------------

C_LONGINT:C283($numImages_i)

If (<>Fnd_Bttn_CacheTotalSize_i><>Fnd_Bttn_CacheMaximumSize_i)
	
	  //Sort the cache so that the newest images are at the start and the oldest images 
	  //are at the end of the array.
	
	SORT ARRAY:C229(<>Fnd_Bttn_Tickcounts_ai;<>Fnd_Bttn_CacheNames_at;<>Fnd_Bttn_CachePictures_apic;<>Fnd_Bttn_PictureWidths_ai;<>Fnd_Bttn_CachePictSizes_ai;<)
	
	$numImages_i:=Size of array:C274(<>Fnd_Bttn_CacheNames_at)
	
	  //Loop until the total cache size is below the maximum cache
	  //size, deleting the oldest images first, but leaving the icon
	  //pictures as these take the longest to generate
	
	While ((<>Fnd_Bttn_CacheTotalSize_i><>Fnd_Bttn_CacheMaximumSize_i) & ($numImages_i>1))
		If (<>Fnd_Bttn_CacheNames_at{$numImages_i}#"iconPicture@")
			Fnd_Bttn_Cache_Image_Delete (<>Fnd_Bttn_CacheNames_at{$numImages_i})
		End if 
		$numImages_i:=$numImages_i-1
	End while 
	
	  //Now delete the IconPictures we're still out of cache space
	
	$numImages_i:=Size of array:C274(<>Fnd_Bttn_CacheNames_at)
	While ((<>Fnd_Bttn_CacheTotalSize_i><>Fnd_Bttn_CacheMaximumSize_i) & ($numImages_i>1))
		Fnd_Bttn_Cache_Image_Delete (<>Fnd_Bttn_CacheNames_at{$numImages_i})
		$numImages_i:=$numImages_i-1
	End while 
End if 