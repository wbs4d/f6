//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_Reset ({feature code})

  // Clears out the start date and other information for the specified feature.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jan 29, 2006
  // ----------------------------------------------------

C_TEXT:C284($1;$featureCode_t)
C_LONGINT:C283($index_i)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

If (Not:C34(Semaphore:C143(<>Fnd_Reg_Semaphore_t;300)))  // Wait up to 5 seconds.
	If ($featureCode_t=<>Fnd_Reg_DefaultFeatureCode_t)
		  // The application feature code should be reset, not deleted.
		<>Fnd_Reg_FeatureCodes_at{1}:=<>Fnd_Reg_DefaultFeatureCode_t
		<>Fnd_Reg_FeatureNames_at{1}:=Fnd_Gen_GetDatabaseInfo ("DatabaseName")
		<>Fnd_Reg_BuyNowURLs_at{1}:=""
		<>Fnd_Reg_DemoMessage_at{1}:=""
		
	Else 
		$index_i:=Fnd_Reg_FeatureCodeIndex ($featureCode_t)
		DELETE FROM ARRAY:C228(<>Fnd_Reg_FeatureCodes_at;$index_i)
		DELETE FROM ARRAY:C228(<>Fnd_Reg_FeatureNames_at;$index_i)
		DELETE FROM ARRAY:C228(<>Fnd_Reg_BuyNowURLs_at;$index_i)
		DELETE FROM ARRAY:C228(<>Fnd_Reg_DemoMessage_at;$index_i)
	End if 
	
	READ WRITE:C146(<>Fnd_Reg_Table_ptr->)
	Fnd_Reg_LoadRegistrationRecord ($featureCode_t)
	DELETE RECORD:C58(<>Fnd_Reg_Table_ptr->)
	
	CLEAR SEMAPHORE:C144(<>Fnd_Reg_Semaphore_t)
End if 
