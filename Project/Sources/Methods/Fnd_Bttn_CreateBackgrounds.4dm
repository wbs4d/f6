//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------

  // Project Method: Fnd_Bttn_CreateBackgrounds


  // Because of the time to do the picture maths for the middle bit of the button, we

  //   will pre-build different sized middle slices and store them in an array.  


  // Access Type: Private


  // Parameters: None


  // Returns: Nothing


  // Created by Mark Mitchenall on 18/9/01

  //   Original Method Name: rollover_CreateBackgrounds

  //   Part of the toolbar_Component, ©2001 mitchenall.com

  //   Used with permission.

  // Modified by Dave Batton on Feb 20, 2004

  //   Updated specifically for use by the Foundation Shell.

  // ----------------------------------------------------

  // Modified: [1] John Craig, 02/07/2014, 13:34:23 Changed | picture operator to COMBINE PICTURES with Superimposition method


C_PICTURE:C286($shiftedSlice_pic)
C_LONGINT:C283($widthNumber_i)

  //1, 2, 4, 8, 16, 32, 64, 128 pixels wide

ARRAY PICTURE:C279(Fnd_Bttn_BG_Middles_apic;8)

  // Get the 1-pixel wide middle portion

Fnd_Bttn_BG_Middles_apic{0}:=Fnd_Bttn_BG_Mid_pic

  // Create middle portions of backgrounds at the various widths in the array.

For ($widthNumber_i;1;8)
	  // Create the next slice by shifting the previous size
	
	  //   and OR'ing it the same piece, unshifted.
	
	$shiftedSlice_pic:=(Fnd_Bttn_BG_Middles_apic{$widthNumber_i-1}+(0 ?+ ($widthNumber_i-1)))
	
	  // Modified: [1] John Craig, 02/07/2014, 13:34:23 Changed | picture operator to COMBINE PICTURES with Superimposition method -->
	  //Fnd_Bttn_BG_Middles_apic{$widthNumber_i}:=Fnd_Bttn_BG_Middles_apic{$widthNumber_i-1} | $shiftedSlice_pic
	COMBINE PICTURES:C987(Fnd_Bttn_BG_Middles_apic{$widthNumber_i};Fnd_Bttn_BG_Middles_apic{$widthNumber_i-1};Superimposition:K61:10;$shiftedSlice_pic)
	  // Modified: [1] John Craig, 02/07/2014, 13:34:23 Changed | picture operator to COMBINE PICTURES with Superimposition method <--
	
	
End for 