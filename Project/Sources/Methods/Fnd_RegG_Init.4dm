//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_RegG_Init

  // Initializes both the process and interprocess variables used by the
  //   Fnd_RegG routines.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Mar 6, 2005
  // ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_RegG_Initialized_b)

If (Not:C34(<>Fnd_RegG_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_RegG 
	
	<>Fnd_RegG_DefaultFeatureCode_t:="APPL"  // This must match a similar variable in Fnd_Reg_Init.
	<>Fnd_RegG_UnlockCodeFormat_t:="####-####-####"  // This must match a similar variable in Fnd_Reg_Init.
	
	<>Fnd_RegG_Initialized_b:=True:C214
End if 
