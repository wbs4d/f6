//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_ListToArray (list name; ->array)

  // A wrapper for 4D's LIST TO ARRAY command.  Tries to read in a
  //   list with an asterisk appended to the name first.  If all it gets is
  //   an empty list (which probably indicates no list) then it tries to
  //   read in a list without the asterisk.
  // See also the Fnd_Loc_Editor_ArrayToList method.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The list name
  //   $2 : Pointer : The text array

  // Returns: Nothing

  // Created by Dave Batton on Jun 13, 2004
  // ----------------------------------------------------

C_TEXT:C284($1;$listName_t)
C_POINTER:C301($2;$array_ptr)

$listName_t:=$1
$array_ptr:=$2

  //ARRAY TEXT($localizations_at;0)  ` $array_ptr is a 2D array, so we can't pass it directly.
  //
  //  ` Try a listname with an asterisk first.
  //EXECUTE METHOD("Fnd_Host_ListToArray";*;$listName_t+"*";->$localizations_at)
  //
  //  ` If it comes back empty, try a the list without the asterisk.
  //If (Size of array($localizations_at)=0)
  //EXECUTE METHOD("Fnd_Host_ListToArray";*;$listName_t;->$localizations_at)
  //End if 
  //
  //COPY ARRAY($localizations_at;$array_ptr->)

  // Try a listname with an asterisk first.
EXECUTE METHOD:C1007("Fnd_Host_ListToArray";*;$listName_t+"*";$array_ptr)

  // If it comes back empty, try a the list without the asterisk.
If (Size of array:C274($array_ptr->)=0)
	EXECUTE METHOD:C1007("Fnd_Host_ListToArray";*;$listName_t;$array_ptr)
End if 
