//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_Active ({activate?}) --> Boolean

// Pass True to activate this component for the current process, or False to deactivate
//   for the current process. May also be called as a function to determine the current state.

// Access Type: Shared

// Parameters: 
//   $1 : Boolean : Activate? (optional)

// Returns: 
//   $0 : Boolean : Active

// Created by Dave Batton on Nov 20, 2005
// ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_Out_Init

If (Count parameters:C259>=1)
	Fnd.out.active:=$1
End if 

$0:=Fnd.out.active

