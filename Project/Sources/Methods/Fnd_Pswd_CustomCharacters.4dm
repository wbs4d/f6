//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Pswd_CustomCharacters ({characters}) --> Text

  // Pass any custom characters that should be used to generate passwords.
  // Returns the currently specified custom characters.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : Custom characters to use (optional)

  // Returns: 
  //   $0 : Text : The current custom characters

  // Created by Dave Batton on Mar 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1)

Fnd_Pswd_Init 

If (Count parameters:C259>=1)
	<>Fnd_Pswd_CustomCharacters_t:=$1
	
	<>Fnd_Pswd_UseCustom_b:=(<>Fnd_Pswd_CustomCharacters_t#"")
End if 

If (Not:C34(<>Fnd_Pswd_UseCustom_b))
	<>Fnd_Pswd_CustomCharacters_t:=""
End if 

$0:=<>Fnd_Pswd_CustomCharacters_t
