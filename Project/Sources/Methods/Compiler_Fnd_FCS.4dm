//%attributes = {"invisible":true}
/* Compiler_Fnd_FCS
Project Method: Compiler_Fnd_FCS

Compile declarations

Created by Wayne Stewart (2019-10-30)
     waynestewart@mac.com
*/

If (False:C215)
	C_TEXT:C284(Fnd_FCS_ListToTextFile; $1)
	C_TEXT:C284(Fnd_FCS_ListToTextFile; $1)
	
	C_OBJECT:C1216(Fnd_Obj_SaveToFile; $1)
	C_TEXT:C284(Fnd_Obj_SaveToFile; $2)
	C_BOOLEAN:C305(Fnd_Obj_SaveToFile; $3)
	
	C_BOOLEAN:C305(Fnd_FCS_ConfirmListExists; $0)
	C_TEXT:C284(Fnd_FCS_ConfirmListExists; $1)
	
	C_TEXT:C284(Fnd_FCS_TrnsListToJSON; $1)
	
	C_TEXT:C284(Fnd_FCS_BuildComponent; $1)
	
	C_TEXT:C284(Fnd_FCS_WriteDocumentation; $1)
	C_BOOLEAN:C305(Fnd_FCS_WriteDocumentation; $2; $3)
	
End if 