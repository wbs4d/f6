//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_File_FilePathToFileName (Path to document) --> File Name

// Takes a fully qualified path name and returns just the file name.

// Access: Shared

// Parameters: 
//   $1 : Text : Path to a document

// Returns: 
//   $0 : Text : The document name

// Created by Wayne Stewart (2016-11-16)
//     waynestewart@mac.com
// ----------------------------------------------------



If (False:C215)
	
	C_TEXT:C284(Fnd_File_FilePathToFileName; $1; $0)
	
End if 

C_TEXT:C284($0; $1; $originalPath)
C_LONGINT:C283($len; $pos; $char; $dirSymbolAscii)

$originalPath:=$1
$dirSymbolAscii:=Character code:C91(Folder separator:K24:12)

$len:=Length:C16($originalPath)
$pos:=0

For ($char; $len; 1; -1)
	If (Character code:C91($originalPath[[$char]])=$dirSymbolAscii)
		$pos:=$char
		$char:=0
	End if 
End for 

If ($pos>0)
	$0:=Substring:C12($originalPath; $pos+1)
Else 
	$0:=$originalPath
End if 