//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Out_OpenDialog

// Called by Fnd_IO_DisplayTable2. Not intended to be called from anywhere else.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 20, 2005
// ----------------------------------------------------


Fnd_Out_Init

var $formName_t; $windowPrefName_t; $processName_t; $tableName_t; $columnsPrefName_t : Text
var $processState_i; $processTime_i; $windowReference_i : Integer
var $listBoxConfiguration_o; $currentTable_es; $formData_o : Object
var $field_ptr; $nil_ptr : Pointer

If (Fnd.out.currentTable=0)
	Fnd.out.currentTable:=Table:C252(Fnd_Gen_CurrentTable)
End if 

$tableName_t:=Table name:C256(Fnd.out.currentTable)

If (Fnd.out.recordIDField=0)
	Fnd.out.recordIDField:=Fnd_Out_getPrimaryKey(Fnd.out.currentTable)
End if 

$field_ptr:=Field:C253(Fnd.out.currentTable; Fnd.out.recordIDField)
$formName_t:="Fnd_Out"

If (Fnd.out.lbox=Null:C1517)  // No definition supplied in Fnd_Hook_IO_DisplayTable
	Fnd.out.lbox:=New object:C1471("columns"; New collection:C1472)
	Fnd_Out_PickOutputColumns
End if 

// This first part overrides the Fnd_Wnd_UseSavedPosition call made by Fnd_IO_DisplayTable2.
PROCESS PROPERTIES:C336(Current process:C322; $processName_t; $processState_i; $processTime_i)  // DB041109 - Added.
$windowPrefName_t:="Fnd_IO: "+$processName_t  // DB041109 - Added.
$columnsPrefName_t:="Fnd_Out_"+Fnd_Text_Capitalize($tableName_t)+"Default"

Fnd_FCS_Trace
$listBoxConfiguration_o:=Fnd_Pref_GetObject($columnsPrefName_t)

Fnd_Wnd_UseSavedPosition($windowPrefName_t)  // DB041109 - Added the preference name.
$windowReference_i:=Fnd_Wnd_OpenFormWindow($nil_ptr; $formName_t)

CALL FORM:C1391($windowReference_i; "Fnd_Out_FormMethod"; (0-On Load:K2:1))  // Finalise setting up after the form has loaded

$currentTable_es:=ds:C1482[$tableName_t].all()

$formData_o:=New object:C1471(\
"listData"; $currentTable_es.copy(ck shared:K85:29); \
"tableNum"; Fnd.out.currentTable; \
"sortBy"; ""; \
"tablePtr"; Fnd_Gen_CurrentTable_ptr; \
"tableName"; Fnd_VS_TableName(Fnd_Gen_CurrentTable_ptr); \
"winRef"; $windowReference_i; \
"lbox"; OB Copy:C1225(Fnd.out.lbox); \
"listBoxConfiguration"; $listBoxConfiguration_o)  // Copy the information across (not certain if this is required)

Fnd_Menu_Window_Add  // Add this process to the Windows menu.

EXECUTE METHOD:C1007("FND_Host_IO_DisplayDialog"; *; $formName_t; $formData_o; False:C215)

Fnd_Menu_Window_Remove
Fnd_FCS_Trace

Fnd_Pref_SetWindow($windowPrefName_t)
Fnd_Pref_SetObject($columnsPrefName_t; $formData_o.listBoxConfiguration)

Fnd_Gen_CurrentFormType(Fnd_Gen_NoForm)
Fnd_Gen_MenuBar
CLOSE WINDOW:C154
