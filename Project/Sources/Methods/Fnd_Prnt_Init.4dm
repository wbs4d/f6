//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Prnt_Init

// Initializes both the process and interprocess variables used by the Printing routines.
// Designed to be called at the beginning of any Protected routines to make sure
//   the necessary variables are initialized.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 5, 2004
// Modified by Dave Batton on Jun 16, 2004
//   Updated the version number to version 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Prnt_Info method.
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Prnt_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Prnt_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Prnt
	
	<>Fnd_Prnt_Initialized_b:=True:C214
End if 
