//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_FCS_TrnsListToJSON

// Another quick and dirty routine that MUST NOT BE USED when deployed

// Access: Shared

// Parameters: 
//   $1 : Text : Translation List Name

// Created by Wayne Stewart (2021-04-13)

//     waynestewart@mac.com
// ----------------------------------------------------


C_TEXT:C284($1)

C_LONGINT:C283($Count_i; $CurrTranslation_i; $List_i; $ListPos_i; $TranslationCount_i)
C_TEXT:C284($FileName_t; $ListName_t)
C_OBJECT:C1216($Final_o)

ARRAY LONGINT:C221($ListRefNumbers_ai; 0)
ARRAY TEXT:C222($Languages_at; 0)
ARRAY TEXT:C222($ListNames_at; 0)
ARRAY TEXT:C222($MasterList_at; 0)
ARRAY TEXT:C222($MatchingLists_at; 0)
ARRAY TEXT:C222($Translations_at; 0)

If (False:C215)
	C_TEXT:C284(Fnd_FCS_TrnsListToJSON; $1)
End if 

If (Count parameters:C259=1)
	$ListName_t:=$1
Else 
	$ListName_t:="Fnd_Gen"  //  Just give a default
End if 

LIST OF CHOICE LISTS:C957($ListRefNumbers_ai; $ListNames_at)

$Count_i:=Count in array:C907($ListNames_at; $ListName_t+"@")

If ($Count_i>1)
	$ListPos_i:=-1
	For ($List_i; 1; $Count_i)
		
		$ListPos_i:=Find in array:C230($ListNames_at; $ListName_t+"@"; $ListPos_i+1)
		
		APPEND TO ARRAY:C911($MatchingLists_at; $ListNames_at{$ListPos_i})
		
	End for 
	
	COPY ARRAY:C226($MatchingLists_at; $Languages_at)
	$Count_i:=Size of array:C274($Languages_at)
	
	For ($List_i; $Count_i; 1; -1)
		If ($Languages_at{$List_i}=$ListName_t)
			DELETE FROM ARRAY:C228($Languages_at; $List_i)
		Else 
			$Languages_at{$List_i}:=Replace string:C233($Languages_at{$List_i}; $ListName_t+"_"; "")
			If (Length:C16($Languages_at{$List_i})>2)  // Languages are only two characters long
				DELETE FROM ARRAY:C228($Languages_at; $List_i)
			End if 
		End if 
	End for 
	
	$Count_i:=Size of array:C274($Languages_at)
	ARRAY OBJECT:C1221($Translations_ao; $Count_i)
	
	LIST TO ARRAY:C288($ListName_t; $MasterList_at)
	
	For ($List_i; 1; $Count_i)
		LIST TO ARRAY:C288($ListName_t+"_"+$Languages_at{$List_i}; $Translations_at)
		
		$TranslationCount_i:=Size of array:C274($MasterList_at)
		$Translations_ao{$List_i}:=New object:C1471
		For ($CurrTranslation_i; 1; $TranslationCount_i)
			OB SET:C1220($Translations_ao{$List_i}; $MasterList_at{$CurrTranslation_i}; $Translations_at{$CurrTranslation_i})
		End for 
		
	End for 
	
	$Final_o:=New object:C1471("Module"; $ListName_t)
	
	For ($List_i; 1; $Count_i)
		OB SET:C1220($Final_o; $Languages_at{$List_i}; $Translations_ao{$List_i})
		
	End for 
	
	$FileName_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Translations"+Folder separator:K24:12
	
	If (Test path name:C476($FileName_t)#Is a folder:K24:2)
		CREATE FOLDER:C475($FileName_t; *)
	End if 
	
	$FileName_t:=$FileName_t+$ListName_t+".json"
	Fnd_Obj_SaveToFile($Final_o; $FileName_t; True:C214)
	
	If (Is compiled mode:C492)
	Else 
		SHOW ON DISK:C922($FileName_t; *)
	End if 
End if 