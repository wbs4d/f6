//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_GetIcon (name{; state}) --> Picture

  // This method just wraps the GET PICTURE FROM LIBRARY command, but if  
  //   you wanted to store your pictures in resources or some other way, you  
  //   could modify this command for getting the pictures.

  // Access Type: Private

  // Parameters: 
  //   $1 : Text : The picture name
  //   $2 : Text : The icon state: "selected" or "disabled" (optional)

  // Returns: 
  //   $0 : Picture : The picture

  // Created by Mark Mitchenall on 20/9/01
  //   Original Method Name: rollover_GetIcon
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // Modified by Dave Batton on Jan 8, 2005
  //   Added the second parameter to specify the icon state to return.
  // [1] Added by: John Craig (12/10/09) - to use external image library
  // [2] Modified by: John Craig (12/10/09) - replaced local variable with process variable
  // [3] Modified by Wayne Stewart, 2016-11-24 - Looks in alternative location for icons before default loaction
  // [4] Modified by Wayne Stewart, 2018-04-16 - Checks if file is present etc
  // ----------------------------------------------------

C_PICTURE:C286($0;Fnd_Bttn_IconImage_pic)  // [2]
C_TEXT:C284($1;$2;$iconName_t)

$iconName_t:=$1

Fnd_Bttn_Init 

Case of 
	: (Count parameters:C259=1)
		$iconName_t:=$iconName_t+"_a"
	: ($2="selected")
		$iconName_t:=$iconName_t+"_b"
	: ($2="disabled")
		$iconName_t:=$iconName_t+"_c"
	Else 
		$iconName_t:=$iconName_t+"_a"
End case 

Case of 
	: (Test path name:C476(<>Fnd_Bttn_AltPicturePath_t+$iconName_t+".png")=Is a document:K24:1)  // [3],[4]
		READ PICTURE FILE:C678(<>Fnd_Bttn_AltPicturePath_t+$iconName_t+".png";Fnd_Bttn_IconImage_pic)
		
	: (Test path name:C476(<>Fnd_Bttn_PicturePath_t+$iconName_t+".png")=Is a document:K24:1)  // [1],[4]
		READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$iconName_t+".png";Fnd_Bttn_IconImage_pic)
		
	Else 
		GET PICTURE FROM LIBRARY:C565($iconName_t;Fnd_Bttn_IconImage_pic)  // [2]
		
End case 

If (Picture size:C356(Fnd_Bttn_IconImage_pic)=0)  // [2]
	Fnd_Gen_RequiredHostMethod ("Fnd_Host_GetPictureFromLibrary")
	
	  // See if the icon exists in the host database's picture library.
	If (<>Fnd_Gen_RunningInHost_b)  // was If (Structure file(*)="@Fnd_Bttn.4DB") - Modified by: Walt Nelson (2/19/10)
		  // Guy Algot, 02/22/09, 21:45:40
		  // updated this so it looks in the component picture lib
		If (<>Fnd_Bttn_PicturePath_t#"")  // [1]
			READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$iconName_t;Fnd_Bttn_IconImage_pic)  // [1]
		Else 
			GET PICTURE FROM LIBRARY:C565($iconName_t;Fnd_Bttn_IconImage_pic)  // [2]
			
		End if 
		
	Else 
		EXECUTE METHOD:C1007("Fnd_Host_GetPictureFromLibrary";*;$iconName_t;->Fnd_Bttn_IconImage_pic)  // [2]
	End if 
	
	If (Picture size:C356(Fnd_Bttn_IconImage_pic)=0)
		If ($iconName_t#"_a")
			$iconName_t:=Change string:C234($iconName_t;"a";Length:C16($iconName_t))  // Change the "_b" or "_c" to "_a".
			If (<>Fnd_Bttn_PicturePath_t#"")  // [1]
				READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$iconName_t;Fnd_Bttn_IconImage_pic)  // [1]
			Else 
				GET PICTURE FROM LIBRARY:C565($iconName_t;Fnd_Bttn_IconImage_pic)  // [2]
				
			End if 
			
			
			If (Picture size:C356(Fnd_Bttn_IconImage_pic)=0)
				  // See if the icon exists in the host database's picture library.
				If (<>Fnd_Gen_RunningInHost_b)  // was If (Structure file(*)#"Foundation_@") - Modified by: Walt Nelson (2/19/10)
					
					If (<>Fnd_Bttn_PicturePath_t#"")  // [1]
						READ PICTURE FILE:C678(<>Fnd_Bttn_PicturePath_t+$iconName_t;Fnd_Bttn_IconImage_pic)  // [1]
					Else 
						GET PICTURE FROM LIBRARY:C565($iconName_t;Fnd_Bttn_IconImage_pic)  // [2]
					End if 
					
					
				Else 
					EXECUTE METHOD:C1007("Fnd_Host_GetPictureFromLibrary";*;$iconName_t;->Fnd_Bttn_IconImage_pic)  // [2]
				End if 
			End if 
		End if 
	End if 
End if 

$0:=Fnd_Bttn_IconImage_pic  // [2]