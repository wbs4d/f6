//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_CenterWindow (width; height{; type{; position{; title{; close box?}}}})

// Opens a new window centered on the screen or over the frontmost window
//   (depending on the value of Fnd_Gen_WindowPosition_i).

// Access Type: Shared

// Parameters: 
//   $1 : Longint : Width of the new window
//   $2 : Longint : Height of the new window
//   $3 : Longint : The window type (optional)
//   $4 : Longint : Window position (optional)
//   $5 : Text : The window title (optional)
//   $6 : Boolean : Has a close box (optional)

// The window position can be one of these:
//   1 = Fnd_Wnd_CenterOnScreen = Centered on screen
//   2 = Fnd_Wnd_CenterOnWindow = Centered over frontmost window

// Returns: 
//   $0 : Longint : The window reference number

// Created by Dave Batton on Jul 6, 2003
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable call 
// ----------------------------------------------------

C_LONGINT:C283($0;$1;$2;$3;$4;$width_i;$height_i;$type_i)
C_LONGINT:C283($backLeft_i;$backTop_i;$backRight_i;$backBottom_i;$position_i)
C_TEXT:C284($5;$title_t)
C_BOOLEAN:C305($6;$hasCloseBox_b)

var $parameters_o : Object

$width_i:=$1
$height_i:=$2

If (Count parameters:C259>=3)  // A window type was specified.
	$type_i:=$3
Else 
	$type_i:=Modal form dialog box:K39:7
End if 

If (Count parameters:C259>=4)
	$position_i:=$4
Else 
	$position_i:=Fnd_Wnd_CenterOnScreen  // Default to centered on screen.
End if 

If (Count parameters:C259>=5)
	$title_t:=$5
Else 
	$title_t:=""
End if 

If (Count parameters:C259>=6)
	$hasCloseBox_b:=$6
Else 
	$hasCloseBox_b:=False:C215
End if 

Fnd_Gen_Init

// If $position_i = 2, then use the coordinates of the frontmost window
//   of the current process as the rect on which to center the new window.  Otherwise,
//   we'll center the new window on the main monitor.
If ($position_i>=2)
	GET WINDOW RECT:C443($backLeft_i;$backTop_i;$backRight_i;$backBottom_i;Frontmost window:C447)
Else 
	$backLeft_i:=0
	$backTop_i:=Menu bar height:C440
	$backRight_i:=Screen width:C187
	$backBottom_i:=Screen height:C188
End if 


Fnd_Gen_Left_i:=(($backRight_i-$backLeft_i-$width_i)\2)+$backLeft_i
Fnd_Gen_Right_i:=Fnd_Gen_Left_i+$width_i
Fnd_Gen_Top_i:=(($backBottom_i-$backTop_i-$height_i)\3)+$backTop_i
Fnd_Gen_Bottom_i:=Fnd_Gen_Top_i+$height_i

$parameters_o:=New object:C1471
$parameters_o.Left:=Fnd_Gen_Left_i
$parameters_o.Top:=Fnd_Gen_Top_i
$parameters_o.Right:=Fnd_Gen_Right_i
$parameters_o.Bottom:=Fnd_Gen_Bottom_i
$parameters_o.windowType:=$type_i

Fnd_Wnd_MoveOnScreen($parameters_o)  //(->Fnd_Gen_Left_i;->Fnd_Gen_Top_i;->Fnd_Gen_Right_i;->Fnd_Gen_Bottom_i;$type_i)

Fnd_Gen_Left_i:=$parameters_o.Left
Fnd_Gen_Top_i:=$parameters_o.Top
Fnd_Gen_Right_i:=$parameters_o.Right
Fnd_Gen_Bottom_i:=$parameters_o.Bottom

If ($hasCloseBox_b)
	$0:=Open window:C153(Fnd_Gen_Left_i;Fnd_Gen_Top_i;Fnd_Gen_Right_i;Fnd_Gen_Bottom_i;$type_i;$title_t;"Fnd_Gen_DummyMethod")
Else 
	$0:=Open window:C153(Fnd_Gen_Left_i;Fnd_Gen_Top_i;Fnd_Gen_Right_i;Fnd_Gen_Bottom_i;$type_i;$title_t)
End if 
