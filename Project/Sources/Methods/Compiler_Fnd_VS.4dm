//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_VS

  // Compiler variables related to the Foundation Virtual Structure routines.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Aug 28, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_VS_Initialized_b)
If (Not:C34(<>Fnd_VS_Initialized_b))
	C_TEXT:C284(<>Fnd_VS_Semaphore_t)
	
	ARRAY TEXT:C222(<>Fnd_VS_TableNames_at;0)
	ARRAY INTEGER:C220(<>Fnd_VS_TableNumbers_ai;0)
	ARRAY TEXT:C222(<>Fnd_VS_FieldNames_at;0;0)
	ARRAY INTEGER:C220(<>Fnd_VS_FieldNumbers_ai;0;0)
	
	ARRAY TEXT:C222(<>Fnd_VS_HostFieldNames_at;0)  // Modified by : Vincent TOURNIER(9/12/11)
	ARRAY TEXT:C222(<>Fnd_VS_ExistFieldNames_at;0)  // Modified by : Vincent TOURNIER(9/12/11)
	ARRAY TEXT:C222(<>Fnd_VS_TmpFieldsNames_at;0)  // Modified by : Vincent TOURNIER(9/12/11)
	
	C_POINTER:C301(<>Fnd_VS_HostArray_ptr)  // Modified by : Vincent TOURNIER(9/12/11)
End if 


  // Process Variables
C_BOOLEAN:C305(Fnd_VS_Initialized_b)
If (Not:C34(Fnd_VS_Initialized_b))
	C_TEXT:C284(Fnd_VS_OldErrorHandler_t;Fnd_VS_CurrentTable_t)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_LONGINT:C283(Fnd_VS_NbrValidFields ;$0)
	C_LONGINT:C283(Fnd_VS_NbrValidFields ;$1)
	
	C_POINTER:C301(Fnd_VS_AddTable ;$1)
	
	C_TEXT:C284(Fnd_VS_FieldName ;$0)
	C_POINTER:C301(Fnd_VS_FieldName ;$1)
	
	C_POINTER:C301(Fnd_VS_GetFields ;$1;$2;$3)
	
	C_POINTER:C301(Fnd_VS_GetTables ;$1;$2)
	
	C_TEXT:C284(Fnd_VS_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_VS_ReplaceString ;$1;$2)
	
	C_POINTER:C301(Fnd_VS_SetFieldTitle ;$1)
	C_TEXT:C284(Fnd_VS_SetFieldTitle ;$2)
	
	C_POINTER:C301(Fnd_VS_SetFieldTitles ;$1;$2;$3)
	
	C_POINTER:C301(Fnd_VS_SetTableTitle ;$1)
	C_TEXT:C284(Fnd_VS_SetTableTitle ;$2)
	C_POINTER:C301(Fnd_VS_SetTableTitles ;$1;$2)
	
	C_POINTER:C301(Fnd_VS_ShowTable ;$1)
	
	C_TEXT:C284(Fnd_VS_TableName ;$0)
	C_POINTER:C301(Fnd_VS_TableName ;$1)
	
	C_BOOLEAN:C305(Fnd_VS_WasSubtable ;$0)
	C_POINTER:C301(Fnd_VS_WasSubtable ;$1)
End if 