//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_SaveRecord (product code)

  // Saves the current [Fnd_Reg] record after first setting the Checksum field.
  // Assumes the record is loaded in read/write mode.
  // Leaves the record loaded.
  // Use Fnd_Reg_VerifyRecord to verify the checksum.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The secret product code

  // Returns: Nothing

  // Created by Dave Batton on Apr 24, 2005
  // Note by Gary Boudreaux on Dec 22, 2008
  //   NOTE: does this method need to check for deleted fields?
  // Modified by: Walt Nelson (3/16/10) per Barclay Berry 
  //  mac string(!1/1/2010!) will return 1/1/10 and on windows it will return 1/1/2010
  // ----------------------------------------------------

C_TEXT:C284($1;$secretKey_t;$text_t)
C_LONGINT:C283($field_i)
C_POINTER:C301($field_ptr)

$secretKey_t:=$1  // Pass this rather than build it in, just to make it a little more obscure.

  // We require the developer to call Fnd_Reg_SetSecretKey first, so <>Fnd_Reg_ProductCode_t should have a value.
If ($secretKey_t="")
	Fnd_Gen_BugAlert (Current method name:C684;"Fnd_Reg_SetSecretKey must be called before calling any other "+"Foundation Registration routines.")
End if 

<>Fnd_Reg_ChecksumFld_ptr->:=""  // Clear the existing checksum so it doesn't get re-included in the MD5.

$text_t:=$secretKey_t
For ($field_i;1;Get last field number:C255(<>Fnd_Reg_Table_ptr))
	$field_ptr:=Field:C253(Table:C252(<>Fnd_Reg_Table_ptr);$field_i)
	
	Case of 
		: ($field_ptr=(<>Fnd_Reg_ChecksumFld_ptr))
			  // Don't use this in the calculation.
			
		: (Type:C295($field_ptr->)=Is date:K8:7)  // Modified by: Walt Nelson (3/16/10)
			$text_t:=$text_t+String:C10(Year of:C25($field_ptr->))+String:C10(Month of:C24($field_ptr->))+String:C10(Day of:C23($field_ptr->))
			  //End Modified by: Walt Nelson (3/16/10)
		: (Type:C295($field_ptr->)#Is alpha field:K8:1)  // We're not using text fields or Booleans.
			$text_t:=$text_t+String:C10($field_ptr->)  // So this works on all other data types.
			
		Else 
			$text_t:=$text_t+$field_ptr->
	End case 
	
End for 

<>Fnd_Reg_ChecksumFld_ptr->:=Fnd_Text_TextToMD5 ($text_t)

SAVE RECORD:C53(<>Fnd_Reg_Table_ptr->)
UNLOAD RECORD:C212(<>Fnd_Reg_Table_ptr->)
FLUSH CACHE:C297
