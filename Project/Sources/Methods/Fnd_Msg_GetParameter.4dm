//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_GetParameter (parameters string; num) --> Text

  // Gets parameters out of the message.  Also see Fnd_Msg_PackParameters.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : A string full of parameters
  //   $1 : Number : The parameter number to get

  // Returns: 
  //   $0 : Text : The parameter value

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$parameterList_t;$parameterValue_t)
C_LONGINT:C283($2;$parameterNumber_i;$position_i;$parameter_i)

$parameterList_t:=$1
$parameterNumber_i:=$2

Fnd_Msg_Init 

$parameterValue_t:=""

  // We need to add an extra delimiter to the end of the parameter list to make this
  //   work.
$parameterList_t:=$parameterList_t+<>Fnd_Msg_ParamDelimiter_t

For ($parameter_i;1;$parameterNumber_i)
	$position_i:=Position:C15(<>Fnd_Msg_ParamDelimiter_t;$parameterList_t)
	If ($position_i>0)
		$parameterValue_t:=Substring:C12($parameterList_t;1;$position_i-1)
		$parameterList_t:=Substring:C12($parameterList_t;$position_i+Length:C16(<>Fnd_Msg_ParamDelimiter_t))
	End if 
End for 

$0:=$parameterValue_t