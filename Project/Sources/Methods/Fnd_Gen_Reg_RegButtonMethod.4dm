//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_RegButtonMethod

  // Called from the OK buttons in the "Fnd_Gen_Reg_RegisterDialog" form.

  // Method Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on May 23, 2004
  // ----------------------------------------------------

Case of 
	: (Form event:C388=On Clicked:K2:4)
		Case of 
			: (<>Fnd_Gen_Reg_UserName_t="")
				Case of 
					: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
						Fnd_Gen_Reg_Alert ("Il faut saisir le nom du développeur associé avec votre code d'activation.")
					Else 
						Fnd_Gen_Reg_Alert ("You must enter the developer name associated with your activation code.")
				End case 
				
			: (<>Fnd_Gen_Reg_ActivationCode_t="")
				GOTO OBJECT:C206(<>Fnd_Gen_Reg_ActivationCode_t)
				Case of 
					: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
						Fnd_Gen_Reg_Alert ("Il faut saisir un code d'activation pour enregistrer ce produit.")
					Else 
						Fnd_Gen_Reg_Alert ("You must enter an activation code to register this product.")
				End case 
				
			Else 
				Fnd_Gen_Reg_Register (<>Fnd_Gen_Reg_ActivationCode_t;<>Fnd_Gen_Reg_UserName_t)
				
				Case of 
					: (OK=1)
						Case of 
							: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
								Fnd_Gen_Reg_Alert ("Nous vous remercions d'avoir enregistré Foundation !")
							Else 
								Fnd_Gen_Reg_Alert ("Thank you for registering Foundation 5!")
						End case 
						ACCEPT:C269
						
					: (Error=18901)
						Case of 
							: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
								Fnd_Gen_Reg_Alert ("Le nom d'utilisateur ou le code d'activation est invalide.")
							Else 
								Fnd_Gen_Reg_Alert ("The user name or activation code is invalid.")
						End case 
						
					Else 
						Case of 
							: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
								Fnd_Gen_Reg_Alert ("Une erreur est survenue.")
							Else 
								Fnd_Gen_Reg_Alert ("An error has occurred.")
						End case 
				End case 
		End case 
End case 
