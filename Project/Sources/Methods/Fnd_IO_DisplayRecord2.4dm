//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_DisplayRecord2 (->table; record number{; calling process})

// Called from Fnd_IO_DisplayRecord to display an input form in a new window.
//   If a calling process number is passed ($3) then that process is called if
//   the record is saved.  In this case, Fnd_IO_AddRecordNumber_i will
//   contain the record number of the modified record.

// Access: Private

// Parameters: 
//   $1 : Pointer : A pointer to the table to display
//   $2 : Longint : Record number
//   $3 : Longint : Number of the calling process (optional)

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Feb 17, 2004
//   Moved the INPUT FORM command so it gets called after the developer hook.
// Modified by Dave Batton on Dec 11, 2004
//   The input window now remembers its position.
// Modified by Dave Batton on Jan 6, 2005
//   Changed <>Fnd_IO_DisplayNavButtons_b to Fnd_IO_DisplayNavButtons_b.
//   Modified so multiple new records will be added if Fnd_IO_AddMultipleRecords_b is True.
//   Changed a call to Fnd_Rec_Alert to Fnd_Dlg_Alert.
// Modified by Dave Batton on Feb 23, 2005
//   Added additional support for the Grid component.
// Modified by Dave Batton on Feb 27, 2005
//   Made a change to fix a problem with the named selection we pass from the calling process.
//   We no longer use the output form's selection if we're adding a new record.
// Modified by Dave Batton on Apr 22, 2005
//   This method will now display the record even if it's in read-only mode.
//   Updated to use the new Fnd_Wnd methods.
// Modified by Dave Batton on Oct 3, 2005
//   Added support for the Wayne Stewart's LBSF component.
// [1] Added by: John Craig (12/14/09)
// Mod by Wayne Stewart, (2022-04-22) - Removed Fnd_Grid and LBSF code
// ----------------------------------------------------

C_POINTER:C301($1)
C_LONGINT:C283($2;$3;$recordNumber_i;$callingProcess_i)
C_BOOLEAN:C305($4)  // [1]
C_TEXT:C284($windowTitle_t;$windowPrefName_t)

Fnd_Gen_CurrentTable($1)  // Set the current table for this process.

$recordNumber_i:=$2
If (Count parameters:C259>=3)
	$callingProcess_i:=$3
Else 
	$callingProcess_i:=-1
End if 

Fnd_IO_Init

If (Count parameters:C259>=4)  // [1]
	Fnd_IO_ShowLockedMessage_b:=$4  // [1]
End if   // [1]

// The first thing we want to do is get the named selection from the calling
//   process (see Fnd_IO_DisplayRecord) so it can stop waiting for us.
If ((Fnd_IO_DisplayNavButtons_b) & ($recordNumber_i#New record:K29:1))  // This stuff is only needed to display the navigation buttons.
	USE NAMED SELECTION:C332("<>Fnd_IO_PassSelection")
End if 
<>Fnd_IO_PassSelectionReceived_b:=True:C214  // DB050227 - Moved this outside of the If statement.

Fnd_Gen_CurrentFormType(Fnd_Gen_InputForm)

Fnd_Gen_MenuBar  // Install the menu bar for the current process

Fnd_Wnd_SendCloseRequests  // Send close window requests to this process.

$windowTitle_t:=Fnd_IO_GetWindowTitle(Fnd_Gen_CurrentTable;$recordNumber_i)

Fnd_Wnd_Title($windowTitle_t)  // DB050422 - Changed from Fnd_Wnd_SetTitle.
Fnd_Wnd_Position(Fnd_Wnd_StackedOnWindow)  // DB050422 - Changed from Fnd_Wnd_SetPosition.
Fnd_Wnd_Type(Regular window:K27:1)  // DB050422 - Changed from Fnd_Wnd_SetType.
Fnd_Wnd_CloseBox(True:C214)  // DB050422 - Changed from Fnd_Wnd_SetCloseBox.

$windowPrefName_t:="Fnd_IO: "+Table name:C256(Fnd_Gen_CurrentTable)+" Detail"  // I intentionally didn't use the VS table name and didn't localize.
Fnd_Wnd_UseSavedPosition($windowPrefName_t)  // DB041210 - Added

// Find the record now, so it's available to the hook.
If (Not:C34(Fnd_IO_DisplayNavButtons_b))  // DB040920
	If ($recordNumber_i#New record:K29:1)
		GOTO RECORD:C242(Fnd_Gen_CurrentTable->;$recordNumber_i)
	End if 
End if 

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_DisplayRecord")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_IO_DisplayRecord";*)  // The developer hook. Put here so the developer can override window settings.
End if 

// Set the default form.
FORM SET INPUT:C55(Fnd_Gen_CurrentTable->;Fnd_IO_InputFormName_t)

Fnd_Wnd_OpenFormWindow(Fnd_Gen_CurrentTable;Fnd_IO_InputFormName_t)
Fnd_Menu_Window_Add

If ($recordNumber_i=New record:K29:1)
	Repeat   // DB050124 - Added this loop.
		ADD RECORD:C56(Fnd_Gen_CurrentTable->;*)
		If (OK=1)  // DB050124 - Moved this into the loop.
			Fnd_IO_RecordEdited(Fnd_Gen_CurrentTable;$callingProcess_i)
		End if 
	Until ((OK=0) | (Not:C34(Fnd_IO_AddMultipleRecords_b)))
Else 
	If (Records in selection:C76(Fnd_Gen_CurrentTable->)=0)
		Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)  // DB05006 - Added ` DB050422 - Changed from Fnd_Wnd_SetPosition.
		Fnd_Dlg_Alert(Fnd_Loc_GetString("Fnd_Rec";"RecordNotFound"))  // DB05006 - Changed from Fnd_Rec_Alert to Fnd_Dlg_Alert
		OK:=0  // To avoid the call process below.
	Else 
		If ((Locked:C147(Fnd_Gen_CurrentTable->)) | (Read only state:C362(Fnd_Gen_CurrentTable->)))  // DB050422 - Added read-only check.
			DIALOG:C40(Fnd_Gen_CurrentTable->;Fnd_IO_InputFormName_t)
		Else 
			MODIFY RECORD:C57(Fnd_Gen_CurrentTable->;*)
			If (OK=1)  // DB050124 - Moved this too.
				Fnd_IO_RecordEdited(Fnd_Gen_CurrentTable;$callingProcess_i)
			End if 
		End if 
	End if 
End if 

Fnd_IO_ShowLockedMessage_b:=True:C214  // [1]

Fnd_Menu_Window_Remove
Fnd_Pref_SetWindow($windowPrefName_t)  // DB041210 - Added
CLOSE WINDOW:C154
