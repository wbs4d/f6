//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Bttn_Cache_Startup

  // These arrays keep track of those Rollovers we've created in the application.  We
  //   will only be rollovers as well need them, then not creating the picture again.  
  //   This allows us to easily switch languages and have all the text in the rollovers
  //   be easily converted without storing loads of pictures in different languages.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Mark Mitchenall on 17/9/01
  //   Original Method Name: rollover_Cache_Startup
  //   Part of the toolbar_Component, ©2001 mitchenall.com
  //   Used with permission.
  // Modified by Dave Batton on Feb 20, 2004
  //   Updated specifically for use by the Foundation Shell.
  // ----------------------------------------------------


ARRAY TEXT:C222(<>Fnd_Bttn_CacheNames_at;0)  //                 composite name of the image
ARRAY PICTURE:C279(<>Fnd_Bttn_CachePictures_apic;0)  //        actual images
ARRAY INTEGER:C220(<>Fnd_Bttn_PictureWidths_ai;0)  //width of the images
ARRAY LONGINT:C221(<>Fnd_Bttn_CachePictSizes_ai;0)  //  size of the images
ARRAY LONGINT:C221(<>Fnd_Bttn_Tickcounts_ai;0)  //     tickcounts of when image last used

  //at startup, the cache is empty
<>Fnd_Bttn_CacheTotalSize_i:=0

  //these values can help you tune your cache to get the best performance
  //from it.
<>Fnd_Bttn_CacheTotalHits_i:=0
<>Fnd_Bttn_CacheTotalMisses_i:=0