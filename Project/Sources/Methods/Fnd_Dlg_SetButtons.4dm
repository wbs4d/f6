//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_SetButtons ({button 1{; button 2{; button 3}}})

  // Sets the button texts for the upcoming alerts.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The OK button text or "" or "*" for localized "OK" (optional)
  //   $2 : Text : The Cancel button text or "*" for localized "Cancel" (optional)
  //   $3 : Text : The third button text (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 4, 2003
  // ----------------------------------------------------

C_TEXT:C284($1;$2;$3)

Fnd_Dlg_Init 

If (Count parameters:C259>=1)
	Fnd_Dlg_Button1_t:=$1
	
	If (Count parameters:C259>=2)
		Fnd_Dlg_Button2_t:=$2
		
		If (Count parameters:C259>=3)
			Fnd_Dlg_Button3_t:=$3
		Else 
			Fnd_Dlg_Button3_t:=""
		End if 
		
	Else 
		Fnd_Dlg_Button2_t:=""
	End if 
	
Else 
	Fnd_Dlg_Button1_t:="*"
End if 