//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_SetFieldTitle (->field; title)

  // Allows the developer to set a virtual field title for a single field.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : The field to rename
  //   $2 : Text : The virtual title

  // Returns: None

  // Created by Dave Batton on Jan 24, 2004
  // Modified by Gary Boudreaux on Dec 8, 2008
  //   Initialized $tableNumber_i and tested for valid $fieldNumber_i
  // Modified by: Walt Nelson (2/11/10) - added * to SET FIELD TITLES
  // ----------------------------------------------------

C_POINTER:C301($1;$table_ptr;$field_ptr)
C_TEXT:C284($2;$title_t)
C_LONGINT:C283($tableNumber_i;$fieldNumber_i;$element_i)

$field_ptr:=$1
$title_t:=$2

Fnd_VS_Init 

$tableNumber_i:=Table:C252($field_ptr)  //GB20081208 - initialized $tableNumber_i

If (Is table number valid:C999($tableNumber_i))
	If (Not:C34(Semaphore:C143(<>Fnd_VS_Semaphore_t;300)))  // Wait up to 5 seconds.
		$tableNumber_i:=Table:C252($field_ptr)
		$table_ptr:=Table:C252($tableNumber_i)
		$fieldNumber_i:=Field:C253($field_ptr)
		
		If (Is field number valid:C1000($tableNumber_i;$fieldNumber_i))  //GB20081208 - check $fieldNumber_i
			$element_i:=Find in array:C230(<>Fnd_VS_FieldNumbers_ai{$tableNumber_i};$fieldNumber_i)
			If ($element_i>0)
				<>Fnd_VS_FieldNames_at{$tableNumber_i}{$element_i}:=$title_t
			End if 
			
			SET FIELD TITLES:C602($table_ptr->;<>Fnd_VS_FieldNames_at{$tableNumber_i};<>Fnd_VS_FieldNumbers_ai{$tableNumber_i};*)  // WN100211 - added *
			
			CLEAR SEMAPHORE:C144(<>Fnd_VS_Semaphore_t)
		End if 
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"A timeout occurred while waiting for the semaphore.")
	End if 
End if 