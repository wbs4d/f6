//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Data_FormatError --> Longint

  // Allows the developer to detect if any of the data formatting routines
  //   had a problem trying to format data.

  // Access: Shared

  // Parameters: None

  // Returns: 
  //   $0 : Longint : Last formatting error

  // Created by Dave Batton on May 13, 2004
  // ----------------------------------------------------

C_LONGINT:C283($0)

Fnd_Data_Init 

$0:=Fnd_Data_FormatError_i