//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_DisplayTable2 (->table)

// Called from Fnd_IO_DisplayTable to display an output form in a new window.

// Access: Private

// Parameters: 
//   $1 : Pointer : A pointer to the table to display

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Feb 13, 2004
//   Moved the OUTPUT FORM command so it gets called after the developer hook.
//   Added a call to INPUT FORM for the retro style toolbar.
//   Now saves and restores the window position.
//   Added support for the Fnd_Grid component.
// Modified by Dave Batton on Sep 10, 2004
//   Now we stick the window title into a process variable, so we can use
//      it again (and again, and again) from Fnd_IO_OutputFormMethod.
// Modified by Dave Batton on Nov 9, 2004
//   Updated the window position preference name to include "Fnd_IO:".
//   We no longer save the window title. This is now handled in the Fnd_IO_DisplayRecord3 method.
// Modified by Dave Batton on Feb 22, 2005
//   We now setup the toolbar here rather than in the output form's On Load phase.
// Modified by Dave Batton on May 28, 2005
//   Updated to use the new Fnd_Wnd methods.
// Modified by Mike Erickson on Sep 28, 2005
//   Updated to use the new AL_TBL and PowerTools components from Automated Solutions Group.
// Modified by: Walt Nelson (6/24/10) to add check for Barclay's iLBListMagic component
// Modified by:  Mike Erickson (1/7/2011) to add support for PT Table View
// Modifed by: Dave Nasralla (4/8/2018) to remove support for unsupported components (Power Tools, Fnd_Grid)
// Modified by: Doug Hall on Feb 7, 2023 to support showing Related records.
// ----------------------------------------------------

C_POINTER:C301($1)
C_TEXT:C284($windowPrefName_t; $processName_t; $oldErrorHandler_t)
C_LONGINT:C283($processState_i; $processTime_i)
C_LONGINT:C283($width; $height)
var $Fnd_Out_b : Boolean

$Fnd_Out_b:=False:C215
Fnd_Gen_CurrentTable($1)

Fnd_IO_Init  // Since this is the start of a new process.

Fnd_Gen_CurrentFormType(Fnd_Gen_OutputForm)

BRING TO FRONT:C326(Current process:C322)
Fnd_Gen_MenuBar  // Install the menu bar for the current process

Fnd_Wnd_SendCloseRequests  // Send close window requests to this process.

// JDH modified 20230207 to support Fnd_Reltd routines.
// ---------------------------------------
C_TEXT:C284($RelatedSetName)
$RelatedSetName:=Fnd_Reltd_SetName(Fnd_Gen_CurrentTable)
If (Records in set:C195($RelatedSetName)>0)
	USE SET:C118($RelatedSetName)
	CREATE EMPTY SET:C140(Fnd_Gen_CurrentTable->; $RelatedSetName)  // Clears some memory without clearing the set itself.
	Fnd_Wnd_Title("Related "+Fnd_VS_TableName(Fnd_Gen_CurrentTable))
Else 
	ALL RECORDS:C47(Fnd_Gen_CurrentTable->)
	Fnd_Wnd_Title(Fnd_VS_TableName(Fnd_Gen_CurrentTable))  // DB050528 - Changed from Fnd_Wnd_SetTitle.
End if 
// ---------------------------------------

Fnd_Wnd_CloseBox(True:C214)  // DB050528 - Changed from Fnd_Wnd_SetCloseBox.
Fnd_Wnd_Position(Fnd_Wnd_Stacked)  // DB050528 - Changed from Fnd_Wnd_SetPosition.
PROCESS PROPERTIES:C336(Current process:C322; $processName_t; $processState_i; $processTime_i)  // DB041109 - Added.
$windowPrefName_t:="Fnd_IO: "+$processName_t  // DB041109 - Added.
Fnd_Wnd_UseSavedPosition($windowPrefName_t)  // DB041109 - Added the preference name.

// Initialize the toolbar before calling the hook, so the user can change it.
Fnd_IO_SetupToolbar

If (Fnd_Shell_DoesMethodExist("Fnd_Hook_IO_DisplayTable")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_IO_DisplayTable"; *)  // The developer hook.  Put here so he can override window settings.
End if 

// Modified by: Dave Nasralla (4/8/2018) to remove obsolete component references (PT Query Pack, SetManager, FND_Grid, Fnd_Out)
//  Set the default output form.

Case of 
	: (Fnd_Gen_ComponentAvailable("iLB@")) | (Fnd_Gen_ComponentAvailable("Listbox Magic"))  //Modified by: Walt Nelson(6/24/10) for iLBListMagic
	Fnd_IO_OutputFormName_t:="iLB_Output"
		
Else 
	FORM SET OUTPUT:C54(Fnd_Gen_CurrentTable->; Fnd_IO_OutputFormName_t)
End case   // Modified by: Walt Nelson(6/24/10) for iLBListMagic

Case of 
	: (Fnd_Gen_ComponentInfo("Fnd_Out"; "state")="active")
		Fnd_Out_OpenDialog
		
	: (Fnd_Gen_ComponentAvailable("iLB@")) | (Fnd_Gen_ComponentAvailable("Listbox Magic"))  // Added by: Walt Nelson (6/24/10) for iLBListMagic
		C_POINTER:C301($null_ptr)
		Fnd_Wnd_OpenFormWindow($null_ptr; Fnd_IO_OutputFormName_t)
		Fnd_Menu_Window_Add  // Add this process to the Windows menu.
		EXECUTE METHOD:C1007("FND_Host_IO_DisplayDialog"; *; Fnd_IO_OutputFormName_t)  // Oct 15, 2009 12:01:57 -- I.Barclay Berry "
		Fnd_Menu_Window_Remove
		Fnd_Pref_SetWindow($windowPrefName_t)  // DB041109 - Added the preference name.
		Fnd_Gen_CurrentFormType(Fnd_Gen_NoForm)
		Fnd_Gen_MenuBar
		CLOSE WINDOW:C154
		// Added by: Walt Nelson (6/24/10) for iLBListMagic
		
	Else 
		Fnd_Wnd_OpenFormWindow(Fnd_Gen_CurrentTable; Fnd_IO_OutputFormName_t)
		Fnd_Menu_Window_Add  // Add this process to the Windows menu.
		MODIFY SELECTION:C204(Fnd_Gen_CurrentTable->; *)
		Fnd_Menu_Window_Remove
		Fnd_Pref_SetWindow($windowPrefName_t)  // DB041109 - Added the preference name.
		Fnd_Gen_CurrentFormType(Fnd_Gen_NoForm)
		Fnd_Gen_MenuBar
		CLOSE WINDOW:C154
End case 

