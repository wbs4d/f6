//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Reg_Init

// Initializes both the process and interprocess variables used by the Fnd_Reg routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 25, 2003
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Reg_Initialized_b; Fnd_Reg_Initialized_b)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Reg_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Reg
	
	Fnd_Reg_Localize
	
	<>Fnd_Reg_Semaphore_t:="$Fnd_Reg_Semaphore"
	
	If (Not:C34(Semaphore:C143(<>Fnd_Reg_Semaphore_t)))  // Notice there's no timeout. We may end up displaying an error message.
		// Set up pointers to the structure. The table we use is not installed with the component.
		<>Fnd_Reg_Table_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]")
		<>Fnd_Reg_IDFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]ID"; Is longint:K8:6)
		<>Fnd_Reg_FeatureCodeFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]Feature_Code"; Is alpha field:K8:1; True:C214)
		<>Fnd_Reg_DemoDaysFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]Demo_Days"; Is integer:K8:5)
		<>Fnd_Reg_DemoStartDateFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]Demo_Start_Date"; Is date:K8:7)
		<>Fnd_Reg_UnlockCodeFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]Unlock_Code"; Is alpha field:K8:1)
		<>Fnd_Reg_UserNameFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]User_Name"; Is alpha field:K8:1)
		<>Fnd_Reg_LicenseInfoFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]License_Info"; Is alpha field:K8:1)
		<>Fnd_Reg_ChecksumFld_ptr:=Fnd_Gen_VerifyStructureItem("Fnd_Reg"; "[Fnd_Reg]Checksum"; Is alpha field:K8:1)
		
		<>Fnd_Reg_DefaultFeatureCode_t:="APPL"  // This must match a similar variable in Fnd_RegG_Init.
		<>Fnd_Reg_UnlockCodeFormat_t:="####-####-####"  // This must match a similar variable in Fnd_RegG_Init.
		<>Fnd_Reg_DefaultDemoDays_i:=30
		
		// Set up arrays to store the featue names.
		ARRAY TEXT:C222(<>Fnd_Reg_FeatureCodes_at; 1)
		ARRAY TEXT:C222(<>Fnd_Reg_FeatureNames_at; 1)
		ARRAY TEXT:C222(<>Fnd_Reg_BuyNowURLs_at; 1)
		ARRAY TEXT:C222(<>Fnd_Reg_DemoMessage_at; 1)
		<>Fnd_Reg_FeatureCodes_at{1}:=<>Fnd_Reg_DefaultFeatureCode_t
		<>Fnd_Reg_FeatureNames_at{1}:=Fnd_Gen_GetDatabaseInfo("DatabaseName")
		<>Fnd_Reg_BuyNowURLs_at{1}:=""
		<>Fnd_Reg_DemoMessage_at{1}:=""
		
		Fnd_Wnd_Type(-1)  // We'll check this value in Fnd_Reg_DemoDialog.
		
		// IP arrays must always be protected using a semaphore.
		<>Fnd_Reg_Semaphore_t:="$Fnd_Menu_Semaphore"
		
		<>Fnd_Reg_Initialized_b:=True:C214
		CLEAR SEMAPHORE:C144(<>Fnd_Reg_Semaphore_t)
	End if 
End if 

If (Not:C34(Fnd_Reg_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Reg
	
	Fnd_Reg_DialogFeatureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	
	Fnd_Reg_Initialized_b:=True:C214
End if 
