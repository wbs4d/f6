//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Dlg_PasswordEntry (->srcArea; ->curValue) --> Boolean

// Replaces typed characters with bullet characters for password entry.
// Returns True if the value has changed.
// Based on the Keystroke function example in the 4D documentation.

// Access: Private

// Parameters: 
//   $1 : Pointer : A pointer to the form object
//   $2 : Pointer : A pointer to a text variable that holds the contents

// Returns: 
//   $0 : Boolean : True if the current value has changed

// Created by Dave Batton on Jul 25, 2003
// ----------------------------------------------------

C_BOOLEAN:C305($0)
C_POINTER:C301($1;$2;$formObject_ptr;$currentValue_ptr)
C_TEXT:C284($newValue_t)
C_LONGINT:C283($start_i;$end_i)

$formObject_ptr:=$1
$currentValue_ptr:=$2

// Get the text selection range within the enterable area
GET HIGHLIGHT:C209($1->;$start_i;$end_i)
// Start working with the current value
$newValue_t:=$currentValue_ptr->
// Depending on the key pressed or the character entered,
// Perform the appropriate actions
Case of 
		
		// The Backspace (Delete) key has been pressed    
	: (Character code:C91(Keystroke:C390)=Backspace:K15:36)
		// Delete the selected characters or the character at the left of the text cursor 
		$newValue_t:=Substring:C12($newValue_t;1;$start_i-1-Num:C11($start_i=$end_i))+Substring:C12($newValue_t;$end_i)
		
		// An acceptable character has been entered
		//: (Position(Keystroke;"abcdefghjiklmnopqrstuvwxyz -0123456789")>0)
		// Allow any printable character.
	: ((Character code:C91(Keystroke:C390)>=SP ASCII code:K15:33) & (Character code:C91(Keystroke:C390)<DEL ASCII code:K15:34))
		If ($start_i#$end_i)
			// One or several characters are selected, the keystroke is going to override them
			$newValue_t:=Substring:C12($newValue_t;1;$start_i-1)+Keystroke:C390+Substring:C12($newValue_t;$end_i)
		Else 
			// The text selection is the text cursor
			Case of 
					// The text cursor is currently at the begining of the text          
				: ($start_i<=1)
					// Insert the character at the begining of the text          
					$newValue_t:=Keystroke:C390+$newValue_t
					// The text cursor is currently at the end of the text
				: ($start_i>=Length:C16($newValue_t))
					// Append the character at the end of the text          
					$newValue_t:=$newValue_t+Keystroke:C390
				Else 
					// The text cursor is somewhere in the text, insert the new character
					$newValue_t:=Substring:C12($newValue_t;1;$start_i-1)+Keystroke:C390+Substring:C12($newValue_t;$start_i)
			End case 
		End if 
		
		If (Is Windows:C1573)
			FILTER KEYSTROKE:C389("*")
		Else 
			FILTER KEYSTROKE:C389("•")
		End if 
		
		// An Arrow key has been pressed
		// Do nothing, but accept the keystroke
	: (Character code:C91(Keystroke:C390)=Left arrow key:K12:16)
	: (Character code:C91(Keystroke:C390)=Right arrow key:K12:17)
	: (Character code:C91(Keystroke:C390)=Up arrow key:K12:18)
	: (Character code:C91(Keystroke:C390)=Down arrow key:K12:19)
		//
	Else 
		// Do not accept characters other than letters, digits, space and dash
		FILTER KEYSTROKE:C389("")
End case 

// Return the value for the next keystroke handling
$currentValue_ptr->:=$newValue_t

// Is the value now different?
$0:=($newValue_t#$currentValue_ptr->)
