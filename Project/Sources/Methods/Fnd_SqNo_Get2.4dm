//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_SqNo_Get2 (group name) --> Number

  // Called by Fnd_SqNo_Get2 to get a sequence number.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The group name

  // Returns: 
  //   $0 : Longint : The next number in the sequence

  // Created by Dave Batton on Nov 5, 2003
  // Modified by Dave Batton on Jan 30, 2004
  //   Fixed the sort order of the reuse array so it returns the smallest number first.
  // Modified by Dave Batton on Mar 23, 2006
  //   This routine now references tables and fields using pointers.
  // Modified by Dave Batton on Sep 24, 2007
  //   Moved some code from this method to the new Fnd_SqNo_LoadGroupRecord method.
  //   Reamed $aNumbersToReuse to $numbersToReuse_ai.
  // ----------------------------------------------------

C_LONGINT:C283($0;$seqNo_i)
C_TEXT:C284($1;$groupName_t)

$groupName_t:=$1
$seqNo_i:=0

Fnd_SqNo_LoadGroupRecord ($groupName_t)

If (Not:C34(Locked:C147(<>Fnd_SqNo_Table_ptr->)))
	ARRAY LONGINT:C221($numbersToReuse_ai;0)
	BLOB TO VARIABLE:C533(<>Fnd_SqNo_RecycleBinFld_ptr->;$numbersToReuse_ai)
	If (Size of array:C274($numbersToReuse_ai)>0)  // If there are elements, we'll get the number from here.
		SORT ARRAY:C229($numbersToReuse_ai;>)  //DB040130 - Changed the sort direction.
		$seqNo_i:=$numbersToReuse_ai{1}  // Return the smallest number first.
		DELETE FROM ARRAY:C228($numbersToReuse_ai;1)  // Delete this element...
		VARIABLE TO BLOB:C532($numbersToReuse_ai;<>Fnd_SqNo_RecycleBinFld_ptr->)  // ...and return the remaining elements to the blob.
	Else   // No numbers to reuse available.
		$seqNo_i:=<>Fnd_SqNo_NextNumberFld_ptr->  // This is where we store the next number to use.
		<>Fnd_SqNo_NextNumberFld_ptr->:=$seqNo_i+1  // …and increment it.
	End if 
	SAVE RECORD:C53(<>Fnd_SqNo_Table_ptr->)  // Either way, we need to save the record.
	UNLOAD RECORD:C212(<>Fnd_SqNo_Table_ptr->)  // Make sure other users can load the record.
End if 

$0:=$seqNo_i