//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_GetTables (->pointer array{; ->text array})

  // Returns in the passed arrays the names of and pointers to all of the visible tables.
  // Returns the arrays already sorted in the order the developer wants them sorted.

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to a pointer array to receive the table pointers
  //   $2 : Pointer : A pointer to a text array to receive the table names (optional)

  // Returns: Nothing

  // Created by Dave Batton on Sep 2, 2003
  // Modified by Dave Batton on Jan 24, 2004
  //   Now returns virtual table names.
  // Modified by: Wayne Stewart (27/02/09)
  //   Now excludes tables starting with Fnd_ if they are invisible or not

  // ----------------------------------------------------

C_POINTER:C301($1;$2;$tablePtrsArray_ptr;$tableNamesArray_ptr)
C_LONGINT:C283($tableNumber_i;$element_i)
C_BOOLEAN:C305($invisible_b;$FndTable_b)

$tablePtrsArray_ptr:=$1

Fnd_VS_Init 

If (Count parameters:C259>=2)
	$tableNamesArray_ptr:=$2
	ARRAY TEXT:C222($tableNamesArray_ptr->;0)
End if 

ARRAY POINTER:C280($tablePtrsArray_ptr->;0)

For ($tableNumber_i;1;Get last table number:C254)
	If (Is table number valid:C999($tableNumber_i))
		GET TABLE PROPERTIES:C687($tableNumber_i;$invisible_b)
		  // Modified by: Wayne Stewart (27/02/09)
		$FndTable_b:=(Position:C15("Fnd";Table name:C256($tableNumber_i))#0)  //  Also exclude table names with Fnd in them
		If (Not:C34($invisible_b) & Not:C34($FndTable_b))  // Include only visible tables, non shell tables
			If (Not:C34(Fnd_VS_WasSubtable (Table:C252($tableNumber_i))))  // Don't include subtables that were automatically changed to tables with v11.
				APPEND TO ARRAY:C911($tablePtrsArray_ptr->;Table:C252($tableNumber_i))
				If (Count parameters:C259>=2)
					APPEND TO ARRAY:C911($tableNamesArray_ptr->;Fnd_VS_TableName (Table:C252($tableNumber_i)))
				End if 
			End if 
		End if 
	End if 
End for 
