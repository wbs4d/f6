//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dict_Name (dict ID) --> Text

  // Returns the name of the dictionary.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : Dictionary ID

  // Returns: 
  //   $0 : Text : Dictionary Name

  // Created by Rob Laveaux
  // ----------------------------------------------------

C_TEXT:C284($0;$name_t)
C_LONGINT:C283($1;$dictionary_i)

$dictionary_i:=$1

Fnd_Dict_LockInternalState (True:C214)

If (Fnd_Dict_IsValid ($dictionary_i))
	$name_t:=<>Fnd_Dict_Names_at{$dictionary_i}
End if 

Fnd_Dict_LockInternalState (False:C215)

$0:=$name_t