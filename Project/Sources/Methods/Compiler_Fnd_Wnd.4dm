//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Wnd

// Compiler variables related to the Foundation window handling routines.
// Called by Fnd_Wnd_Init.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 16, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Wnd_Initialized_b)
If (Not:C34(<>Fnd_Wnd_Initialized_b))
	C_LONGINT:C283(<>Fnd_Wnd_StackHOffset_i; <>Fnd_Wnd_StackVOffset_i)
	C_BOOLEAN:C305(<>Fnd_Wnd_Closing_b)
	C_TEXT:C284(<>Fnd_Wnd_Semaphore_t)
	C_LONGINT:C283(<>Fnd_Wnd_ScreenCount_i)
	C_LONGINT:C283(<>Fnd_Wnd_SemaphoreTimeout_i)
	
	ARRAY LONGINT:C221(<>Fnd_Wnd_StackWndRefNos_ai; 0)
	ARRAY LONGINT:C221(<>Fnd_Wnd_StackLeftPositions_ai; 0)
	ARRAY LONGINT:C221(<>Fnd_Wnd_StackTopPositions_ai; 0)
	ARRAY TEXT:C222(<>Fnd_Wnd_CloseProcessNames_at; 0)
	ARRAY LONGINT:C221(<>Fnd_Wnd_ScreenMap_ai; 0; 0)
	ARRAY TEXT:C222(<>Fnd_Wnd_MenuItemNames_at; 0)
	ARRAY LONGINT:C221(<>Fnd_Wnd_MenuProcessNumbers_ai; 0)
End if 


// Process Variables
C_BOOLEAN:C305(Fnd_Wnd_Initialized_b)
If (Not:C34(Fnd_Wnd_Initialized_b))
	C_BOOLEAN:C305(Fnd_Wnd_CloseBox_b)
	
	C_TEXT:C284(Fnd_Wnd_WindowTitle_t)
	C_TEXT:C284(Fnd_Wnd_PositionPrefName_t)
	
	C_LONGINT:C283(Fnd_Wnd_WindowPosition_i)
	
	C_LONGINT:C283(Fnd_Wnd_width_i; Fnd_Wnd_height_i)  // wwn 9/11/09
	
End if 


// Parameters
If (False:C215)
	C_BOOLEAN:C305(Fnd_Wnd_CloseAllWindows; $0; $1)
	
	C_BOOLEAN:C305(Fnd_Wnd_CloseBox; $0; $1)
	
	C_BOOLEAN:C305(Fnd_Wnd_CloseNow; $0)
	
	C_LONGINT:C283(Fnd_Wnd_GetProcessWindowRef; $0; $1)
	
	C_TEXT:C284(Fnd_Wnd_Info; $0; $1)
	
	C_LONGINT:C283(Fnd_Wnd_OpenFormWindow; $0)
	C_POINTER:C301(Fnd_Wnd_OpenFormWindow; $1)
	C_TEXT:C284(Fnd_Wnd_OpenFormWindow; $2)
	
	C_LONGINT:C283(Fnd_Wnd_OpenWindow; $0; $1; $2; $3; $4)
	
	C_LONGINT:C283(Fnd_Wnd_GetNextStackElement; $0)
	
	C_VARIANT:C1683(Fnd_Wnd_MoveOnScreen; $1)
	C_POINTER:C301(Fnd_Wnd_MoveOnScreen; $2; $3; $4)
	C_LONGINT:C283(Fnd_Wnd_MoveOnScreen; $5)
	
	C_LONGINT:C283(Fnd_Wnd_Position; $0; $1; $2; $3; $4)
	
	C_TEXT:C284(Fnd_Wnd_SavePosition; $1)
	C_LONGINT:C283(Fnd_Wnd_SavePosition; $2)
	
	C_LONGINT:C283(Fnd_Wnd_SendCloseRequests; $1)
	
	C_TEXT:C284(Fnd_Wnd_Title; $0; $1)
	
	C_LONGINT:C283(Fnd_Wnd_Type; $0; $1)
	
	C_TEXT:C284(Fnd_Wnd_UseSavedPosition; $1)
	
	C_LONGINT:C283(Fnd_Wnd_ToolbarHeight; $0)
	
End if 