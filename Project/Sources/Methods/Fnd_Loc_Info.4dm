//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo method
//   for more information.

// Access Type: Shared

// Parameters: 
//   $1 : Text : Info desired ("version" or "name" or "language")

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on May 12, 2004
// Modified by Dave Batton on Dec 28, 2004
//   Updated the version number to 4.0.5.
// Modified by Dave Batton on Feb 23, 2005
//   Updated the version number to 4.1.
// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="version")
		$reply_t:="6.0"
		
	: ($request_t="name")
		$reply_t:="Foundation Localization"
		
	: ($request_t="language")
		Fnd_Loc_Init
		$reply_t:=<>Fnd_Loc_CurrentLanguage_t
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
