//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_Type ({window type}) --> Number

// Sets and gets the window type for the next window to be opened.
// This method replaces the original Fnd_Wnd_SetType method, which is now obsolete.

// Access Type: Shared

// Parameters:
//   $1 : Longint : Window type (optional)

// Returns:
//   $0 : Longint : Window type

// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header
// Created by Dave Batton on Apr 23, 2005
// ----------------------------------------------------

C_LONGINT:C283($0;$1)

Fnd_Wnd_Init

If (Count parameters:C259>=1)
	Fnd.Wnd.windowType:=$1
End if 

$0:=Fnd.Wnd.windowType