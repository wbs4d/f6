//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_SqNo

  // Compiler variables related to the Foundation Sequence Number routines.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 5, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_SqNo_Initialized_b)
If (Not:C34(<>Fnd_SqNo_Initialized_b))
	C_TEXT:C284(<>Fnd_SqNo_Semaphore_t)
	C_TEXT:C284(<>Fnd_SqNo_Name_t)
	C_TEXT:C284(<>Fnd_SqNo_EditGroupName_t)
	C_TEXT:C284(<>Fnd_SqNo_CurrentGroupName_t)
	C_TEXT:C284(<>Fnd_SqNo_Label1_t;<>Fnd_SqNo_Label2_t;<>Fnd_SqNo_Label3_t;<>Fnd_SqNo_Label4_t)
	C_TEXT:C284(<>Fnd_SqNo_DlgMessage_t)
	
	C_LONGINT:C283(<>Fnd_SqNo_Number_i;<>Fnd_SqNo_EditNextNumber_i)
	C_LONGINT:C283(<>Fnd_SqNo_Timeout_i)
	C_LONGINT:C283(<>Fnd_SqNo_NamesList_i)
	C_LONGINT:C283(<>Fnd_SqNo_AddNumButton_i;<>Fnd_SqNo_DeleteNumButton_i;<>Fnd_SqNo_AutoFixButton_i)
	C_LONGINT:C283(<>Fnd_SqNo_OKButton_i;<>Fnd_SqNo_CancelButton_i;<>Fnd_SqNo_OffscreenButton_i)
	C_LONGINT:C283(<>Fnd_SqNo_DesignerOnlyCheckbox_i;<>Fnd_SqNo_SizingButton_i)
	C_LONGINT:C283(<>Fnd_SqNo_AddGroupButton_i;<>Fnd_SqNo_EditGroupButton_i;<>Fnd_SqNo_DeleteGroupButton_i)
	C_LONGINT:C283(<>Fnd_SqNo_NamesList_i)
	C_LONGINT:C283(<>Fnd_SqNo_CurrentGroupID_i)
	
	C_POINTER:C301(<>Fnd_SqNo_TempTable_ptr)
	C_POINTER:C301(<>Fnd_SqNo_Table_ptr)
	C_POINTER:C301(<>Fnd_SqNo_IDFld_ptr)
	C_POINTER:C301(<>Fnd_SqNo_GroupNameFld_ptr)
	C_POINTER:C301(<>Fnd_SqNo_NextNumberFld_ptr)
	C_POINTER:C301(<>Fnd_SqNo_RecycleBinFld_ptr)
	C_POINTER:C301(<>Fnd_SqNo_DesignerOnlyFld_ptr)
	
	ARRAY LONGINT:C221(<>Fnd_SqNo_Numbers_ai;0)
	ARRAY LONGINT:C221(<>Fnd_SqNo_ReuseNumbersArray_ai;0)
End if 


  // Parameters
If (False:C215)  // So we never run this as code.
	C_LONGINT:C283(Fnd_SqNo_EditGroup ;$1)
	
	C_TEXT:C284(Fnd_SqNo_Enable ;$1)
	C_LONGINT:C283(Fnd_SqNo_Enable ;$2)
	
	C_POINTER:C301(Fnd_SqNo_Fix ;$1)
	
	C_LONGINT:C283(Fnd_SqNo_Get ;$0)
	C_TEXT:C284(Fnd_SqNo_Get ;$1)
	
	C_LONGINT:C283(Fnd_SqNo_Get2 ;$0)
	C_TEXT:C284(Fnd_SqNo_Get2 ;$1)
	
	C_TEXT:C284(Fnd_SqNo_GetMany ;$1)
	C_LONGINT:C283(Fnd_SqNo_GetMany ;$2)
	C_POINTER:C301(Fnd_SqNo_GetMany ;$3)
	
	C_TEXT:C284(Fnd_SqNo_GetMany2 ;$1)
	C_POINTER:C301(Fnd_SqNo_GetMany2 ;$2)
	
	C_TEXT:C284(Fnd_SqNo_Info ;$0;$1)
	
	C_POINTER:C301(Fnd_SqNo_IDFieldPtr ;$0;$1)
	
	C_BOOLEAN:C305(Fnd_SqNo_IsInDesignGroup ;$0)
	
	C_TEXT:C284(Fnd_SqNo_LoadGroupRecord ;$1)
	
	C_TEXT:C284(Fnd_SqNo_Put ;$1)
	C_LONGINT:C283(Fnd_SqNo_Put ;$2)
	
	C_TEXT:C284(Fnd_SqNo_Put2 ;$1)
	C_LONGINT:C283(Fnd_SqNo_Put2 ;$2)
	
	C_TEXT:C284(Fnd_SqNo_Set ;$1)
	C_LONGINT:C283(Fnd_SqNo_Set ;$2)
	
	C_POINTER:C301(Fnd_SqNo_SetRecordID ;$1)
	
	C_TEXT:C284(Fnd_SqNo_TableName ;$0)
	C_POINTER:C301(Fnd_SqNo_TableName ;$1)
	
	C_TEXT:C284(Fnd_SqNo_TableSeqNoName ;$0)
	C_POINTER:C301(Fnd_SqNo_TableSeqNoName ;$1)
End if 