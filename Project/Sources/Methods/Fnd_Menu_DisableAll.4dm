//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_DisableAll

  // Disables all of the menu items.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on May 19, 2005
  // ----------------------------------------------------

C_LONGINT:C283($menu_i;$menuItem_i)

For ($menu_i;1;Count menus:C404)
	For ($menuItem_i;1;Count menu items:C405($menu_i))
		DISABLE MENU ITEM:C150($menu_i;$menuItem_i)
	End for 
End for 
