//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Text_Init

// Initializes both the process and interprocess variables used by the Text routines.
// Designed to be called at the beginning of any Protected routines to make sure
//   the necessary variables are initialized.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 27, 2004
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the Fnd_Text_Info method.
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Text_Initialized_b)
C_LONGINT:C283($word_i)
C_TEXT:C284($language_t; $listName_t)

Fnd_Init  // All Init methods must call this method

If (Not:C34(<>Fnd_Text_Initialized_b))  // So we only do this once per session.
	Compiler_Fnd_Text
	
	$language_t:=Fnd_Gen_ComponentInfo("Fnd_Loc"; "language")
	If (Length:C16($language_t)=2)
		$listName_t:="Fnd_Text_CapExclude_"+$language_t
	Else 
		$listName_t:="Fnd_Text_CapExclude_EN"
	End if 
	ARRAY TEXT:C222(<>Fnd_Text_CapExcludeWords_at; 0)
	LIST TO ARRAY:C288($listName_t; <>Fnd_Text_CapExcludeWords_at)
	For ($word_i; 1; Size of array:C274(<>Fnd_Text_CapExcludeWords_at))
		<>Fnd_Text_CapExcludeWords_at{$word_i}:=" "+<>Fnd_Text_CapExcludeWords_at{$word_i}+" "
	End for 
	
	Fnd_Text_TextToMD5_Calc("INIT")
	
	<>Fnd_Text_Initialized_b:=True:C214
End if 
