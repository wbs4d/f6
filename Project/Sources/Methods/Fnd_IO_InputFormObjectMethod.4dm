//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_InputFormObjectMethod (->form object)

  // 

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : Form object

  // Returns: Nothing

  // Created by Dave Batton on Oct 15, 2007
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   NOTE: This method is called in the object methods of the inherited input form.
  //     Should this NOT be used directly in our forms?
  // Modified by: Walt Nelson (3/1/10)
  // fails to resolve the "Self" pointer to get the variable name for some Windows users - on occasion per Dave Nasralla
  // ----------------------------------------------------

C_POINTER:C301($1;$object_ptr)
C_TEXT:C284($variableName_t;$action_t)
C_LONGINT:C283($tableNo_i;$fieldNo_i)
C_BOOLEAN:C305($allow_b;$optionKeyDown_b)

$action_t:=""  // Modified by: Walt Nelson (3/1/10) - DGN added default value 
$object_ptr:=$1

  // Modified by: Walt Nelson (3/1/10) - DGN added to allow second parameter 
If (Count parameters:C259>1)
	C_TEXT:C284($2)
	$action_t:=$2
End if 


Case of 
	: (Form event:C388=On Clicked:K2:4)
		RESOLVE POINTER:C394($object_ptr;$variableName_t;$tableNo_i;$fieldNo_i)
		
		If ($action_t="")  // Modified by: Walt Nelson (3/1/10) - DGN added if statement for backward compatability 
			
			Case of 
				: ($variableName_t="Fnd_IO_OKButton_i")
					$action_t:="save"
				: (($variableName_t="Fnd_IO_CancelButton_i") | ($variableName_t="Fnd_IO_EscButton_i"))
					$action_t:="cancel"
				: ($variableName_t="Fnd_IO_FirstButton_i")
					$action_t:="first"
				: ($variableName_t="Fnd_IO_PrevButton_i")
					$action_t:="previous"
				: ($variableName_t="Fnd_IO_NextButton_i")
					$action_t:="next"
				: ($variableName_t="Fnd_IO_LastButton_i")
					$action_t:="last"
				: ($variableName_t="Fnd_IO_CloseButton_i")
					$action_t:="close"
					$optionKeyDown_b:=Macintosh option down:C545
			End case 
		End if   // Modified by: Walt Nelson (3/1/10) - DGN added endif 
		
		$allow_b:=True:C214
		If (Fnd_Shell_DoesMethodExist ("Fnd_Hook_IO_InputFormButton")=1)
			EXECUTE METHOD:C1007("Fnd_Hook_IO_InputFormButton";$allow_b;$action_t)
		End if 
		
		If ($allow_b)
			Case of 
				: ($action_t="close")
					If (($optionKeyDown_b) & (Fnd_IO_InputFormClose ))
						Fnd_Wnd_CloseAllWindows 
					End if 
				: ($action_t="cancel")
					CANCEL:C270  // We need special handling for CANCEL, since REJECT doesn't work.
			End case 
			
		Else 
			REJECT:C38
		End if 
End case 
