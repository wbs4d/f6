//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Prnt_Alert (message)

// Displays an alert message. Uses the Foundation Dialog module alert if available.

// Access: Private

// Parameters: 
//   $1 : Text : The alert message text

// Returns: Nothing

// Created by Dave Batton on Oct 13, 2007
// [1] Modified by: John Craig (1/7/10) changed Fnd_Wnd_SetPosition to Fnd_Wnd_Position
// ----------------------------------------------------

C_TEXT:C284($1; $message_t)

$message_t:=$1  // A process variable since we may call this from multiple processes.

//If (Fnd_Gen_ComponentAvailable("Fnd_Dlg"))
//EXECUTE METHOD("Fnd_Wnd_Position"; *; Fnd_Wnd_MacOSXSheet)  // [1]
//EXECUTE METHOD("Fnd_Dlg_Alert"; *; $message_t)
//Else 
//ALERT($message_t)
//End if 

Fnd_Wnd_Position(Fnd_Wnd_MacOSXSheet)
Fnd_Dlg_Alert($message_t)