//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_IO_DisplayNavButtons ({enable?}) --> Boolean

  // Allows the developer to display or hide the record navigation buttons
  //   for the current process.

  // Access: Shared

  // Parameters: 
  //   $1 : Boolean : True to enable the display of record navigation buttons (optional)

  // Returns: 
  //   $0 : Boolean : True if display of record navigation buttons is enabled

  // Created by Dave Batton on Jan 6, 2005
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Enhanced parameter descriptions in header
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$1)

Fnd_IO_Init 

If (Count parameters:C259>=1)
	Fnd_IO_DisplayNavButtons_b:=$1
End if 

$0:=Fnd_IO_DisplayNavButtons_b
