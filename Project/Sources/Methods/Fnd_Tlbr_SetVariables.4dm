//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_SetVariables

  // Updates the toolbar variables depending on the current platform and size settings.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jan 16, 2005
  // Modified by Dave Batton on Apr 14, 2005
  //   Added support for the new Prefs style toolbar.
  // Modified by Dave Batton on Nov 18, 2005
  //   Added support for menu buttons.
  // ----------------------------------------------------

Case of 
	: (((Fnd_Tlbr_Style_t="Large") | (Fnd_Tlbr_Style_t="Prefs")) & (Fnd_Tlbr_Platform_t="Win"))
		Fnd_Tlbr_ButtonLeftOffset_i:=1
		Fnd_Tlbr_ButtonTopOffset_i:=1
		Fnd_Tlbr_ButtonPadding_i:=0
		Fnd_Tlbr_DividerImageName_t:="Fnd_Tlbr_Divider_LargeWin"
		Fnd_Tlbr_DividerPadding_i:=1
		Fnd_Tlbr_DividerTopOffset_i:=2
		Fnd_Tlbr_MenuTopOffset_i:=35  // DB051118
		
		
	: ((Fnd_Tlbr_Style_t="Large") & (Fnd_Tlbr_Platform_t="Mac"))
		Fnd_Tlbr_ButtonLeftOffset_i:=12
		Fnd_Tlbr_ButtonTopOffset_i:=0
		Fnd_Tlbr_ButtonPadding_i:=12
		Fnd_Tlbr_DividerImageName_t:="Fnd_Tlbr_Divider_LargeMac"
		Fnd_Tlbr_DividerPadding_i:=12
		Fnd_Tlbr_DividerTopOffset_i:=3
		Fnd_Tlbr_MenuTopOffset_i:=40  // DB051118
		
		
	: ((Fnd_Tlbr_Style_t="Prefs") & (Fnd_Tlbr_Platform_t="Mac"))  // DB050413
		Fnd_Tlbr_ButtonLeftOffset_i:=7
		Fnd_Tlbr_ButtonTopOffset_i:=0
		Fnd_Tlbr_ButtonPadding_i:=2  // Since the padding's built into the button.
		If (Fnd_Tlbr_Platform_t="Win")
			Fnd_Tlbr_DividerImageName_t:="Fnd_Tlbr_Divider_LargeWin"
		Else 
			Fnd_Tlbr_DividerImageName_t:="Fnd_Tlbr_Divider_LargeMac"
		End if 
		Fnd_Tlbr_DividerPadding_i:=7
		Fnd_Tlbr_DividerTopOffset_i:=3
		Fnd_Tlbr_MenuTopOffset_i:=0  //  ` DB051118 - Not a valid option.
		
		
	: (Fnd_Tlbr_Style_t="Small@")
		Fnd_Tlbr_ButtonLeftOffset_i:=4
		Fnd_Tlbr_ButtonTopOffset_i:=4
		Fnd_Tlbr_ButtonPadding_i:=4  // The space between buttons.
		Fnd_Tlbr_DividerImageName_t:="Fnd_Tlbr_Divider_Small"+Substring:C12(Fnd_Tlbr_Platform_t;1;3)
		Fnd_Tlbr_DividerPadding_i:=4
		Fnd_Tlbr_DividerTopOffset_i:=4
		Fnd_Tlbr_MenuTopOffset_i:=22  // DB051118
		
	Else 
		Fnd_Gen_BugAlert (Current method name:C684;"Unrecognized toolbar style name: "+Fnd_Tlbr_Style_t)
End case 

Fnd_Tlbr_ButtonsAreValid_b:=False:C215
