//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_Display --> Longint

  // Displays the built dialog.
  // Sets the OK variable.

  // Access: Shared

  // Parameters: None

  // Returns:
  //   $0 : Longint : 1 if OK is clicked, 0 if Cancel is clicked, 2 if the optional button is clicked

  // Created by Dave Batton on Jul 4, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   Changed the call to Fnd_Dlg_Reset to just setting the Fnd_Dlg_Initialized_b variable to False.
  // Modified by Dave Batton on Jan 15, 2005
  //   This routine new gets the window position from the Fnd_Wnd component rather than from a Fnd_Dlg process variable.
  // Modified by Dave Batton on Sep 15, 2007
  //   Updated for 4D v11 SQL.
  // ----------------------------------------------------

C_LONGINT:C283($0;$ok_i)
C_TEXT:C284($formula_t)

Fnd_Dlg_Init 
Fnd_Dlg_FormAlert 

Fnd_Dlg_Initialized_b:=False:C215  // So the settings get reset to the default values.

  // Set the value of the OK variable in the host database to match the
  //   value of the OK variable in this component.
If (Fnd_Shell_DoesMethodExist ("Fnd_Host_ExecuteFormula")=1)
	$ok_i:=OK  // EXECUTE METHOD will change the value of the OK variable!
	$formula_t:="OK:="+String:C10(OK)
	EXECUTE METHOD:C1007("Fnd_Host_ExecuteFormula";*;$formula_t)
	OK:=$ok_i
End if 

$0:=OK