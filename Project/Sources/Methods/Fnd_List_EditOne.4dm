//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_EditOne (list name)

  // Call this method to edit the specified list in a semi-modal dialog.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the list

  // Returns: Nothing

  // Created by Dave Batton on Oct 11, 2003
  // Modified by Dave Batton on Jun 16, 2004
  //   Localized the error message.
  // ----------------------------------------------------

C_TEXT:C284($1;$listName_t;$semaphore_t)
C_LONGINT:C283($processNumber_i)

$listName_t:=$1

$semaphore_t:="<>Fnd_List: "+$listName_t  // Notice this is a global semaphore.

If (Semaphore:C143($semaphore_t))
	Fnd_Dlg_Alert (Fnd_Gen_GetString ("Fnd_List";"ListIsNotAvailable";$listName_t))
	
Else 
	$processNumber_i:=New process:C317("Fnd_List_EditOne2";Fnd_Gen_DefaultStackSize;"Fnd_List Editor";$listName_t)
	
	  // Now wait until it's done.
	Repeat 
		DELAY PROCESS:C323(Current process:C322;5)
	Until (Process state:C330($processNumber_i)<0)
	
	  // Now we can return control to the calling process, and it can safely get the 
	  //   updated information from the list.
	CLEAR SEMAPHORE:C144($semaphore_t)
End if 