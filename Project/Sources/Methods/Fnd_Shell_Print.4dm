//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_Print

// Displays the list of printing options.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Nov 30, 2003
// ----------------------------------------------------

Case of 
	: (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_Print")=1)
		Fnd_Wnd_Title(Fnd_Loc_GetString("Fnd_Shell"; "PrintWindowTitle"))
		Fnd_List_Clear
		EXECUTE METHOD:C1007("Fnd_Hook_Shell_Print"; *)
		
	Else 
		
		//: (Fnd_Gen_ComponentAvailable("Fnd_Prnt"))
		Fnd_Wnd_Title(Fnd_Loc_GetString("Fnd_Shell"; "PrintWindowTitle"))
		Fnd_List_Clear
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "ReportEditor"); "Fnd_Prnt_QuickReportEditor")  // 4D's Quick Report Editor
		Fnd_List_AddItem(Fnd_Loc_GetString("Fnd_Shell"; "LabelEditor"); "Fnd_Prnt_LabelEditor")  // 4D's Label Editor
		Fnd_List_CommandDialog  // Display the selection dialog. 
		
		//Else 
		//QR REPORT(Fnd_Gen_CurrentTable->; Char(1))
End case 