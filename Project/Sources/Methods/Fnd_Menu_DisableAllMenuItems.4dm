//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Menu_DisableAllMenuItems (menu reference)

  // Disables all menu items in the specified menu.
  // A temporary work-around for a 4D v11 SQL 1.0 bug, which doesn't
  //   disable all menu items if 0 is passed as the menu item.

  // Access: Private

  // Parameters: 
  //   $1 : String : A menu reference

  // Returns: Nothing

  // Created by Dave Batton on Oct 24, 2007
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_TEXT:C284($1;$menu_t)
C_LONGINT:C283($item_i)

$menu_t:=$1

For ($item_i;1;Count menu items:C405($menu_t))
	DISABLE MENU ITEM:C150($menu_t;$item_i)
End for 