//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_ShowAll

// Displays all records for the current table.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on May 2, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Rec component is no longer required.
// ----------------------------------------------------


Fnd_Rec_ShowAll