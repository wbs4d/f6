//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_MessageClose

  // Closes the progress indicator window.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 25, 2003
  // Modified by Dave Batton on Feb 5, 2004
  //   Changed the call to Fnd_Dlg_Reset to just setting the Fnd_Dlg_Initialized_b variable to False.
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Corrected method name in header
  // ----------------------------------------------------

Fnd_Dlg_Init 

Repeat 
	<>Fnd_Dlg_MessageClose_b:=True:C214
	POST OUTSIDE CALL:C329(Process number:C372(<>Fnd_Dlg_MessageProcessName_t))
Until (Process number:C372(<>Fnd_Dlg_MessageProcessName_t)<=0)

Fnd_Dlg_Initialized_b:=False:C215  // DB040205 - So the settings get reset to the default values.
