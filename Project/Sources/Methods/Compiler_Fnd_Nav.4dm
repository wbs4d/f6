//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Nav

// Compiler variables related to the Foundation Art routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 28, 2003
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Nav_Initialized_b)
If (Not:C34(<>Fnd_Nav_Initialized_b))
	C_LONGINT:C283(<>Fnd_Nav_Button1_i; <>Fnd_Nav_Button2_i; <>Fnd_Nav_Button3_i; <>Fnd_Nav_Button4_i; <>Fnd_Nav_Button5_i)
	C_LONGINT:C283(<>Fnd_Nav_Button6_i; <>Fnd_Nav_Button7_i; <>Fnd_Nav_Button8_i; <>Fnd_Nav_Button9_i; <>Fnd_Nav_Button10_i)
	C_LONGINT:C283(<>Fnd_Nav_Button11_i; <>Fnd_Nav_Button12_i; <>Fnd_Nav_Button13_i; <>Fnd_Nav_Button14_i; <>Fnd_Nav_Button15_i)
	C_LONGINT:C283(<>Fnd_Nav_Button16_i; <>Fnd_Nav_Button17_i; <>Fnd_Nav_Button18_i; <>Fnd_Nav_Button19_i; <>Fnd_Nav_Button20_i)
	C_LONGINT:C283(<>Fnd_Nav_Button21_i; <>Fnd_Nav_Button22_i; <>Fnd_Nav_Button23_i; <>Fnd_Nav_Button24_i; <>Fnd_Nav_Button25_i)
	C_LONGINT:C283(<>Fnd_Nav_Button26_i; <>Fnd_Nav_Button27_i; <>Fnd_Nav_Button28_i; <>Fnd_Nav_Button29_i; <>Fnd_Nav_Button30_i)
	C_LONGINT:C283(<>Fnd_Nav_Button31_i; <>Fnd_Nav_Button32_i; <>Fnd_Nav_Button33_i; <>Fnd_Nav_Button34_i; <>Fnd_Nav_Button35_i)
	C_LONGINT:C283(<>Fnd_Nav_Button36_i; <>Fnd_Nav_Button37_i; <>Fnd_Nav_Button38_i; <>Fnd_Nav_Button39_i; <>Fnd_Nav_Button40_i)
	
	C_LONGINT:C283(<>Fnd_Nav_Left_i; <>Fnd_Nav_Top_i; <>Fnd_Nav_Right_i; <>Fnd_Nav_Bottom_i)
	C_LONGINT:C283(<>Fnd_Nav_MaxButtons_i)
	
	C_TEXT:C284(<>Fnd_Nav_Label1_t; <>Fnd_Nav_Label2_t; <>Fnd_Nav_Label3_t; <>Fnd_Nav_Label4_t; <>Fnd_Nav_Label5_t)
	C_TEXT:C284(<>Fnd_Nav_Label6_t; <>Fnd_Nav_Label7_t; <>Fnd_Nav_Label8_t; <>Fnd_Nav_Label9_t; <>Fnd_Nav_Label10_t)
	C_TEXT:C284(<>Fnd_Nav_Label11_t; <>Fnd_Nav_Label12_t; <>Fnd_Nav_Label13_t; <>Fnd_Nav_Label14_t; <>Fnd_Nav_Label15_t)
	C_TEXT:C284(<>Fnd_Nav_Label16_t; <>Fnd_Nav_Label17_t; <>Fnd_Nav_Label18_t; <>Fnd_Nav_Label19_t; <>Fnd_Nav_Label20_t)
	C_TEXT:C284(<>Fnd_Nav_Label21_t; <>Fnd_Nav_Label22_t; <>Fnd_Nav_Label23_t; <>Fnd_Nav_Label24_t; <>Fnd_Nav_Label25_t)
	C_TEXT:C284(<>Fnd_Nav_Label26_t; <>Fnd_Nav_Label27_t; <>Fnd_Nav_Label28_t; <>Fnd_Nav_Label29_t; <>Fnd_Nav_Label30_t)
	C_TEXT:C284(<>Fnd_Nav_Label31_t; <>Fnd_Nav_Label32_t; <>Fnd_Nav_Label33_t; <>Fnd_Nav_Label34_t; <>Fnd_Nav_Label35_t)
	C_TEXT:C284(<>Fnd_Nav_Label36_t; <>Fnd_Nav_Label37_t; <>Fnd_Nav_Label38_t; <>Fnd_Nav_Label39_t; <>Fnd_Nav_Label40_t)
	
	
	
	C_BOOLEAN:C305(<>Fnd_Nav_NeedsRedraw_b)
	
	ARRAY TEXT:C222(<>Fnd_Nav_Labels_at; 0)
	ARRAY POINTER:C280(<>Fnd_Nav_TablePtrs_aptr; 0)
	ARRAY TEXT:C222(<>Fnd_Nav_Methods_at; 0)
	ARRAY LONGINT:C221(<>Fnd_Nav_ForegroundColors_ai; 0)
	ARRAY LONGINT:C221(<>Fnd_Nav_BackgroundColors_ai; 0)
	
	ARRAY POINTER:C280(<>Fnd_Nav_TempTablePtrsArray_aptr; 0)  // Just used in Fnd_Nav_AutoConfigure.
End if 


// Parameters
If (False:C215)  // So we never run this as code.
	C_LONGINT:C283(Fnd_Nav_Add; $0)
	C_TEXT:C284(Fnd_Nav_Add; $1)
	
	C_TEXT:C284(Fnd_Nav_AddButtonMethod; $1; $2)
	C_LONGINT:C283(Fnd_Nav_AddButtonMethod; $3; $4)
	
	C_POINTER:C301(Fnd_Nav_AddButtonTable; $1)
	C_TEXT:C284(Fnd_Nav_AddButtonTable; $2)
	C_LONGINT:C283(Fnd_Nav_AddButtonTable; $3; $4)
	
	C_LONGINT:C283(Fnd_Nav_ButtonMethod; $1)
	
	C_TEXT:C284(Fnd_Nav_Delete; $1)
	
	C_TEXT:C284(Fnd_Nav_Info; $0; $1)
	
	C_LONGINT:C283(Fnd_Nav_WindowHeight; $0)
	
	C_LONGINT:C283(Fnd_Nav_WindowWidth; $0)
End if 