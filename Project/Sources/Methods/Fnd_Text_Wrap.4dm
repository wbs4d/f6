//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Text_Wrap (text; max line length) --> Text

  // Takes the text and wraps it into lines with the specified maximum length.
  // Routine by Marco Bernasconi <mbernasconi@befund.com>

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The text to wrap
  //   $2 : Longint : Max line length of wrapped text

  // Returns: 
  //   $0 : Text : The wrapped text

  // Created by Dave Batton on Apr 27, 2004
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$remainingText_t;$result_t)
C_LONGINT:C283($2;$lineLength_i;$character_i)

$remainingText_t:=$1
$lineLength_i:=$2

Fnd_Text_Init 

$result_t:=""

While (Length:C16($remainingText_t)>0)
	If (Length:C16($remainingText_t)<($lineLength_i+1))
		$result_t:=$result_t+$remainingText_t+Char:C90(Carriage return:K15:38)
		$remainingText_t:=""
	Else 
		If (((Position:C15(Char:C90(Carriage return:K15:38);$remainingText_t))#0) & ((Position:C15(Char:C90(Carriage return:K15:38);$remainingText_t))<($lineLength_i+2)))
			
			$result_t:=$result_t+Substring:C12($remainingText_t;1;(Position:C15(Char:C90(Carriage return:K15:38);$remainingText_t)-1))+Char:C90(Carriage return:K15:38)
			$remainingText_t:=Delete string:C232($remainingText_t;1;Position:C15(Char:C90(Carriage return:K15:38);$remainingText_t))
		Else 
			For ($character_i;($lineLength_i+1);1;-1)
				If ($remainingText_t[[$character_i]]=" ")
					$result_t:=$result_t+Substring:C12($remainingText_t;1;($character_i-1))+Char:C90(Carriage return:K15:38)
					$remainingText_t:=Delete string:C232($remainingText_t;1;$character_i)
					$character_i:=-2
				End if 
			End for 
			If ($character_i=0)
				$result_t:=$result_t+Substring:C12($remainingText_t;1;$lineLength_i)+Char:C90(Carriage return:K15:38)
				$remainingText_t:=Delete string:C232($remainingText_t;1;$lineLength_i)
			End if 
		End if 
	End if 
End while 

$0:=Substring:C12($result_t;1;(Length:C16($result_t)-1))
