//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Nav_AutoConfigure

// Automatically determines the contents of the Navigation Palette.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 16, 2003
// ----------------------------------------------------

C_LONGINT:C283($tableNumber_i; $button_i)
C_BOOLEAN:C305($invisible_b)

ARRAY POINTER:C280(<>Fnd_Nav_TempTablePtrsArray_aptr; 0)

Fnd_VS_GetTables(-><>Fnd_Nav_TempTablePtrsArray_aptr)
//If (Fnd_Gen_ComponentAvailable("Fnd_VS"))
//EXECUTE METHOD("Fnd_VS_GetTables"; *; -><>Fnd_Nav_TempTablePtrsArray_aptr)

//Else 
//For ($tableNumber_i; 1; Get last table number)
//If (Is table number valid($tableNumber_i))
//GET TABLE PROPERTIES($tableNumber_i; $invisible_b)
//If (Not($invisible_b))  // Include only visible tables.
//APPEND TO ARRAY(<>Fnd_Nav_TempTablePtrsArray_aptr; Table($tableNumber_i))
//End if 
//End if 
//End for 
//End if 

For ($button_i; 1; Size of array:C274(<>Fnd_Nav_TempTablePtrsArray_aptr))
	Fnd_Nav_AddButtonTable(<>Fnd_Nav_TempTablePtrsArray_aptr{$button_i})
End for 

ARRAY POINTER:C280(<>Fnd_Nav_TempTablePtrsArray_aptr; 0)
