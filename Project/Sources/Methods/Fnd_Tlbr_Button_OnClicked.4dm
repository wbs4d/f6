//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Tlbr_Button_OnClicked (->form button object)

  // Called whenever a button is clicked.  Each button in the toolbar has the 
  //  following script for their On Clicked event....
  //        Fnd_Tlbr_Button_OnClicked (Self)

  // Access: Shared

  // Parameters: 
  //   $1 : Pointer : A pointer to the clicked picture button object

  // Returns: Nothing

  // Created by Dave Batton on Feb 17, 2004
  // Based on a routine from Mark Mitchenall's Toolbar Component.
  // Modified by Dave Batton on May 10, 2005
  //   Removed the unused If statement after the EXECUTE.
  // Modified by Dave Batton on Nov 18, 2005
  //   Added support for menu buttons.
  // Modified by: Walt Nelson (3/1/10) - per Dave Nasralla toolbar fails to resolve the "Self" pointer 
  //    to get the variable name for some Windows users - on occasion
  // ----------------------------------------------------

C_POINTER:C301($1;$buttonClicked_ptr)
C_LONGINT:C283($buttonNumber_i;$item_i;$left_i;$top_i;$right_i;$bottom_i;$ignore_i)
C_TEXT:C284($buttonNumber_t;$statement_t;$items_t;$command_t;$buttonVariableName_t)

$buttonClicked_ptr:=$1

  // Figure out the button number from the variable name.
  //RESOLVE POINTER($buttonClicked_ptr;$buttonVariableName_t;$ignore_i;$ignore_i)
$buttonVariableName_t:=$2  // Modified by: Walt Nelson (3/1/10)

$buttonNumber_t:=Replace string:C233($buttonVariableName_t;"Fnd_Tlbr_PictureButton";"")
$buttonNumber_t:=Replace string:C233($buttonNumber_t;"_i";"")
$buttonNumber_i:=Num:C11($buttonNumber_t)

$statement_t:=Fnd_Tlbr_ObjectMethods_at{$buttonNumber_i}
$statement_t:=Replace string:C233($statement_t;"<FndButtonName>";Fnd_Tlbr_ObjectNames_at{$buttonNumber_i})
$statement_t:=Replace string:C233($statement_t;"<FndButtonLabel>";Fnd_Tlbr_ObjectLabels_at{$buttonNumber_i})

Case of 
	: ($buttonNumber_i<=0)
		  // Nothing to do.
		
		
	: (Fnd_Tlbr_Style_t="prefs")  // DB051118
		Fnd_Tlbr_CurrentButton (Fnd_Tlbr_ObjectNames_at{$buttonNumber_i})
		$statement_t:=Replace string:C233($statement_t;"<FndMenuNumber>";"")
		$statement_t:=Replace string:C233($statement_t;"<FndMenuLabel>";"")
		EXECUTE FORMULA:C63($statement_t)
		
		
	: ((Fnd_Tlbr_Style_t#"prefs") & (Size of array:C274(Fnd_Tlbr_ObjectMenuItems_at{$buttonNumber_i})>0))  // DB051118
		$items_t:=""
		For ($item_i;1;Size of array:C274(Fnd_Tlbr_ObjectMenuItems_at{$buttonNumber_i}))
			$items_t:=$items_t+Fnd_Tlbr_ObjectMenuItems_at{$buttonNumber_i}{$item_i}+";"
		End for 
		
		  // Mod by Wayne Stewart, 2016-11-24 - via pointer was not getting the correct position
		OBJECT GET COORDINATES:C663(*;$buttonVariableName_t;$left_i;$top_i;$right_i;$bottom_i)
		  //OBJECT GET COORDINATES($buttonClicked_ptr->;$left_i;$top_i;$right_i;$bottom_i)
		$command_t:="Fnd_Tlbr_Result_i:="+Command name:C538(542)+"(\""+$items_t+"\";0;"+String:C10($left_i+1)+";"+String:C10($top_i+Fnd_Tlbr_MenuTopOffset_i)+")"  // 542=Pop up menu
		EXECUTE FORMULA:C63($command_t)  // We can't call Pop up menu with extra parameters directly, since this won't compile in 4D 2003.x.
		
		If (Fnd_Tlbr_Result_i>0)
			$statement_t:=Replace string:C233($statement_t;"<FndMenuNumber>";String:C10(Fnd_Tlbr_Result_i))
			$statement_t:=Replace string:C233($statement_t;"<FndMenuLabel>";Fnd_Tlbr_ObjectMenuItems_at{$buttonNumber_i}{Fnd_Tlbr_Result_i})
			EXECUTE FORMULA:C63($statement_t)
		End if 
		
		
	Else 
		  //Normal picture button clicked... run the installed method.
		$statement_t:=Replace string:C233($statement_t;"<FndMenuNumber>";"")
		$statement_t:=Replace string:C233($statement_t;"<FndMenuLabel>";"")
		EXECUTE FORMULA:C63($statement_t)
End case 
