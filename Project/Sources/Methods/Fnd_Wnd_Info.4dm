//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Wnd_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo 
//   method for more information.

// Access Type: Shared

// Parameters: 
//   $1 : Text : Info desired ("version" or "name" or "position" or "title")

// Returns: 
//   $0 : Text : Response

// Created by Dave Batton on Dec 27, 2004
// Modified by Dave Batton on Jan 15, 2005
//   Added an option to get the window position and title variable values.
// Modified by Gary Boudreaux on Dec 21, 2008
//   Enhanced parameter description in header
// ----------------------------------------------------

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="version")
		$reply_t:="6.0"
		
	: ($request_t="name")
		$reply_t:="Foundation Windows"
		
	: ($request_t="position")  // DB050115
		Fnd_Wnd_Init
		$reply_t:=String:C10(Fnd_Wnd_WindowPosition_i)
		
	: ($request_t="title")  // DB050115
		Fnd_Wnd_Init
		$reply_t:=Fnd.Wnd.Title
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
