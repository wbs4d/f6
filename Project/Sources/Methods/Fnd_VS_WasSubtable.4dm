//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_WasSubtable (->table) --> Boolean

  // Returns true if the referenced table was a subtable
  // Test is done on the "id_added_by_converter" field name.

  // Access: Private

  // Parameters: 
  //   $1 : Pointer : A table

  // Returns: 
  //   $0 : Boolean : True if the table was converted from a subtable

  // Based on the WasSubtable method donated by by Jérôme Pupier
  // ----------------------------------------------------

C_BOOLEAN:C305($0;$wasSubtable_b)
C_POINTER:C301($1;$table_ptr)
C_LONGINT:C283($tableNum_i;$loop_i)
C_TEXT:C284($fieldName_t)

$table_ptr:=$1

$wasSubtable_b:=False:C215
$tableNum_i:=Table:C252($table_ptr)

  // Loop reverse order, just for a little optimization.
For ($loop_i;Get last field number:C255($table_ptr);1;-1)
	
	If (Is field number valid:C1000($tableNum_i;$loop_i))
		$fieldName_t:=Field name:C257($tableNum_i;$loop_i)
		If ($fieldName_t="id_added_by_converter")
			$wasSubtable_b:=True:C214
			$loop_i:=0
		End if 
	End if 
	
End for 

$0:=$wasSubtable_b
