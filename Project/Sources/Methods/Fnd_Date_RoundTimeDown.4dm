//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Date_RoundTimeDown (time) --> Time

  // Rounds the time down to the nearest quarter hour.

  // Access: Private

  // Parameters: 
  //   $1 : Time : The time to round

  // Returns: 
  //   $0 : Time : The rounded time

  // Created by Dave Batton on May 12, 2004
  // ----------------------------------------------------

C_TIME:C306($0;$1;$time)
C_TEXT:C284($timeStr)
C_LONGINT:C283($hours;$minutes)

$time:=$1

$timeStr:=String:C10($time;HH MM:K7:2)
$hours:=Num:C11(Substring:C12($timeStr;1;2))
$minutes:=Num:C11(Substring:C12($timeStr;4;2))

Case of 
	: ($minutes<15)
		$minutes:=0
	: ($minutes<30)
		$minutes:=15
	: ($minutes<45)
		$minutes:=30
	Else 
		$minutes:=45
End case 

$timeStr:=String:C10($hours;"00")+":"+String:C10($minutes;"00")
$time:=Time:C179($timeStr)

$0:=$time