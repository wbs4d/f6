//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Gen_Reg_CheckTimout

  // Checks to see if we've reached the demo timeout value.  If so, an alert dialog is displayed
  //   and ABORT is called.

  // Method Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jan 1, 2004
  // Modified by Dave Batton on Jan 29, 2004
  //   To avoid problems, the timeout is no longer checked on 4D Server.
  // Modified by Dave Batton on 23 Mai 2004
  //   Updated the localization technique.
  // ----------------------------------------------------

If (<>Fnd_Gen_Reg_State_i#1)
	If (Application type:C494#4D Server:K5:6)  // DB040129 - Don't disrupt 4D Server. It gets really confusing for developers on 4D Client.
		If (<>Fnd_Gen_Reg_Timeout_i<=Tickcount:C458)
			<>Fnd_Gen_Reg_Timeout_i:=Tickcount:C458+(30*60)  // Allow enough extra time to display the alert (30 seconds).
			Case of 
				: (<>Fnd_Gen_Reg_LanguageCode_t="FR")
					Fnd_Gen_Alert ("Cette version de démo de Foundation 5 a dépassé la période d'essai.";"OK")  // wwn 04/28/09 added "OK" for button
				Else 
					Fnd_Gen_Alert ("This demo version of Foundation 5 has timed out.";"OK")
			End case 
			<>Fnd_Gen_Reg_Timeout_i:=Tickcount:C458+(30*60)  // Tell them again in 30 seconds.
			  //If (Fnd_Gen_ComponentAvailable ("Fnd_Shell"))
			  //Fnd_Gen_QuitNow (True)
			  //Else 
			  //ABORT
			  //End if 
		End if 
	End if 
End if 
