//%attributes = {"invisible":true}
/* Fnd_FCS_ListToTextFile  {(ListName)}
Project Method: Fnd_FCS_ListToTextFile

Dumps the contents of a 4D list into disk file in the resources folder

This can only be used in development!!!

Access: Private

Parameters: 
   $1 : Text :: The List Name (optional)

Created by Wayne Stewart (2019-10-30)
     waynestewart@mac.com
*/

C_TEXT:C284($ListName_t; $ListContents_t; $Path_t; $FileName_t)
C_LONGINT:C283($i)
// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(Fnd_FCS_ListToTextFile; $1)
	C_TEXT:C284(Fnd_FCS_ListToTextFile; $1)
End if 

If (Count parameters:C259=1)
	$ListName_t:=$1
	OK:=1
Else 
	$ListName_t:=Request:C163("Please enter the list name:")
End if 

If (OK=1)
	If (Fnd_FCS_ConfirmListExists($ListName_t))
		ARRAY TEXT:C222($ListElements_at; 0)
		LIST TO ARRAY:C288($ListName_t; $ListElements_at)
		
		For ($i; 1; Size of array:C274($ListElements_at))
			$ListContents_t:=$ListContents_t+$ListElements_at{$i}+Char:C90(Carriage return:K15:38)
		End for 
		
		$ListContents_t:=Substring:C12($ListContents_t; 1; Length:C16($ListContents_t)-1)
		
		$Path_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Lists"+Folder separator:K24:12
		
		If (Test path name:C476($Path_t)#Is a folder:K24:2)
			CREATE FOLDER:C475($Path_t; *)
		End if 
		
		$FileName_t:=$Path_t+$ListName_t+".txt"
		
		TEXT TO DOCUMENT:C1237($FileName_t; $ListContents_t; UTF8 text without length:K22:17)
		SHOW ON DISK:C922($FileName_t)
		
	Else 
		Alert2("The list \""+$ListName_t+"\" is undefined.")
	End if 
	
	
End if 

