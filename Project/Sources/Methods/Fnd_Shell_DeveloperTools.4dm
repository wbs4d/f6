//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Shell_DeveloperTools

// Initialises 4D POP & the Code Analysis components
//  Will only run interpreted
// http://openroaddevelopment.com/projects_4dComponent_CodeAnalysis.html
// http://download.4d.com/Components/4Dv16/4DPop.zip

// Access: Private

// Created by Wayne Stewart (2017-03-24)
//     waynestewart@mac.com
// Mod by Wayne Stewart, 2018-04-16 - Removed automatic export which was quite painful in remote mode!

// ----------------------------------------------------

ARRAY TEXT:C222($Components_at; 0)
COMPONENT LIST:C1001($Components_at)

If (Not:C34(Is compiled mode:C492))
	
	If (Find in array:C230($Components_at; "4DPop")>0)
		EXECUTE METHOD:C1007("4DPop_Palette")
	End if 
	
	If (Find in array:C230($Components_at; "Code Analysis")>0)
		EXECUTE METHOD:C1007("CA_ShowQuickLauncher")
	End if 
	
	
End if 