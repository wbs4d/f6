//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Tbl_CreateAccessArray

// Build the <>Fnd_Tbl_UserCanAccess_ab array.
// This array stores whether a person can display the various tables.
// Create/modify the Fnd_Hook_Tbl_UserCanDisplay method in the host
// database to return whether or not the user can display its $1-> table.
// 
// This should be run on shell initialization or when new user signs in.
// It is used by Fnd_Reltd methods, but it could also be used by
// Fnd_Hook_Shell_OpenTable as a way of consolidating authorization to tables.

// Access: Private

// Created by Doug Hall (20030617)
// ----------------------------------------------------



var $tableNumber_i; $numTables_i : Integer
var $hasAuthorizationMethod_b; $userCanAccess_b : Boolean
var $Table_ptr : Pointer

$numTables_i:=Get last table number:C254

ARRAY BOOLEAN:C223(<>Fnd_Tbl_UserCanAccess_ab; $numTables_i)

$hasAuthorizationMethod_b:=(Fnd_Shell_DoesMethodExist("Fnd_Hook_Tbl_UserCanDisplay")=1)
For ($tableNumber_i; 1; $numTables_i)
	If (Is table number valid:C999($tableNumber_i))
		If ($hasAuthorizationMethod_b)
			$Table_ptr:=Table:C252($tableNumber_i)
			EXECUTE METHOD:C1007("Fnd_Hook_Tbl_UserCanAccess"; $userCanAccess_b; $Table_ptr)
			<>Fnd_Tbl_UserCanAccess_ab{$tableNumber_i}:=$userCanAccess_b
		Else 
			<>Fnd_Tbl_UserCanAccess_ab{$tableNumber_i}:=True:C214
		End if 
	Else 
		<>Fnd_Tbl_UserCanAccess_ab{$tableNumber_i}:=False:C215
	End if 
End for 


