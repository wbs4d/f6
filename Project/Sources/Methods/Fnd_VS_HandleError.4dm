//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_VS_HandleError

  // Special Error handling for the Virtual Structure component.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 13, 2005
  // ----------------------------------------------------

Case of 
	: (Error=36)  // Invalid table name error.
		If (Fnd_VS_CurrentTable_t="")
			ALERT:C41("Foundation Virtual Structure Component Error\r\r"+"One of the table names is not valid.")
		Else 
			ALERT:C41("Foundation Virtual Structure Component Error\r\r"+"One of the field names in the ["+Fnd_VS_CurrentTable_t+"] table is not valid.")
		End if 
		Error:=0  // So we don't trigger another error message.
		
	Else 
		  // If it's not our error, call the previously installed error handler (if any).
		If (Fnd_VS_OldErrorHandler_t#"")
			EXECUTE FORMULA:C63(Fnd_VS_OldErrorHandler_t)
		End if 
End case 