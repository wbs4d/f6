//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Prnt

  // Compiler variables related to the Foundation Printing routines.
  // Called by Fnd_Prnt_Init.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Feb 5, 2004
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Prnt_Initialized_b)
If (Not:C34(<>Fnd_Prnt_Initialized_b))  // So we only do this once.
	
End if 


  // Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Prnt_Alert ;$1)
	
	C_TEXT:C284(Fnd_Prnt_Info ;$0;$1)
End if 
