//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_IO_Init

// Initializes both the process and interprocess variables used
//   by the Fnd_IO routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Aug 28, 2003
// Modified by Dave Batton on Jan 29, 2004
//   Updated to version 4.0.2 (skipped 4.0.1).
//   This routine now recognizes if it's being run on 4D Server, and 
//   doesn't do things that aren't needed for that environment.
// Modified by Dave Batton on May 23, 2004
//   Updated the version number to 4.0.3.
// Modified by Dave Batton on Sep 9, 2004
//   Added initialization of the new <>Fnd_IO_MultiWindow_b variable.
//   Added initialization of the new <>Fnd_IO_DisplayNavButtons_b variable.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_IO_Info method.
// Modified by Dave Batton on Jan 6, 2005
//   Changed <>Fnd_IO_DisplayNavButtons_b to Fnd_IO_DisplayNavButtons_b.
//   Added the new Fnd_IO_ToolbarStyleName_t variable.
//   Added initialization of the new Fnd_IO_AddMultipleRecords_b variable.
//   Changed <>Fnd_IO_MultiWindow_b to a process variable.
// Modified by Dave Batton on Feb 7, 2007
//   Now initializes the  new Fnd_IO_AddTableNumber_i variable.
//[1] Added by: John Craig (12/14/09)
// Modified by Wayne Stewart, 2018-04-16 - Remove multiple calls to Fnd_Gen_ComponentCheck
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method

C_BOOLEAN:C305(<>Fnd_IO_Initialized_b; Fnd_IO_Initialized_b)

If (Not:C34(<>Fnd_IO_Initialized_b))  // So we only do this once.
	Compiler_Fnd_IO2
	
	Fnd_IO_Localize
	
	<>Fnd_IO_Initialized_b:=True:C214
End if 

If (Not:C34(Fnd_IO_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_IO
	
	Fnd.IO:=New object:C1471
	
	Fnd.IO.InputFormName_t:="Input"
	Fnd.IO.OutputFormName_t:="Output"
	Fnd.IO.ReportFormName_t:="Report"
	Fnd.IO.IconGroupName_t:=""  // DB050106 - Needs to be blank.
	Fnd.IO.AddTableNumber_i:=0  // DB070207 - Added for use in Fnd.IO.RecordEdited.
	Fnd.IO.AddRecordNumber_i:=No current record:K29:2
	Fnd.IO.DisplayNavButtons_b:=True:C214  // DB040929, DB050106
	Fnd.IO.AddMultipleRecords_b:=False:C215  // DB050124
	Fnd.IO.MultiWindow_b:=True:C214  // DB050223 - Changed to a process variable.
	Fnd.IO.ShowLockedMessage_b:=True:C214  //[1] Added by: John Craig (12/14/09)
	
	
	Fnd_IO_InputFormName_t:="Input"
	Fnd_IO_OutputFormName_t:="Output"
	Fnd_IO_ReportFormName_t:="Report"
	Fnd_IO_IconGroupName_t:=""  // DB050106 - Needs to be blank.
	Fnd_IO_AddTableNumber_i:=0  // DB070207 - Added for use in Fnd_IO_RecordEdited.
	Fnd_IO_AddRecordNumber_i:=No current record:K29:2
	Fnd_IO_DisplayNavButtons_b:=True:C214  // DB040929, DB050106
	Fnd_IO_AddMultipleRecords_b:=False:C215  // DB050124
	Fnd_IO_MultiWindow_b:=True:C214  // DB050223 - Changed to a process variable.
	Fnd_IO_ShowLockedMessage_b:=True:C214  //[1] Added by: John Craig (12/14/09)
	
	Fnd_IO_Initialized_b:=True:C214
End if 
