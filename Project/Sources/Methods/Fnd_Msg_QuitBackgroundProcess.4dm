//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Msg_QuitBackgroundProcess

  // Ends the message loop background process.
  // If there are messages still waiting to be delivered, this process waits until a
  //   timeout period is reached so hopefully the messages will get delivered.

  // Access: Shared

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Nov 27, 2004
  // ----------------------------------------------------

C_LONGINT:C283($backgroundProcessNumber_i;$timeOut_i)

Fnd_Msg_Init 

$backgroundProcessNumber_i:=Process number:C372(<>Fnd_Msg_BackgroundProcessName_t)

If ($backgroundProcessNumber_i>0)
	  // Wait for it to finish sending any messages it has to send.
	$timeOut_i:=Tickcount:C458+<>Fnd_Msg_TimeOutTicks_i
	While ((Process state:C330($backgroundProcessNumber_i)#Paused:K13:6) & (Tickcount:C458<$timeOut_i))
		DELAY PROCESS:C323(Current process:C322;5)
	End while 
	
	<>Fnd_Msg_RunBackgroundProcess_b:=False:C215
	RESUME PROCESS:C320($backgroundProcessNumber_i)  // In case it's paused.
	
	  // Wait for the loop to quit.
	$timeOut_i:=Tickcount:C458+(60*30)  // 30 seconds.
	While ((Process state:C330($backgroundProcessNumber_i)>0) & (Tickcount:C458<$timeOut_i))
		DELAY PROCESS:C323(Current process:C322;5)
	End while 
	
	  // Clear the message arrays.
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgFromProcNums_ai;0)
	ARRAY INTEGER:C220(<>Fnd_Msg_MsgToProcNums_ai;0)
	ARRAY BOOLEAN:C223(<>Fnd_Msg_MsgActiveFlags_ab;0)
	ARRAY TEXT:C222(<>Fnd_Msg_MsgTexts_at;0)
	ARRAY PICTURE:C279(<>Fnd_Msg_MsgBlobs_apic;0)
	ARRAY LONGINT:C221(<>Fnd_Msg_MsgEndTime_ai;0)
End if 