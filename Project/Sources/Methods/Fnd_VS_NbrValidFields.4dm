//%attributes = {"invisible":true,"shared":true}

  // ----------------------------------------------------
  // Nom utilisateur(OS): Vincent TOURNIER
  // Date et heure : 17/12/11, 21:37:35
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------
  // Méthode : Fnd_VS_NbrValidFields
  // Description
  // Returns the number of valid fields of a table
  // 
  // Paramètres
  // $1-tablenumber to scan
  // $0-number of valid fields
  // ----------------------------------------------------

C_LONGINT:C283($1;$tableNumber_l)  // table number
C_LONGINT:C283($0;$nbrValidFields_l)  // number of valid fields for$1 table
C_LONGINT:C283($element_i)  // loop indice

$tableNumber_l:=$1

Case of 
	: (Count parameters:C259=0)
		ALERT:C41(Current method name:C684+" : You must specify a table number.")
		
	: (Not:C34(Is table number valid:C999($tableNumber_l)))
		ALERT:C41(Current method name:C684+" : Invalid table number.")
		
	Else 
		
		For ($element_i;1;Get last field number:C255($tableNumber_l))
			If (Is field number valid:C1000($tableNumber_l;$element_i))
				$nbrValidFields_l:=$nbrValidFields_l+1
			End if 
		End for 
End case 

$0:=$nbrValidFields_l
