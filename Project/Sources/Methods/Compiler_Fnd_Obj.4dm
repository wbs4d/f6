//%attributes = {"invisible":true}
/* Compiler_Fnd_Obj
Project Method: Compiler_Fnd_Obj

Compiler declarations

Created by Wayne Stewart (2020-09-22)
     waynestewart@mac.com
*/

If (False:C215)
	C_OBJECT:C1216(Fnd_Obj_SaveToFile; $1)
	C_TEXT:C284(Fnd_Obj_SaveToFile; $2)
	C_BOOLEAN:C305(Fnd_Obj_SaveToFile; $3)
	
	C_OBJECT:C1216(Fnd_Obj_LoadFromFile; $0)
	C_TEXT:C284(Fnd_Obj_LoadFromFile; $1)
	
End if 
