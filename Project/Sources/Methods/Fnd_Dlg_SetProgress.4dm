//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Dlg_SetProgress (to or from{; to})

  // The developer calls this to set the From and To numbers.
  // If only one number is passed, the From is assumed to be 1.

  // Access: Shared

  // Parameters: 
  //   $1 : Longint : The ending number if no second parameter or the starting number
  //   $2 : Longint : The ending number (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 26, 2003
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Removed extraneous "[" in header
  // ----------------------------------------------------

C_LONGINT:C283($1;$2)

Fnd_Dlg_Init 

If (Count parameters:C259=1)
	<>Fnd_Dlg_ProgFrom_i:=1
	<>Fnd_Dlg_ProgTo_i:=$1
Else 
	<>Fnd_Dlg_ProgFrom_i:=$1
	<>Fnd_Dlg_ProgTo_i:=$2
End if 
