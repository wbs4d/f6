//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_SetSecretKey (key)

  // Allows the developer to set the secret key.
  // For security reasons, there is no routine to get the secret key.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The secret key

  // Returns: Nothing

  // Created by Dave Batton on Apr 23, 2005
  // ----------------------------------------------------

C_TEXT:C284($1)

Fnd_Reg_Init 

If ($1#"")
	<>Fnd_Reg_SecretKey_t:=$1
	
Else 
	Fnd_Gen_BugAlert (Current method name:C684;"The secret key may not be blank.")
End if 
