//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_VerifyStructureItem (component; table or field name; field type{; indexed}) --> Pointer

// Makes sure the required field exists. If not, the developer is notified.

// Access Type: Shared

// Parameters: 
//   $1 : Text :The component prefix
//   $2 : Text : The required table or field name
//   $3 : Longint : The expected field type
//   $4 : Boolean : The field should be indexed? (optional)

// Returns: 
//   $0 : Pointer : A pointer to the field

// Created by Dave Batton on Feb 7, 2006
// ----------------------------------------------------

C_POINTER:C301($0;<>Fnd_Gen_Field_ptr)
C_TEXT:C284($1;$2;$component_t;$fieldName_t;$oldErrorHandler_t)
C_LONGINT:C283($3;$fieldType_i)
C_BOOLEAN:C305($4;$indexed_b)

$component_t:=$1
$fieldName_t:=$2
$fieldType_i:=0
$indexed_b:=False:C215

If (Count parameters:C259>=3)
	$fieldType_i:=$3
	If (Count parameters:C259>=4)
		$indexed_b:=$4
	End if 
End if 

// We won't bother with calling Fnd_Gen_Init just to keep this fast.

$oldErrorHandler_t:=Method called on error:C704
ON ERR CALL:C155("Fnd_Gen_DummyMethod")
Error:=0

EXECUTE FORMULA:C63("<>Fnd_Gen_Field_ptr:=->"+$fieldName_t)

If (Error=0)
	If ($fieldName_t#"@]")  // Make sure it's a field, not a table.
		If ($fieldType_i=Type:C295(<>Fnd_Gen_Field_ptr->))
			If ($indexed_b)
				SET INDEX:C344(<>Fnd_Gen_Field_ptr->;True:C214;*)  // *=asynchronous
			End if 
			
		Else 
			Fnd_Gen_BugAlert(Current method name:C684;"The "+$fieldName_t+" field is set to a data type of "+String:C10(Type:C295(<>Fnd_Gen_Field_ptr->))+" rather than to the expected type of "+String:C10($fieldType_i)+".")
		End if 
	End if 
	
Else 
	If ($fieldName_t="@]")  // It's a table
		Fnd_Gen_BugAlert(Current method name:C684;"The "+$component_t+" component requires a table that does not exist: "+$fieldName_t)
	Else 
		Fnd_Gen_BugAlert(Current method name:C684;"The "+$component_t+" component requires a field that does not exist: "+$fieldName_t)
	End if 
	ON ERR CALL:C155($oldErrorHandler_t)
	ABORT:C156  // Give up. Otherwise we'll display an error message for every missing field, which could be a lot of error messages.
End if 

ON ERR CALL:C155($oldErrorHandler_t)


$0:=<>Fnd_Gen_Field_ptr
