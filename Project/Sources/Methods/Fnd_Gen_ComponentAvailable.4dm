//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_ComponentAvailable (component name) --> Boolean

// Returns true if the component is installed.

// Access: Shared

// Parameters:
//   $1 : Text : The name of the component

// Returns:
//   $0 : Boolean : True if the component is available

// Created by Dave Batton on Jul 28, 2003
// Modified by Dave Batton on Jan 30, 2005
//  This routine no longer changes the value of 4D's Error variable.
// ----------------------------------------------------


Fnd_Gen_Init

var $0 : Boolean
var $1 : Text

var $component_t; $methodName_t; $errorMethod_t; $formula_t; $result_t : Text
var $available_b : Boolean
var $oldErrorValue_i : Integer


ARRAY TEXT:C222($Components_at; 0)
ARRAY TEXT:C222($MethodNames_at; 0)

C_LONGINT:C283(Error)

$available_b:=False:C215
$component_t:=$1

Case of 
	: (Find in array:C230(<>Fnd_Gen_InstalledComponents_at; $component_t)>0)
		$available_b:=True:C214
		
	: (Find in array:C230(<>Fnd_Gen_UnavailableComponents_a; $component_t)>0)
		$available_b:=False:C215
		
		
	Else 
		// Maybe it's a Foundation sub-component?
		// If it is, then it should have an info method
		
		$methodName_t:=$component_t+"_Info"
		METHOD GET NAMES:C1166($MethodNames_at; $methodName_t)  // No * => search Foundation code
		$available_b:=(Size of array:C274($MethodNames_at)>0)
		
		
		If (Not:C34($available_b))  // Check the host database
			METHOD GET NAMES:C1166($MethodNames_at; $methodName_t; *)  // With * => search Host code
			$available_b:=(Size of array:C274($MethodNames_at)>0)
		End if 
		
		
		If (Not:C34($available_b))  // Try this last version, realistically this code won't ever be called
			$oldErrorValue_i:=Error  // DB050130
			$errorMethod_t:=Method called on error:C704
			ON ERR CALL:C155("Fnd_Gen_DummyMethod")
			Error:=0
			
			$formula_t:=$component_t+"_Info"
			
			EXECUTE METHOD:C1007($formula_t; $result_t; "name")
			$available_b:=(Error=0)  // The old way of doing it. It works again now
			
			// Restore the previous error value and error handler.
			ON ERR CALL:C155($errorMethod_t)
			Error:=$oldErrorValue_i  // DB050130
			
		End if 
		
		If ($available_b)
			APPEND TO ARRAY:C911(<>Fnd_Gen_InstalledComponents_at; $component_t)
		Else 
			APPEND TO ARRAY:C911(<>Fnd_Gen_UnavailableComponents_a; $component_t)
		End if 
End case 


$0:=$available_b