//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Log

// Compiler variables related to the Foundation Log routines.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 26, 2003
// Modified by Ed Heckman, October 27, 2015
// Mod by Wayne Stewart, (2021-04-15) - Rewritten for Foundation 6
// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(Fnd_Log_FileName;$1;$0)
	C_TEXT:C284(Fnd_Log_Info;$1;$0)
	C_LONGINT:C283(Fnd_Log_FormEventAsText;$1)
	C_TEXT:C284(Fnd_Log_FormEventAsText;$0)
	C_TEXT:C284(Fnd_Log_Path;$1;$0)
	C_LONGINT:C283(Fnd_Log_StopLogWriter;$1)
	C_TEXT:C284(Fnd_Log_DeclareLog;$1;$2;$3)
	
	C_TEXT:C284(Fnd_Log_UseLog;$1)
	C_TEXT:C284(Fnd_Log_CloseLog2;$1)
	C_TEXT:C284(Fnd_Log_CloseLog;$1)
	C_TEXT:C284(Fnd_Log_This;$1;$2;$3)
	C_BOOLEAN:C305(Fnd_Log_Enable;$0;$1)
	C_TEXT:C284(Fnd_Log_AddEntry;${1})
	C_LONGINT:C283(Fnd_Log_MaximumSize;$1;$0)
	
End if 

If (Fnd.Log=Null:C1517)
	ARRAY TEXT:C222(Fnd_Log_Paths_at;0)
	ARRAY TIME:C1223(Fnd_Log_DocRefs_ah;0)
	
End if 
