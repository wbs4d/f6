//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_Preferences

// Called from the menu bar.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Sep 29, 2003
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Pref component is no longer required.
// ----------------------------------------------------

//If (Fnd_Gen_ComponentAvailable("Fnd_Pref"))
//EXECUTE METHOD("Fnd_Pref_Display"; *; "Preferences")
//End if 

Fnd_Pref_Display("Preferences")