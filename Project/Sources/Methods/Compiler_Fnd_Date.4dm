//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_Date

// Compiler variables related to the Foundation Date routines.
// Called by Fnd_Date_tnit.

// Access: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Apr 28, 2004
// ----------------------------------------------------

// Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Date_Initialized_b)
If (Not:C34(<>Fnd_Date_Initialized_b))  // So we only do this once.
	C_POINTER:C301(<>Fnd_Date_CalendarDateField_ptr)
	C_TEXT:C284(<>Fnd_Date_CalendarDOW1_t; <>Fnd_Date_CalendarDOW2_t; <>Fnd_Date_CalendarDOW3_t; <>Fnd_Date_CalendarDOW4_t; <>Fnd_Date_CalendarDOW5_t; <>Fnd_Date_CalendarDOW6_t; <>Fnd_Date_CalendarDOW7_t)
	C_TEXT:C284(<>Fnd_Date_MonthYear_t)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay1_i; <>Fnd_Date_CalendarDay2_i; <>Fnd_Date_CalendarDay3_i; <>Fnd_Date_CalendarDay4_i; <>Fnd_Date_CalendarDay5_i; <>Fnd_Date_CalendarDay6_i; <>Fnd_Date_CalendarDay7_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay8_i; <>Fnd_Date_CalendarDay9_i; <>Fnd_Date_CalendarDay10_i; <>Fnd_Date_CalendarDay11_i; <>Fnd_Date_CalendarDay12_i; <>Fnd_Date_CalendarDay13_i; <>Fnd_Date_CalendarDay14_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay15_i; <>Fnd_Date_CalendarDay16_i; <>Fnd_Date_CalendarDay17_i; <>Fnd_Date_CalendarDay18_i; <>Fnd_Date_CalendarDay19_i; <>Fnd_Date_CalendarDay20_i; <>Fnd_Date_CalendarDay21_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay22_i; <>Fnd_Date_CalendarDay23_i; <>Fnd_Date_CalendarDay24_i; <>Fnd_Date_CalendarDay25_i; <>Fnd_Date_CalendarDay26_i; <>Fnd_Date_CalendarDay27_i; <>Fnd_Date_CalendarDay28_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay29_i; <>Fnd_Date_CalendarDay30_i; <>Fnd_Date_CalendarDay31_i; <>Fnd_Date_CalendarDay32_i; <>Fnd_Date_CalendarDay33_i; <>Fnd_Date_CalendarDay34_i; <>Fnd_Date_CalendarDay35_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarDay36_i; <>Fnd_Date_CalendarDay37_i; <>Fnd_Date_CalendarDay38_i; <>Fnd_Date_CalendarDay39_i; <>Fnd_Date_CalendarDay40_i; <>Fnd_Date_CalendarDay41_i; <>Fnd_Date_CalendarDay42_i)
	C_LONGINT:C283(<>Fnd_Date_CalendarWindowRef_i; <>Fnd_Date_OffscreenButton_i; <>Fnd_Date_CalendarTodayButton_i; <>Fnd_Date_CalendarPrevButton_i; <>Fnd_Date_CalendarNextButton_i)
	C_DATE:C307(<>Fnd_Date_CalendarDate_d)
End if 


//C_BOOLEAN(Fnd_Date_Initialized_b)
//If (Not(Fnd_Date_Initialized_b))  // So we only do this once.
//C_TEXT(Fnd.Date.EntryFilterValue)
//End if 


// Parameters
If (False:C215)
	C_BOOLEAN:C305(Fnd_Date_Calendar; $0)
	C_POINTER:C301(Fnd_Date_Calendar; $1)
	
	C_POINTER:C301(Fnd_Date_CalendarDayButton; $1)
	
	C_TEXT:C284(Fnd_Date_CalendarSetDate; $1; $2)
	
	C_TEXT:C284(Fnd_Date_DateAndTimeToISO; $0)
	C_DATE:C307(Fnd_Date_DateAndTimeToISO; $1)
	C_TIME:C306(Fnd_Date_DateAndTimeToISO; $2)
	
	C_TEXT:C284(Fnd_Date_DateToString; $0; $3)
	C_DATE:C307(Fnd_Date_DateToString; $1; $2)
	
	C_DATE:C307(Fnd_Date_EndOfMonth; $0; $1)
	
	C_POINTER:C301(Fnd_Date_EntryFilter; $1)
	
	C_TEXT:C284(Fnd_Date_Info; $0; $1)
	
	C_DATE:C307(Fnd_Date_ISOtoDate; $0)
	C_TEXT:C284(Fnd_Date_ISOtoDate; $1)
	
	C_TIME:C306(Fnd_Date_ISOtoTime; $0)
	C_TEXT:C284(Fnd_Date_ISOtoTime; $1)
	
	C_TEXT:C284(Fnd_Date_MonthName; $0)
	C_LONGINT:C283(Fnd_Date_MonthName; $1)
	
	C_TIME:C306(Fnd_Date_RoundTime; $0; $1; $2)
	
	C_TIME:C306(Fnd_Date_RoundTimeDown; $0; $1)
	
	C_TIME:C306(Fnd_Date_RoundTimeNear; $0; $1)
	
	C_DATE:C307(Fnd_Date_StringToDate; $0)
	C_TEXT:C284(Fnd_Date_StringToDate; $1; $2)
	
	C_TEXT:C284(Fnd_Date_SystemDateFormat; $0)
	
	C_DATE:C307(Fnd_Date_YearMonthDayToDate; $0)
	C_LONGINT:C283(Fnd_Date_YearMonthDayToDate; $1; $2; $3)
End if 
