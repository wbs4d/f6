//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: ProgOpen (total; message; {window title{; button text}})

  // Allows an older Foundation based database to use the new Fnd_Dlg routines.
  // Part of the Foundation Compatibility component.

  // Parameters: 
  //   $1 : Longint : The total to count up to
  //   $2 : Text : The message to display
  //   $3 : Text : The window title (optional)
  //   $4 : Text : The button label (optional)

  // Returns: Nothing

  // Created by Dave Batton on Jul 27, 2003
  // Modified by Dave Batton on Feb 18, 2006
  //   Removed an extra unused parameter from the call to Fnd_Cmpt_GetBooleanValue.
  // ----------------------------------------------------

C_LONGINT:C283($1)
C_TEXT:C284($2;$3;$4)

Fnd_Cmpt_Init 

Fnd_Dlg_SetProgress ($1)
Fnd_Dlg_SetText ($2)

If (Count parameters:C259>=3)
	Fnd_Wnd_Title ($3)
	If (Count parameters:C259>=4)
		Fnd_Dlg_SetButtons ($4)
	End if 
End if 

Fnd_Dlg_SetCancelable (Fnd_Cmpt_GetLongintValue ("ThermoCan")=1)

If (Fnd_Cmpt_GetBooleanValue ("gCenterWind"))  // DB-60218
	Fnd_Wnd_Position (Fnd_Wnd_CenterOnWindow)
End if 

Fnd_Cmpt_SetBooleanValue ("ThermoAbort";False:C215)

Fnd_Dlg_MessageOpen 
