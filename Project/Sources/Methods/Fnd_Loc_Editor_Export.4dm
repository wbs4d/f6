//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Loc_Editor_Export

// Exports a text file and saves it as a 4D list.
// Designed to be called from the Localization Editor dialog.  Don't call directly.

// Access Type: Private

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Jul 16, 2003
// Modified by Dave Batton on Jan 3, 2006
//   Now exports the developer override lists (they end with the * character) if they exist.
// Modified by : Vincent Tournier (9/12/2011)
// ----------------------------------------------------

C_TEXT:C284($export_t; $rootFolderPath_t; $docPath_t; $languageFolderPath_t; $directorySeparator_t)
C_TEXT:C284($recordDelimiter; $listName_t; $rootFolderName_t)
C_TIME:C306($docRef_time)
C_LONGINT:C283($languageNumber_i; $listElement_i; $moduleNumber_i)
ARRAY TEXT:C222(<>Fnd_Loc_ListStrings_at; 0)  // Modification by : Vincent TOURNIER(9/12/11)

$rootFolderName_t:="Foundation Localization"  // Also used in Fnd_Loc_Editor_Import.

$directorySeparator_t:=Folder separator:K24:12
$recordDelimiter:=Char:C90(Carriage return:K15:38)+(Char:C90(Line feed:K15:40)*Num:C11(Is Windows:C1573))

$rootFolderPath_t:=Select folder:C670("Where do you want to save the new folder?")

If (OK=1)
	$rootFolderPath_t:=$rootFolderPath_t+$rootFolderName_t+$directorySeparator_t
	
	If (Not:C34(Test path name:C476($rootFolderPath_t)=Is a folder:K24:2))
		CREATE FOLDER:C475($rootFolderPath_t)
		
		COPY ARRAY:C226(<>Fnd_Loc_EditorLanguageCodes_at; $languageCodes_at)
		INSERT IN ARRAY:C227($languageCodes_at; 1)  // Add an element for the look up codes.
		$languageCodes_at{1}:="Lookup Codes"
		
		For ($languageNumber_i; 1; Size of array:C274($languageCodes_at))
			$languageFolderPath_t:=$rootFolderPath_t+$languageCodes_at{$languageNumber_i}+$directorySeparator_t
			CREATE FOLDER:C475($languageFolderPath_t)
			
			For ($moduleNumber_i; 1; Size of array:C274(<>Fnd_Loc_EditorModules_at))
				$listName_t:=<>Fnd_Loc_EditorModules_at{$moduleNumber_i}
				If ($languageCodes_at{$languageNumber_i}#"Lookup Codes")
					$listName_t:=$listName_t+"_"+$languageCodes_at{$languageNumber_i}
				End if 
				
				// DB060103-If the developer has created an overriding list, use it instead.
				
				// BEGIN modification by : Vincent TOURNIER(9/12/11)
				Fnd_Loc_ListToArray($listName_t+"*"; -><>Fnd_Loc_ListStrings_at)
				If (Size of array:C274(<>Fnd_Loc_ListStrings_at)=0)
					Fnd_Loc_ListToArray($listName_t; -><>Fnd_Loc_ListStrings_at)
				End if 
				// END modification by : Vincent TOURNIER(9/12/11)
				
				$docPath_t:=$languageFolderPath_t+$listName_t+".TXT"
				$docRef_time:=Create document:C266($docPath_t; "TEXT")
				If (OK=1)
					For ($listElement_i; 1; Size of array:C274(<>Fnd_Loc_ListStrings_at))
						$export_t:=<>Fnd_Loc_ListStrings_at{$listElement_i}+$recordDelimiter
						SEND PACKET:C103($docRef_time; $export_t)
					End for 
					CLOSE DOCUMENT:C267($docRef_time)
					
				End if 
			End for 
		End for 
		
		ALERT:C41("The localization files have been created.")
		
	Else 
		ALERT:C41("A folder named "+$rootFolderName_t+" already exists in this location.")
	End if 
End if 
