//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Gen_Reg

  // Compiler variables related to the Foundation registration routines.

  // Method Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jul 16, 2003
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Gen_Reg_Initialized_b)
If (Not:C34(<>Fnd_Gen_Reg_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(<>Fnd_Gen_Reg_State_i;<>Fnd_Gen_Reg_FinishTime_i)
	C_LONGINT:C283(<>Fnd_Gen_Reg_OKButton_i;<>Fnd_Gen_Reg_CancelButton_i)
	C_LONGINT:C283(<>Fnd_Gen_Reg_RegisterButton_i;<>Fnd_Gen_Reg_BuyNowButton_i;<>Fnd_Gen_Reg_OffscreenButton_i)
	C_LONGINT:C283(<>Fnd_Gen_Reg_Timeout_i;<>Fnd_Gen_Reg_Status_i)
	
	C_TEXT:C284(<>Fnd_Gen_Reg_CountDown1_t;<>Fnd_Gen_Reg_CountDown2_t)
	C_TEXT:C284(<>Fnd_Gen_Reg_UserName_t;<>Fnd_Gen_Reg_ActivationCode_t)
	C_TEXT:C284(<>Fnd_Gen_Reg_ListName_t)
	C_TEXT:C284(<>Fnd_Gen_Reg_LanguageCode_t)
End if 


  // Parameters
If (False:C215)
	C_TEXT:C284(Fnd_Gen_Reg_Alert ;$1)
	
	C_TEXT:C284(Fnd_Gen_Reg_GetActivationCode ;$0)
	
	C_TEXT:C284(Fnd_Gen_Reg_GetUserName ;$0;$1)
	
	C_TEXT:C284(Fnd_Gen_Reg_GetValueForElement ;$0;$1)
	
	C_TEXT:C284(Fnd_Gen_Reg_LicenseFilePath ;$0)
	
	C_TEXT:C284(Fnd_Gen_Reg_Register ;$1;$2)
	
	C_BOOLEAN:C305(Fnd_Gen_Reg_Validate ;$0)
	C_TEXT:C284(Fnd_Gen_Reg_Validate ;$1;$2)
	
	C_TEXT:C284(Fnd_Gen_Reg_SaveRegInfo ;$1;$2)
End if 

