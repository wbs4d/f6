//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Sort_PrefsPut (pref name; text value)

// Adds an item to the user's preferences if the Fnd_Pref component is available.

// Access: Private

// Parameters: 
//   $1 : Text : The preference name
//   $2 : Text : The value to store

// Returns: Nothing

// Created by Dave Batton on Nov 6, 2004
// ----------------------------------------------------

C_TEXT:C284($1; $2)

//If (Fnd_Gen_ComponentAvailable("Fnd_Pref"))
//EXECUTE METHOD("Fnd_Pref_SetText"; *; $1; $2)
//End if 

Fnd_Pref_SetText($1; $2)