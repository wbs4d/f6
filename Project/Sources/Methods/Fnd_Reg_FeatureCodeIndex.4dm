//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_FeatureCodeIndex (feature code) --> Number

  // Returns the index in the arrays for the specified feature code.
  //   If the feature code hasn't been added to the arrays yet it is created.

  // Access: Private

  // Parameters: 
  //   $1 : Text : A feature code

  // Returns: 
  //   $0 : Longint : The index in the arrays

  // Created by Dave Batton on Jan 4, 2006
  // ----------------------------------------------------

C_LONGINT:C283($0;$index_i)
C_TEXT:C284($1;$featureCode_t)

$featureCode_t:=$1

$index_i:=Find in array:C230(<>Fnd_Reg_FeatureCodes_at;$featureCode_t)

If ($index_i=-1)
	$index_i:=Size of array:C274(<>Fnd_Reg_FeatureCodes_at)+1
	
	INSERT IN ARRAY:C227(<>Fnd_Reg_FeatureCodes_at;$index_i)
	INSERT IN ARRAY:C227(<>Fnd_Reg_FeatureNames_at;$index_i)
	INSERT IN ARRAY:C227(<>Fnd_Reg_BuyNowURLs_at;$index_i)
	INSERT IN ARRAY:C227(<>Fnd_Reg_DemoMessage_at;$index_i)
	
	<>Fnd_Reg_FeatureCodes_at{$index_i}:=$featureCode_t
	<>Fnd_Reg_FeatureNames_at{$index_i}:="Feature: "+$featureCode_t
	<>Fnd_Reg_BuyNowURLs_at{$index_i}:=""
	<>Fnd_Reg_DemoMessage_at{$index_i}:=""
End if 

$0:=$index_i
