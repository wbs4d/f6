//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Menu_ModifyRecords

// Called from the Modify Records item in the Edit menu.

// Access: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Dec 8, 2005
// Modified by Dave Batton on Mar 26, 2007
//   Modified so the Fnd_Shell component is no longer required.
// ----------------------------------------------------


If (Fnd_Shell_DoesMethodExist("Fnd_Hook_Shell_ModifyRecords")=1)
	EXECUTE METHOD:C1007("Fnd_Hook_Shell_ModifyRecords";*)
Else 
	Fnd_Rec_Modify
	
End if 
