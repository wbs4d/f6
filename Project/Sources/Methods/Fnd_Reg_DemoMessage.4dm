//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_DemoMessage ({feature code{; message}}) --> Text

  // Sets and gets the demo message for the specified feature code.
  // This text is displayed in the demo dialog.
  // This text can be localized, so it's not stored in the with the registration
  //   record. If you want to use something other than the default values, you'll
  //   need to call this method to set up the names before trying to retrieve them or
  //   before displaying the registration dialogs.
  // Pass an empty string to revert to the default message (from the localization strings).

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The feature code (optional)
  //   $2 : Text : The new demo message (optional)

  // Returns: 
  //   $0 : Text : The current demo message

  // Created by Dave Batton on Jan 2, 2006
  // Modified by Gary Boudreaux on Dec 22, 2008
  //   Modified header info to reflect optional aspect of $1
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$featureCode_t;$message_t)
C_LONGINT:C283($index_i)

Fnd_Reg_Init 

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

$message_t:=""

If (Not:C34(Semaphore:C143(<>Fnd_Reg_Semaphore_t;300)))  // Wait up to 5 seconds.
	$index_i:=Fnd_Reg_FeatureCodeIndex ($featureCode_t)
	
	If (Count parameters:C259>=2)
		<>Fnd_Reg_DemoMessage_at{$index_i}:=$2
	End if 
	
	$message_t:=<>Fnd_Reg_DemoMessage_at{$index_i}
	
	CLEAR SEMAPHORE:C144(<>Fnd_Reg_Semaphore_t)
End if 

$0:=$message_t
