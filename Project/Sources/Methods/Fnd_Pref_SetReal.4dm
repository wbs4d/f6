//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Pref_SetReal (name; value{; scope})

// Saves a real number value in the user's preferences file.

// Access Type: Shared

// Parameters: 
//   $1 : Text : The name of the preference item
//   $2 : Real : The real value to save
//   $3 : Longint : How to save it (optional)

// Returns: Nothing

// Created by Dave Batton on Oct 30, 2003
// Modified by Wayne Stewart on 2023-07-27:
//   Replaced the Interprocess "constants" with actual constants
// ----------------------------------------------------

C_TEXT:C284($1; $itemName_t; $itemValue_t)
C_REAL:C285($2; $itemValue_r)
C_LONGINT:C283($3; $itemScope_i; $element_i)

$itemName_t:=$1
$itemValue_r:=$2
If (Count parameters:C259>=3)
	$itemScope_i:=$3
Else 
	$itemScope_i:=Fnd_Pref_Local
End if 

Fnd_Pref_Init

If (Not:C34(Semaphore:C143(Fnd_Pref_Semaphore; Fnd_Pref_SemaphoreTimeout)))
	$itemValue_t:=String:C10($itemValue_r)
	Fnd_Pref_SetValue($itemName_t; Is real:K8:4; $itemValue_t; $itemScope_i)
	CLEAR SEMAPHORE:C144(Fnd_Pref_Semaphore)
	
Else 
	Fnd_Gen_BugAlert(Current method name:C684; "Timed-out waiting for the semaphore.")
End if 
