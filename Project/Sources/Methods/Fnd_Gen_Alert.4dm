//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Gen_Alert (message{; button text})

// Displays an alert message.  Uses the Foundation Dialog module alert if available.

// Access Type: Private

// Parameters: 
//   $1 : Text : The alert message text
//   $2 : Text : The button label (optional)

// Returns: Nothing

// Created by Dave Batton on Aug 20, 2003
// 2021-05-04 WBS - Remove Fnd_Gen_ComponentAvailable call 
// ----------------------------------------------------

C_TEXT:C284($1; $2; $message_t; $button_t)

$message_t:=$1  // A process variable since we may call this from multiple processes.
If (Count parameters:C259>=2)
	$button_t:=$2
End if 

Fnd_Dlg_Alert($message_t; $button_t)
