//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_File_DeleteEmptyFolder (Path)

// Deletes folder & subfolders (must be empty of files)

// Access: Shared

// Parameters: 
//   $1 : TEXT : Path to the folder

// Created by Wayne Stewart (2016-11-15)
//     waynestewart@mac.com
// ----------------------------------------------------



C_TEXT:C284($1)

C_LONGINT:C283($CurrentFolder_i; $NumberOfFolders_i)
C_TEXT:C284($PathName_t)

ARRAY TEXT:C222($FolderPaths_at; 0)

If (False:C215)
	C_TEXT:C284(Fnd_File_DeleteEmptyFolder; $1)
End if 


$PathName_t:=$1

If (Test path name:C476($PathName_t)=Is a folder:K24:2)
	FOLDER LIST:C473($PathName_t; $FolderPaths_at)
	
	$NumberOfFolders_i:=Size of array:C274($FolderPaths_at)
	
	For ($CurrentFolder_i; 1; $NumberOfFolders_i)
		Fnd_File_DeleteEmptyFolder($PathName_t+Folder separator:K24:12+$FolderPaths_at{$CurrentFolder_i})
	End for 
	
	DELETE FOLDER:C693($PathName_t)
End if 
