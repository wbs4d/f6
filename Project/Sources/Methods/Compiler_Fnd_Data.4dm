//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Compiler_Fnd_Data

  // Compiler variables related to the Foundation Data Entry routines.
  // Called by Fnd_Data_Init.

  // Access: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Apr 27, 2004
  // ----------------------------------------------------

  // Interprocess Variables
C_BOOLEAN:C305(<>Fnd_Data_Initialized_b)
If (Not:C34(<>Fnd_Data_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(<>Fnd_Data_UserBypassKey_i)
	
	ARRAY TEXT:C222(<>Fnd_Data_NameTitles_at;0)
End if 


  // Process Variables
C_BOOLEAN:C305(Fnd_Data_Initialized_b)
If (Not:C34(Fnd_Data_Initialized_b))  // So we only do this once.
	C_LONGINT:C283(Fnd_Data_FormatError_i)
End if 


  // Parameters
If (False:C215)
	C_BOOLEAN:C305(Fnd_Data_FormatBypass ;$0)
	
	C_LONGINT:C283(Fnd_Data_FormatBypassKey ;$0;$1)
	
	C_LONGINT:C283(Fnd_Data_FormatError ;$0)
	
	C_TEXT:C284(Fnd_Data_FormatPhone ;$1;$0)
	C_LONGINT:C283(Fnd_Data_FormatPhone ;$2)
	
	C_TEXT:C284(Fnd_Data_FormatText ;$1;$0)
	
	C_TEXT:C284(Fnd_Data_FormatWebURL ;$0;$1)
	
	C_TEXT:C284(Fnd_Data_FormatPostalCode ;$1;$0)
	C_LONGINT:C283(Fnd_Data_FormatPostalCode ;$2)
	
	C_TEXT:C284(Fnd_Data_Info ;$0;$1)
	
	C_TEXT:C284(Fnd_Data_ParseName ;$1)
	C_POINTER:C301(Fnd_Data_ParseName ;$2;$3;$4)
	
	C_LONGINT:C283(Fnd_Data_EmailAddressError ;$0)
	C_TEXT:C284(Fnd_Data_EmailAddressError ;$1)
End if 
