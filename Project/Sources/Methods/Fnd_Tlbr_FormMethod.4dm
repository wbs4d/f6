//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_FormMethod

// To be called from any form method that inherits the toolbar form.
// Expects the On Load and On Resize form events to be enabled.

// Access Type: Shared

// Parameters: None

// Returns: Nothing

// Created by Dave Batton on Feb 15, 2004
//   Based on code by Mark Mitchenall.
// Modified by Dave Batton on May 15, 2004
//   Now the Fnd_Tlbr_Redraw method is called when the window is resized,
//   regardless of the toolbar style.
// Modified by Dave Batton on Mar 22, 2005
//   The toolbar background now gets redrawn on all resizes with 4D 2004.
// Modified by Dave Batton on May 10, 2005
//   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
// Modified by Dave Batton on Nov 18, 2005
//   Added code to move the invisible "offscreen" buttons offscreen.
// Modified by Dave Batton on Feb 27, 2007
//   Now we call Fnd_Tlbr_StatusMessage to hide the status line in case it was previously set to blank.
// Modified by Ed Heckman, October 27, 2015
// ----------------------------------------------------

C_LONGINT:C283($objectNumber_i)

Fnd_Tlbr_Init

Case of 
	: (Current process:C322=1)
		// Don't do this stuff in the User process.
		
		
	: (Form event code:C388=On Load:K2:1)
		OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_HideThis@";False:C215)  // Hide objects we never want to show at runtime.
		OBJECT SET VISIBLE:C603(*;"Fnd_Tlbr_Info_@";False:C215)  // Hide the status bar background for the platform we're not using.
		
		// Move the offscreen buttons offscreen.
		OBJECT MOVE:C664(*;"Fnd_Tlbr_OffscreenButton@";0;-100)
		
		Fnd_Tlbr_ButtonsAreValid_b:=False:C215
		Fnd_Tlbr_BackgroundIsValid_b:=False:C215
		
		Fnd_Tlbr_UpdateButtons
		Fnd_Tlbr_UpdateBackground
		Fnd_Tlbr_StatusMessage(Fnd_Tlbr_Info_t)  // DB070227 - Hides the objects if they're blank.
		
		
	: (Form event code:C388=On Activate:K2:9)
		// Enable the buttons we disabled.
		For ($objectNumber_i;1;Fnd_Tlbr_UsedObjects_i)
			If (Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}=0)  // It's a button.
				If (Fnd_Tlbr_ObjectEnabled_ab{$objectNumber_i})
					//ENABLE BUTTON(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
					OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";True:C214)
				End if 
			End if 
		End for 
		
		
	: (Form event code:C388=On Deactivate:K2:10)
		// We disable all of the buttons when the window is not frontmost.
		For ($objectNumber_i;1;Fnd_Tlbr_UsedObjects_i)
			If (Fnd_Tlbr_ObjectTypes_ai{$objectNumber_i}=0)  // It's a button.
				//DISABLE BUTTON(Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}->)
				OBJECT SET ENABLED:C1123(*;"Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i";False:C215)
			End if 
		End for 
		
		
	: (Form event code:C388=On Resize:K2:27)
		If (Fnd_Tlbr_Style_t="Small@")  // Buttons may need to resize.
			Fnd_Tlbr_ButtonsAreValid_b:=False:C215
		End if 
		Fnd_Tlbr_UpdateButtons
		
		If (Application version:C493>="0800")  // DB050321 - Are we using 4D 2004?
			Fnd_Tlbr_BackgroundIsValid_b:=False:C215
		End if 
		Fnd_Tlbr_UpdateBackground
End case 
