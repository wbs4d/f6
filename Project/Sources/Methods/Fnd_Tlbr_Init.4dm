//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_Tlbr_Init

//Must be called at the start of a new process which contains dialogs which have 
//toolbars.  

// Based on Mark Mitchenall's Toolbar Component.
// ©2002 mitchenall.com
// Used with permission.

// Parameters: None

// Returns: Nothing

// Created by Mark Mitchenall on July 2, 2002
// Modified by Dave Batton on May 15, 2004
//   This method now initializes the Fnd_Tlbr_PlatformName_t variable.
//   Updated the version number to 4.0.3.
// Modified by Dave Batton on Dec 27, 2004
//   Moved the version number to the new Fnd_Tlbr_Info method.
// Modified by Dave Batton on Jan 9, 2005
//   Changed the default toolbar style name to "Default".
//   Moved the initialization of the Fnd_Tlbr_PlatformName_t variable to the Fnd_Tlbr_StyleName method.
// Modified by Dave Batton on May 10, 2005
//   Changed the Fnd_Tlbr_ObjectStates_at array to a Boolean array named Fnd_Tlbr_ObjectEnabled_ab.
// Modified by Dave Batton on Nov 18, 2005
//   Added support for menu buttons.
// Modified by Wayne Stewart, 2018-04-16 - Remove Fnd_Gen_ComponentCheck
// ----------------------------------------------------

Fnd_Init  // All Init methods must call this method


C_BOOLEAN:C305(<>Fnd_Tlbr_Initialized_b; Fnd_Tlbr_Initialized_b)
C_LONGINT:C283($objectNumber_i)

If (Not:C34(<>Fnd_Tlbr_Initialized_b))  // So we only do this once.
	
	Compiler_Fnd_Tlbr2
	
	<>Fnd_Tlbr_MaxObjects_i:=20  // Should match the available form objects.
	
	<>Fnd_Tlbr_Initialized_b:=True:C214
End if 

If (Not:C34(Fnd_Tlbr_Initialized_b))  // So we only do this once per process.
	Compiler_Fnd_Tlbr2
	
	// Initialize the configuration arrays.
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectNames_at; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectLabels_at; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectIconNames_at; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY INTEGER:C220(Fnd_Tlbr_ObjectTypes_ai; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY BOOLEAN:C223(Fnd_Tlbr_ObjectEnabled_ab; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectMethods_at; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectShortcuts_at; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY POINTER:C280(Fnd_Tlbr_Pictures_aptr; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY POINTER:C280(Fnd_Tlbr_PictureButtons_aptr; <>Fnd_Tlbr_MaxObjects_i)
	ARRAY TEXT:C222(Fnd_Tlbr_ObjectMenuItems_at; <>Fnd_Tlbr_MaxObjects_i; 0)
	
	For ($objectNumber_i; 1; <>Fnd_Tlbr_MaxObjects_i)
		Fnd_Tlbr_Pictures_aptr{$objectNumber_i}:=Get pointer:C304("Fnd_Tlbr_ButtonImage"+String:C10($objectNumber_i)+"_pic")
		Fnd_Tlbr_PictureButtons_aptr{$objectNumber_i}:=Get pointer:C304("Fnd_Tlbr_PictureButton"+String:C10($objectNumber_i)+"_i")
	End for 
	
	Fnd_Tlbr_Style_t:="Large"
	Fnd_Tlbr_UsedObjects_i:=0  // Keep track of how many objects we've used.
	Fnd_Tlbr_CurrentButton_t:=""  // Keep track of the "current" button's name (Mac OS X Prefs style).
	
	Fnd_Tlbr_Initialized_b:=True:C214  // Must be done before calling Fnd_Tlbr_StyleName.
	
	Fnd_Tlbr_Platform("Auto")  // Set the Fnd_Tlbr_Platform_t variable and initialize the variables for the default style.
End if 
