//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_DoesMethodExist ($methodName_t) --> Longint

  // Replaces call to Fnd_DoesMethodExist which was deprecated in V14

  // Access: Public

  // Parameters:
  //   $1 : Text : $methodName_t

  // Returns:
  //   $0 : Longint : -> 1 if method exists

  // Created by Walt Nelson on July 17, 2014:
  // ----------------------------------------------------

C_LONGINT:C283($0)
C_TEXT:C284($1)

C_BOOLEAN:C305($exists_b)
C_TEXT:C284($methodName_t)

ARRAY TEXT:C222($arrNames;0)

If (False:C215)
	C_LONGINT:C283(Fnd_Shell_DoesMethodExist ;$0)
	C_TEXT:C284(Fnd_Shell_DoesMethodExist ;$1)
End if 

$methodName_t:=$1

METHOD GET NAMES:C1166($arrNames;$methodName_t;*)  // * means get the host methods
$exists_b:=(Size of array:C274($arrNames)>0)

$0:=Num:C11($exists_b)