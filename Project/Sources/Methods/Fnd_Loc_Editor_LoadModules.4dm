//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Loc_Editor_LoadModules

  // Loads the array of Modules names in the Localization Editor window.

  // Access Type: Private

  // Parameters: None

  // Returns: Nothing

  // Created by Dave Batton on Jun 12, 2004
  // Modified by : Vincent Tournier (12/9/2011)
  // Modified by Ed Heckman, October 27, 2015
  // ----------------------------------------------------

C_LONGINT:C283($moduleNumber_i)
ARRAY TEXT:C222(<>Fnd_Loc_ModuleNames_at;0)  // Modification by : Vincent TOURNIER(9/12/11)
C_LONGINT:C283($moduleNumber_i)

<>Fnd_Loc_EditorModuleModified_b:=False:C215  // Gets set to True when the user modifies something.
<>Fnd_Loc_EditorModuleName_t:=""  // The editable name of the currently selected module.

  // The list of modules is a combination of all Foundation modules and any modules
  // the developer has added.  Duplicates and empty entries are then removed.

  // Load the Foundation component module names.
ARRAY TEXT:C222(<>Fnd_Loc_EditorModules_at;0)
Fnd_Gen_ComponentData (-><>Fnd_Loc_EditorModules_at)  // Get a list of Foundation components.

  // Load the developer's module names.

  // BEGIN Modification by : Vincent TOURNIER(9/12/11)
  // LIST TO ARRAY("Fnd_Loc_EditorModules*";◊Fnd_Loc_moduleNames_at)
Fnd_Loc_ListToArray ("Fnd_Loc_EditorModules*";-><>Fnd_Loc_ModuleNames_at)
  // END Modification by : Vincent TOURNIER(9/12/11)

For ($moduleNumber_i;1;Size of array:C274(<>Fnd_Loc_moduleNames_at))
	If (Find in array:C230(<>Fnd_Loc_EditorModules_at;<>Fnd_Loc_ModuleNames_at{$moduleNumber_i})=-1)
		INSERT IN ARRAY:C227(<>Fnd_Loc_EditorModules_at;1)
		<>Fnd_Loc_EditorModules_at{1}:=<>Fnd_Loc_ModuleNames_at{$moduleNumber_i}
	End if 
End for 

SORT ARRAY:C229(<>Fnd_Loc_EditorModules_at;>)

  // Verify all of the lists are still there.  Remove any empty lists.
For ($moduleNumber_i;Size of array:C274(<>Fnd_Loc_EditorModules_at);1;-1)
	Fnd_Loc_ListToArray (<>Fnd_Loc_EditorModules_at{$moduleNumber_i};-><>Fnd_Loc_EditorTempStringsEN_at)
	If (Size of array:C274(<>Fnd_Loc_EditorTempStringsEN_at)=0)
		DELETE FROM ARRAY:C228(<>Fnd_Loc_EditorModules_at;$moduleNumber_i)
	End if 
End for 

  // Clear the temporary array we just used.
ARRAY TEXT:C222(<>Fnd_Loc_EditorTempStringsEN_at;0)

<>Fnd_Loc_EditorModules_at:=0
Fnd_Loc_Editor_ClickOnModsList 

ARRAY TEXT:C222(<>Fnd_Loc_EditorGroups_at;0)
<>Fnd_Loc_EditorGroups_at:=0
Fnd_Loc_Editor_ClickOnGroupList 

ARRAY TEXT:C222(<>Fnd_Loc_EditorCodes_at;0)
<>Fnd_Loc_EditorCodes_at:=0
Fnd_Loc_Editor_ClickOnCodesList 
