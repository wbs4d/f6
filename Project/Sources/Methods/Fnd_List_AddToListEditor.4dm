//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_List_AddToListEditor (listname)

  // Called from the Fnd_Hook_List_SetEditableLists method.

  // Access: Shared

  // Parameters: 
  //   $1 : Text : The name of the list to make available.

  // Returns: Nothing

  // Created by Dave Batton on Nov 27, 2003
  // Modified by Gary Boudreaux on Dec 21, 2008
  //   Added parameter name to first line and corrected description in header
  // ----------------------------------------------------

C_TEXT:C284($1;$listName_t)

$listName_t:=$1

Fnd_List_Init 

APPEND TO LIST:C376(<>Fnd_List_ListOfLists_i;$listName_t;0)
