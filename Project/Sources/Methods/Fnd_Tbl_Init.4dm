//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_Tbl_Init

// Initializes both the process and interprocess variables used by the Fnd_Tbl routines.
// Method Type:    Protected

// Created by Wayne Stewart (2023-02-28)
//     waynestewart@mac.com
// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_Tbl_Initialised_b)
If (Not:C34(<>Fnd_Tbl_Initialised_b))  // So we only do this once.
	Compiler_Fnd_Tbl
	Fnd_Tbl_CreateAccessArray  // Setup the arrays
	
	<>Fnd_Tbl_Initialised_b:=True:C214
End if 
