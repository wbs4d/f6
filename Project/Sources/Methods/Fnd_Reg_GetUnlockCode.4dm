//%attributes = {"invisible":true,"shared":true}
  // ----------------------------------------------------
  // Project Method: Fnd_Reg_GetUnlockCode ({feature code}) --> Text

  // Returns the activation code that's been entered into the data file.
  //   Returns an empty string if the program hasn't been registered yet.

  // Access: Private

  // Parameters: 
  //   $1 : Text : The name of the feature in question (optional)

  // Returns: 
  //   $0 : Text : The feature's unlock code

  // Created by Dave Batton on August 16, 2003
  // ----------------------------------------------------

C_TEXT:C284($0;$1;$featureCode_t)

If (Count parameters:C259>=1)
	$featureCode_t:=$1
	If ($featureCode_t="")
		$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
	End if 
Else 
	$featureCode_t:=<>Fnd_Reg_DefaultFeatureCode_t
End if 

READ ONLY:C145(<>Fnd_Reg_Table_ptr->)
Fnd_Reg_LoadRegistrationRecord ($featureCode_t)

$0:=<>Fnd_Reg_UnlockCodeFld_ptr->
